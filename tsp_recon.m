narf_set_path

dbopen

sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid),...
     ' ORDER BY cellid'];
rundata=mysql(sql);

cellids={rundata.cellid};

keepidx=ones(size(cellids));
for kk=1:length(cellids),
    fname=rundata(kk).cellid;
    a=regexp(fname,'por(\d*).*$','tokens');
    if ~isempty(a),
        a=str2num(a{1}{1});
        if a>=31 && a<=65,
            keepidx(kk)=0;
        end
    end
end
 
cellids=cellids(find(keepidx));
cellcount=length(cellids);


mismatch=zeros(cellcount,1);
attband=zeros(cellcount,2);
resp1=[];
resp2=[];
respp=[];
if ~exist('animal','var') || strcmp(animal,'por'),
    animal='por';
    cset=1:15;
    logoffset=0.2;
else
    animal='sti';
    cset=16:29;
    logoffset=2;
end
lowDI=[];
for ii=cset, %cellcount,
    rasterfs=100;
    res=tsp_raster_load(cellids{ii},batchid,rasterfs,0);
    
    [sfiles,si]=sort(res.stimfiles);
    scodes=res.filecode(si);
    
    for jj=1:length(res.stimfiles),
        parmfile=res.stimpaths{jj};
        [tstim,tstimparam]=loadstimfrombaphy(parmfile,[],[],'envelope',...
                                           rasterfs,0,0,1);
        if ii==cset(1) && jj==1,
            stim=tstim;
            stimparam=tstimparam;
        end
        if size(tstim,2)>size(stim,2),
            tstim=tstim(:,1:size(stim,2),:);
        end
        if length(tstimparam.tags)==length(stimparam.tags)+4,
            tstimparam.tags=tstimparam.tags([1 6:end]);
        end
        for kk=1:length(stimparam.tags),
            if ~mismatch(ii) && ~strcmp(stimparam.tags{kk},tstimparam.tags{kk}),
                fprintf('mismatch %s %s (jj=%d k=%d)\n',...
                    res.stimfiles{[1 jj]},jj,kk);
                mismatch(ii)=1;
            end
        end
        if res.filecode{jj}(1)=='A',
            mm=max(res.baphyparms{jj}.Trial_TargetIdxFreq);
            mfc=min(find(res.baphyparms{jj}.Trial_TargetIdxFreq==mm));
            if isfield(res.baphyparms{jj},'Trial_TargetChannel'),
                tarchan=res.baphyparms{jj}.Trial_TargetChannel(mfc);
            else
                tarchan=mfc;
            end
            attband(ii,tarchan)=jj;
        end
    end
    
    if ~mismatch(ii),
        fprintf('adding %s to big resps\n',cellids{ii});
        
        p=setdiff(1:length(res.stimfiles),attband(ii,:));
        tr1=res.rresp{attband(ii,1)};
        tr2=res.rresp{attband(ii,2)};
        trp=cat(2,res.rresp{p});
        if ii==cset(1),
            trange=1:size(tr1,1);
        end
        mt=10;
        if size(tr1,2)<mt,
            tr1=cat(2,tr1,nan(size(tr1,1),mt-size(tr1,2),size(tr1,3)));
        end
        if size(tr2,2)<mt,
            tr2=cat(2,tr2,nan(size(tr2,1),mt-size(tr2,2),size(tr2,3)));
        end
        if size(trp,2)<mt,
            trp=cat(2,trp,nan(size(trp,1),mt-size(trp,2),size(trp,3)));
        end
        
        resp1=cat(4,resp1,tr1(trange,1:mt,:));
        resp2=cat(4,resp2,tr2(trange,1:mt,:));
        respp=cat(4,respp,trp(trange,1:mt,:));
        
        currcellcount=size(resp1,4);
        f1=max(find(strcmp(res.filecode,'A1')));
        f2=max(find(strcmp(res.filecode,'A2')));
        %if strcmp(cellids{ii}(1:6),'por049'),
        %    disp('unstable cell'),
        %    return
        %end
        if isempty(f1) | isempty(f2),
            disp('missing A1 or A2'),
            %return
        end
        if res.DI(1,f1)<50 | res.DI(3,f2)<50,
            disp('low DI');
            lowDI(currcellcount,1)=1;
            %return
        else
            lowDI(currcellcount,1)=0;
        end
        

    end
    
    
end
reconid=find(~mismatch);
if 0,
    goodDI=find(~lowDI);
    fprintf('keeping %d/%d cells after screening DI\n',length(goodDI));
    resp1=resp1(:,:,:,goodDI);
    resp2=resp2(:,:,:,goodDI);
    respp=respp(:,:,:,goodDI);
else
    goodDI=1:size(resp1,4);
end

if size(stim,3)==34,
    stim=stim(:,:,[1 6:end]);
    resp1=resp1(:,:,[1 6:end],:);
    resp2=resp2(:,:,[1 6:end],:);
    respp=respp(:,:,[1 6:end],:);
end

%  r0 - time X cell X stimid
%  stim0 - channel X time X stimid
%  test_r0 - time X cell X stimid
%[xc_pred,predxc,Hall]=quick_recon(r0,stim0,test_r0,params);
params.maxlag=[-20 10];
params.jackcount=18;

stim0=log(stim(:,1:size(respp,1),:)+logoffset)-log2(logoffset);

if 0,
    % use all data
    r0=permute(squeeze(nanmean(respp,2)),[1 3 2]);
    r0(isnan(r0))=0;
    r1=permute(squeeze(nanmean(resp1,2)),[1 3 2]);
    r2=permute(squeeze(nanmean(resp2,2)),[1 3 2]);
else
    % use only longest trial
    %r0=permute(squeeze(respp(:,1,:,:)),[1 3 2]);
    r0=permute(squeeze(nanmean(respp,2)),[1 3 2]);
    r0(isnan(r0))=0;
    r1=zeros(size(resp1,1),size(resp1,4),size(resp1,3)).*nan;
    r2=zeros(size(r1)).*nan;
    for ii=1:size(resp1,4),
        for jj=1:size(resp1,3),
            ff=sum(~isnan(resp1(:,:,jj,ii)));
            r1(:,ii,jj)=resp1(:,min(find(ff==max(ff))),jj,ii);
            ff=sum(~isnan(resp2(:,:,jj,ii)));
            r2(:,ii,jj)=resp2(:,min(find(ff==max(ff))),jj,ii);
        end
    end
    % shortcut, 
    %r1=permute(squeeze(resp1(:,1,:,:)),[1 3 2]);
    %r2=permute(squeeze(resp2(:,1,:,:)),[1 3 2]);
end
for ii=1:size(r1,2)
    r0(:,ii,:)=r0(:,ii,:)-nanmean(nanmean(r0(:,ii,:)));
    r1(:,ii,:)=r1(:,ii,:)-nanmean(nanmean(r1(:,ii,:)));
    r2(:,ii,:)=r2(:,ii,:)-nanmean(nanmean(r2(:,ii,:)));
end

keepcells=find(sum(sum(~isnan(r2+r1),3),1)>4000);
%r1=r1(:,keepcells,:);
%r2=r2(:,keepcells,:);
%r0=r0(:,keepcells,:);

%r0=nanmean(cat(4,r1,r2),4);

[teststim1,xcperchan]=quick_recon(r0,stim0,r1,params);
teststim1=nanmean(teststim1,4);
[teststim2,xcperchan]=quick_recon(r0,stim0,r2,params);
teststim2=nanmean(teststim2,4);
tt=find(~isnan(teststim1(1,:)) & ~isnan(teststim2(1,:)));
[xcov(teststim1(1,tt),stim0(1,tt),0,'coeff') ...
 xcov(teststim1(2,tt),stim0(2,tt),0,'coeff')]
[std(teststim1(1,tt)-stim0(1,tt)) ...
 std(teststim1(2,tt)-stim0(2,tt))]

[xcov(teststim2(1,tt),stim0(1,tt),0,'coeff') ...
 xcov(teststim2(2,tt),stim0(2,tt),0,'coeff')]
[std(teststim2(1,tt)-stim0(1,tt)) ...
 std(teststim2(2,tt)-stim0(2,tt))]

figure
subplot(2,1,1);
ttnot=setdiff(1:(size(stim0,2)*size(stim0,3)),tt);
ts=stim0(1,:,:);
%ts(isnan(teststim1(1,:,:)))=nan;
ts(ttnot)=nan;
plot([nanmean(ts,3)' nanmean(teststim1(1,:,:),3)']);
title(animal);

subplot(2,1,2);
ts=stim0(1,:,:);
%ts(isnan(teststim2(1,:,:)))=nan;
ts(ttnot)=nan;
plot([nanmean(ts,3)' nanmean(teststim2(1,:,:),3)']);

return


figure;
uset=unique(ceil(tt./size(r1,1)))
ucount=length(uset);
for uu=1:ucount,
    subplot(ucount,1,uu);
    stimidx=uset(uu);
    plot(stim0(1,:,stimidx)','Color',[0.4 0.4 0.4]);
    hold on
    plot(teststim1(1,:,stimidx)','Color',[1 0 0]);
    plot(teststim2(1,:,stimidx)','Color',[0 0 1]);
    hold off
    if uu==1,
        title(animal);
    end
end
legend('actual','attin','attaway');
fullpage portrait

figure;
subplot(2,2,1);
plot(stim0(1,tt),teststim1(1,tt),'.','Color',[1 0 0])
subplot(2,2,2);
plot(stim0(1,tt),teststim2(1,tt),'.','Color',[0 0 1])
subplot(2,1,2);
plot(stim0(1,tt)','Color',[0.4 0.4 0.4]);
hold on
plot(teststim1(1,tt)','Color',[1 0 0]);
plot(teststim2(1,tt)','Color',[0 0 1]);
hold off


figure
hb=bar([0.6278 0.6987; 0.4744 0.6067]);
set(hb(1),'FaceColor',[1 0 0]);
set(hb(2),'FaceColor',[0 0 1]);
xlabel('Animal 1 (n=15) -- Animal 2 (n=14)');
ylabel('Reconstruction Accuracy');
legend(-1,'attend in','attend away');


figure
subplot(2,1,1);
plot([nanmean((stim0(1,:,:)),3)' ...
      nanmean((stim0(1,:,:)),3)'+...
      nanmean(abs(stim0(1,:,:)-teststim1(1,:,:)),3)'-0.025])

subplot(2,1,2);
plot([nanmean((stim0(1,:,:)),3)' ...
      nanmean((stim0(1,:,:)),3)'+...
      nanmean(abs(stim0(1,:,:)-teststim2(1,:,:)),3)'-0.025])






