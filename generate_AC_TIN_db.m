% AC data from multichannel recording

% Active pure tone/post-passive

ACdata(1).cellid='TAR010c-01-1';
ACdata(1).area='AC';
ACdata(1).type='Var_SNR';
ACdata(1).note='multichannel recordings in TAR AC';
ACdata(1).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(1).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(1).passive_rawid=123676;
ACdata(1).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(1).active_rawid=123675;
ACdata(1).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(1).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

ACdata(2).cellid='TAR010c-06-1';
ACdata(2).area='AC';
ACdata(2).type='Var_SNR';
ACdata(2).note='multichannel recordings in TAR AC';
ACdata(2).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(2).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(2).passive_rawid=123676;
ACdata(2).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(2).active_rawid=123675;
ACdata(2).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(2).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

ACdata(3).cellid='TAR010c-12-1';
ACdata(3).area='AC';
ACdata(3).type='Var_SNR';
ACdata(3).note='multichannel recordings in TAR AC';
ACdata(3).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(3).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(3).passive_rawid=123676;
ACdata(3).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(3).active_rawid=123675;
ACdata(3).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(3).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(4).cellid='TAR010c-15-1';
ACdata(4).area='AC';
ACdata(4).type='Var_SNR';
ACdata(4).note='multichannel recordings in TAR AC';
ACdata(4).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(4).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(4).passive_rawid=123676;
ACdata(4).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(4).active_rawid=123675;
ACdata(4).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(4).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(5).cellid='TAR010c-18-1';
ACdata(5).area='AC';
ACdata(5).type='Var_SNR';
ACdata(5).note='multichannel recordings in TAR AC';
ACdata(5).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(5).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(5).passive_rawid=123676;
ACdata(5).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(5).active_rawid=123675;
ACdata(5).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(5).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(6).cellid='TAR010c-19-1';
ACdata(6).area='AC';
ACdata(6).type='Var_SNR';
ACdata(6).note='multichannel recordings in TAR AC';
ACdata(6).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(6).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(6).passive_rawid=123676;
ACdata(6).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(6).active_rawid=123675;
ACdata(6).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(6).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(7).cellid='TAR010c-21-1';
ACdata(7).area='AC';
ACdata(7).type='Var_SNR';
ACdata(7).note='multichannel recordings in TAR AC';
ACdata(7).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(7).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(7).passive_rawid=123676;
ACdata(7).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(7).active_rawid=123675;
ACdata(7).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(7).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(8).cellid='TAR010c-21-2';
ACdata(8).area='AC';
ACdata(8).type='Var_SNR';
ACdata(8).note='multichannel recordings in TAR AC';
ACdata(8).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(8).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(8).passive_rawid=123676;
ACdata(8).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(8).active_rawid=123675;
ACdata(8).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(8).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(9).cellid='TAR010c-27-1';
ACdata(9).area='AC';
ACdata(9).type='Var_SNR';
ACdata(9).note='multichannel recordings in TAR AC';
ACdata(9).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(9).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(9).passive_rawid=123676;
ACdata(9).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(9).active_rawid=123675;
ACdata(9).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(9).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(10).cellid='TAR010c-27-2';
ACdata(10).area='AC';
ACdata(10).type='Var_SNR';
ACdata(10).note='multichannel recordings in TAR AC';
ACdata(10).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(10).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(10).passive_rawid=123676;
ACdata(10).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(10).active_rawid=123675;
ACdata(10).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(10).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(11).cellid='TAR010c-30-1';
ACdata(11).area='AC';
ACdata(11).type='Var_SNR';
ACdata(11).note='multichannel recordings in TAR AC';
ACdata(11).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(11).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(11).passive_rawid=123676;
ACdata(11).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(11).active_rawid=123675;
ACdata(11).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(11).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(12).cellid='TAR010c-33-1';
ACdata(12).area='AC';
ACdata(12).type='Var_SNR';
ACdata(12).note='multichannel recordings in TAR AC';
ACdata(12).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(12).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(12).passive_rawid=123676;
ACdata(12).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(12).active_rawid=123675;
ACdata(12).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(12).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(13).cellid='TAR010c-58-1';
ACdata(13).area='AC';
ACdata(13).type='Var_SNR';
ACdata(13).note='multichannel recordings in TAR AC';
ACdata(13).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(13).passive_spike='TAR010c10_p_PTD.spk.mat';
ACdata(13).passive_rawid=123676;
ACdata(13).active_spike='TAR010c09_a_PTD.spk.mat';
ACdata(13).active_rawid=123675;
ACdata(13).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(13).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Active pure tone 2/post-passive

ACdata(14).cellid='TAR010c-01-1';
ACdata(14).area='AC';
ACdata(14).type='Var_SNR';
ACdata(14).note='multichannel recordings in TAR AC';
ACdata(14).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(14).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(14).passive_rawid=123681;
ACdata(14).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(14).active_rawid=123677;
ACdata(14).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(14).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

ACdata(15).cellid='TAR010c-06-1';
ACdata(15).area='AC';
ACdata(15).type='Var_SNR';
ACdata(15).note='multichannel recordings in TAR AC';
ACdata(15).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(15).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(15).passive_rawid=123681;
ACdata(15).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(15).active_rawid=123677;
ACdata(15).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(15).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

ACdata(16).cellid='TAR010c-12-1';
ACdata(16).area='AC';
ACdata(16).type='Var_SNR';
ACdata(16).note='multichannel recordings in TAR AC';
ACdata(16).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(16).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(16).passive_rawid=123681;
ACdata(16).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(16).active_rawid=123677;
ACdata(16).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(16).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(17).cellid='TAR010c-15-1';
ACdata(17).area='AC';
ACdata(17).type='Var_SNR';
ACdata(17).note='multichannel recordings in TAR AC';
ACdata(17).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(17).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(17).passive_rawid=123681;
ACdata(17).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(17).active_rawid=123677;
ACdata(17).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(17).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(18).cellid='TAR010c-18-1';
ACdata(18).area='AC';
ACdata(18).type='Var_SNR';
ACdata(18).note='multichannel recordings in TAR AC';
ACdata(18).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(18).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(18).passive_rawid=123681;
ACdata(18).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(18).active_rawid=123677;
ACdata(18).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(18).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(19).cellid='TAR010c-19-1';
ACdata(19).area='AC';
ACdata(19).type='Var_SNR';
ACdata(19).note='multichannel recordings in TAR AC';
ACdata(19).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(19).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(19).passive_rawid=123681;
ACdata(19).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(19).active_rawid=123677;
ACdata(19).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(19).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(20).cellid='TAR010c-21-1';
ACdata(20).area='AC';
ACdata(20).type='Var_SNR';
ACdata(20).note='multichannel recordings in TAR AC';
ACdata(20).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(20).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(20).passive_rawid=123681;
ACdata(20).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(20).active_rawid=123677;
ACdata(20).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(20).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(21).cellid='TAR010c-21-2';
ACdata(21).area='AC';
ACdata(21).type='Var_SNR';
ACdata(21).note='multichannel recordings in TAR AC';
ACdata(21).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(21).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(21).passive_rawid=123681;
ACdata(21).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(21).active_rawid=123677;
ACdata(21).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(21).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(22).cellid='TAR010c-27-1';
ACdata(22).area='AC';
ACdata(22).type='Var_SNR';
ACdata(22).note='multichannel recordings in TAR AC';
ACdata(22).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(22).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(22).passive_rawid=123681;
ACdata(22).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(22).active_rawid=123677;
ACdata(22).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(22).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(23).cellid='TAR010c-27-2';
ACdata(23).area='AC';
ACdata(23).type='Var_SNR';
ACdata(23).note='multichannel recordings in TAR AC';
ACdata(23).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(23).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(23).passive_rawid=123681;
ACdata(23).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(23).active_rawid=123677;
ACdata(23).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(23).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(24).cellid='TAR010c-30-1';
ACdata(24).area='AC';
ACdata(24).type='Var_SNR';
ACdata(24).note='multichannel recordings in TAR AC';
ACdata(24).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(24).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(24).passive_rawid=123681;
ACdata(24).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(24).active_rawid=123677;
ACdata(24).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(24).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(25).cellid='TAR010c-33-1';
ACdata(25).area='AC';
ACdata(25).type='Var_SNR';
ACdata(25).note='multichannel recordings in TAR AC';
ACdata(25).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(25).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(25).passive_rawid=123681;
ACdata(25).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(25).active_rawid=123677;
ACdata(25).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(25).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


ACdata(26).cellid='TAR010c-58-1';
ACdata(26).area='AC';
ACdata(26).type='Var_SNR';
ACdata(26).note='multichannel recordings in TAR AC';
ACdata(26).datapath='/auto/data/daq/Tartufo/TAR010c/sorted/';
ACdata(26).passive_spike='TAR010c12_p_PTD.spk.mat';
ACdata(26).passive_rawid=123681;
ACdata(26).active_spike='TAR010c11_a_PTD.spk.mat';
ACdata(26).active_rawid=123677;
ACdata(26).on_BF=nan; % 1=on BF target, 0=off BF, nan is for AC multichannel
ACdata(26).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


