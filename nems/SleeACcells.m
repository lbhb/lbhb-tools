%function [c,r]=SleeACcells()

%persistent ACcellids
%persistent rawids

% run following script, choosing the particular cell with the index
% ii.  when complete, you can identify relevant files with the
% filetype array:
%
% cellfiledata(find(filetype==1)) -- pre-passive
% cellfiledata(find(filetype==2)) -- active
% cellfiledata(find(filetype==3)) -- post-passive

narf_set_path;

ACcellids={'oni015b-b1'
    'oni015b-a1'
    'oni013b-d1'
    'oni013b-c1'
    'oni013a-d1'
    'oni008d-a1'
    'oni007b-d1'
    'oni007b-c1'
    'oni007b-c1'
    'oni007b-b1'
    'oni007b-b1'
    'oni007b-a1'
    'oni007b-a1'
    'oni005m-31-1'
    'oni005m-23-1'
    'oni005m-22-1'
    'oni005m-13-1'
    'oni005k-08-1'
    'min041c-d1'
    'min041c-c1'
    'min040c-b2'
    'min040c-b1'
    'min025b-d2'
    'min025b-d2'
    'min025b-d1'
    'min025b-d1'
    'min025b-c1'
    'min025b-c1'
    'min025b-a1'
    'min024b-d1'
    'min024b-d1'
    'min024b-c1'
    'min024b-c1'
    'min024b-a3'
    'min024b-a2'
    'min024b-a2'
    'min024b-a1'
    'min023b-d1'
    'min023b-c2'
    'min023b-c2'
    'min023b-b1'
    'min022b-d1'
    'min022b-b2'
    'min022b-a1'
    'min021a-d1'
    'min021a-c1'
    'min021a-a2'
    'min021a-a1'
    'min020a-d1'
    'min020a-b1'
    'mar036a-c1'
    'mar036a-c1'
    'mar036a-b1'
    'mar035a-a1'
    'mar033c-d2'
    'mar033c-d1'
    'mar033c-c1'
    'mar015b-d1'
    'mar015a-d3'
    'mar015a-d1'
    'mar014c-b2'
    'mar013b-d1'
    'mar013b-c1'
    'mar012b-d1'
    'mar012b-c1'
    'mar012b-a2'
    'mar012b-a1'
    'mar012a-c2'
    'mar011b-d2'
    'mar011b-a1'
    'mar010b-a1'
    'mar010b-a1'
    'mar009b-c1'
    'mar007a-c2'
    'mar006a-b1'
    'mar004a-a1'
    'lim045a-27-1'
    'lim045a-26-1'
    'lim045a-25-1'
    'lim045a-24-1'
    'lim045a-22-1'
    'lim045a-21-1'
    'lim045a-20-1'
    'lim045a-19-1'
    'lim045a-18-1'
    'lim045a-17-1'
    'lim045a-15-1'
    'lim045a-11-2'
    'lim045a-11-1'
    'lim045a-10-1'
    'lim045a-09-2'
    'lim045a-09-1'
    'lim045a-05-1'
    'lim043a-28-1'
    'lim043a-27-1'
    'lim043a-26-1'
    'lim043a-25-2'
    'lim043a-25-1'
    'lim043a-24-1'
    'lim043a-22-2'
    'lim043a-22-1'
    'lim043a-21-1'
    'lim043a-20-2'
    'lim043a-20-1'
    'lim043a-19-1'
    'lim043a-18-1'
    'lim043a-17-2'
    'lim043a-17-1'
    'lim043a-15-1'
    'lim043a-12-1'
    'lim043a-11-2'
    'lim043a-11-1'
    'lim043a-10-2'
    'lim043a-10-1'
    'lim043a-09-1'
    'lim043a-05-1'
    'lim043a-03-1'
    'lim028a-16-1'
    'lim027a-16-1'
    'lim027a-16-1'
    'ele090c-d2'
    'ele090b-c1'
    'ele089b-d1'
    'ele089b-d1'
    'ele089b-c2'
    'ele089b-c1'
    'ele089b-c1'
    'ele089b-b3'
    'ele089b-b2'
    'ele089b-b1'
    'ele088a-c1'
    'ele088a-b2'
    'ele087b-c1'
    'ele087b-b2'
    'ele087a-c2'
    'ele087a-c1'
    'ele087a-b1'
    'ele086b-c2'
    'ele086b-c1'
    'ele086b-b1'
    'ele086b-b1'
    'ele085a-c1'
    'ele081a-a1'
    'ele080c-b1'
    'ele080b-b1'
    'ele073b-d1'
    'ele073b-c2'
    'ele073b-c1'
    'ele072a-d2'
    'ele072a-d1'
    'ele072a-c1'
    'ele071c-d1'
    'ele071c-d1'
    'ele071c-c1'
    'ele071c-c1'
    'ele069b-d1'
    'ele069b-c1'
    'ele068a-d1'
    'ele068a-d1'
    'ele068a-c1'
    'ele068a-c1'
    'ele068a-c1'
    'ele066d-b1'
    'ele066b-a1'
    'ele064b-c2'
    'ele061c-c2'
    'ele061c-c1'
    'ele061c-b1'
    'ele061b-b1'
    'ele058b-c1'
    'ele057b-b1'
    'ele056d-c2'
    'ele056d-c1'
    'ele056c-a1'
    'ele055c-b2'
    'ele055c-b1'
    'ele055c-a2'
    'ele055c-a1'
    'ele055a-b2'
    'ele055a-b1'
    'ele054b-a1'
    'ele053c-a1'
    'ele053b-b1'
    'ele053b-a2'
    'ele053b-a1'
    'ele051a-a1'
    'ele050b-a1'
    'ele027a-c1'
    'ele027a-b1'
    'ele027a-b1'
    'ele027a-a2'
    'ele027a-a1'
    'ele027a-a1'
    'ele026b-c1'
    'ele026b-c1'
    'ele026b-b1'
    'ele026b-b1'
    'ele026b-a1'
    'ele026b-a1'
    'ele025b-a1'
    'ele025b-a1'
    'ele024b-c1'
    'ele024b-b2'
    'ele024b-b1'
    'ele024b-b1'
    'ele024b-a1'
    'ele024b-a1'
    'ele022a-b1'
    'ele022a-b1'
    'ele022a-a2'
    'ele022a-a1'
    'ele022a-a1'
    'ele021a-b2'
    'ele021a-b2'
    'ele021a-b1'
    'ele021a-b1'
    'ele021a-a1'
    'ele020c-c1'
    'ele020b-c1'
    'ele020b-b1'
    'ele020b-a1'
    'ele019b-a1'
    'ele019a-b1'
    'ele019a-a1'
    'ele017b-a1'
    'ele017b-a1'
    'ele016a-b1'
    'ele016a-a1'
    'dai026a-c1'
    'dai026a-a1'
    'dai020a-c1'
    'dai011b-d1'
    'dai011b-c1'
    'dai011b-a1'
    'dai010a-d1'
    'dai010a-c1'
    'dai010a-b1'
    'dai010a-a1'
    'dai009a-d1'
    'dai009a-a1'
    'dai008b-d1'
    'dai008b-b2'
    'dai008b-a1'
    'dai007a-b1'
    'dai003b-d1'
    'dai002b-b2'
    'dai002b-a1'};

ii=1;
cellid=dms_cells{ii};

DETRUNCLASS=32;
matchset=[32];

%find all matching DMS or TOR files
cellfiledata=dbgetscellfile('cellid',cellid,...
                            'runclassid',[1 DETRUNCLASS]);
dms_idx=[];
runclassid=cat(2,cellfiledata.runclassid);
stimspeedid=cat(2,cellfiledata.stimspeedid);
activefile=zeros(1,length(cellfiledata));
for testidx=1:length(cellfiledata),
   if ~isempty(findstr('_a_',cellfiledata(testidx).stimfile)),
      activefile(testidx)=1;
   end
end

rawid=cat(1,cellfiledata(find(activefile)).rawid);
if ~isempty(rawid),rawid=rawid(1);end

for testidx=find(ismember(runclassid,matchset)),
   if cellfiledata(testidx).rawid==rawid,
      dms_idx=testidx;
      activefile(dms_idx)=1;
   end
end

filetype=zeros(length(cellfiledata),1);
if ~isempty(dms_idx),
   filetype(dms_idx)=2;  % IE, DURING
   
   % try to find short torcs first
   tor1_idx=max(find((runclassid(1:dms_idx)==1 | ...
                     (runclassid(1:dms_idx)==42 & ~activefile(1:dms_idx)) | ...
                     (runclassid(1:dms_idx)==DETRUNCLASS &...
                      ~activefile(1:dms_idx))) & ...
                     stimspeedid(1:dms_idx)<3));
   if isempty(tor1_idx),
      % if no short torcs, fall back on long ones.
      tor1_idx=max(find(runclassid(1:dms_idx)==1 | ...
                        (runclassid(1:dms_idx)==DETRUNCLASS &...
                         ~activefile(1:dms_idx))));
   end
   if ~isempty(tor1_idx),
      filetype(tor1_idx)=1; %PRE TORC
   end
   
   tor2_idx=min(find((runclassid(dms_idx:end)==1 |...
                     (runclassid(dms_idx:end)==42&~activefile(dms_idx:end))|...
                     (runclassid(dms_idx:end)==DETRUNCLASS &...
                      ~activefile(dms_idx:end)) & ...
                      stimspeedid(dms_idx:end)<3)))+dms_idx-1;
   if isempty(tor2_idx),
      tor2_idx=min(find(runclassid(dms_idx:end)==1 |...
                        (runclassid(dms_idx:end)==DETRUNCLASS &...
                         ~activefile(dms_idx:end))))+dms_idx-1;
   end
   
   if ~isempty(tor2_idx),
      filetype(tor2_idx)=3; %POST TORC
   end
end





if isempty(ICcellids),
   
   
   
   
   % sean IC data, screen for matches to his onBF list
   disp('finding matches in Slee database')
   metapath='/auto/users/svd/data/slee/Aim7Analysis/IC_DataBase3';
   rawids=[];
   ICcellids=cell(1,95);
   for jj=1:95;
      
      %----------------------------
      %load analysis file
      fname = sprintf('%s/IC_Data_File_%d',metapath,jj);
      load(fname);
      sql=['SELECT * FROM gDataRaw',...
         ' WHERE parmfile like "',ICDataFile.PreFileName,'%"',...
         ' OR parmfile like "',ICDataFile.OnBFTarFileName,'%"'];
      rawdata=mysql(sql);
      rawids=cat(2,rawids,rawdata.id);
      
      siteid=ICDataFile.datID(1:min(findstr(ICDataFile.datID,'Ch')-1));
      channel=char(str2num(ICDataFile.datID(end-2))-1+'a');
      unit=(ICDataFile.datID(end));
      ICcellids{jj}=[siteid '-' channel unit];
      if strcmpi(ICcellids{jj},'btn25b-a1'), ICcellids{jj}='btn025b-a1'; end
      if strcmpi(ICcellids{jj},'btn26a-a1'), ICcellids{jj}='btn026a-a1'; end
      if strcmpi(ICcellids{jj},'btn33a-a1'), ICcellids{jj}='btn033a-a1'; end
      if strcmpi(ICcellids{jj},'btn33b-a1'), ICcellids{jj}='btn033b-a1'; end
      if strcmpi(ICcellids{jj},'btn33c-a1'), ICcellids{jj}='btn033c-a1'; end
      if strcmpi(ICcellids{jj},'btn0111b-b1'), ICcellids{jj}='btn111b-b1'; end
   end
end
c=ICcellids;
r=rawids;
