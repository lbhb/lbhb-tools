narf_set_path

% pick a batch
batchid=309;
get_ptd_tuning;

% pick a cell
cidx = 1;
cd=celldata(cidx);
bestidx = cd.bestsnr_idx;

% display the strf and some stats
figure();
stplot(cd.strf{bestidx},cd.lfreq(bestidx),cd.basep(bestidx),2);
title(sprintf('%s File %s BF=%.0f SNR=%.2f',cd.cellid,cd.parmfile{bestidx},...
   cd.bf(bestidx), cd.snr(bestidx)),'interpreter','none');

