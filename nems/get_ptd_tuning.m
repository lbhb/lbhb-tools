% get_ptd_tuning
% requires you to set batchid, then run script
% gets all cells in batch, measures STRF for each file for each cell.
% generates celldata with complete STRF information, cellframe for export
% to csv
%
% eg:
% batchid=309
% get_ptd_tuning
% struct2csv(cellframe,'tuning_info_batch_307.csv') 
%  resulting CSV file can be read by pandas.read_csv()
%

narf_set_path;
if ~exist('batchid','var')
   error('Need to set batchid (eg 307, 309, 311, 312, 295) before running get_ptd_tuning.m')
end

%batchid=307;

cachefile=sprintf('/auto/data/tmp/strf_batch%d.mat',batchid);
FORCE_RELOAD=0;

if ~exist(cachefile) || FORCE_RELOAD
   [cfd,cellids]=dbbatchcells(batchid);
   
   celldata=struct();
   ucellids=unique(cellids);
   jj=0;
   for ii = 1:length(ucellids)
      
      cellid=ucellids{ii};
      
      cellfiledata=dbbatchcells(batchid, cellid);
      filecount=length(cellfiledata);
      
      runclassid=cat(2,cellfiledata.runclassid);
      rawid=cat(2,cellfiledata.rawid);
      stimspeedid=cat(2,cellfiledata.stimspeedid);
      activefile=zeros(1,filecount);
      filetype=zeros(1,filecount);
      pre_active=1;
      for testidx=1:filecount
         if strcmpi(cellfiledata(testidx).behavior,'active'),
            activefile(testidx)=1;
            filetype(testidx)=2; % active
            pre_active=0;
         elseif pre_active
            filetype(testidx)=1; % pre-passive
         else
            filetype(testidx)=3; % post-passive
         end
      end
      
      celldata(ii).cellid = cellid;
      celldata(ii).rawid = rawid;
      celldata(ii).parmfile = {cellfiledata.stimfile};
      celldata(ii).filetype = filetype;
      celldata(ii).runclassid = runclassid;
      celldata(ii).stimspeedid = stimspeedid;
      celldata(ii).activefile = activefile;
      celldata(ii).cellfiledata = cellfiledata;
      
      % TODO?: Isolation
      celldata(ii).miniso = min([cellfiledata(filetype>0).isolation]);
      
      % figure out target freq and BF
      datfile_act=cellfiledata((filetype==2));
      
      datfile_pre=cellfiledata((filetype==1));
      %if empty use post-passive
      if isempty(datfile_pre) && sum(filetype==3)>0
         datfile_pre=cellfiledata((filetype==3));
         disp('Using post-passive')
      end
      
      %make sure pre and act files exist. If not skip and set gain=nan
      if (isempty(datfile_pre) || isempty(datfile_act))
         celldata(ii).TarFreq=nan;
         celldata(ii).BF=nan;
         celldata(ii).tardist=nan;
         disp('no pre or active file')
         %continue
      end
      
      %----------------------------
      %get the target frequency
      celldata(ii).TarFreq = zeros(1,filecount);
      celldata(ii).bf = zeros(1,filecount);
      celldata(ii).snr = zeros(1,filecount);
      celldata(ii).lfreq = zeros(1,filecount);
      celldata(ii).hfreq = zeros(1,filecount);
      celldata(ii).basep = zeros(1,filecount);
      celldata(ii).strf = {};
      celldata(ii).StStims = {};
      for fidx = 1:filecount
         if filetype(fidx)==2
            LoadMFile([cellfiledata(fidx).stimpath cellfiledata(fidx).stimfile]);
            celldata(ii).TarFreq(fidx) = exptparams.TrialObject.TargetHandle.Frequencies(1);
         end
         
         % get strf, bf and snr for each file
         stimfile = [cellfiledata(fidx).stimpath cellfiledata(fidx).stimfile];
         respfile = [cellfiledata(fidx).path cellfiledata(fidx).respfile];
         
         [bf,bw,lat,offlat,snr,linpred,strf0,StimParam]= ...
            tor_tuning(stimfile, respfile, cellfiledata(fidx).channum,...
            cellfiledata(fidx).unit);
         celldata(ii).bf(fidx) = bf;
         celldata(ii).lfreq(fidx) = StimParam.lfreq;
         celldata(ii).hfreq(fidx) = StimParam.hfreq;
         celldata(ii).basep(fidx) = StimParam.basep;
         celldata(ii).StStims{fidx} = StimParam.StStims;
         
         if isfinite(snr)
            celldata(ii).snr(fidx) = snr;
         end
         celldata(ii).strf{fidx} = strf0;
      end
      
      bidx = find(celldata(ii).snr == max(celldata(ii).snr), 1);
      celldata(ii).bestsnr_idx = bidx;
      
      %BF(jj,1)=tor_tuning(cellid);
      active1=find(activefile==1, 1 );
      celldata(ii).tardist = log2(celldata(ii).bf(bidx)) - log2(celldata(ii).TarFreq);
      %----------------------------
      %print results
      [ii celldata(ii).TarFreq/1000 celldata(ii).bf/1000 celldata(ii).tardist]
      [datfile_pre.isolation datfile_act.isolation]
      
      %end of outer loop
   end
   
   save(cachefile,'celldata');
else
   
   load(cachefile)
   
end

%tardist = [celldata.tardist];

%OnBF = isfinite(tardist) & abs(tardist)<0.5;
%OffBF = isfinite(tardist) & abs(tardist)>=0.5;

cellframe=struct();
for cidx=1:length(celldata)
   cellframe(cidx).cellid = celldata(cidx).cellid;
   cellframe(cidx).bestsnr = celldata(cidx).snr(celldata(cidx).bestsnr_idx);
   cellframe(cidx).bestbf = celldata(cidx).bf(celldata(cidx).bestsnr_idx);

   pcount=0;
   acount=1;
   for ff= 1:length(celldata(cidx).filetype)
      fidx = celldata(cidx).filetype(ff);
      if fidx==1 || fidx==3
         aname=sprintf('PASSIVE_%d',pcount);
         pcount=pcount+1;
      elseif fidx==2,
         if pcount==0,
            aname='ACTIVE_0';
         else
            aname=sprintf('ACTIVE_%d',acount);
            acount = acount+1;
         end
      end
      
      cellframe(cidx).([aname,'_snr']) = celldata(cidx).snr(ff);
      cellframe(cidx).([aname,'_bf']) = celldata(cidx).bf(ff);
      
      cellframe(cidx).(aname) = celldata(cidx).parmfile{ff};
      if fidx==2,
         cellframe(cidx).([aname,'_tf']) = celldata(cidx).TarFreq(ff);
         cellframe(cidx).([aname,'_tardist']) = celldata(cidx).tardist(ff);
      end
   end
end



%% ------------------------------------------------------------------------
%**************************************************************************
%plot stuff and compute stats
% NanSpotBF=find(isnan(BF));
% length(NanSpotBF)
% 
% 
% figure(2),clf
% plot(BF,TarFreq,'bo')
% 
% %compute tar freqs re BF in octaves
% TarDist=log2(TarFreq./BF);
% 
% figure(3),clf
% hist(TarDist)
% 
% 
% OnBFSpots=find(abs(TarDist)<0.5);
% OffBFSpots=find(abs(TarDist)>=0.5);

