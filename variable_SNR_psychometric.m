% copied from varSNR_behavior function results=varSNR_behavior(animal)
% to visualize psychometric functions - DS 2017-06-12 - modified 2018-07-16
function variable_SNR_psychometric(options)

baphy_set_path
narf_set_path
dbopen;

options.animal=getparm(options,'animal','Babybell');
options.cellprefix=getparm(options,'cellprefix','bbl');
options.save_fig=getparm(options,'save_fig',0);
options.include_training=getparm(options,'include_training',1);
options.pure_tone_epoch=getparm(options,'pure_tone_epoch',0);


% if strcmpi(animal,'Babybell')
%    cellprefix='bbl';
% elseif strcmpi(animal,'Tartufo')
%    cellprefix='TAR';
% elseif strcmpi(animal,'Turkeytail')
%    cellprefix='TTL';
% elseif strcmpi(animal,'Beartooth')
%    cellprefix='BRT';
% elseif strcmpi(animal, 'Leyva')
%    cellprefix='ley'
% end


if ~options.include_training
   sTrain=' AND gDataRaw.training=0';
else
   sTrain='';
end

if options.pure_tone_epoch
   PT=' AND gDataRaw.id>125259';
else
   PT='';
end


sql=['SELECT gDataRaw.*,gCellMaster.penid,gPenetration.pendate FROM gDataRaw',...
   ' INNER JOIN gCellMaster ON gDataRaw.masterid=gCellMaster.id'...
   ' INNER JOIN gPenetration ON gCellMaster.penid=gPenetration.id'...
     ' WHERE runclass="PTD"',...
     ' AND trials>20',...
     PT,...
     sTrain, ...
     ' AND not(resppath like "L:%")',...
     ' AND behavior="active"',...
     ' AND gDataRaw.cellid like "' options.cellprefix '%"',...
     ' AND not(gDataRaw.bad) ORDER BY gDataRaw.id'];

rawdata=mysql(sql);
filecount=length(rawdata);


training=nan(filecount,1);
difficulty=nan(filecount,1);
trialcount=nan(filecount,1);


% Populate a matrix with SNR/DI/freq/difficulty
di_matrix = [];

for ii=1:filecount
    rawid=rawdata(ii).id;
    training(ii)=rawdata(ii).training;
    disp(rawdata(ii).parmfile);
    [parms,perf]=dbReadData(rawdata(ii).id);
    badfile=0;
    
    % Only consider sessions where animal was doing task at "criterion"
    if parms.Trial_OveralldB<55
        badfile=1;
    end
  
    add_matrix = [];
    if ~badfile
        Trial_TargetIdxFreq=parms.Trial_TargetIdxFreq;
        tcount=length(Trial_TargetIdxFreq);
        
        % Separate blocks by task difficulty
        if tcount==5
            
            % Easy blocks
            if Trial_TargetIdxFreq(1)==0.3
                block_difficulty=0;
            % Even blocks
            elseif Trial_TargetIdxFreq(1)==0.2
                block_difficulty=1;
            % Hard blocks   
            elseif Trial_TargetIdxFreq(1)==0.1
                block_difficulty=2;
            end
            % Pure tone blocks
            if Trial_TargetIdxFreq(1)==0.8
                block_difficulty=3;
            end
            
            uDI=perf.uDiscriminationIndex;
            uHitRate=perf.uHitRate;
            uReactionTime=perf.uReactionTime;
            
            
            SNRs=parms.Trial_RelativeTarRefdB;
            
            TarFreq=parms.Tar_Frequencies(1);
            
            
            for jj=1:length(SNRs)
                add_x = [SNRs(jj) uDI(jj) TarFreq block_difficulty];
                add_matrix = cat(1, add_matrix, add_x);
             
            end
        end
        
    end
    
    di_matrix = cat(1, di_matrix, add_matrix);
end

% Calculate DI averaging across all freqs, all difficulties
di_matrix_all_freq=di_matrix(:,1:2);
unique_all_freq_SNRs=unique(di_matrix_all_freq(:,1));

mean_all_freq_di_matrix=zeros(length(unique_all_freq_SNRs),3);
mean_all_freq_di_matrix(:,1)=unique_all_freq_SNRs;

for jj=1:length(unique_all_freq_SNRs)
    
    jj_idx=find(di_matrix_all_freq(:,1)==unique_all_freq_SNRs(jj));
    
    mean_all_freq_di_matrix(jj,2)=nanmean(di_matrix_all_freq(jj_idx,2));
    mean_all_freq_di_matrix(jj,3)=nanstd(di_matrix_all_freq(jj_idx,2));
end

% Calculate DI averaging across all freqs, easy blocks

all_freq_easy_idx=find(di_matrix(:,4)==0);
di_matrix_all_freq_easy=di_matrix(all_freq_easy_idx,1:2);

unique_all_freq_easy_SNRs=unique(di_matrix_all_freq_easy(:,1));

mean_di_matrix_all_freq_easy=zeros(length(unique_all_freq_easy_SNRs),3);
mean_di_matrix_all_freq_easy(:,1)=unique_all_freq_easy_SNRs;

for jj=1:length(unique_all_freq_easy_SNRs)
    
    jj_idx=find(di_matrix_all_freq_easy(:,1)==unique_all_freq_easy_SNRs(jj));
    
    mean_di_matrix_all_freq_easy(jj,2)=nanmean(di_matrix_all_freq_easy(jj_idx,2));
    mean_di_matrix_all_freq_easy(jj,3)=nanstd(di_matrix_all_freq_easy(jj_idx,2));
end

% Calculate DI averaging across all freqs, medium blocks

all_freq_medium_idx=find(di_matrix(:,4)==1);
di_matrix_all_freq_medium=di_matrix(all_freq_medium_idx,1:2);

unique_all_freq_medium_SNRs=unique(di_matrix_all_freq_medium(:,1));

mean_di_matrix_all_freq_medium=zeros(length(unique_all_freq_medium_SNRs),3);
mean_di_matrix_all_freq_medium(:,1)=unique_all_freq_medium_SNRs;

for jj=1:length(unique_all_freq_medium_SNRs)
    
    jj_idx=find(di_matrix_all_freq_medium(:,1)==unique_all_freq_medium_SNRs(jj));
    
    mean_di_matrix_all_freq_medium(jj,2)=nanmean(di_matrix_all_freq_medium(jj_idx,2));
    mean_di_matrix_all_freq_medium(jj,3)=nanstd(di_matrix_all_freq_medium(jj_idx,2));
end

% Calculate DI averaging across all freqs, hard blocks

all_freq_hard_idx=find(di_matrix(:,4)==2);
di_matrix_all_freq_hard=di_matrix(all_freq_hard_idx,1:2);

unique_all_freq_hard_SNRs=unique(di_matrix_all_freq_hard(:,1));

mean_di_matrix_all_freq_hard=zeros(length(unique_all_freq_hard_SNRs),3);
mean_di_matrix_all_freq_hard(:,1)=unique_all_freq_hard_SNRs;

for jj=1:length(unique_all_freq_hard_SNRs)
    
    jj_idx=find(di_matrix_all_freq_hard(:,1)==unique_all_freq_hard_SNRs(jj));
    
    mean_di_matrix_all_freq_hard(jj,2)=nanmean(di_matrix_all_freq_hard(jj_idx,2));
    mean_di_matrix_all_freq_hard(jj,3)=nanstd(di_matrix_all_freq_hard(jj_idx,2));
end


% Calculate DI averaging across all freqs, pure tone blocks

all_freq_pt_idx=find(di_matrix(:,4)==3);
di_matrix_all_freq_pt=di_matrix(all_freq_pt_idx,1:2);

unique_all_freq_pt_SNRs=unique(di_matrix_all_freq_pt(:,1));

mean_di_matrix_all_freq_pt=zeros(length(unique_all_freq_pt_SNRs),3);
mean_di_matrix_all_freq_pt(:,1)=unique_all_freq_pt_SNRs;

for jj=1:length(unique_all_freq_pt_SNRs)
    
    jj_idx=find(di_matrix_all_freq_pt(:,1)==unique_all_freq_pt_SNRs(jj));
    
    mean_di_matrix_all_freq_pt(jj,2)=nanmean(di_matrix_all_freq_pt(jj_idx,2));
    mean_di_matrix_all_freq_pt(jj,3)=nanstd(di_matrix_all_freq_pt(jj_idx,2));
end

% Calculate mean DI for each freq range, medium only
% low frequency <=2000Hz
low_freq_idx=find(di_matrix(:,3)<=2000 & di_matrix(:,4)==1);
low_freq_matrix=di_matrix(low_freq_idx,1:2);

unique_low_freq_SNRs=unique(low_freq_matrix(:,1));

mean_low_freq_di_matrix=zeros(length(unique_low_freq_SNRs),3);
mean_low_freq_di_matrix(:,1)=unique_low_freq_SNRs;

for jj=1:length(unique_low_freq_SNRs)
    
    jj_idx=find(low_freq_matrix(:,1)==unique_low_freq_SNRs(jj));
    
    mean_low_freq_di_matrix(jj,2)=mean(low_freq_matrix(jj_idx,2));
    mean_low_freq_di_matrix(jj,3)=std(low_freq_matrix(jj_idx,2));
end

% medium frequency >2000Hz and <= 14000Hz
medium_freq_idx=find(di_matrix(:,3)>2000 & di_matrix(:,3)<=14000 & di_matrix(:,4)==1);
medium_freq_matrix=di_matrix(medium_freq_idx,1:2);

unique_medium_freq_SNRs=unique(medium_freq_matrix(:,1));

mean_medium_freq_di_matrix=zeros(length(unique_medium_freq_SNRs),3);
mean_medium_freq_di_matrix(:,1)=unique_medium_freq_SNRs;

for jj=1:length(unique_medium_freq_SNRs)
    
    jj_idx=find(medium_freq_matrix(:,1)==unique_medium_freq_SNRs(jj));
    
    mean_medium_freq_di_matrix(jj,2)=mean(medium_freq_matrix(jj_idx,2));
    mean_medium_freq_di_matrix(jj,3)=std(medium_freq_matrix(jj_idx,2));
end

% high frequency <14000Hz
high_freq_idx=find(di_matrix(:,3)>14000 & di_matrix(:,4)==1);
high_freq_matrix=di_matrix(high_freq_idx,1:2);

unique_high_freq_SNRs=unique(high_freq_matrix(:,1));

mean_high_freq_di_matrix=zeros(length(unique_high_freq_SNRs),3);
mean_high_freq_di_matrix(:,1)=unique_high_freq_SNRs;

for jj=1:length(unique_high_freq_SNRs)
    
    jj_idx=find(high_freq_matrix(:,1)==unique_high_freq_SNRs(jj));
    
    mean_high_freq_di_matrix(jj,2)=mean(high_freq_matrix(jj_idx,2));
    mean_high_freq_di_matrix(jj,3)=std(high_freq_matrix(jj_idx,2));
end

figure;

%all freqs all difficulties
ax1=subplot(3,2,1);
errorshade(mean_all_freq_di_matrix(:,1), mean_all_freq_di_matrix(:,2),...
    mean_all_freq_di_matrix(:,3));
hold on
plot(di_matrix_all_freq(:,1), di_matrix_all_freq(:,2), '.k');
ylabel('DI');
title(sprintf('%s psychometric function all tone freqs all difficulties', options.animal));

% all freqs, easy, medium, hard
ax2=subplot(3,2,2);
% errorshade(mean_di_matrix_all_freq_easy(:,1), mean_di_matrix_all_freq_easy(:,2),...
%     mean_di_matrix_all_freq_easy(:,3), [.65 .79 .18], [.83 .93 .57]);
% errorshade(mean_di_matrix_all_freq_medium(:,1), mean_di_matrix_all_freq_medium(:,2),...
%     mean_di_matrix_all_freq_medium(:,3));
% errorshade(mean_di_matrix_all_freq_hard(:,1), mean_di_matrix_all_freq_hard(:,2),...
%     mean_di_matrix_all_freq_hard(:,3), [.99 .8 0], [1 .8 .8]);

plot(mean_di_matrix_all_freq_easy(:,1), mean_di_matrix_all_freq_easy(:,2), '-g', 'DisplayName', 'Easy');
hold on
plot(mean_di_matrix_all_freq_medium(:,1), mean_di_matrix_all_freq_medium(:,2), '-b', 'DisplayName','Medium');
plot(mean_di_matrix_all_freq_hard(:,1), mean_di_matrix_all_freq_hard(:,2), '-r', 'DisplayName', 'Hard');
plot(mean_di_matrix_all_freq_pt(:,1), mean_di_matrix_all_freq_pt(:,2), '-k', 'DisplayName', 'Pure Tone');
ylabel('DI');
legend('show');
title(sprintf('%s psychometric f all freqs all diff', options.animal));

% low frequencies, medium difficulty
ax3=subplot(3,2,3);
errorshade(mean_low_freq_di_matrix(:,1), mean_low_freq_di_matrix(:,2),...
    mean_low_freq_di_matrix(:,3));
hold on
plot(low_freq_matrix(:,1), low_freq_matrix(:,2), '.k');
ylabel('DI');
title(sprintf('%s psychometric f tone freq <= 2000Hz medium diff', options.animal));


ax4=subplot(3,2,4);
errorshade(mean_medium_freq_di_matrix(:,1), mean_medium_freq_di_matrix(:,2), ...
    mean_medium_freq_di_matrix(:,3));
hold on
plot(medium_freq_matrix(:,1), medium_freq_matrix(:,2), '.k');
ylabel('DI');
title(sprintf('%s psychometric f 2000Hz<tone freq<=14000Hz medium diff', options.animal));

ax5=subplot(3,2,5);
errorshade(mean_high_freq_di_matrix(:,1), mean_high_freq_di_matrix(:,2),...
    mean_high_freq_di_matrix(:,3));
hold on
plot(high_freq_matrix(:,1), high_freq_matrix(:,2), '.k');
ylabel('DI');
title(sprintf('%s psychometric f tone freq > 14000Hz medium diff', options.animal));
 
axis([ax1 ax2 ax3], [-30 20 0 105]);

if options.save_fig
    set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
    saveas(gcf, ['/auto/users/daniela/docs/AC_IC_project/eps_files_PTD/comb_beha_ephy_saved_figures/'...
        options.animal '_psychometric.eps']);
    close(gcf);
end


end 