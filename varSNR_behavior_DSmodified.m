% function results=varSNR_behavior(animal)
% SVD and DS to analyze task-difficulty behavioral data - 2017-04-11


function results=varSNR_behavior_DSmodified(animal,include_training,pure_tone_epoch)

narf_set_path

dbopen;

if ~exist('animal','var')
    animal = 'Babybell';
end
if ~exist('include_training','var')
    include_training=0;
end

if ~exist('pure_tone_epoch','var')
    pure_tone_epoch=0;
end

if strcmpi(animal,'Babybell')
    cellprefix='bbl';
elseif strcmpi(animal,'Tartufo')
    cellprefix='TAR';
elseif strcmpi(animal,'Button')
    cellprefix='btn';
elseif strcmpi(animal,'Magic')
    cellprefix='mgc';
elseif strcmpi(animal, 'Beartooth')
    cellprefix='BRT';
elseif strcmpi(animal, 'Leyva')
    cellprefix='ley';
end

if ~include_training
    sTrain=' AND gDataRaw.training=0';
else
    sTrain='';
end

% only look at subset that has pure tone behavior

if pure_tone_epoch
   PT=' AND gDataRaw.id>125259';
else
   PT='';
end

% To add: only pick training sessions with Trial_OveralldB =>55 dB (trained only)
% separated var SNR from other SVD and DS style
sql=['SELECT gDataRaw.*,gCellMaster.penid,gPenetration.pendate FROM gDataRaw',...
    ' INNER JOIN gCellMaster ON gDataRaw.masterid=gCellMaster.id'...
    ' INNER JOIN gPenetration ON gCellMaster.penid=gPenetration.id'...
    ' WHERE runclass="PTD"',...
    ' AND trials>20',...
    PT,...
    sTrain, ...
    ' AND not(resppath like "L:%")',...
    ' AND behavior="active"',...
    ' AND gDataRaw.cellid like "' cellprefix '%"',...
    ' AND not(gDataRaw.bad) ORDER BY gDataRaw.id'];

rawdata=mysql(sql);
filecount=length(rawdata);

% data to collect:
masterid=nan(filecount,1);
training=nan(filecount,1);
probeRT=nan(filecount,1);
block_order=nan(filecount,1);
cumulativehits=nan(filecount,1);
trialcount=nan(filecount,1);
probeSNR=nan(filecount,1);
probe_freq=nan(filecount,1);
n_tar_presented=nan(filecount,1);
n_refs_presented=nan(filecount,1);
n_probe_tar_presented=nan(filecount,1);
tokenHR=nan(filecount,1);
tokenprobeHR=nan(filecount,1);
per_trialFAR=nan(filecount,1);
tokenFAR=nan(filecount,1);
d_prime=nan(filecount,1);
probe_d_prime=nan(filecount,1);
criterion=nan(filecount,1);
probe_criterion=nan(filecount,1);
probeDI=nan(filecount,1);
meanDI=nan(filecount,1);
difficulty=nan(filecount,1);

% % number of params to display in matrix
% num_parms = 12;
% 
% beha_matrix = zeros(filecount, num_parms);


% rawids for sessions with TONEinTORCs SVD style
flipsetrawids=[120446 120448 120451 120475 120477 120525 ...
    120528 120530 120538 120539 120542 120544];

% rawids for sessions with TONEinTORCs DS style
straddlesetrawids=[120110 120111 120163 120165 120184 120185 ...
    120188 120190 120199 120201 120207 120209 120211 120214 ...
    120234 120234 120254 120256 120258 120260 120272 120273 ...
    120274 120275 120293 120283 120285 120286 120289 120290 ...
    120293 120310 120311 120312 120313 120314 120316 120317 ...
    120435 120436 120437];


lastpendate='';
thispencount=0;
thishits=0;

if include_training
    fid = fopen(['/auto/users/daniela/code/python_ACIC_proj/', animal, '_behavior_output.csv'], 'w');
else
    fid = fopen(['/auto/users/daniela/code/python_ACIC_proj/', animal, '_behavior_physiology_only.csv'], 'w');
end

fprintf(fid, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','Animal', ... 
                'Parmfile', 'HR', 'Probe HR', 'FAR', "Number of Probe Targets", "Number of Targets",...
                'Number of References', 'Probe DI (%)', 'DI (%)', 'Probe RT (s)', 'Difficulty');
            
            
       

for ii=1:filecount
    rawid=rawdata(ii).id;
    training(ii)=rawdata(ii).training;
    
    [parms,perf]=dbReadData(rawdata(ii).id);
    badfile=0;
    
    if parms.Trial_OveralldB<55
        badfile=1;
    end
    if ~isfield(perf,'uDiscriminationIndex')
        
        evpfile=[rawdata(ii).resppath rawdata(ii).respfileevp];
        if exist(evpfile,'file')
            parmfile=[rawdata(ii).resppath rawdata(ii).parmfile];
            replicate_behavior_analysis(parmfile,1);
            close;
            [parms,perf]=dbReadData(rawdata(ii).id);
        else
            badfile=1;
        end
    end
    if ~badfile
        Trial_TargetIdxFreq=parms.Trial_TargetIdxFreq;
        tcount=length(Trial_TargetIdxFreq);
        if tcount==5 && isinf(parms.Trial_RelativeTarRefdB(1)) && Trial_TargetIdxFreq(1)==0.8
            difficulty(ii)=0;  %pure-tone common
        elseif tcount==5 && sum(Trial_TargetIdxFreq==0.2)==5
            difficulty(ii)=2; % medium
        elseif tcount==5 && Trial_TargetIdxFreq(1)==0.3
            difficulty(ii)=1;  %easy
        elseif tcount==5 && Trial_TargetIdxFreq(1)==0.1
            difficulty(ii)=3;  %hard
        elseif tcount==1 && strcmpi(animal,'Tartufo')
            difficulty(ii)=0; %tone only
        elseif tcount==1 && strcmpi(animal,'Leyva')
            difficulty(ii)=0; %tone only
            
        elseif ismember(rawid,flipsetrawids)
            if parms.Trial_RelativeTarRefdB(2)<0.5
                difficulty(ii)=1;
            else
                difficulty(ii)=3;
            end
            
        elseif ismember(rawid,straddlesetrawids)
            if parms.Trial_RelativeTarRefdB(1)>parms.Trial_RelativeTarRefdB(2)
                difficulty(ii)=1;
            else
                difficulty(ii)=3;
            end
        end
        
        %uDI=perf.uDiscriminationIndex; % these value come from the default analysis of DI
        uHitRate=perf.uHitRate;
        uReactionTime=perf.uReactionTime;
        
        % assign the probe idx depending on experimental set up
        probeidx=0;
        if length(parms.Trial_RelativeTarRefdB)==5
            probeidx=3; % always test with middle SNR (varSNR behavior)
        elseif ismember(rawid,flipsetrawids)
            probeidx=2; % 2nd target always same SNR
        elseif ismember(rawid,straddlesetrawids)
            probeidx=2; % 2nd target always same SNR
        elseif tcount==1  % don't use pure tone-only as probe
            probeidx=1;
        end
        
        per_trialFAR(ii)=perf.FalseAlarmRate;
        
        masterid(ii)=rawdata(ii).masterid;
        
        % figure out which block this was today
        pendate=rawdata(ii).pendate;
        if ~strcmpi(pendate,lastpendate)
            thispencount=0;
            thishits=0;
        end
        lastpendate=pendate;
        thispencount=thispencount+1;
        block_order(ii)=thispencount;
        cumulativehits(ii)=thishits;
        thishits=thishits+perf.Hit(1);
        trialcount(ii)=rawdata(ii).trials;
        
        if probeidx
            try
                parmfile=[rawdata(ii).resppath rawdata(ii).parmfile];
                %    di_nolick outputs: metrics and metrics_newT are structs containing the metrics
                %    metrics_newT has metrics calculated only from trials immediately
                %    following hits or misses (should be trials in which a new stimulus was
                %    played)
                [metrics,metrics_newT]=di_nolick(parmfile);
                details=metrics.details;
                
                meanDI(ii)=nanmean(details.uDI);

%                 if difficulty(ii) == 0
%                     keyboard
%                 end

                probeDI(ii)=details.uDI(probeidx);
                probeRT(ii)=uReactionTime(probeidx);
                probeSNR(ii)=parms.Trial_RelativeTarRefdB(probeidx);
                probe_freq(ii)=parms.Tar_Frequencies(probeidx);
               
                %%Not sure what's this about?
                %if probeidx
                %
                %   probeHR(ii)=details.uHR(probeidx);
                %  %probeRT(ii)=uReactionTime(probeidx);
                %  %probeSNR(ii)=parms.Trial_RelativeTarRefdB(probeidx);
                %  %probe_freq(ii)=parms.Tar_Frequencies(probeidx);
                %end
                
                per_trialFAR(ii)=perf.FalseAlarmRate;
                %tokenprobeHR(ii)=details.uHR(probeidx); % I think this is
                %the per trial HR in the probe.
                
                
                % number of probe targets occur
                n_probe_tar_presented(ii)=(details.uHit(probeidx)+details.uMiss(probeidx));
                
                % number of all targets occur
                n_tar_presented(ii)=(details.Hits+details.Misses);
                
                % number of distractors
                n_refs_presented(ii)=details.FAs+details.CRs;
                
                % extract HR and per_trialFAR from di_nolick that are calculated
                % according to SDT per token
                tokenprobeHR(ii)=details.uHit(probeidx)/n_probe_tar_presented(ii);
                
                tokenHR(ii)=details.Hits/n_tar_presented(ii);
                
                tokenFAR(ii)=details.FAs/(details.FAs+details.CRs);
                
                avgREFcount=mean(parms.Trial_ReferenceCountFreq.*...
                    0:length(parms.Trial_ReferenceCountFreq)-1);

                
                % method to correct for HR=100 for d' analysis (Macmillian
                % & Kaplan, 1985). According to the paper to solve the
                % issue of having HR=1.0 and FR=0 or vice verse that would
                % make dprime calculation infinite, we can make it so that
                % a HR = 1 would become HR=1-1/(2N), where N is the number of
                % relevant stimuli (in case of HR, it would be targets).
                % If the parameter is 0, e.g., per_trialFAR=0, then per_trialFAR=1/(2N)
                if tokenprobeHR(ii) == 1.0
                    tokenprobeHR(ii)= tokenprobeHR(ii)-(1/(2*n_probe_tar_presented(ii)));
                    %tokenprobeHR(ii)=(details.uHit(probeidx)-1/(n_probe_tar_presented+n_refs_presented))/n_probe_tar_presented;
                end
                
                if tokenHR(ii) == 1.0
                    tokenHR(ii)= tokenHR(ii)-(1/(2*n_tar_presented(ii)));
                    %tokenprobeHR(ii)=(details.uHit(probeidx)-1/(n_probe_tar_presented+n_refs_presented))/n_probe_tar_presented;
                end
                
                if tokenFAR(ii) == 0.0
                    tokenFAR(ii)= tokenFAR(ii)-(1/(2*n_refs_presented(ii)));
                end
                
                % to calculate d' according to SDT, we need the z-score of
                % HR (both for probe and all trials) and FR calculated on a
                % token-base. For some reasons this is not equivalent no
                % NORMINV and leads to unreasonable values of d' and
                % criterion
                p=1/(1+avgREFcount);
                m=n_probe_tar_presented(ii)*p;
                sigma=sqrt(n_probe_tar_presented(ii)*p*(1-p));
                ZtokenprobeHR=(tokenprobeHR(ii)*n_probe_tar_presented(ii) - m) ./sigma;
                
                p=1/(1+avgREFcount);
                m=n_tar_presented(ii)*p;
                sigma=sqrt(n_tar_presented(ii)*p*(1-p));
                ZtokenHR=(tokenHR(ii)*n_tar_presented(ii) - m) ./sigma; 
              
                p=1/(1+avgREFcount);
                m=n_refs_presented(ii)*p;
                sigma=sqrt(n_refs_presented(ii)*p*(1-p));
                ZtokenFAR=(tokenFAR(ii)*n_refs_presented(ii) - m) ./sigma;
                
                % calculate d' according to SDT for the probe trials
                probe_d_prime(ii)=ZtokenprobeHR-ZtokenFAR;
                % calculate d' according to SDT for the all trials
                d_prime(ii)=ZtokenHR-ZtokenFAR;
                

                % calculated criterion according to SDT for the probe
                % trials
                probe_criterion(ii)=-0.5*(ZtokenprobeHR+ZtokenFAR);
                
                criterion(ii)=-0.5*(ZtokenHR+ZtokenFAR);

                % what is catch about?
               catch
                    disp('failed to run di_nolick');
%                     tokenprobeHR(ii)=probeHR(ii)./100;
%                     tokenFAR(ii)=per_trialFAR(ii)./100;
            end

           
            fprintf(fid, '"%s","%s",%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d\n',animal,... 
                rawdata(ii).parmfile, tokenHR(ii), tokenprobeHR(ii), tokenFAR(ii),n_probe_tar_presented(ii),...
                n_tar_presented(ii), n_refs_presented(ii), probeDI(ii), meanDI(ii), probeRT(ii), difficulty(ii));

            fprintf('"%s","%s",%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d\n',animal,... 
                rawdata(ii).parmfile, tokenHR(ii), tokenprobeHR(ii), tokenFAR(ii),n_probe_tar_presented(ii),...
                n_tar_presented(ii), n_refs_presented(ii), probeDI(ii), meanDI(ii), probeRT(ii), difficulty(ii));

        end
    end
end

goodblocks=find(~isnan(difficulty) & ~isnan(probeDI));

results=struct;
results.rawid=cat(1,rawdata.id);
results.masterid=masterid;
results.difficulty=difficulty;
results.probeDI=probeDI;
results.meanDI=meanDI;
results.tokenHR=tokenHR;
results.tokenprobeHR=tokenprobeHR;
results.probeRT=probeRT;
results.per_trialFAR=per_trialFAR;
results.tokenFAR=tokenFAR;
results.probeSNR=probeSNR;
results.probe_freq=probe_freq;
results.block_order=block_order;
results.cumulativehits=cumulativehits;
results.trialcount=trialcount;
results.d_prime=d_prime;
results.probe_d_prime=probe_d_prime;


ff=fields(results);
for ii=1:length(ff)
    results.(ff{ii})=results.(ff{ii})(goodblocks);
end


unique_masterid=unique(masterid(:)'); % use all data
%unique_masterid=unique(masterid(~training)'); % only use ephys data

%calculate differences within-session
diffDI=zeros(size(unique_masterid));
diffper_trialFAR=zeros(size(unique_masterid));
for jj=1:length(unique_masterid)
    diffDI(jj)=mean(probeDI(masterid==unique_masterid(jj) & difficulty==3)) -...
        mean(probeDI(masterid==unique_masterid(jj) & difficulty==1));
    diffper_trialFAR(jj)=mean(per_trialFAR(masterid==unique_masterid(jj) & difficulty==3)) -...
        mean(per_trialFAR(masterid==unique_masterid(jj) & difficulty==1));
end

col=[.8 .8 .8];
figure;
diff=0:3;

subplot(4,3,1);
mean_di=zeros(3,1);
se_di=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_di(i)=nanmean(probeDI(didx));
    se_di(i)=nanstd(probeDI(didx))./sqrt(length(didx));
end

for jj=1:length(unique_masterid)
    hold on
    ii=find(masterid==unique_masterid(jj) & difficulty~=2 & meanDI>0.5);
    plot(difficulty(ii),probeDI(ii),'-o','Color', col);
end

nn=find(~isnan(mean_di));
errorbar(diff(nn), mean_di(nn), se_di(nn),'r');
hold off
xlabel('difficulty');
ylabel('probe DI');


subplot(4,3,2);

Y=meanDI;
Ylabel='mean DI';

mean_x=zeros(3,1);
se_x=zeros(3,1);

for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn),mean_x(nn),se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(4,3,3);

Y=probe_d_prime;
Ylabel="probe d'";

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(4,3,4);

Y=d_prime;
Ylabel="d'";

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(4,3,5);

Y=tokenprobeHR;
Ylabel='token probe HR';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(4,3,6);

Y=tokenHR;
Ylabel='token HR';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'.', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);


subplot(4,3,7);

Y=criterion;
Ylabel='criterion';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'.', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

% mean_x=zeros(5,1);
% se_x=zeros(5,1);
% for bl=1:5
%     didx=find(block_order==bl);
%     
%     mean_x(bl)=nanmean(probeDI(didx));
%     se_x(bl)=nanstd(probeDI(didx))./sqrt(length(didx));
% end
% plot(block_order,probeDI,'.', 'Color', col);
% hold on
% errorbar(mean_x,se_x,'r');
% hold off
% xlabel('block order');
% ylabel('probe DI');

subplot(4,3,8);

Y=probe_criterion;
Ylabel='probe criterion';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'.', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);


% Ylabel='per trial FAR';
% 
% mean_x=zeros(5,1);
% se_x=zeros(5,1);
% for bl=1:5
%     didx=find(block_order==bl);
%     
%     mean_x(bl)=mean(per_trialFAR(didx));
%     se_x(bl)=std(per_trialFAR(didx))./sqrt(length(didx));
% end
% plot(block_order,per_trialFAR,'-o', 'Color', col);
% hold on
% errorbar(mean_x(nn), se_x(nn),'r');
% hold off
% xlabel('block order');
% ylabel(Ylabel);

subplot(4,3,9);

Y=tokenFAR;
Ylabel='token FAR';

mean_x=zeros(3,1);
se_x=zeros(3,1);

for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(4,3,10);

Y=probeDI;
Ylabel='probe DI';

X=meanDI;
Xlabel='mean DI';

plot(X,Y,'o', 'Color', col);

xlabel(Xlabel);
ylabel(Ylabel);

subplot(4,3,11);

Y=d_prime;
Ylabel="d'";

X=probe_d_prime;
Xlabel="prome d'";

plot(X,Y,'o', 'Color', col);

xlabel(Xlabel);
ylabel(Ylabel);

subplot(4,3,12);

Y=probeDI;
Ylabel='probe DI';

X=probe_d_prime;
Xlabel="prome d'";

%ax = axis;

% xmin = ax(1);
% xmax = ax(1);
% 
% ymin = ax(3);
% ymax = ax(4);

plot(X,Y,'o', 'Color', col);

xlabel(Xlabel);
ylabel(Ylabel);





% subplot(3,3,9);
% 
% mean_x=zeros(5,1);
% se_x=zeros(5,1);
% for bl=1:5
%     didx=find(block_order==bl);
%     
%     mean_x(bl)=mean(probeRT(didx));
%     se_x(bl)=std(probeRT(didx))./sqrt(length(didx));
% end
% 
% plot(block_order,probeRT,'.');
% hold on
% errorbar(mean_x(nn),se_x(nn),'r');
% hold off 
% xlabel('block order');
% ylabel('probe RT');







