clear;
if 0 
    mfiles={'/auto/data/daq/Babybell/bbl064/bbl064g02_p_PTD.m','/auto/data/daq/Babybell/bbl064/bbl064g03_a_PTD.m'};
    electrodes=1:4;
    fh=figure;ah=subplot1(2,2);
else
    mfiles={'/auto/data/daq/Tartufo/TAR010/TAR010c10_p_PTD.m','/auto/data/daq/Tartufo/TAR010/TAR010c09_a_PTD.m'};
    electrodes=1:64;
    options.gapfactor=[.02 -.5];
    [ah,dc,fh]=probe_64D_axes(electrodes,[],options);
end


options.rasterfs=20;
options.colors=[0 0 0; 1 0 0];
options.datause='Collapse reference';

for i=1:length(electrodes)
    options.channel=electrodes(i);
    options.unit=1;
    options.usesorted=0;
    [ph(i,:,:),rate(i,:,:)]=plot_psth_comparison(mfiles,ah(i),options);
    mr(i)=max(max(rate(i,:,:)));
    set(ah(i),'YLim',[0 mr(i)*1.2])
end

%single units
siteid='TAR010';
runclass='PTD';
[cfd]=dbgetscellfile('cellid',siteid,'runclass',runclass,'isolation','>84');

cellids=unique({cfd.cellid});
chan=nan(size(cellids));
for ii=1:length(cellids)
    ind=find(strcmp({cfd.cellid},cellids{ii}));
    indz(ii)=ind(1);
    chan(ii)=cfd(ind(1)).channum;
    ov=find(chan(ii)==chan(1:ii-1));
    if(~isempty(ov))
        fprintf(['\nCell', cellids{ii},' is the same channel as ',cellids{ov}, '(',num2str(chan(ii)),'). Changing to ',num2str(chan(ii)+1)])
        chan(ii)=chan(ii)+1;
    end
end
%iso values: [cfd(indz).isolation]

[ah,dc,fh]=probe_64D_axes(electrodes,[],options);
mr=nan(size(cellids));
clear ph rate
for i=1:length(cellids)
    ind=find(strcmp({cfd.cellid},cellids{i}));
    options.channel=cfd(ind(1)).channum;
    options.unit=cfd(ind(1)).unit;
    options.usesorted=1;
    [ph(i,:,:),rate(i,:,:)]=plot_psth_comparison(mfiles,ah(chan(i)),options);
    mr(i)=max(max(rate(i,:,:)));
    set(ah(chan(i)),'YLim',[0 mr(i)*1.2])   
    th(i)=text(0,mr(i)*1.2,sprintf('%d-%d',options.channel,options.unit),'VerticalAlignment','top','FontSize',8);
end