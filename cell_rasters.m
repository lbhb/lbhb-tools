% function cell_rasters(cellid,runclass,options)
%
% options: .psthfs[=15]
%          .unit[=1]
%          .channel[=1]
%          .active[=1]  (0 means active and passive)
%          .datause[='Collapse both']
%          .usesorted[=1] (0 means raw threhold crossing)
%          .h[=figure] (default new figure)
%          .lick[=1] (1 means plot lick below psth)
%          .raster[=1] (0 means don't plot raster)
%          .rawid[=[]] (list of specific rawfile ids to include)
%
% created svd for SFN 2007

function rawid=cell_rasters(cellid,runclass,options)

if ~exist('options','var'),
   options=[];
end
if ~isfield(options,'psthfs')
   options.psthfs=15;
end
if ~isfield(options,'unit')
   options.unit=1;
end
if ~isfield(options,'channel')
   options.channel=1;
end
if ~isfield(options,'active')
   options.active=1;
end
if ~isfield(options,'maxplots')
   options.maxplots=0;
end
if ~isfield(options,'axes')
   options.axes=[];
end
options.rasterfs=options.psthfs;
options.sigthreshold=getparm(options,'sigthreshold',4);
if ~isfield(options,'datause')
   options.datause='Collapse both';
end
if ~isfield(options,'usesorted')
   options.usesorted=1;
end
options.psth=1;
options.rasterfs=1000;
%options.lfp=2;
if ~isfield(options,'h')
   options.h=figure;
end
if ~isfield(options,'lick')
   options.lick=1;
end 
if ~isfield(options,'PreStimSilence'),
   options.PreStimSilence=0.4;
end 
if ~isfield(options,'PostStimSilence'),
   options.PostStimSilence=0.8;
end
options.rawid=getparm(options,'rawid',[]);
options.raster=getparm(options,'raster',1);
options.mergeset=getparm(options,'mergeset',{});
options.runclass=runclass;

dbopen;
if ~isempty(options.rawid),
   if length(options.rawid)>1,
      rawstr=mat2str(options.rawid);
      rawstr=rawstr(2:(end-1));
   else
      rawstr=num2str(options.rawid);
   end
   rawstr=strrep(rawstr,' ',',');
   sitestr=['gDataRaw.id in (',rawstr,')']
   active=0;
else
   sitestr=['runclass in ("',strrep(runclass,',','","'),'")'];
end
siteid=strsep(cellid,'-');
if length(siteid)>2,
   options.channel=siteid{2};
   options.unit=siteid{3};
elseif length(siteid)>1
   options.channel=siteid{2}(1)-'a'+1;
   options.unit=str2num(siteid{2}(2));
end
siteid=siteid{1};

if ~options.usesorted,
   sql=['SELECT gDataRaw.* FROM gDataRaw',...
        ' WHERE gDataRaw.cellid="',siteid,'"',...
        ' AND ',sitestr,...
        ' AND not(bad)',...
        ' ORDER BY parmfile']
elseif options.active,
   sql=['SELECT gDataRaw.*,sCellFile.goodtrials FROM gDataRaw,sCellFile',...
        ' WHERE gDataRaw.id=sCellFile.rawid',...
        ' AND sCellFile.cellid="',cellid,'"',...
        ' AND ',sitestr,...
        ' AND not(bad)',...
        ' AND behavior="active"',...
        ' ORDER BY parmfile']
else
   sql=['SELECT gDataRaw.*,sCellFile.goodtrials FROM gDataRaw,sCellFile',...
        ' WHERE gDataRaw.id=sCellFile.rawid',...
        ' AND sCellFile.cellid="',cellid,'"',...
        ' AND ',sitestr,...
        ' AND not(bad)',...
        ' ORDER BY parmfile']
end
rawdata=mysql(sql);
if length(rawdata)>=1,
   siteid=rawdata(1).cellid;
end

if options.maxplots>0 && length(rawdata)>options.maxplots,
   rawdata=rawdata(1:options.maxplots);
end

r={};
tags={};
mresp=0;
for fidx=1:length(rawdata),
   if ~isempty(rawdata(fidx).goodtrials),
       options.trialrange=eval(rawdata(fidx).goodtrials);
   elseif isfield(options,'trialrange'),
       options=rmfield(options,'trialrange');
   end
   if any(strcmp(rawdata(fidx).parmfile,options.mergeset)),
       for tt=1:length(options.mergeset),
           mfile=[rawdata(fidx).resppath options.mergeset{tt}]
           [tr,tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
           if tt==1,
               r{fidx}=tr;
           else
               r{fidx}=cat(2,r{fidx},tr);
           end
       end
   else
       mfile=[rawdata(fidx).resppath rawdata(fidx).parmfile];
       [r{fidx},tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
   end
   
   smcount=round(options.rasterfs./options.psthfs);
   smfilt=ones(smcount,1)./smcount.*1000;
   mr=rconv2(squeeze(nanmean(r{fidx},2)),smfilt);
   mr=max(mr(:));
   mresp=max(mresp,mr);
end
if isfield(options,'trialrange'),
   options=rmfield(options,'trialrange');
end

if ~isfield(options,'lickmax'),
   options.lickmax=0.55;
end
if ~isfield(options,'psthmax'),
   options.psthmax=mresp.*0.95;
end
if options.raster,
   rowcount=4;
else
   rowcount=5;
end
colcount=max(3,ceil(length(rawdata)./rowcount));
for fidx=1:length(rawdata),
   
   mfile=[rawdata(fidx).resppath rawdata(fidx).parmfile];
   sfigure(options.h);
   if ~isempty(options.axes),
      hs=options.axes(fidx);
      axes(hs);
   else
      hs=subplot(rowcount,colcount,fidx);
   end
   
   if strcmpi(rawdata(fidx).runclass,'BNB'),
       toptions=options;
       toptions.datause='Reference Only';
       chord_strf_online(mfile,options.channel,options.unit,hs,toptions);
   elseif strcmpi(rawdata(fidx).runclass,'TOR') && ~options.usesorted,
       toptions=options;
       toptions.datause='Reference Only';
       % standard TORC strf
       toptions.usefirstcycle=0;
       toptions.tfrac = 1;
       strf_online(mfile,options.channel,hs,toptions);
       %strf_offline2(mfilename,spikefile,Electrode,options.sortedunit);
   else
       raster_plot(mfile,r{fidx},tags{fidx},hs,options);
   end
   
   [pp,bb]=fileparts(mfile);
   title(sprintf('%s cell %d-%d',bb,options.channel,options.unit));
end
fullpage portrait

rawid=cat(1,rawdata.id);