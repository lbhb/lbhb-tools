%function p = pupil_pop_state(rawid, runclass, fs)
%function p = pupil_pop_state(rawid, runclass, fs)
%
% rawid=116169; %zee015h NAT
% rawid=[118698 118702]; %BOL005c VOC
% rawid=118698; %BOL005c VOC
% rawid=118703; %BOL005c VOC
% rawid=[118758]; %BOL006b VOC
% rawid=116168; %zee015h  VOC
%  pupil_pop_state(rawid,'',20);
%
% created svd 2016-12-21 (happy winter solstice!)

if ~exist('rawid','var'),
   rawid=118698;
   %rawid=118702;
end

miniso='>84';

parmfile={};
spikefile={};
for ii=1:length(rawid),
   %cfd = dbgetscellfile('rawid',rawid,'isolation',miniso,'runclass',runclass);
   if exist('runclass','var') && ~isempty(runclass),
      cfd = dbgetscellfile('rawid',rawid(ii),'isolation',miniso,'runclass',runclass);
   else
      cfd = dbgetscellfile('rawid',rawid(ii),'isolation',miniso);
      runclass=[];
   end
   parmfile{ii} = [cfd(1).stimpath cfd(1).stimfile];
   spikefile{ii} = [cfd(1).path cfd(1).respfile];
   if ii==1,
      [cellids,ia]=unique({cfd.cellid});
      channels=cat(1,cfd(ia).channum);
      units=cat(1,cfd(ia).unit);
   else
      [cellids,ia]=intersect(cellids,{cfd.cellid});
      units=units(ia);
      channels=channels(ia);
   end
end

LoadMFile(parmfile{1})
prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
duration = exptparams.TrialObject.ReferenceHandle.Duration;

options = struct();
options.avg_stim=getparm(options,'avg_stim',0);
options.rasterfs = fs;
if isempty(runclass) && ~isempty(strfind(parmfile{1},'VOC_VOC')),
   options.tag_masks={'Reference1'};
elseif isempty(runclass),
   options.tag_masks={'Reference'};
else
   options.runclass=runclass;
end

%load spike raster
r_options = options;
r_options.includeprestim = 1;
%r_options.channel=cfd(1).channum;
%r_options.unit=cfd(1).unit;
r_options.channel=channels;
r_options.unit=units;
r_options.meansub=0;


pr_options = options;
pr_options.pupil = 1;
pr_options.pupil_median=0.5;
pr_options.pupil_offset=0.9;

for ii=1:length(rawid)
   [tr,tags,ttrialset] = loadsiteraster(spikefile{ii}, r_options);
   tr=tr*r_options.rasterfs;

   %load pupil diameter raster
   [tpr0,ptags,tptrialset] = loadevpraster(parmfile{ii}, pr_options);
   
   % is this necessary?
   tpr=tpr0.*nan;
   trialcount=max(ttrialset(:));
   
   for trialidx=1:trialcount,
      pb=find(tptrialset==trialidx);
      rb=find(ttrialset==trialidx);  % (rep,stimidx)
      tpr(:,rb)=tpr0(:,pb);
   end
   
   pr_options0=pr_options;
   pr_options0.pupil_median=0.1;
   pupil_raw= loadevpraster(parmfile{ii}, pr_options0);
   
   if ii==1,
      r=tr;
      pr=tpr;
      trialset=ttrialset;
   else
      r=cat(2,r,tr);
      pr=cat(2,pr,tpr);
      trialset=cat(1,trialset,ttrialset+nanmax(trialset(:)));
   end
   
end


repcount=size(r,2);
cellcount=size(r,4);
stimcount=size(r,3);

if 0,
   % use evoked period
   r0=repmat(nanmean(r,2),[1 repcount]);
   r=r-r0;
   prebins=prestim.*fs;
   allbins=size(r,1);
   bincount=floor(allbins./fs);
   bins=bincount.*fs;  % 1-sec bins
   aa=(prebins+1):bins;
   bincount=floor(length(aa)./fs);
   bins=length(aa);
else
   % spont period
   bins=prestim.*fs;
   bincount=1;
   aa=1:bins;
end

if bincount==1,
   rspont=nanmean(permute(r(aa,:,:,:),[1 3 2 4]));
   pspont=nanmean(permute(pr(aa,:,:,:),[1 3 2 4]));
else
   tr=permute(r(aa,:,:,:),[1 3 2 4]);
   rspont=nanmean(reshape(tr,[bins/bincount bincount*stimcount repcount cellcount]));
   tp=permute(pr(aa,:,:,:),[1 3 2 4]);
   pspont=nanmean(reshape(tp,[bins/bincount bincount*stimcount repcount]));
end

X=reshape(rspont,[repcount*stimcount*bincount,cellcount]);
Y=pspont(:);

ts=repmat(trialset',[bincount 1]);
ts=ts(:);
[~,si]=sort(ts);
X=X(si,:);
Y=Y(si);

keepidx=find(~isnan(X(:,1)));
X=X(keepidx,:);
Y=Y(keepidx);

for ii=1:cellcount,
   X(:,ii)=X(:,ii)-mean(X(:,ii));
   X(:,ii)=X(:,ii)./std(X(:,ii));
end

[U,S,V]=svd(X);
single_cc=zeros(cellcount,1);

pcmag=diag(S)./sum(diag(S));

for ii=1:cellcount,
   if xcov(Y,U(:,ii),0,'coeff')<0,
      U(:,ii)=-U(:,ii);
      V(:,ii)=-V(:,ii);
   end
   
   single_cc(ii)=xcov(X(:,ii),Y,0,'coeff');
end

%x=[X ones(size(Y))];
[b,Y0]=nested_regress(Y,X);
%Y0=x*b;

figure
xc_set=zeros(cellcount+1,1);
xc_step=zeros(cellcount+1,1);
for ii=1:cellcount+1,
   if ii==cellcount+1,
      %x=[X ones(size(Y))];
      %b=regress(Y,x);
      [b,Y1,berr]=nested_regress(Y,X);
      
   else
      
      u=U(:,1:ii);
      [b,Y1,berr]=nested_regress(Y,u);
      
      
   end
   xc_set(ii)=xcov(Y,Y1,0,'coeff');
   if ii==1,
      xc_step(ii)=xc_set(ii);
   else
      xc_step(ii)=xc_set(ii)-xc_set(ii-1);
   end
   
   
   if ii==cellcount+1,
      subplot(3,3,8);
      hl=plot([Y Y1]);
      set(hl(1),'Color',[0 0 0]);
      axis(aa);
      
      title(sprintf('all PCs xc=%.3f',xcov(Y,Y1,0,'coeff')));
      
      subplot(3,3,9);
      plot([xc_set(1:(end-1)) xc_step(1:(end-1)) pcmag],'o-');
      legend('total cc','step cc','pc mag');
      legend boxoff
   elseif ii>=8,
      % do nothing
   else
      subplot(3,3,ii);
      hl=plot([Y Y1 U(:,ii)./std(U(:,ii)).*std(Y)]);
      set(hl(1),'Color',[0 0 0]);
      set(hl(3),'Color',[.8 .8 .80]);
      aa=axis;
      aa=[0 length(Y)+1 aa(3:4)];
      axis(aa);
      
      title(sprintf('ii=%d xc=%.3f',ii,xcov(Y,Y1,0,'coeff')));
   end
end

figure;
subplot(2,2,1);
plot(single_cc);
for jj=1:cellcount,
   if single_cc(jj)>0,
      ht=text(jj,single_cc(jj)+0.02,cellids{jj}(9:end),'FontSize',6,'Rotation',90);
   else
      ht=text(jj,single_cc(jj)-0.02,cellids{jj}(9:end),...
         'FontSize',6,'Rotation',90,'HorizontalAlignment','right');
   end
end
aa=axis;
axis([0 cellcount+1 aa(3:4)+[-0.1 0.1]]);
title(sprintf('%s (%d)',basename(parmfile{1}),rawid),'Interpreter','none');
xlabel('unit');
ylabel('spike-pupil cc');

subplot(2,2,2);
M=mean(X,2);
plot(Y,M,'.');
title(sprintf('ii=%d xc=%.3f',ii,xcov(Y,M,0,'coeff')));
xlabel('pupil (pix)');
ylabel('mean (normed) spike rate across channels');

subplot(2,2,3);
plot(diag(S));

subplot(2,2,4);
hist(pspont(:));
xlabel('pupil (pix)');
ylabel('trials');

figure;
for ii=1:6,
   subplot(4,3,ii),
   plot(Y,U(:,ii),'.');
   %[b,Y0]=nested_regress(Y,U(:,ii));
   %plot(Y0,Y,'.');
   
   title(sprintf('ii=%d xc=%.3f',ii,xcov(Y,U(:,ii),0,'coeff')));
   
   subplot(4,3,ii+6),
   plot(V(:,ii));
   for jj=1:cellcount,
      if V(jj,ii)>0,
         ht=text(jj,V(jj,ii)+0.05,cellids{jj}(9:end),'FontSize',6,'Rotation',90);
      else
         ht=text(jj,V(jj,ii)-0.05,cellids{jj}(9:end),...
            'FontSize',6,'Rotation',90,'HorizontalAlignment','right');
      end
   end
   aa=axis;
   axis([0 cellcount+1 aa(3:4)]);

end

return

figure;
for ii=1:6,
   subplot(2,3,ii);
   y=Y-mean(Y);
   y=y./std(y).*std(U(:,ii));
   hl=plot([U(:,ii) y [0;diff(y)]]);
   set(hl(1),'Color',[0 0 0],'LineWidth',2);
   title(sprintf('ii=%d xc=%.3f xcd=%.3f',ii,xcov(Y,U(:,ii),0,'coeff'),xcov([0;diff(Y)],U(:,ii),0,'coeff')));
end




%pr(find(pr==0)) = nan; %remove zeros (blinks, measurement failures)
%pr(find(pr<min(pupilbins))) = nan; %set threshold on pupil size
%pr(find(pr>max(pupilbins))) = nan;

%classify each trial by mean pupil diameter in silence
pravg = nanmean(pr(1:round(prestim*fs),:,:),1);
pravg1 = nanmean(pr(1:round(prestim/2*fs),:,:),1);
pravg2 = nanmean(pr(round(prestim/2*fs+1):round(prestim*fs),:,:),1);
pg=zeros([size(squeeze(pravg)) length(pupilbins)]);
for b = 1:length(pupilbins),
   pg(:,:,b) = squeeze(pravg > pupilbins(b));
end
pg = sum(pg,3);

%calculate psths
p=zeros(size(r,1),size(r,3),length(pupilbins),size(r,4));
for b=1:length(pupilbins)
   for stim=1:size(r,3)
     %find the stimulus repetitions in each pupil bin
     pgt = (pg(:,stim)==b); %pupil group trials
     %average over those repetitions
     p(:,stim,b,:) = nanmean(r(:,pgt,stim,:),2);
   end
end
if options.avg_stim,
    p = squeeze(nanmean(p,2)); %average over stimuli
end

% cross-corr spont activity
prebins=1:round(fs*prestim);
corrbins=round(fs*0.2);
prestimr=r(prebins,:,:,:);
prestimr=reshape(prestimr,corrbins,size(prestimr,1)/corrbins,size(prestimr,2)*size(prestimr,3),size(prestimr,4));
prestimr=squeeze(mean(prestimr,1));

b=[pravg(:)];
[~,bi]=sort(b);
bincount=6;
cellcount=size(r,4);
bstep=size(bi,1)/bincount;
bcenter=zeros(bincount,1);
bmean=zeros(bincount,cellcount);
pairs=nchoosek(1:cellcount,2);
bxc=zeros(bincount,size(pairs,1));

for bb=1:bincount
   ff=round((bb-1)*bstep+1):round(bb*bstep);
   tr=prestimr(:,bi(ff),:);
   tr=reshape(tr,size(tr,1)*size(tr,2),cellcount);
   
   bcenter(bb)=mean(b(bi(ff),1));
   bmean(bb,:)=mean(tr,1);
   
   for pp=1:size(pairs,1),
      bxc(bb,pp)=xcov(tr(:,pairs(pp,1)),tr(:,pairs(pp,2)),0,'coeff');
   end
end

figure;
subplot(2,1,1);
plot(bcenter,bmean);
subplot(2,1,2);
plot(bcenter,bxc);
legend(num2str(pairs));

return
%prestimpupil=prestimpupil-repmat(mean(prestimpupil,1),size(prestimpupil,1),1);

prxc=zeros(41,trialcount);
for ii=1:trialcount,
   prxc(:,ii)=xcov(prestimr(:,ii),prestimpupil(:,ii),20,'unbiased');
end
meanxc=zeros(size(prxc,1),length(pupilbins));
for b=1:length(pupilbins)
   meanxc(:,b)=mean(prxc(:,find(pg==b)),2);
end

figure;
subplot(2,1,1);
plot(meanxc);

subplot(2,1,2);
plot(prestimpupil(:)+randn(size(prestimpupil(:))),prestimr(:)+randn(size(prestimpupil(:))),'.');

disp('done');


b0=squeeze(nanmean(r(1:round(fs*prestim),:,:)));



if showplot,
   figure;
   subplot(2,2,1);
   hist(pravg(~isnan(pravg)));
   
   subplot(2,2,2);
   plot(squeeze(p(:,1,:)));
   ps=cell(length(pupilbins),1);
   for ll=1:length(pupilbins),
      n=squeeze(nansum(nansum(pg==ll)));
      ps{ll}=sprintf('%d (%d trials)',pupilbins(ll),n);
   end
   legend(ps);
   title([cellid ' - ' runclass]);
   
   b0=squeeze(nanmean(r(1:round(fs*prestim),:,:)));

   pravg1 = nanmean(pr(1:round(prestim/2*fs),:,:),1);
   pravg2 = nanmean(pr(round(prestim/2*fs+1):round(prestim*fs),:,:),1);
   
   b1=squeeze(nanmean(r(1:round(prestim/2*fs),:,:)));
   b2=squeeze(nanmean(r(round(prestim/2*fs+1):round(prestim*fs),:,:)));
   
   e0=squeeze(nanmean(r(round(fs*prestim+1):round(fs*(prestim+duration)),:,:)))-b0;
   pravg=squeeze(pravg);
   
   stimcount=size(pravg,2);
   bcenter=zeros(bincount,1);
   bmean=zeros(bincount,1);
   ecenter=zeros(bincount,stimcount);
   emean=zeros(bincount,stimcount);
   
   b=[pravg(:) b0(:)];
   %b=[pravg1(:) b1(:); pravg2(:) b2(:)];
   b=b(~isnan(b(:,1)),:);
   bs=sortrows(b);
   bstep=size(bs,1)/bincount;
   for bb=1:bincount
      ff=round((bb-1)*bstep+1):round(bb*bstep);
      bcenter(bb)=mean(bs(ff,1));
      bmean(bb)=mean(bs(ff,2));
   end
   
   for s=1:stimcount,
      e=[pravg(:,s) e0(:,s)];
      e=e(find(~isnan(e(:,1))),:);
      es=sortrows(e);
      estep=size(es,1)/bincount;
      for bb=1:bincount
         ff=round((bb-1)*estep+1):round(bb*estep);
         ecenter(bb,s)=mean(es(ff,1));
         emean(bb,s)=mean(es(ff,2));
      end
   end
   
   %pravg=squeeze(pravg);
   dd=[pravg(:) b0(:)];
   %dd=[pravg1(:) b1(:);
   %   pravg2(:) b2(:)];
   dd=dd(find(~isnan(dd(:,1))),:);
   subplot(2,2,3);
   plot(dd(:,1),dd(:,2),'.','Color',[0.3 0.3 1]);
   hold on
   plot(bcenter,bmean);
   hold off
   
   xlabel('diameter');
   ylabel('spont');
   
   dotcolors=[0.3 0.3 1; 1 0.3 0.3];
   linecolors=[0 0 1; 1 0 0];
   subplot(2,2,4);
   for s=1:stimcount,
      dd=[pravg(:,s) e0(:,s)];
      dd=dd(~isnan(dd(:,1)),:);
      plot(dd(:,1),dd(:,2),'.','Color',dotcolors(s,:));
      hold on
      plot(ecenter(:,s),emean(:,s),'Color',linecolors(s,:));
   end
   hold off
   xlabel('diameter');
   ylabel('evoked activity');
end

prebins=1:round(fs*prestim);
%prebins=1:round(fs*(prestim+1));
prestimr=r(prebins,:);
prestimpupil=pupil_raw(prebins,:);
%prestimpupil=prestimpupil-repmat(mean(prestimpupil,1),size(prestimpupil,1),1);

prxc=zeros(41,trialcount);
for ii=1:trialcount,
   prxc(:,ii)=xcov(prestimr(:,ii),prestimpupil(:,ii),20,'unbiased');
end
meanxc=zeros(size(prxc,1),length(pupilbins));
for b=1:length(pupilbins)
   meanxc(:,b)=mean(prxc(:,find(pg==b)),2);
end

figure;
subplot(2,1,1);
plot(meanxc);

subplot(2,1,2);
plot(prestimpupil(:)+randn(size(prestimpupil(:))),prestimr(:)+randn(size(prestimpupil(:))),'.');

disp('done');


