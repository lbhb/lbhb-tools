close all

spontonly=0;

cellid='eno027d-c1';
%cellid='eno028e-c1';
%cellid='eno029d-c1';
%cellid='eno052d-a1';
options=struct('fs',10);
[r,pr]=pupil_load(cellid,options);


prestimsilence=2;
duration=3;
prestimbins=prestimsilence.*options.fs;
m0=nanmean(nanmean(nanmean(r(1:prestimbins,:,:))));

if spontonly,
   uu=1:prestimbins;
   y=r(uu,:)-m0;
   x=pr(uu,:);
   y=y(:);
   x=x(:);
   p_plot=x;
else
   uu=1:size(r,1);
   y=r(uu,:);
   r0=repmat(nanmean(r(uu,:,:)-m0,2),[1 size(r,2) 1]);
   tpr=pr(uu,:);
   x=[tpr(:) r0(:)];
   y=y(:);
   p_plot=tpr(:);
end

gidx=find(~isnan(y));
x=x(gidx,:);
y=y(gidx);

% DC only model
% x it Time (all trials concatenated) X 1 vector of pupil diam.
%
% DC/GAIN model
% x is Time (all trials concatenated) X 2 matrix
%  first column is pupil, second column is r0
%
% y is Time X 1 vector of spike rate
%
% beta contains one row of hinge function parameters for each 
%  column of x 

% initial conditions are based on linear fit to pupil vs spike rate
p1=polyfit(x(:,1),y,1);
if spontonly,
   beta0=[p(1) p(1) mean(x) p(2)];
else
   p2=polyfit(x(:,1).*x(:,2),y,1);
   beta0=[p1(1) p1(1) mean(x(:,1)) p1(2);
      p2(1) p2(1) mean(x(:,1)) p2(2)];
end

opts = optimset('fminsearch');
%opts.MaxFunEvals = Inf;
%opts.MaxIter = 10000;

mynegloglik = @(beta) ll_poisson(local_lin(beta,x),y);
betaHatML = fminsearch(mynegloglik,beta0,opts);
betaHatMSE =lsqcurvefit(@local_lin,beta0,x,y);

mononegloglik = @(beta) ll_poisson(local_lin_mono(beta,x),y);
betaMonoML = fminsearch(mononegloglik,beta0,opts);
betaMonoMSE =lsqcurvefit(@local_lin_mono,beta0,x,y);


iisubset=unique(round(linspace(1,length(p_plot),200)));
xplot=linspace(min(p_plot),max(p_plot),100)';

figure;
subplot(2,2,1);
plot(p_plot(iisubset),y(iisubset),'k.');
hold on
plot(xplot,local_lin(beta0,xplot),'g--');
plot(xplot,local_lin(betaHatML(1,:),xplot),'b');
if ~spontonly, plot(xplot,local_lin(betaHatML(2,:),xplot),'r'); end
hold off
title([cellid ' - Non-mono LL']);

subplot(2,2,2);
plot(p_plot(iisubset),y(iisubset),'k.');
hold on
plot(xplot,local_lin(beta0,xplot),'g--');
plot(xplot,local_lin(betaHatMSE(1,:),xplot),'b');
if ~spontonly, plot(xplot,local_lin(betaHatMSE(2,:),xplot),'r'); end
hold off
title('Non-mono MSE');

subplot(2,2,3);
plot(p_plot(iisubset),y(iisubset),'k.');
hold on
plot(xplot,local_lin(beta0,xplot),'g--');
plot(xplot,local_lin(betaMonoML(1,:),xplot),'b');
if ~spontonly, plot(xplot,local_lin(betaMonoML(2,:),xplot),'r'); end
hold off
title('Mono LL');

subplot(2,2,4);
plot(p_plot(iisubset),y(iisubset),'k.');
hold on
plot(xplot,local_lin(beta0,xplot),'g--');
plot(xplot,local_lin(betaMonoMSE(1,:),xplot),'b');
if ~spontonly, plot(xplot,local_lin(betaMonoMSE(2,:),xplot),'r'); end
hold off
title('Mono MSE');

figure;
subplot(2,2,1);
yhat=local_lin(betaHatML,x);
plot(yhat,y,'b.');
title(sprintf('Non-mono LL r=%.3f',xcov(y,yhat,0,'coeff')));

subplot(2,2,2);
yhat=local_lin(betaHatMSE,x);
plot(yhat,y,'b.');
title(sprintf('Non-mono MSE r=%.3f',xcov(y,yhat,0,'coeff')));

subplot(2,2,3);
yhat=local_lin(betaMonoML,x);
plot(yhat,y,'b.');
title(sprintf('Mono LL r=%.3f',xcov(y,yhat,0,'coeff')));

subplot(2,2,4);
yhat=local_lin(betaMonoMSE,x);
plot(yhat,y,'b.');
title(sprintf('Mono MSE r=%.3f',xcov(y,yhat,0,'coeff')));


