function [r,pr]=pupil_load(cellid, options)

if ~exist('options','var'),
   options=struct();
end

fs=getparm(options,'fs',50);   %sampling frequency (Hz)
offset = getparm(options,'offset',0.75); %delay to apply to pupil data (s)

cellfiledata = dbgetscellfile('cellid', cellid, 'runclass', 'VOC', 'Subsets',5, 'pupil',2);

for cellidx=1:length(cellfiledata)
   
   clear('r','rsort','pr','psort');
   
   %load experimental parameters
   parmfile = [cellfiledata(cellidx).stimpath cellfiledata(cellidx).stimfile];
   spkfile = [cellfiledata(cellidx).path cellfiledata(cellidx).respfile];
   LoadMFile(parmfile);
   if strcmp(exptparams.TrialObject.ReferenceClass,'FerretVocal'),
      prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
      duration = exptparams.TrialObject.ReferenceHandle.Duration;
   else
      prestim = exptparams.TrialObject.TargetHandle.PreStimSilence;
      duration = exptparams.TrialObject.TargetHandle.Duration;
   end
   %load spike raster
   clear('options');
   options.channel = cellfiledata(cellidx).channum;
   options.unit = cellfiledata(cellidx).unit;
   options.rasterfs = fs;
   options.usesorted = 1;
   options.includeprestim=1;
   if ~isempty(cellfiledata(cellidx).goodtrials)
      options.trialrange = eval(cellfiledata(cellidx).goodtrials);
   end
   options.runclass='VOC';
   
   %r = raster_load(parmfile, channel, unit, options);
   r = loadspikeraster(spkfile, options);
   
   %load pupil diameter
   clear('options');
   options.pupil = 1;
   options.verbose = false;
   options.rasterfs = fs;
   options.pupil_offset = offset;
   options.pupil_mm = 1;
   options.runclass='VOC';
   options.includeprestim=1;
   if ~isempty(cellfiledata(cellidx).goodtrials)
      options.trialrange = eval(cellfiledata(cellidx).goodtrials);
   end
   [pr, tags] = loadevpraster(parmfile, options);
   
   %parse vocalization names
   for ii=1:length(tags)
      vocstr = regexp(tags(ii), '(\w*).wav', 'match');
      vocnames(ii) = vocstr{1};
   end
end


