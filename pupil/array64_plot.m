%% do the array plot
%
%[AH,DC]=probe_64D_axes(1:length(cellids));
[AH,DC]=probe_64D_axes(channel);

xc=zeros(cellcount,1);
for ii=1:length(cellids),
   %axes(AH(channel(ii)));
   axes(AH(ii));
   
   if strcmpi(runclass,'TOR'),
      r=resp{ii}((PreStimBins+1):(end-PostStimBins),:,:);
      [strf{ii},snr,StimParam]=strf_est_core(r,exptparams.TrialObject.ReferenceHandle,fs,0);
      
      startbins=size(strf{ii},2);
      keepbins=min(startbins,10);
      basep=StimParam.basep./startbins.*keepbins;
      tstrf=strf{ii}(:,1:keepbins);
      stplot(tstrf,StimParam.lfreq,basep,1,StimParam.octaves);
      if snr<0.3,
         mm=max(abs(strf{ii}(:)));
         mm=mm./(max(snr,0.05).*10/3);
         set(gca,'Clim',[-1 1].*mm);
      end
      axis off
      text(0,0,sprintf('%d: %.3f',ii,snr),'FontSize',4);
    elseif strcmpi(analysis,'strf') && strcmpi(runclass,'PPS'),
       % FRA analysis
       
       s=reshape(stim,size(stim,1),size(stim,2)*size(stim,3));
       %stim=stim(:,PreBins+1:(end-PostBins),:);
       r=resp{ii}(PreStimBins+1:(end-PostStimBins),:,:);
       s=s(:,:)';
       r=permute(r,[2 1 3]);
       r=r(:,:)';
       r=r.*fs;
       
       if ~isempty(pupil)
          pr0=pupil{ii};
          pr0=pr0(PreStimBins+1:(end-PostStimBins),:,:);
          pr0=permute(pr0,[2 1 3]);
          pr0=pr0(:,:)';
          
          sp=sort(pr0(:));
          mm=sp(round(length(sp).*0.5)); % 0.5=median
          %mm=nanmedian(pr0(:));
          rset=ones(size(r));
          rset(pr0>mm)=2;
       else
          rset=ones(size(r));
       end
       
       setcount=length(unique(rset(:)));
       
       ustim=unique(s(:));
       ustim=flipud(ustim(ustim>0));
       l_count=length(ustim);
       freq_count=size(s,2);
       
       FRA=zeros(l_count,freq_count,setcount);
       FRAe=zeros(l_count,freq_count,setcount);
       rwin=round(0.01*options.rasterfs):round(0.04.*options.rasterfs);
       cclag=10;
       ccwin=zeros(cclag*2+1,setcount);
       ccn=zeros(cclag*2+1,setcount);
       for setidx=1:setcount,
          rtemp=r;
          rtemp(rset~=setidx)=nan;
          
          for ff=1:freq_count,
             for ll=1:l_count,
                dd=find(s(:,ff)==ustim(ll));
                dd=dd(dd<=size(r,1)-max(rwin));
                iiset=[];
                for rr=1:length(rwin),
                   iiset=cat(3,iiset,rtemp(dd+rwin(rr),:));
                end
                rr=nanmean(iiset,3);
                
                FRA(ll,ff,setidx)=nanmean(rr(:));
                FRAe(ll,ff,setidx)=nanstd(rr(:))./sqrt(sum(~isnan(rr(:))));
                
                if ll<=3,
                   for rr=1:length(ccwin),
                      tdd=dd+rr-cclag-1;
                      tdd=tdd(tdd>=1 & tdd<=size(rtemp,1));
                      ccwin(rr,setidx)=ccwin(rr,setidx)+nansum(nansum(rtemp(tdd,:)));
                      ccn(rr,setidx)=ccn(rr,setidx)+sum(sum(~isnan(rtemp(tdd,:))));
                   end
                end
             end
          end
       end
       ccwin=ccwin./ccn;
       
       % FRAs
       strf{ii}=cat(2,FRA(:,:,1),zeros(size(FRA,1),1),FRA(:,:,2));
       imagesc(strf{ii});
       axis ij
       
       % timecourse
       %hl=plot((-cclag:cclag)'./options.rasterfs,ccwin);
       %set(hl(1),'Color',locolor);
       %set(hl(2),'Color',hicolor);
       %%xlabel('time from pip onset (s)');
       %%ylabel('mean spike rate (Hz)');
       %%legend('small','big','Location','NorthWest');
       %%legend boxoff
       
       % frequency tuning
%        levidx=3;
%        mm=mean(mean(mean(FRA(:,:,(1:(setcount))))));
%        ff=gsmooth(mean(FRA(levidx,:,1:(setcount))-mm,3),0.5);
%        bfi=round(min(find(abs(ff)==max(abs(ff))))./2);
%        hl=errorbar(squeeze(FRA(levidx,:,1:(setcount))),...
%           squeeze(FRAe(levidx,:,1:(setcount))));
       
       
       % level tuning
       %hl=errorbar(squeeze(FRA(:,bfi,1:(setcount))),squeeze(FRAe(:,bfi,1:(setcount))));
       
       set(hl(1),'Color',locolor);
       set(hl(2),'Color',hicolor);
       
       axis tight
       axis off;
       
       aa=axis;
       text(aa(1),aa(4),sprintf('%d',ii),'FontSize',4);
       
       %FRA=cat(3,FRA,FRA(:,:,2)-FRA(:,:,1));
       %setcount=setcount+1;
       
   elseif strcmpi(analysis,'strf'),
      if size(strf{ii},2)<=3,
         plot(strf{ii}');
      else
         plotastrf(strf{ii},2);
      end
      
      axis off
      %text(0,0,sprintf('%.3f',snr));
   elseif strcmpi(analysis,'raster') && exist('subset','var') && subset==8,

   elseif strcmpi(analysis,'raster'),
      tr=resp{ii};
      spontrate=nanmean(nanmean(resp{ii}(1:PreStimBins,:)));
      tr=rconv2(tr,[1;1;1]./3);
      m=mean(tr,2);
      se=std(tr,0,2)./sqrt(size(tr,2));
      rrange=(PreStimBins-50)+(1:150);
      %plot(m(rrange),'k');
      %hold on
      %plot([m(rrange)+se(rrange),m(rrange)-se(rrange)],'g');
      plot(m(rrange)./se(rrange),'k');
      hold on;
      plot([1 length(rrange)],[0 0],'g--');
      
      mm=min(find(m(PreStimBins:end)-se(PreStimBins:end).*2.5>0));
      if ~isempty(mm),
         %plot([mm mm]+50,[min(m-se) max(m+se)],'r-');
         plot([mm mm]+50,[0 4],'r-');
      end
      hold off
      text(0,0,sprintf('%d - %.0f',mm,iso(ii)),'FontSize',4);
      
%       tr=tr((PreStimBins-99):(end-PostStimBins+100),:);
%       [xx,yy]=find(tr);
%       plot(xx./1000,yy,'k.','MarkerSize',1);
%       hold on;
%       plot([0.1 0.1],[min(yy) max(yy)],'g-');
%       plot([0.2 0.2],[min(yy) max(yy)],'g-');
%       hold off
      
      options=struct('rasterfs',1000,'psthfs',100,'psth',1,...
         'PreStimSilence',0.1,'PostStimSilence',0.1);
      %raster_plot(parmfile,tr,tags,AH(ii),options);
      axis off
      axis tight
      %text(0,0,sprintf('%.3f',snr));
   elseif strcmpi(analysis,'spont') || strcmpi(analysis,'evoked'),
      if strcmpi(analysis,'spont'),
         pm=(pupil{ii}(1:PreStimBins,:));
         rm=(resp{ii}(1:PreStimBins,:));
      else
         pm=pupil{ii}((PreStimBins+1):(end-PostStimBins),:);
         rm=resp{ii}((PreStimBins+1):(end-PostStimBins),:);
         %pm=pupil{ii}((PreStimBins+1):(end),:);
         %rm=resp{ii}((PreStimBins+1):(end),:);
         r0=nanmean(rm,2);
         rm=rm-repmat(r0,[1 size(rm,2) 1]);
      end

      T=size(pm,1);
      pm=nanmean(reshape(pm,T,[]));
      rm=nanmean(reshape(rm,T,[]));
      pm=pm(~isnan(pm));
      rm=rm(~isnan(rm));
      
      %rm=gsmooth(rm,2);
      [xc(ii),exc,~,p]=randxcov(pm,rm,0,500);
      dotcolor=[0.9 0.9 0.9].*(1-abs(xc(ii)));
      plot(pm,rm,'.','Color',dotcolor);
      if p<0.05,
         text(min(pm),max(rm),sprintf('%d: %.3f*',ii,xc(ii)),'FontSize',4);
      else
         text(min(pm),max(rm),sprintf('%d: %.3f',ii,xc(ii)),'FontSize',4);
      end
      
      bincount=8;
      sd=sortrows([pm(:) rm(:)]);
      pb=[sd(:,1);max(pm(:))+1];
      bidx=round(linspace(1,length(pb),bincount+1));
      pb=zeros(bincount,1);
      rb=zeros(bincount,1);
      for jj=1:bincount,
         pb(jj)=mean(sd(bidx(jj):(bidx(jj+1)-1),1));
         rb(jj)=mean(sd(bidx(jj):(bidx(jj+1)-1),2));
      end
      hold on;
      plot(pb,rb,'k-');
      hold off
      
      axis off
      axis tight
      aa=axis;
   elseif strcmpi(analysis,'psth') && exist('subset','var') && subset==8,
      
      vocidx=[1 53];
      rm=resp{ii}(:,:,vocidx);
      rm=squeeze(nanmean(rm,2));
      tt=(1:size(rm,1))./fs;
      mm=max(rm(:));
      plot([0 0]+prestim,[0 mm],':','Color',[0.5 1 0.5]);
      hold on
      plot([0 0]+prestim+duration,[0 mm],':','Color',[0.5 1 0.5]);
      plot([prestim prestim],[0 mm],'--','Color',[0.6 0.6 0.6]);
      plot(tt,rm(:,1),'Color',hicolor);
      plot(tt,rm(:,2),'Color',locolor);
      hold off
      
      text(0,0,sprintf('%s',cellids{ii}),'FontSize',6);
      
      axis off
      axis tight
   elseif strcmpi(analysis,'psth'),
      pm=nanmean(pupil{ii});
      rm=resp{ii};
      pm=repmat(pm,[size(rm,1) 1 1]);
      pmed=nanmedian(pm(:));
      tr=rm;
      tr(pm<pmed)=nan;
      r0=nanmean(tr,2);
      tr=rm;
      tr(pm>pmed)=nan;
      r1=nanmean(tr,2);
      
      T=size(pm,1);
      pm=nanmean(reshape(pm,T,[]));
      rm=nanmean(reshape(rm,T,[]));
      pm=pm(~isnan(pm));
      rm=rm(~isnan(rm));
      
      plot([r0(:) r1(:)]);
      xc(ii)=xcov(pm,rm,0,'coeff');
      text(0,0,sprintf('%.3f',xc(ii)),'FontSize',6);
      
      axis off
      axis tight
   end
   
end

set(gcf,'Name',[siteid '_' runclass '_' analysis]); 
set(gcf,'PaperPosition',[0.25 0.25 2 10.5]);

fprintf('to save:\n print -f%d -dpdf array_%s%d_%s.pdf\n',gcf,runclass,subset,analysis);

