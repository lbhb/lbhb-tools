% define some plot colors
locolor=[.5 .5 .5];
hicolor=[0 0 0];
lofitcolor=[0 0 0.8];
hifitcolor=[0.8 0.2 .2];

%% begin decoding analysis
% 
PUPIL_CORRECT=1;
if PUPIL_CORRECT,
   rbase={};
   rnorm={};
   betas=zeros(4,cellcount);
   %trimbins=0;
   trimbins=PreStimBins;
   
   params=struct();
   params.maxlag=[0 6];
   params.jackcount=20;
   params.trimbins=trimbins;
   smoothbins=8;
   dsbins=4;
   
   fprintf('trimbins=%d\n',trimbins);
   for ii=1:cellcount
      r=resp{ii};
      p=pupil{ii};
      %p=repmat(nanmean(p(1:PreStimBins,:,:),1),[size(p,1) 1 1]);
      p=p-nanmean(p(:));
      
      rreps=squeeze(sum(~isnan(r(1,:,:))));
      
      % smooth, downsample
      if smoothbins>1,
         % smooth and downsample
         respfilt=ones(smoothbins,1)./smoothbins;
         for jj=1:length(rreps),
            r(:,1:rreps,jj)=rconv2(r(:,1:rreps,jj),respfilt);
         end
         
         if ii==1,
            stimfilt=ones(1,smoothbins,1)./smoothbins;
            %stim0=sqrt(stim);
            %stim0=convn(sqrt(stim),stimfilt,'same');
            stim0=convn(stim,stimfilt,'same');
            params.trimbins=trimbins/smoothbins;
         end
      end
      
      r=sqrt(r);
      %m0=nanmean(nanmean(r((trimbins+1):(end-trimbins),:)));
      m0=nanmean(nanmean(r(1:trimbins,:)));
      r=r-m0;
      rr=r;
      rn=r;
      ridx=find(rreps==max(rreps));
      %ridx=1:length(rreps);
      tr=r(:,:,ridx);
      tr=tr((trimbins+1):(end-trimbins),:,:);
      pr=p((trimbins+1):(end-trimbins),:,ridx);
      psth=repmat(mean(tr,2),[1 max(rreps) 1]);
      X=[ones(size(pr(:))) pr(:) psth(:) psth(:).*pr(:)];
      Y=tr(:)-psth(:);
      X=X(find(~isnan(Y)),:);
      Y=Y(~isnan(Y));
      [b,bint]=regress(Y,X);
      %[b,bint]=robustfit(X(:,2:4),Y);
      sigbins=abs(b)>(bint(:,2)-bint(:,1)).*1.5;
      %sigbins=bint.p<0.05
      if ~sum(sigbins>0),
         % do nothing
         b(:)=0;
         
      else
         
         if sum(~sigbins)>0,
            X=X(:,find(sigbins));
            [bn,bintn]=regress(Y,X);
            %[bn,bintn]=robustfit(Y,X(:,2:4));
            b(:)=0;
            bint(:)=0;
            b(find(sigbins))=bn;
            bint(find(sigbins),:)=bintn;
         end
         %b=bint(:,1).*(b>0) + bint(:,2).*(b<0);
         
         pshuff=p;
         pshuff(~isnan(p))=shuffle(p(~isnan(p)));
         rr=r-b(1)-b(2).*pshuff.*2.5;
         rn=r-b(1)-b(2).*p.*2.5;
         if b(4),
            rr=rr./(1+b(3)+b(4).*pshuff);
            rn=rn./(1+b(3)+b(4).*p);
         end
         
      end
      rbase{ii}=rr;
      rnorm{ii}=rn;
      betas(:,ii)=b;
   end
   
   if ~isempty(perf),
      perfmtx=cat(1,perf{:});
   else
      perfmtx=ones(cellcount,1);
   end
   %predcells=find(perfmtx(:,1)>0.3);
   %cidx=predcells;
   
   cidx=find(sum(abs(betas([2],:)),1)>0 & perfmtx(:,1)'>0.2);
   %cidx=1:cellcount;cd
   layer4cells=24:41;
   layer3cells=[42:49];
   layer5cells=[4:23];
   %cidx=layer3cells;
   
   fprintf('length(cidx)=%d\n',length(cidx));
   r1=permute(cat(4,rbase{cidx}),[1 4 2 3]);
   r2=permute(cat(4,rnorm{cidx}),[1 4 2 3]);
   stim0=repmat(permute(stim0,[1 2 4 3]),[1 1 size(r1,3) 1]);
   kk=find(~isnan(r1(1,1,:)));
   r1=r1(:,:,kk);
   r2=r2(:,:,kk);
   stim0=stim0(:,:,kk);
   
   if strfbatch==263,
      cleantrials=find(abs(stim0(7,1,:))<max(abs(stim0(7,1,:)))/20);
      noisetrials=setdiff((1:size(stim0,3))',cleantrials);
      %r1=r1(:,:,noisetrials);
      %r2=r2(:,:,noisetrials);
      %stim0=stim0(:,:,noisetrials); 
      chrange=3:17;
      ccplot=6;
      showstim=1:18;
      %showstim=19:36;
      %showstim=157:174;
      stim0=stim0(chrange,:,:);
   elseif strfbatch==283,
      stim0=sum(stim0,1);
      chrange=1;
      ccplot=1;
      showstim=1:12;
   end

   if dsbins>1,
      % downsample
      r1=r1(1:dsbins:end,:,:);
      r2=r2(1:dsbins:end,:,:);
      stim0=stim0(:,1:dsbins:end,:);
   end
   
   params.trimbins=8;
   rr=(params.trimbins+1):(size(r1,1)-params.trimbins);
   
   fprintf('decoding with raw reponse\n');
   [vstim1,~,estim1]=medium_recon(r1,stim0,r2,params);
   fprintf('decoding with pupil-normed reponse\n');
   [vstim2,~,estim2]=medium_recon(r2,stim0,r1,params);
   chancount=size(stim0,1);
   vmse=zeros(chancount,2);
   vxc=zeros(chancount,2);
   
   estim1=estim1.*(estim1>0);
   estim2=estim2.*(estim2>0);
   
   for cc=1:chancount,
      s1=estim1(cc,rr,:);s2=stim0(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmse(cc,1)=mserr(s1(:),s2(:));
      vxc(cc,1)=xcov(s1(:),s2(:),0,'coeff');
      s1=estim2(cc,rr,:);s2=stim0(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmse(cc,2)=mserr(s1(:),s2(:));
      vxc(cc,2)=xcov(s1(:),s2(:),0,'coeff');
   end

   figure;
   
   subplot(2,3,1);
   tt=(rr.*dsbins-PreStimBins)./fs;
   %mm=mean(estim1(ccplot,rr,showstim),3);
   %ee=std(estim1(ccplot,rr,showstim),0,3);
   %errorshade(tt,mm,ee,[0.8 0.8 0.8],[0.8 0.8 0.8]);
   plot(tt,squeeze(estim1(ccplot,rr,showstim)),'LineWidth',0.5,'Color',[0.8 0.8 0.8]);
   hold on
   plot(tt,mean(stim0(ccplot,rr,showstim),3)','LineWidth',1,'Color',[0 0 0]);
   hold off
   axis([tt(1)-2/fs tt(end)+2./fs -1 26]);
   title(sprintf('c=%d rawresp E=%.3f xc=%.3f',...
      chrange(ccplot),vmse(ccplot,1),vxc(ccplot,1)));
   
   subplot(2,3,2);
   %mm=mean(estim2(ccplot,rr,showstim),3);
   %ee=std(estim2(ccplot,rr,showstim),0,3);
   %errorshade(tt,mm,ee,[0.8 0.8 0.8],[0.8 0.8 0.8]);
   plot(tt,squeeze(estim2(ccplot,rr,showstim)),'LineWidth',0.5,'Color',[0.8 0.8 0.8]);
   hold on
   plot(tt,mean(stim0(ccplot,rr,showstim),3)','LineWidth',1,'Color',[0 0 0]);
   hold off
   axis([tt(1)-2/fs tt(end)+2./fs -1 26]);
   title(sprintf('c=%d normed resp E=%.3f xc=%.3f',...
      chrange(ccplot),vmse(ccplot,2),vxc(ccplot,2)));
   
   subplot(2,3,3);
   plot(chrange,(vxc(:,2).^2-vxc(:,1).^2)./vxc(:,1).^2);
   hold on
   plot(chrange([1 end]),[0 0],'k--');
   hold off
   title('recon xc difference');
   
   subplot(2,3,4);
   imagesc(estim2(:,rr,showstim(2)));
   axis xy
   set(gca,'YTick',1:size(estim2,1),...
      'YTickLabel',round(stimparam.ff(chrange).*1000));
   
   subplot(2,3,5);
   imagesc(estim2(:,rr,showstim(4)));
   axis xy
   set(gca,'YTick',1:size(estim2,1),...
      'YTickLabel',round(stimparam.ff(chrange).*1000));
   
   subplot(2,3,6);
   imagesc(stim0(:,rr,showstim(2)));
   axis xy
   set(gca,'YTick',1:size(estim2,1),...
      'YTickLabel',round(stimparam.ff(chrange).*1000));
   
   colormap(1-gray);
   fullpage landscape
   set(gcf,'PaperPosition',[0.25 2.25 10.5 5]);
   
else
   
   %perfmtx=cat(1,perf{:});
   %predcells=find(perfmtx(:,1)>0.3);
   predcells=1:cellcount;
   
   goodcells=predcells;
   
   %goodcells=intersect(predcells,layer5cells);
   %goodcells=intersect(predcells,layer4cells);
   
   % non-gain cells:
   %goodcells=[9:19 45:55];
   
   % gain cells:
   %goodcells=[20:26 29:43];
   
   r0=permute(cat(4,resp{goodcells}),[1 4 2 3]);
   stim0=repmat(permute(stim,[1 2 4 3]),[1 1 size(r0,3) 1]);
   kk=find(~isnan(r0(1,1,:)));
   r0=r0(:,:,kk);
   stim0=stim0(:,:,kk);
   p=nanmean(pupil{1}(:,kk))./25; %approx pix2mm conversion
   %p=rand(size(p));
   
   s0=sqrt(stim0);
   
   if strfbatch==263,
      cleantrials=find(abs(stim0(7,1,:))<max(abs(stim0(7,1,:)))/20);
      noisetrials=setdiff((1:size(stim0,3))',cleantrials);
%        lotrials=noisetrials;
%        hitrials=cleantrials;
%       lotrials=[cleantrials(find(p(cleantrials)<median(p(cleantrials))));
%          noisetrials(find(p(noisetrials)<median(p(noisetrials))))];
%       hitrials=[cleantrials(find(p(cleantrials)>=median(p(cleantrials))));
%          noisetrials(find(p(noisetrials)>=median(p(noisetrials))))];
%       lotrials=cleantrials(p(cleantrials)<median(p(cleantrials)));
%       hitrials=cleantrials(p(cleantrials)>=median(p(cleantrials)));
%        lotrials=noisetrials(p(noisetrials)<median(p(noisetrials)));
%        hitrials=noisetrials(p(noisetrials)>=median(p(noisetrials)));
      lotrials=find(p<median(p));
      hitrials=find(p>=median(p));
      chrange=6:15;
      s0=s0(6:15,:,:);
   else
      lotrials=find(p<median(p));
      hitrials=find(p>=median(p));
      chrange=1;
      s0=s0(chrange,:,:);
   end
   
   stimlo=s0(:,:,lotrials);
   stimhi=s0(:,:,hitrials);
   resplo=r0(:,:,lotrials);
   resphi=r0(:,:,hitrials);
   % shuffle test for random baseline
%    resplo=r0(:,:,hitrials);
%    resphi=r0(:,:,lotrials);
   
   sm=4;
   if sm>1,
      % smooth and downsample
      respfilt=ones(sm,1,1)./sm;
      stimfilt=ones(1,sm,1)./sm;
      resplo=convn(resplo,respfilt,'same');
      resphi=convn(resphi,respfilt,'same');
      stimlo=convn(stimlo,stimfilt,'same');
      stimhi=convn(stimhi,stimfilt,'same');
      resplo=resplo(1:sm:end,:,:);
      resphi=resphi(1:sm:end,:,:);
      stimlo=stimlo(:,1:sm:end,:);
      stimhi=stimhi(:,1:sm:end,:);
   end
   
   params.maxlag=[0 5];
   params.jackcount=12;
   params.trimbins=5;
   rr=(params.trimbins+1):(size(resplo,1)-params.trimbins);
   
   [vstim,xcperchan,estim]=medium_recon(resplo,stimlo,resphi,params);
   chancount=size(stimhi,1);
   emselo=zeros(chancount,1);
   vmselo=zeros(chancount,1);
   exclo=zeros(chancount,1);
   vxclo=zeros(chancount,1);
   
%    pmedlo=median(p(lotrials));
%    pmedhi=median(p(hitrials));
%    smlotrials=(p(lotrials)<pmedlo);
%    lglotrials=(p(lotrials)>=pmedlo);
%    smhitrials=(p(hitrials)<pmedhi);
%    lghitrials=(p(hitrials)>=pmedhi);
%    
   for cc=1:chancount,
      s1=estim(cc,rr,:);s2=stimlo(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      emselo(cc)=mserr(s1(:),s2(:));
      exclo(cc)=xcov(s1(:),s2(:),0,'coeff');
      
      s1=vstim(cc,rr,:);s2=stimhi(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmselo(cc)=mserr(s1(:),s2(:));
      vxclo(cc)=xcov(s1(:),s2(:),0,'coeff');
      
%       s1=estim(cc,rr,smlotrials);s2=stimlo(cc,rr,smlotrials);
%       excsmlo=xcov(s1(:),s2(:),0,'coeff');
%       s1=estim(cc,rr,lglotrials);s2=stimlo(cc,rr,lglotrials);
%       exclglo=xcov(s1(:),s2(:),0,'coeff');
%       
%       s1=vstim(cc,rr,smhitrials);s2=stimhi(cc,rr,smhitrials);
%       excsmhi=xcov(s1(:),s2(:),0,'coeff');
%       s1=vstim(cc,rr,lghitrials);s2=stimhi(cc,rr,lghitrials);
%       exclghi=xcov(s1(:),s2(:),0,'coeff');
%  
   end
   
   figure;
   
   ccplot=1;
   subplot(2,3,1);
   tt=(rr.*sm-PreStimBins)./fs;
   
   hl=plot(tt,[mean(estim(ccplot,rr,:),3)' mean(stimlo(ccplot,rr,:),3)']);
   set(hl(1),'Color',lofitcolor,'LineWidth',1);
   set(hl(2),'Color',locolor,'LineWidth',1);
   axis([tt(1)-1/fs tt(end)+1./fs -1 26]);
   title(sprintf('c=%d fitlo-predlo E=%.3f xc=%.3f',...
      chrange(ccplot),emselo(ccplot),exclo(ccplot)));
   
   subplot(2,3,2);
   hl=plot(tt,[mean(vstim(ccplot,rr,:),3)' mean(stimhi(ccplot,rr,:),3)']);
   set(hl(1),'Color',lofitcolor,'LineWidth',1);
   set(hl(2),'Color',hicolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('c=%d fitlo-predhi E=%.3f xc=%.3f',...
      chrange(ccplot),vmselo(ccplot),vxclo(ccplot)));
   
   subplot(2,3,3);
   if length(chrange)==1,
      xx=linspace(2,3.5,20);
      n1=hist(p(lotrials),xx);
      n2=hist(p(hitrials),xx);
      hb=bar(xx(:),[n1(:) n2(:)],'stacked');
      set(hb(1),'FaceColor',locolor,'LineStyle','none');
      set(hb(2),'FaceColor',hicolor,'LineStyle','none');
      aa=axis;
      axis([2 3.5 aa(3:4)]);
   else
      plot(chrange,[exclo vxclo]);
      legend('fit lo, pred lo','fit lo pred hi');
   end   
  
   [vstim,xcperchan,estim]=medium_recon(resphi,stimhi,resplo,params);
   chancount=size(stimlo,1);
   emsehi=zeros(chancount,1);
   vmsehi=zeros(chancount,1);
   exchi=zeros(chancount,1);
   vxchi=zeros(chancount,1);
   for cc=1:chancount,
      s1=estim(cc,rr,:);s2=stimhi(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      emsehi(cc)=mserr(s1(:),s2(:));
      exchi(cc)=xcov(s1(:),s2(:),0,'coeff');
      
      s1=vstim(cc,rr,:);s2=stimlo(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmsehi(cc)=mserr(s1(:),s2(:),1);
      vxchi(cc)=xcov(s1(:),s2(:),0,'coeff');
%       
%       s1=estim(cc,rr,smhitrials);s2=stimhi(cc,rr,smhitrials);
%       excsmhi=xcov(s1(:),s2(:),0,'coeff');
%       s1=estim(cc,rr,lghitrials);s2=stimhi(cc,rr,lghitrials);
%       exclghi=xcov(s1(:),s2(:),0,'coeff');
%       
%       s1=vstim(cc,rr,smlotrials);s2=stimlo(cc,rr,smlotrials);
%       excsmlo=xcov(s1(:),s2(:),0,'coeff');
%       s1=vstim(cc,rr,lglotrials);s2=stimlo(cc,rr,lglotrials);
%       exclglo=xcov(s1(:),s2(:),0,'coeff');
%  
   end
   
   subplot(2,3,4);
   hl=plot(tt,[mean(vstim(ccplot,rr,:),3)' mean(stimlo(ccplot,rr,:),3)']);
   set(hl(1),'Color',hifitcolor,'LineWidth',1);
   set(hl(2),'Color',locolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('fit hi, pred lo E=%.3f xc=%.3f',vmsehi(ccplot),vxchi(ccplot)));
   
   subplot(2,3,5);
   hl=plot(tt,[mean(estim(ccplot,rr,:),3)' mean(stimhi(ccplot,rr,:),3)']);
   set(hl(1),'Color',hifitcolor,'LineWidth',1);
   set(hl(2),'Color',hicolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('c=%d fithi-predhi E=%.3f xc=%.3f',...
      chrange(ccplot),emsehi(ccplot),exchi(ccplot)));

   if length(chrange)>1,
      subplot(2,3,6);
      plot(chrange,[vxchi exchi]);
      legend('fit hi, pred lo','fit hi pred hi');
   end
   
   %set(gcf,'PaperPosition',[0.25 0.25 2 10.5]);
end

fprintf('to save:\n print -f%d -dpdf array_%s%d_%s.pdf\n',gcf,runclass,subset,analysis);
