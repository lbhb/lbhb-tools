function y=local_lin(beta,x)
% function y=local_lin(beta,x)
%
% dc only:
%   beta=[g1 g2 cross-over offset]
%   x : T x 1 pupil vector
%  output:
%   y=hinge(beta,x)   (Tx1 vector)
%
% dc+gain:
%   beta = [g1 g2 cross-over offset] X 2 rows
%   x= time X 2 matrix [pupil r0]
%  output:
%   y= hinge(beta(1,:),x(:,1)) + hinge(beta(:,2),x(:,1)).*x(:,2) (Tx1 vector)
%
% svd 2017-01-16

D=size(x,2);
if D>size(beta,2),
   beta=repmat(beta(1,:),[D 1]);
end

g1=beta(:,1);
g2=beta(:,2);
crossover=beta(:,3);
offset=beta(:,4);
sh=crossover.*g1-crossover.*g2+offset;

y=zeros(size(x,1),1);
dd=x(:,1)<=crossover(1);
y(dd)=y(dd)+x(dd,1).*g1(1)+offset(1);
y(~dd)=y(~dd)+x(~dd,1).*g2(1)+sh(1);

if D==2,
   dd=x(:,1)<=crossover(2);
   y(dd)=y(dd)+(x(dd,1).*g1(2)+offset(2)) .* x(dd,2);
   y(~dd)=y(~dd)+(x(~dd,1).*g2(2)+sh(2)) .* x(~dd,2);
end   
