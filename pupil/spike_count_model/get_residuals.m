function dy=get_residuals(st1, smodel)
%function show_residuals(spiketimes, smodel)
% spiketimes is a cell structure holding times in ms; each cell is a
% separate trial
%
% smodel is a model for the mean spike count trajectory.
%
% out: dy cell array, with residuals for each trial


for i1 = 1:length(st1)
    % generate the counts as 1:'how many spikes there were'
    dy{i1} = slmeval(st1{i1},smodel)-(1:length(st1{i1}))';
end
