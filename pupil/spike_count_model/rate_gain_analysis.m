%% analysis of cell data
% needs rs loaded. load results first if want to continue...
load('pupil_voc_10000_hz_sp.mat')
clear opt

warning off
% to silence some issues with fminunc in infer_gs that are due to old
% options setting.

opt.Fs = 10000; % Hz samling rate
%T_all=5500; % ms
opt.T_end=5000; % ms, end of sound
opt.sound_onset = 2000; % ms

opt.T_stim(1) = opt.sound_onset; %ms, length of spontaneous region
opt.T_stim(2) = opt.T_end - opt.sound_onset;

% spontaneous/evoked model structure
opt.knot(1) = 1; % for spontaneous rate model; simplest possible
opt.knot(2)=round(opt.T_stim(2)/100); % # of knows; roughly 100ms precision. Modify here for more/less.

opt.type_label{1} = 'spontaneous';
opt.type_label{2} = 'evoked';

results = rate_gain_model(rs,pupil,opt);
% returns results, a cell array of n cells, each with 
%   cell{stim,condition} structure. So,
% results{cell}{stim, condition} contains
%   .smodel - an SLM model for the cumulative mean rate (needs SLMtools)
%   .gmodel - a gamma distribution model for the gain
%   .x      - additional outputs of smodel and gmodel: times, residuals and
%             squared residuals
%   .gs     - inferred maximum likely gains for each trials
%   .r_gp   - [rho, pval] of gain vs pupil diameters distribution
% possible fitting issues (negative evoked dE)
% 24,2e (-4, see what's really wrong)
% 24,1e +31, really big improvement, check why


% save results, and include pupil data.

save pupil_voc_10000_hz_results.mat results pupil cellids opt
% opt not saved yet
% pupil: A cell array of pupil diameter measurements (in mm). Each measurement 
% has the dimensions epoch x repetition x stimulus. The "epoch" dimension c
% ontains two positions: the mean pupil size during the prestimulus period, 
% followed by the mean size during the stimulus.



%% compare g to pupil
% pupil data now saved in results
load pupil_voc_10000_hz_results.mat 
fignum = 1;
subnum = 0;
cellok = 0;
p5 = '';
for cell_n = 1:length(results)
    for stim_n = 1:size(results{cell_n},1)
        for cond = 1:2  % let's do evoked only
            %try
            figure(fignum)  % another option - even figs for spont; odd figs for evoked
            figname = strcat('gVSpupil',num2str(fignum),'.pdf');
            orient tall
            subplot(4,2,subnum+cond)  % +cond or +stim_n
            try
            %if (isfield(results{cell_n}{stim_n,cond},'gs'))
                cellok = 1;
                gs = results{cell_n}{stim_n,cond}.gs;
                pupils = pupil.mean{cell_n}(cond, :, stim_n)';
                ndx = find( (pupils > 0) & (~isnan(gs)) );  % remove 0 pupil and NaN g
                gs = gs(ndx); pupils = pupils(ndx);
                %[rx,mx,bx] = regression(results{cell_n}{stim_n,cond}.gs,pupil.mean{cell_n}(cond, :, stim_n));
                rp = results{cell_n}{stim_n, cond}.r_gp;
                %[rho, pval] = corr( gs, pupils);
                %rp = [rho, pval];
                if rp(2)<0.05, p5='*'; else p5 = ''; end 
                scatter( pupil.mean{cell_n}(cond, :, stim_n), results{cell_n}{stim_n,cond}.gs);
                lsline
                % plotregression(pupils, gs,   strcat(cellids(cell_n),',',results{cell_n}{stim_n,cond}.label,',p(g)'));
                %title(strcat(cellids(cell_n),', stim ', num2str(stim_n)))
                title(strcat(cellids(cell_n),', stim', num2str(stim_n), ',', results{cell_n}{stim_n,cond}.label))%, ',',...
                %      num2str(cell_n),',', num2str(stim_n)))
                ylabel( strcat('g, r=', num2str(rp(1),2), p5 )) %', p=', num2str(rp(2)) ))
                %xlabel('pupil diam, mm')
                %pause
            catch
                cellok = 0;
            end %if/try
        end % cond
        % organize figures here if cond involved...
    end % stim_n
       % organize figures
    if cellok,  subnum = subnum + 2; cellok = 0; end
    if subnum>6
        subplot(4,2,7)
        xlabel('pupil diam, mm')
        subplot(4,2,8)
        xlabel('pupil diam, mm')
        subnum = 0; fignum = fignum+1; 
        saveas(gcf, figname); 
    end
end %cell_n
saveas(gcf, figname);
% even here can see some biomodal distribution in pupil diameter
                
%% review models
load pupil_voc_10000_hz_results.mat 
figure(1)
figure(2)

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2  % assume spont and evoked conditions
            try
                smodel = results{cell_n}{stim_n,cond}.smodel;
                gmodel = results{cell_n}{stim_n,cond}.gmodel;
                
                figure(2)
                close
                plotslm(smodel,'dy')
                
                figure(1)
                % plotrange = icdf(gmodel,[.025, 0975]);
                x = 0:0.05:5;
                %plot(x,pdf(gmodel,x))  % this is using PDF; fix
                plot(x, pdf('Gamma',x, gmodel.a, gmodel.b) )
                title(strcat('\sigma=', num2str(gmodel.b),', ', cellids(cell_n),', s', num2str(stim_n), ',', results{cell_n}{stim_n,cond}.label) )

                pause
            catch
                [cell_n stim_n cond]
            end %try
        end
    end
end
% hmm, most of variances pretty large, not a nice hump around 1, more like
% exponential with mean 1. try some censoring - remove traces with very low
% likelihood, re-estimate. Up to ? 5-10% of data...
%% compare sigma_g to sigma_pupil

sigma{1}.g = [];
sigma{1}.p = [];
sigma{2}.g = [];
sigma{2}.p = [];
% average over stimuli

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'gmodel'))
                sigma{cond}.g = [sigma{cond}.g results{cell_n}{stim_n,cond}.gmodel.b];
                sigma{cond}.p = [sigma{cond}.p std( nonzeros(pupil.mean{cell_n}(cond, :, stim_n)))];
                % may not be that easy, since at least for first pupil
                % distribution is bimodal... and very large 0.
                % ? could g-s be bimodal as well? different latent gain
                % model, maybe mixture of high and low sigma.
                % 0 is sometimes a special indicator of 'can't register'
                % ignore for now; may do more elaborate processing later -
                % some distribution around 'small'
            end %if
        end % stim
    end % stim_n
end %cell_n

%% visualize sigma pupil vs gain
% clean outliers
sigma{1}.g(sigma{1}.g>2)=NaN;
sigma{2}.g(sigma{2}.g>2)=NaN;


figure(1)
subplot(2,1,1)
scatter(sigma{1}.p, sigma{1}.g)
title('spontaneous')
xlabel('\sigma pupil')
ylabel('\sigma gain')
subplot(2,1,2)
scatter(sigma{2}.p, sigma{2}.g)
title('evoked')
xlabel('\sigma pupil')
ylabel('\sigma gain')

% gain: s1(22) and s2(13) large outliers (check which cell...)
% interesting, in spontaneous there is some trend.
% in evoked - maybe the opposite, but a few pops in the middle...
%
% a bit better after ignoring 0-s