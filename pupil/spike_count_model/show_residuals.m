function show_residuals(st1, smodel)
%function show_residuals(spiketimes, smodel)
% spiketimes is a cell structure holding times in ms; each cell is a
% separate trial
%
% smodel is a model for the mean spike count trajectory.

colors='bgrcmy';

clf
hold on
for i1 = 1:length(st1)
    % generate the counts as 1:'how many spikes there were'
    dy = slmeval(st1{i1},smodel)-(1:length(st1{i1}))';
    plot(st1{i1},dy,strcat(colors(mod(i1,length(colors))+1),'.'))
    % fitting with uniform grid is easy, so try it here.
end
xlabel('time, ms')
ylabel('cumulative residuals')
hold off