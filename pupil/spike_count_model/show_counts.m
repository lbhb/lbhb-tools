function show_counts(st1, gains, scale, col_map)
%function show_counts(spiketimes,[gains, scale, colormap])
% spiketimes is a cell structure holding times in ms; each cell is a
% separate trial
%
% gains, if present, typically indicate inferred g-s for each trial, 
% and paint the trajectories according to those gains
%
% scale, if present (true/false), divides counts by gain, simulating pure Poisson process
% and visualizing variance reduced by gain.

if nargin<2
    gains=(1:length(st1))';  
end

if nargin<3
    scale=false;
end

if nargin<4
    col_map=cool;
end

% preserve range
cmm = minmax(gains');
% process colors
colors = floor((gains - cmm(1))/range(gains)*(length(col_map)-1)) + 1;
%min(colors), max(colors)

% index in a colormap

cvals = col_map(colors, :);

% this would be the 'average' cumulative spike count; 
[all_t,ndx] = sort(cell2mat(st1)); % in msec.
all_s = (1:length(all_t))'/length(st1);
% not complete yet; this shows the means assuming counts, not other units
% (like residuals, or squared residuals)


clf
colormap(col_map)
hold on
for i1 = 1:length(st1)
    % generate the counts as 1:'how many spikes there were'
    yn = 1:length(st1{i1});
    if scale
        yn = yn/gains(i1);
    end %if
    stairs(st1{i1}, yn, 'Color', cvals(i1,:))
    % fitting with uniform grid is easy, so try it here.
    % here, can divide by gains (won't affect mean)
end
caxis(cmm) % ? relabel colorbar
xx=colorbar;
xx.Label.String = 'Inferred scale g';
xlabel('time, ms')
ylabel('cumulative number of spikes')

plot(all_t,all_s,'k', 'LineWidth',1)
hold off