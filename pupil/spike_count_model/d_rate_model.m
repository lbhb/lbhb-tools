function [smodel, smodel0]=d_rate_model(spiketimes, T_end, order_n, iterations)
% function [slm, slm0]=d_rate_model(spiketimes, T_end, knots, iterations)
% inputs:
% * spiketimes is a cell array of spike times in ms. The number of cells
% indicate number of trials
% * T_end - end time, or length of trial, in ms
% actually, spiketimes and T_end should be in the same units, regardless
% what they are...
%
% order_n defines the model order
%
% * iterations, if present, runs several re-estimates of the model trying to
% get the knots uniform on the output (range), rather in the input. That
% will put more knots where the rate is changing more
%
% output: slm is a spline? model from the matlab curve fitting toolbox
% initially try just linear regression for a very simple fit on
% spontaneous.

% since we assume that the total count will only increase, I can do a
% constant rate model, line 2 params, plus slm for the variability around
% the line

if nargin<3, iterations=0; end

%model_def = slmset;

% for interactive, has to have fixed knots! otherwise too slow...
% not sure if i should constrain jerk. nah, 3rd derivative - 2nd derivative of rate, 
% can't be just one sign
%
% left(min)value and left(min)slope - may want to constrain somehow. 0 and 0? 0
% for the slope means 0 rate at that time, probably not reasonable. 0 for
% value may be ok (does not affect the rate). is it reasonable?
% 
% minslope = 0 ensures increasing cumulative again.
%
% ? robust
% see if using time in s may be better for the fitters.


% this would be the 'average' cumulative spike count; I will try to fit a
% curve to this for the mean. At some point may try fitting to the raw data
% rather than the mean
all_t = sort(cell2mat(spiketimes)); % ms.
all_s = (1:length(all_t))'/length(spiketimes);

smodel = fit(all_t,all_s,'poly1');
smodel0 = smodel;
%smodel.stats.RMSE

% if iterations>0
%     for i1 = 1:iterations
%         knots_v = [ 0 slmeval((1:knot_n)/(knot_n+1)*slmeval(T_end, smodel), smodel, -1) T_end];
%         % right now cannot invert if outside of data set. extrapolate? (a
%         % little...)
%         dE = smodel.stats.RMSE;
%         smodel = slmengine(all_t,all_s,model_def,'knots',knots_v);
%         dE = dE - smodel.stats.RMSE;
%     end %for
%     dE
% end %if

