function st1 = raster2cell(raster, Fs, To, T1)
%function spike_struct = raster2cell(raster, Fs [,To, T1])
% raster is n x T, n trials, T bins, 
% sampling frequency Fs
% optional range [To T1] ms
%
% spikes_struct contains
%   
%   spiketimes is n cells, each cell has the spike times in ms.
% keep like this for now; later maybe more. All relative to To

ms=1000/Fs;

if nargin<3 To = 0; end
if nargin<4 % T1 in sampling units
    T1 = size(raster,1); 
else
    T1 = min(round(T1/ms), size(raster, 1) ); % time is down, not across.
end %if

To = round(To/ms)+1; % To in sampling units, to be used as index

%[To T1]

for i1 = 1:size(raster, 2)
    %st1{i1,1} = unique(0, find(raster(To:T1,i1))*ms);  % spike times in ms, relative to To (I think)
    st1{i1,1} = find(raster(To:T1,i1))*ms;
    % adding the initial 0 may anchor the 1st point at (0,0)
    % but I need to generate the counts appropriately. function!
    % nah, will break other things. deal with it in the model definition
end %for


% correction for know error in some cases - last item is all 1-s
if length(st1{end,1})==(T1-To+1) % if as long as raster,
    st1{end,1} = []; % erase it
    'raster2cell exception'
end