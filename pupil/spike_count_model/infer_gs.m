function gs = infer_gs(st1, smodel, gmodel)
% function gs = infer_gs(st1, smodel, gmodel)

de=1e-7;
%optim_opts = optimoptions(@fminunc,'Display','off','FunctionTolerance',de,...
%    'FiniteDifferenceType','central','MaxIterations',500,'Algorithm','quasi-newton',...
%    'OptimalityTolerance',de,'StepTolerance',de);
% pre-2013 does not have optimoptions. see what works in common.
optim_opts = optimset('fminunc');
optim_opts = optimset(optim_opts, 'Display','off','TolFun',de,...
'MaxIter',500,'Algorithm','active-set','TolX',de);
% or trust-region
% active set does not seem to make a difference; trust-region requires a
% gradient, and quasi-newton cannot be set with optimset, only with
% optimoptions (looks like)
% right now method automatically selects quasi-newton and seems to work
% well, but issues a warning. I have supperssed the warning in the main
% code.

gs = zeros(size(st1));

for i1 = 1: length(gs)
    %try  % I think because of empty st1, but do a capture anyway
    if ~isempty(st1{i1})
        logg = fminunc(@(g) (-ll_pgmodel(exp(g), st1{i1}, smodel, gmodel)), 1, optim_opts);
        gs(i1) = exp(logg);
    else
    %catch
        gs(i1) = NaN;
    end %try/if
end %for
% nah, l_pgmodel is not working too well, saturates for some reason.

% check for following error:

% Error using fminusub (line 16)
% Objective function is undefined at initial point. Fminunc cannot continue.
% 
% Error in fminunc (line 439)
% [x,FVAL,GRAD,HESSIAN,EXITFLAG,OUTPUT] = fminusub(funfcn,x, ...
% 
% Error in infer_gs (line 12)
% gs(i1) = fminunc(@(g) -ll_pgmodel(g, st1{i1}, smodel, gmodel), 1, optim_opts);
%
% Hmm, maybe I should constrain g to be positive. use fmincon instead? or
% do exp(g). no, does not fix that completely...
% I could do something simpler and just pick the max from a discrete list,
% should be about ok, dg = bin size...