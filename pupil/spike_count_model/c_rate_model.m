function [smodel, smodel0]=c_rate_model(spiketimes, T_end, knot_n, iterations, dElim)
% function [slm, slm0]=c_rate_model(spiketimes, T_end, knots, iterations)
% inputs:
% * spiketimes is a cell array of spike times in ms. The number of cells
% indicate number of trials
% * T_end - end time, or length of trial, in ms
% actually, spiketimes and T_end should be in the same units, regardless
% what they are...
%
% * knots define the size of the model; knots*2 parameters. The correspond
% roughly to precision, so a good start may be T/precision
%
% * iterations, if present, runs several re-estimates of the model trying to
% get the knots uniform on the output (range), rather in the input. That
% will put more knots where the rate is changing more
%
% output: slm is a spline mode using the SLMtools toolbox. It is twice
% differentiable. The inferred rate is the derivative of the model.
% if desired, slm0 as the initial fit can be returned as well

% since we assume that the total count will only increase, I can do a
% constant rate model, line 2 params, plus slm for the variability around
% the line

if nargin<3, iterations=0; end
if nargin<4, dElim = 0; end

model_def = slmset;
if knot_n>1 % indication for a rich model
    model_def = slmset(model_def,'increasing','on','interiorknots','fixed',...
    'knots',knot_n,'minslope',0,'verbosity',0,'leftvalue',0);
else %indication for a linear fit
    model_def = slmset(model_def,'increasing','on','interiorknots','fixed',...
    'knots', [0 T_end], 'degree',1, 'minslope',0,'verbosity',0,'leftvalue',0);
    % possibly say 'knots',[0 T_end] here for 2 knots... was 2
    % ? how to make the value at knot 0 be 0 too? with 'leftvalue', or
    % 'leftminvalue' for less constraining (allows initial positive rate
end %if
    
% for interactive, has to have fixed knots! otherwise too slow...
% not sure if i should constrain jerk. nah, 3rd derivative - 2nd derivative of rate, 
% can't be just one sign
%
% left(min)value and left(min)slope - may want to constrain somehow. 0 and 0? 0
% for the slope means 0 rate at that time, probably not reasonable. 0 for
% value may be ok (does not affect the rate). is it reasonable?
% 
% minslope = 0 ensures increasing cumulative again.
%
% ? robust
% see if using time in s may be better for the fitters.


% this would be the 'average' cumulative spike count; I will try to fit a
% curve to this for the mean. At some point may try fitting to the raw data
% rather than the mean
all_t = sort(cell2mat(spiketimes)); % ms.
all_s = (1:length(all_t))'/length(spiketimes);

smodel = slmengine(all_t,all_s,model_def);
smodel0 = smodel;
%smodel.stats.RMSE

if iterations>0
    for i1 = 1:iterations
        knots_v = [ 0 slmeval((1:knot_n)/(knot_n+1)*slmeval(T_end, smodel), smodel, -1) T_end];
        % right now cannot invert if outside of data set. extrapolate? (a
        % little...)
        dE = smodel.stats.RMSE;
        smodel = slmengine(all_t,all_s,model_def,'knots',knots_v);
        dE = dE - smodel.stats.RMSE;
        if dE<=dElim, break; end  % another control, quicker exit and make sure no negative changes
        % see if I need to remember previous model...
    end %for
    sprintf('%u %u %g total %g',iterations, i1, dE, smodel0.stats.RMSE-smodel.stats.RMSE)
end %if

