function results = rate_gain_model(rs,pupil,opt)
% results = rate_gain_model(rs,pupil,opt)
% takes the rs cell structure which is num_cells list of cells, each cell
% contains cells of n different stimuli. Each stim has a spontaneous
% portion and evoked portion, as described in opt.sound_onset, opt.T_stim
% and opt.T_end. 
% also takes pupil, for its mean field, a list of n cells, each with a
% matrix conditions x trials x stimuli (conditions = 2 here: evoked and
% spontaneous)
% 
% returns results, a cell array of n cells, each with 
%   cell{stim,condition} structure. So,
% results{cell}{stim, condition} contains
%   .smodel - an SLM model for the cumulative mean rate (needs SLMtools)
%   .gmodel - a gamma distribution model for the gain
%   .x      - additional outputs of smodel and gmodel: times, residuals and
%             squared residuals
%   .gs     - inferred maximum likely gains for each trials
%   .r_gp   - [rho, pval] of gain vs pupil diameters distribution

warning off
% to suppress warning from fminunc due to deprecated use of optimset


Fs = opt.Fs; % Hz samling rate

T_stim = opt.T_stim; % sound onset
T_end = opt.T_end;
sound_onset = opt.sound_onset;
% spontaneous/evoked model structure
knot = opt.knot;


repeats(1) = 0;
repeats(2) = 10; % max iters for some non-uniform binning; no need for spont, since linear model
dElim = 1e-4; % another iteration precision limit, complementing repeats. 
% In some cases the node re-adjustments was gving me negative improvements,
% so try to stop that, and finish up if improvements small.

type_label = opt.type_label;


% clear results. or could try try:catch structure
% prepare for inputing results to continue
% if ~exist('results','var')
     results = {};
% end
% 
start_n = length(results)+1;  % was 1; debug with 2
    
% probably do one joint; but if I do it, the combined error catch would be
% more difficult to make. so ok like that.
for cell_n = start_n:length(rs)
    for stim_n = 1:length(rs{cell_n})      
        % spontaneous
        st{1} = raster2cell(rs{cell_n}{stim_n},Fs, 0, sound_onset);
        % evoked
        st{2} = raster2cell(rs{cell_n}{stim_n},Fs, sound_onset, T_end);
        for i1 = 1:2 %spontaneous/evoked models
            sprintf('trying cell %d stim %d, %s', cell_n, stim_n, type_label{i1})
            results{cell_n}{stim_n,i1}.label = type_label(i1);
            results{cell_n}{stim_n,i1}.st = st{i1};
            try
                % models. used later, so give local names.
                smodel=c_rate_model(st{i1}, T_stim(i1), min(knot(i1),round(length(st{i1})/3)), repeats(i1), dElim);
                results{cell_n}{stim_n,i1}.smodel = smodel;
                % smodel modified so knots depend on data size...

                [s2_g, x.all_t, x.gs_res, x.dn2]  = gs_model(st{i1}, smodel);
                results{cell_n}{stim_n,i1}.x = x; % extra info for diagnostics
                % ok, so some of the drawbacks of the current mean model is that it is
                % trying to fit the end points too hard, and because of that may make
                % some very sharp transitions there. And that will put more nodes in
                % that vicinity, which may exacerbate the problem.
                %
                % see if there is a parameters that can tempre this behaviors.
                % anothe option - use a different basis, maybe hermit over the whole
                % time range...

                % gain model. uses probability objects
                %gmodel = makedist('Gamma','a',1/s2_g,'b',s2_g);
                gmodel.a = s2_g; gmodel.b = s2_g;
                results{cell_n}{stim_n,i1}.gmodel = gmodel;
                
                % infer g-s
                %g_95 = icdf(gmodel, [0.05, 1-0.05]);
                gs = infer_gs(st{i1}, smodel, gmodel);
                results{cell_n}{stim_n,i1}.gs = gs;
                
                pupils = pupil.mean{cell_n}(i1, :, stim_n)';
                ndx = find( (pupils > 0) & (~isnan(gs)) );  % remove 0 pupil and NaN g
                gs = gs(ndx); pupils = pupils(ndx);
                %[rx,mx,bx] = regression(results{cell_n}{stim_n,cond}.gs,pupil.mean{cell_n}(cond, :, stim_n));

                [rho, pval] = corr( gs, pupils );
                % I think they have to be in columns, and I think that will
                % do it. Not for corrcoeff, use corr
                results{cell_n}{stim_n, i1}.r_gp = [rho, pval];

                % assign to structure. intersperse so I can catch at least
                % some work, even if there is error
            catch ME
               'error!'
               results{cell_n}{stim_n,i1}.ME = ME;
            end % catch
        end % i1
    end % stim_n
end % cell_n