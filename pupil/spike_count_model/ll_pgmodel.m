function ll = ll_pgmodel(g, ti, smodel, gmodel)
% function ll = ll_pgmodel(g, spiketimes, smodel, gmodel)
% is a (mean, per sample) log-likelihood function of g, depending on 
% spiketimes - a vector! of spike times (so use times{n} if in an array)
% smodel - a model for the mean, currently assumung slm structure
% gmodel - a model for the g
%
% ll to be maximized for inferred  g under the gmodel

% would be helpful to provide the derivative d/dg of this function


ni = 1:length(ti); % those are the inferred counts.
% the same as the index, but keep separate in case structure changes later
% on.
mu_t = max(0,slmeval(ti,smodel) ); % those are the poisson model means for those times.
% artificially make sure rate positive; fix in model definition later on...

ll=0; % log likelihood accumulator
% !! what if g is vectorized? see ll_pgdmodel
for i1 = 1:length(ti)
    % I need to run a loop, since the mean changes from time to time. can't
    % do a vector n input; mu is not fixed.
    %ll = ll+log( pdf('Poisson', ni(i1) ,g*mu_t(i1)) ) + log(pdf(gmodel,g));
    %ll = ll+log( pdf('Poisson', ni(i1) ,g*mu_t(i1)) ) + gamma_log_pdf(gmodel,g);
    ll = ll+log( pdf('Poisson', ni(i1) ,g*mu_t(i1)) ) + log(pdf('Gamma',g, gmodel.a, gmodel.b));
    % this also uses the PDF objects; revise for more basic matlab
    % hmm, pdf should be present in the first form in 2010.
    % I may be able to do something like pdf('GammaDistribution', g, a,b)
    
    % !! sometimes I may get negative mu_t, which breaks down Poisson!!
    % ? in model, start initial knot from (0,0)?
    % add the ll coming from the poisson distribution with given scaled mean at
    % time ti, g*m
    % to that I need to add the ll coming from the g distribution, with
    % free g, which has to be optimized. make it a function, and try a
    % bunch of g-s
    % good, already vectorized.
end
ll = ll/length(ti);