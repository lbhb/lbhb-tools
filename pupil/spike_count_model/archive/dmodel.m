%% dmodel
% requires most variables from zach_analysis_spont
% main changing variables: cell_n and stim_n
% output in results cell

sprintf('trying cell %d stim %d, %s', cell_n, stim_n, type_label{i1})
results{cell_n}{stim_n,i1}.label = type_label(i1);
results{cell_n}{stim_n,i1}.st = st{i1};
try
    % models
    smodel=d_rate_model(st{i1}, T_stim(i1), 1, 0);
    results{cell_n}{stim_n,i1}.smodel = smodel;
    % smodel modified so knots depend on data size...

    [s2_g, x.all_t, x.gs_res, x.dn2]  = gs_dmodel(st{i1}, smodel);  % fix to use curvefitting toolbox model
    results{cell_n}{stim_n,i1}.x = x; % extra info for diagnostics
    % ok, so some of the drawbacks of the current mean model is that it is
    % trying to fit the end points too hard, and because of that may make
    % some very sharp transitions there. And that will put more nodes in
    % that vicinity, which may exacerbate the problem.
    %
    % see if there is a parameters that can tempre this behaviors.
    % anothe option - use a different basis, maybe hermit over the whole
    % time range...

    % infer g-s
    gmodel = makedist('Gamma','a',1/s2_g,'b',s2_g);
    results{cell_n}{stim_n,i1}.gmodel = gmodel;

    %g_95 = icdf(gmodel, [0.05, 1-0.05]);
    gs = dinfer_gs(st{i1}, smodel, gmodel);  % fix gor curve fitting model
    results{cell_n}{stim_n,i1}.gs = gs;

    % assign to structure. intersperse so I can catch at least
    % some work, even if there is error       
catch ME
    'error!'
    results{cell_n}{stim_n,i1}.ME = ME;
    % ah, it may be that I get errors because of empty responses (empty
    % cell)
end % catch. still errors...
  