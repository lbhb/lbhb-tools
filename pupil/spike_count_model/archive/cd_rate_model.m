function [smodel, smodel0]=cd_rate_model(spiketimes, T_end, knot_n, iterations)
% function [slm, slm0]=c_rate_model(spiketimes, knots, iterations)
% inputs:
% * spiketimes is a cell array of spike times in ms. The number of cells
% indicate number of trials
% * T_end - end time, or length of trial, in ms
% actually, spiketimes and T_end should be in the same units, regardless
% what they are...
%
% * knots define the size of the model; knots*2 parameters. The correspond
% roughly to precision, so a good start may be T/precision
%
% * iterations, if present, runs several re-estimates of the model trying to
% get the knots uniform on the output (range), rather in the input. That
% will put more knots where the rate is changing more
%
% output: slm is a spline mode using the SLMtools toolbox. It is twice
% differentiable. The inferred rate is the derivative of the model.
% if desired, slm0 as the initial fit can be returned as well

if nargin<3, iterations=0; end

model_def = slmset;
model_def = slmset(model_def,'increasing','on','interiorknots','fixed','knots',knot_n,...
    'minslope',0,'verbosity',0);
% for interactive, has to have fixed knots! otherwise too slow...
% not sure if i should constrain jerk. nah, 3rd derivative - 2nd derivative of rate, 
% can't be just one sign
%
% left(min)value and left(min)slope - may want to constrain somehow. 0 and 0? 0
% for the slope means 0 rate at that time, probably not reasonable. 0 for
% value may be ok (does not affect the rate). is it reasonable?
% 
% minslope = 0 ensures increasing cumulative again.
%
% ? robust
% see if using time in s may be better for the fitters.


% try average with the raw data.
for i1 = 1:length(spiketimes)
    spikecounts{i1} = 1:length(spiketimes{i1});
end
[all_t,ndx] = sort( cell2mat(spiketimes)); % ms.
all_n = cell2mat(spikecounts);
all_n = all_n(ndx);



smodel = slmengine(all_t,all_n,model_def);
smodel0 = smodel;
%smodel.stats.RMSE

if iterations>0
    for i1 = 1:iterations
        knots_v = [ 0 slmeval((1:knot_n)/(knot_n+1)*slmeval(T_end, smodel), smodel, -1) T_end];
        % right now cannot invert if outside of data set. extrapolate? (a
        % little...)
        dE = smodel.stats.RMSE;
        smodel = slmengine(all_t,all_n,model_def,'knots',knots_v);
        dE = dE - smodel.stats.RMSE;
    end %for
    dE
end %if

