function p = gamma_log_pdf(gmodel,x)
% function l = gamma_log_pdf(gmodel,x)
% input
% gmodel.a and gmodel.b shape parameters;
% x is a vector of values
% output
% p is the log likelihood for observations/hypotheses in x

p = NaN(size(x));
ndx = find(x>0);
a = gmodel.a;
b = gmodel.b;

p(ndx) = -(x/b) - a .* log(b) + (-1 + a).* log(x) - gammaln(a);