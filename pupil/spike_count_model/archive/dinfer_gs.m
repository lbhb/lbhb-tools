function gs = dinfer_gs(st1, smodel, gmodel)
% function gs = infer_gs(st1, smodel, gmodel)

de=1e-7;
optim_opts = optimoptions(@fminunc,'Display','off','FunctionTolerance',de,...
    'FiniteDifferenceType','central','MaxIterations',500,'Algorithm','quasi-newton',...
    'OptimalityTolerance',de,'StepTolerance',de);

gs = zeros(size(st1)); % assuming st1 is n x 1....

for i1 = 1: length(gs)
    try  % I think because of empty st1, but do a capture anyway
        logg = fminunc(@(g) (-ll_pgdmodel(exp(g), st1{i1}, smodel, gmodel)), 1, optim_opts);
        gs(i1) = exp(logg);
    catch
        gs(i1) = NaN;
    end %try
end %for
% nah, l_pgmodel is not working too well, saturates for some reason.

% check for following error:

% Error using fminusub (line 16)
% Objective function is undefined at initial point. Fminunc cannot continue.
% 
% Error in fminunc (line 439)
% [x,FVAL,GRAD,HESSIAN,EXITFLAG,OUTPUT] = fminusub(funfcn,x, ...
% 
% Error in infer_gs (line 12)
% gs(i1) = fminunc(@(g) -ll_pgmodel(g, st1{i1}, smodel, gmodel), 1, optim_opts);
%
% Hmm, maybe I should constrain g to be positive. use fmincon instead? or
% do exp(g). no, does not fix that completely...
% I could do something simpler and just pick the max from a discrete list,
% should be about ok, dg = bin size...