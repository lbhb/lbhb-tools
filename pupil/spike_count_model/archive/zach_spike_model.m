% test spline fits for gain-poisson model
% use SLMtool?

%% see README for details. returns est_data struct

load /auto/data/a1_voc_resp.mat
% establish common time frame in s. convert ms later in s.
T_end = 3800; %ms
sound_onset = 400; %ms, take from field later



%% SLM model fit alternative
% to use slmengine in the interactive search
% shape prescription
% file:///Users/alex/Documents/MATLAB/SLMtools/html/slm_tutorial.html#13
knot_n = round(T_end/100); % T / precision, both in ms. 3800/100 is 3.8s with ~ 100Hz precision
% some of the precision controlled by knot placement by the algorithm.
% 50-100ms may be over-fitting... develop some testing

cell_n = 106;
stim_n = 1;

all_spike_times = est_data(cell_n).spiketimes_ms;
for stim_n = 1:40
    st1 = all_spike_times(:,stim_n);
    
    
    n_samp = length(st1);



    % times are in all_t. 'mean' cumulative count in all_s. includes starting
    % time.
    [n_samp stim_n]
    
    % try a model with spike times as inputs, not the average. May help with
    % some of the fits, not make them hug the average that close (or not, but
    % try)
    smodel=c_rate_model(st1, T_end, knot_n, 10);
    dy = get_residuals(st1,smodel);
    [s2_g, all_t, gs_res, dn2]  = gs_model(st1, smodel);
    % ok, so some of the drawbacks of the current mean model is that it is
    % trying to fit the end points too hard, and because of that may make
    % some very sharp transitions there. And that will put more nodes in
    % that vicinity, which may exacerbate the problem.
    %
    % see if there is a parameters that can tempre this behaviors.
    % anothe option - use a different basis, maybe hermit over the whole
    % time range...
    
    % infere g-s
    gmodel = makedist('Gamma','a',1/s2_g,'b',s2_g);
    gs = infer_gs(st1, smodel, gmodel);
    
   

    % these are the inidividual spike counts for n experiments
    figure(1)
    show_counts(st1, gs)
    
    % figure 2. model plot
    % slmengine makes a new figure. so close the current one.
    figure(2)
    close
    plotslm(smodel,'dy')
    
    
    % plot of residuals, not for now
    figure(3)
    show_residuals(st1, smodel)
    % good, it looks like expansion of trajectories is as expected...
    
    % get sigma for g distribution (geometric)
    figure(4)
    subplot(2,1,1)
    plot(all_t, dn2,'.')
    subplot(2,1,2)
    plot(all_t, gs_res,'.')
    ylim([-5,10])
    title(num2str(s2_g))
    
    pause

end


% hmm, may have to put some weights in, if trying for a ML heteroskedastic
% model with variance increasing with time. for pure poisson, v = Mu, for
% scaled poisson even worse, Mu + s^2 Mu^2...
% seems to be working.

% as a first try, the free nodes were not set too well; clustered at the
% beginning. Try again with equal nodes. much faster, compared to 'free'.
% but may need more nodes...
% otherwise rate when there were nodes looks pretty good.
% another option for the free nots is to start them uniform on the y axis,
% and pass them through a rough (interp1) model of the function to get non-uniform x
% knots. knots can be a list of vectors, so ok to generate it that way. 
% or could use - values, which use the datapoints themselves; ? adapting to
% data?

%% SLM model fit
% shape prescription
% file:///Users/alex/Documents/MATLAB/SLMtools/html/slm_tutorial.html#13
knot_n = round(T_end/200); % T / precision, both in ms. 3800/100 is 3.8s with ~ 100Hz precision
% some of the precision controlled by knot placement by the algorithm.
% stim_n = 1; % shows a very nice distinction between t-uniform and
% n-uniform knots; lower RMSE AND improved resolution (more dynamics
% compared to original)

[smodel, smodel0]=cd_rate_model(st1, T_end, knot_n, 1);
% weird, when using the raw data there seems to be a big spike at the end.
% not sure what it is trying to fit there; maybe set an extra condition...


plotslm(smodel0,'fundata')
plotslm(smodel,'fundata')
[smodel0.stats.RMSE smodel.stats.RMSE]

%% some more tests. use st1
close all
% for i1 = 1:length(st1)
%     spikecounts{i1} = 1:length(st1{i1});
% end
% [all_t,ndx] = sort( cell2mat(st1)); % ms.
% all_s = (1:length(all_t))'/length(st1);
% all_n = cell2mat(spikecounts);
% all_n = all_n(ndx)'; % ' ? somehow swapped to row vector
% plot(all_t,all_n, '.')
% 
% knot_n = round(T_end/25);
% model_def = slmset;
% model_def = slmset(model_def,'increasing','on','interiorknots','fixed','knots',knot_n,...
%     'minslope',0,'verbosity',0);
% smodel = slmengine(all_t,all_s,model_def);
% plotslm(smodel,'fundata')
% even for large number of nodes, this one tends to stay constant, then
% jump some. Definite jump at the end. Check more of slmenging, otherwise -
% do mean for now. Ah, I think I get it - it is trying to get to the last
% point, which is a node, and which happens to be pretty high here. 
% !!!

[s2_g, all_t, gs_res, dn2]  = gs_model(st1, smodel);

figure
subplot(2,1,1)
plot(all_t, dn2, '.')
% then find the mean of that. or fit directly to m + s^2 m^2
subplot(2,1,2)
plot(all_t, gs_res,'.')
% cell 1 stim 1 looks pretty good and constant that way, except for a little at
% the beginning when the rate is low ...
s2_g

%% likelihoods?
trial = 6;

mu=2;
%pd = makedist('Poisson','lambda',mu); 
% pdf('Poisson',x,mu(t)) would work better for variable mu
px = (0:round(mean(mu)*5));

gd = makedist('Gamma','a',1/s2_g,'b',s2_g);
gx = (-50:2:50)/100*6*std(gd) + mean(gd);

ll=l_pgmodel(gx, st1{trial}, smodel, gd);

% maximize
% good function, concave down, with natural starting point of 1.

de=1e-7;
optim_opts = optimoptions(@fminunc,'Display','off','FunctionTolerance',de,...
    'FiniteDifferenceType','central','MaxIterations',500,'Algorithm','quasi-newton',...
    'OptimalityTolerance',de,'StepTolerance',de);
[g_max, nll_max] = fminunc(@(g) -l_pgmodel(g, st1{trial}, smodel, gd), 1, optim_opts);
g_max


figure(5)
subplot(3,1,1)
plot(gx, pdf(gd,gx))
title('Gamma distribution')
subplot(3,1,3)
bar(px, pdf('Poisson', px,mu))
title('Poisson distribution')
xlabel('N')

% say we have a model, in smodel. pick up the times for trial 1, in ti (to
% be trial i eventually)



subplot(3,1,2)
plot(gx, ll)
hold on
plot(g_max, -nll_max,'rx')
hold off
title(strcat('Log Likelihood of g-Poisson model for \sigma^2 = ', num2str(s2_g)))
xlabel('g')

% seems to be working ok. Now streamline, and plot trajectories colored by
% their g-s, with some colorbar at the end...

%% stochastic gain
% Let's see if we can get the stochastic gain model variance this way, 
% with the same assumptions as Goris et al
% according to that, v[t] as a function should be M[t] + s^2 M[t]^2
% So I do var[t] and try to fit that to the expression, with a single param
% s?



%% most recent data
% different data structure; rasters in r, at 10kHz. almost continuous...
%load /auto/data/pupil_voc_10000_hz.mat
% very big, around 5G in this format. use sparse version below.
%% fix stim to spars
for i1 = 1:length(r)
   rs{i1}{1,1} = sparse(r{i1}(:,:,1)); 
   rs{i1}{1,2} = sparse(r{i1}(:,:,2));
end


%% sparse data
load pupil_voc_10000_hz_sp.mat


%% cumulative rasters
cell_n = 1;
To = 20000; % stim starts, .1 ms
T1 = 50000; % stim ends, .1ms. And an extra .5 sec of silence, for a total of 55000 samples
r1 = rs{cell_n}{1}(1:To,:);
r2 = rs{cell_n}{2};

figure(1)
plot(cumsum(r1))
title('total response')
%figure(4)
hold on
plot(mean(cumsum(r1),2),'k', 'LineWidth',1)
hold off

figure(2)
plot(cumsum(r1(1:To,:)))
title('spontaneous pre-stimulus')
hold on
plot(mean(cumsum(r1(1:To,:)),2),'k', 'LineWidth',1)
plot(var(cumsum(r1(1:To,:)),[],2),'r', 'LineWidth',1)
linem = (1:2e4)*16.5/2e4;
plot(1:2e4,linem+0.35*linem.^2,'b','LineWidth',2)
hold off

figure(3)
plot(cumsum(r1(To:T1,:)))
title('response to stimulus')
hold on
plot(mean(cumsum(r1(To:T1,:)),2),'k', 'LineWidth',1)
hold off

%% convert raster to cell structure
cell_n = 2;
stim_n = 1;

% cell 1: both stims don't have such a good fit as the previous cases. For stim 2
% specifically there are a couple of trajectories that are real bad
% (outliers?). see what happens if I remove them.
% stim 1 not that obvious, but maybe 10-ish trajectories that are kind of
% high...

%% figures
% run zach_analysis 

% good pairs: 3 2, 3 1, 4 1, 4 2! 5 1, 5 2, 6 1, 12 2, 15 1, 19 1 good stim
% 20 1, 22 2, 27 1 nice stim demo, few samples..., 31 2 stim, 34 1 stim
% model?, 39 2 stim, 43 1!, 44 1!, 


% these are the inidividual spike counts for n experiments
cell_n =  2
stim_n =  1
if ~isfield(results{cell_n}{stim_n,1},'ME')  % error?
    
dy0 = get_residuals(results{cell_n}{stim_n,1}.st, results{cell_n}{stim_n,1}.smodel);



% spontaneous
figure(1)
clf
subplot(2,1,1)
show_counts(results{cell_n}{stim_n,1}.st,results{cell_n}{stim_n,1}.gs,false)
title(strcat(cellids(cell_n,:),',',results{cell_n}{stim_n,1}.label, ',',...
    num2str(cell_n),',', num2str(stim_n)))
subplot(2,1,2)
show_counts(results{cell_n}{stim_n,1}.st,results{cell_n}{stim_n,1}.gs,true)
% another figure that may be useful is the count/g, which should be poisson
% distributed... I don't really need the residuals plot, so replace fig 3
% with that...
% also, rescale counts by gain, see how they look (technically -
% re-generate, so no fractional counts, but approximately should be correct)
%
% after this looks too tight; check procedure and assumptions again. for
% re-scaled trajectories, do var estimate, compare to mean (should be the
% same now?)

% figure 2. model plot
% slmengine makes a new figure. so close the current one.
figure(2)
close
plotslm(results{cell_n}{stim_n,1}.smodel,'dy')
% seems to be working ok. forgot that I showed the rate, so it was flat
% (constant)


% plot of g-s
figure(3)
plot(results{cell_n}{stim_n,1}.gs, '.')
title('g vs trial number spontaneous')
%show_residuals(st1, smodel)
% good, it looks like expansion of trajectories is as expected...

% get sigma for g distribution (geometric)
figure(4)
subplot(2,1,1)
plot(results{cell_n}{stim_n,1}.x.all_t, results{cell_n}{stim_n,1}.x.dn2,'.')
subplot(2,1,2)
plot(results{cell_n}{stim_n,1}.x.all_t, results{cell_n}{stim_n,1}.x.gs_res,'.')
ylim([-5,20])
title(strcat( '\sigma^2 = ',num2str(results{cell_n}{stim_n,1}.gmodel.b)))
%title(strcat( '\sigma^2 = ',num2str(s2_g0), ', .95 range  ', num2str(g0_95)) )
% ? show the 95% quantiles of g?
% s2 6.8353

% evoked
% sw 1.4762 (not very good, no match. but! more attention with sound? check
% consistancy)
% not quite working yet. some mix-up with time frames. Right, should use
% T_sound, not T_end
%all_t = sort(cell2mat(st1)); % ms.
%all_s = (1:length(all_t))'/length(st1);
else
    'error null'
end % error stim 1


if ~isfield(results{cell_n}{stim_n,2},'ME')  % error 2
dy = get_residuals(results{cell_n}{stim_n,2}.st, results{cell_n}{stim_n,2}.smodel);
figure(5)
clf
subplot(2,1,1)
show_counts(results{cell_n}{stim_n,2}.st,results{cell_n}{stim_n,2}.gs)
title(strcat(cellids(cell_n,:),', ',results{cell_n}{stim_n,2}.label, ', ',...
    num2str(cell_n),', ', num2str(stim_n)))
subplot(2,1,2)
show_counts(results{cell_n}{stim_n,2}.st,results{cell_n}{stim_n,2}.gs, true)
% another figure that may be useful is the count/g, which should be poisson
% distributed... I don't really need the residuals plot, so replace fig 3
% with that...

% figure 2. model plot
% slmengine makes a new figure. so close the current one.
figure(6)
close
plotslm(results{cell_n}{stim_n,2}.smodel,'dy')
% seems to be working ok. forgot that I showed the rate, so it was flat
% (constant)


% plot of g-s
figure(7)
plot(results{cell_n}{stim_n,2}.gs, '.')
title('g vs trial number evoked')
%show_residuals(st1, smodel)
% good, it looks like expansion of trajectories is as expected...

% get sigma for g distribution (geometric)
figure(8)
subplot(2,1,1)
plot(results{cell_n}{stim_n,2}.x.all_t, results{cell_n}{stim_n,2}.x.dn2,'.')
subplot(2,1,2)
plot(results{cell_n}{stim_n,2}.x.all_t, results{cell_n}{stim_n,2}.x.gs_res,'.')
ylim([-5,20])
title(strcat( '\sigma^2 = ',num2str(results{cell_n}{stim_n,2}.gmodel.b)))
%title(strcat( '\sigma^2 = ',num2str(s2_g0), ', .95 range  ', num2str(g0_95)) )
% ? show the 95% quantiles of g?
else
    'error stim'
end %if error 2