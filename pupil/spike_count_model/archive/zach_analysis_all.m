%% analysis of cell data
% needs rs loaded. load results first if want to continue...
%load('pupil_voc_10000_hz_sp.mat')
clear opt

opt.Fs = 10000; % Hz samling rate
%T_all=5500; % ms
opt.T_end=5000; % ms, end of sound
opt.sound_onset = 2000; % ms

opt.T_stim(1) = opt.sound_onset; %ms, length of spontaneous region
opt.T_stim(2) = opt.T_end - opt.sound_onset;

% spontaneous/evoked model structure
opt.knot(1) = 1; % for spontaneous rate model; simplest possible
opt.knot(2)=round(opt.T_stim(2)/100); % # of knows; roughly 100ms precision. Modify here for more/less.

opt.type_label{1} = 'spontaneous';
opt.type_label{2} = 'evoked';

results = rate_gain_model(rs,pupil,opt);


% save results, and include pupil data.

save pupil_voc_10000_hz_results.mat results pupil cellids

% pupil: A cell array of pupil diameter measurements (in mm). Each measurement 
% has the dimensions epoch x repetition x stimulus. The "epoch" dimension c
% ontains two positions: the mean pupil size during the prestimulus period, 
% followed by the mean size during the stimulus.

%% check for error sites, try to improve. needs results
for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'ME'))
                [cell_n, stim_n, cond]
                ME
                % size input must be integers error
                %results{cell_n}{stim_n,cond} 
            end %if
        end
    end
end
%% compare sigma between conditions

s0=[]; s1=[];
for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        if (isfield(results{cell_n}{stim_n,1},'gmodel')&&isfield(results{cell_n}{stim_n,2},'gmodel'))
            s0 = [s0 results{cell_n}{stim_n,1}.gmodel.b];
            s1 = [s1 results{cell_n}{stim_n,2}.gmodel.b];
        end %if
    end % stim_n
end %cell_n
scatter(s0,s1)
xlabel('\sigma spontaneous')
ylabel('\sigma evoked')
xlim([0 200])
ylim([0 100])
% better now with improved fits. large sigma still bad, but correlation for
% small sigma...
% stephen wants a research here - hypothesis that they are the same. see
% below, check again after match of spont g  model under different stims
% (should not change...)

            

%% compare sigma between stims

s0=[]; s1=[];
for cell_n = 1:length(results)
    for stim_n = 2:2 %size(results{cell_n},2)  % over cases; assumes only 2 stims
        if (isfield(results{cell_n}{1,stim_n},'gmodel')&&isfield(results{cell_n}{2,stim_n},'gmodel'))
            s0 = [s0 results{cell_n}{1,stim_n}.gmodel.b];
            s1 = [s1 results{cell_n}{2,stim_n}.gmodel.b];
        end %if
    end % stim_n
end %cell_n
scatter(s0,s1)
xlabel('\sigma stim 1')
ylabel('\sigma stim 2')
xlim([0 200])
ylim([0 100])
% again decent relation for small sigma... some differences; may be some
% stim dependence. Here is where model selection can work - 2 separate g
% models or a common one...
% 
% looks like this should not be happening, so check fitting, 
% the gain model should hopefully be a characteristic of the cell, not
% external condition.
%% compare g to pupil
% need pupil data too. now in results

fignum = 1;
subnum = 0;
cellok = 0;
p5 = '';
for cell_n = 1:length(results)
    for stim_n = 1:size(results{cell_n},1)
        for cond = 2:2  % let's do evoked only
            %try
            figure(fignum)  % another option - even figs for spont; odd figs for evoked
            figname = strcat('gVSpupil',num2str(fignum),'.pdf');
            orient tall
            subplot(4,2,subnum+stim_n)  % was +cond
            try
            %if (isfield(results{cell_n}{stim_n,cond},'gs'))
                cellok = 1;
                gs = results{cell_n}{stim_n,cond}.gs;
                pupils = pupil.mean{cell_n}(cond, :, stim_n)';
                ndx = find( (pupils > 0) & (~isnan(gs)) );  % remove 0 pupil and NaN g
                gs = gs(ndx); pupils = pupils(ndx);
                %[rx,mx,bx] = regression(results{cell_n}{stim_n,cond}.gs,pupil.mean{cell_n}(cond, :, stim_n));
                %rp = results{cell_n}{stim_n, cond}.r_gp;
                [rho, pval] = corr( gs, pupils);
                rp = [rho, pval];
                if rp(2)<0.05, p5='*'; else p5 = ''; end 
                scatter( pupil.mean{cell_n}(cond, :, stim_n), results{cell_n}{stim_n,cond}.gs);
                lsline
                % plotregression(pupils, gs,   strcat(cellids(cell_n),',',results{cell_n}{stim_n,cond}.label,',p(g)'));
                title(strcat(cellids(cell_n),', stim ', num2str(stim_n)))
                %title(strcat(cellids(cell_n),',',results{cell_n}{stim_n,cond}.label))%, ',',...
                %      num2str(cell_n),',', num2str(stim_n)))
                ylabel( strcat('g, r=', num2str(rp(1),2), p5 )) %', p=', num2str(rp(2)) ))
                %xlabel('pupil diam, mm')
                %pause
            catch
                cellok = 0;
            end %if/try
        end % cond
        % organize figures here if cond involved...
    end % stim_n
       % organize figures
    if cellok,  subnum = subnum + 2; cellok = 0; end
    if subnum>6
        subplot(4,2,7)
        xlabel('pupil diam, mm')
        subplot(4,2,8)
        xlabel('pupil diam, mm')
        subnum = 0; fignum = fignum+1; 
        saveas(gcf, figname); 
    end
end %cell_n
saveas(gcf, figname);
% even here can see some biomodal distribution in pupil diameter
                
%% estimate corr coeff b/w g and pupil size
% need pupil data too. now in results

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'gs'))
                try
                    [rho, pval] = corr( results{cell_n}{stim_n,cond}.gs, pupil.mean{cell_n}(cond, :, stim_n)' );
                    % I think they have to be in columns, and I think that will
                    % do it. Not for corrcoeff, use that
                    results{cell_n}{stim_n, cond}.r_gp = [rho, pval];
                catch
                    [cell_n stim_n cond]
                end
            end %if
        end % stim
    end % stim_n
end %cell_n
% even here can see some biomodal distribution in pupil diameter
save pupil_voc_10000_hz_results.mat results pupil cellids

%% review 
rhos = []
pvals = []

for cell_n = 1:length(results)
    for stim_n = 1:size(results{cell_n},1)
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'r_gp'))
                xx = results{cell_n}{stim_n,cond}.r_gp;
                rhos = [rhos xx(1)];
                pvals = [pvals xx(2)];
            end %if
        end % stim
    end % stim_n
end %cell_n

plot(rhos, 'x')
hold on
plot(rhos(pvals<0.05), 'rx')
hold off
title('Correclation coefficient between pupil diameter and inferred g')
xlabel('significant ones are in red')

%% relate significant to cell id-s

summary={};
p_crit = 0.05;
for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'r_gp'))
                xx = results{cell_n}{stim_n,cond}.r_gp;
                %if xx(2)<p_crit
                    summary{end+1}.coord = [cell_n stim_n cond];
                    summary{end}.id = strcat(cellids{cell_n}, '-', results{cell_n}{stim_n,cond}.label);
                    summary{end}.rho = xx(1);
                    summary{end}.pval = xx(2);
                %end %if xx
            end %if isfield
        end % cond
    end % stim_n
end %cell_n
%% compare sigma_g to sigma_pupil

sigma{1}.g = [];
sigma{1}.p = [];
sigma{2}.g = [];
sigma{2}.p = [];
% average over stimuli

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'gmodel'))
                sigma{cond}.g = [sigma{cond}.g results{cell_n}{stim_n,cond}.gmodel.b];
                sigma{cond}.p = [sigma{cond}.p std( nonzeros(pupil.mean{cell_n}(cond, :, stim_n)))];
                % may not be that easy, since at least for first pupil
                % distribution is bimodal... and very large 0.
                % ? could g-s be bimodal as well? different latent gain
                % model, maybe mixture of high and low sigma.
                % 0 is sometimes a special indicator of 'can't register'
                % ignore for now; may do more elaborate processing later -
                % some distribution around 'small'
            end %if
        end % stim
    end % stim_n
end %cell_n

%%
% clean outliers
sigma{1}.g(sigma{1}.g>2)=NaN;
sigma{2}.g(sigma{2}.g>2)=NaN;


figure(1)
subplot(2,1,1)
scatter(sigma{1}.p, sigma{1}.g)
title('spontaneous')
xlabel('\sigma pupil')
ylabel('\sigma gain')
subplot(2,1,2)
scatter(sigma{2}.p, sigma{2}.g)
title('evoked')
xlabel('\sigma pupil')
ylabel('\sigma gain')

% gain: s1(22) and s2(13) large outliers (check which cell...)
% interesting, in spontaneous there is some trend.
% in evoked - maybe the opposite, but a few pops in the middle...
%
% a bit better after ignoring 0-s

%% visualize g distribution

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'gmodel'))
                gmodel = results{cell_n}{stim_n,cond}.gmodel;
                % plotrange = icdf(gmodel,[.025, 0975]);
                x = 0:0.05:5;
                plot(x,pdf(gmodel,x))
                title(gmodel.b)
                pause
            end
        end
    end
end
% hmm, most of variances pretty large, not a nice hump around 1, more like
% exponential with mean 1. try some censoring - remove traces with very low
% likelihood, re-estimate. Up to ? 5-10% of data...
       

%% visualize fits

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'gs'))
                figure(1)
                clf
                subplot(2,1,1)
                show_counts(results{cell_n}{stim_n,cond}.st,results{cell_n}{stim_n,cond}.gs,false)
                title(strcat(cellids(cell_n),',',results{cell_n}{stim_n,cond}.label, ',',...
                    num2str(cell_n),',', num2str(stim_n)))
                subplot(2,1,2)
                show_counts(results{cell_n}{stim_n,cond}.st,results{cell_n}{stim_n,cond}.gs,true)
                title('scaled by gain')

                % figure 2. model plot
                % slmengine makes a new figure. so close the current one.
                figure(2)
                close
                plotslm(results{cell_n}{stim_n,cond}.smodel,'dy')
                % seems to be working ok. forgot that I showed the rate, so it was flat
                % (constant)


                % plot of g-s
                figure(3)
                plot(results{cell_n}{stim_n,cond}.gs, 'd')
                title('g vs trial number spontaneous')
                %show_residuals(st1, smodel)
                % good, it looks like expansion of trajectories is as expected...

                % get sigma for g distribution (geometric)
                figure(4)
                subplot(2,1,1)
                plot(results{cell_n}{stim_n,cond}.x.all_t, results{cell_n}{stim_n,cond}.x.dn2,'.')
                subplot(2,1,2)
                plot(results{cell_n}{stim_n,cond}.x.all_t, results{cell_n}{stim_n,cond}.x.gs_res,'.')
                ylim([-5,20])
                title(strcat( '\sigma^2 = ',num2str(results{cell_n}{stim_n,cond}.gmodel.b)))

                pause
            end
        end
    end
end