function ll = l_pgmodel(g, ti, smodel, gmodel)
% function ll = l_pgmodel(g, spiketimes, smodel, gmodel)
% is a (mean, per sample) likelihood function of g, depending on 
% spiketimes - a vector! of spike times (so use times{n} if in an array)
% smodel - a model for the mean, currently assumung slm structure
% gmodel - a model for the g
%
% ll to be maximized for inferred  g under the gmodel

% would be helpful to provide the derivative d/dg of this function


ni = 1:length(ti); % those are the inferred counts.
% the same as the index, but keep separate in case structure changes later
% on.
mu_t = slmeval(ti,smodel); % those are the poisson model means for those times.

ll=0; % log likelihood accumulator
for i1 = 1:length(ti)
    ll = ll+log( pdf('Poisson', ni(i1) ,g*mu_t(i1)) ) + log(pdf(gmodel,g));
    % add the ll coming from the poisson distribution with given scaled mean at
    % time ti, g*m
    % to that I need to add the ll coming from the g distribution, with
    % free g, which has to be optimized. make it a function, and try a
    % bunch of g-s
    % good, already vectorized.
end
ll = exp(ll/length(ti));