%% analysis of cell data

Fs = 10000; % Hz samling rate
T_all=5500; % ms
T_end=5000; % ms, end of sound
sound_onset = 2000; % ms

T_stim(1)=sound_onset; %ms, length of spontaneous region
T_stim(2) = T_end - sound_onset;


% spontaneous/evoked model structure
knot(2)=round((T_end - sound_onset)/100);
knot(1) = 1; % for spontaneous rate model

repeats(1) = 0;
repeats(2) = 6; % for some non-uniform binning; no need for spont, since linear model

type_label{1} = 'spontaneous';
type_label{2} = 'evoked';

clear results
cell_n = 2; stim_n = 1; i1 = 1;
% for cell_n = 1:length(rs)
%     for stim_n = 1:length(rs{cell_n})      
        % spontaneous
        st{1} = raster2cell(rs{cell_n}{stim_n},Fs, 0, sound_onset);
        % evoked
        st{2} = raster2cell(rs{cell_n}{stim_n},Fs, sound_onset, T_end);
%         for i1 = 1:2 %spontaneous/evoked models
            sprintf('trying cell %d stim %d, %s', cell_n, stim_n, type_label{i1})
%            try
            % models
            smodel=c_rate_model(st{i1}, T_stim(i1), min(knot(i1),length(st{i1})/3), repeats(i1));
            % change knots to respect data size; fewer knots if little
            % data. see if that helps. nope. dig deeper
            [s2_g, x.all_t, x.gs_res, x.dn2]  = gs_model(st{i1}, smodel);
            % ok, so some of the drawbacks of the current mean model is that it is
            % trying to fit the end points too hard, and because of that may make
            % some very sharp transitions there. And that will put more nodes in
            % that vicinity, which may exacerbate the problem.
            %
            % see if there is a parameters that can tempre this behaviors.
            % anothe option - use a different basis, maybe hermit over the whole
            % time range...

            % infer g-s
            gmodel = makedist('Gamma','a',1/s2_g,'b',s2_g);
            %g_95 = icdf(gmodel, [0.05, 1-0.05]);
            gs = infer_gs(st{i1}, smodel, gmodel);

            % assign to structure
            results{cell_n}{stim_n,i1}.label = type_label(i1);
            results{cell_n}{stim_n,i1}.st = st{i1};
            results{cell_n}{stim_n,i1}.smodel = smodel;
            results{cell_n}{stim_n,i1}.gmodel = gmodel;
            results{cell_n}{stim_n,i1}.gs = gs;
            results{cell_n}{stim_n,i1}.x = x; % extra info for diagnostics
%            catch ME
%                ME
%                results{cell_n}{stim_n,i1}.ME = ME;
%            end % catch
%         end % i1
%     end % stim_n
% end % cell_n

% save results, and include pupil data.

%save pupil_voc_10000_hz_results.mat results pupil cellids

% pupil: A cell array of pupil diameter measurements (in mm). Each measurement 
% has the dimensions epoch x repetition x stimulus. The "epoch" dimension c
% ontains two positions: the mean pupil size during the prestimulus period, 
% followed by the mean size during the stimulus.

%% check for error sites, try to improve. needs results
for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if (isfield(results{cell_n}{stim_n,cond},'ME'))
                [cell_n, stim_n, cond]
                results{cell_n}{stim_n,cond}.ME.message
            end
        end
    end
end
%% compare sigma between conditions

s0=[]; s1=[];
for cell_n = 1:length(rs)
    for stim_n = 1:length(rs{cell_n})
        if ~(isfield(results{cell_n}{stim_n,1},'ME')||isfield(results{cell_n}{stim_n,2},'ME'))
            s0 = [s0 results{cell_n}{stim_n,1}.gmodel.b];
            s1 = [s1 results{cell_n}{stim_n,2}.gmodel.b];
        end %if
    end % stim_n
end %cell_n
scatter(s0,s1)
xlim([0 20])
ylim([0 20])
% not much relation between the 2 sigma
            
%% compare g to pupil
% need pupil data too. now in results

for cell_n = 1:length(rs)
    for stim_n = 1:length(rs{cell_n})
        for cond = 1:2
            if ~(isfield(results{cell_n}{stim_n,cond},'ME'))
                scatter(results{cell_n}{stim_n,cond}.gs, pupil{cell_n}(cond, :, stim_n) );
                title(strcat(cellids(cell_n,:),',',results{cell_n}{stim_n,cond}.label, ',',...
    num2str(cell_n),',', num2str(stim_n)))
                xlabel('g (scale)')
                ylabel('pupil diam, mm')
                pause
            end %if
        end % stim
    end % stim_n
end %cell_n
                
%% compare sigma_g to sigma_pupil

sigma{1}.g = [];
sigma{1}.p = [];
sigma{2}.g = [];
sigma{2}.p = [];
% average over stimuli

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        for cond = 1:2
            if ~(isfield(results{cell_n}{stim_n,cond},'ME'))
                sigma{cond}.g = [sigma{cond}.g results{cell_n}{stim_n,cond}.gmodel.b];
                sigma{cond}.p = [sigma{cond}.p std( nonzeros(pupil{cell_n}(cond, :, stim_n)))];
                % may not be that easy, since at least for first pupil
                % distribution is bimodal... and very large 0.
                % ? could g-s be bimodal as well? different latent gain
                % model, maybe mixture of high and low sigma.
                % 0 is sometimes a special indicator of 'can't register'
                % ignore for now; may do more elaborate processing later -
                % some distribution around 'small'
            end %if
        end % stim
    end % stim_n
end %cell_n

%%
% clean outliers
sigma{1}.g(sigma{1}.g>1e3)=NaN;
sigma{2}.g(sigma{2}.g>1e3)=NaN;


figure(1)
subplot(2,1,1)
scatter(sigma{1}.p, sigma{1}.g)
title('spontaneous')
xlabel('\sigma pupil')
ylabel('\sigma gain')
subplot(2,1,2)
scatter(sigma{2}.p, sigma{2}.g)
title('evoked')
xlabel('\sigma pupil')
ylabel('\sigma gain')

% gain: s1(22) and s2(13) large outliers (check which cell...)
% interesting, in spontaneous there is some trend.
% in evoked - maybe the opposite, but a few pops in the middle...
%
% a bit better after ignoring 0-s

                               
                