function p = gamma_pdf(gmodel,x)
% function p = gamma_pdf(gmodel,x)
% input
% gmodel.a and gmodel.b shape parameters;
% x is a vector of values
% output
% p is the probability density for the corresponding x

p = zeros(size(x));
ndx = find(x>0);
a = gmodel.a;
b = gmodel.b;

p(ndx) = ( b^(-a).* exp(-(x(ndx)./b)).* x(ndx).^(-1 + a))./gamma(a);