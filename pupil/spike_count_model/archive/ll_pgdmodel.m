function ll = ll_pgdmodel(g, ti, smodel, gmodel)
% function ll = ll_pgmodel(g, spiketimes, smodel, gmodel)
% is a (mean, per sample) log-likelihood function of g, depending on 
% spiketimes - a vector! of spike times (so use times{n} if in an array)
% smodel - a model for the mean, currently assumung slm structure
% gmodel - a model for the g
%
% ll to be maximized for inferred  g under the gmodel

% would be helpful to provide the derivative d/dg of this function


ni = 1:length(ti); % those are the inferred counts.
% the same as the index, but keep separate in case structure changes later
% on.
mu_t = smodel(ti); %slmeval(ti,smodel); % those are the poisson model means for those times.

ll=zeros(length(g)); % log likelihood accumulator
% !! what if g is vectorized?
for i1 = 1:length(ti)
    % I need to run a loop, since the mean changes from time to time. can't
    % do a vector n input; mu is not fixed.
    for g1 = 1:length(ll)
        ll(g1) = ll(g1)+log( pdf('Poisson', ni(i1) ,g(g1)*mu_t(i1)) ) + log(pdf(gmodel,g(g1)));
    end %g1
    % add the ll coming from the poisson distribution with given scaled mean at
    % time ti, g*m
    % to that I need to add the ll coming from the g distribution, with
    % free g, which has to be optimized. make it a function, and try a
    % bunch of g-s
    % good, already vectorized.
end
ll = ll/length(ti);