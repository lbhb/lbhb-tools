%% analysis of cell data
% needs rs loaded. load results first if want to continue...
% load('pupil_voc_10000_hz_sp.mat')
Fs = 10000; % Hz samling rate
T_all=5500; % ms
T_end=5000; % ms, end of sound
sound_onset = 2000; % ms

T_stim(1)=sound_onset; %ms, length of spontaneous region
T_stim(2) = T_end - sound_onset;


% spontaneous/evoked model structure
%knot(2)=round((T_end - sound_onset)/100);
%knot(1) = 1; % for spontaneous rate model

%repeats(1) = 0;
%repeats(2) = 6; % for some non-uniform binning; no need for spont, since linear model

type_label{1} = 'spontaneous';
type_label{2} = 'evoked';

% clear results. or could try try:catch structure
results = {};
% if ~exist('results','var')
%     results = {}
% end
% 
%start_n = length(results)+1;
%     

for cell_n = 1:length(rs)
    for stim_n = 1:length(rs{cell_n})      
        % spontaneous
        st{1} = raster2cell(rs{cell_n}{stim_n},Fs, 0, sound_onset);
        % there seem to be some weird errors that fill a whole line with
        % spikes. check why. 7,2; 42,1;x 46, 2;x 47, 2;x 48, 1; 56, 2;x 57, 2;x 58,
        % 2; 
        % 47, 2 has another largish outlier.
        % not sure how the rest of the analysis will take empty trials (no
        % spikes)
        i1 = 1;
        dmodel %script
    end % stim_n
end % cell_n
% save results, and include pupil data.

save pupil_voc_10000_hz_spont_results.mat results pupil cellids

% pupil: A cell array of pupil diameter measurements (in mm). Each measurement 
% has the dimensions epoch x repetition x stimulus. The "epoch" dimension c
% ontains two positions: the mean pupil size during the prestimulus period, 
% followed by the mean size during the stimulus.

%% quality of fits
% load('pupil_voc_10000_hz_spont_results.mat')
times = 1:10:2000;
for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        [cell_n stim_n]
        try
            show_counts(results{cell_n}{stim_n}.st)
            hold on
            plot(times, results{cell_n}{stim_n}.smodel(times), 'r-', 'LineWidth', 2)
            hold off
            title(strcat(cellids{cell_n},', stim=',num2str(stim_n)))
            pause
        catch
            strcat('error_', cellids{cell_n},', stim=',num2str(stim_n))
        end %try 
    end
end

%% compare sigma between stims (should be the same where it exists
s0=[]; s1=[];
for cell_n = 1:length(results)
    %for stim_n = 1:length(results{cell_n})  % over stims; assumes only 2 stims
        try
            gs0=results{cell_n}{1}.gmodel.b;
            gs1=results{cell_n}{2}.gmodel.b;
            s0 = [s0 gs0];
            s1 = [s1 gs1];
        catch
            strcat('error_', cellids{cell_n})
            % some model errors still exist: 55, zee019d-b1 and 56, zee019e-a1
            % s2_g estimate negative for those; see why.
        end %if
    %end % stim_n
end %cell_n
scatter(s0,s1)
xlabel('\sigma stim 1')
ylabel('\sigma stim 2')
xlim([0 20])
ylim([0 20])
% hmm, again not much relation between the 2 sigma. some outliers to take care of.
% looks like this should not be happening, so check fitting, 
% the gain model should hopefully be a characteristic of the cell, not
% external condition.
% mean model looks ok visually. So estimation of gain model questionable..
% for small sigma things seem to be ok. So main problem seems to be large sigma est, which probably wrong.
%
% ok, cool, after initial 200ms drop (try 300ms), small sigma are strongly
% correlated... Some obvious problems with large sigma, so no worry there
% (means, model type doesn't work very well)

%% check smodel residuals for gmodel

for cell_n = 1:length(results)
    for stim_n = 1:length(results{cell_n})
        [cell_n stim_n]
        try
            all_t = results{cell_n}{stim_n}.x.all_t;
            dn2 = results{cell_n}{stim_n}.x.dn2;
            gs_res = results{cell_n}{stim_n}.x.gs_res;
            subplot(2,1,1)
            plot(all_t, dn2,'.')
            title(strcat('\sigma=',num2str(results{cell_n}{stim_n}.gmodel.b)))
            subplot(2,1,2)
            plot(all_t, gs_res,'.')
            ylim([-5,20])
            pause
        catch
            strcat('error_', cellids{cell_n},', stim=',num2str(stim_n))
        end %try 
    end
end
% initial bias again. start estimating from 200ms or so?
% issues: 2 2; 7 1 (milder); 8 2 (m); 192 1-2; 

%% compare g to pupil
% need pupil data too. now in results

fignum = 1;
subnum = 0;
cellok = 0;
p5 = '';
% there may be error shere because I am removing some cells from
% raster2cell. and because some gs are NaN. See what remains for now
% new format - cells are rows; cols are stim.
%

for cell_n = 1:length(results)
    for stim_n = 1:2
        %try
        cond=1; %spontaneous only
        figure(fignum)
        figname = strcat('dgVSpupil',num2str(fignum),'.pdf');
        orient tall
        subplot(4,2,subnum+stim_n)
        try
        %if (isfield(results{cell_n}{stim_n,cond},'gs'))
            cellok = 1;
            gs = results{cell_n}{stim_n}.gs;
            pupils = pupil.mean{cell_n}(cond, :, stim_n)';
            ndx = find( (pupils > 0) & (~isnan(gs)) );  % remove 0 pupil and NaN g
            gs = gs(ndx); pupils = pupils(ndx);
            %[rx,mx,bx] = regression(results{cell_n}{stim_n,cond}.gs,pupil.mean{cell_n}(cond, :, stim_n));
            % rp = results{cell_n}{stim_n, cond}.r_gp; rho on the fly
            %ndx = find(~isnan(gs)));
            [rho, pval] = corr( gs, pupils);
            rp = [rho, pval];
            % NaN appears as no corr estimate; remove NaN-s to deal with it...
            if rp(2)<0.05, p5='*'; else p5 = ''; end 
            scatter( pupil.mean{cell_n}(cond, :, stim_n), results{cell_n}{stim_n,cond}.gs);
            lsline
            % plotregression(pupils, gs,   strcat(cellids(cell_n),',',results{cell_n}{stim_n,cond}.label,',p(g)'));
            title(strcat(cellids(cell_n),', stim ', num2str(stim_n)))
            %,results{cell_n}{stim_n,cond}.label))%, ',',...
            %      num2str(cell_n),',', num2str(stim_n)))
            ylabel( strcat('g, r=', num2str(rp(1),2), p5 )) %', p=', num2str(rp(2)) ))
            %xlabel('pupil diam, mm')
            %pause
        catch
            cellok = 0;
        end %if/try
    end % stim_n
       % organize figures
    if cellok,  subnum = subnum + 2; cellok = 0; end
    if subnum>6
        subplot(4,2,7)
        xlabel('pupil diam, mm')
        subplot(4,2,8)
        xlabel('pupil diam, mm')
        subnum = 0; fignum = fignum+1; 
        saveas(gcf, figname); 
    end
end %cell_n
saveas(gcf, figname);
% even here can see some biomodal distribution in pupil diameter
