function [sg, all_t, gs_res, dn2]  = gs_model(spiketimes, smodel)
% function [gs, all_t, gs_res, dn2]  = gs_model(spiketimes, smodel)

for i1 = 1:length(spiketimes)
    spikecounts{i1} = 1:length(spiketimes{i1});
end

[all_t,ndx] = sort( cell2mat(spiketimes)); % ms.
%all_s = (1:length(all_t))'/length(spiketimes);
all_n = cell2mat(spikecounts);
all_n = all_n(ndx)'; % ' ? somehow swapped to row vector

mu = slmeval(all_t,smodel);
dn2 = (all_n-mu).^2;
gs_res = abs(dn2 - mu)./(mu.^2);
% abs residual. Should be positive, but gave errors a couple of times, so
% make positive while looking for a different estimator.

% initial drop in ms. Hmm, refactor, pass as param.
To = 300; %ms
ndx = find(all_t>To);
sg = mean(gs_res(ndx));
%sg_se = std(gs_res(ndx))/sqrt(length(ndx)-1); % standard error estimate