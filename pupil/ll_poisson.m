function r=ll_poisson(p,r)
% function r=ll_poisson(p,r);
%
% r = - nanmean(r.*log(p) - p) ./ (nanmean(r)*log(nanmean(r)));
% denominator normalizes result

p(p<0.001)=0.001;
%r = - nanmean(r.*log(p) - p) ./ (nanmean(r)*log(nanmean(r)));
r = - nanmean(r.*log(p) - p) ;
