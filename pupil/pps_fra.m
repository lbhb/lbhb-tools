function pps_fra(cellid,options)

if 0
   [~,cellids]=dbbatchcells(293);
   cellids=unique(cellids);
   close all
   for ii=71:90
      options=struct();
      %options.pupil_lowpass=0;
      %options.pupil_highpass=0.1;
      options.pupil_derivative='pos';
      pps_fra(cellids{ii},options);
      %options.pupil_lowpass=0.1;
      %options.pupil_highpass=0;
      %options.pupil_derivative='neg';
      %pps_fra(cellids{ii},options);
      %options.pupil_lowpass=0;
      %options.pupil_highpass=0;
      options.pupil_derivative='';
      pps_fra(cellids{ii},options);
   end
end

if ~exist('options','var')
   options=struct();
end

options.filtfmt=getparm(options,'filtfmt','parm');
options.chancount=getparm(options,'chancount',0);
options.rasterfs=getparm(options,'rasterfs',200);
options.usesorted=getparm(options,'usesorted',1);
options.datause=getparm(options,'datause','Reference Only');
options.runclass=getparm(options,'runclass','PPS');
options.pupil=getparm(options,'pupil',1);
options.pupil_highpass=getparm(options,'pupil_highpass',0);
options.pupil_lowpass=getparm(options,'pupil_lowpass',0);
options.recache=getparm(options,'recache',0);

[r,stim,stimparam,pr]=pps_load(cellid,options);
LoadMFile(stimparam.parmfiles{1});

realrepcount=size(r,2);
   
options.PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
options.PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;
options.ReferenceClass=exptparams.TrialObject.ReferenceClass;

PreBins=round(options.PreStimSilence.*options.rasterfs);
PostBins=round(options.PostStimSilence.*options.rasterfs);

pr0=pr;
if options.pupil,
   ss=sort(pr0(:));
   mm=ss(round(length(ss).*0.5));
   if mm==0 && ss(end)==0
      mm=max(ss(ss<0));
   end
   %mm=nanmedian(pr0(:));
   rset=ones(size(r));
   rset(pr0>mm)=2;
else
   rset=ones(size(r));
end

setcount=length(unique(rset(:)));


ustim=unique(stim(:));
ustim=flipud(ustim(ustim>0));
l_count=length(ustim);
freq_count=size(stim,2);

FRA=zeros(l_count,freq_count,setcount);
FRAe=zeros(l_count,freq_count,setcount);
rwin=round(0.01*options.rasterfs):round(0.04.*options.rasterfs);
cclag=10;
ccwin=zeros(cclag*2+1,setcount);
ccn=zeros(cclag*2+1,setcount);
for setidx=1:setcount,
   rtemp=r;
   rtemp(rset~=setidx)=nan;
   
   for ff=1:freq_count,
      for ll=1:l_count,
         dd=find(stim(:,ff)==ustim(ll));
         dd=dd(dd<=size(r,1)-max(rwin));
         iiset=[];
         for rr=1:length(rwin),
            iiset=cat(3,iiset,rtemp(dd+rwin(rr),:));
         end
         rr=nanmean(iiset,3);
         
         FRA(ll,ff,setidx)=nanmean(rr(:));
         FRAe(ll,ff,setidx)=nanstd(rr(:))./sqrt(sum(~isnan(rr(:))));
         
         for rr=1:length(ccwin),
            tdd=dd+rr-cclag-1;
            tdd=tdd(tdd>=1 & tdd<=size(rtemp,1));
            ccwin(rr,setidx)=ccwin(rr,setidx)+nansum(nansum(rtemp(tdd,:)));
            ccn(rr,setidx)=ccn(rr,setidx)+sum(sum(~isnan(rtemp(tdd,:))));
         end
      end
   end
end
ccwin=ccwin./ccn;

FRA=cat(3,FRA,FRA(:,:,2)-FRA(:,:,1));
setcount=setcount+1;

freqs=exptparams.TrialObject.ReferenceHandle.Frequencies;
levels=exptparams.TrialObject.OveralldB-...
   exptparams.TrialObject.ReferenceHandle.AttenuationLevels;

figure;
for setidx=1:setcount
   subplot(setcount,2,setidx*2-1);
   if setidx<setcount,
      clim=[min(min(min(FRA(:,:,1:(setcount-1))))) max(max(max(FRA(:,:,1:(setcount-1)))))];
   else
      clim=[-1 1].*max(max(abs(FRA(:,:,setcount))));
   end
   
   %tfra=imresize(FRA(:,:,setidx),2,'bilinear');
   tfra=FRA(:,:,setidx);
   imagesc(1:freq_count,1:l_count,tfra,clim);
   
   tfra=gsmooth(imresize(FRA(:,:,setidx),2,'bilinear'),1);
   hold on
   contour((0.5:0.5:freq_count)+0.25,(0.5:0.5:l_count)+0.25,tfra,clim(end)*0.5.*[1 1],...
      'k','LineWidth',1);
   hold off
   
   xt=get(gca,'XTick');
   set(gca,'XTick',xt,'XTickLabel',freqs(xt),'YTick',1:l_count,'YTickLabel',levels);
   
   colorbar;
   xlabel('Frequency');
   ylabel('Level (dB SPL)');
   if setidx<setcount
      ht=title(sprintf('%s medpup=%d',cellid,round(nanmean(pr0(rset==setidx)))));
   else
      ht=title(sprintf('%s difference',cellid));
   end
      
   axis square
   set(ht,'Interpreter','none');
end

subplot(4,2,2);
plim=[nanmin(pr0(:)) nanmax(pr0(:))];
pp=linspace(plim(1),plim(2),50);
n1=hist(pr0(rset==1),pp);
n2=hist(pr0(rset==2),pp);
bar(pp,[n1(:) n2(:)],'stacked');
aa=axis;
axis([plim(1)-1 plim(2)+5 aa(3:4)]);
xlabel('pupil diameter');
ylabel('bincount');
ts='';
if options.pupil_lowpass
   ts=strcat(ts,sprintf('lp=%.3f',options.pupil_lowpass));
end
if options.pupil_highpass
   ts=strcat(ts,sprintf(' hp=%.3f',options.pupil_highpass));
end
if options.pupil_derivative
   ts=strcat(ts,sprintf(' D=%s',options.pupil_derivative));
end
title(sprintf('pupil (%s)',ts));

subplot(4,2,4);
plot((-cclag:cclag)'./options.rasterfs,ccwin);
xlabel('time from pip onset (s)');
ylabel('mean spike rate (Hz)');
legend('small','big','Location','NorthWest');
legend boxoff

subplot(3,2,5);
mm=mean(mean(mean(FRA(:,:,(1:(setcount-1))))));
ff=gsmooth(mean(FRA(1,:,1:(setcount-1))-mm,3),0.5);
bfi=min(find(abs(ff)==max(abs(ff))));
errorbar(squeeze(FRA(1,:,1:(setcount-1))),squeeze(FRAe(1,:,1:(setcount-1))));
%xt=get(gca,'XTick');
%xt=xt(xt>0 & xt<=length(freqs));
set(gca,'XTick',xt,'XTickLabel',freqs(xt));
aa=axis;
axis([0.5 length(ff)+0.5 aa(3:4)]);
colorbar
axis square
xlabel('frequency (Hz)');
ylabel('mean spike rate');

subplot(3,2,6);
sm=FRA(:,:,1);
lg=FRA(:,:,2);
plot(sm(:),lg(:),'.');
hold on;
aa=axis;
plot([0 max(aa(3:4)) ],[0 max(aa(3:4)) ],'k--');
hold off
axis tight;

%errorbar(squeeze(FRA(:,bfi,1:(setcount-1))),squeeze(FRAe(:,bfi,1:(setcount-1))));
%set(gca,'XTick',1:length(levels),'XTickLabel',(levels));
%xlabel('level (dB SPL)');
%ylabel('mean spike rate');

fullpage portrait
set(gcf,'Name',sprintf('%s(%d)',cellid,realrepcount));
%colormap(1-gray);

