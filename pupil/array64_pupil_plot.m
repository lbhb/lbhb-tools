
narf_set_path;
dbopen;
subset=0;
fs=20;
%runclass='TOR';  fs=1000;
runclass='SPS';  fs=100;
%runclass='VOC'; subset=5;
%runclass='VOC'; subset=8;  fs=100;
%runclass='PPS';  fs=100;
%runclass='BNB';  fs=1000;

%analysis='strf'; 
analysis='decode';
%analysis='raster';
%analysis='psth';  fs=20;
%analysis='spont'; fs=20;
%analysis='evoked'; fs=20;

if strcmpi(runclass,'SPS'),
   modelname='env100_logn_adp1pc_fir15_siglog100_fit05a';
   strfbatch=283;
elseif strcmpi(runclass,'VOC'),
   modelname='fb18ch100_lognn_wcg02_adp1pc_fir15_siglog100_fit05h_fit05c';
   strfbatch=263;
end

batchid=283;
usesorted=0;
channelrange=1:64;

siteid='BOL005c'; usesorted=1;
layer4cells=24:41;
layer3cells=[42:49];
layer5cells=[4:23];
   
%siteid='BOL006b';  usesorted=0;

if usesorted,
   sql=['SELECT * FROM sRunData',...
      ' WHERE sRunData.batch=',num2str(batchid),...
      ' AND cellid like "',siteid,'%"',...
      ' ORDER BY cellid'];
   celldata=mysql(sql);

   cellids={celldata.cellid};
   siteid=strsep(cellids{1},'-');
   siteid=siteid{1};
else
   cellids={};
   for ii=channelrange,
      cellids=cat(2,cellids,sprintf('%s-%02d-1',siteid,channelrange(ii)));
   end
end
cellcount=length(cellids);

resp={};
pupil={};
strf={};
perf={};
iso=zeros(cellcount,1);

for ii=1:cellcount,
   cellid=cellids{ii};
   if usesorted,
   
      if exist('subset','var') && subset>0,
         [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass,'pupil',2,'Subsets',subset);
      else
         [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass,'pupil',2);
      end
      if isempty(cfd),
         [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass);
      end
      iso(ii)=cfd(1).isolation;
      
      spikefile = [cfd(1).path cfd(1).respfile];
   else
      sql=['SELECT resppath as stimpath,parmfile as stimfile,',...
         ' eyewin as pupil FROM gDataRaw WHERE cellid="',siteid,'"',...
         ' AND runclass like "%',runclass,'%" AND not(bad) and eyewin'];
      cfd=mysql(sql);
      if isempty(cfd),
         sql=['SELECT resppath as stimpath,parmfile as stimfile,',...
            ' eyewin as pupil FROM gDataRaw WHERE cellid="',siteid,'"',...
            ' AND runclass like "%',runclass,'%" AND not(bad)'];
         cfd=mysql(sql);
      end
      
   end
   parmfile = [cfd(1).stimpath cfd(1).stimfile];
   LoadMFile(parmfile)
   prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
   duration = exptparams.TrialObject.ReferenceHandle.Duration;
   poststim = exptparams.TrialObject.ReferenceHandle.PostStimSilence;
   PreStimBins=round(prestim.*fs);
   PostStimBins=round(poststim.*fs);
   
   if strcmpi(analysis,'strf') && ...
         (strcmpi(runclass,'SPS') || ...
         (strcmpi(runclass,'VOC') && subset==8)),
      
      % load from narf post-processed files
       strf(ii)=get_strf_from_model(strfbatch,{cellid},{modelname});
       if strcmpi(analysis,'decode'),
          % slow, 
          [pred{ii},resp{ii},pupil{ii},fs,perf{ii},stim0]=load_narf_pred_pupil(cellid,strfbatch,modelname);
          if ii==1,
             stimbytrial=stim0;
          end
       end
   else
      
      % load via baphy functions
      options = struct();
      options.runclass=runclass;
      options.rasterfs = fs;
      options.includeprestim = 1;
      pr_options = options;
      pr_options.pupil = 1;
      pr_options.pupil_median=1;
      pr_options.pupil_mm=0;
      pr_options.pupil_offset=0.75;
      
      r0=[];
      if ii==1, pr0=[]; end
      for fidx=1:1, % length(cfd),
         parmfile = [cfd(fidx).stimpath cfd(fidx).stimfile];
         
         r_options = options;
         
         %load spike raster
         if usesorted,
            spikefile = [cfd(fidx).path cfd(fidx).respfile];
            
            r_options.channel=cfd(fidx).channum;
            r_options.unit=cfd(fidx).unit;
            if ~isempty(cfd(fidx).goodtrials),
               r_options.trialrange=eval(cfd(fidx).goodtrials);
            end
            if strfbatch==263,
               r_options.tag_masks={'Reference1'};
               [r1,tags1,trialset1] = loadspikeraster(spikefile, r_options);
               r_options.tag_masks={'Reference2'};
               [r2,tags2,trialset2] = loadspikeraster(spikefile, r_options);
               r=cat(3,r1,r2);
               tags={tags1{:} tags2{:}};
               trialset=cat(2,trialset1,trialset2);
            else
               [r,tags,trialset] = loadspikeraster(spikefile, r_options);
            end
         else
            r_options.channel=channelrange(ii);
            [r,tags,trialset] = loadevpraster(parmfile,r_options);
         end
         r0=cat(2,r0,r);
         
         %load pupil diameter raster
         if ii==1 && cfd(fidx).pupil>=2,
            if strfbatch==263,
               pr_options.tag_masks={'Reference1'};
               [pupil1, ptags1, ptrialset1] = loadevpraster(parmfile, pr_options);
               pr_options.tag_masks={'Reference2'};
               [pupil2, ptags2, ptrialset2] = loadevpraster(parmfile, pr_options);
               p=cat(3,pupil1,pupil2);
               ptags=cat(2,ptags1,ptags2);
               ptrialset=cat(2,ptrialset1,ptrialset2);
            else
               [p,ptags,ptrialset] = loadevpraster(parmfile, pr_options);
            end
            pr0=cat(2,pr0,p);
         end
         
         if ii==1,
            if strcmpi(analysis,'strf') && strcmpi(runclass,'PPS'),
               % load PPS spectrogram
               disp('Loading stimulus spectrogram...');
               [stim,stimparam]=loadstimfrombaphy(parmfile,[],[],'parm',fs,0,0,0);
            elseif strcmpi(analysis,'decode'),
               % load other spectrogram
               if strfbatch==283,
                  [stim,stimparam]=loadstimfrombaphy(parmfile,[],[], 'envelope',fs,0,0,1);
               elseif strfbatch==263,
                  [stim1,stimparam] = loadstimfrombaphy(...
                     parmfile, [], [], 'ozgf', fs, 18, 0, 1);
                  [stim2] = loadstimfrombaphy(...
                     parmfile, [], [], 'ozgf', fs, 18, 0, 1,'TargetHandle');
                  stim=cat(3,stim1,stim2);
               else
                  [stim,stimparam]=loadstimfrombaphy(parmfile,[],[], 'ozgf',fs,18,0,1);
               end
            end
         end
      end
      resp{ii}=r0*r_options.rasterfs;
      pupil{ii}=pr0;
   end
end

locolor=[.5 .5 .5];
hicolor=[0 0 0];
lofitcolor=[0 0 0.8];
hifitcolor=[0.8 0.2 .2];


%% decoding analysis
% 
if strcmpi(analysis,'decode'),
   
   rnorm={};
   betas=zeros(4,cellcount);
   for ii=1:cellcount
      r=resp{ii};
      p=pupil{ii};
      
      m0=nanmean(nanmean(nanmean(r((PreStimBins+1):(end-PostStimBins),:,:))));
      r=r-m0;
      
      rreps=squeeze(sum(~isnan(r(1,:,:))));
      ridx=find(rreps==max(rreps));
      
      tr=r((PreStimBins+1):(end-PostStimBins),:,ridx);
      psth=repmat(mean(tr,2),[1 rreps(1) 1]);
      pr=p((PreStimBins+1):(end-PostStimBins),:,ridx);
      X=[ones(size(pr(:))) pr(:) psth(:) psth(:).*pr(:)];
      Y=tr(:)-psth(:);
      [b,bint]=regress(Y,X);
      sigbins=bint(:,1).*bint(:,2)>0;
      if ~sum(sigbins>0),
         % do nothing
         b(:)=0;
      else
         if sum(~sigbins)>0,
            X=X(:,find(sigbins));
            [bn,bintn]=regress(Y,X);
            b(:)=0;
            bint(:)=0;
            b(find(sigbins))=bn;
            bint(find(sigbins),:)=bintn;
         end
         r=r-b(1)-b(2).*p;
         if b(4),
            r=r./(1+b(3)+b(4).*p);
         end
         
      end
      rnorm{ii}=r;
      betas(:,ii)=b;
   end

   cidx=find(sum(abs(betas))>0);
   cidx=1:cellcount;
 
   fprintf('length(cidx)=%d\n',length(cidx));
   r1=permute(cat(4,resp{cidx}),[1 4 2 3]);
   r2=permute(cat(4,rnorm{cidx}),[1 4 2 3]);
   stim0=repmat(permute(stim,[1 2 4 3]),[1 1 size(r1,3) 1]);
   kk=find(~isnan(r1(1,1,:)));
   r1=r1(:,:,kk);
   r2=r2(:,:,kk);
   stim0=stim0(:,:,kk);
   
   if strfbatch==263,
      cleantrials=find(abs(stim0(7,1,:))<max(abs(stim0(7,1,:)))/20);
      noisetrials=setdiff((1:size(stim0,3))',cleantrials);
   elseif strfbatch==283,
      stim0=sum(stim0,1);
   end

   sm=2;
   if sm>1,
      % smooth and downsample
      respfilt=ones(sm,1,1)./sm;
      stimfilt=ones(1,sm,1)./sm;
      r1=convn(r1,respfilt,'same');
      r2=convn(r2,respfilt,'same');
      stim0=convn(stim0,stimfilt,'same');
      r1=r1(1:sm:end,:,:);
      r2=r2(1:sm:end,:,:);
      stim0=stim0(:,1:sm:end,:);
   end

   params.maxlag=[-4 8];
   params.jackcount=12;
   params.trimbins=PreStimBins/sm;
   rr=(params.trimbins+1):(size(r1,1)-params.trimbins);
   
   [vstim1,~,estim1]=medium_recon(r1,stim0,r2,params);
   [vstim2,~,estim2]=medium_recon(r2,stim0,r1,params);
   chancount=size(stim0,1);
   vmse=zeros(chancount,2);
   vxc=zeros(chancount,2);
   
   for cc=1:chancount,
      s1=estim1(cc,rr,:);s2=stim0(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmse(cc,1)=mserr(s1(:),s2(:));
      vxc(cc,1)=xcov(s1(:),s2(:),0,'coeff');
      s1=estim2(cc,rr,:);s2=stim0(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmse(cc,2)=mserr(s1(:),s2(:));
      vxc(cc,2)=xcov(s1(:),s2(:),0,'coeff');
   end


return
   
   perfmtx=cat(1,perf{:});
   predcells=find(perfmtx(:,1)>0.3);
   
   goodcells=predcells;
   
   %goodcells=intersect(predcells,layer5cells);
   %goodcells=intersect(predcells,layer4cells);
   
   % non-gain cells:
   %goodcells=[9:19 45:55];
   
   % gain cells:
   %goodcells=[20:26 29:43];

   
   r0=cat(3,resp{goodcells});
   r0=permute(r0,[1 3 2]);
   p=nanmean(pupil{1})./25; %approx pix2mm conversion
   p=rand(size(p));
   
   s0=sqrt(stimbytrial);
   
   if strfbatch==263,
      cleantrials=find(abs(stimbytrial(7,1,:))<max(abs(stimbytrial(7,1,:)))/20);
      noisetrials=setdiff((1:size(stimbytrial,3))',cleantrials);
%        lotrials=noisetrials;
%        hitrials=cleantrials;
%       lotrials=[cleantrials(find(p(cleantrials)<median(p(cleantrials))));
%          noisetrials(find(p(noisetrials)<median(p(noisetrials))))];
%       hitrials=[cleantrials(find(p(cleantrials)>=median(p(cleantrials))));
%          noisetrials(find(p(noisetrials)>=median(p(noisetrials))))];
%       lotrials=cleantrials(p(cleantrials)<median(p(cleantrials)));
%       hitrials=cleantrials(p(cleantrials)>=median(p(cleantrials)));
%        lotrials=noisetrials(p(noisetrials)<median(p(noisetrials)));
%        hitrials=noisetrials(p(noisetrials)>=median(p(noisetrials)));
      lotrials=find(p<median(p));
      hitrials=find(p>=median(p));
      chrange=6:15;
      s0=s0(6:15,:,:);
   else
      lotrials=find(p<median(p));
      hitrials=find(p>=median(p));
      chrange=1;
      s0=s0(chrange,:,:);
   end
   
   stimlo=s0(:,:,lotrials);
   stimhi=s0(:,:,hitrials);
   resplo=r0(:,:,lotrials);
   resphi=r0(:,:,hitrials);
   % shuffle test for random baseline
%    resplo=r0(:,:,hitrials);
%    resphi=r0(:,:,lotrials);
   
   sm=2;
   if sm>1,
      % smooth and downsample
      respfilt=ones(sm,1,1)./sm;
      stimfilt=ones(1,sm,1)./sm;
      resplo=convn(resplo,respfilt,'same');
      resphi=convn(resphi,respfilt,'same');
      stimlo=convn(stimlo,stimfilt,'same');
      stimhi=convn(stimhi,stimfilt,'same');
      resplo=resplo(1:sm:end,:,:);
      resphi=resphi(1:sm:end,:,:);
      stimlo=stimlo(:,1:sm:end,:);
      stimhi=stimhi(:,1:sm:end,:);
   end
   
   params.maxlag=[-4 8];
   params.jackcount=12;
   params.trimbins=15;
   rr=(params.trimbins+1):(size(resplo,1)-params.trimbins);
   
   [vstim,xcperchan,estim]=medium_recon(resplo,stimlo,resphi,params);
   chancount=size(stimhi,1);
   emselo=zeros(chancount,1);
   vmselo=zeros(chancount,1);
   exclo=zeros(chancount,1);
   vxclo=zeros(chancount,1);
   
%    pmedlo=median(p(lotrials));
%    pmedhi=median(p(hitrials));
%    smlotrials=(p(lotrials)<pmedlo);
%    lglotrials=(p(lotrials)>=pmedlo);
%    smhitrials=(p(hitrials)<pmedhi);
%    lghitrials=(p(hitrials)>=pmedhi);
%    
   for cc=1:chancount,
      s1=estim(cc,rr,:);s2=stimlo(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      emselo(cc)=mserr(s1(:),s2(:));
      exclo(cc)=xcov(s1(:),s2(:),0,'coeff');
      
      s1=vstim(cc,rr,:);s2=stimhi(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmselo(cc)=mserr(s1(:),s2(:));
      vxclo(cc)=xcov(s1(:),s2(:),0,'coeff');
      
%       s1=estim(cc,rr,smlotrials);s2=stimlo(cc,rr,smlotrials);
%       excsmlo=xcov(s1(:),s2(:),0,'coeff');
%       s1=estim(cc,rr,lglotrials);s2=stimlo(cc,rr,lglotrials);
%       exclglo=xcov(s1(:),s2(:),0,'coeff');
%       
%       s1=vstim(cc,rr,smhitrials);s2=stimhi(cc,rr,smhitrials);
%       excsmhi=xcov(s1(:),s2(:),0,'coeff');
%       s1=vstim(cc,rr,lghitrials);s2=stimhi(cc,rr,lghitrials);
%       exclghi=xcov(s1(:),s2(:),0,'coeff');
%  
   end
   
   figure;
   
   ccplot=1;
   subplot(2,3,1);
   tt=(rr.*sm-PreStimBins)./fs;
   
   hl=plot(tt,[mean(estim(ccplot,rr,:),3)' mean(stimlo(ccplot,rr,:),3)']);
   set(hl(1),'Color',lofitcolor,'LineWidth',1);
   set(hl(2),'Color',locolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('c=%d fitlo-predlo E=%.3f xc=%.3f',...
      chrange(ccplot),emselo(ccplot),exclo(ccplot)));
   
   subplot(2,3,2);
   hl=plot(tt,[mean(vstim(ccplot,rr,:),3)' mean(stimhi(ccplot,rr,:),3)']);
   set(hl(1),'Color',lofitcolor,'LineWidth',1);
   set(hl(2),'Color',hicolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('c=%d fitlo-predhi E=%.3f xc=%.3f',...
      chrange(ccplot),vmselo(ccplot),vxclo(ccplot)));
   
   subplot(2,3,3);
   if length(chrange)==1,
      xx=linspace(2,3.5,20);
      n1=hist(p(lotrials),xx);
      n2=hist(p(hitrials),xx);
      hb=bar(xx(:),[n1(:) n2(:)],'stacked');
      set(hb(1),'FaceColor',locolor,'LineStyle','none');
      set(hb(2),'FaceColor',hicolor,'LineStyle','none');
      aa=axis;
      axis([2 3.5 aa(3:4)]);
   else
      plot(chrange,[exclo vxclo]);
      legend('fit lo, pred lo','fit lo pred hi');
   end   
  
   [vstim,xcperchan,estim]=medium_recon(resphi,stimhi,resplo,params);
   chancount=size(stimlo,1);
   emsehi=zeros(chancount,1);
   vmsehi=zeros(chancount,1);
   exchi=zeros(chancount,1);
   vxchi=zeros(chancount,1);
   for cc=1:chancount,
      s1=estim(cc,rr,:);s2=stimhi(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      emsehi(cc)=mserr(s1(:),s2(:));
      exchi(cc)=xcov(s1(:),s2(:),0,'coeff');
      
      s1=vstim(cc,rr,:);s2=stimlo(cc,rr,:);
      s1=s1(:)-mean(s1(:));s2=s2(:)-mean(s2(:));
      s1=s1(:)./std(s1(:));s2=s2(:)./std(s2(:));
      vmsehi(cc)=mserr(s1(:),s2(:),1);
      vxchi(cc)=xcov(s1(:),s2(:),0,'coeff');
%       
%       s1=estim(cc,rr,smhitrials);s2=stimhi(cc,rr,smhitrials);
%       excsmhi=xcov(s1(:),s2(:),0,'coeff');
%       s1=estim(cc,rr,lghitrials);s2=stimhi(cc,rr,lghitrials);
%       exclghi=xcov(s1(:),s2(:),0,'coeff');
%       
%       s1=vstim(cc,rr,smlotrials);s2=stimlo(cc,rr,smlotrials);
%       excsmlo=xcov(s1(:),s2(:),0,'coeff');
%       s1=vstim(cc,rr,lglotrials);s2=stimlo(cc,rr,lglotrials);
%       exclglo=xcov(s1(:),s2(:),0,'coeff');
%  
   end
   
   subplot(2,3,4);
   hl=plot(tt,[mean(vstim(ccplot,rr,:),3)' mean(stimlo(ccplot,rr,:),3)']);
   set(hl(1),'Color',hifitcolor,'LineWidth',1);
   set(hl(2),'Color',locolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('fit hi, pred lo E=%.3f xc=%.3f',vmsehi(ccplot),vxchi(ccplot)));
   
   subplot(2,3,5);
   hl=plot(tt,[mean(estim(ccplot,rr,:),3)' mean(stimhi(ccplot,rr,:),3)']);
   set(hl(1),'Color',hifitcolor,'LineWidth',1);
   set(hl(2),'Color',hicolor,'LineWidth',1);
   %axis([tt(1)-1/fs tt(end)+1./fs -0.01 0.06]);
   title(sprintf('c=%d fithi-predhi E=%.3f xc=%.3f',...
      chrange(ccplot),emsehi(ccplot),exchi(ccplot)));

   if length(chrange)>1,
      subplot(2,3,6);
      plot(chrange,[vxchi exchi]);
      legend('fit hi, pred lo','fit hi pred hi');
   end
   
   %set(gcf,'PaperPosition',[0.25 0.25 2 10.5]);
   
   fprintf('to save:\n print -f%d -dpdf array_%s%d_%s.pdf\n',gcf,runclass,subset,analysis);
   return
end


%% do the array plot
%
[AH,DC]=probe_64D_axes(1:length(cellids));

for ii=1:length(cellids),
   axes(AH(ii));
   
   if strcmpi(runclass,'TOR'),
      r=resp{ii}((PreStimBins+1):(end-PostStimBins),:,:);
      [strf{ii},snr,StimParam]=strf_est_core(r,exptparams.TrialObject.ReferenceHandle,fs,0);
      
      startbins=size(strf{ii},2);
      keepbins=min(startbins,10);
      basep=StimParam.basep./startbins.*keepbins;
      tstrf=strf{ii}(:,1:keepbins);
      stplot(tstrf,StimParam.lfreq,basep,1,StimParam.octaves);
      if snr<0.3,
         mm=max(abs(strf{ii}(:)));
         mm=mm./(max(snr,0.05).*10/3);
         set(gca,'Clim',[-1 1].*mm);
      end
      axis off
      text(0,0,sprintf('%d: %.3f',ii,snr),'FontSize',4);
    elseif strcmpi(analysis,'strf') && strcmpi(runclass,'PPS'),
       % FRA analysis
       
       s=reshape(stim,size(stim,1),size(stim,2)*size(stim,3));
       %stim=stim(:,PreBins+1:(end-PostBins),:);
       r=resp{ii}(PreStimBins+1:(end-PostStimBins),:,:);
       s=s(:,:)';
       r=permute(r,[2 1 3]);
       r=r(:,:)';
       r=r.*fs;
       
       if ~isempty(pupil)
          pr0=pupil{ii};
          pr0=pr0(PreStimBins+1:(end-PostStimBins),:,:);
          pr0=permute(pr0,[2 1 3]);
          pr0=pr0(:,:)';
          
          sp=sort(pr0(:));
          mm=sp(round(length(sp).*0.5)); % 0.5=median
          %mm=nanmedian(pr0(:));
          rset=ones(size(r));
          rset(pr0>mm)=2;
       else
          rset=ones(size(r));
       end
       
       setcount=length(unique(rset(:)));
       
       
       ustim=unique(s(:));
       ustim=flipud(ustim(ustim>0));
       l_count=length(ustim);
       freq_count=size(s,2);
       
       FRA=zeros(l_count,freq_count,setcount);
       FRAe=zeros(l_count,freq_count,setcount);
       rwin=round(0.01*options.rasterfs):round(0.04.*options.rasterfs);
       cclag=10;
       ccwin=zeros(cclag*2+1,setcount);
       ccn=zeros(cclag*2+1,setcount);
       for setidx=1:setcount,
          rtemp=r;
          rtemp(rset~=setidx)=nan;
          
          for ff=1:freq_count,
             for ll=1:l_count,
                dd=find(s(:,ff)==ustim(ll));
                dd=dd(dd<=size(r,1)-max(rwin));
                iiset=[];
                for rr=1:length(rwin),
                   iiset=cat(3,iiset,rtemp(dd+rwin(rr),:));
                end
                rr=nanmean(iiset,3);
                
                FRA(ll,ff,setidx)=nanmean(rr(:));
                FRAe(ll,ff,setidx)=nanstd(rr(:))./sqrt(sum(~isnan(rr(:))));
                
                if ll<=3,
                   for rr=1:length(ccwin),
                      tdd=dd+rr-cclag-1;
                      tdd=tdd(tdd>=1 & tdd<=size(rtemp,1));
                      ccwin(rr,setidx)=ccwin(rr,setidx)+nansum(nansum(rtemp(tdd,:)));
                      ccn(rr,setidx)=ccn(rr,setidx)+sum(sum(~isnan(rtemp(tdd,:))));
                   end
                end
             end
          end
       end
       ccwin=ccwin./ccn;
       %strf{ii}=cat(2,FRA(:,:,1),zeros(size(FRA,1),1),FRA(:,:,2));
       %imagesc(strf{ii});
       %axis ij
       
       % timecourse
       %hl=plot((-cclag:cclag)'./options.rasterfs,ccwin);
       %set(hl(1),'Color',locolor);
       %set(hl(2),'Color',hicolor);
       %%xlabel('time from pip onset (s)');
       %%ylabel('mean spike rate (Hz)');
       %%legend('small','big','Location','NorthWest');
       %%legend boxoff
       
       % frequency tuning
       levidx=3;
       mm=mean(mean(mean(FRA(:,:,(1:(setcount))))));
       ff=gsmooth(mean(FRA(levidx,:,1:(setcount))-mm,3),0.5);
       bfi=round(min(find(abs(ff)==max(abs(ff))))./2);
       hl=errorbar(squeeze(FRA(levidx,:,1:(setcount))),...
          squeeze(FRAe(levidx,:,1:(setcount))));
       
       
       % level tuning
       %hl=errorbar(squeeze(FRA(:,bfi,1:(setcount))),squeeze(FRAe(:,bfi,1:(setcount))));
       
       set(hl(1),'Color',locolor);
       set(hl(2),'Color',hicolor);
       
       axis tight
       axis off;
       
       aa=axis;
       text(aa(1),aa(4),sprintf('%d',ii),'FontSize',4);
       
       %FRA=cat(3,FRA,FRA(:,:,2)-FRA(:,:,1));
       %setcount=setcount+1;
       
   elseif strcmpi(analysis,'strf'),
      if size(strf{ii},2)<=3,
         plot(strf{ii}');
      else
         plotastrf(strf{ii},2);
      end
      
      axis off
      %text(0,0,sprintf('%.3f',snr));
   elseif strcmpi(analysis,'raster'),
      tr=resp{ii};
      spontrate=nanmean(nanmean(resp{ii}(1:PreStimBins,:)));
      tr=rconv2(tr,[1;1;1]./3);
      m=mean(tr,2);
      se=std(tr,0,2)./sqrt(size(tr,2));
      rrange=(PreStimBins-50)+(1:150);
      %plot(m(rrange),'k');
      %hold on
      %plot([m(rrange)+se(rrange),m(rrange)-se(rrange)],'g');
      plot(m(rrange)./se(rrange),'k');
      hold on;
      plot([1 length(rrange)],[0 0],'g--');
      
      mm=min(find(m(PreStimBins:end)-se(PreStimBins:end).*2.5>0));
      if ~isempty(mm),
         %plot([mm mm]+50,[min(m-se) max(m+se)],'r-');
         plot([mm mm]+50,[0 4],'r-');
      end
      hold off
      text(0,0,sprintf('%d - %.0f',mm,iso(ii)),'FontSize',4);
      
%       tr=tr((PreStimBins-99):(end-PostStimBins+100),:);
%       [xx,yy]=find(tr);
%       plot(xx./1000,yy,'k.','MarkerSize',1);
%       hold on;
%       plot([0.1 0.1],[min(yy) max(yy)],'g-');
%       plot([0.2 0.2],[min(yy) max(yy)],'g-');
%       hold off
      
      options=struct('rasterfs',1000,'psthfs',100,'psth',1,...
         'PreStimSilence',0.1,'PostStimSilence',0.1);
      %raster_plot(parmfile,tr,tags,AH(ii),options);
      axis off
      axis tight
      %text(0,0,sprintf('%.3f',snr));
   elseif strcmpi(analysis,'spont') || strcmpi(analysis,'evoked'),
      if strcmpi(analysis,'spont'),
         pm=(pupil{ii}(1:PreStimBins,:));
         rm=(resp{ii}(1:PreStimBins,:));
      else
         pm=pupil{ii}((PreStimBins+1):(end-PostStimBins),:);
         rm=resp{ii}((PreStimBins+1):(end-PostStimBins),:);
         %pm=pupil{ii}((PreStimBins+1):(end),:);
         %rm=resp{ii}((PreStimBins+1):(end),:);
         r0=nanmean(rm,2);
         rm=rm-repmat(r0,[1 size(rm,2) 1]);
      end

      T=size(pm,1);
      pm=nanmean(reshape(pm,T,[]));
      rm=nanmean(reshape(rm,T,[]));
      pm=pm(~isnan(pm));
      rm=rm(~isnan(rm));
      
      %rm=gsmooth(rm,2);
      [xc,exc,~,p]=randxcov(pm,rm,0,500);
      dotcolor=[0.9 0.9 0.9].*(1-abs(xc));
      plot(pm,rm,'.','Color',dotcolor);
      if p<0.05,
         text(min(pm),max(rm),sprintf('%d: %.3f*',ii,xc),'FontSize',4);
      else
         text(min(pm),max(rm),sprintf('%d: %.3f',ii,xc),'FontSize',4);
      end
      
      axis off
      axis tight
      aa=axis;
   elseif strcmpi(analysis,'psth'),
      pm=nanmean(pupil{ii});
      rm=resp{ii};
      pm=repmat(pm,[size(rm,1) 1 1]);
      pmed=nanmedian(pm(:));
      tr=rm;
      tr(pm<pmed)=nan;
      r0=nanmean(tr,2);
      tr=rm;
      tr(pm>pmed)=nan;
      r1=nanmean(tr,2);
      
      T=size(pm,1);
      pm=nanmean(reshape(pm,T,[]));
      rm=nanmean(reshape(rm,T,[]));
      pm=pm(~isnan(pm));
      rm=rm(~isnan(rm));
      
      plot([r0(:) r1(:)]);
      xc=xcov(pm,rm,0,'coeff');
      text(0,0,sprintf('%.3f',xc),'FontSize',6);
      
      axis off
      axis tight
   end
   
end

set(gcf,'Name',[siteid '_' runclass '_' analysis]); 
set(gcf,'PaperPosition',[0.25 0.25 2 10.5]);

fprintf('to save:\n print -f%d -dpdf array_%s%d_%s.pdf\n',gcf,runclass,subset,analysis);

