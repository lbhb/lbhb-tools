function pupil_voc(cellid, save_slide, options)

  if 0
     [~,cellids]=dbbatchcells(294);
     cellids=unique(cellids);
     close all
     for ii=126:135
        options=struct();
        options.pupil_lowpass=0;
        options.pupil_highpass=0.1;
        pupil_voc(cellids{ii},0,options);
        options.pupil_lowpass=0.1;
        options.pupil_highpass=0;
        pupil_voc(cellids{ii},0,options);
        options.pupil_lowpass=0;
        options.pupil_highpass=0;
        pupil_voc(cellids{ii},0,options);
     end
  end

  if ~exist('save_slide','var')
     save_slide = 0;
  end
  if ~exist('options','var')
     options=struct();
  end
  options.pupil_highpass=getparm(options,'pupil_highpass',0);
  options.pupil_lowpass=getparm(options,'pupil_lowpass',0);
  

  fs = 50; %sampling frequency (Hz)
  offset = 0.75; %delay to apply to pupil data (s)
  pbs = 3; %number of pupil diameter bins
  if options.pupil_highpass>0
     binedges=[-0.5 -0.03 0.03 0.5];
     len=0.3;
     lbin=-0.45;
  else
     binedges=[0 0.75 1.5 3];
     len = 2; %length of each pupil diameter bin (mm)
     lbin=0; %bottom edge of first bin
  end
  bincenters=diff(binedges)/2+binedges(1:(end-1));
  
  mintrials = 5; %minumum trials required for inclusion in gain analysis
  fbs = 10; %firing rate bins (for plotting gain analysis)

  %colors for plotting pupil bins
  %co = hsv(7);
  %co = repmat(ones(7,1).*((1/7):(1/7):1)',1,3);
  co = [0     0     0.7  ;
        0.7   0     0    ;
        0     0.75  0.75 ;
        0.25  0.25  0.25 ;
        0.850 0.325 0.098;
        0     0.447 0.741;
        0.929 0.694 0.125;
        0.494 0.184 0.556;
        0.466 0.674 0.188;
        0.301 0.745 0.933;
        0.635 0.078 0.184];

  cellfiledata = dbgetscellfile('cellid', cellid, 'runclass', 'VOC', 'Subsets',5, 'pupil',2);

  for cellidx=1:length(cellfiledata)

    clear('r','rsort','pr','psort'); 

    %load experimental parameters
    parmfile = [cellfiledata(cellidx).stimpath cellfiledata(cellidx).stimfile];
    spkfile = [cellfiledata(cellidx).path cellfiledata(cellidx).respfile];
    LoadMFile(parmfile);
    if strcmp(exptparams.TrialObject.ReferenceClass,'FerretVocal'),
       prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
       duration = exptparams.TrialObject.ReferenceHandle.Duration;
    else
       prestim = exptparams.TrialObject.TargetHandle.PreStimSilence;
       duration = exptparams.TrialObject.TargetHandle.Duration;
    end
    %load spike raster
    toptions=options;
    toptions.channel = cellfiledata(cellidx).channum;
    toptions.unit = cellfiledata(cellidx).unit;
    toptions.rasterfs = fs;
    toptions.usesorted = 1;
    toptions.includeprestim=1;
    if ~isempty(cellfiledata(cellidx).goodtrials)
      toptions.trialrange = eval(cellfiledata(cellidx).goodtrials);
    end
    toptions.runclass='VOC';
    
    %r = raster_load(parmfile, channel, unit, options);
    [r,~,rtrialset] = loadspikeraster(spkfile, toptions);
    
    %load pupil diameter
    toptions=options;
    toptions.pupil = 1;
    toptions.verbose = false;
    toptions.rasterfs = fs;
    toptions.pupil_offset = offset;
    toptions.pupil_mm = 1;
    toptions.runclass='VOC';
    toptions.includeprestim=1;
    if ~isempty(cellfiledata(cellidx).goodtrials)
      toptions.trialrange = eval(cellfiledata(cellidx).goodtrials);
    end
    [pr, tags,ptrialset] = loadevpraster(parmfile, toptions);
    if size(r,2)>size(pr,2)
       r=r(:,1:size(pr,2),:);
    elseif size(pr,2)>size(r,2)
       pr=pr(:,1:size(r,2),:);
    end
    
    keepidx=find(isfinite(r(1,1,:)));
    r=r(:,:,keepidx);
    pr=pr(:,:,keepidx);
    tags=tags(keepidx);
    
    %parse vocalization names
    for ii=1:length(tags)
      vocstr = regexp(tags(ii), '(\w*).wav', 'match');
      vocnames(ii) = vocstr{1};
    end

    %lay out figure
    fig = figure('units','normalized','outerposition',[0 0 0.7 1]);
    ax(1) = subplot(5,4,5:6);
    ax(2) = subplot(5,4,7:8);
    ax(3) = subplot(5,4,9:10);
    ax(4) = subplot(5,4,11:12);
    ax(5) = subplot(5,4,13:14);
    ax(6) = subplot(5,4,15:16);
    ax(7) = subplot(5,4,1:3);
    ax(8) = subplot(5,4,4);
    ax(9) = subplot(5,4,17);
    ax(10) = subplot(5,4,18);
    ax(11) = subplot(5,4,19);
    ax(12) = subplot(5,4,20);
    fullpage landscape

    %add a title
    str = sprintf('%s isolation: %0.1f', ...
      cellfiledata(cellidx).cellid, ....
      cellfiledata(cellidx).isolation);
    s = suptitle(str, 0);
    set(s, 'interpreter', 'none')

    %sort trials by average pupil diameter
    pravg = nanmean(pr(1:(prestim*fs),:,:),1);
    [psort,sortidx] = sort(pravg);
    for voc=1:2
      rsort(:,:,voc) = r(:,sortidx(:,:,voc),voc);
    end

    %plot sorted and unsorted rasters
    for voc=1:2
       
       axes(ax(voc))
       [spiketimes, trials] = find(r(:,:,voc));
       if length(spiketimes) > 3000 %adjust raster dot size
          ms = 3;
       else
          ms = 5;
       end
       hold on
       plot((spiketimes/fs)-prestim, trials, 'k.', 'markersize', ms)
       axis tight
       aa = axis;
       plot([0 0], [aa(3) aa(4)], 'g--');
       plot([duration duration], [aa(3) aa(4)], 'g--');
       hold off
       title(sprintf('Vocalization %s', vocnames{voc}))
       if voc==1
          ylabel('Trial number');
       end
       
       axes(ax(voc+2))
       [spiketimes, trials] = find(rsort(:,:,voc));
       hold on
       plot((spiketimes/fs)-prestim, trials, 'k.', 'markersize', ms)
       axis tight
       aa = axis;
       plot([0 0], [aa(3) aa(4)], 'g--');
       plot([duration duration], [aa(3) aa(4)], 'g--');
       hold off
       title('Sorted by pupil diameter')
       %xlabel('Time from Stimulus Onset (s)');
       if voc==1
          ylabel('Pupil diameter (mm)')
       end
       set(ax(voc+2), 'yticklabel', ...
          round(psort(:,get(ax(voc+2),'ytick'),voc),1))
       
    end

    %plot mean pupil diameter over time
    axes(ax(7))
    plot(squeeze(pravg), 'linewidth', 2)
    xlabel('Trial number');
    ylabel('Pupil diameter (mm)')
    legend(vocnames{1}, vocnames{2}, 'location', 'southwest')
    legend boxoff
    box off
    axis tight

    %calculate histogram of binned pupil diameters
    pupil = pravg(:);
    h = [];
    for b=1:pbs
      u = binedges(b+1);
      l = binedges(b);
      x = pupil<u & pupil>= l;
      h(b) = sum(x);
    end

    %plot histogram of pupil diameters
    axes(ax(8))
    hold on
    for b=1:pbs
       bar(bincenters(b),h(b),mean(diff(bincenters))/2, ...
          'facecolor', co(b,:), ...
          'edgecolor', co(b,:), ...
          'linewidth', 1.5)
    end
    xlabel('Pupil diameter (mm)')
    ylabel('Trials')
    box off
    axis tight square
    xlim(binedges([1 end]));
    plot(binedges([1 end]), [mintrials mintrials], ...
       'k-', 'linewidth', 2)
    hold off

    %find correlation between mean pupil diameter and spontaneous rate
    pupil = pravg(:);
    spont = nanmean(r(1:round(prestim*fs),:,:),1).*fs;
    spont = spont(:);
    rcorr = corrcoef(spont, pupil);

    %bin by pupil diameter and find mean spontaneous rate in each bin
    mean_spont = nan(pbs,1);
    for b=1:pbs
      u = binedges(b+1); %upper bin boundary
      l = binedges(b); %lower bin boundary
      x = pupil<u & pupil>=l;
      mean_spont(b) = nanmean(spont(x));
    end

    %plot relationship between pupil diameter and spontaneous rate
    axes(ax(9))
    hold on
    plot(pupil, spont, 'k.')
    plot(bincenters, mean_spont, 'r.-', 'linewidth', 3) 
    xlabel('Pupil diameter (mm)')
    ylabel('Spontaneous rate (Hz)')
    title(sprintf('Pearson''s r: %0.2f', rcorr(2)))
    axis tight square
    xlim(binedges([1 end]));
    box off
    hold off

    %find correlation between mean pupil diameter and driven rate
    evoke = nanmean(r((round(prestim*fs)+1):round((prestim+duration)*fs),:,:),1).*fs;
    evoke = evoke(:);
    drive = evoke - spont;
    rcorr = corrcoef(drive, pupil);

    %bin by pupil diameter and find mean driven rate in each bin
    mean_drive = nan(pbs,1);
    for b=1:pbs
       u = binedges(b+1); %upper bin boundary
       l = binedges(b); %lower bin boundary
       x = pupil<u & pupil>=l;
       mean_drive(b) = nanmean(drive(x));
    end

    %plot relationship between pupil diameter and driven rate
    axes(ax(10))
    hold on
    plot(pupil, drive, 'k.')
    plot(bincenters, mean_drive, 'r.-', 'linewidth', 3) 
    xlabel('Pupil diameter (mm)')
    ylabel('Evoked rate (Hz)')
    title(sprintf('Pearson''s r: %0.2f', rcorr(2)))
    axis tight square
    xlim(binedges([1 end]));
    box off
    hold off

    %find reliability (mean trial-to-trial correlation) for each pupil bin
    %resp = r((round(prestim*fs)+1):round((prestim+duration)*fs),:,:);
    %spont = nanmean(r(1:(round(prestim*fs)),:,:),1); 
    %resp = resp - repmat(spont, size(resp,1), 1); %subtract spont
    %vocs = size(r,3);
    %rely = nan(pbs, vocs);

    %for v=1:vocs
    %  pupil = pravg(:,:,v);
    %  for b=1:pbs
    %    u = b*len;
    %    l = u-len;
    %    x = pupil<u & pupil>=l;
    %    c = corrcoef(resp(:,x,v));
    %    c(logical(eye(size(c)))) = nan;
    %    rely(b,v) = nanmean(c(:));
    %  end
    %end

    %%plot reliabilty
    %axes(ax(11))
    %hold on
    %plot(rely(:,1), 'b.-', 'markersize', 15)
    %plot(rely(:,2), 'g.-', 'markersize', 15)
    %hold off
    %xlabel('Pupil diameter (mm)')
    %ylabel('Reliability')
    %axis tight square
    %xlim([len/2 len*pbs+(len/2)]);

    %calculate psths binned by pupil size
    tbs = size(r,1); %time bins
    vocs = size(r,3);
    binned_psths = nan(tbs, vocs, pbs); %time x vocalization x pupil bin
    for v=1:vocs
       pupil = pravg(:,:,v);
       for b=1:pbs
          u = binedges(b+1); %upper bin boundary
          l = binedges(b); %lower bin boundary
          x = pupil<u & pupil>= l;
          hist_tmp(v,b) = sum(x);
          binned_psths(:,v,b) = nanmean(r(:,x,v),2).*fs;
       end
    end

    %exclude undersampled bins
    hist_tmp = sum(hist_tmp);
    for b=1:pbs
      if hist_tmp(b) < mintrials
         binned_psths(:,:,b) = nan;
      end
    end

    %subtract spontaneous rate
    %binned_spont = nanmean(binned_psths(1:round(prestim*fs),:,:),1);
    %binned_psths = binned_psths - repmat(binned_spont, tbs, 1);

    %plot pupil-binned psths
    %d = 1; %decimation factor
    %tbs = round(size(binned_psths,1)./d);
    %t = ((1:tbs)./(fs/d))-prestim;
    tbs = size(binned_psths,1); %time bins
    t = ((1:tbs)./fs)-prestim;
    for v=1:2
       axes(ax(v+4))
       hold on
       for b=1:pbs
          h = squeeze(binned_psths(:,v,b));
          %h = decimate(h, d); %downsample for plotting
          h = trifilt(h,7); %triangle filter
          plot(t, h, 'color', co(b,:), 'linewidth', 2);
       end
       if v==1
          ylabel('Driven rate (Hz)')
       end
       axis tight
       aa = axis;
       plot([0 0], [aa(3) aa(4)], 'g--');
       plot([duration duration], [aa(3) aa(4)], 'g--');
    end
    
    evoked_epoch = (round(prestim*fs)+1):(round((prestim+duration)*fs));

    mean_psth = squeeze(nanmean(r,2).*fs); %psth across all pupil conditions 
    %subtract spontaneous rate
    spont = nanmean(mean_psth(1:round(prestim*fs),:),1);
    mean_psth = mean_psth - repmat(spont, size(mean_psth,1), 1);
    mean_psth = mean_psth(evoked_epoch,:);
    mean_psth = mean_psth(:); %concatenate across vocalizations

    %gain analysis
    %concatenate evoked epoch for all vocalizations
    all_psths = [];
    gain = nan(2,pbs);
    for v=1:vocs
      all_psths = cat(1, all_psths, binned_psths(evoked_epoch,v,:));
    end
    all_psths = squeeze(all_psths); %time x pupil bin
    %to find gain, fit mean psth against each pupil condition 
    %rotate data before fitting to avoid bias towards zero slope
    for b=1:pbs
      h = all_psths(:,b);
      gain(:,b) = polyfit((mean_psth+h)./2, h-mean_psth, 1);
    end
    gain(1,:) = gain(1,:) + 1; %correct for rotation

    %bin firing rates for plotting
    [mean_psth_sorted, sortidx] = sort(mean_psth);
    p = zeros(size(all_psths));
    for b=1:pbs
      p(:,b) = all_psths(sortidx, b);
    end
    t = size(mean_psth_sorted,1); %time bins
    n = round(t/fbs); %time bins in each firing rate bin
    l =  1:n:((t-n)+1);
    u =  n:n:t;
    pupil_binned_fr = zeros(fbs,pbs);
    mean_fr = zeros(fbs,1);
    for b=1:fbs
      pupil_binned_fr(b,:) = nanmean(p(l(b):u(b),:),1);
      mean_fr(b) = nanmean(mean_psth_sorted(l(b):u(b),:),1);
    end

    %plot gain model
    axes(ax(11))
    hold on
    for b=1:pbs
      plot(mean_fr, pupil_binned_fr(:,b), '-', ...
	'color', co(b,:), ...
	'linewidth', 2)
    end
    hold off
    axis equal tight
    ylabel('Pupil-binned firing rate (Hz)')
    xlabel('Mean firing rate (Hz)')

    %plot gain model parameters
    axes(ax(12))
    hold on
    for b=1:pbs
       bar(bincenters(b),gain(1,b),mean(diff(bincenters))/2, ...
          'facecolor', co(b,:), ...
          'edgecolor', co(b,:), ...
          'linewidth', 1.5)
    end
    hold off
    xlabel('Pupil diameter (mm)')
    ylabel('Gain')
    xlim(binedges([1 end]));
    axis square

    %save figure
    if save_slide
       %create archive directory if necessary
       archive_path = '/auto/users/schwarza/pupil/plots';
       archive_path = [archive_path datestr(now, 'yyyy_mm_dd') '/'];
       if ~exist(archive_path, 'dir')
          mkdir(archive_path)
       end
       %save pdf
       saveas(fig, [archive_path cellfiledata(cellidx).cellid '_VOC_pupil.pdf'])
    end
  end

end
