%% load the data

array64_load

r=cat(4,resp{:});


[stim,stimparam] = loadstimfrombaphy(parmfile, [], [], 'ozgf', fs, 18, 0, 1,'TargetHandle');

%% do the plot
pm=squeeze(mean(pr0(1:PreStimBins,:,:),1));
stimidx=2;
trialset=[10 19 27 36];
duration=3;
chancount=size(stim,1);

figure 
subplot(3,2,1);
plot(pm(:,stimidx),'k');
hold on
plot(pm(:,3-stimidx),'k--');
plot(trialset,pm(trialset,1),'ko');
plot(trialset,pm(trialset,2),'ko');
hold off
xlabel('trial');
ylabel('mean pupil');
box off

subplot(3,2,2);
tt=(1:size(stim,2))./fs-prestim;
imagesc(tt,1:chancount,sqrt(stim(:,:,stimidx)));
axis xy;
hold on
plot([0 0],[1 chancount],'g--');
plot([duration duration],[1 chancount],'g--');
hold off
colormap(1-gray);
yt=round(linspace(1,chancount,4));
set(gca,'YTick',yt,'YTickLabel',round(stimparam.ff(yt).*1000));

mr=gsmooth(nanmean(mean(r(:,:,stimidx,:),2),4),4)./2;
for trialidx=1:length(trialset),
   subplot(3,2,2+trialidx);
   tr=squeeze(r(:,trialset(trialidx),stimidx,:));
   [ii,jj]=find(tr);
   plot(ii./fs-prestim,cellcount+1-jj+15,'k.','MarkerSize',2);
   hold on
   plot([0 0],[0 cellcount+1],'g--');
   plot([duration duration],[0 cellcount+1],'g--');
   tp=gsmooth(mean(tr,2),4)./2;
   plot((1:length(tp))./fs-prestim,mr,'Color',[0.7 0.7 0.7]);
   plot((1:length(tp))./fs-prestim,tp,'Color',[0 0 0]);
   hold off
   axis([-prestim-0.1 duration+poststim+0.1 0 cellcount+16]);
   box off
   title(sprintf('trial %d',trialset(trialidx)));
   
end


