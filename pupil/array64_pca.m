%% baseline/gain analysis

trialcount=sum(trialset(:)>0);
utrials=unique(trialset(trialset>0));
trialmap=zeros(trialcount,1);
for tt=1:trialcount,
   trialmap(tt)=find(trialset==utrials(tt));
end


disp('Baseline/gain analysis');
trialcount=max(trials{1});
g=nan(trialcount,cellcount);
gn=nan(trialcount,cellcount);
b=nan(trialcount,cellcount);
bwn=nan(trialcount,cellcount);
bn=nan(trialcount,cellcount);
e=nan(trialcount,cellcount);

trial_box=0;

for ii=1:length(cellids),
   
   trialcount=length(trials{ii});
   
   N=sqrt(nanmean(resp{ii}(:).^2));
   thispred=pred{ii}(:,trialmap);
   thisresp=resp{ii}(:,trialmap);
   thispupil=pupil{ii}(:,trialmap);
   
   for tt=1:trialcount,
      if tt>trial_box && tt<trialcount-trial_box+1,
         ttr=(tt-trial_box):(tt+trial_box);
      elseif tt<=trial_box,
         ttr=1:(tt+trial_box);
      else
         ttr=(tt-trial_box):trialcount;
      end
      
      tp=thispred(:,ttr);
      tr=thisresp(:,ttr);
      tp(isnan(tr))=nan;
      T=size(tr,1);
      if PreStimBins>100,
         br=nanmean(nanmean(tr(1:PreStimBins,:)));
         bp=nanmean(nanmean(tp(1:PreStimBins,:)));
      else
         br=nanmean(nanmean(tr([1:PreStimBins (T-PostStimBins+20):T],:)));
         bp=nanmean(nanmean(tp([1:PreStimBins (T-PostStimBins+20):T],:)));
      end
      b0=br;
      
      tp=tp(~isnan(tr(:)));
      tr=tr(~isnan(tr(:)));
      
      %smwin=ones(7,1)./7;
      smwin=linspace(0,1,9)';
      smwin=cat(1,smwin,flipud(smwin(1:(end-1))));
      smwin=smwin./sum(smwin);
      tr=rconv2(tr,smwin);
      
      bincount=12;
      sp=[sort(tp);max(tp)+1];
      edges=sp(round(linspace(1,length(sp),bincount+1)));
      edges=unique(edges);
      bincount=length(edges)-1;
      pbinned=zeros(bincount,1);
      rbinned=zeros(bincount,1);
      for bb=1:bincount,
         pbinned(bb)=mean(tp(tp>=edges(bb) & tp<edges(bb+1)));
         rbinned(bb)=mean(tr(tp>=edges(bb) & tp<edges(bb+1)));
      end
      
      % method 1
      ttout=trials{ii}(tt);
      b(ttout,ii)=b0;
      pbinned=pbinned+br-bp;
      %pbinned=pbinned-bp;
      %rbinned=rbinned-br;
      g(ttout,ii)=sum(pbinned.*rbinned)./sum(pbinned.^2);
      e(ttout,ii)=std(tp-tr)./N;
      %e(tt)=std(pbinned-rbinned)./std(rbinned);
   end
   
   bn(:,ii)=b(:,ii);
   gn(:,ii)=g(:,ii);
   % remove nans to clean up
   bwn(:,ii)=b(:,ii);
   b(isnan(b(:,ii)),ii)=nanmean(b(:,ii));
   b(:,ii)=b(:,ii)-bp;
   g(isnan(g(:,ii)),ii)=nanmean(g(:,ii));
   
end
pup=mean(pupil{1}(1:100,:),1)';

g(g<0.5)=0.5;
g(g>2)=2;
 if METHOD==2
    g1=log2(1+g);
else
    g1=log2(g);
end

x=[b];
x=x-repmat(mean(x,1),[size(x,1),1]);
x=x./repmat(std(x,0,1),[size(x,1),1]);
[u,s,v]=svd(x);

x=[g1];
x=x-repmat(mean(x,1),[size(x,1),1]);
x=x./repmat(std(x,0,1),[size(x,1),1]);
[ug,sg,vg]=svd(x);

for pidx=1:cellcount,
   xc=xcov(pup,u(:,pidx),0,'coeff');
   if xc<0,
      u(:,pidx)=-u(:,pidx);
      v(:,pidx)=-v(:,pidx);
   end
   xc=xcov(pup,ug(:,pidx),0,'coeff');
   if xc<0,
      ug(:,pidx)=-ug(:,pidx);
      vg(:,pidx)=-vg(:,pidx);
   end
end

mb=mean(b,2);
mg=mean(g,2);
pr=[floor(min(pup)-1) ceil(max(pup)+1)];

cset={[0.8 0.8 0.8],[0 0 0],[0 0 1],[0.3 0.8 0.3]};

figure;
trange=1:trialcount;

subplot(6,1,1);
plot(trange,(pup-mean(pup))/2+5,'k-','LineWidth',2,'Color',cset{1});
hold on
ttb=b;
ttb(isnan(bn))=nan;
plot(trange,ttb);
plot(trange,mb,'-','LineWidth',2,'Color',cset{2});
hold off
aa=axis; axis([0 trialcount+1 aa(3:4)]);

subplot(6,1,2);
plot(trange,(pup-mean(pup))/20+2,'k-','LineWidth',2,'Color',cset{1});
hold on
ttg=g;
ttg(isnan(gn))=nan;
plot(trange,ttg);
plot(trange,nanmean(g,2),'LineWidth',2,'Color',cset{2});
hold off
aa=axis; axis([0 trialcount+1 aa(3:4)]);

subplot(6,1,3);
plot(trange,(pup-mean(pup))/10+2,'k-','LineWidth',2,'Color',cset{1});
hold on
plot(trange,mb,'LineWidth',2,'Color',cset{2});
plot(u(:,1).*10,'LineWidth',2,'Color',cset{3});
plot(u(:,2).*10,'LineWidth',2,'Color',cset{4});
hold off
aa=axis; axis([0 trialcount+1 aa(3:4)]);

subplot(4,4,9);
plot(pup,mb,'.','Color',cset{2});
title(sprintf('pup v mean base %.3f',xcov(pup,mb,0,'coeff')));
axis([pr floor(min(mb*5))/5 ceil(max(mb*5))/5]);
axis square

subplot(4,4,10);
plot(pup,u(:,1),'.','Color',cset{3});
title(sprintf('pup v PC1 %.3f',xcov(pup,u(:,1),0,'coeff')));
axis([pr floor(min(u(:,1)*20))/20 ceil(max(u(:,1)*20))/20]);
axis square

subplot(4,4,11);
plot(pup,u(:,2),'.','Color',cset{4});
title(sprintf('pup v PC2 %.3f',xcov(pup,u(:,2),0,'coeff')));
axis([pr floor(min(u(:,2)*20))/20 ceil(max(u(:,2)*20))/20]);
axis square

subplot(4,4,12);
plot(v(:,1),'LineWidth',2,'Color',cset{3});
hold on
plot(v(:,2),'LineWidth',2,'Color',cset{4});
plot(diag(s)/50+0.5,'LineWidth',2,'Color',cset{2});
hold off
legend('pc1','pc2');
legend boxoff
set(gca,'Xlim',[0 cellcount+1]);
xlabel('cell');
axis square

subplot(4,4,13);
plot(pup,mg,'.','Color',cset{2});
title(sprintf('pup v mean gain %.3f',xcov(pup,mg,0,'coeff')));
axis([pr floor(min(mg*5))/5 ceil(max(mg*5))/5]);
axis square

subplot(4,4,14);
plot(pup,ug(:,1),'.','Color',cset{3});
title(sprintf('pup v PC1 %.3f',xcov(pup,ug(:,1),0,'coeff')));
axis([pr floor(min(ug(:,1)*20))/20 ceil(max(ug(:,1)*20))/20]);
axis square

subplot(4,4,15);
plot(pup,ug(:,2),'.','Color',cset{4});
title(sprintf('pup v PC2 %.3f',xcov(pup,ug(:,2),0,'coeff')));
axis([pr floor(min(ug(:,2)*20))/20 ceil(max(ug(:,2)*20))/20]);
axis square

subplot(4,4,16);
plot(vg(:,1),'LineWidth',2,'Color',cset{3});
hold on
plot(vg(:,2),'LineWidth',2,'Color',cset{4});
plot(diag(sg)/50+0.5,'LineWidth',2,'Color',cset{2});
hold off
legend('pc1','pc2');
legend boxoff
set(gca,'Xlim',[0 cellcount+1]);
xlabel('cell');
axis square

fullpage portrait


rc=3;
cc=cellcount;

f1=figure;

for ii=1:cellcount,
   modelname='fb18ch100_lognn_wcg02_adp1pc_ap3z1_dexp_fit05v';
   cellid=cellids{ii};
   strfs(ii)=get_strf_from_model(batch,cellids(ii),{modelname});
   
   subplot(rc,cc,ii);
   fr=round(2.^linspace(log2(200),log2(20000),size(strfs{ii},1))./100)./10;
   if ~isempty(strfs{ii}),
      plotastrf(strfs{ii},1,fr,500);
      title(sprintf('%s\n%.2f',cellids{ii},perf{ii}(4)),...
         'Interpreter','none','FontSize',6);
      axis square
      set(gca,'fontsize',6);
   end
   
   subplot(rc,cc,ii+cc);
   plot(pup,bn(:,ii),'k.');
   ff=find(~isnan(bn(:,ii)));
   title(sprintf('%.3f',xcov(pup(ff),bn(ff,ii),0,'coeff')),'fontsize',6);
   axis([pr floor(min(bn(ff,ii))) ceil(max(bn(ff,ii)))]);
   axis square
   set(gca,'fontsize',6);
   
   subplot(rc,cc,ii+cc.*2);
   plot(pup,gn(:,ii),'k.');
   title(sprintf('%.3f',xcov(pup(ff),gn(ff,ii),0,'coeff')),'fontsize',6);
   axis([pr floor(min(gn(ff,ii)*5))/5 ceil(max(gn(ff,ii)*5))/5]);
   axis square
   set(gca,'fontsize',6);
end
set(f1,'PaperPosition',[0.25 0.25 16.5 10.5],'PaperSize',[17 11],...
   'PaperOrientation','portrait');

X=51;
ff=find(~isnan(bwn(X,:)));
r0=[];
for jj=1:length(ff),
    D=maxtrial-X;
    r0=cat(3,r0,resp{ff(jj)}(:,(end-D):end));
end
r0=permute(r0,[1 3 2]);
stim0=stimbytrial(:,:,X:end);
params.maxlag=[-12 0];

[teststim,xcperchan,predstim]=quick_recon(r0,stim0,r0,params);

bapp=u(:,2)*v(:,2)' .*100;
gapp=ug(:,1)*vg(:,1)' + ug(:,2)*vg(:,2)' + 1;


r0norm=r0;
for ii=1:length(ff),
    for jj=1:size(r0,3),
        %r0norm(:,ii,jj)=r0(:,ii,jj)-bapp(jj+X-1,ff(ii));
        r0norm(:,ii,jj)=(r0(:,ii,jj)-bwn(jj+X-1,ff(ii)));
        %r0norm(:,ii,jj)=(r0(:,ii,jj)-bwn(jj+X-1,ff(ii)))./gapp(jj+X-1,ff(ii));
        %r0norm(:,ii,jj)=r0(:,ii,jj)./g(jj,ff(ii));
    end
end

% inputs:
%  r0 - time X cell X stimid
%  stim0 - channel X time X stimid
%  test_r0 - time X cell X stimid
%
[teststimnorm,xcperchan_norm]=quick_recon(r0norm,stim0,r0norm,params);

s1=mean(teststim,4);
s1=s1(:);
s2=stim0(:);
xcov(s1,s2,0,'coeff')
s1norm=mean(teststimnorm,4);
s1norm=s1norm(:);
xcov(s1norm,s2,0,'coeff')

figure;
plot([xcperchan xcperchan_norm])
title(sprintf('raw %.3f  normed %.3f',xcov(s1,s2,0,'coeff'),xcov(s1norm,s2,0,'coeff')));


%figure;plot([bwn(:,1) squeeze(mean(r0norm([1:PreStimBins (T-PostStimBins+20):T],1,:)))])
%figure;plot([bwn(:,1) bapp(:,1)])




if 0,
   print -f1 -dpdf ~/docs/current/pupil/multichan/site_sum.pdf
   print -f2 -dpdf ~/docs/current/pupil/multichan/per_cell.pdf
end


