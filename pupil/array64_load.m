
narf_set_path;
dbopen;

if 0,
   subset=0;
   fs=100;
   %runclass='TOR';  fs=1000;
   %runclass='SPS';  fs=100;
   %runclass='VOC'; subset=5;
   %runclass='VOC'; subset=8;  fs=100;
   %runclass='PPS';  fs=100;
   %runclass='BNB';  fs=1000;
   runclass='PTD';  subset=0;
   
   %analysis='strf';
   %analysis='decode';
   %analysis='raster';
   %analysis='psth';  fs=30;
   analysis='spont'; fs=20;
   %analysis='evoked'; fs=20;
   
   batchid=263;
   %batchid=283;
   %usesorted=0;
   channelrange=1:64;
   %rawid=0;
   
   %siteid='TAR010c'; usesorted=0; rawid=123676;
   siteid='TAR010c'; usesorted=0; rawid=123675;
   %siteid='BOL005c'; usesorted=1;
   %layer4cells=24:41;
   %layer3cells=[42:49];
   %layer5cells=[4:23];
   
   %siteid='BOL006b';  usesorted=0;
end

if strcmpi(runclass,'SPS'),
   modelname='env100_logn_adp1pc_fir15_siglog100_fit05a';
   strfbatch=283;
elseif strcmpi(runclass,'VOC') && subset==8,
   modelname='fb18ch100_lognn_wcg02_adp1pc_fir15_siglog100_fit05h_fit05c';
   strfbatch=263;
else
   modelname='';
   strfbatch=0;
end



if usesorted,
%    sql=['SELECT sRunData.* FROM sRunData ',...
%       ' WHERE sRunData.batch=',num2str(batchid),...
%       ' AND sRunData.cellid like "',siteid,'%"',...
%       ' ORDER BY sRunData.cellid'];
%    cfd=mysql(sql);

   %[cfd]=dbgetscellfile('cellid',siteid,'runclass',runclass,'pupil',2,'Subsets',subset,'isolation','>84');
   if rawid,
      [cfd]=dbgetscellfile('cellid',siteid,'runclass',runclass,'pupil',2,'isolation','>84','rawid',rawid);
   else
      [cfd]=dbgetscellfile('cellid',siteid,'runclass',runclass,'pupil',2,'isolation','>84');
   end 
   cellids=unique({cfd.cellid});
   
   siteid=strsep(cellids{1},'-');
   siteid=siteid{1};
else
   cellids={};
   for ii=channelrange,
      cellids=cat(2,cellids,sprintf('%s-%02d-1',siteid,channelrange(ii)));
   end
end
cellcount=length(cellids);

resp={};
pupil={};
strf={};
perf={};
iso=zeros(cellcount,1);
channel=zeros(cellcount,1);
maxreps=0;
for ii=1:cellcount,
   cellid=cellids{ii};
   if usesorted,
   
      if exist('subset','var') && subset>0,
         [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass,'pupil',2,'Subsets',subset,'isolation','>80');
      elseif rawid,
         [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass,'pupil',2,'rawid',rawid);
      else
         [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass,'pupil',2);
      end
      %if isempty(cfd),
      %   [cfd]=dbgetscellfile('cellid',cellid,'runclass',runclass);
      %end
      iso(ii)=cfd(1).isolation;
      channel(ii)=cfd(1).channum;
      
      spikefile = [cfd(1).path cfd(1).respfile];
   else
      if rawid,
         sql=['SELECT resppath as stimpath,parmfile as stimfile,',...
            ' eyewin as pupil,id as rawid FROM gDataRaw',...
            ' WHERE id=',num2str(rawid),' AND not(bad) and eyewin=2'];
      else
         sql=['SELECT resppath as stimpath,parmfile as stimfile,',...
            ' eyewin as pupil,id as rawid FROM gDataRaw WHERE cellid="',siteid,'"',...
            ' AND runclass like "%',runclass,'%" AND not(bad) and eyewin=2'];
      end
      cfd=mysql(sql);
      if isempty(cfd),
         sql=['SELECT resppath as stimpath,parmfile as stimfile,',...
            ' eyewin as pupil, id as rawid FROM gDataRaw WHERE cellid="',siteid,'"',...
            ' AND runclass like "%',runclass,'%" AND not(bad)'];
         cfd=mysql(sql);
      end
      channel(ii)=ii;      
   end
   parmfile = [cfd(1).stimpath cfd(1).stimfile];
   LoadMFile(parmfile);
   prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
   duration = exptparams.TrialObject.ReferenceHandle.Duration;
   poststim = exptparams.TrialObject.ReferenceHandle.PostStimSilence;
   PreStimBins=round(prestim.*fs);
   PostStimBins=round(poststim.*fs);
   
   if strcmpi(analysis,'strf') && ...
         (strcmpi(runclass,'SPS') || ...
         (strcmpi(runclass,'VOC') && subset==8)),
      
      % load from narf post-processed files
       strf(ii)=get_strf_from_model(strfbatch,{cellid},{modelname});
   else
      
      % load via baphy functions
      options = struct();
      if ~strcmp(analysis,'spont'),
         options.runclass=runclass;
      else
         options.tag_masks={'Reference'};
      end      
      options.rasterfs = fs;
      options.includeprestim = 1;
      pr_options = options;
      pr_options.pupil = 1;
      pr_options.pupil_median=1;
      %pr_options.pupil_deblink=1;
      pr_options.pupil_mm=0;
      pr_options.pupil_offset=0.75;
      
      r0=[];
      if ii==1, pr0=[]; end
      for fidx=1:1, % length(cfd),
         parmfile = [cfd(fidx).stimpath cfd(fidx).stimfile];
         
         r_options = options;
         
         %load spike raster
         if usesorted,
            spikefile = [cfd(fidx).path cfd(fidx).respfile];
            
            r_options.channel=cfd(fidx).channum;
            r_options.unit=cfd(fidx).unit;
            if ~isempty(cfd(fidx).goodtrials),
               r_options.trialrange=eval(cfd(fidx).goodtrials);
            end
            if strfbatch==263,
               r_options.tag_masks={'Reference1'};
               [r1,tags1,trialset1] = loadspikeraster(spikefile, r_options);
               r_options.tag_masks={'Reference2'};
               [r2,tags2,trialset2] = loadspikeraster(spikefile, r_options);
               r=cat(3,r1,r2);
               tags={tags1{:} tags2{:}};
               trialset=cat(2,trialset1,trialset2);
            else
               [r,tags,trialset] = loadspikeraster(spikefile, r_options);
            end
         elseif strfbatch==263,
            r_options.channel=channelrange(ii);
            r_options.tag_masks={'Reference1'};
            [r1,tags1,trialset1] = loadevpraster(parmfile,r_options);
            r_options.tag_masks={'Reference2'};
            [r2,tags2,trialset2] = loadevpraster(parmfile,r_options);
            r=cat(3,r1,r2);
            tags={tags1{:} tags2{:}};
            trialset=cat(2,trialset1,trialset2);
         else
            r_options.channel=channelrange(ii);
            [r,tags,trialset] = loadevpraster(parmfile,r_options);
         end
         r0=cat(2,r0,r);
         
         %load pupil diameter raster
         if ii==1 && cfd(fidx).pupil>=2,
            if strfbatch==263,
               pr_options.tag_masks={'Reference1'};
               [pupil1, ptags1, ptrialset1] = loadevpraster(parmfile, pr_options);
               pr_options.tag_masks={'Reference2'};
               [pupil2, ptags2, ptrialset2] = loadevpraster(parmfile, pr_options);
               p=cat(3,pupil1,pupil2);
               ptags=cat(2,ptags1,ptags2);
               ptrialset=cat(2,ptrialset1,ptrialset2);
            else
               [p,ptags,ptrialset] = loadevpraster(parmfile, pr_options);
            end
            pr0=cat(2,pr0,p);
         end
         
         if ii==1,
            if strcmpi(analysis,'strf') && strcmpi(runclass,'PPS'),
               % load PPS spectrogram
               disp('Loading stimulus spectrogram...');
               [stim,stimparam]=loadstimfrombaphy(parmfile,[],[],'parm',fs,0,0,0);
            elseif strcmpi(analysis,'decode'),
               % load other spectrogram
               if strfbatch==283,
                  [stim,stimparam]=loadstimfrombaphy(parmfile,[],[], 'envelope',fs,0,0,1);
               elseif strfbatch==263,
                  [stim1,stimparam] = loadstimfrombaphy(...
                     parmfile, [], [], 'ozgf', fs, 18, 0, 1);
                  [stim2] = loadstimfrombaphy(...
                     parmfile, [], [], 'ozgf', fs, 18, 0, 1,'TargetHandle');
                  stim=cat(3,stim1,stim2);
               else
                  [stim,stimparam]=loadstimfrombaphy(parmfile,[],[], 'ozgf',fs,18,0,1);
               end
               
            end
         end
         
         if ~isempty(modelname),
            sql=['SELECT * FROM NarfResults WHERE cellid="',cellid,'"', ...
               ' AND batch=',num2str(strfbatch),...
               ' AND modelname="',modelname,'"'];
            ndata=mysql(sql);
            if ~isempty(ndata),
               perf{ii}=ndata(1).r_test;
            end
         end
      end
      maxreps=max(maxreps,size(r0,2));
      resp{ii}=r0*r_options.rasterfs;
      pupil{ii}=pr0;
   end
end

% find best-isolated unit if multiple units exist for each channel
% keep=zeros(size(channel));
% for cc=unique(channel(:)'),
%    ff=find(channel==cc);
%    keep(ff(iso(ff)==max(iso(ff))))=1;
% end
keep=ones(size(channel));
for ii=1:length(resp),
   if size(resp{ii},2)<maxreps,
      keep(ii)=0;
   end
end
keepidx=find(keep);
pupil=pupil(keepidx);
resp=resp(keepidx);
cellids=cellids(keepidx);
if ~isempty(strf),
   strf=strf(keepidx);
end
if ~isempty(perf),
   perf=perf(keepidx);
end
iso=iso(keepidx);
channel=channel(keepidx);
cellcount=length(keepidx);


locolor=[.5 .5 .5];
hicolor=[0 0 0];
lofitcolor=[0 0 0.8];
hifitcolor=[0.8 0.2 .2];

disp('array64_load.m: complete');

