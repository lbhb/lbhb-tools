close all

global ES_LINE ES_SHADE

% cellid='bbl071d-a1';
cellid='bbl071d-a2'; % good example
% cellid='bbl074g-a1'; % not showing enhancement in active
% cellid='bbl078k-a1';
% cellid='bbl081d-a1'; % good example
% cellid='BRT005c-a1'; % entrainment cell
% cellid='BRT006d-a1';
% cellid='BRT006d-a2';
% cellid='BRT007c-a1';
% cellid='BRT007c-a3';
% cellid='BRT009c-a1';
% cellid='BRT015b-a1';
% cellid='BRT015c-a1';
% cellid='BRT016f-a1';
% cellid='BRT017g-a1'; %cool example
% cellid='ley011c-a1';



figurepath='/auto/users/daniela/docs/AC_IC_project/eps_files_PTD/comb_beha_ephy_saved_figures/IC_data_all_pupil/';

cfd=dbgetscellfile('cellid',cellid,'runclass','PTD');

pupil=cat(1,cfd.pupil);
cfd=cfd(pupil==2);

f1=figure;
f2=figure;
f3=figure;


lstr={};
hl=zeros(length(cfd),1);

t0=0;
p_trial_avg={};

rasterfs=25;
includeincorrect=1;

for ii=1:length(cfd)
    stimfile=[cfd(ii).stimpath cfd(ii).stimfile];
    
    poptions=struct('tag_masks',{{'SPECIAL-TRIAL'}},...
        'rasterfs',rasterfs,'includeincorrect',includeincorrect,...
        'pupil',1,'pupil_median',1,'pupil_offset',0.75);
    
    p=loadevpraster(stimfile,poptions);
    
    options=struct('tag_masks',{{'SPECIAL-TRIAL'}},...
        'rasterfs',rasterfs,'includeincorrect',includeincorrect);
    spkfile=[cfd(ii).path cfd(ii).respfile];
    r=loadspikeraster(spkfile,options);
    r=gsmooth(r,[5 0.01]);

    p=p(1:size(r,1),:);
    
    basep=nanmean(p(1:13,:));
    p_norm=p-repmat(basep,size(p,1),1);
    pmean=nanmean(p_norm(1:(options.rasterfs*4),:),2);
    
    p=p(:);
    r=r(:);
    r=r(~isnan(p));
    p=p(~isnan(p));
    
    t=t0+(1:length(p))./options.rasterfs;
    t0=max(t);
    figure(f1);
    hl(ii)=plot(t,p,'Color',ES_LINE{ii});
    hold on
    plot(t,r,'Color',ES_LINE{ii});
    lstr{ii}=cfd(ii).stimfile;
    %export_fig([figurepath cellid '_pupil_trace.eps'],'-eps'); 
    
    figure(f2);
    plot(pmean,'Color',ES_LINE{ii});
    hold on
    drawnow
   % export_fig([figurepath cellid '_pupil_trace_onsetPSTH.eps'],'-eps'); 
    
    figure(f3);
    subplot(3,3,ii);
    hf=histogram(p,10:5:80);
    set(hf,'FaceColor',ES_SHADE{ii});
        %export_fig([figurepath cellid '_pupil_histograms.eps'],'-eps');
end
hold off

figure(f1);
legend(hl,lstr,'Interpreter','none');


raster_options=struct('lick',0,'psthfs',25,'active',0);
cell_rasters(cellid,'PTD',raster_options);




