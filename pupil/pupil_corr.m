function p = pupil_corr(cellid, runclass, fs, pupilbins, options)
%P = PUPIL_BINNED_PSTH(CELLID, RUNCLASS, FS, PUPILBINS, OPTIONS)
%Calculates PSTH for response data binned by pupil bin.
%Inputs:
% cellid: code indicating recording site and possibly unit (e.g. "eno013c-b1")
% runclass: three-letter code indicating stimulus type (e.g. "VOC")
% fs: sampling rate (Hz)
% pupilbins: a vector of lower boundaries for pupil bins (pixels)
% options: options for loading spike raster (e.g. channel, unit, tag_masks)
%Returns
% p: binned psths (time x pupil)
%Created ZPS 11/5/15

if nargin < 5
   options = struct();
end
showplot=getparm(options,'showplot',0);
options.avg_stim=getparm(options,'avg_stim',0);
options.runclass=runclass;

rawid=118702;
if exist('rawid','var'),
   cfd = dbgetscellfile('cellid', cellid, 'runclass', runclass,'rawid',rawid); %cellfiledata
else
   cfd = dbgetscellfile('cellid', cellid, 'runclass', runclass); %cellfiledata
end
parmfile = [cfd(1).stimpath cfd(1).stimfile];
spikefile = [cfd(1).path cfd(1).respfile];
LoadMFile(parmfile)
prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
duration = exptparams.TrialObject.ReferenceHandle.Duration;

rawid=cfd(1).rawid;
sql=['SELECT channum,unit FROM sCellFile WHERE rawid=',num2str(rawid)];
fdata=mysql(sql);
unitset=[cat(1,fdata.channum).*10+cat(1,fdata.unit)];


%load spike raster
r_options = options;
r_options.rasterfs = fs;
r_options.includeprestim = 1;
%r_options.channel=cfd(1).channum;
%r_options.unit=cfd(1).unit;
r_options.channel=floor(unitset/10);
r_options.unit=mod(unitset,10);
r_options.meansub=0;
[r,tags,trialset] = loadsiteraster(spikefile, r_options);
r=r*r_options.rasterfs;

%load pupil diameter raster
pr_options = options;
pr_options.rasterfs = fs;
pr_options.pupil = 1;
pr_options.pupil_median=0.5;
pr_options.pupil_offset=0.9;
[pr0,ptags,ptrialset] = loadevpraster(parmfile, pr_options);

pr_options0=pr_options;
pr_options0.pupil_median=0.1;
pupil_raw= loadevpraster(parmfile, pr_options0);

% is this necessary?
pr=pr0.*nan;
trialcount=max(trialset(:));
for trialidx=1:trialcount,
    pb=find(ptrialset==trialidx);
    rb=find(trialset==trialidx);  % (rep,stimidx)
    pr(:,rb)=pr0(:,pb);
end

keepidx=find(sum(~isnan(r(1,:,:,1)),2)>0);
r=r(:,:,keepidx,:);
pr=pr(:,:,keepidx);

%pr(find(pr==0)) = nan; %remove zeros (blinks, measurement failures)
%pr(find(pr<min(pupilbins))) = nan; %set threshold on pupil size
%pr(find(pr>max(pupilbins))) = nan;

%classify each trial by mean pupil diameter in silence
pravg = nanmean(pr(1:round(prestim*fs),:,:),1);
pravg1 = nanmean(pr(1:round(prestim/2*fs),:,:),1);
pravg2 = nanmean(pr(round(prestim/2*fs+1):round(prestim*fs),:,:),1);
pg=zeros([size(squeeze(pravg)) length(pupilbins)]);
for b = 1:length(pupilbins),
   pg(:,:,b) = squeeze(pravg > pupilbins(b));
end
pg = sum(pg,3);

%calculate psths
p=zeros(size(r,1),size(r,3),length(pupilbins),size(r,4));
for b=1:length(pupilbins)
   for stim=1:size(r,3)
     %find the stimulus repetitions in each pupil bin
     pgt = (pg(:,stim)==b); %pupil group trials
     %average over those repetitions
     p(:,stim,b,:) = nanmean(r(:,pgt,stim,:),2);
   end
end
if options.avg_stim,
    p = squeeze(nanmean(p,2)); %average over stimuli
end

% cross-corr spont activity
prebins=1:round(fs*prestim);
corrbins=round(fs*0.2);
prestimr=r(prebins,:,:,:);
prestimr=reshape(prestimr,corrbins,size(prestimr,1)/corrbins,size(prestimr,2)*size(prestimr,3),size(prestimr,4));
prestimr=squeeze(mean(prestimr,1));

b=[pravg(:)];
[~,bi]=sort(b);
bincount=6;
cellcount=size(r,4);
bstep=size(bi,1)/bincount;
bcenter=zeros(bincount,1);
bmean=zeros(bincount,cellcount);
pairs=nchoosek(1:cellcount,2);
bxc=zeros(bincount,size(pairs,1));

for bb=1:bincount
   ff=round((bb-1)*bstep+1):round(bb*bstep);
   tr=prestimr(:,bi(ff),:);
   tr=reshape(tr,size(tr,1)*size(tr,2),cellcount);
   
   bcenter(bb)=mean(b(bi(ff),1));
   bmean(bb,:)=mean(tr,1);
   
   for pp=1:size(pairs,1),
      bxc(bb,pp)=xcov(tr(:,pairs(pp,1)),tr(:,pairs(pp,2)),0,'coeff');
   end
end

figure;
subplot(2,1,1);
plot(bcenter,bmean);
subplot(2,1,2);
plot(bcenter,bxc);
legend(num2str(pairs));

return
%prestimpupil=prestimpupil-repmat(mean(prestimpupil,1),size(prestimpupil,1),1);

prxc=zeros(41,trialcount);
for ii=1:trialcount,
   prxc(:,ii)=xcov(prestimr(:,ii),prestimpupil(:,ii),20,'unbiased');
end
meanxc=zeros(size(prxc,1),length(pupilbins));
for b=1:length(pupilbins)
   meanxc(:,b)=mean(prxc(:,find(pg==b)),2);
end

figure;
subplot(2,1,1);
plot(meanxc);

subplot(2,1,2);
plot(prestimpupil(:)+randn(size(prestimpupil(:))),prestimr(:)+randn(size(prestimpupil(:))),'.');

disp('done');


b0=squeeze(nanmean(r(1:round(fs*prestim),:,:)));



if showplot,
   figure;
   subplot(2,2,1);
   hist(pravg(~isnan(pravg)));
   
   subplot(2,2,2);
   plot(squeeze(p(:,1,:)));
   ps=cell(length(pupilbins),1);
   for ll=1:length(pupilbins),
      n=squeeze(nansum(nansum(pg==ll)));
      ps{ll}=sprintf('%d (%d trials)',pupilbins(ll),n);
   end
   legend(ps);
   title([cellid ' - ' runclass]);
   
   b0=squeeze(nanmean(r(1:round(fs*prestim),:,:)));

   pravg1 = nanmean(pr(1:round(prestim/2*fs),:,:),1);
   pravg2 = nanmean(pr(round(prestim/2*fs+1):round(prestim*fs),:,:),1);
   
   b1=squeeze(nanmean(r(1:round(prestim/2*fs),:,:)));
   b2=squeeze(nanmean(r(round(prestim/2*fs+1):round(prestim*fs),:,:)));
   
   e0=squeeze(nanmean(r(round(fs*prestim+1):round(fs*(prestim+duration)),:,:)))-b0;
   pravg=squeeze(pravg);
   
   stimcount=size(pravg,2);
   bcenter=zeros(bincount,1);
   bmean=zeros(bincount,1);
   ecenter=zeros(bincount,stimcount);
   emean=zeros(bincount,stimcount);
   
   b=[pravg(:) b0(:)];
   %b=[pravg1(:) b1(:); pravg2(:) b2(:)];
   b=b(~isnan(b(:,1)),:);
   bs=sortrows(b);
   bstep=size(bs,1)/bincount;
   for bb=1:bincount
      ff=round((bb-1)*bstep+1):round(bb*bstep);
      bcenter(bb)=mean(bs(ff,1));
      bmean(bb)=mean(bs(ff,2));
   end
   
   for s=1:stimcount,
      e=[pravg(:,s) e0(:,s)];
      e=e(find(~isnan(e(:,1))),:);
      es=sortrows(e);
      estep=size(es,1)/bincount;
      for bb=1:bincount
         ff=round((bb-1)*estep+1):round(bb*estep);
         ecenter(bb,s)=mean(es(ff,1));
         emean(bb,s)=mean(es(ff,2));
      end
   end
   
   %pravg=squeeze(pravg);
   dd=[pravg(:) b0(:)];
   %dd=[pravg1(:) b1(:);
   %   pravg2(:) b2(:)];
   dd=dd(find(~isnan(dd(:,1))),:);
   subplot(2,2,3);
   plot(dd(:,1),dd(:,2),'.','Color',[0.3 0.3 1]);
   hold on
   plot(bcenter,bmean);
   hold off
   
   xlabel('diameter');
   ylabel('spont');
   
   dotcolors=[0.3 0.3 1; 1 0.3 0.3];
   linecolors=[0 0 1; 1 0 0];
   subplot(2,2,4);
   for s=1:stimcount,
      dd=[pravg(:,s) e0(:,s)];
      dd=dd(~isnan(dd(:,1)),:);
      plot(dd(:,1),dd(:,2),'.','Color',dotcolors(s,:));
      hold on
      plot(ecenter(:,s),emean(:,s),'Color',linecolors(s,:));
   end
   hold off
   xlabel('diameter');
   ylabel('evoked activity');
end

prebins=1:round(fs*prestim);
%prebins=1:round(fs*(prestim+1));
prestimr=r(prebins,:);
prestimpupil=pupil_raw(prebins,:);
%prestimpupil=prestimpupil-repmat(mean(prestimpupil,1),size(prestimpupil,1),1);

prxc=zeros(41,trialcount);
for ii=1:trialcount,
   prxc(:,ii)=xcov(prestimr(:,ii),prestimpupil(:,ii),20,'unbiased');
end
meanxc=zeros(size(prxc,1),length(pupilbins));
for b=1:length(pupilbins)
   meanxc(:,b)=mean(prxc(:,find(pg==b)),2);
end

figure;
subplot(2,1,1);
plot(meanxc);

subplot(2,1,2);
plot(prestimpupil(:)+randn(size(prestimpupil(:))),prestimr(:)+randn(size(prestimpupil(:))),'.');

disp('done');


