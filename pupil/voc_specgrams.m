%% load the data
fs=200;
chancount=128;
parmfile='/auto/data/daq/Boleto/BOL005/BOL005c05_p_PPS_VOC';
[stim,stimparam] = loadstimfrombaphy(parmfile, [], [], 'ozgf', fs, chancount, 0, 1,'TargetHandle');
stimwav = loadstimfrombaphy(parmfile, [], [], 'wav', 16000, 1, 0, 1,'TargetHandle');

LoadMFile(parmfile);
PreStimSilence=exptparams.TrialObject.TargetHandle.PreStimSilence;

%% do the plot
figure;

tw=(1:size(stimwav,2))./16000-PreStimSilence;
tt=(1:size(stim,2))./fs-PreStimSilence;

subplot(2,2,1);
tw=(1:size(stimwav,2))./16000-PreStimSilence;
plot(tw,stimwav(:,:,1),'k');
xlabel('Time from stimulus onset (s)');
ylabel('Amplitude');
axis([tw([1 end]) -1 1]);
box off

subplot(2,2,2);
plot(tw,stimwav(:,:,2),'k');
xlabel('Time from stimulus onset (s)');
ylabel('Amplitude');
axis([tw([1 end]) -1 1]);
box off

subplot(2,2,3);
imagesc(tt,1:chancount,sqrt(stim(:,:,1)));
axis xy;
colormap(1-gray);
yt=get(gca,'YTick');
ff=round(stimparam.ff(yt)*10)./10;
set(gca,'YTick',yt,'YTickLabel',ff);
xlabel('Time from stimulus onset (s)');
ylabel('Frequency (kHz)');
box off

subplot(2,2,4);
imagesc(tt,1:chancount,sqrt(stim(:,:,2)));
axis xy;
colormap(1-gray);
yt=get(gca,'YTick');
ff=round(stimparam.ff(yt)*10)./10;
set(gca,'YTick',yt,'YTickLabel',ff);
xlabel('Time from stimulus onset (s)');
ylabel('Frequency (kHz)');
box off

set(gcf,'PaperPosition',[0.25 2.5 8 3]);
