
modelpath = '/auto/users/bburan/code/raw_files';
savefile = fullfile(modelpath, 'metadata');

cfdmaster = dbgetscellfile('runclass', 'RDT', 'Trial_Mode', 'RdtWithSingle');
data = {};
for j = 1:length(cfdmaster),
	fprintf('.');
	cell_file = cfdmaster(j);
	stim_file = fullfile(cell_file.stimpath, cell_file.stimfile) ;
    LoadMFile(stim_file);
	[~, processed_params] = load_RDT_by_trial(stim_file);
    data(end+1).cell_file = cell_file;
    data(end).params = exptparams;
	data(end).processed_params = processed_params;
end
save(savefile, 'data');
