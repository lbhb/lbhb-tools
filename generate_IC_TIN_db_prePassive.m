
% IC data structure bbl IC RH. 2017-06-07 Daniela Saderi
% modified from generate IC_TIN_db to only contain cells that have a
% prepassive

ICdata=struct;

% Pre-passive - Active hard
ICdata(1).cellid='bbl021h-a1';
ICdata(1).area='ICx';
ICdata(1).type='Probe_DS';
ICdata(1).note='Stable isolation P90 A90; hard to tell BF';
ICdata(1).datapath='/auto/data/daq/Babybell/bbl021/sorted/';
ICdata(1).passive_spike='bbl021h02_p_PTD.spk.mat';
ICdata(1).passive_rawid=120254;
ICdata(1).active_spike='bbl021h04_a_PTD.spk.mat';
ICdata(1).active_rawid=120256;
ICdata(1).on_BF=0; % 1=on BF target, 0=off BF
ICdata(1).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy
ICdata(2).cellid='bbl021h-a1';
ICdata(2).area='ICx';
ICdata(2).type='Probe_DS';
ICdata(2).note='Stable isolation P90 A90; hard to tell BF';
ICdata(2).datapath='/auto/data/daq/Babybell/bbl021/sorted/';
ICdata(2).passive_spike='bbl021h02_p_PTD.spk.mat';
ICdata(2).passive_rawid=120254;
ICdata(2).active_spike='bbl021h04_a_PTD.spk.mat';
ICdata(2).active_rawid=120258;
ICdata(2).on_BF=0; % 1=on BF target, 0=off BF
ICdata(2).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy
ICdata(3).cellid='bbl022g-a1';
ICdata(3).area='ICx';
ICdata(3).type='Probe_DS';
ICdata(3).note='Stable isolation P95 A99';
ICdata(3).datapath='/auto/data/daq/Babybell/bbl022/sorted/';
ICdata(3).passive_spike='bbl022g04_p_PTD.spk.mat';
ICdata(3).passive_rawid=120286;
ICdata(3).active_spike='bbl022g07_a_PTD.spk.mat';
ICdata(3).active_rawid=120289;
ICdata(3).on_BF=0; % 1=on BF target, 0=off BF
ICdata(3).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(4).cellid='bbl022g-a1';
ICdata(4).area='ICx';
ICdata(4).type='Probe_DS';
ICdata(4).note='Stable isolation P95 A99';
ICdata(4).datapath='/auto/data/daq/Babybell/bbl022/sorted/';
ICdata(4).passive_spike='bbl022g04_p_PTD.spk.mat';
ICdata(4).passive_rawid=120286;
ICdata(4).active_spike='bbl022g08_a_PTD.spk.mat';
ICdata(4).active_rawid=120290;
ICdata(4).on_BF=0; % 1=on BF target, 0=off BF
ICdata(4).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy
ICdata(5).cellid='bbl023c-a1';
ICdata(5).area='ICx';
ICdata(5).type='Probe_DS';
ICdata(5).note='Stable isolation P99 A95; not quite onBF, offset response';
ICdata(5).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(5).passive_spike='bbl023c02_p_PTD.spk.mat';
ICdata(5).passive_rawid=120310;
ICdata(5).active_spike='bbl023c03_a_PTD.spk.mat';
ICdata(5).active_rawid=120311;
ICdata(5).on_BF=0; % 1=on BF target, 0=off BF
ICdata(5).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(6).cellid='bbl023c-a1';
ICdata(6).area='ICx';
ICdata(6).type='Probe_DS';
ICdata(6).note='Stable isolation P99 A99; not quite onBF, offest response';
ICdata(6).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(6).passive_spike='bbl023c02_p_PTD.spk.mat';
ICdata(6).passive_rawid=120310;
ICdata(6).active_spike='bbl023c04_a_PTD.spk.mat';
ICdata(6).active_rawid=120312;
ICdata(6).on_BF=0; % 1=on BF target, 0=off BF
ICdata(6).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active hard
ICdata(7).cellid='bbl026f-a1';
ICdata(7).area='ICc';
ICdata(7).type='Probe_SVD';
ICdata(7).note='Stable isolation P99 A95';
ICdata(7).datapath='/auto/data/daq/Babybell/bbl026/sorted/';
ICdata(7).passive_spike='bbl026f02_p_PTD.spk.mat';
ICdata(7).passive_rawid=120538;
ICdata(7).active_spike='bbl026f03_a_PTD.spk.mat';
ICdata(7).active_rawid=120539;
ICdata(7).on_BF=0; % 1=on BF target, 0=off BF
ICdata(7).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy
ICdata(8).cellid='bbl026f-a1';
ICdata(8).area='ICc';
ICdata(8).type='Probe_SVD';
ICdata(8).note='Stable isolation P99 A90; iso changed a bit';
ICdata(8).datapath='/auto/data/daq/Babybell/bbl026/sorted/';
ICdata(8).passive_spike='bbl026f02_p_PTD.spk.mat';
ICdata(8).passive_rawid=120538;
ICdata(8).active_spike='bbl026f04_a_PTD.spk.mat';
ICdata(8).active_rawid=120542;
ICdata(8).on_BF=0; % 1=on BF target, 0=off BF
ICdata(8).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active hard
ICdata(9).cellid='bbl026f-a2';
ICdata(9).area='ICc';
ICdata(9).type='Probe_SVD';
ICdata(9).note='OK isolation P90 A90';
ICdata(9).datapath='/auto/data/daq/Babybell/bbl026/sorted/';
ICdata(9).passive_spike='bbl026f02_p_PTD.spk.mat';
ICdata(9).passive_rawid=120538;
ICdata(9).active_spike='bbl026f03_a_PTD.spk.mat';
ICdata(9).active_rawid=120539;
ICdata(9).on_BF=1; % 1=on BF target, 0=off BF
ICdata(9).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active easy
ICdata(10).cellid='bbl027i-a1';
ICdata(10).area='ICc';
ICdata(10).type='Var_SNR';
ICdata(10).note='Great isolation P99 A99; broad-band response';
ICdata(10).datapath='/auto/data/daq/Babybell/bbl027/sorted/';
ICdata(10).passive_spike='bbl027i01_p_PTD.spk.mat';
ICdata(10).passive_rawid=120706;
ICdata(10).active_spike='bbl027i02_a_PTD.spk.mat';
ICdata(10).active_rawid=120707;
ICdata(10).on_BF=0; % 1=on BF target, 0=off BF
ICdata(10).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active hard
ICdata(11).cellid='bbl027i-a1';
ICdata(11).area='ICc';
ICdata(11).type='Var_SNR';
ICdata(11).note='Great isolation P99 A99; broad-band response; not great behavior';
ICdata(11).datapath='/auto/data/daq/Babybell/bbl027/sorted/';
ICdata(11).passive_spike='bbl027i01_p_PTD.spk.mat';
ICdata(11).passive_rawid=120706;
ICdata(11).active_spike='bbl027i03_a_PTD.spk.mat';
ICdata(11).active_rawid=120708;
ICdata(11).on_BF=0; % 1=on BF target, 0=off BF
ICdata(11).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active hard
ICdata(12).cellid='bbl029f-a1';
ICdata(12).area='ICc';
ICdata(12).type='Var_SNR';
ICdata(12).note='Good isolation P99 A95, iso a bit worse in A; bad behavior';
ICdata(12).datapath='/auto/data/daq/Babybell/bbl029/sorted/';
ICdata(12).passive_spike='bbl029f03_p_PTD.spk.mat';
ICdata(12).passive_rawid=120761;
ICdata(12).active_spike='bbl029f04_a_PTD.spk.mat';
ICdata(12).active_rawid=120764;
ICdata(12).on_BF=1; % 1=on BF target, 0=off BF
ICdata(12).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active easy
ICdata(13).cellid='bbl029f-a1';
ICdata(13).area='ICc';
ICdata(13).type='Var_SNR';
ICdata(13).note='Good isolation P99 A95, iso a bit worse in A; bad behavior';
ICdata(13).datapath='/auto/data/daq/Babybell/bbl029/sorted/';
ICdata(13).passive_spike='bbl029f03_p_PTD.spk.mat';
ICdata(13).passive_rawid=120761;
ICdata(13).active_spike='bbl029f05_a_PTD.spk.mat';
ICdata(13).active_rawid=120767;
ICdata(13).on_BF=1; % 1=on BF target, 0=off BF
ICdata(13).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy 1
ICdata(14).cellid='bbl030e-a1';
ICdata(14).area='ICc';
ICdata(14).type='Var_SNR';
ICdata(14).note='Good isolation P90 A95';
ICdata(14).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(14).passive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(14).passive_rawid=120792;
ICdata(14).active_spike='bbl030e07_a_PTD.spk.mat';
ICdata(14).active_rawid=120793;
ICdata(14).on_BF=1; % 1=on BF target, 0=off BF
ICdata(14).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard 1
ICdata(15).cellid='bbl030e-a1';
ICdata(15).area='ICc';
ICdata(15).type='Var_SNR';
ICdata(15).note='Good isolation P95 A95';
ICdata(15).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(15).passive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(15).passive_rawid=120792;
ICdata(15).active_spike='bbl030e08_a_PTD.spk.mat';
ICdata(15).active_rawid=120797;
ICdata(15).on_BF=1; % 1=on BF target, 0=off BF
ICdata(15).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active easy 2
ICdata(16).cellid='bbl030e-a1';
ICdata(16).area='ICc';
ICdata(16).type='Var_SNR';
ICdata(16).note='Good isolation P95 A95; same passive as prev';
ICdata(16).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(16).passive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(16).passive_rawid=120792;
ICdata(16).active_spike='bbl030e11_a_PTD.spk.mat';
ICdata(16).active_rawid=120801;
ICdata(16).on_BF=1; % 1=on BF target, 0=off BF
ICdata(16).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard 2
ICdata(17).cellid='bbl030e-a1'; 
ICdata(17).area='ICc';
ICdata(17).type='Var_SNR';
ICdata(17).note='Good isolation P95 A95; same passive as prev';
ICdata(17).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(17).passive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(17).passive_rawid=120792;
ICdata(17).active_spike='bbl030e12_a_PTD.spk.mat';
ICdata(17).active_rawid=120806;
ICdata(17).on_BF=1; % 1=on BF target, 0=off BF
ICdata(17).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active easy
ICdata(18).cellid='bbl032f-a1';
ICdata(18).area='ICc';
ICdata(18).type='Var_SNR';
ICdata(18).note='Good isolation P99 A99, suppression on target? Off response higher freq';
ICdata(18).datapath='/auto/data/daq/Babybell/bbl032/sorted/';
ICdata(18).passive_spike='bbl032f02_p_PTD.spk.mat';
ICdata(18).passive_rawid=120899;
ICdata(18).active_spike='bbl032f03_a_PTD.spk.mat';
ICdata(18).active_rawid=120901;
ICdata(18).on_BF=0; % 1=on BF target, 0=off BF
ICdata(18).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active hard
ICdata(19).cellid='bbl032f-a1';
ICdata(19).area='ICc';
ICdata(19).type='Var_SNR';
ICdata(19).note='Good isolation P99 A95, suppression on target? Off response higher freq';
ICdata(19).datapath='/auto/data/daq/Babybell/bbl032/sorted/';
ICdata(19).passive_spike='bbl032f02_p_PTD.spk.mat';
ICdata(19).passive_rawid=120899;
ICdata(19).active_spike='bbl032f04_a_PTD.spk.mat';
ICdata(19).active_rawid=120903;
ICdata(19).on_BF=0; % 1=on BF target, 0=off BF
ICdata(19).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active easy
ICdata(20).cellid='bbl034e-a1';
ICdata(20).area='ICc';
ICdata(20).type='Var_SNR';
ICdata(20).note='Great isolation P99 A99; no fixed TORC';
ICdata(20).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(20).passive_spike='bbl034e02_p_PTD.spk.mat';
ICdata(20).passive_rawid=120983;
ICdata(20).active_spike='bbl034e03_a_PTD.spk.mat';
ICdata(20).active_rawid=120984;
ICdata(20).on_BF=1; % 1=on BF target, 0=off BF
ICdata(20).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(21).cellid='bbl034e-a1';
ICdata(21).area='ICc';
ICdata(21).type='Var_SNR';
ICdata(21).note='Great isolation P99 A99; no fixed TORC';
ICdata(21).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(21).passive_spike='bbl034e02_p_PTD.spk.mat';
ICdata(21).passive_rawid=120983;
ICdata(21).active_spike='bbl034e04_a_PTD.spk.mat';
ICdata(21).active_rawid=120985;
ICdata(21).on_BF=1; % 1=on BF target, 0=off BF
ICdata(21).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active easy
ICdata(22).cellid='bbl036e-a1';
ICdata(22).area='ICc';
ICdata(22).type='Var_SNR';
ICdata(22).note='Good isolation P95 A95; no fixed TORC; no second A';
ICdata(22).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(22).passive_spike='bbl036e05_p_PTD.spk.mat';
ICdata(22).passive_rawid=121034;
ICdata(22).active_spike='bbl036e06_a_PTD.spk.mat';
ICdata(22).active_rawid=121035;
ICdata(22).on_BF=1; % 1=on BF target, 0=off BF
ICdata(22).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active hard
ICdata(23).cellid='bbl039d-a1';
ICdata(23).area='ICc';
ICdata(23).type='Var_SNR';
ICdata(23).note='Great isolation P99 A99; controversial on/off BF';
ICdata(23).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(23).passive_spike='bbl039d02_p_PTD.spk.mat';
ICdata(23).passive_rawid=121282;
ICdata(23).active_spike='bbl039d04_a_PTD.spk.mat';
ICdata(23).active_rawid=121284;
ICdata(23).on_BF=0; % 1=on BF target, 0=off BF
ICdata(23).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy
ICdata(24).cellid='bbl039d-a1';
ICdata(24).area='ICc';
ICdata(24).type='Var_SNR';
ICdata(24).note='Great isolation P99 A99; controversial on/off BF';
ICdata(24).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(24).passive_spike='bbl039d02_p_PTD.spk.mat';
ICdata(24).passive_rawid=121282;
ICdata(24).active_spike='bbl039d05_a_PTD.spk.mat';
ICdata(24).active_rawid=121285;
ICdata(24).on_BF=0; % 1=on BF target, 0=off BF
ICdata(24).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active hard
ICdata(25).cellid='bbl041e-a1';
ICdata(25).area='ICc';
ICdata(25).type='Var_SNR';
ICdata(25).note='Great isolation P99 A99';
ICdata(25).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(25).passive_spike='bbl041e02_p_PTD.spk.mat';
ICdata(25).passive_rawid=121417;
ICdata(25).active_spike='bbl041e03_a_PTD.spk.mat';
ICdata(25).active_rawid=121418;
ICdata(25).on_BF=0; % 1=on BF target, 0=off BF
ICdata(25).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy
ICdata(26).cellid='bbl041e-a1';
ICdata(26).area='ICc';
ICdata(26).type='Var_SNR';
ICdata(26).note='Great isolation P99 A99';
ICdata(26).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(26).passive_spike='bbl041e02_p_PTD.spk.mat';
ICdata(26).passive_rawid=121417;
ICdata(26).active_spike='bbl041e04_a_PTD.spk.mat';
ICdata(26).active_rawid=121419;
ICdata(26).on_BF=0; % 1=on BF target, 0=off BF
ICdata(26).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (even if it occurred after another active) - Active easy
ICdata(27).cellid='bbl041e-a1';
ICdata(27).area='ICc';
ICdata(27).type='Var_SNR';
ICdata(27).note='Great isolation P99 A99';
ICdata(27).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(27).passive_spike='bbl041e06_p_PTD.spk.mat';
ICdata(27).passive_rawid=121421;
ICdata(27).active_spike='bbl041e07_a_PTD.spk.mat';
ICdata(27).active_rawid=121422;
ICdata(27).on_BF=1; % 1=on BF target, 0=off BF
ICdata(27).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(28).cellid='bbl041e-a1';
ICdata(28).area='ICc';
ICdata(28).type='Var_SNR';
ICdata(28).note='Great isolation P99 A95';
ICdata(28).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(28).passive_spike='bbl041e06_p_PTD.spk.mat';
ICdata(28).passive_rawid=121421;
ICdata(28).active_spike='bbl041e08_a_PTD.spk.mat';
ICdata(28).active_rawid=121423;
ICdata(28).on_BF=1; % 1=on BF target, 0=off BF
ICdata(28).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active pure tone 
ICdata(29).cellid='bbl074g-a1';
ICdata(29).area='ICc';
ICdata(29).type='Var_SNR';
ICdata(29).note='OK isolation P99 A99; off and on responses'
ICdata(29).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
ICdata(29).passive_spike='bbl074g03_p_PTD.spk.mat';
ICdata(29).passive_rawid=125662;
ICdata(29).active_spike='bbl074g05_a_PTD.spk.mat';
ICdata(29).active_rawid=125665;
ICdata(29).on_BF=1; % 1=on BF target, 0=off BF
ICdata(29).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active hard
ICdata(30).cellid='bbl074g-a1';
ICdata(30).area='ICc';
ICdata(30).type='Var_SNR';
ICdata(30).note='OK isolation P99 A99; off and on responses'
ICdata(30).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
ICdata(30).passive_spike='bbl074g03_p_PTD.spk.mat';
ICdata(30).passive_rawid=125662;
ICdata(30).active_spike='bbl074g07_a_PTD.spk.mat';
ICdata(30).active_rawid=125668;
ICdata(30).on_BF=1; % 1=on BF target, 0=off BF
ICdata(30).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
 
% %--------------------------------------------------------------------------
 
% Pre-passive - Active easy 
ICdata(31).cellid='bbl078k-a1';
ICdata(31).area='ICc';
ICdata(31).type='Var_SNR';
ICdata(31).note='OK isolation P95 A95; off and on responses; sorted all at once'
ICdata(31).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(31).passive_spike='bbl078k03_p_PTD.spk.mat';
ICdata(31).passive_rawid=125886;
ICdata(31).active_spike='bbl078k04_a_PTD.spk.mat';
ICdata(31).active_rawid=125887;
ICdata(31).on_BF=1; % 1=on BF target, 0=off BF
ICdata(31).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(32).cellid='bbl078k-a1';
ICdata(32).area='ICc';
ICdata(32).type='Var_SNR';
ICdata(32).note='OK isolation P95 A95; off and on responses; sorted all at once'
ICdata(32).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(32).passive_spike='bbl078k03_p_PTD.spk.mat';
ICdata(32).passive_rawid=125886;
ICdata(32).active_spike='bbl078k05_a_PTD.spk.mat';
ICdata(32).active_rawid=125888;
ICdata(32).on_BF=1; % 1=on BF target, 0=off BF
ICdata(32).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(33).cellid='bbl078k-a1';
ICdata(33).area='ICc';
ICdata(33).type='Var_SNR';
ICdata(33).note='OK isolation P95 A95; off and on responses; sorted all at once'
ICdata(33).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(33).passive_spike='bbl078k03_p_PTD.spk.mat';
ICdata(33).passive_rawid=125886;
ICdata(33).active_spike='bbl078k07_a_PTD.spk.mat';
ICdata(33).active_rawid=125890;
ICdata(33).on_BF=1; % 1=on BF target, 0=off BF
ICdata(33).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active pure tone
ICdata(34).cellid='bbl081d-a1';
ICdata(34).area='ICx';
ICdata(34).type='Var_SNR';
ICdata(34).note='OK isolation P95 A95; sorted all at once'
ICdata(34).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(34).passive_spike='bbl081d03_p_PTD.spk.mat';
ICdata(34).passive_rawid=126472;
ICdata(34).active_spike='bbl081d04_a_PTD.spk.mat';
ICdata(34).active_rawid=126473;
ICdata(34).on_BF=0; % 1=on BF target, 0=off BF
ICdata(34).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard 
ICdata(35).cellid='bbl081d-a1';
ICdata(35).area='ICx';
ICdata(35).type='Var_SNR';
ICdata(35).note='OK isolation P95 A95; sorted all at once'
ICdata(35).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(35).passive_spike='bbl081d03_p_PTD.spk.mat';
ICdata(35).passive_rawid=126472;
ICdata(35).active_spike='bbl081d06_a_PTD.spk.mat';
ICdata(35).active_rawid=126475;
ICdata(35).on_BF=0; % 1=on BF target, 0=off BF
ICdata(35).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% % save the struct in data folder
% fname='/auto/users/daniela/ICdata';
% save(fname,'ICdata');
% 























