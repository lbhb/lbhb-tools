function [ssa, fig] = ssa_simulation(cellid, depstr, deptau)
%SSA_SIMULATION(CELLID, DEPSTR, DEPTAU)
%Plots a cartoon of depression/facilitation during an SSA stimulus.
%Inputs:
% CELLID: a label for the plot
% DEPSTR: matrix of depression strengths (arbitrary units), 
%  channels x "synapses"
% DEPTAU: matrix of vesicle recovery constants (10s of ms), 
%  channels x "synapses"
%Returns:
% FIG: figure handle of the plot
% SSA: estimated SSA index for each channel
%ZPS, 9/8/15
fs =  100; %internal sampling rate (hz)
dur = 0.1; %noise burst duration (s)
isi = 0.3; %interstimulus interval (s)
%common stimulus
s(1,:) = [repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 4]) ...
  zeros(1, isi*fs) zeros(1, dur*fs) ...
  repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 8]) ...
  zeros(1, isi*fs) zeros(1, dur*fs) ...
  repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 7]) ...
  0];
%rare stimulus
s(2,:) = [repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 4]) ...
  zeros(1, isi*fs) ones(1, dur*fs) ...
  repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 8]) ...
  zeros(1, isi*fs) ones(1, dur*fs) ...
  repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 7]) ...
  0];
%common stimulus
%s(1,:) = [repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 8]) ...
%  zeros(1, isi*fs) zeros(1, dur*fs) ...
%  repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 21]) ...
%  zeros(1, isi*fs) zeros(1, dur*fs) ...
%  repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 9]) ...
%  0];
%%rare stimulus
%s(2,:) = [repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 8]) ...
%  zeros(1, isi*fs) ones(1, dur*fs) ...
%  repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 21]) ...
%  zeros(1, isi*fs) ones(1, dur*fs) ...
%  repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 9]) ...
%  0];
%noise bursts in each condition
bursts = [18 2];
%scale depression/facilitation strength from percentage to fraction of stim
str_norm = max(s(:)).*fs;
%taus are in 10s of ms, convert to time bins
tau_norm = fs/100;     
t = 0:1/fs:(size(s,2)-1)/fs;
fig = figure
[synapses, channels] = size(depstr);
for c = 1:channels
  u = depstr(:,c)./str_norm;
  tau = deptau(:,c).*tau_norm;
  for condition = 1:2
    subplot(channels,2,(c*2)-(condition-1))
    stim = s(condition,:);
    dstim = adp_bank(stim, u, tau);
    %dstim = sum(dstim,1)./synapses; %average input across synapses
    subplot(channels,2,(c*2)-(condition-1))
    hold on
    plot(t, stim, 'g', 'linewidth', 1)
    plot(t, dstim', 'k', 'linewidth', 2)
    plot(t, s(mod(condition,2)+1,:), 'color', [1 0.5 0], 'linewidth', 1) 
    ylabel('Stimulus')
    title(['Channel ' sprintf('%d ', c) ...
           'Tau: '    sprintf('%0.1f ', deptau(:,c)) ...
           'U: '      sprintf('%0.1f ', depstr(:,c))]);
    axis tight
    xlabel('Time (s)')
    %find the average response per noise burst
    %leave out the first noise burst (onset)
    resp(condition) = sum(dstim((((isi+dur)*fs)+1):end))/bursts(condition);
  end
  ssa(c) = (resp(2)-resp(1))/(resp(2)+resp(1));
end
l = suptitle([cellid ' SSA: ' sprintf('%0.2f ', ssa)], 0);
set(l, 'interpreter', 'none')
hold off
