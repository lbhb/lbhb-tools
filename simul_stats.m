narf_set_path;
dbopen;
global STACK META XXX

RELOAD=1;
METHOD=1;

%rawid=59383; % lime lim015a
%rawid=73871; % onion oni005m
%rawid=73870; % onion oni005m val
%rawid=94600; %fidx=5; % portabello por031a
%rawid=103577; %fidx=16; % stinkhorn sti021c 
%rawid=103588; %fidx=17; % stinkhorn sti021d
%SPN
%rawid=56314; % min021a
%rawid=56474; % min022a
%rawid=63438; % dai003b
%rawid=68404; % mag006b
%rawid=59590; % lime lim017a
%rawid=73875; % oni005m
%rawid=73876; % oni005m VAL
%rawid=93950; % por026b
%rawid=95029; % por049a

% NAT
rawid=116169; %zee015h05

if ~exist('RELOAD','var') || RELOAD || ...
        ~exist('pred','var') || saverawid~=rawid,
    
    saverawid=rawid;
    sql=['SELECT rawid,masterid,path,respfile,repcount,stimfile,'...
         ' runclassid, count(rawid) as unitcount, goodtrials',...
         ' FROM sCellFile',...
         ' WHERE runclassid in (4,8,112) and area="A1" ',...
         ' GROUP BY rawid HAVING unitcount>6 ORDER BY rawid'];
    mdata=mysql(sql);

    rawids=cat(1,mdata.rawid);
    fidx=find(rawids==rawid);

    runclassid=mdata(fidx).runclassid;
    spkfile=[mdata(fidx).path mdata(fidx).respfile];
    stimfile=mdata(fidx).stimfile;
    
    switch runclassid,
       case 4,
          batches=246;
          %modelname='fb18ch100_lognn_wcg03_ap3z1_dexp_fit05v';
          modelname='fb18ch100_lognn_wcg03_adp1pc_ap3z1_dexp_fit05v';
          rasterfs=100;
       case 8,
          batches=[179 240];
          modelname='env100_logn_fir15_siglog100_fit05h';
          rasterfs=100;
       case 112,
          batches=289;
          %modelname='fb18ch100_lognn_wcg03_ap3z1_dexp_fit05v';
          modelname='fb18ch100_lognn_wcg02_fir15_dexp_fit05a';
          rasterfs=100;
    end
    baphy_parms=dbReadData(rawid);
    
    PreStimBins=round(baphy_parms.Ref_PreStimSilence.*rasterfs);
    PostStimBins=round(baphy_parms.Ref_PostStimSilence.*rasterfs);
    
    bb=basename(spkfile);
    sql=['SELECT cellid,channum,unit FROM sCellFile WHERE respfile="',bb,'"',...
         ' ORDER BY cellid'];
    fdata=mysql(sql);
    unitset=sort(cat(1,fdata.channum).*10+cat(1,fdata.unit));
    channel=floor(unitset/10);
    unit=mod(unitset,10);
    cellids={fdata.cellid};
    cellcount=length(cellids);

    bidx=1;
    perf={[]};
    while isempty(cat(1,perf{:})) && bidx<=length(batches),
        batch=batches(bidx);
        [stacks,metas,x0s,perf]=load_model_batch(batch,cellids,{modelname});
        bidx=bidx+1;
    end
    resp=[];
    pred=[];
    keepid=[];
    perfs=[];
    exclude_cellids={'oni005m-05-1' 'oni005m-13-1',...
       'zee015h-10-1','zee015h-15-1','zee015h-17-1',...
       'zee015h-19-1','zee015h-19-2'};
    for ii=1:cellcount,
        if ~isempty(stacks{ii}) && perf{ii}(4)>0.15 &&...
                ~sum(strcmp(cellids{ii},exclude_cellids)),
            perfs=cat(1,perfs,perf{ii});
            STACK=stacks{ii};
            XXX={x0s{ii}};
            keepid=[keepid ii];
            files=cat(2,XXX{1}.training_set,XXX{1}.test_set);
            
            if sum(strcmp(stimfile,files)),
                s2=stimfile;
            elseif sum(strcmp(files,[stimfile,'_est'])),
                XXX{1}.test_set={stimfile};
                s2=stimfile;
            end
            update_xxx(2);
            if isempty(pred),
                %resp=XXX{end}.dat.(s2).resp;
                pred=XXX{end}.dat.(s2).stim;
            else
                %resp=cat(4,r,XXX{end}.dat.(s2).resp);
                pred=cat(4,pred,XXX{end}.dat.(s2).stim);
            end
        end
    end
    pred=permute(pred,[1 3 2 4]);

    cellids=cellids(keepid);
    unit=unit(keepid);
    channel=channel(keepid);
    options=struct('rasterfs',rasterfs,'includeprestim',1,'meansub',0,...
                   'channel',channel,'unit',unit,'tag_masks',{{'Reference'}});
                
    %if ~isempty(mdata(fidx).goodtrials),
    %   options.trialrange=eval(mdata(fidx).goodtrials);
    %end
    [r,tags,trialset]=loadsiteraster(spkfile,options);
    r=r.*rasterfs;
    repcount=size(r,2);
    stimcount=size(r,3);
    cellcount=size(r,4);

    % sort by trial order
    validtrials=unique(trialset(trialset>0 & ~isnan(trialset)))';
    trialcount=length(validtrials);
    si=zeros(trialcount,1);
    for tt=1:length(validtrials),
       si(tt)=find(trialset(:)==validtrials(tt));
    end
%     trialcount=nanmax(trialset(:));
%     si=zeros(trialcount,1);
%     for tt=1:trialcount,
%         si(tt)=find(trialset(:)==tt);
%     end
%     %[~,si]=sort(trialset(~isnan(trialset)));

    pred=repmat(pred,[1 repcount 1 1]);
    r=permute(r,[1 4 2 3]);
    pred=permute(pred,[1 4 2 3]);
    r=r(:,:,si);
    pred=pred(:,:,si);
    
    strfs=get_strf_from_model(batch,cellids,{modelname});
    figure
    colcount=ceil(sqrt(length(strfs)));
    rowcount=ceil(length(strfs)./colcount);
    for ii=1:length(strfs),
       subplot(rowcount,colcount,ii);
       ff=round(2.^linspace(log2(200),log2(20000),size(strfs{ii},1)));
       plotastrf(strfs{ii},1,ff,500);
       title(sprintf('%s (%.2f)',cellids{ii},perfs(ii,4)),...
          'Interpreter','none');
    end
end

% figure out gain and spont diff for each trial
disp('Baseline/gain analysis');
g=zeros(cellcount,trialcount);
b=zeros(cellcount,trialcount);
e=zeros(cellcount,trialcount);
trial_box=4;
N=permute(r,[2 1 3]);
N=sqrt(nanmean(N(:,:).^2,2));
for cc=1:cellcount,
    for tt=1:trialcount,
        if tt>trial_box && tt<trialcount-trial_box+1,
            ttr=(tt-trial_box):(tt+trial_box);
        elseif tt<=trial_box,
            ttr=1:(tt+trial_box);
        else
            ttr=(tt-trial_box):trialcount;
        end
        
        tp=pred(:,cc,ttr);
        tr=r(:,cc,ttr);
        T=size(r,1);
        b0=mean(mean(tr([1:PreStimBins (T-PostStimBins+20):T],:)))-...
           mean(mean(tp([1:PreStimBins (T-PostStimBins+20):T],:)));
        tp=tp(:);
        tr=tr(:);
        
        if METHOD==0,
           x0=mean(mean(tp([1:PreStimBins (T-PostStimBins+20):T],:)));
           y0=mean(mean(tr([1:PreStimBins (T-PostStimBins+20):T],:)));
           
           x=[tp(:) ones(size(tp(:)))];
           %x=[tp(:)-x0];
           y=[tr(:)];
           [beta,bint]=regress(y,x);
           g(cc,tt)=beta(1);
           b(cc,tt)=beta(2);
           
        else
           %smwin=ones(7,1)./7;
           smwin=[linspace(0,1,6) linspace(1,0,6)]';
           %smwin=[zeros(1,5) 1 linspace(1,0,6)]';
           smwin=smwin([1:6 8:12]);
           smwin=smwin./sum(smwin);
           tr=rconv2(tr,smwin);
        
           bincount=12;
           sp=sort(tp);
           sp(end+1)=max(sp)+1;
           edges=sp(round(linspace(1,length(sp),bincount+1)));
           edges=unique(edges);
           bincount=length(edges)-1;
           pbinned=zeros(bincount,1);
           rbinned=zeros(bincount,1);
           for bb=1:bincount,
              pbinned(bb)=mean(tp(tp>=edges(bb) & tp<edges(bb+1)));
              rbinned(bb)=mean(tr(tp>=edges(bb) & tp<edges(bb+1)));
           end
           
           if METHOD==1,
              % method 1
              b(cc,tt)=b0;
              pbinned=pbinned+b(cc,tt);
              g(cc,tt)=sum(pbinned.*rbinned)./sum(pbinned.^2);
           elseif METHOD==4,
              % method 4 -- linear regression
              
              x=[pbinned(:) ones(size(pbinned(:)))];
              y=[rbinned(:)];
              [beta,bint]=regress(y,x);
              g(cc,tt)=beta(1);
              b(cc,tt)=beta(2);
              
            elseif METHOD==2,
              % method 2
              m=(pbinned+rbinned)./2;
              d=(rbinned-pbinned)./2;
              % p(1)=gain, p(2)=baseline
              p=polyfit(m,d,1);
              %p=polyfit(tp,tr,1);
              g(cc,tt)=p(1);
              b(cc,tt)=p(2);
           elseif METHOD==3,
              % method 3
              % p(1)=gain, p(2)=baseline
              p=polyfit(tp,tr,1);
              g(cc,tt)=p(1);
              b(cc,tt)=p(2);
           end
        end
        e(cc,tt)=std(tp-tr)./N(cc);
        %e(cc,tt)=std(pbinned-rbinned)./std(rbinned);
    end
end

g(g<0.5)=0.5;
g(g>2)=2;

if METHOD==2
    g1=log2(1+g);
else
    g1=log2(g);
end

x=[g1;b];
x=x./repmat(std(x,0,2),[1,size(x,2)]);
[u,s,v]=svd(x);


figure;
subplot(5,1,1);
plot(g1');
hold on
mm=nanmean(g1,1);
if METHOD==2 || 1,
    plot([0 length(mm+1)],[0 0],'k--');
else
    plot([0 length(mm+1)],[1 1],'k--');
end
plot(mm,'k','LineWidth',2);
hold off
title(sprintf('%s (n=%d) (METHOD %d)',stimfile,cellcount,METHOD),...
      'Interpreter', 'none');
ylabel('gain');

subplot(5,1,2);
plot(b');
hold on
mm=nanmean(b,1);
plot([0 length(mm+1)],[0 0],'k--');
plot(mm,'k','LineWidth',2);
hold off
ylabel('baseline change');

subplot(5,1,3);
mm=nanmean(e,1);
ee=nanstd(e);
errorshade(1:length(mm),mm,ee,[0 0 0],[0.8 0.8 0.8]);
hold on
plot(e');
plot([0 length(mm+1)],[0 0],'k--');
plot(mm,'k','LineWidth',2);
hold off
ylabel('error');
xlabel('trial');

subplot(5,1,4);
plot(v(:,1:2));

subplot(5,2,9);
plot(u(:,1:2));

subplot(5,2,10);
plot(v(:,1),v(:,2));
hold on
hp=plot(v(1,1),v(1,2),'o');
hp=plot(v(end,1),v(end,2),'o','MarkerFaceColor',[0 0 0]);
hold off
xlabel('PC 1');
ylabel('PC 2');


return






spont=mean(r(1:40,:,:,:));
spont=permute(spont,[4 2 3 1]);
spont=spont(:,si);
spont=spont-repmat(mean(spont,2),[1 trialcount]);
spont=spont./repmat(std(spont,0,2),[1 trialcount]);

figure;
subplot(3,1,1);
imagesc(spont);

[u,s,v]=svd(spont);
subplot(3,2,3);
plot(u(:,1:2));
subplot(3,2,4);
plot(diag(s));

if size(r,3)<=2,
    e=nanmean(r(41:340,:,:,:))-nanmean(r(1:40,:,:,:));
    em=squeeze(nanmax(nanmax(abs(e))));
    e1=squeeze(e(:,:,1,:))'./repmat((em),[1 size(e,2)]);
    e2=squeeze(e(:,:,2,:))'./repmat((em),[1 size(e,2)]);
    
    subplot(3,2,5);
    imagesc(e1);
    subplot(3,2,6);
    imagesc(e2);
end
