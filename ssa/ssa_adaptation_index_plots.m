%Script to produce population plots of SSA data.
%ZPS, 8/30/15

archive_path = ...
  ['/auto/users/schwarza/ssa_spn/plots/' datestr(now(),'yyyy_mm_dd') '/'];
if ~exist(archive_path)
  mkdir(archive_path);
end
cache_path = '/auto/users/schwarza/code/ssa/cache/';

%get a list of SSA cells
ssa_cells = dbgetscellfile('runclass', 'SSA');
ssa_cellids = {ssa_cells.cellid};
start_idx = find(strcmp(ssa_cellids, 'chn062c-c1')); %half-max protocol only
cellids = ssa_cellids(start_idx:end);
%get a list of SSA/SPN cells
%ssa_cells = dbgetscellfile('runclass', 'SSA');
%ssa_cellids = {ssa_cells.cellid};
%spn_cells = mysql('SELECT cellid FROM NarfResults WHERE batch=259');
%spn_cellids = {spn_cells.cellid};
%cellids = intersect(ssa_cellids, spn_cellids);
%start_idx = find(strcmp(cellids, 'chn062c-c1')); %half-max protocol only
%cellids = cellids(start_idx:end);

%load and cache if not cached already
for cellidx=1:length(cellids)
  cellid = cellids{cellidx};
  if ~exist([cache_path cellid '.mat']);
    ssa_plot(cellid);
  end
end

%screen cells against criteria
ssa = nan(length(cellids), 2);
combined_ssa = nan(length(cellids), 1);
adapt = nan(length(cellids), 2);
facilitate = nan(length(cellids), 2);
delta_f = nan(length(cellids), 1);
evoked = nan(length(cellids), 6);
isolation = nan(length(cellids), 1);
crit.isolation = 80;
crit.level = 65;
crit.bandwidth = 0.125;
reject.isolation = 0;
reject.params = 0;
reject.sig = 0;
analyzed = 0;
for cellidx=1:length(cellids)
 cellid = cellids{cellidx};
 load([cache_path cellid '.mat'])
 if (cellfiledata.isolation < crit.isolation)
    reject.isolation = reject.isolation + 1;
    continue
 end
 if (exptparams.TrialObject.ReferenceHandle.Bandwidth ~= crit.bandwidth) && ...
    (exptparams.TrialObject.OveralldB ~= crit.level) 
    reject.params = reject.params + 1;
    continue
 end
 if ~is_sig
   reject.sig = reject.sig + 1;
   continue
 end
 analyzed = analyzed + 1;
 ssa(cellidx,:) = ssa_indexes.ssa;
 combined_ssa(cellidx,:) = ssa_indexes.combined_ssa;
 adapt(cellidx,:) = ssa_indexes.adapt;
 facilitate(cellidx,:) = ssa_indexes.facilitate;
 freqs = exptparams.TrialObject.ReferenceHandle.Frequencies;
 delta_f(cellidx) = log2(max(freqs)/min(freqs));
 evoked(cellidx, :) = mr_evoked;
 isolation(cellidx, :) = cellfiledata.isolation;
end
disp(sprintf('\nAnalyzing %d recordings.', length(cellids)));
disp(sprintf('%d rejected - isolation.', reject.isolation));
disp(sprintf('%d rejected - stimulus parameters incorrect.', reject.params));
disp(sprintf('%d rejected - did not respond to stimulus.', reject.sig));
disp(sprintf('%d included in analysis.', analyzed));

%plot ssa index
%fig = figure;
%hold on
%plot(ssa(:,1), ssa(:,2), '.', 'MarkerSize', 15,'Color',[0 0 0]);
%plot([0 0], [-1 1], 'k--');
%plot([-1 1], [0 0], 'k--');
%plot([1 -1], [-1 1], 'k--');
%xlabel('Frequency A');
%ylabel('Frequency B');
%[~, p] = ttest(ssa(:));
%above = sum((ssa(:,1)+ssa(:,2)) > 0);
%below = sum((ssa(:,1)+ssa(:,2)) <= 0);
%[phat, pci] = binofit(above, above+below);
%title(sprintf(['SSA Index, Mean: %0.3f (p = %0.8f, t-Test)\n' ...
%    'Above Diagonal: %d, Below Diagonal: %d, 95%% C.I. (Binomial Test):' ...
%    ' %0.2f %0.2f'], nanmean(ssa(:)), p, above, below, pci(1), pci(2)));
%axis square
%hold off
%print(fig, '-depsc', [archive_path 'ssa_index'])

%%plot combined SSA index
%fig = figure;
%hold on
%m = nanmedian(combined_ssa);
%hist(combined_ssa);
%aa = axis;
%plot([m m], [aa(3) aa(4)], 'k--');
%xlabel('Combined SSA Index')
%ylabel('Frequency')
%p = signrank(combined_ssa);
%title(sprintf(['Combined SSA Index: Median: %0.3f' ...
%  ' (p = %0.8f, Wilcoxon signed-rank test)'], m, p))
%axis square
%hold off
%print(fig, '-depsc', [archive_path 'combined_ssa_index'])

%plot tuning width
%fig = figure;
%hold on
%scatter(ssa(:,1), ssa(:,2), 50, delta_f, 'filled')
%plot([0 0], [-1 1], 'k--');
%plot([-1 1], [0 0], 'k--');
%plot([1 -1], [-1 1], 'k--');
%xlabel('Frequency A');
%ylabel('Frequency B');
%title('SSA Index')
%axis square
%c = colorbar;
%title(c, 'Delta F (oct)')
%print(fig, '-depsc', [archive_path 'ssa_delta_f'])

%plot isolation
%fig = figure;
%plot(isolation, combined_ssa, '.','MarkerSize',15,'Color',[0 0 0]);
%xlabel('Isolation (%)')
%ylabel('Combined SSA Index')
%axis square
%print(fig, '-depsc', [archive_path 'ssa_isolation'])

%plot adaptation index
%fig = figure;
%hold on
%plot(adapt(:,1), adapt(:,2), '.','MarkerSize',15,'Color',[0 0 0]);
%plot([0 0], [-1 1], 'k--');
%plot([-1 1], [0 0], 'k--');
%plot([1 -1], [-1 1], 'k--');
%xlabel('Frequency A');
%ylabel('Frequency B');
%[~, p] = ttest(adapt(:));
%above = sum((adapt(:,1)+adapt(:,2)) > 0);
%below = sum((adapt(:,1)+adapt(:,2)) <= 0);
%[phat, pci] = binofit(above, above+below);
%title(sprintf(['Adaptation Index, Mean: %0.3f (p = %0.9f, t-Test)\n' ...
%    'Above Diagonal: %d, Below Diagonal: %d, 95%% C.I. (Binomial Test):' ...
%    ' %0.2f %0.2f'], nanmean(adapt(:)), p, above, below, pci(1), pci(2)));
%axis square
%hold off
%print(fig, '-depsc', [archive_path 'adaptation_index']);

%plot facilitation indices
%fig = figure;
%hold on
%plot(facilitate(:,1), facilitate(:,2), 'r.','MarkerSize',15,'Color',[0 0 0]);
%plot([0 0], [-1 1], 'k--');
%plot([-1 1], [0 0], 'k--');
%plot([1 -1], [-1 1], 'k--');
%xlabel('Frequency A');
%ylabel('Frequency B');
%[~, p] = ttest(facilitate(:));
%above = sum((facilitate(:,1)+facilitate(:,2)) > 0);
%below = sum((facilitate(:,1)+facilitate(:,2)) <= 0);
%[phat, pci] = binofit(above, above+below);
%title(sprintf(['Facilitation Index, Mean: %0.3f (p = %0.9f, t-Test)\n' ...
%    'Above Diagonal: %d, Below Diagonal: %d, 95%% C.I. (Binomial Test):' ...
%    ' %0.2f %0.2f'], nanmean(facilitate(:)), p, above, below, pci(1), pci(2)));
%axis square
%hold off
%print(fig, '-depsc', [archive_path 'facilitation_index']);

%plot standard vs. deviant
%rare = [1 4];
%common = [2 5];
%fig = figure;
%hold on
%evoked_rare = nanmean(evoked(:,rare),2);
%evoked_common = nanmean(evoked(:,common),2);
%difference = evoked_common - evoked_rare;
%plot(evoked_rare, evoked_common, '.', 'MarkerSize', 15, 'Color', [0 0 0]);
%[~, p] = ttest(evoked_rare, evoked_common);
%above = sum((difference)>=0);
%below = sum((difference)<0);
%[phat, pci] = binofit(above, above+below);
%title(sprintf(['Average Response, Mean Difference: %0.1f' ...
%               ' (P = %0.4f, Paired t-Test)\n', ...
%               'Above Diagonal: %d, Below Diagonal: %d,' ...
%               ' C.I. (Binomial Test): %0.2f %0.2f'], ...
%               nanmean(difference), p, above, below, pci(1), pci(2)));
%xlabel('Rare (spikes/s)')
%ylabel('Common (spikes/s)')
%max_val = round(max([evoked_rare; evoked_common]));
%plot([0 max_val], [0 max_val], 'k--')
%axis tight square
%hold off
%print(fig, '-depsc', [archive_path 'standard_vs_deviant'])

%plot SSA vs. cross-adaptation
combined_crossadapt = -nanmean(facilitate,2);
fig = figure;
hold on
plot(combined_ssa, combined_crossadapt, '.','MarkerSize',15,'Color',[0 0 0]);
plot([0 0], [min(combined_crossadapt) max(combined_crossadapt)], 'k--')
plot([min(combined_ssa) max(combined_ssa)], [0 0], 'k--')
text(combined_ssa, combined_crossadapt, cellids, 'FontSize', 8)
hold off
xlabel('SSA')
ylabel('Cross-Adaptation')
axis tight square
