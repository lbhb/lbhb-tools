function subs=UTget_subs(N,M)

if exist('M','var'),
   subs={N,M};
elseif(N==1)
   subs={1,1};
elseif(N<=2)
   subs={1,2};
elseif(N<=3)
   subs={3,1};
elseif(N<=4)
   subs={2,2};
elseif(N<=6)
   subs={3,2};
elseif(N<=8)
   subs={4,2};
elseif(N<=12)
   subs={4,3};
elseif(N<=15)
   subs={5,3};
elseif(N<=16)
   subs={4,4};
elseif(N<=20)
   subs={5,4};
elseif(N<=24)
   subs={6,4};
elseif(N<=30)
   subs={6,5};
elseif(N<=35)
   subs={7,5};
else
   subs={floor(sqrt(N)),ceil(sqrt(N))};
   if(subs{1}*subs{2}<N)
      subs{2}=subs{2}+1;
   end
end
    
end