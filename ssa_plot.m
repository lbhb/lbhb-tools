function fig = ssa_plot(cellid, options)
%SSA_PLOT(CELLID, OPTIONS)
%
%Produces raster and PSTH plots of response to SSA stimulus, separated by
%common, rare, and onset conditions. Also calculates SSA/adaptation/facilitation
%indexes and caches data.
%
%CELLID: Cell to analyze/plot (e.g. 'eno002c-c2').
%OPTIONS: An optional structure with the following fields
%  fig: Figure handle for all plots.
%  psth_ax1, psth_ax2: Axis handle for PSTHs of response to each frequency.
%  raster_ax: Axis handle for raster. Does not plot raster if raster_ax = 0.
 
  %input parsing
  if (~exist('options', 'var'))
    fig = figure;
    ax1 = subplot(3,1,1);
    ax2 = subplot(3,1,2);
    ax3 = subplot(3,1,3);
    options = struct('fig', fig, ...
                     'add_title', 1, ...
                     'psth_ax1', ax1, ...
                     'psth_ax2', ax2, ...
                     'raster_ax', ax3);
  end
  %load and cache data
  start_dir = pwd;
  cd /auto/users/schwarza/code/ssa/cache
  rare   = [1 4];
  common = [2 5];
  onset  = [3 6];
  if exist([cellid '.mat'], 'file')
    load([cellid '.mat']);
  else
    rasterfs = 100;
    ssa_cells = dbgetscellfile('runclass','SSA');
    n = find(strcmp({ssa_cells.cellid}, cellid));
    cellfiledata = ssa_cells(n);
    stimpath = cellfiledata.stimpath;
    stimfile = cellfiledata.stimfile;
    parmfile = [stimpath stimfile];
    LoadMFile(parmfile);
    options.rasterfs = rasterfs;
    options.usesorted = 1;
    [r, tags] = raster_load(parmfile, cellfiledata.channum, cellfiledata.unit, ...
      options);
    options.rasterfs = 1000;
    r_1000 = raster_load(parmfile, cellfiledata.channum, cellfiledata.unit, ...
      options);
    %calculate z-score for each time bin
    pipdur = exptparams.TrialObject.ReferenceHandle.PipDuration;
    prepip = exptparams.TrialObject.ReferenceHandle.PipInterval./2;
    start = round(prepip*rasterfs)+1; %bin in which stimulus begins
    trial_spont = repmat(nanmean(r(1:(start-1),:,:),1), size(r,1), 1);
    trial_m_with_spont = nanmean(r,2)*rasterfs;
    trial_sem_with_spont = (nanstd(r,0,2)*rasterfs)./sqrt(sum(~isnan(r),2));
    trial_m = nanmean((r-trial_spont),2)*rasterfs;
    trial_sem  = (nanstd((r-trial_spont),0,2)*rasterfs)./sqrt(sum(~isnan(r),2));
    z = trial_m./trial_sem;
    %cells with at least one significant time bin (z > 3) in the response to
    %common, rare or onset stimuli at both frequencies are considered significant
    sigbin_threshold = 3;
    sigcell_threshold = 1;
    sigwindow = [round((prepip+0.01)*rasterfs+1) : round((prepip+0.12)*rasterfs)];
    rare_sigbin_count = sum(z(sigwindow,:,rare)>sigbin_threshold);
    common_sigbin_count = sum(z(sigwindow,:,common)>sigbin_threshold);
    onset_sigbin_count = sum(z(sigwindow,:,onset)>sigbin_threshold);
    is_sig = sum((rare_sigbin_count + onset_sigbin_count + common_sigbin_count)...
      >=sigcell_threshold,3)==2;
    if is_sig
      sig_str = 'Significant';
    else
      sig_str = 'Not Significant';
    end
    %significant time bins
    onset_sig = cat(3, repmat(z(:,:,onset(1)),[1 1 3]), ...
      repmat(z(:,:,onset(2)),[1 1 3]));
    rare_sig = cat(3, repmat(z(:,:,rare(1)),[1 1 3]), ...
      repmat(z(:,:,rare(2)),[1 1 3]));
    common_sig = cat(3, repmat(z(:,:,common(1)),[1 1 3]), ...
      repmat(z(:,:,common(2)),[1 1 3]));
    sigbins = false(size(r,1), 1, size(r,3));
    sigbins(sigwindow,:,:) = 1;
    %response during significant time bins
    mr_sigbins = nanmean(r,2)*rasterfs;
    mr_sigbins(~sigbins) = nan;
    mr_sig = squeeze(nanmean(mr_sigbins,1));
    %spontaneous activity
    mr_spont = squeeze(nanmean(nanmean(r(1:(start-1),:,:),2))*rasterfs);
    %evoked activity
    mr_evoked = mr_sig - mr_spont;
    %ssa index
    ssa = [(mr_sig(1)-mr_sig(2))/(mr_sig(1)+mr_sig(2)) ...
      (mr_sig(4)-mr_sig(5))/(mr_sig(4)+mr_sig(5))];
    %combined ssa index
    combined_ssa = [((mr_sig(1)+mr_sig(4))-(mr_sig(2)+mr_sig(5)))/...
                     (mr_sig(1)+mr_sig(4)+mr_sig(2)+mr_sig(5))];
    %adaptation index
    adapt = [(mr_sig(3)-mr_sig(2))/(mr_sig(3)+mr_sig(2)) ...
      (mr_sig(6)-mr_sig(5))/(mr_sig(6)+mr_sig(5))];
    %facilitation index
    facilitate = [(mr_sig(1)-mr_sig(3))/(mr_sig(1)+mr_sig(3)) ...
      (mr_sig(4)-mr_sig(6))/(mr_sig(4)+mr_sig(6))];
    ssa_indexes = struct('ssa', ssa, ...
                         'combined_ssa', combined_ssa, ...
                         'adapt', adapt, ...
                         'facilitate', facilitate);
    save([cellid '.mat'], 'r', 'r_1000', 'tags', 'exptparams', 'rasterfs', ...
      'ssa_indexes', 'prepip', 'pipdur', 'trial_m', 'trial_sem', ...
      'trial_m_with_spont', 'trial_sem_with_spont', 'sigbins', 'sig_str', ...
      'mr_evoked', 'is_sig', 'cellfiledata');
  end
  cd(start_dir)
  %plot psths
  concolors = [1 0 0; 0 0 1; 0 0 0];
  tt = (1:size(r,1))./rasterfs-prepip;
  freqs = exptparams.TrialObject.ReferenceHandle.Frequencies./1000;
  fig = figure(options.fig);
  psth_axes = [options.psth_ax1 options.psth_ax2];
  for ii=1:2
    axes(psth_axes(ii));
    hold on
    for jj=1:3
      kk=(ii-1)*3+jj;
      l(jj) = plot(tt, trial_m_with_spont(:,kk), 'color', concolors(jj,:), ...
	'linewidth', 2);
      errorbar(tt, trial_m_with_spont(:,kk), trial_sem_with_spont(:,kk), ...
	'color', concolors(jj,:), 'linewidth', 0.8);
    end
    if ii == 1
      legend(l, 'Rare', 'Common', 'Onset', 'Location', 'NorthEast')
    end
    clear l
    y_min = round((min(min(trial_m,[],3))) - (max(max(trial_sem,[],3))));
    y_max = (round((max(max(trial_m,[],3))) + (max(max(trial_sem,[],3))))) * 1.25;
    x_min = min(tt);
    x_max = max(tt);
    ylim([y_min, y_max]);
    xlim([x_min, x_max]);
    aa = axis;
    plot([0 0],aa(3:4),'g--');
    plot([0 0]+pipdur,aa(3:4),'g--');
    ylabel('Spikes/s');
    sigbin_idxs = sigbins(:,:,(ii-1)+3);
    text(tt(sigbin_idxs), repmat(y_max-(y_max/10),1,sum(sigbin_idxs)), '-', ...
      'fontsize', 18);
    str = {sprintf('Stimulus: %0.1f kHz', freqs(ii)),
    sprintf('SSA: %0.2f', ssa_indexes.ssa(ii)),
    sprintf('Adaptation: %0.2f', ssa_indexes.adapt(ii)),
    sprintf('Facilitation: %0.2f', ssa_indexes.facilitate(ii)),
    sprintf('%s', sig_str)};
    text(0.01, 0.7, str, 'Units', 'normalized')
    hold off
  end
  %plot raster
  if options.raster_ax
    axes(options.raster_ax);
    set(gca, 'Ydir', 'reverse');
    hold on
    %plot the same number of common and rare trials
    %note: assumes probability of rare trials = 0.05
    rtmp = r_1000(:, 1:19:end, common);
    r_1000(:, :, common) = nan;
    r_1000(:, 1:size(rtmp,2), common) = rtmp;
    conds = size(r_1000, 3);
    reps = squeeze(sum(~isnan(r_1000(1,:,:))));
    startidx = [1; cumsum(reps)+1];
    stopidx = startidx(2:end) - 1;
    r_1000(isnan(r_1000)) = 0;
    raster_colors = [concolors; concolors];
    for cond = 1:conds
      [spiketimes, trials] = find(r_1000(:,:,cond));
      plot((spiketimes/1000)-prepip, trials+startidx(cond), '.', ...
	'MarkerSize', 10, ...
	'MarkerFaceColor', raster_colors(cond,:), ...
	'MarkerEdgeColor', raster_colors(cond,:));
    end
    axis tight;
    aa = axis;
    plot([0 0],aa(3:4),'g--');
    plot([0 0]+pipdur,aa(3:4),'g--');
    freq_str = {sprintf('%0.1f kHz', freqs(1)),
                sprintf('%0.1f kHz', freqs(2))};
    set(gca, 'ytick', startidx([2 5])-(startidx([2 5])-stopidx([2 5]))/2,...
      'yticklabel', freq_str);
    xlim([min(tt) max(tt)]);
    hold off
  end
  xlabel('Time from Stimulus Onset (s)');
  if options.add_title
    suptitle(cellid, 0)
  end
end
