% function [predboth,resp,meta,r0]=RDT_predboth_stream_from_modelpath(modelpath);
%
% modelpath:  e.g., modelpath column from SQL query on NarfResults
%
% returns:  predboth
%               predicted response to combined stream 1 and 2
%           resp
%               Actual response (averaged across all repetitions for that
%               stream)
%           meta
%               Information regarding the model fit
%           r0
%               Baseline firing rate (full prediction should be predboth+r0)

function [predboth,resp,meta,r0]=RDT_predboth_stream_from_modelpath(modelpath);

load_model(modelpath);

global STACK XXX FLATXXX META
FLATXXX=XXX;

if ismember(META.batch,[281 282]),
    % special stuff for fits with RandSingle files
    if length(XXX{1}.training_set)>1,
        matched=0;
        for ii=1:length(XXX{1}.training_set),
            if ~matched && isempty(findstr(XXX{1}.training_set{ii},'_est')),
                XXX{1}.test_set={XXX{1}.training_set{ii}};
                XXX{1}.training_set{ii}=[XXX{1}.training_set{ii} '_all'];
                matched=1;
            end
        end
    else
        XXX{1}.test_set{1}=strrep(XXX{1}.test_set{1},'_val','');
    end
else
    XXX{1}.test_set{1}=strrep(XXX{1}.test_set{1},'_val','');
end
f=XXX{1}.test_set{1};
fe=XXX{1}.training_set{1};

% remove offset
[~,fir_idx] = find_modules(STACK, 'fir_filter', true);

for jj=1:length(STACK{fir_idx}),
    r0=STACK{fir_idx}{jj}.baseline;
    STACK{fir_idx}{jj}.baseline=0;
end
zpad=14;

% Load combined predictions for streams
STACK{1}{1}.zpad=zpad;
STACK{1}{1}.include_prestim=1;
XXX=XXX(1);
FLATXXX=FLATXXX(1);
update_xxx(1);
predboth=XXX{end}.dat.(f).stim((zpad+1):end,:,:);

% correct for possible offset
additional_offset=predboth(1);
r0=r0+additional_offset;
predboth=predboth-additional_offset;

resp = XXX{end}.dat.(f).resp((zpad+1):end,:,:);

meta.experiment = XXX{1}.test_set{1};
meta.score_test_corr = XXX{end}.score_test_corr;
meta.score_test_floorcorr = XXX{end}.score_test_floorcorr;
meta.trial_to_sequence = XXX{end}.dat.(f).trial2sequence;
