% function [parmfile,spkfile]=RDT_pred_stream(cellid,batchid,modelname);
%
% cellid:     e.g., 'chn016c-d1'
% batchid=269:  A1 passive
% batchid=273:  PEG passive
%
% plots raster and PSTH aligned at trial onsets
%
% returns:  parmfile - path/filename of baphy parameter file
%           spkfile - sorted spike file
%
% SVD 2015-06-04
function [parmfile,spkfile]=RDT_psth(cellid,batchid);

sql=['SELECT * FROM NarfBatches WHERE cellid="',cellid,'"',...
     ' AND batch=',num2str(batchid)];

bdata=mysql(sql);
if isempty(bdata),
    disp('NarfBatches entry not found');
    return
end

val_set=char(bdata(1).val_set);
val_set=eval(strrep(val_set,'_val',''));
f=val_set{1};

sql=['SELECT * FROM sCellFile WHERE cellid="',cellid,'"',...
     ' AND stimfile="',f,'"'];
cfd=mysql(sql);
parmfile=[cfd.stimpath,cfd.stimfile];
spkfile=[cfd.path cfd.respfile];

options=struct('channel',cfd.channum,...
               'unit',cfd.unit,...
               'psthfs',30,...
               'rasterfs',1000,...
               'use_sorted',1,...
               'psth',1,...
               'datause','Per trial');
raster_online(parmfile,cfd.channum, cfd.unit,[],options);
