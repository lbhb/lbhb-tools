% function [pred,resp,pupil,fs,perf,stim]=load_narf_pred_pupil(cellid,batch,modelname,stimfile)
% 
% pred,resp taken from XXX{end} via load_narf_prediction
% pupil also taken from XXX{end} after load_narf_prediction is complete
%  perf=[NarfResults.r_test .r_fit .r_floor .r_ceiling .r_active];
%
% SVD 2016-10-10
function [pred,resp,pupil,fs,perf,stim]=load_narf_pred_pupil(cellid,batch,modelname,stimfile)

if ~exist('cellid','var'),
   %cellid='eno005d-a1';
   cellid='eno005d-b1';
   %cellid='eno005d-b2';
   %cellid='eno013b-b1';
   %cellid='eno013b-b2';
   %cellid='eno027d-c1';
   %cellid='eno027d-a1';
   %cellid='eno028c-c1';
   %cellid='eno032e-c1';
end

if ~exist('batch','var'),
   batch=283;
end
if ~exist('modelname','var'),
   if batch==283,
      modelname='env100_logn_adp1pc_fir15_siglog100_fit05a';
   elseif batch==289,
      modelname='fb18ch100_lognn_wcg02_fir15_dexp_fit05a';
   end
end

dbopen;
if ~exist('stimfile','var'),
   cfd=dbbatchcells(batch,cellid);
   stimfile=cfd(1).stimfile;
end

[pred,resp,fs,perf]=load_narf_prediction(cellid,modelname,batch,stimfile);
global XXX

stim=XXX{2}.dat.(stimfile).stim;
stim=permute(stim,[3 1 2]);

dbopen;
sql=['SELECT * FROM gDataRaw WHERE parmfile LIKE "',stimfile,'%"'];
rawdata=mysql(sql);
baphyparms=dbReadData(rawdata.id);
PreStimBins=round(baphyparms.Ref_PreStimSilence.*fs);
PostStimBins=round(baphyparms.Ref_PostStimSilence.*fs);
parmfile=[rawdata.resppath rawdata.parmfile];

spkfile=[cfd(1).path,cfd(1).respfile];
options=struct();
options.rasterfs = 100;
options.unit=cfd(1).unit;
options.channel=cfd(1).channum;
options.includeprestim=1;
if ~isempty(cfd(1).goodtrials),
   options.trialrange=eval(cfd(1).goodtrials);
end
%[r0,rtags]=loadspikeraster(spkfile,options);

pr_options = options;
pr_options.pupil = 1;
pr_options.pupil_median=0.5;
pr_options.pupil_offset=0.75;

if batch==263,
   pr_options.tag_masks={'Reference1'};
   [pupil1,ptags1,trialset1] = loadevpraster(parmfile, pr_options);
   pr_options.tag_masks={'Reference2'};
   [pupil2,ptags2,trialset2] = loadevpraster(parmfile, pr_options);
   pupil=cat(3,pupil1,pupil2);
   ptags=cat(2,ptags1,ptags2);
   trialset=cat(2,trialset1,trialset2);
else
   pr_options.tag_masks={'Reference'};
   [pupil,ptags,trialset] = loadevpraster(parmfile, pr_options);
end

resp=permute(resp,[1 3 2]);
repcount=size(resp,2);
pred=repmat(permute(pred,[1 3 2]),[1 repcount 1]);
stim=repmat(permute(stim,[1 2 4 3]),[1 1 repcount 1]);

% sort by trial order
validtrials=unique(trialset(trialset>0 & ~isnan(trialset)))';
trialcount=length(validtrials);
si=zeros(trialcount,1);
for tt=1:length(validtrials),
   si(tt)=find(trialset(:)==validtrials(tt));
end

resp=resp(:,si);
pred=pred(:,si);
pupil=pupil(:,si);
stim=stim(:,:,si);

if nargout>0,
   return
end


% figure out gain and spont diff for each trial
disp('Baseline/gain analysis');
g=zeros(trialcount,1);
b=zeros(trialcount,1);
e=zeros(trialcount,1);
trial_box=2;
N=sqrt(nanmean(resp(:).^2));
METHOD=1;

for tt=1:trialcount,
   if tt>trial_box && tt<trialcount-trial_box+1,
      ttr=(tt-trial_box):(tt+trial_box);
   elseif tt<=trial_box,
      ttr=1:(tt+trial_box);
   else
      ttr=(tt-trial_box):trialcount;
   end
   
   tp=pred(:,ttr);
   tr=resp(:,ttr);
   tp(isnan(tr))=nan;
   T=size(resp,1);
   b0=nanmean(nanmean(tr([1:PreStimBins (T-PostStimBins+20):T],:)))-...
      nanmean(nanmean(tp([1:PreStimBins (T-PostStimBins+20):T],:)));
   tp=tp(~isnan(tr(:)));
   tr=tr(~isnan(tr(:)));
   
   if METHOD==0,
      x0=mean(mean(tp([1:PreStimBins (T-PostStimBins+20):T],:)));
      y0=mean(mean(tr([1:PreStimBins (T-PostStimBins+20):T],:)));
      
      x=[tp(:) ones(size(tp(:)))];
      %x=[tp(:)-x0];
      y=[tr(:)];
      [beta,bint]=regress(y,x);
      g(tt)=beta(1);
      b(tt)=beta(2);
      
   else
      %smwin=ones(7,1)./7;
      smwin=[linspace(0,1,6) linspace(1,0,6)]';
      %smwin=[zeros(1,5) 1 linspace(1,0,6)]';
      smwin=smwin([1:6 8:12]);
      smwin=smwin./sum(smwin);
      tr=rconv2(tr,smwin);
      
      bincount=12;
      sp=sort(tp);
      sp(end+1)=max(sp)+1;
      edges=sp(round(linspace(1,length(sp),bincount+1)));
      edges=unique(edges);
      bincount=length(edges)-1;
      pbinned=zeros(bincount,1);
      rbinned=zeros(bincount,1);
      for bb=1:bincount,
         pbinned(bb)=mean(tp(tp>=edges(bb) & tp<edges(bb+1)));
         rbinned(bb)=mean(tr(tp>=edges(bb) & tp<edges(bb+1)));
      end
      
      if METHOD==1,
         % method 1
         b(tt)=b0;
         pbinned=pbinned+b(tt);
         g(tt)=sum(pbinned.*rbinned)./sum(pbinned.^2);
      elseif METHOD==4,
         % method 4 -- linear regression
         
         x=[pbinned(:) ones(size(pbinned(:)))];
         y=[rbinned(:)];
         [beta,bint]=regress(y,x);
         g(tt)=beta(1);
         b(tt)=beta(2);
         
      elseif METHOD==2,
         % method 2
         m=(pbinned+rbinned)./2;
         d=(rbinned-pbinned)./2;
         % p(1)=gain, p(2)=baseline
         p=polyfit(m,d,1);
         %p=polyfit(tp,tr,1);
         g(tt)=p(1);
         b(tt)=p(2);
      elseif METHOD==3,
         % method 3
         % p(1)=gain, p(2)=baseline
         p=polyfit(tp,tr,1);
         g(tt)=p(1);
         b(tt)=p(2);
      end
   end
   e(tt)=std(tp-tr)./N;
   %e(tt)=std(pbinned-rbinned)./std(rbinned);
end

g(g<0.5)=0.5;
g(g>2)=2;

if METHOD==2
    g1=log2(1+g);
else
    g1=log2(g);
end

pup=mean(pupil(1:100,:),1)';

figure;
subplot(6,1,1);
plot(pup);
title([cellid ' - ' stimfile],'Interpreter','none');
ylabel('pupil');

subplot(6,1,2);
plot(b);
ylabel('baseline');

subplot(6,1,3);
plot(g1);
ylabel('gain');

subplot(2,2,3);
plot(pup,b,'.');

xlabel('pupil');
ylabel('baseline');
title(sprintf('xc=%.2f',xcov(pup,b,0,'coeff')));

subplot(2,2,4);
plot(pup,g,'.');
xlabel('pupil');
ylabel('gain');
title(sprintf('xc=%.2f',xcov(pup,g,0,'coeff')));


