

narf_set_path
dbopen


%cfd=dbgetscellfile('rawid',116168);
sql=['SELECT * FROM sCellFile WHERE rawid=116168',...
   ' ORDER BY cellid'];
fdata=mysql(sql);
unitset=sort(cat(1,fdata.channum).*10+cat(1,fdata.unit));
channel=floor(unitset/10);
unit=mod(unitset,10);
cellids={fdata.cellid};
cellcount=length(cellids);

spkfile=[fdata(1).path fdata(1).respfile];
parmfile=[fdata(1).stimpath fdata(1).stimfile];

options=struct('rasterfs',100,'includeprestim',1,'meansub',0,...
   'channel',channel,'unit',unit,'trialrange',1:116);
[r,tags,trialset]=loadsiteraster(spkfile,options);

outfile='/auto/data/tmp/zee015h04_p_VOC_siteraster_100Hz.mat';

save(outfile,'r','trialset','tags','cellids','cellcount','parmfile');


% alt, generate some simulated data for one channel
r1=r(:,:,:,4);

r2=r(:,:,:,9);
c=[0.3 0.7 0 0 0]';
r2=convn(r2,c,'same');

r(:,:,:,15)=r1*0.1 - r2.*0.1 + randn(size(r(:,:,:,15)))/2;


outfile='/auto/data/tmp/zee015h04_ch15dummy.mat';

save(outfile,'r','trialset','tags','cellids','cellcount','parmfile');
