function r=tsp_cell_info(cellid,batch)

% find a file that matches specific behavioral selection
% criteria for this batch

tr=tsp_resp_resp2(cellid,batch);
bnb_tuning(cellid);

cellfiledata=dbgettspfiles(cellid,batch);
activefile=strcmpi({cellfiledata.behavior},'active');

acounter=1;
pcounter=0;
filecount=length(cellfiledata);
train_set=cell(1,filecount);
test_set=cell(1,filecount);
rawid=zeros(1,filecount);
targetHz=zeros(1,filecount);
targetband=zeros(1,filecount);

spnlo=zeros(2,filecount);
spnhi=zeros(2,filecount);
[celllo,cellhi,tt]=spn_tuning(cellid);
cellband=[celllo cellhi];
band1=[];
for ii=1:filecount,
   train_set{ii}=[cellfiledata(ii).stimfile,'_est'];
   test_set{ii}=[cellfiledata(ii).stimfile,'_val'];
   rawid(ii)=cellfiledata(ii).rawid;
   
   baphyparms=dbReadData(rawid(ii));
   spnlo(:,ii)=baphyparms.Ref_LowFreq;
   spnhi(:,ii)=baphyparms.Ref_HighFreq;
   
   if activefile(ii),
      res=spn_tuning_match(cellid,batch,0);
      %res=spn_tuning_match2(cellid,286,0);
      if isfield(baphyparms,'Tar_Frequencies'),
         maxtar=length(baphyparms.Tar_Frequencies);
      else
         maxtar=length(baphyparms.Trial_TargetIdxFreq);
      end
      snr=baphyparms.Trial_RelativeTarRefdB;
      targetfreq=baphyparms.Trial_TargetIdxFreq(1:maxtar);
      commontargetid=min(find(targetfreq==max(targetfreq)));
      if isempty(band1),
         band1=[spnlo(1) spnhi(1)];
         band2=[spnlo(2) spnhi(2)];
         bandoverlap=res;
      end
      targetHz(ii)=baphyparms.Tar_Frequencies(commontargetid)
      if targetHz(ii)>=spnlo(1,ii) && targetHz(ii)<=spnhi(1,ii),
         targetband(ii)=1;
      elseif targetHz(ii)>=spnlo(2,ii) && targetHz(ii)<=spnhi(2,ii),
         targetband(ii)=2;
      end
      
      if ismember(batch,[251 253 254])
         % batch 251, 253, 254:  A1 hard, A2 easy
         % for in/out data, A1: attend in
         % for high/low data. A1: attemd low
         commonHz=baphyparms.Tar_Frequencies(commontargetid);
         uncommontarid=min(find(targetfreq==min(targetfreq)));
         uncommonHz=baphyparms.Tar_Frequencies(uncommontarid);
         commontarinref=length(baphyparms.Ref_LowFreq)==1 && ...
            commonHz>=baphyparms.Ref_LowFreq &&....
            commonHz<=baphyparms.Ref_HighFreq;
         uncommontarinref=length(baphyparms.Ref_LowFreq)==1 && ...
            uncommonHz>=baphyparms.Ref_LowFreq &&....
            uncommonHz<=baphyparms.Ref_HighFreq;
         if commontarinref && ~uncommontarinref,
            Aidx=1;
         elseif uncommontarinref && ~commontarinref,
            Aidx=2;
         elseif snr(commontargetid)==max(snr),
            Aidx=2;
         else
            Aidx=1;
         end
         
      elseif ismember(batch,[244 255])
         % lr  batches
         % A1: attend contra; A2: attend ipsi
         if isfield(baphyparms,'Trial_TargetChannel'),
            Aidx=baphyparms.Trial_TargetChannel(commontargetid);
         elseif isfield(baphyparms,'Tar_SplitChannels') &&...
               strcmpi(baphyparms.Tar_SplitChannels,'Yes')
            Aidx=commontargetid;
         else
            disp('unknown lr condition');
            keyboard
         end
         
      elseif ismember(batch,[241 252 255 258 268 274 275 286 287])
         % lr/hl batches
         % A1: attend band near BF, A2: attend away
         if isfield(baphyparms,'Trial_TargetChannel'),
            commontargetchan=baphyparms.Trial_TargetChannel(commontargetid);
         else
            commontargetchan=commontargetid;
         end
         if max(res)>0 && res(commontargetchan)==max(res),
            Aidx=1;
         else
            Aidx=2;
         end
         if Aidx==1,
            %keyboard
         end
      end
      
      file_code{ii}=['A',num2str(Aidx)];
      acounter=acounter+1;
      pcounter=0;
   elseif ~pcounter,
      file_code{ii}=['P',num2str(acounter)];
      pcounter=1;
   else
      % disabled lettering for multiple passives.  now
      % just collapse into a single set
      %file_code{ii}=['P',num2str(acounter),char('a'-1+pcounter)];
      file_code{ii}=['P',num2str(acounter)];
      pcounter=pcounter+1;
   end
end

if ~exist('test_file_code','var')
   test_file_code=file_code;
end
if ~exist('test_rawid','var')
   test_rawid=rawid;
end

r = struct();
r.cellid = cellid;
r.training_set = train_set;
r.test_set = test_set;
r.filecode = file_code;
r.test_filecode = test_file_code;
r.training_rawid = rawid;
r.test_rawid = test_rawid;

r.cellband=cellband;
r.band1=band1;
r.band2=band2;
r.bandoverlap=bandoverlap;
r.targetHz=targetHz;
r.targetband=targetband;
r.spnhi=spnhi;
r.spnlo=spnlo;


