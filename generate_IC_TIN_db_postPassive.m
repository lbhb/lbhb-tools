
% IC data structure bbl IC RH. 2017-06-07 Daniela Saderi

ICdata=struct;

% Post-passive - Active hard
ICdata(1).cellid='bbl021h-a1';
ICdata(1).area='ICx';
ICdata(1).type='Probe_DS';
ICdata(1).note='Stable isolation P90 A90; hard to tell BF';
ICdata(1).datapath='/auto/data/daq/Babybell/bbl021/sorted/';
ICdata(1).passive_spike='bbl021h06_p_PTD.spk.mat';
ICdata(1).passive_rawid=120260;
ICdata(1).active_spike='bbl021h04_a_PTD.spk.mat';
ICdata(1).active_rawid=120256;
ICdata(1).on_BF=0; % 1=on BF target, 0=off BF
ICdata(1).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(2).cellid='bbl021h-a1';
ICdata(2).area='ICx';
ICdata(2).type='Probe_DS';
ICdata(2).note='Stable isolation P90 A90; hard to tell BF';
ICdata(2).datapath='/auto/data/daq/Babybell/bbl021/sorted/';
ICdata(2).passive_spike='bbl021h06_p_PTD.spk.mat';
ICdata(2).passive_rawid=120260;
ICdata(2).active_spike='bbl021h04_a_PTD.spk.mat';
ICdata(2).active_rawid=120258;
ICdata(2).on_BF=0; % 1=on BF target, 0=off BF
ICdata(2).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active easy
ICdata(3).cellid='bbl022g-a1';
ICdata(3).area='ICx';
ICdata(3).type='Probe_DS';
ICdata(3).note='Stable isolation P95 A99';
ICdata(3).datapath='/auto/data/daq/Babybell/bbl022/sorted/';
ICdata(3).passive_spike='bbl022g09_p_PTD.spk.mat';
ICdata(3).passive_rawid=120293;
ICdata(3).active_spike='bbl022g07_a_PTD.spk.mat';
ICdata(3).active_rawid=120289;
ICdata(3).on_BF=0; % 1=on BF target, 0=off BF
ICdata(3).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard
ICdata(4).cellid='bbl022g-a1';
ICdata(4).area='ICx';
ICdata(4).type='Probe_DS';
ICdata(4).note='Stable isolation P95 A99';
ICdata(4).datapath='/auto/data/daq/Babybell/bbl022/sorted/';
ICdata(4).passive_spike='bbl022g09_p_PTD.spk.mat';
ICdata(4).passive_rawid=120293;
ICdata(4).active_spike='bbl022g08_a_PTD.spk.mat';
ICdata(4).active_rawid=120290;
ICdata(4).on_BF=0; % 1=on BF target, 0=off BF
ICdata(4).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active easy
ICdata(5).cellid='bbl023c-a1';
ICdata(5).area='ICx';
ICdata(5).type='Probe_DS';
ICdata(5).note='Stable isolation P99 A95; not quite onBF, offset response';
ICdata(5).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(5).passive_spike='bbl023c05_p_PTD.spk.mat';
ICdata(5).passive_rawid=120313;
ICdata(5).active_spike='bbl023c03_a_PTD.spk.mat';
ICdata(5).active_rawid=120311;
ICdata(5).on_BF=0; % 1=on BF target, 0=off BF
ICdata(5).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard
ICdata(6).cellid='bbl023c-a1';
ICdata(6).area='ICx';
ICdata(6).type='Probe_DS';
ICdata(6).note='Stable isolation P99 A99; not quite onBF, offest response';
ICdata(6).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(6).passive_spike='bbl023c05_p_PTD.spk.mat';
ICdata(6).passive_rawid=120313;
ICdata(6).active_spike='bbl023c04_a_PTD.spk.mat';
ICdata(6).active_rawid=120312;
ICdata(6).on_BF=0; % 1=on BF target, 0=off BF
ICdata(6).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-Passive - Active hard
ICdata(7).cellid='bbl023c-a1';
ICdata(7).area='ICx';
ICdata(7).type='Probe_DS';
ICdata(7).note='Stable isolation P99 A99, diff freq from prev but similar response; offset response';
ICdata(7).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(7).passive_spike='bbl023c05_p_PTD.spk.mat';
ICdata(7).passive_rawid=120313;
ICdata(7).active_spike='bbl023c06_a_PTD.spk.mat';
ICdata(7).active_rawid=120314;
ICdata(7).on_BF=0; % 1=on BF target, 0=off BF
ICdata(7).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-Passive - Active easy
ICdata(8).cellid='bbl023c-a1';
ICdata(8).area='ICx';
ICdata(8).type='Probe_DS';
ICdata(8).note='Stable isolation P99 A99, offset response';
ICdata(8).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(8).passive_spike='bbl023c05_p_PTD.spk.mat';
ICdata(8).passive_rawid=120313;
ICdata(8).active_spike='bbl023c07_a_PTD.spk.mat';
ICdata(8).active_rawid=120316;
ICdata(8).on_BF=0; % 1=on BF target, 0=off BF
ICdata(8).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% % Second Post-Passive - Active hard
% ICdata(7).cellid='bbl023c-a1';
% ICdata(7).area='ICx';
% ICdata(7).type='Probe_DS';
% ICdata(7).note='Stable isolation P99 A99, diff freq from prev but similar response; offset response';
% ICdata(7).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
% ICdata(7).passive_spike='bbl023c08_p_PTD.spk.mat';
% ICdata(7).passive_rawid=120317;
% ICdata(7).active_spike='bbl023c06_a_PTD.spk.mat';
% ICdata(7).active_rawid=120314;
% ICdata(7).on_BF=0; % 1=on BF target, 0=off BF
% ICdata(7).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
% 
% % Second Post-Passive - Active easy
% ICdata(8).cellid='bbl023c-a1';
% ICdata(8).area='ICx';
% ICdata(8).type='Probe_DS';
% ICdata(8).note='Stable isolation P99 A99, offset response';
% ICdata(8).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
% ICdata(8).passive_spike='bbl023c08_p_PTD.spk.mat';
% ICdata(8).passive_rawid=120317;
% ICdata(8).active_spike='bbl023c07_a_PTD.spk.mat';
% ICdata(8).active_rawid=120316;
% ICdata(8).on_BF=0; % 1=on BF target, 0=off BF
% ICdata(8).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------


% Post-passive - Active easy
ICdata(9).cellid='bbl027i-a1';
ICdata(9).area='ICc';
ICdata(9).type='Var_SNR';
ICdata(9).note='Great isolation P95 A99; broad-band response';
ICdata(9).datapath='/auto/data/daq/Babybell/bbl027/sorted/';
ICdata(9).passive_spike='bbl027i04_p_PTD.spk.mat';
ICdata(9).passive_rawid=120709;
ICdata(9).active_spike='bbl027i02_a_PTD.spk.mat';
ICdata(9).active_rawid=120707;
ICdata(9).on_BF=0; % 1=on BF target, 0=off BF
ICdata(9).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active hard
ICdata(10).cellid='bbl027i-a1';
ICdata(10).area='ICc';
ICdata(10).type='Var_SNR';
ICdata(10).note='Great isolation P95 A99; broad-band response; not great behavior';
ICdata(10).datapath='/auto/data/daq/Babybell/bbl027/sorted/';
ICdata(10).passive_spike='bbl027i04_p_PTD.spk.mat';
ICdata(10).passive_rawid=120709;
ICdata(10).active_spike='bbl027i03_a_PTD.spk.mat';
ICdata(10).active_rawid=120708;
ICdata(10).on_BF=0; % 1=on BF target, 0=off BF
ICdata(10).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------


% Post-passive - Active easy 1
ICdata(11).cellid='bbl028d-a1';
ICdata(11).area='ICc';
ICdata(11).type='Var_SNR';
ICdata(11).note='Great isolation P99 A99';
ICdata(11).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(11).passive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(11).passive_rawid=120737;
ICdata(11).active_spike='bbl028d05_a_PTD.spk.mat';
ICdata(11).active_rawid=120735;
ICdata(11).on_BF=1; % 1=on BF target, 0=off BF
ICdata(11).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active hard 1
ICdata(12).cellid='bbl028d-a1';
ICdata(12).area='ICc';
ICdata(12).type='Var_SNR';
ICdata(12).note='Great isolation P99 A99';
ICdata(12).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(12).passive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(12).passive_rawid=120737;
ICdata(12).active_spike='bbl028d06_a_PTD.spk.mat';
ICdata(12).active_rawid=120736;
ICdata(12).on_BF=1; % 1=on BF target, 0=off BF
ICdata(12).difficulty=1;  % 0=pure tone, 1=easy, 2=hard    


% Post-passive - Active easy 2
ICdata(13).cellid='bbl028d-a1';
ICdata(13).area='ICc';
ICdata(13).type='Var_SNR';
ICdata(13).note='Great isolation P99 A99; same passive as before';
ICdata(13).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(13).passive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(13).passive_rawid=120737;
ICdata(13).active_spike='bbl028d08_a_PTD.spk.mat';
ICdata(13).active_rawid=120738;
ICdata(13).on_BF=1; % 1=on BF target, 0=off BF
ICdata(13).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard 2
ICdata(14).cellid='bbl028d-a1';
ICdata(14).area='ICc';
ICdata(14).type='Var_SNR';
ICdata(14).note='Good isolation P99 A95; same passive as before';
ICdata(14).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(14).passive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(14).passive_rawid=120737;
ICdata(14).active_spike='bbl028d09_a_PTD.spk.mat';
ICdata(14).active_rawid=120739;
ICdata(14).on_BF=1; % 1=on BF target, 0=off BF
ICdata(14).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% % Second Post-passive - Active easy 2
% ICdata(13).cellid='bbl028d-a1';
% ICdata(13).area='ICc';
% ICdata(13).type='Var_SNR';
% ICdata(13).note='Great isolation P95 A99; last passive';
% ICdata(13).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
% ICdata(13).passive_spike='bbl028d09_p_PTD.spk.mat';
% ICdata(13).passive_rawid=120740;
% ICdata(13).active_spike='bbl028d08_a_PTD.spk.mat';
% ICdata(13).active_rawid=120738;
% ICdata(13).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(13).difficulty=1;  % 0=pure tone, 1=easy, 2=hard
% 
% % Second Post-passive - Active hard 2
% ICdata(14).cellid='bbl028d-a1';
% ICdata(14).area='ICc';
% ICdata(14).type='Var_SNR';
% ICdata(14).note='Good isolation P95 A95; last passive';
% ICdata(14).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
% ICdata(14).passive_spike='bbl028d09_p_PTD.spk.mat';
% ICdata(14).passive_rawid=120740;
% ICdata(14).active_spike='bbl028d09_a_PTD.spk.mat';
% ICdata(14).active_rawid=120739;
% ICdata(14).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(14).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------


% Post-passive - Active easy 1
ICdata(15).cellid='bbl030e-a1';
ICdata(15).area='ICc';
ICdata(15).type='Var_SNR';
ICdata(15).note='Good isolation P95 A95';
ICdata(15).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(15).passive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(15).passive_rawid=120799;
ICdata(15).active_spike='bbl030e07_a_PTD.spk.mat';
ICdata(15).active_rawid=120793;
ICdata(15).on_BF=1; % 1=on BF target, 0=off BF
ICdata(15).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard 1
ICdata(16).cellid='bbl030e-a1';
ICdata(16).area='ICc';
ICdata(16).type='Var_SNR';
ICdata(16).note='Good isolation P95 A95';
ICdata(16).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(16).passive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(16).passive_rawid=120799;
ICdata(16).active_spike='bbl030e08_a_PTD.spk.mat';
ICdata(16).active_rawid=120797;
ICdata(16).on_BF=1; % 1=on BF target, 0=off BF
ICdata(16).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active easy 2
ICdata(17).cellid='bbl030e-a1';
ICdata(17).area='ICc';
ICdata(17).type='Var_SNR';
ICdata(17).note='Good isolation P95 A95; same passive as prev';
ICdata(17).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(17).passive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(17).passive_rawid=120799;
ICdata(17).active_spike='bbl030e11_a_PTD.spk.mat';
ICdata(17).active_rawid=120801;
ICdata(17).on_BF=1; % 1=on BF target, 0=off BF
ICdata(17).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard 2
ICdata(18).cellid='bbl030e-a1'; 
ICdata(18).area='ICc';
ICdata(18).type='Var_SNR';
ICdata(18).note='Good isolation P95 A95; same passive as prev';
ICdata(18).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(18).passive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(18).passive_rawid=120799;
ICdata(18).active_spike='bbl030e12_a_PTD.spk.mat';
ICdata(18).active_rawid=120806;
ICdata(18).on_BF=1; % 1=on BF target, 0=off BF
ICdata(18).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Post-passive - Active easy
ICdata(19).cellid='bbl032f-a1';
ICdata(19).area='ICc';
ICdata(19).type='Var_SNR';
ICdata(19).note='Good isolation P95 A99, suppression on target? Off response higher freq';
ICdata(19).datapath='/auto/data/daq/Babybell/bbl032/sorted/';
ICdata(19).passive_spike='bbl032f05_p_PTD.spk.mat';
ICdata(19).passive_rawid=120906;
ICdata(19).active_spike='bbl032f03_a_PTD.spk.mat';
ICdata(19).active_rawid=120901;
ICdata(19).on_BF=0; % 1=on BF target, 0=off BF
ICdata(19).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active hard
ICdata(20).cellid='bbl032f-a1';
ICdata(20).area='ICc';
ICdata(20).type='Var_SNR';
ICdata(20).note='Good isolation P95 A95, suppression on target? Off response higher freq';
ICdata(20).datapath='/auto/data/daq/Babybell/bbl032/sorted/';
ICdata(20).passive_spike='bbl032f05_p_PTD.spk.mat';
ICdata(20).passive_rawid=120906;
ICdata(20).active_spike='bbl032f04_a_PTD.spk.mat';
ICdata(20).active_rawid=120903;
ICdata(20).on_BF=0; % 1=on BF target, 0=off BF
ICdata(20).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------


% Post-passive - Active hard
ICdata(21).cellid='bbl033c-a1';
ICdata(21).area='ICc';
ICdata(21).type='Var_SNR';
ICdata(21).note='OK isolation P90 A95; nearBF';
ICdata(21).datapath='/auto/data/daq/Babybell/bbl033/sorted/';
ICdata(21).passive_spike='bbl033c05_p_PTD.spk.mat';
ICdata(21).passive_rawid=120942;
ICdata(21).active_spike='bbl033c03_a_PTD.spk.mat';
ICdata(21).active_rawid=120940;
ICdata(21).on_BF=0; % 1=on BF target, 0=off BF
ICdata(21).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active easy
ICdata(22).cellid='bbl033c-a1';
ICdata(22).area='ICc';
ICdata(22).type='Var_SNR';
ICdata(22).note='OK isolation P90 A90; nearBF';
ICdata(22).datapath='/auto/data/daq/Babybell/bbl033/sorted/';
ICdata(22).passive_spike='bbl033c05_p_PTD.spk.mat';
ICdata(22).passive_rawid=120942;
ICdata(22).active_spike='bbl033c04_a_PTD.spk.mat';
ICdata(22).active_rawid=120941;
ICdata(22).on_BF=0; % 1=on BF target, 0=off BF
ICdata(22).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active easy
ICdata(23).cellid='bbl034e-a1';
ICdata(23).area='ICc';
ICdata(23).type='Var_SNR';
ICdata(23).note='Great isolation P99 A99; no fixed TORC';
ICdata(23).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(23).passive_spike='bbl034e05_p_PTD.spk.mat';
ICdata(23).passive_rawid=120986;
ICdata(23).active_spike='bbl034e03_a_PTD.spk.mat';
ICdata(23).active_rawid=120984;
ICdata(23).on_BF=1; % 1=on BF target, 0=off BF
ICdata(23).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard
ICdata(24).cellid='bbl034e-a1';
ICdata(24).area='ICc';
ICdata(24).type='Var_SNR';
ICdata(24).note='Great isolation P99 A99; no fixed TORC';
ICdata(24).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(24).passive_spike='bbl034e05_p_PTD.spk.mat';
ICdata(24).passive_rawid=120986;
ICdata(24).active_spike='bbl034e04_a_PTD.spk.mat';
ICdata(24).active_rawid=120985;
ICdata(24).on_BF=1; % 1=on BF target, 0=off BF
ICdata(24).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active easy
ICdata(25).cellid='bbl034e-a1';
ICdata(25).area='ICc';
ICdata(25).type='Var_SNR';
ICdata(25).note='Great isolation P99 A99; no fixed TORC; no second active';
ICdata(25).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(25).passive_spike='bbl034e09_p_PTD.spk.mat';
ICdata(25).passive_rawid=120990;
ICdata(25).active_spike='bbl034e07_a_PTD.spk.mat';
ICdata(25).active_rawid=120988;
ICdata(25).on_BF=0; % 1=on BF target, 0=off BF
ICdata(25).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------


% Post-passive - Active easy
ICdata(26).cellid='bbl036e-a1';
ICdata(26).area='ICc';
ICdata(26).type='Var_SNR';
ICdata(26).note='Good isolation P95 A95; no fixed TORC; no second A';
ICdata(26).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(26).passive_spike='bbl036e09_p_PTD.spk.mat';
ICdata(26).passive_rawid=121038;
ICdata(26).active_spike='bbl036e06_a_PTD.spk.mat';
ICdata(26).active_rawid=121035;
ICdata(26).on_BF=1; % 1=on BF target, 0=off BF
ICdata(26).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active hard
ICdata(27).cellid='bbl039d-a1';
ICdata(27).area='ICc';
ICdata(27).type='Var_SNR';
ICdata(27).note='Great isolation P99 A99; controversial off BF';
ICdata(27).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(27).passive_spike='bbl039d06_p_PTD.spk.mat';
ICdata(27).passive_rawid=121288;
ICdata(27).active_spike='bbl039d04_a_PTD.spk.mat';
ICdata(27).active_rawid=121284;
ICdata(27).on_BF=0; % 1=on BF target, 0=off BF
ICdata(27).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(28).cellid='bbl039d-a1';
ICdata(28).area='ICc';
ICdata(28).type='Var_SNR';
ICdata(28).note='Great isolation P99 A99; controversial off BF';
ICdata(28).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(28).passive_spike='bbl039d06_p_PTD.spk.mat';
ICdata(28).passive_rawid=121288;
ICdata(28).active_spike='bbl039d05_a_PTD.spk.mat';
ICdata(28).active_rawid=121285;
ICdata(28).on_BF=0; % 1=on BF target, 0=off BF
ICdata(28).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(29).cellid='bbl039d-a1';
ICdata(29).area='ICc';
ICdata(29).type='Var_SNR';
ICdata(29).note='Great isolation P99 A99; controversial on BF';
ICdata(29).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(29).passive_spike='bbl039d09_p_PTD.spk.mat';
ICdata(29).passive_rawid=121296;
ICdata(29).active_spike='bbl039d07_a_PTD.spk.mat';
ICdata(29).active_rawid=121289;
ICdata(29).on_BF=1; % 1=on BF target, 0=off BF
ICdata(29).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard
ICdata(30).cellid='bbl039d-a1';
ICdata(30).area='ICc';
ICdata(30).type='Var_SNR';
ICdata(30).note='Great isolation P99 A99; controversial on BF';
ICdata(30).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(30).passive_spike='bbl039d09_p_PTD.spk.mat';
ICdata(30).passive_rawid=121296;
ICdata(30).active_spike='bbl039d08_a_PTD.spk.mat';
ICdata(30).active_rawid=121292;
ICdata(30).on_BF=1; % 1=on BF target, 0=off BF
ICdata(30).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active hard
ICdata(31).cellid='bbl041e-a1';
ICdata(31).area='ICc';
ICdata(31).type='Var_SNR';
ICdata(31).note='Great isolation P99 A99';
ICdata(31).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(31).passive_spike='bbl041e05_p_PTD.spk.mat';
ICdata(31).passive_rawid=121420;
ICdata(31).active_spike='bbl041e03_a_PTD.spk.mat';
ICdata(31).active_rawid=121418;
ICdata(31).on_BF=0; % 1=on BF target, 0=off BF
ICdata(31).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(32).cellid='bbl041e-a1';
ICdata(32).area='ICc';
ICdata(32).type='Var_SNR';
ICdata(32).note='Great isolation P99 A99';
ICdata(32).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(32).passive_spike='bbl041e05_p_PTD.spk.mat';
ICdata(32).passive_rawid=121420;
ICdata(32).active_spike='bbl041e04_a_PTD.spk.mat';
ICdata(32).active_rawid=121419;
ICdata(32).on_BF=0; % 1=on BF target, 0=off BF
ICdata(32).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(33).cellid='bbl041e-a1';
ICdata(33).area='ICc';
ICdata(33).type='Var_SNR';
ICdata(33).note='Great isolation P99 A99';
ICdata(33).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(33).passive_spike='bbl041e09_p_PTD.spk.mat';
ICdata(33).passive_rawid=121424;
ICdata(33).active_spike='bbl041e07_a_PTD.spk.mat';
ICdata(33).active_rawid=121422;
ICdata(33).on_BF=1; % 1=on BF target, 0=off BF
ICdata(33).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard
ICdata(34).cellid='bbl041e-a1';
ICdata(34).area='ICc';
ICdata(34).type='Var_SNR';
ICdata(34).note='Great isolation P99 A95';
ICdata(34).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(34).passive_spike='bbl041e09_p_PTD.spk.mat';
ICdata(34).passive_rawid=121424;
ICdata(34).active_spike='bbl041e08_a_PTD.spk.mat';
ICdata(34).active_rawid=121423;
ICdata(34).on_BF=1; % 1=on BF target, 0=off BF
ICdata(34).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active hard
ICdata(35).cellid='bbl053c-a3';
ICdata(35).area='ICc';
ICdata(35).type='Var_SNR';
ICdata(35).note='OK isolation P95 A90';
ICdata(35).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
ICdata(35).passive_spike='bbl053c05_p_PTD.spk.mat';
ICdata(35).passive_rawid=121934;
ICdata(35).active_spike='bbl053c04_a_PTD.spk.mat';
ICdata(35).active_rawid=121933;
ICdata(35).on_BF=1; % 1=on BF target, 0=off BF
ICdata(35).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active hard
ICdata(36).cellid='bbl053c-a3';
ICdata(36).area='ICc';
ICdata(36).type='Var_SNR';
ICdata(36).note='OK isolation P95 A95; different set of SNRs from prev';
ICdata(36).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
ICdata(36).passive_spike='bbl053c05_p_PTD.spk.mat';
ICdata(36).passive_rawid=121934;
ICdata(36).active_spike='bbl053c07_a_PTD.spk.mat';
ICdata(36).active_rawid=121937;
ICdata(36).on_BF=1; % 1=on BF target, 0=off BF
ICdata(36).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(37).cellid='bbl053c-a3';
ICdata(37).area='ICc';
ICdata(37).type='Var_SNR';
ICdata(37).note='OK isolation P95 A90';
ICdata(37).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
ICdata(37).passive_spike='bbl053c05_p_PTD.spk.mat';
ICdata(37).passive_rawid=121934;
ICdata(37).active_spike='bbl053c08_a_PTD.spk.mat';
ICdata(37).active_rawid=121940;
ICdata(37).on_BF=1; % 1=on BF target, 0=off BF
ICdata(37).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% % Second Post-passive - Active hard
% ICdata(36).cellid='bbl053c-a3';
% ICdata(36).area='ICc';
% ICdata(36).type='Var_SNR';
% ICdata(36).note='OK isolation P95 A95; different set of SNRs from prev';
% ICdata(36).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
% ICdata(36).passive_spike='bbl053c09_p_PTD.spk.mat';
% ICdata(36).passive_rawid=121943;
% ICdata(36).active_spike='bbl053c07_a_PTD.spk.mat';
% ICdata(36).active_rawid=121937;
% ICdata(36).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(36).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
% 
% % Post-passive - Active easy
% ICdata(37).cellid='bbl053c-a3';
% ICdata(37).area='ICc';
% ICdata(37).type='Var_SNR';
% ICdata(37).note='OK isolation P95 A90';
% ICdata(37).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
% ICdata(37).passive_spike='bbl053c09_p_PTD.spk.mat';
% ICdata(37).passive_rawid=121943;
% ICdata(37).active_spike='bbl053c08_a_PTD.spk.mat';
% ICdata(37).active_rawid=121940;
% ICdata(37).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(37).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Post-passive - Active hard
ICdata(38).cellid='bbl058c-a1';
ICdata(38).area='ICc';
ICdata(38).type='Var_SNR';
ICdata(38).note='OK isolation P99 A95; offBF?';
ICdata(38).datapath='/auto/data/daq/Babybell/bbl058c/sorted/';
ICdata(38).passive_spike='bbl058c05_p_PTD.spk.mat';
ICdata(38).passive_rawid=122575;
ICdata(38).active_spike='bbl058c04_a_PTD.spk.mat';
ICdata(38).active_rawid=122567;
ICdata(38).on_BF=0; % 1=on BF target, 0=off BF
ICdata(38).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(39).cellid='bbl058c-a1';
ICdata(39).area='ICc';
ICdata(39).type='Var_SNR';
ICdata(39).note='OK isolation P99 A99; offBF?';
ICdata(39).datapath='/auto/data/daq/Babybell/bbl058c/sorted/';
ICdata(39).passive_spike='bbl058c05_p_PTD.spk.mat';
ICdata(39).passive_rawid=122575;
ICdata(39).active_spike='bbl058c06_a_PTD.spk.mat';
ICdata(39).active_rawid=122579;
ICdata(39).on_BF=0; % 1=on BF target, 0=off BF
ICdata(39).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active tone only
ICdata(40).cellid='bbl058c-a1';
ICdata(40).area='ICc';
ICdata(40).type='Var_SNR';
ICdata(40).note='OK isolation P99 A99; offBF?';
ICdata(40).datapath='/auto/data/daq/Babybell/bbl058c/sorted/';
ICdata(40).passive_spike='bbl058c05_p_PTD.spk.mat';
ICdata(40).passive_rawid=122575;
ICdata(40).active_spike='bbl058c07_a_PTD.spk.mat';
ICdata(40).active_rawid=122580;
ICdata(40).on_BF=0; % 1=on BF target, 0=off BF
ICdata(40).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Post-passive - Active easy
ICdata(42).cellid='bbl060c-a1';
ICdata(42).area='ICc';
ICdata(42).type='Var_SNR';
ICdata(42).note='OK isolation P90 A90; merge with prev';
ICdata(42).datapath='/auto/data/daq/Babybell/bbl060c/sorted/';
ICdata(42).passive_spike='bbl060c08_p_PTD.spk.mat';
ICdata(42).passive_rawid=122712;
ICdata(42).active_spike='bbl060c07_a_PTD.spk.mat';
ICdata(42).active_rawid=122710;
ICdata(42).on_BF=0; % 1=on BF target, 0=off BF
ICdata(42).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------


% Post-passive - Active easy
ICdata(44).cellid='bbl060c-a2';
ICdata(44).area='ICc';
ICdata(44).type='Var_SNR';
ICdata(44).note='OK isolation P90 A90; merge with prev';
ICdata(44).datapath='/auto/data/daq/Babybell/bbl060c/sorted/';
ICdata(44).passive_spike='bbl060c08_p_PTD.spk.mat';
ICdata(44).passive_rawid=122712;
ICdata(44).active_spike='bbl060c07_a_PTD.spk.mat';
ICdata(44).active_rawid=122710;
ICdata(44).on_BF=0; % 1=on BF target, 0=off BF
ICdata(44).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Post-passive - Active hard //3 units in one tetrode (first active not
% stable)
ICdata(45).cellid='bbl071d-a1';
ICdata(45).area='ICc';
ICdata(45).type='Var_SNR';
ICdata(45).note='OK isolation P90 A95; alterneted P and A';
ICdata(45).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(45).passive_spike='bbl071d05_p_PTD.spk.mat';
ICdata(45).passive_rawid=125262;
ICdata(45).active_spike='bbl071d06_a_PTD.spk.mat';
ICdata(45).active_rawid=125264;
ICdata(45).on_BF=1; % 1=on BF target, 0=off BF
ICdata(45).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active pure tone
ICdata(46).cellid='bbl071d-a1';
ICdata(46).area='ICc';
ICdata(46).type='Var_SNR';
ICdata(46).note='OK isolation P95 A95';
ICdata(46).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(46).passive_spike='bbl071d05_p_PTD.spk.mat';
ICdata(46).passive_rawid=125262;
ICdata(46).active_spike='bbl071d08_a_PTD.spk.mat';
ICdata(46).active_rawid=125267;
ICdata(46).on_BF=1; % 1=on BF target, 0=off BF
ICdata(46).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active easy
ICdata(47).cellid='bbl071d-a1';
ICdata(47).area='ICc';
ICdata(47).type='Var_SNR';
ICdata(47).note='OK isolation P99 A90';
ICdata(47).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(47).passive_spike='bbl071d05_p_PTD.spk.mat';
ICdata(47).passive_rawid=125262;
ICdata(47).active_spike='bbl071d10_a_PTD.spk.mat';
ICdata(47).active_rawid=125270;
ICdata(47).on_BF=1; % 1=on BF target, 0=off BF
ICdata(47).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% % Mid-passive - Active hard //3 units in one tetrode (first active not
% % stable)
% ICdata(45).cellid='bbl071d-a1';
% ICdata(45).area='ICc';
% ICdata(45).type='Var_SNR';
% ICdata(45).note='OK isolation P90 A95; alterneted P and A';
% ICdata(45).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(45).passive_spike='bbl071d07_p_PTD.spk.mat';
% ICdata(45).passive_rawid=125265;
% ICdata(45).active_spike='bbl071d06_a_PTD.spk.mat';
% ICdata(45).active_rawid=125264;
% ICdata(45).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(45).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
% 
% % Mid-passive - Active pure tone
% ICdata(46).cellid='bbl071d-a1';
% ICdata(46).area='ICc';
% ICdata(46).type='Var_SNR';
% ICdata(46).note='OK isolation P95 A95';
% ICdata(46).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(46).passive_spike='bbl071d07_p_PTD.spk.mat';
% ICdata(46).passive_rawid=125265;
% ICdata(46).active_spike='bbl071d08_a_PTD.spk.mat';
% ICdata(46).active_rawid=125267;
% ICdata(46).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(46).difficulty=0;  % 0=pure tone, 1=easy, 2=hard
% 
% % Mid-passive - Active easy
% ICdata(47).cellid='bbl071d-a1';
% ICdata(47).area='ICc';
% ICdata(47).type='Var_SNR';
% ICdata(47).note='OK isolation P99 A90';
% ICdata(47).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(47).passive_spike='bbl071d07_p_PTD.spk.mat';
% ICdata(47).passive_rawid=125265;
% ICdata(47).active_spike='bbl071d10_a_PTD.spk.mat';
% ICdata(47).active_rawid=125270;
% ICdata(47).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(47).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% % Post-passive - Active hard //3 units in one tetrode (first active not
% % stable)
% ICdata(45).cellid='bbl071d-a1';
% ICdata(45).area='ICc';
% ICdata(45).type='Var_SNR';
% ICdata(45).note='OK isolation P90 A95; alterneted P and A';
% ICdata(45).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(45).passive_spike='bbl071d09_p_PTD.spk.mat';
% ICdata(45).passive_rawid=125268;
% ICdata(45).active_spike='bbl071d06_a_PTD.spk.mat';
% ICdata(45).active_rawid=125264;
% ICdata(45).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(45).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
% 
% % Post-passive - Active pure tone
% ICdata(46).cellid='bbl071d-a1';
% ICdata(46).area='ICc';
% ICdata(46).type='Var_SNR';
% ICdata(46).note='OK isolation P95 A95';
% ICdata(46).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(46).passive_spike='bbl071d09_p_PTD.spk.mat';
% ICdata(46).passive_rawid=125268;
% ICdata(46).active_spike='bbl071d08_a_PTD.spk.mat';
% ICdata(46).active_rawid=125267;
% ICdata(46).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(46).difficulty=0;  % 0=pure tone, 1=easy, 2=hard
% 
% % Post-passive - Active easy
% ICdata(47).cellid='bbl071d-a1';
% ICdata(47).area='ICc';
% ICdata(47).type='Var_SNR';
% ICdata(47).note='OK isolation P99 A90';
% ICdata(47).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(47).passive_spike='bbl071d09_p_PTD.spk.mat';
% ICdata(47).passive_rawid=125268;
% ICdata(47).active_spike='bbl071d10_a_PTD.spk.mat';
% ICdata(47).active_rawid=125270;
% ICdata(47).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(47).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Post-passive - Active hard (not great iso in passive)
ICdata(48).cellid='bbl071d-a2';
ICdata(48).area='ICc';
ICdata(48).type='Var_SNR';
ICdata(48).note='OK isolation P85 A99';
ICdata(48).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(48).passive_spike='bbl071d09_p_PTD.spk.mat';
ICdata(48).passive_rawid=125268;
ICdata(48).active_spike='bbl071d10_a_PTD.spk.mat';
ICdata(48).active_rawid=125270;
ICdata(48).on_BF=1; % 1=on BF target, 0=off BF
ICdata(48).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active pure tone
ICdata(49).cellid='bbl071d-a3';
ICdata(49).area='ICc';
ICdata(49).type='Var_SNR';
ICdata(49).note='OK isolation P99 A90';
ICdata(49).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(49).passive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(49).passive_rawid=125265;
ICdata(49).active_spike='bbl071d08_a_PTD.spk.mat';
ICdata(49).active_rawid=125267;
ICdata(49).on_BF=1; % 1=on BF target, 0=off BF
ICdata(49).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active easy
ICdata(50).cellid='bbl071d-a3';
ICdata(50).area='ICc';
ICdata(50).type='Var_SNR';
ICdata(50).note='OK isolation P99 A90';
ICdata(50).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(50).passive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(50).passive_rawid=125265;
ICdata(50).active_spike='bbl071d10_a_PTD.spk.mat';
ICdata(50).active_rawid=125270;
ICdata(50).on_BF=1; % 1=on BF target, 0=off BF
ICdata(50).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% % Second Post-passive - Active easy
% ICdata(50).cellid='bbl071d-a3';
% ICdata(50).area='ICc';
% ICdata(50).type='Var_SNR';
% ICdata(50).note='OK isolation P99 A90';
% ICdata(50).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
% ICdata(50).passive_spike='bbl071d09_p_PTD.spk.mat';
% ICdata(50).passive_rawid=125268;
% ICdata(50).active_spike='bbl071d10_a_PTD.spk.mat';
% ICdata(50).active_rawid=125270;
% ICdata(50).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(50).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Post-passive - Active pure tone 
ICdata(51).cellid='bbl074g-a1';
ICdata(51).area='ICc';
ICdata(51).type='Var_SNR';
ICdata(51).note='OK isolation P99 A99; off and on responses'
ICdata(51).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
ICdata(51).passive_spike='bbl074g06_p_PTD.spk.mat';
ICdata(51).passive_rawid=125667;
ICdata(51).active_spike='bbl074g05_a_PTD.spk.mat';
ICdata(51).active_rawid=125665;
ICdata(51).on_BF=1; % 1=on BF target, 0=off BF
ICdata(51).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


% Post-passive - Active hard
ICdata(52).cellid='bbl074g-a1';
ICdata(52).area='ICc';
ICdata(52).type='Var_SNR';
ICdata(52).note='OK isolation P99 A99; off and on responses'
ICdata(52).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
ICdata(52).passive_spike='bbl074g06_p_PTD.spk.mat';
ICdata(52).passive_rawid=125667;
ICdata(52).active_spike='bbl074g07_a_PTD.spk.mat';
ICdata(52).active_rawid=125668;
ICdata(52).on_BF=1; % 1=on BF target, 0=off BF
ICdata(52).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% % Second Post-passive - Active pure tone 
% ICdata(51).cellid='bbl074g-a1';
% ICdata(51).area='ICc';
% ICdata(51).type='Var_SNR';
% ICdata(51).note='OK isolation P99 A99; off and on responses'
% ICdata(51).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
% ICdata(51).passive_spike='bbl074g08_p_PTD.spk.mat';
% ICdata(51).passive_rawid=125669;
% ICdata(51).active_spike='bbl074g05_a_PTD.spk.mat';
% ICdata(51).active_rawid=125665;
% ICdata(51).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(51).difficulty=0;  % 0=pure tone, 1=easy, 2=hard
% 
% 
% % Second Post-passive - Active hard
% ICdata(52).cellid='bbl074g-a1';
% ICdata(52).area='ICc';
% ICdata(52).type='Var_SNR';
% ICdata(52).note='OK isolation P99 A99; off and on responses'
% ICdata(52).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
% ICdata(52).passive_spike='bbl074g08_p_PTD.spk.mat';
% ICdata(52).passive_rawid=125669;
% ICdata(52).active_spike='bbl074g07_a_PTD.spk.mat';
% ICdata(52).active_rawid=125668;
% ICdata(52).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(52).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

 
% %--------------------------------------------------------------------------
 

% Post-passive - Active easy 
ICdata(53).cellid='bbl078k-a1';
ICdata(53).area='ICc';
ICdata(53).type='Var_SNR';
ICdata(53).note='OK isolation P95 A95; off and on responses; sorted all at once'
ICdata(53).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(53).passive_spike='bbl078k06_p_PTD.spk.mat';
ICdata(53).passive_rawid=125889;
ICdata(53).active_spike='bbl078k04_a_PTD.spk.mat';
ICdata(53).active_rawid=125887;
ICdata(53).on_BF=1; % 1=on BF target, 0=off BF
ICdata(53).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard
ICdata(54).cellid='bbl078k-a1';
ICdata(54).area='ICc';
ICdata(54).type='Var_SNR';
ICdata(54).note='OK isolation P95 A95; off and on responses; sorted all at once'
ICdata(54).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(54).passive_spike='bbl078k06_p_PTD.spk.mat';
ICdata(54).passive_rawid=125889;
ICdata(54).active_spike='bbl078k05_a_PTD.spk.mat';
ICdata(54).active_rawid=125888;
ICdata(54).on_BF=1; % 1=on BF target, 0=off BF
ICdata(54).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard
ICdata(55).cellid='bbl078k-a1';
ICdata(55).area='ICc';
ICdata(55).type='Var_SNR';
ICdata(55).note='OK isolation P95 A95; off and on responses; sorted all at once'
ICdata(55).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(55).passive_spike='bbl078k06_p_PTD.spk.mat';
ICdata(55).passive_rawid=125889;
ICdata(55).active_spike='bbl078k07_a_PTD.spk.mat';
ICdata(55).active_rawid=125890;
ICdata(55).on_BF=1; % 1=on BF target, 0=off BF
ICdata(55).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


% % Post-passive - Active easy 
% ICdata(53).cellid='bbl078k-a1';
% ICdata(53).area='ICc';
% ICdata(53).type='Var_SNR';
% ICdata(53).note='OK isolation P95 A95; off and on responses; sorted all at once'
% ICdata(53).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
% ICdata(53).passive_spike='bbl078k08_p_PTD.spk.mat';
% ICdata(53).passive_rawid=125891;
% ICdata(53).active_spike='bbl078k04_a_PTD.spk.mat';
% ICdata(53).active_rawid=125887;
% ICdata(53).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(53).difficulty=1;  % 0=pure tone, 1=easy, 2=hard
% 
% % Post-passive - Active hard
% ICdata(54).cellid='bbl078k-a1';
% ICdata(54).area='ICc';
% ICdata(54).type='Var_SNR';
% ICdata(54).note='OK isolation P95 A95; off and on responses; sorted all at once'
% ICdata(54).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
% ICdata(54).passive_spike='bbl078k08_p_PTD.spk.mat';
% ICdata(54).passive_rawid=125891;
% ICdata(54).active_spike='bbl078k05_a_PTD.spk.mat';
% ICdata(54).active_rawid=125888;
% ICdata(54).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(54).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
% 
% % Post-passive - Active hard
% ICdata(55).cellid='bbl078k-a1';
% ICdata(55).area='ICc';
% ICdata(55).type='Var_SNR';
% ICdata(55).note='OK isolation P95 A95; off and on responses; sorted all at once'
% ICdata(55).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
% ICdata(55).passive_spike='bbl078k08_p_PTD.spk.mat';
% ICdata(55).passive_rawid=125891;
% ICdata(55).active_spike='bbl078k07_a_PTD.spk.mat';
% ICdata(55).active_rawid=125890;
% ICdata(55).on_BF=1; % 1=on BF target, 0=off BF
% ICdata(55).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Post-passive - Active pure tone
ICdata(56).cellid='bbl081d-a1';
ICdata(56).area='ICx';
ICdata(56).type='Var_SNR';
ICdata(56).note='OK isolation P95 A95; sorted all at once'
ICdata(56).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(56).passive_spike='bbl081d05_p_PTD.spk.mat';
ICdata(56).passive_rawid=126474;
ICdata(56).active_spike='bbl081d04_a_PTD.spk.mat';
ICdata(56).active_rawid=126473;
ICdata(56).on_BF=0; % 1=on BF target, 0=off BF
ICdata(56).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Post-passive - Active hard 
ICdata(57).cellid='bbl081d-a1';
ICdata(57).area='ICx';
ICdata(57).type='Var_SNR';
ICdata(57).note='OK isolation P95 A95; sorted all at once'
ICdata(57).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(57).passive_spike='bbl081d05_p_PTD.spk.mat';
ICdata(57).passive_rawid=126474;
ICdata(57).active_spike='bbl081d06_a_PTD.spk.mat';
ICdata(57).active_rawid=126475;
ICdata(57).on_BF=0; % 1=on BF target, 0=off BF
ICdata(57).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% % Second Post-passive - Active pure tone
% ICdata(56).cellid='bbl081d-a1';
% ICdata(56).area='ICx';
% ICdata(56).type='Var_SNR';
% ICdata(56).note='OK isolation P95 A95; sorted all at once'
% ICdata(56).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
% ICdata(56).passive_spike='bbl081d07_p_PTD.spk.mat';
% ICdata(56).passive_rawid=126477;
% ICdata(56).active_spike='bbl081d04_a_PTD.spk.mat';
% ICdata(56).active_rawid=126473;
% ICdata(56).on_BF=0; % 1=on BF target, 0=off BF
% ICdata(56).difficulty=0;  % 0=pure tone, 1=easy, 2=hard
% 
% % Second Post-passive - Active hard 
% ICdata(57).cellid='bbl081d-a1';
% ICdata(57).area='ICx';
% ICdata(57).type='Var_SNR';
% ICdata(57).note='OK isolation P95 A95; sorted all at once'
% ICdata(57).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
% ICdata(57).passive_spike='bbl081d07_p_PTD.spk.mat';
% ICdata(57).passive_rawid=126477;
% ICdata(57).active_spike='bbl081d06_a_PTD.spk.mat';
% ICdata(57).active_rawid=126475;
% ICdata(57).on_BF=0; % 1=on BF target, 0=off BF
% ICdata(57).difficulty=2;  % 0=pure tone, 1=easy, 2=hard



% % save the struct in data folder
% fname='/auto/users/daniela/ICdata';
% save(fname,'ICdata');
% 























