function [gain]=fitgainonly(predpsth,resp)

r0=resp;
r1=predpsth;
d1=sum(r1.^2);

gain=sum(r0.*r1)./d1;

