# README #

Matlab tools for analysis in LBHB

Not very organized, but a couple subdirectories contain topically related code:

    pupil - tools related to measuring spike rate vs. pupil relationships.
    ssa - stimulus-specific adaptation code
