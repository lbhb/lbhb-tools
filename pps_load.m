% function [r,stim,stimparam,pr]=pps_load(cellid,options)
%
% load PPS data and concatenate all trials into a single spectrogram and
% response vector
%
% options - defaults
%options.filtfmt='parm';
%options.chancount=0;
%options.rasterfs=100;
%options.usesorted=1;
%options.datause='Reference Only';
%options.runclass='PPS';
%options.pupil=getparm(options,'pupil',1);
%
% returns
%  r: Time X Rep matrix (psth=mean(r,2))
%  stim: Time X Frequency matrix 
%  stimparam: structure with some details about the stimulus
%  pr: Time X Rep matrix of pupil (if options.pupil==1)
%
% SVD hacked out of pps_fra 2017-01-19
%
function [r,stim,stimparam,pr]=pps_load(cellid,options)

if ~exist('options','var'),
   options=struct();
end

options.filtfmt=getparm(options,'filtfmt','parm');
options.rasterfs=getparm(options,'rasterfs',100);
options.pupil=getparm(options,'pupil',0);
options.chancount=0;
options.usesorted=1;
options.datause='Reference Only';
options.runclass='PPS';

if exist(cellid,'file'),
   % special case, user passed parm file name, not cellid
   parmfile=cellid;
   cellid=sprintf('%s chan %d unit %d',basename(parmfile),options.channel,options.unit);
   fprintf('%s: Analyzing channel %d (rasterfs %d)\n',...
      parmfile,options.channel,options.rasterfs);
   parmfiles={parmfile};
else
   if options.pupil,
      cfd=dbgetscellfile('cellid',cellid,'runclass','PPS','pupil',2);
   else
      cfd=dbgetscellfile('cellid',cellid,'runclass','PPS');
   end
   parmfiles={};
   for ii=1:1, %length(cfd),
      parmfiles{ii}=[cfd(ii).stimpath,cfd(ii).stimfile];
   end
   options.unit=cfd(ii).unit;
   options.channel=cfd(ii).channum;
end

LoadMFile(parmfiles{1});

options.PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
options.PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;
options.ReferenceClass=exptparams.TrialObject.ReferenceClass;

PreBins=round(options.PreStimSilence.*options.rasterfs);
PostBins=round(options.PostStimSilence.*options.rasterfs);
unit=options.unit;
channel=options.channel;

Ar=[];
Astim=[];
Apr0=[];

for ii=1:length(parmfiles),
   disp('Loading response...');
   [r,tags,trialset]=raster_load(parmfiles{ii},options.channel,options.unit,options);
   realrepcount=size(r,2);
   
   if ~isempty(findstr(lower(options.datause),'per trial')),
      disp('Loading stimulus spectrogram...');
      %  options - struct with optional fields:
      %   filtfmt - currently can be 'wav' or 'specgram' or 'wav2aud' or
      %              'audspectrogram' or 'envelope' (collapsed over frequency)
      %   fsout - output sampling rate
      %   chancount - output number of channels
      toptions=struct('filtfmt',options.filtfmt,'fsout',options.rasterfs,...
         'chancount',options.chancount);
      [stim,stimparam]=loadstimbytrial(parmfiles{ii},toptions);
      stim=stim(:,1:size(r,1),trialset);
      r=permute(r,[1 3 2]);
   else
      disp('Loading stimulus spectrogram...');
      [stim,stimparam]=loadstimfrombaphy(parmfiles{ii},[],[],options.filtfmt, ...
         options.rasterfs,options.chancount,0,0);
   end
   
   %if ~isempty(findstr(cellid,'BOL005')),
  %    PreBins=round(PreBins.*10/16);
   %   PostBins=size(r,1)-size(stim,2)-PreBins;
   %end
   
   stim=reshape(stim,size(stim,1),size(stim,2)*size(stim,3));
   %stim=stim(:,PreBins+1:(end-PostBins),:);
   r=r(PreBins+1:(end-PostBins),:,:);
   stim=stim(:,:)';
   r=permute(r,[2 1 3]);
   r=r(:,:)';
   r=r.*options.rasterfs;
   
   if options.pupil,
      pr_options = options;
      pr_options.pupil = 1;
      pr_options.pupil_median=1;
      %pr_options.pupil_mm=1;
      pr_options.pupil_deblink=1;
      pr_options.pupil_offset=1; %0.75;
      pr_options.tag_masks={'Ref'};
      [pr0,ptags,ptrialset] = loadevpraster(parmfiles{ii}, pr_options);
      
      pr0=pr0(PreBins+1:(end-PostBins),:,:);
      pr0=permute(pr0,[2 1 3]);
      pr0=pr0(:,:)';
   end

   Ar=cat(1,Ar,r);
   Astim=cat(1,Astim,stim);
   if options.pupil,
      Apr0=cat(1,Apr0,pr0);
   else
      Apr0=[];
   end
end

r=Ar;
stim=Astim;
pr=Apr0;

stimparam.freqs=exptparams.TrialObject.ReferenceHandle.Frequencies;
stimparam.levels=exptparams.TrialObject.OveralldB-...
   exptparams.TrialObject.ReferenceHandle.AttenuationLevels;
stimparam.parmfiles=parmfiles;

return


