
batchid=287;

sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid),' ORDER BY cellid'];
rundata=mysql(sql);
cellids={rundata.cellid};

dpall=[];
cidxgood=[];
lowDIall=[];
lowDImidall=[];
xcall=[];
for cidx=1:length(cellids),
    tsp_decode;
    if sum(abs(dprime))>0,
    %if mean(dprime)>0,
        dpall=cat(1,dpall,dprime);
        cidxgood=cat(1,cidxgood,cidx);
        lowDIall=cat(1,lowDIall,lowDI);
        xcall=cat(1,xcall,[xcA1 xcA2]);
        
        a=regexp(cellids{cidx},'por(\d*).*$','tokens');
        pormid=0;
        if ~isempty(a),
            a=str2num(a{1}{1});
            if a>=31 && a<=65,
                pormid=1;
            end
        end
        lowDImidall=cat(1,lowDImidall,double(lowDI|pormid));
        
        d1=find(lowDIall==0);
        d2=find(lowDIall==1);
        
        sfigure(2);
        
        subplot(2,2,1);
        plot(dpall(d2,2),dpall(d2,1),'o','Color',[.8 .8 .8],'MarkerFaceColor',[.8 .8 .8],'MarkerSize',4);
        hold on
        plot(dpall(d1,2),dpall(d1,1),'o','Color',[0 0 0],'MarkerFaceColor',[0 0 0],'MarkerSize',4);
        plot([-.5 .7],[-.5 .7],'k--');
        hold off
        xlabel('attend off-BF');
        ylabel('attend on-BF');
        title(sprintf('mean dp=[%.2f %.2f]',mean(dpall)));
        axis tight square
        
        subplot(2,2,2);
        DOMEDIAN=1;
        
        dpd1=dpall(d1,1)-dpall(d1,2);
        dpd2=dpall(d2,1)-dpall(d2,2);
        
        histcomp(dpd1,dpd2,'D'' hi DI','lo DI','frac',[-0.4 0.4], DOMEDIAN, 16);
        
        subplot(2,2,3);
        %d1=find(lowDImidall==0);
        %d2=find(lowDImidall==1);
        plot(xcall(d2,3),xcall(d2,1),'o','Color',[.8 .8 .8],'MarkerFaceColor',[.8 .8 .8],'MarkerSize',4);
        hold on
        plot(xcall(d1,3),xcall(d1,1),'o','Color',[0 0 0],'MarkerFaceColor',[0 0 0],'MarkerSize',4);
        plot([-.1 .7],[-.1 .7],'k--');
        hold off
        axis tight square
        
        subplot(2,2,4);
        
        xcd1=xcall(d1,1)-xcall(d1,2);
        xcd2=xcall(d2,1)-xcall(d2,2);
        histcomp(xcd1,xcd2,'xc hi DI','lo DI','frac',[-0.2 0.2], DOMEDIAN, 16);

        colormap(gray);
        drawnow
    end
end
