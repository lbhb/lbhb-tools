% function [beta,beta_err,z]=resp_resp_plot(r1,r2,bincount,linecolor,fitdcg);
function [beta,beta_err,z]=resp_resp_plot(r1,r2,bincount,linecolor,fitdcg);

if ~exist('linecolor','var'),
    linecolor=[0 0 1];
end
if ~exist('fitdcg','var'),
    fitdcg=1;  % if 0, fit g only
end

r1=squeeze(nanmean(r1,2));
r2=squeeze(nanmean(r2,2));

if size(r1,1)>size(r2,1),
    r1=r1(1:size(r2,1),:,:);
elseif size(r1,1)<size(r2,1),
    r2=r2(1:size(r1,1),:,:);
end

m=(r1(:)+r2(:))./2;

ii=find(~isnan(m));
m=m(ii);
r1=r1(ii);
r2=r2(ii);
r1=gsmooth(r1,2);
r2=gsmooth(r2,2);
m=(r1+r2)./2;

[~,si]=sort(m);
b1=max(find(m(si)==min(m)));
if ~isempty(b1) && b1>length(m)./bincount,

    bb=[1 round(linspace(b1+1,length(m)+1,bincount))];
else
    bb=round(linspace(1,length(m)+1,bincount+1));
end
%fprintf('%d samples, %d bins\n',length(m),bincount);

rb=zeros(bincount,2);
re=zeros(bincount,2);
for ii=1:bincount,
    bbr=si(bb(ii):(bb(ii+1)-1));
    rb(ii,1)=mean(r1(bbr));
    rb(ii,2)=mean(r2(bbr));
    re(ii,1)=std(r1(bbr))./sqrt(length(bbr));
    re(ii,2)=std(r2(bbr))./sqrt(length(bbr));
end

dd=[r1(si) r2(si)];
dd=dd(b1:end,:);
smbins=ceil(length(dd)./bincount./30);
smbins=4;
if smbins>1
   smfilt=ones(smbins,1)./smbins;
   dd=rconv2(dd,smfilt);
   dd=dd(round(smbins./2):smbins:end,:);
end

jackcount=min(length(dd)-1,20);
[~,jidx]=sort(rand(length(dd),1));
jackstep=length(jidx)./jackcount;
jbeta=zeros(jackcount,2);
for jj=1:jackcount,
    jb=[1:round(jackstep.*(jj-1)+1) round(jackstep*jj):length(jidx)];
    %jb=jidx(jb);
    
    if fitdcg,
        [dcgparms]=fitdcgain(dd(jb,1),dd(jb,2));
        [dcgparms2]=fitdcgain(dd(jb,2),dd(jb,1));
        dcgparms(1)=(dcgparms(1)-dcgparms2(1)./dcgparms2(2))./2;
        dcgparms(2)=(dcgparms(2)+1./dcgparms2(2))./2;
    else
        % only gain
        [g]=fitgainonly(dd(jb,1),dd(jb,2));
        [g2]=fitgainonly(dd(jb,2),dd(jb,1));
        %dcgparms=[0 (g + 1./g2)./2];
        dcgparms=[0 exp((log(g) - log(g2))./2)];
        if g<0.4 || g>2 || g2<0.4 || g2>2,
           %keyboard
        end
    end
    jbeta(jj,:)=dcgparms;
end

ljbeta=[jbeta(:,1) log(jbeta(:,2))];
lbeta=mean(ljbeta);
lbeta_err=std(ljbeta,0).*sqrt(jackcount-1);
z=lbeta./lbeta_err;
beta=mean(jbeta);
beta_err=std(jbeta,0).*sqrt(jackcount-1);

xx=[min(m) max(rb(:)+re(:))];
yy=beta(1)+xx.*beta(2);

%figure
plot([min(m) max(rb(:)+re(:))],[min(m) max(rb(:)+re(:))],'k--');
hold on
plot(xx,yy,'g--');
errorbar(rb(:,1),rb(:,2),re(:,2),'Color',linecolor);
hold off
if fitdcg,
    text(xx(1),xx(2),sprintf('(d,g)=(%.2f,%.2f)',beta),...
         'VerticalAlign','top');
else
    text(xx(1),xx(2),sprintf('(g)=(%.2f)',beta(2)),...
         'VerticalAlign','top');
end
axis tight equal
