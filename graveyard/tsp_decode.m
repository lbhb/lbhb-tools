
batchid=287;

narf_set_path

dbopen

sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid),' ORDER BY cellid'];
rundata=mysql(sql);

cellids={rundata.cellid};

singledata=mysql(['SELECT * FROM gSingleCell WHERE cellid="',...
                  cellids{cidx},'"']);

%cidx=6;
rasterfs=100;

res=tsp_raster_load(cellids{cidx},batchid,rasterfs,0);
if ~isfield(res,'resppaths'),
    res=tsp_raster_load(cellids{cidx},batchid,rasterfs,1);
end
fprintf('cellid %s\n',cellids{cidx});

ct=dbReadTuning(cellids{cidx});
fprintf('BNB BF: %d\n',ct.bnbbf);
if isfield(ct,'bf'),
    fprintf('TOR BF: %d\n',ct.bf);
end

ff=find(strcmp(res.filecode,'A1') | strcmp(res.filecode,'A2'));
for jj=1:length(ff),
    maxidx=min(find(res.baphyparms{ff(jj)}.Trial_TargetIdxFreq==...
                    max(res.baphyparms{ff(jj)}.Trial_TargetIdxFreq)));
    if isfield(res.baphyparms{ff(jj)},'Trial_TargetChannel'),
        tarchan=res.baphyparms{ff(jj)}.Trial_TargetChannel(maxidx);
    else
        tarchan=maxidx;
    end
    fprintf('(%d) %s: %d (%d-%d)\n',ff(jj),...
            res.filecode{ff(jj)},tarchan,...
            res.baphyparms{ff(jj)}.Ref_LowFreq(tarchan),...
            res.baphyparms{ff(jj)}.Ref_HighFreq(tarchan));
end

dprime=[0 0];
f1=max(find(strcmp(res.filecode,'A1')));
f2=max(find(strcmp(res.filecode,'A2')));
if strcmp(cellids{cidx}(1:6),'por049'),
    disp('unstable cell'),
    return
end
if isempty(f1) | isempty(f2),
    disp('missing A1 or A2'),
    return
end
if res.DI(2,f1)<60 || res.DI(4,f2)<60 || res.DI(1,f1)<=50 || res.DI(3,f2)<=50,
    disp('low DI');
    lowDI=1;
    %return
else
    lowDI=0;
end

% reconstruction stuff
a=regexp(cellids{cidx},'por(\d*).*$','tokens');
pormid=0;
if ~isempty(a),
    a=str2num(a{1}{1});
    if a>=31 && a<=65,
        pormid=1;
    end
end

stim=cell(length(res.stimfiles),1);
for jj=1:length(res.stimfiles),
    parmfile=res.stimpaths{jj};
    if pormid && ismember(jj,[f1 f2]),
        options=struct();
        options.filtfmt='envelope';
        options.fsout=rasterfs;
        options.chancount=0;
        [stim{jj},tstimparam] = loadstimbytrial(res.stimpaths{jj},options);
        
        options=struct();
        options.rasterfs=rasterfs;
        options.unit=singledata.unit;
        options.channel=singledata.channum;
        options.includeincorrect=0;
        options.includeprestim=1;
        options.tag_masks={'SPECIAL-TRIAL'};
        
        spkfile=res.resppaths{jj};
        [r1,~,trialset1,exptevents1]=loadspikeraster(spkfile,options);
        stim{jj}=stim{jj}(:,:,trialset1);
        for trialidx=1:size(stim{jj},3),
            gg=find(~isnan(stim{jj}(1,:,trialidx)));
            r1((gg(end)+1):end,trialidx)=nan;
        end
        if jj==f1,
            resp1=permute(r1,[1 3 2]);
        else
            resp2=permute(r1,[1 3 2]);
        end
    else
        [stim{jj},tstimparam]=loadstimfrombaphy(parmfile,[],[],'envelope',...
                                                rasterfs,0,0,1);
    end
end
p=setdiff(1:length(res.stimfiles),[f1 f2]);
if ~pormid,
    resp1=res.rresp{f1};
    resp2=res.rresp{f2};
end
respp=cat(2,res.rresp{p});
resprange=1:size(resp1,1);
trange=51:200;
mt=10;
if size(resp1,2)<mt,
    resp1=cat(2,resp1,nan(size(resp1,1),mt-size(resp1,2),size(resp1,3)));
end
if size(resp2,2)<mt,
    resp2=cat(2,resp2,nan(size(resp2,1),mt-size(resp2,2),size(resp2,3)));
end
if size(respp,2)<mt,
    respp=cat(2,respp,nan(size(respp,1),mt-size(respp,2),size(respp,3)));
end
resp1=resp1(trange,1:mt,:);
resp2=resp2(trange,1:mt,:);
respp=respp(trange,1:mt,:);
stim1=stim{f1};
stim2=stim{f2};
stimpas=stim{p(1)};
if size(stimpas,3)==34,
    if ~pormid,
        stim1=stim1(:,:,[1 6:end]);
        stim2=stim2(:,:,[1 6:end]);
        resp1=resp1(:,:,[1 6:end],:);
        resp2=resp2(:,:,[1 6:end],:);
    end
    stimpas=stimpas(:,:,[1 6:end]);
    respp=respp(:,:,[1 6:end],:);
end

params.maxlag=[-15 15];
params.jackcount=18;

stim0=stimpas(:,1:size(respp,1),:);
stim1=stim1(:,1:size(resp1,1),:);
stim2=stim2(:,1:size(resp2,1),:);
r0=permute(squeeze(nanmean(respp,2)),[1 3 2]);
r0(isnan(r0))=0;
r1=zeros(size(resp1,1),size(resp1,4),size(resp1,3)).*nan;
r2=zeros(size(r1)).*nan;
for ii=1:size(resp1,4),
    for jj=1:size(resp1,3),
        ff=sum(~isnan(resp1(:,:,jj,ii)));
        r1(:,ii,jj)=resp1(:,min(find(ff==max(ff))),jj,ii);
    end
end
for ii=1:size(resp2,4),
    for jj=1:size(resp2,3),
        ff=sum(~isnan(resp2(:,:,jj,ii)));
        r2(:,ii,jj)=resp2(:,min(find(ff==max(ff))),jj,ii);
    end
end
for ii=1:size(r1,2)
    r0(:,ii,:)=r0(:,ii,:)-nanmean(nanmean(r0(:,ii,:)));
    r1(:,ii,:)=r1(:,ii,:)-nanmean(nanmean(r1(:,ii,:)));
    r2(:,ii,:)=r2(:,ii,:)-nanmean(nanmean(r2(:,ii,:)));
end

if 1,
    tr=r1(:);
    tk=find(~isnan(r1));
    tr=tr(tk);
    ts=stim1(:,tk);
    [teststim1,xcperchan1]=quick_recon(tr,ts,tr,params);
    tr=r2(:);
    tk=find(~isnan(r2));
    tr=tr(tk);
    ts=stim2(:,tk);
    [teststim2,xcperchan2]=quick_recon(tr,ts,tr,params);
    if tarchan==1,
        xcA1=xcperchan1';
        xcA2=xcperchan2';
    else
        xcA1=xcperchan1([2 1])';
        xcA2=xcperchan2([2 1])';
    end
else
    [teststim1,xcperchan]=quick_recon(r0,stim0,r1,params);
    teststim1=nanmean(teststim1,4);
    [teststim2,xcperchan]=quick_recon(r0,stim0,r2,params);
    teststim2=nanmean(teststim2,4);
    %tt=find(~isnan(teststim1(1,:)) & ~isnan(teststim2(1,:)));
    tt=find(~isnan(teststim1(1,:)));
    xcA1=[xcov(teststim1(tarchan,tt),stim1(tarchan,tt),0,'coeff') ...
          xcov(teststim1(3-tarchan,tt),stim1(3-tarchan,tt),0,'coeff')];
    tt=find(~isnan(teststim2(1,:)));
    xcA2=[xcov(teststim2(tarchan,tt),stim2(tarchan,tt),0,'coeff') ...
          xcov(teststim2(3-tarchan,tt),stim2(3-tarchan,tt),0,'coeff')];
end




TarPre=res.baphyparms{f2}.Ref_PreStimSilence;
TarPost=0.75;
TarDur=0.35;
tprebins=round(TarPre*rasterfs);
tdurbins=round(TarDur*rasterfs);
tpostbins=round(TarPost*rasterfs);
tbins=tprebins+tprebins;
c_smooth=2.5;
toff=0;

sfigure(1);
tt=(-tprebins:(tprebins-1))./rasterfs;
g1=find(~isnan(res.toutcome{f1}(:,1)) & res.toutcome{f1}(:,1)<=2);
g2=find(~isnan(res.toutcome{f2}(:,1)) & res.toutcome{f2}(:,1)<=2);
%ncount=[sum(~isnan(res.tresp{f1}(tprebins,:,1)));
%        sum(~isnan(res.tresp{f2}(tprebins,:,1)))];
ncount=[length(g1) length(g2)];
if isempty(g1) || isempty(g2),
   return
end

mr1=nanmean(nanmean(gsmooth(res.rresp{f1}((tprebins+tdurbins)+(1:tdurbins),:),c_smooth).*rasterfs));
mr2=nanmean(nanmean(gsmooth(res.rresp{f2}((tprebins+tdurbins)+(1:tdurbins),:),c_smooth).*rasterfs));
ms1=nanmean(nanmean(gsmooth(res.rresp{f1}((1:tprebins),:),c_smooth).*rasterfs));
ms2=nanmean(nanmean(gsmooth(res.rresp{f2}((1:tprebins),:),c_smooth).*rasterfs));

subplot(2,2,1);
sing_trial_resp=gsmooth(res.tresp{f1}(1:tbins,g1,1),c_smooth).*rasterfs;
rr=round(linspace(1,length(g1),min(length(g2),length(g1))));
%plot(tt,nanmean(gsmooth(res.tresp{f1}(1:tbins,g1,1),c_smooth),2));
plot(tt,sing_trial_resp(:,rr));
hold on
plot(tt([1 end]),mr1.*[1 1],'k--');
hold off
title(sprintf('attend bf (n=%d)',ncount(1)));
a1=axis;

subplot(2,2,3);
sing_trial_resp=gsmooth(res.tresp{f2}(1:tbins,g2,1),c_smooth).*rasterfs;
rr=round(linspace(1,length(g2),min(length(g2),length(g1))));
%plot(tt,nanmean(gsmooth(res.tresp{f2}(1:tbins,g2,1),c_smooth),2));
plot(tt,sing_trial_resp(:,rr));
hold on
plot(tt([1 end]),mr2.*[1 1],'k--');
hold off
title(sprintf('attend away (n=%d)',ncount(2)));
a2=axis;

axis([a1(1:3) max(a1(4),a2(4))]);
subplot(2,2,1);
axis([a1(1:3) max(a1(4),a2(4))]);


xx=linspace(0,100,15)';
dprime=[0 0];

subplot(2,2,2);
refbase=gsmooth(res.rresp{f1}((tprebins+tpostbins)+(1:tdurbins),:),c_smooth).*rasterfs;
tarpre=gsmooth(res.tresp{f1}(1:tprebins,g1,1),c_smooth).*rasterfs;
tarpost=gsmooth(res.tresp{f1}((1:tdurbins)+tprebins+toff,g1,1),c_smooth).*rasterfs;
tarpre=[refbase(~isnan(refbase)); tarpre(~isnan(tarpre))];
tarpost=tarpost(~isnan(tarpost));
n1=hist(tarpre,xx);
n2=hist(tarpost,xx);
p1=n1'./sum(n1);
p2=n2'./sum(n2);
bar(xx,[p1 p2]);
hold on
plot(xx,[flipud(cumsum(flipud(p1))) flipud(cumsum(flipud(p2)))]);
hold off
axis ([-10 110 0 1]);
dprime(1)=(mean(tarpost)-mean(tarpre))./(std(tarpre)+std(tarpost));
title(sprintf('kldiv=%.3f dp=%.2f',kldiv(xx,p2+eps,p1+eps),dprime(1)));

subplot(2,2,4);
refbase=gsmooth(res.rresp{f2}((tprebins+tpostbins)+(1:tdurbins),:),c_smooth).*rasterfs;
tarpre=gsmooth(res.tresp{f2}(1:tprebins,g2,1),c_smooth).*rasterfs;
tarpost=gsmooth(res.tresp{f2}((1:tdurbins)+tprebins+toff,g2,1),c_smooth).*rasterfs;
tarpre=[refbase(~isnan(refbase)); tarpre(~isnan(tarpre))];
tarpost=tarpost(~isnan(tarpost));
n1=hist(tarpre,xx);
n2=hist(tarpost,xx);
p1=n1'./sum(n1);
p2=n2'./sum(n2);
bar(xx,[p1 p2]);
hold on
plot(xx,[flipud(cumsum(flipud(p1))) flipud(cumsum(flipud(p2)))]);
hold off
axis ([-10 110 0 1]);
dprime(2)=(mean(tarpost)-mean(tarpre))./(std(tarpre)+std(tarpost));
title(sprintf('kldiv=%.3f dp=%.2f',kldiv(xx,p2+eps,p1+eps),dprime(2)));


return





return
%[stim,stimparam]=loadstimbytrial(parmfile,options);

options=struct();
options.rasterfs=rasterfs;
options.unit=singledata.unit;
options.channel=singledata.channum;

options.tag_masks={'Target'};
options.includeincorrect=1;
options.includeprestim=[0.5 0.5];
spkfile=res.resppaths{f1};
[tar1]=loadspikeraster(spkfile,options);

spkfile=res.resppaths{f2};
[tar2]=loadspikeraster(spkfile,options);

options.tag_masks={'Reference'};
options.includeincorrect=0;
options.includeprestim=[0.5 0.5];
spkfile=res.resppaths{f1};
[ref1]=loadspikeraster(spkfile,options);

spkfile=res.resppaths{f2};
[ref2]=loadspikeraster(spkfile,options);

totalbins=size(tar1,1);
taridx=2;
tarmean=[nanmean(tar1(:,:,taridx),2) nanmean(tar2(:,:,taridx),2)];

refmean=[nanmean(nanmean(ref1(1:totalbins,:),2),3) ...
         nanmean(nanmean(ref2(1:totalbins,:),2),3)]

figure;
subplot(2,2,1);
plot([refmean(:,1) tarmean(:,1)]);
subplot(2,2,2);
plot([refmean(:,2) tarmean(:,2)]);
subplot(2,2,3);
plot(nanmean(res.tresp{f1}(:,:,1),2));
subplot(2,2,4);
plot(nanmean(res.tresp{f2}(:,:,1),2));


figure
xx=linspace(0,100,15)';

subplot(2,1,1);
tarpre=gsmooth(tar1(1:tprebins,:,taridx),2).*rasterfs;
tarpost=gsmooth(tar1((1:tprebins)+tprebins,:,taridx),2).*rasterfs;
tarpre=tarpre(~isnan(tarpre));
tarpost=tarpost(~isnan(tarpost));
n1=hist(tarpre,xx);
n2=hist(tarpost,xx);
bar(xx,[n1'./sum(n1) n2'./sum(n2)]);
%plot([cumsum((n1')./sum(n1)) cumsum((n2')./sum(n2))]);

subplot(2,1,2);
tarpre=gsmooth(tar2(1:tprebins,:,taridx),[2]).*rasterfs;
tarpost=gsmooth(tar2((1:tardurbins)+tprebins,:,taridx),2).*rasterfs;
tarpre=tarpre(~isnan(tarpre));
tarpost=tarpost(~isnan(tarpost));
n1=hist(tarpre,xx);
n2=hist(tarpost,xx);
bar(xx,[n1'./sum(n1) n2'./sum(n2)]);
%plot([cumsum((n1')./sum(n1)) cumsum((n2')./sum(n2))]);




options=struct();
options.rasterfs=rasterfs;
options.unit=singledata.unit;
options.channel=singledata.channum;
options.includeincorrect=1;
options.includeprestim=1;
options.tag_masks={'SPECIAL-TRIAL'};

spkfile=res.resppaths{f1};
[r1,~,trialset1,exptevents1]=loadspikeraster(spkfile,options);

spkfile=res.resppaths{f2};
[r2,~,trialset2,exptevents2]=loadspikeraster(spkfile,options);


[t1,ttrial1,tNote]=evtimes(exptevents1,TargetStr,trialset1);
[t2,ttrial2,tNote]=evtimes(exptevents2,TargetStr,trialset1);

tarmean=zeros(tprebins+tpostbins+1,2);
tar1bin=round(t1*rasterfs);
tar2bin=round(t2*rasterfs);
tm=zeros(tprebins+tpostbins+1,length(t1));
for ii=1:length(t1),
    ttt=find(trialset1==ttrial1(ii));
    tm(:,ii)=r1(tar1bin(ii)+(-tprebins:tpostbins),ttt)./length(t1);
end
tarmean(:,1)=nanmean(tm,2);
tm=zeros(tprebins+tpostbins+1,length(t2));
for ii=1:length(t2),
    ttt=find(trialset2==ttrial2(ii));
    tm(:,ii)=r2(tar2bin(ii)+(-tprebins:tpostbins),ttt)./length(t2);
end
tarmean(:,2)=nanmean(tm,2);


