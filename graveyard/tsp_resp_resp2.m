function r=tsp_resp_resp2(cellid,batchid,singlecondition)
% AP_beta= A as function of P. AP_beta(2)>1 => A gain > P gain
% A1P_beta= A1 as function of P. A1P_beta(2)>1 => A1 gain > P gain
% A2P_beta= A2 as function of P. A2P_beta(2)>1 => A2 gain > P gain
% AA_beta= A2 as function of A1. AA_beta(2)>1 => A2 gain > A1 gain

if ~exist('singlecondition','var'),
   singlecondition=1;
end

rrbins=10;
rasterfs=20;
res=tsp_raster_load(cellid,batchid,rasterfs,1);

[sfiles,si]=sort(res.stimfiles);
scodes=res.filecode(si);

p=zeros(length(scodes),1);
for ii=1:length(scodes),
    if scodes{ii}(1)=='P',
        p(ii)=1;
    end
    res.rresp{ii}(:,res.routcome{ii}>2)=nan;
    res.tresp{ii}(:,res.toutcome{ii}>2)=nan;
    for dd=1:size(res.toutcome{ii},2),
       if sum(res.toutcome{ii}(:,dd)<3)<4,
          res.tresp{ii}(:,:,dd)=nan;
       end
    end
end

if strcmp(cellid,'sti018a-b1') || strcmp(cellid,'sti016a-b1'),
   sa1=max(find(strcmp(scodes,'A1')));
else
   sa1=min(find(strcmp(scodes,'A1')));
end

if ~isempty(sa1),
    spr1=max(find(p(1:sa1)));
    spo1=min(find(p(sa1:end))+sa1-1);
else
    spr1=[];spo1=[];
end
pr1=si(spr1);a1=si(sa1);po1=si(spo1);

if strcmp(cellid,'sti016a-a1'),
   sa2=max(find(strcmp(scodes,'A2')));
else
   sa2=min(find(strcmp(scodes,'A2')));
end

if ~isempty(sa2),
    spr2=max(find(p(1:sa2)));
    spo2=min(find(p(sa2:end))+sa2-1);
else
    spr2=[];spo2=[];
end
pr2=si(spr2);a2=si(sa2);po2=si(spo2);

if (length([a1 a2])<2 && ~singlecondition) ||...
      min(res.iso([a1 a2]))<80 ||... % 85,
      (~isempty(a1) && sum(sum(res.routcome{a1}==1))<5) ||...
      (~isempty(a2) && sum(sum(res.routcome{a2}==1))<5)
    disp([cellid ' missed 2 active/isolation criterion']);
    %keyboard
    r=0;
    return
end

sfigure(1);
clf
if size(res.tresp{a1},3)==2,
    tarindex=2; % high SNR target
else
    tarindex=1; % BF target
end

if ismember(batchid,[251,253,254]),
    % for effort task, make sure target is near BF
    t=dbReadTuning(cellid);
    aa=[a1 a2];
    aa=aa(1);
    lof=res.baphyparms{aa}.Ref_LowFreq(1);
    hif=res.baphyparms{aa}.Ref_HighFreq(1);
    if isfield(t,'bnblof') && t.bnblof<hif && t.bnbhif>lof
        inband=1;
    elseif isfield(t,'bf') && ~isempty(t.bf) && t.bf>lof && t.bf<hif
        inband=1;
    else
        inband=0;
    end
else
    inband=1;
end

%if isempty(pr1),pr1=po1;end
%if isempty(po1),po1=pr1;end
%if isempty(pr2),pr2=po2;end
%if isempty(po2),po2=pr2;end

if ~isempty(a1),
    prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
else
    prebins=round(res.baphyparms{a2}.Ref_PreStimSilence.*rasterfs);
end
pset=unique([pr1 po1 pr2 po2]);
if isempty(a1),
   compset=[2 4];
elseif isempty(a2),
   compset=[1 4];
else
   compset=1:4;
end

for nn={'A1P','A2P','AA','AP'},
   r.([nn{1} '_r0'])=[nan nan];
   r.([nn{1} '_r0_err'])=[nan nan];
   r.([nn{1} '_beta'])=[nan nan];
   r.([nn{1} '_beta_err'])=[nan nan];
   r.([nn{1} '_z'])=[nan nan];
end

for compidx=compset,
   if compidx==1,
      % A1 vs. P
      %set1=[pr1 po1];
      set1=[ po1 pr2 po2 pr1];
      set2=a1;
      n={'P','A1'};
      nn='A1P';
   elseif compidx==2,
       %set1=[pr2 po2];
      set1=[ po1 pr2 po2 pr1];
      set2=a2;
      n={'P','A2'};
      nn='A2P';
   elseif compidx==3,
      set1=a1;
      set2=a2;
      n={'A1','A2'};
      nn='AA';
   elseif compidx==4,
      set1=pset;
      set2=[a1 a2];
      n={'A','P'};
      nn='AP';
   end
   
   mp=size(res.rresp{set1(1)},1);
   for ii=[set1 set2],
      if mp>size(res.rresp{ii},1),
         mp=size(res.rresp{ii},1);
      end
   end
   
   mr1=[];
   mt1=[];
   
   beta=zeros(length(set1),2);
   beta_err=zeros(length(set1),2);
   z=zeros(length(set1),2);
   r0=zeros(length(set1),2);
   r0_err=zeros(length(set1),2);
   rev=zeros(length(set1),1);
   rev_err=zeros(length(set1),1);
   
   for kk=1:length(set1),
      setidx=set1(kk);
      r1=[];
      r2=[];
      t1=[];
      t2=[];
      for ii=set1,
         r1=cat(2,r1,res.rresp{ii}(1:mp,:,:).*rasterfs);
         t1=cat(2,t1,res.tresp{ii}(:,:,tarindex).*rasterfs);
      end
      for ii=set2,
         r2=cat(2,r2,res.rresp{ii}(1:mp,:,:).*rasterfs);
         t2=cat(2,t2,res.tresp{ii}(:,:,tarindex).*rasterfs);
      end
      mr1=cat(2,mr1,r1);
      mt1=cat(2,mt1,t1);
      mr2=r2;
      mt2=t2;
      if ~strcmp(nn,'AA'),
         for jj=1:size(r1,3),
            nn1=sum(~isnan(r1(prebins,:,jj)));
            nn2=sum(~isnan(r2(prebins,:,jj)));
            if nn1>nn2,
               f1=find(~isnan(r1(prebins,:,jj)));
               ll=shuffle(f1);
               ll=ll(1:(length(f1)-nn2));
               r1(:,ll,jj)=nan;
            elseif nn2>nn1,
               f2=find(~isnan(r2(prebins,:,jj)));
               ll=shuffle(f2);
               ll=ll(1:(length(f2)-nn1));
               r2(:,ll,jj)=nan;
            end
         end
      end
      
      r10=nanmean(nanmean(r1(1:prebins,:)));
      r10e=nanstd(nanmean(r1(1:prebins,:)))./sqrt(sum(~isnan(r1(1,:))));
      r1=r1-r10;
      r20=nanmean(nanmean(r2(1:prebins,:)));
      r20e=nanstd(nanmean(r2(1:prebins,:)))./sqrt(sum(~isnan(r2(1,:))));
      r2=r2-r20;
      fprintf('r0_A1=%.1f  r0_A2=%.1f  A2-A1=%.1f\n',...
         r10,r20,r20-r10);
      r0(kk,:)=[r10 r20];
      r0_err(kk,:)=[r10e r20e];
      
      subplot(2,4,4+compidx);
      hold on
      FITDC=0;
      if 1,
         [beta(kk,:),beta_err(kk,:),z(kk,:)]=resp_resp_plot(r1,r2,rrbins,[0 0 1],FITDC);
         [b1,be1,bz1]=resp_resp_plot(r1,cat(2,r1,r2),rrbins,[1 0 0],FITDC);
         hold on
         [b2,be2,bz2]=resp_resp_plot(r2,cat(2,r1,r2),rrbins,[0 0 1],FITDC);
         beta(kk,:)=[mean([b1(1) b2(1)]) b1(2)./b2(2)];
         beta_err(kk,:)=sqrt(be1.^2+be2.^2);
      else
         [b1,be1,bz1]=resp_resp_plot(r1,r2,rrbins,[1 0 0],FITDC);
         %[b2,be2,bz2]=resp_resp_plot(r2,r1,rrbins,[0 0 1],FITDC);
         %beta(kk,:)=[mean([b1(1) b1(1)]) exp((log(b1(2))-log(b2(2)))/2)];
         %beta_err(kk,:)=sqrt(be1.^2+be2.^2);
         beta(kk,:)=b1;
         beta_err(kk,:)=be1;
      end
      
      hold off
      xlabel('passive');
      ylabel('active');
      title(sprintf('%s %s (%.0f %.0f)',n{1},n{2},...
         min(res.iso(set1)),min(res.iso(set2))));
      
      re1=nanmean(r1((prebins+1):end,:,:),2);
      re2=nanmean(r2((prebins+1):end,:,:),2);
      regood=find(~isnan(re1) & ~isnan(re2));
      
      rev(kk,:)=nanmean([re2(regood)-re1(regood)]);
      reverr(kk,:)=nanstd([re2(regood)-re1(regood)])./sqrt(length(regood));
   end
   
   r.([nn '_r0'])=diff(mean(r0,1));
   r.([nn '_r0_err'])=mean(sqrt((r0_err(:,1).^2+r0_err(:,2).^2)./2),1);
   
   r.([nn '_rev'])=mean(rev,1);
   r.([nn '_rev_err'])=sqrt(mean(reverr.^2));
   
   r.([nn '_beta'])=[r.([nn '_r0']) mean(beta(:,2),1)];
   r.([nn '_beta_err'])=[r.([nn '_r0_err']) mean(beta_err(:,2),1)];
   r.([nn '_z'])=[r.([nn '_r0'])./r.([nn '_r0_err']) ...
      (mean(beta(:,2),1)-1)./mean(beta_err(:,2),1)];
   
   subplot(4,4,compidx);
   m=[nanmean(nanmean(mr1,2),3) nanmean(nanmean(mr2,2),3)];
   %m=[nanmean(nanmean(mr1,2),3)-mean(r0(:,1)) nanmean(nanmean(mr2,2),3)-mean(r0(:,2))];
   plot(m);
   axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
   title(sprintf('%s/%d %s-%s',cellid,batchid,n{:}));
   legend(n{1},n{2});
   legend boxoff
   ylabel('ref sp/sec');
   text(2,5,sprintf('dr0=%.1f',r.([nn '_r0'])));
   
   subplot(4,4,4+compidx);
   m=[nanmean(nanmean(mt1,2),3) nanmean(nanmean(mt2,2),3)];
   plot(m);
   if ~isnan(max(m(:))),
      axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
   end
   legend boxoff
   ylabel('tar sp/sec');
   xlabel('time bin');
   
   %if compidx==3,
   %   keyboard
   %end
end

r.P1P_z=[0 0];
r.P2P_z=[0 0];
r.P1P_beta=[0 1];
r.P2P_beta=[0 1];


r.iso=min(res.iso([a1 a2]));

if ~isempty(a1)
   prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
   durbins=round(res.baphyparms{a1}.Tar_Duration.*rasterfs);
else
   prebins=round(res.baphyparms{a2}.Ref_PreStimSilence.*rasterfs);
   durbins=round(res.baphyparms{a2}.Tar_Duration.*rasterfs);
end
t1=[];
for tt=[pr1 po1 pr2 po2],
    t1=cat(2,t1,res.tresp{tt}(:,:,tarindex).*rasterfs);
end
if isempty(t1),
    for tt=[a1 a2],
        t1=cat(2,t1,res.tresp{tt}(:,:,tarindex).*rasterfs);
    end
end
pretar=nanmean(nanmean(t1(1:prebins,:)));
postar=nanmean(nanmean(t1(prebins+(1:(durbins-1)),:)));

r.tar_resp=postar-pretar;
r.inband=inband;
r.cellid=cellid;
r.DImean=nanmean(res.DI(:));
r.DImin=nanmin(res.DI(:));

% compute some avg psths for different behavior conditions
%tarindex=1;
r1=nanmean(nanmean(res.rresp{a1},2),3).*rasterfs;
r2=nanmean(nanmean(res.rresp{a2},2),3).*rasterfs;
if ~isempty(a1),
   t1=nanmean(nanmean(res.tresp{a1}(:,:,tarindex),2),3).*rasterfs;
end
if ~isempty(a2),
   t2=nanmean(nanmean(res.tresp{a2}(:,:,tarindex),2),3).*rasterfs;
else
   t2=nan(size(t1));
   r2=nan(size(r1));
end
if isempty(a1),
   t1=nan(size(t2));
   r1=nan(size(r2));
end
rp=[];
tp=[];

for tt=[pr1 po1 pr2 po2],
    rp=cat(2,rp,res.rresp{tt}(:,:,:).*rasterfs);
    tp=cat(2,tp,res.tresp{tt}(:,:,tarindex).*rasterfs);
end
rp=nanmean(nanmean(rp,2),3);
tp=nanmean(nanmean(tp,2),3);
if isempty(tp),tp=ones(size(t1)).*nan; end

%prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
if prebins>round(0.5.*rasterfs),
    ss=prebins-round(0.5.*rasterfs)+1;
    padbins=0;
else
    ss=1;
    padbins=round(0.5.*rasterfs)-prebins;
end
pp=ones(padbins,3).*nan;

mm=min([length(r1) length(r2) length(rp)]);
r.rresp=[pp; r1(ss:mm) r2(ss:mm) rp(ss:mm)];
mm=min([length(t1) length(t2) length(tp)]);
r.tresp=[pp; t1(ss:mm) t2(ss:mm) tp(ss:mm)];


fprintf('A1- (%s) DI=%.0f\n',res.stimfiles{a1},res.DI(tarindex,a1));
fprintf('A2- (%s) DI=%.0f\n',res.stimfiles{a2},res.DI(tarindex,a2));


return

if 0
    subplot(4,4,1);
    m=[nanmean(nanmean(cat(2,r1,r2),2),3) nanmean(nanmean(ra,2),3)];
    plot(m);
    axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
    title(sprintf('%s/%d A1 v P',cellid,batchid));
    legend('P',res.filecode{a1});
    legend boxoff
    ylabel('ref sp/sec');
    
    subplot(4,4,5);
    m=[nanmean(nanmean(t1,2),3) nanmean(nanmean(t2,2),3)];
    plot(m);
    axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
    legend(res.filecode{pr1},res.filecode{po1});
    legend boxoff
    ylabel('tar sp/sec');
    xlabel('time bin');
    
    subplot(2,4,5);
    [r.P1P_beta,r.P1P_beta_err,r.P1P_z]=resp_resp_plot(r1a,r2a,rrbins,[0 0 1],1);
    hold on
    %resp_resp_plot(r1,r2,rrbins,[0 0 0.5]);
    hold on
    [r.A1P_beta,r.A1P_beta_err,r.A1P_z]=resp_resp_plot(cat(2,r1,r2),ra,rrbins,[1 0 0],1);
    hold off
    xlabel('P');
    ylabel(res.filecode{a1});
    title(sprintf('%s %s %s (%.0f %.0f %.0f)',...
                  res.filecode{[pr1 a1 po1]},res.iso([pr1 a1 po1])));
end

% A2
if length([pr2 a2 po2])==3,
    r1=res.rresp{pr2}(:,1:end,:).*rasterfs;
    r2=res.rresp{po2}(:,1:end,:).*rasterfs;
    r1a=res.rresp{pr2}(:,2:end,:).*rasterfs;
    r2a=res.rresp{po2}(:,2:end,:).*rasterfs;
    for jj=1:size(r1,3),
        nn1=sum(~isnan(r1(prebins,:,jj)));
        nn2=sum(~isnan(r2(prebins,:,jj)));
        if nn1>nn2,
            f1=find(~isnan(r1(prebins,:,jj)));
            ll=shuffle(f1);
            ll=ll(1:(length(f1)-nn1));
            r1(:,ll,jj)=nan;
        elseif nn2>nn1,
            f2=find(~isnan(r2(prebins,:,jj)));
            ll=shuffle(f2);
            ll=ll(1:(length(f2)-nn2));
            r2(:,ll,jj)=nan;
        end
    end
    ra=res.rresp{a2}.*rasterfs;
    if size(ra,1)<size(r1,1),
        r1=r1(1:size(ra,1),:,:);
        r2=r2(1:size(ra,1),:,:);
        r1a=r1a(1:size(ra,1),:,:);
        r2a=r2a(1:size(ra,1),:,:);
    end
    t1=cat(2,res.tresp{pr2}(:,:,tarindex),...
           res.tresp{po2}(:,:,tarindex)).*rasterfs;
    t2=res.tresp{a2}(:,:,tarindex).*rasterfs;
    
    subplot(4,4,2);
    m=[nanmean(nanmean(cat(2,r1,r2),2),3) nanmean(nanmean(ra,2),3)];
    plot(m);
    axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
    title(sprintf('%s/%d A2 vs P',cellid,batchid));
    legend('P',res.filecode{a2});
    legend boxoff
    ylabel('ref sp/sec');
   
    subplot(4,4,6);
    m=[nanmean(nanmean(t1,2),3) nanmean(nanmean(t2,2),3)];
    plot(m);
    axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
    title(sprintf('%s/%d A2 tar',cellid,batchid));
    legend(res.filecode{pr2},res.filecode{po2});
    legend boxoff
    ylabel('tar sp/sec');
    xlabel('time bin');
    
    subplot(2,4,6);
    [r.P2P_beta,r.P2P_beta_err,r.P2P_z]=resp_resp_plot(r1a,r2a,rrbins,[0 0 1],1);
    hold on
    %resp_resp_plot(r1,r2,rrbins,[0 0 0.5]);
    hold on
    [r.A2P_beta,r.A2P_beta_err,r.A2P_z]=resp_resp_plot(cat(2,r1,r2),ra,rrbins,[1 0 0],1);
    hold off
    xlabel('P');
    ylabel(res.filecode{a1});
    title(sprintf('%s %s %s (%.0f %.0f %.0f)',...
                  res.filecode{[pr2 a2 po2]},res.iso([pr2 a2 po2])));
end

if length([a1 a2])==2,
    r1=res.rresp{a1}.*rasterfs;
    r2=res.rresp{a2}.*rasterfs;
    if size(r1,1)<size(r2,1),
        r2=r2(1:size(r1,1),:,:);
    elseif size(r1,1)>size(r2,1)
        r1=r1(1:size(r2,1),:,:);
    end
    t1=res.tresp{a1}(:,:,tarindex).*rasterfs;
    t2=res.tresp{a2}(:,:,tarindex).*rasterfs;
    
    subplot(4,4,3);
    %m=[nanmean(nanmean(t1,2),3) nanmean(nanmean(t2,2),3)];
    m=[nanmean(nanmean(r1,2),3) nanmean(nanmean(r2,2),3)];
    plot(m);
    axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
    title(sprintf('%s/%d A2-A1',cellid,batchid));
    legend(res.filecode{a1},res.filecode{a2});
    legend boxoff
    ylabel('ref sp/sec');
    
    prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
%     for jj=1:size(r1,3),
%         nn1=sum(~isnan(r1(prebins,:,jj)));
%         nn2=sum(~isnan(r2(prebins,:,jj)));
%         if nn1>nn2,
%             f1=find(~isnan(r1(prebins,:,jj)));
%             ll=shuffle(f1);
%             ll=ll(1:(length(f1)-nn1));
%             r1(:,ll,jj)=nan;
%         elseif nn2>nn1,
%             f2=find(~isnan(r2(prebins,:,jj)));
%             ll=shuffle(f2);
%             ll=ll(1:(length(f2)-nn2));
%             r2(:,ll,jj)=nan;
%         end
%     end
    r10=nanmean(nanmean(r1(1:prebins,:)));
    r10e=nanstd(nanmean(r1(1:prebins,:)))./sqrt(sum(~isnan(r1(1,:))));
    r1=r1-r10;
    r20=nanmean(nanmean(r2(1:prebins,:)));
    r20e=nanstd(nanmean(r2(1:prebins,:)))./sqrt(sum(~isnan(r2(1,:))));
    r2=r2-r20;
    fprintf('r0_A1=%.1f  r0_A2=%.1f  A2-A1=%.1f\n',...
            r10,r20,r20-r10);
    r.AA_r0=r20-r10;
    r.AA_r0_err=sqrt(r10e.^2+r20e.^2);
    text(2,5,sprintf('dr0=%.1f',r.AA_r0));
    
    subplot(4,4,7);
    m=[nanmean(nanmean(t1,2),3) nanmean(nanmean(t2,2),3)];
    plot(m);
    axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
    legend(res.filecode{a1},res.filecode{a2});
    legend boxoff
    ylabel('tar sp/sec');
    xlabel('time bin');
    
    subplot(2,4,7);
    [r.AA_beta,r.AA_beta_err,r.AA_z]=resp_resp_plot(r1,r2,rrbins,[1 0 0],0);
    xlabel(res.filecode{a1});
    ylabel(res.filecode{a2});
    title(sprintf('%s %s (%.0f %.0f)',...
                  res.filecode{[a1 a2]},res.iso([a1 a2])));
    fprintf('dc,g=(%.2f,%.2f) +- (%.2f,%.2f)\n',r.AA_beta,r.AA_beta_err);
    
    r.AA_beta(1)=r.AA_r0;
    r.AA_beta_err(1)=r.AA_r0_err;
    r.AA_z(1)= r.AA_beta(1)./r.AA_beta_err(1);
    
end

pas=si(find(p));
act=si(find(~p));
mp=size(res.rresp{1},1);
r1=[];
r2=[];
t1=[];
t2=[];

for ii=1:length(p),
    if mp>size(res.rresp{ii},1),
        mp=size(res.rresp{ii},1);
    end
end
for ii=1:length(p)
    if p(ii),
        r1=cat(2,r1,res.rresp{ii}(1:mp,:,:).*rasterfs);
        t1=cat(2,t1,res.tresp{ii}(:,:,tarindex).*rasterfs);
    else
        r2=cat(2,r2,res.rresp{ii}(1:mp,:,:).*rasterfs);
        t2=cat(2,t2,res.tresp{ii}(:,:,tarindex).*rasterfs);
    end
end

subplot(4,4,4);
m=[nanmean(nanmean(r1,2),3) nanmean(nanmean(r2,2),3)];
plot(m);
axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
title(sprintf('%s/%d A-P',cellid,batchid));
legend('P','A');
legend boxoff
ylabel('ref sp/sec');

prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
for jj=1:size(r1,3),
   nnc=sum(~isnan(r2(prebins,:,jj)));
   f1=find(~isnan(r1(prebins,:,jj)));
   ll=shuffle(f1);
   ll=ll(1:(length(f1)-nnc));
   r1(:,ll,jj)=nan;
end
r10=nanmean(nanmean(r1(1:prebins,:)));
r10e=nanstd(nanmean(r1(1:prebins,:)))./sqrt(sum(~isnan(r1(1,:))));
r1=r1-r10;
r20=nanmean(nanmean(r2(1:prebins,:)));
r20e=nanstd(nanmean(r2(1:prebins,:)))./sqrt(sum(~isnan(r2(1,:))));
r2=r2-r20;
fprintf('r0_A1=%.1f  r0_A2=%.1f  A2-A1=%.1f\n',...
        r10,r20,r20-r10);
r.AP_r0=r20-r10;
r.AP_r0_err=sqrt(r10e.^2+r20e.^2);

text(2,5,sprintf('dr0=%.1f',r.AP_r0));

subplot(4,4,8);
m=[nanmean(nanmean(t1,2),3) nanmean(nanmean(t2,2),3)];
plot(m);
axis([0 length(m) 0 ceil(max(m(:))./10)*10]);
legend('P','A');
legend boxoff
ylabel('tar sp/sec');
xlabel('time bin');


subplot(2,4,8);
[r.AP_beta,r.AP_beta_err,r.AP_z]=resp_resp_plot(r1,r2,rrbins,[0 0 1],0);
xlabel('passive');
ylabel('active');
title(sprintf('A P'));

r.AP_beta(1)=r.AP_r0;
r.AP_beta_err(1)=r.AP_r0_err;
r.AP_z(1)= r.AP_beta(1)./r.AA_beta_err(1);

fprintf('A1- (%s) DI=%.0f\n',res.stimfiles{a1},res.DI(tarindex,a1));
fprintf('A2- (%s) DI=%.0f\n',res.stimfiles{a2},res.DI(tarindex,a2));

r.iso=min(res.iso([a1 a2]));

prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
durbins=round(res.baphyparms{a1}.Tar_Duration.*rasterfs);
t1=[];
for tt=[pr1 po1 pr2 po2],
    t1=cat(2,t1,res.tresp{tt}(:,:,tarindex).*rasterfs);
end
if isempty(t1),
    for tt=[a1 a2],
        t1=cat(2,t1,res.tresp{tt}(:,:,tarindex).*rasterfs);
    end
end
pretar=nanmean(nanmean(t1(1:prebins,:)));
postar=nanmean(nanmean(t1(prebins+(1:(durbins-1)),:)));

r.tar_resp=postar-pretar;
r.inband=inband;
r.cellid=cellid;
r.DImean=nanmean(res.DI(:));
r.DImin=nanmin(res.DI(:));

% compute some avg psths for different behavior conditions
tarindex=1;
r1=nanmean(nanmean(res.rresp{a1},2),3).*rasterfs;
r2=nanmean(nanmean(res.rresp{a2},2),3).*rasterfs;
t1=nanmean(nanmean(res.tresp{a1}(:,:,tarindex),2),3).*rasterfs;
t2=nanmean(nanmean(res.tresp{a2}(:,:,tarindex),2),3).*rasterfs;
rp=[];
tp=[];

for tt=[pr1 po1 pr2 po2],
    rp=cat(2,rp,res.rresp{tt}(:,:,:).*rasterfs);
    tp=cat(2,tp,res.tresp{tt}(:,:,tarindex).*rasterfs);
end
rp=nanmean(nanmean(rp,2),3);
tp=nanmean(nanmean(tp,2),3);
if isempty(tp),tp=ones(size(t1)).*nan; end

prebins=round(res.baphyparms{a1}.Ref_PreStimSilence.*rasterfs);
if prebins>round(0.5.*rasterfs),
    ss=prebins-round(0.5.*rasterfs)+1;
    padbins=0;
else
    ss=1;
    padbins=round(0.5.*rasterfs)-prebins;
end
pp=ones(padbins,3).*nan;

mm=min([length(r1) length(r2) length(rp)]);
r.rresp=[pp; r1(ss:mm) r2(ss:mm) rp(ss:mm)];
mm=min([length(t1) length(t2) length(tp)]);
r.tresp=[pp; t1(ss:mm) t2(ss:mm) tp(ss:mm)];
