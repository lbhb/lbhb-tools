function [DI, Bias, details]=behavior_per_token(parmfile)
% function [DI, Bias, details]=behavior_per_token(parmfile)
%
% calculate behavioral performance on a per-token basis using "traditional"
% performance metrics
%
% DI = (1+HR-FAR)/2;  % area under the ROC
% Bias = (HR+FAR)/2
%


LoadMFile(parmfile);


trialparms=exptparams.TrialObject;
trialcount=globalparams.rawfilecount;
perf=exptparams.Performance(1:globalparams.rawfilecount);

% "strict" - FA is response to any possible target slot preceeding the target
TarPreStimSilence=exptparams.TrialObject.TargetHandle.PreStimSilence;
if trialparms.SingleRefSegmentLen>0,
   RefSegLen=trialparms.SingleRefSegmentLen;
   PossibleRefTimes=(find(trialparms.ReferenceCountFreq(:))-1).*...
      trialparms.SingleRefSegmentLen+perf(1).FirstRefTime;
else
    RefSegLen=trialparms.ReferenceHandle.PreStimSilence+...
              trialparms.ReferenceHandle.Duration+...
              trialparms.ReferenceHandle.PostStimSilence;
    PossibleRefTimes=(find(trialparms.ReferenceCountFreq(:))-1).*...
        RefSegLen+TarPreStimSilence;
end

TarWindowStart=exptparams.BehaveObject.EarlyWindow;
if exptparams.BehaveObject.ResponseWindow>RefSegLen,
   TarWindowStop=TarWindowStart+RefSegLen;
else
   TarWindowStop=TarWindowStart+exptparams.BehaveObject.ResponseWindow;
end

trialtargetid=zeros(globalparams.rawfilecount,1);
if isfield(exptparams,'UniqueTargets') && length(exptparams.UniqueTargets)>1,
    UniqueCount=length(exptparams.UniqueTargets);
    for tt=1:trialcount,
        if ~perf(tt).NullTrial,
            trialtargetid(tt)=find(strcmp(perf(tt).ThisTargetNote,...
                                          exptparams.UniqueTargets),1);
        end
    end
else
    UniqueCount=1;
    trialtargetid=ones(size(perf));
end

[~,LightTrials] = evtimes(exptevents,'*+Light');
LightTrials=unique(LightTrials(LightTrials<trialcount));

optotrial=zeros(trialcount,1);
optotrial(LightTrials)=1;

resptime=[];
resptimeperfect=[];
stimtype=[];
stimtime=[];
tcounter=[];
optostim=[];

% exclude misses at very beginning and end
Misses=cat(1,perf.Miss);
t1=find(~Misses, 1 );
t2=find(~Misses, 1, 'last' );
if isempty(t1),t1=1;t2=1;end
for tt=t1:t2,
   RefCount=sum(PossibleRefTimes<perf(tt).FirstTarTime);
   stimtime=cat(1,stimtime,PossibleRefTimes(1:RefCount),...
      perf(tt).FirstTarTime);
   resptime=cat(1,resptime,ones(RefCount+1,1).*perf(tt).FirstLickTime);
   stimtype=cat(1,stimtype,zeros(RefCount,1),1);
   tcounter=cat(1,tcounter,ones(RefCount+1,1).*trialtargetid(tt));
   optostim=cat(1,optostim,ones(RefCount+1,1).*optotrial(tt));
end

resptime(resptime==0)=Inf;

NoLick=resptime>stimtime+TarWindowStop;
Lick=(resptime>=stimtime+TarWindowStart & resptime<stimtime+TarWindowStop);
ValidStim=resptime>=stimtime+TarWindowStart;

FA=Lick & ValidStim & stimtype==0;
CR=NoLick & ValidStim & stimtype==0;
Hit=Lick & ValidStim & stimtype==1;
Miss=NoLick & ValidStim & stimtype==1;
details=struct('Hits',sum(Hit),'Misses',sum(Miss),...
   'FAs',sum(FA),'CRs',sum(CR),'trialcount',trialcount);
FAR=sum(FA)./(sum(FA)+sum(CR));
HR=sum(Hit)./(sum(Hit)+sum(Miss));
DI=(1+HR-FAR)/2;
Bias= (HR+FAR)/2;



uFA=[sum(Lick & ValidStim & stimtype==0)...
   sum(Lick & ValidStim & stimtype==0)];
uCR=[sum(NoLick & ValidStim & stimtype==0)...
   sum(NoLick & ValidStim & stimtype==0)];
uHit=[sum(Lick & ValidStim & stimtype==1 & tcounter==1) ...
   sum(Lick & ValidStim & stimtype==1 & tcounter==2)];
uMiss=[sum(NoLick & ValidStim & stimtype==1 & tcounter==1)...
   sum(NoLick & ValidStim & stimtype==1 & tcounter==2)];
uFAR=uFA./(uFA+uCR);
uHR=uHit./(uHit+uMiss);
uDI=(1+uHR-uFAR)./2;
uBias=(uHR+uFAR)./2;
details.uFAR=uFAR;
details.uHR=uHR;
details.uDI=uDI;
details.uBias=uBias;
details.uRefN=sum(ValidStim & stimtype==0) .* [1 1];
details.uTarN=[sum(ValidStim & stimtype==1 & tcounter==1) ...
   sum(ValidStim & stimtype==1 & tcounter==2)];
details.uSNR=exptparams.TrialObject.RelativeTarRefdB;




