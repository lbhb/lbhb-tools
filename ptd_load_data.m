narf_set_path
close all
drawnow;
dbopen;

% SET THE FOLLOWING VARIABLES ACCORDING TO WHAT KIND OF ANALYSIS YOU WANT
% TO RUN

% set to 0 if you don't want to include behavior
including_behavior = 0;

% want to save the figures? 0 no saving, 1 saving
savefigures=0;
makenewfigs=1;

% choose the animal to analyze
%animal='Baby+Bear'
% animal='Babybell'
% animal='Beartooth'
% animal='Button' 
% animal='Magic'
 animal='Slee' %:  both Magic and Button
% animal='Tartufo' % AC PTD data from array

% chose what data set to look at 
dataset='IC_all_data_multiple_passives'; % this data struct contains triplets of prep/act/postp
% dataset='IC_bbl_only'; % bbl IC only multiple passives
% dataset='IC_BRT_only'; % BRT IC only multiple passives
% dataset='AC'; % Tartufo AC array data

% PUPIL_ONLY=1 means exclude all data w/o pupil for gain analysis
% and in gain analysis, "normalize" pupil by excluding trials with
% pupil < mean-2*std of active pupil distribution
PUPIL_ONLY=0;

% Look at prepassive vs active
% Set to 0 if prepassive vs active - remember to set including_behavior =1
% Set to 1 if postpassive vs active - remember to set including_behavior =1 
% Set to 2 if prepassive vs postpassive
comparison = 0; 

%%

% analyze Babybell ond Beartooth data combined with data_structure with
% multiple passives
if strcmpi(animal, 'Baby+Bear') && strcmpi(dataset, 'IC_all_data_multiple_passives')
    
    generate_IC_TIN_db_all_multiples; % data struct with multiple passives
    
    cellids={ICdata.cellid};
    
    cellidsNFix = {'bbl020c-a1','bbl020c-a3','bbl021h-a1','bbl022g-a1',...
        'bbl023c-a1','bbl023c-a2','bbl034e-a1','bbl036e-a1', 'bbl071d-a1'};
    
    rawids=cell(size(cellids));
    
    for ii=1:length(ICdata)
        if comparison == 0
            if ~isempty(ICdata(ii).prepassive_rawid) 
                rawids{ii}=[ICdata(ii).prepassive_rawid ICdata(ii).active_rawid];
            end 
        elseif comparison == 1
            if ~isempty(ICdata(ii).postpassive_rawid)
                rawids{ii}=[ICdata(ii).postpassive_rawid ICdata(ii).active_rawid];
            end
        elseif comparison == 2
            if ~isempty(ICdata(ii).postpassive_rawid)
                rawids{ii}=[ICdata(ii).prepassive_rawid ICdata(ii).postpassive_rawid];
            end
        end
    end
    
% analyze Babybell IC data only with data_struct with multiple passives
elseif strcmpi(animal, 'Babybell') && strcmpi(dataset, 'IC_bbl_only')
    
    generate_IC_TIN_db_all_bblonly; % bbl IC structure
    
    cellids={ICdata.cellid};
    
    cellidsNFix = {'bbl020c-a1','bbl020c-a3','bbl021h-a1','bbl022g-a1',...
        'bbl023c-a1','bbl023c-a2','bbl036e-a1', 'bbl071d-a1'};
    
    rawids=cell(size(cellids));  
    
    for ii=1:length(ICdata)
        if comparison == 0
            if ~isempty(ICdata(ii).prepassive_rawid) 
                rawids{ii}=[ICdata(ii).prepassive_rawid ICdata(ii).active_rawid];
            end 
        elseif comparison == 1
            if ~isempty(ICdata(ii).postpassive_rawid)
                rawids{ii}=[ICdata(ii).postpassive_rawid ICdata(ii).active_rawid];
            end
        elseif comparison == 2
            if ~isempty(ICdata(ii).postpassive_rawid)
                rawids{ii}=[ICdata(ii).prepassive_rawid ICdata(ii).postpassive_rawid];
            end
        end
    end

% analyze Beartooth IC data only with data_struct with multiple passives
elseif strcmpi(animal, 'Beartooth') && strcmpi(dataset, 'IC_BRT_only')
    
    generate_IC_TIN_db_all_BRTonly; % BRT IC structure
    
    cellids={ICdata.cellid};
    
    rawids=cell(size(cellids));
    
    for ii=1:length(ICdata)
        if comparison == 0
            if ~isempty(ICdata(ii).prepassive_rawid) 
                rawids{ii}=[ICdata(ii).prepassive_rawid ICdata(ii).active_rawid];
            end 
        elseif comparison == 1
            if ~isempty(ICdata(ii).postpassive_rawid)
                rawids{ii}=[ICdata(ii).postpassive_rawid ICdata(ii).active_rawid];
            end
        elseif comparison == 2
            if ~isempty(ICdata(ii).postpassive_rawid)
                rawids{ii}=[ICdata(ii).prepassive_rawid ICdata(ii).postpassive_rawid];
            end
        end
    end
    
    
% analize Tartufo AC data        
elseif strcmpi(animal,'Tartufo') && strcmpi(dataset, 'AC')
    
    generate_AC_TIN_db; % bbl IC structure
    
    % bbl036e-a1 has fixed torc in bbl036e08 and bbl036e09
    cellids={ACdata.cellid};
    
    rawids=cell(size(cellids));
    for ii=1:length(ACdata)
        rawids{ii}=[ACdata(ii).passive_rawid ACdata(ii).active_rawid];
    end
    
% Analyze Sean's animals for comparison
elseif strcmpi(animal,'Button') || strcmpi(animal,'Magic') || ...
        strcmpi(animal,'Slee')
    metapath='/auto/users/svd/data/slee/Aim7Analysis/IC_DataBase3';
    cellids=cell(1,95);
    rawids=cell(1,95);
    ICdata=struct();
    for jj=1:95
        
        %----------------------------
        %load analysis file
        fname = sprintf('%s/IC_Data_File_%d',metapath,jj);
        load(fname);
        siteid=ICDataFile.datID(1:min(findstr(ICDataFile.datID,'Ch')-1));
        channel=char(str2num(ICDataFile.datID(end-2))-1+'a');
        unit=(ICDataFile.datID(end));
        
        cellids{jj}=[siteid '-' channel unit];
        if strcmpi(cellids{jj},'btn25b-a1'), cellids{jj}='btn025b-a1'; end
        if strcmpi(cellids{jj},'btn26a-a1'), cellids{jj}='btn026a-a1'; end
        if strcmpi(cellids{jj},'btn33a-a1'), cellids{jj}='btn033a-a1'; end
        if strcmpi(cellids{jj},'btn33b-a1'), cellids{jj}='btn033b-a1'; end
        if strcmpi(cellids{jj},'btn33c-a1'), cellids{jj}='btn033c-a1'; end
        if strcmpi(cellids{jj},'btn0111b-b1'), cellids{jj}='btn111b-b1'; end
        
        sql=['SELECT * FROM gDataRaw',...
            ' WHERE parmfile like "',ICDataFile.PreFileName,'%"'];
        rawdata_pas=mysql(sql);
        sql=['SELECT * FROM gDataRaw',...
            ' WHERE parmfile like "',ICDataFile.OnBFTarFileName,'%"'];
        rawdata_act=mysql(sql);
        rawids{jj}=[rawdata_pas.id rawdata_act.id];
        
        if ICDataFile.IsICC
            area='ICc';
        else
            area='ICx';
        end
        ICdata(jj).area=area;
        ICdata(jj).on_BF=1;
        %       dbsetarea(siteid,area);
    end
    
    jj=find(strcmpi(cellids,'btn079d-b2') | strcmpi(cellids,'btn118-b1'));
    cellids=cellids(setdiff(1:95,jj));
    rawids=rawids(setdiff(1:95,jj));
    
    cellidsNFix = cellids;

end

if PUPIL_ONLY
   %pupil_cellids={'bbl071d-a1','bbl071d-a2','bbl071d-a3','bbl074g-a1',...
   %   'bbl078k-a1','bbl081d-a1','bbl081d-a2','BRT005c-a1',...
   %   'BRT006d-a1','BRT006d-a2','BRT007c-a1','BRT007c-a3','BRT009a-a1'};
   %jj=ismember(cellids,pupil_cellids);
   jj=[];
   for ii=1:length(rawids)
      sql=['SELECT sum(not(isnull(eyewin)) and eyewin=2) AS pupil_count FROM gDataRaw WHERE id in (',...
         num2str(rawids{ii}(1)),',',num2str(rawids{ii}(2)),')'];
      rd=mysql(sql);
      if str2num(rd.pupil_count)==2
         jj=cat(1,jj,ii);
      end
   end
   
   cellids=cellids(jj);
   rawids=rawids(jj);
   ICdata=ICdata(jj);
end

% Remove cells with empty raws ideas (only for multiple passive data
% struct)
  jj=[];
   for ii=1:length(rawids)
      if ~isempty(rawids{ii})
         jj=cat(1,jj,ii);
      end
   end
   
   cellids=cellids(jj);
   rawids=rawids(jj);
   ICdata=ICdata(jj);

% Special case to analyse Sean's data for two ferrets
if strcmpi(animal,'Slee')
    results_beha=varSNR_behavior('Magic');
    r2=varSNR_behavior('Button');
    ff=fields(results_beha);
    for ii=1:length(ff)
        results_beha.(ff{ii})=cat(1,results_beha.(ff{ii}),r2.(ff{ii}));
    end
    
elseif strcmpi(animal, 'Baby+Bear')
    results_beha=varSNR_behavior('Babybell');
    r2=varSNR_behavior('Beartooth');
    ff=fields(results_beha);
    for ii=1:length(ff)
        results_beha.(ff{ii})=cat(1,results_beha.(ff{ii}),r2.(ff{ii}));
    end
else
    results_beha=varSNR_behavior(animal, including_behavior);
end

if savefigures
    set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
    saveas(gcf, ['/auto/users/daniela/docs/AC_IC_project/eps_files_PTD/comb_beha_ephy_saved_figures/'...
        dataset '_behavior_analysis.pdf']);
    close(gcf);
end

%% load gain data

% meta data
results_beha.passive_rawid = nan(size(results_beha.rawid));
results_beha.on_BF = nan(size(results_beha.rawid));
%results_beha.passive_position = nan(size(results_beha.rawid));
results_beha.ICc = nan(size(results_beha.rawid));
results_beha.cellid = cell(size(results_beha.rawid));
results_beha.area = cell(size(results_beha.rawid));
results_beha.pupil = nan(size(results_beha.rawid));
results_beha.comparison = nan(size(results_beha.rawid));

% gain results
results_beha.gain = nan(length(results_beha.rawid),3);
results_beha.gainZ = nan(length(results_beha.rawid),3);
results_beha.spont_active = nan(size(results_beha.rawid));
results_beha.spont_passive = nan(size(results_beha.rawid));
results_beha.spontZ = nan(length(results_beha.rawid),3);
results_beha.targain = nan(length(results_beha.rawid),6);
results_beha.targainZ = nan(length(results_beha.rawid),6);
results_beha.singleid = nan(size(results_beha.rawid));
results_beha.tardist = nan(size(results_beha.rawid));
results_beha.tardiff = nan(size(results_beha.rawid));
results_beha.ref_vs_tar_pass = nan(size(results_beha.rawid,1),2);
results_beha.ref_vs_tar_act = nan(size(results_beha.rawid,1),2);
results_beha.ref_all_pass = nan(size(results_beha.rawid));
results_beha.ref_all_act = nan(size(results_beha.rawid));

%strf results
results_beha.strfc = cell(size(results_beha.rawid));
results_beha.strfsnr = nan(size(results_beha.rawid,1),2);
results_beha.strfgain = nan(size(results_beha.rawid,1),1);


for ii=1:length(rawids)
    
    % WHY DO WE DEFINE options struct here if we redefine it in
    % ptd_gain_comp??? WHY is firstpassiveonly=2 and then it's set to 1?
    
    %     options=struct('firstpassiveonly',2,'runclass','PTD,TOR','psthfs',50,'binsize',50,...
    %         'gainAnalysis','Traditional');
    
    % figurepath changes depending on what dataset is run in prev cell
    
    options=struct('firstpassiveonly',2, ...
                   'runclass', 'PTD,TOR', ...
                   'psthfs', 50, ...
                   'binsize', 2, ...
                   'figurepath', '/auto/users/daniela/docs/AC_IC_project/eps_files_PTD/comb_beha_ephy_saved_figures/');
               
    if strcmpi(dataset, 'AC')
        options.figurepath = strcat(options.figurepath, 'AC_data_Tartufo/');
    
    elseif strcmpi(dataset, 'IC_all_data_multiple_passives')
        options.figurepath = strcat(options.figurepath, 'IC_data_all/');
        
    elseif strcmpi(dataset, 'IC_bbl_only')
        options.figurepath = strcat(options.figurepath, 'IC_data_bblonly/');
        
    elseif strcmpi(dataset, 'IC_BRT_only')
        options.figurepath = strcat(options.figurepath, 'IC_data_BRTonly/');
    
    elseif strcmpi(dataset, 'IC_all_data_multiple_passives') && PUPIL_ONLY==1
        options.figurepath = strcat(options.figurepath, 'IC_data_all_pupil/');
        
%     else
%         options.figurepath = strcat(options.figurepath, 'IC_data_all/');
    end
    
    if ~isempty(rawids{ii})
        options.rawid=rawids{ii};
    end
    
    options.savefigures=savefigures;
    options.makenewfigs=makenewfigs;
    if PUPIL_ONLY
       % set pupil_min=1 to allow only trials when pupil is > mean-2*std of
       % active
       options.pupil_min=1;
    end
    
    % compute gain for a single cell:
    drawnow;
    
    results_gain=ptd_gain_comp(cellids{ii},options);
    
    if ~ismember(cellids{ii},cellidsNFix)
        toptions=options;
        toptions.datause='Target Only';
        results_tar=ptd_gain_comp(cellids{ii},toptions);
    end
    
    for idx = 1:length(results_gain.rawid)
        
        jj = min(find(results_beha.rawid == results_gain.rawid(idx), 1));
        
        if ~isnan(results_beha.gain(jj))
           % not the first time this rawid has appeared (>1 cell at site)
           ff=fields(results_beha);
           for kk=1:length(ff)
              results_beha.(ff{kk})=cat(1,results_beha.(ff{kk}),...
                 results_beha.(ff{kk})(jj,:));
           end
           jj=length(results_beha.rawid);
        end
        if ~isempty(jj)
           % save ephys results
           results_beha.cellid{jj}=cellids{ii};
           results_beha.area{jj}=results_gain.area{idx};
           results_beha.gain(jj,:) = results_gain.gain(idx,:);
           results_beha.gainZ(jj,:) = results_gain.gainZ(idx,:);
           results_beha.spont_active(jj) = results_gain.spont_active(idx);
           results_beha.spont_passive(jj) = results_gain.spont_passive(idx);
           results_beha.spontZ(jj,:) = results_gain.spontZ(idx,:);
           if ~ismember(cellids{ii},cellidsNFix)
              results_beha.targain(jj,:) = results_tar.gain(idx,:);
              results_beha.targainZ(jj,:) = results_tar.gainZ(idx,:);
              results_beha.ref_vs_tar_pass(jj,:) = results_tar.ref_vs_tar_pass;
              results_beha.ref_vs_tar_act(jj,:) = results_tar.ref_vs_tar_act;
           end
           results_beha.ref_all_pass(jj)=results_gain.ref_all_pass;
           results_beha.ref_all_act(jj)=results_gain.ref_all_act;
           results_beha.singleid(jj) = results_gain.singleid(idx);
           results_beha.tardist(jj) = results_gain.tardist(idx);
           results_beha.tardiff(jj) = results_gain.tardiff(idx);
           results_beha.strfc{jj} = results_gain.strfc;
           results_beha.strfsnr(jj,:) = results_gain.strfsnr;
           results_beha.strfgain(jj) = results_gain.strfgain;
           
           results_beha.on_BF(jj)=ICdata(ii).on_BF;
           results_beha.pupil(jj)=ICdata(ii).pupil;
           results_beha.comparison(jj)=comparison;
          
           if strcmpi(ICdata(ii).area,'ICx')
               results_beha.ICc(jj)=0;
           else
               results_beha.ICc(jj)=1;
           end
        end
    end
end

results_beha.on_BF(isnan(results_beha.on_BF))=-1;

% save data matrix
savefile=sprintf('/auto/data/tmp/daniela/ptd_pop_data_%s_%s_pupil%d_comparison%d.mat',...
   animal,dataset, PUPIL_ONLY, comparison);

fprintf('saving to %s\n',savefile);
save(savefile,'results_beha','ICdata','animal','dataset','cellids');


