
narf_set_path
dbopen

%cfd=dbgetscellfile('runclass','SSA','area','A1');
cfd=dbgetscellfile('runclass','SSA');

r0=zeros(length(cfd),1);
ssaidx=zeros(length(cfd),2);
ssarespz=zeros(length(cfd),2);
adpidx=zeros(length(cfd),2);

figure;
h=gca;

for ii=1:length(cfd),
    parmfile=[cfd(ii).stimpath cfd(ii).stimfile];
    options=struct('rasterfs',100,'usesorted',1,...
                   'channel',cfd(ii).channum,'unit',cfd(ii).unit);
    if ~isempty(cfd(ii).goodtrials),
        options.trialrange=eval(['[' cfd(ii).goodtrials ']'])
    end
    [ssaidx(ii,:),ssarespz(ii,:),r0(ii),adpidx(ii,:)]=ssa_psth(parmfile,options,h);
    drawnow;
end

r0thresh=11;
sig=find(min(ssarespz,[],2)>3);
sig1=find(min(ssarespz,[],2)>3 & r0>r0thresh./100);
sig2=find(min(ssarespz,[],2)>3 & r0<=r0thresh./100);
nsig=find(min(ssarespz,[],2)<=3);

figure;
subplot(2,2,1);
plot([-0.5 .5],[0 0],'k--');
hold on
plot([0 0],[-.5 .5],'k--');
plot([-.5 .5],[.5 -.5],'k');
%plot(ssaidx(nsig,1),ssaidx(nsig,2),'.','Color',[0.7 0.7 0.7]);
plot(ssaidx(sig1,1),ssaidx(sig1,2),'.','Color',[0.7 0.7 0.7]);
plot(ssaidx(sig2,1),ssaidx(sig2,2),'.','Color',[0 0 0]);
hold off
axis tight square
xlabel('ssaidx 1');
ylabel('ssaidx 2');
title(sprintf('r0>%.0f(n=%d): ssa=%.3f r0<%.0f(n=%d): ssa=%.3f',...
              r0thresh,length(sig1),median(mean(ssaidx(sig1,:),2)),...
              r0thresh,length(sig2),median(mean(ssaidx(sig2,:),2)))); 


subplot(2,2,2);
plot(r0(sig).*100,mean(ssaidx(sig,:),2),'.');
hold on
plot([r0thresh r0thresh],[-0.25 0.25],'k--');
hold off
xlabel('spont rate');
ylabel('SSA idx');
axis([0 30 -0.25 0.25]);

subplot(2,2,3);
plot([-0.5 .5],[0 0],'k--');
hold on
plot([0 0],[-.5 .5],'k--');
plot([-.5 .5],[.5 -.5],'k');
%plot(ssaidx(nsig,1),ssaidx(nsig,2),'.','Color',[0.7 0.7 0.7]);
plot(adpidx(sig1,1),adpidx(sig1,2),'.','Color',[0.7 0.7 0.7]);
plot(adpidx(sig2,1),adpidx(sig2,2),'.','Color',[0 0 0]);
hold off
axis tight square
xlabel('adpidx 1');
ylabel('adpidx 2');
title(sprintf('r0>10(n=%d): adp=%.3f r0<10(n=%d): adp=%.3f',...
              length(sig1),median(mean(adpidx(sig1,:),2)),...
              length(sig2),median(mean(adpidx(sig2,:),2)))); 

meanssa=mean(ssaidx,2);
meanadp=mean(adpidx,2);

subplot(2,2,4);
plot([-0.5 .5],[0 0],'k--');
hold on
plot([0 0],[-.5 .5],'k--');
plot([-.5 .5],[.5 -.5],'k');
plot(meanadp(sig1),meanssa(sig1),'.','Color',[0.7 0.7 0.7]);
plot(meanadp(sig2),meanssa(sig2),'.','Color',[0 0 0]);
hold off
axis tight square
xlabel('adpidx');
ylabel('ssaidx');
