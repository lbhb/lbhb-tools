% IC data structure bbl IC RH
% modified from generate IC_TIN_db_all_multiples.m
% Babybell IC only

ICdata=struct;
n=1;

ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl021h-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl021/sorted/';
ICdata(n).prepassive_spike='bbl021h02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120254;
ICdata(n).active_spike='bbl021h04_a_PTD.spk.mat';
ICdata(n).active_rawid=120256;
ICdata(n).postpassive_spike='bbl021h06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120260;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl021h-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl021/sorted/';
ICdata(n).prepassive_spike='bbl021h02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120254;
ICdata(n).active_spike='bbl021h04_a_PTD.spk.mat';
ICdata(n).active_rawid=120258;
ICdata(n).postpassive_spike='bbl021h06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120260;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl022g-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl022/sorted/';
ICdata(n).prepassive_spike='bbl022g04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120286;
ICdata(n).active_spike='bbl022g07_a_PTD.spk.mat';
ICdata(n).active_rawid=120289;
ICdata(n).postpassive_spike='bbl022g09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120293;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl022g-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl022/sorted/';
ICdata(n).prepassive_spike='bbl022g04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120286;
ICdata(n).active_spike='bbl022g08_a_PTD.spk.mat';
ICdata(n).active_rawid=120290;
ICdata(n).postpassive_spike='bbl022g09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120293;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl023c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(n).prepassive_spike='bbl023c02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120310;
ICdata(n).active_spike='bbl023c03_a_PTD.spk.mat';
ICdata(n).active_rawid=120311;
ICdata(n).postpassive_spike='bbl023c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120313;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl023c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(n).prepassive_spike='bbl023c02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120310;
ICdata(n).active_spike='bbl023c04_a_PTD.spk.mat';
ICdata(n).active_rawid=120312;
ICdata(n).postpassive_spike='bbl023c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120313;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-Passive (none) - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl023c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(n).active_spike='bbl023c06_a_PTD.spk.mat';
ICdata(n).active_rawid=120314;
ICdata(n).postpassive_spike='bbl023c08_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120317;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-Passive (none) - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl023c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Probe_DS';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl023/sorted/';
ICdata(n).active_spike='bbl023c07_a_PTD.spk.mat';
ICdata(n).active_rawid=120316;
ICdata(n).postpassive_spike='bbl023c08_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120317;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl027i-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl027/sorted/';
ICdata(n).prepassive_spike='bbl027i01_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120706;
ICdata(n).active_spike='bbl027i02_a_PTD.spk.mat';
ICdata(n).active_rawid=120707;
ICdata(n).postpassive_spike='bbl027i04_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120709;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl027i-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl027/sorted/';
ICdata(n).prepassive_spike='bbl027i01_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120706;
ICdata(n).active_spike='bbl027i03_a_PTD.spk.mat';
ICdata(n).active_rawid=120708;
ICdata(n).postpassive_spike='bbl027i04_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120709;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active hard - Postpassive (none)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl029f-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl029/sorted/';
ICdata(n).prepassive_spike='bbl029f03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120761;
ICdata(n).active_spike='bbl029f04_a_PTD.spk.mat';
ICdata(n).active_rawid=120764;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy - Postpassive (none)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl029f-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl029/sorted/';
ICdata(n).prepassive_spike='bbl029f03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120761;
ICdata(n).active_spike='bbl029f05_a_PTD.spk.mat';
ICdata(n).active_rawid=120767;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive (none) - Active easy 1 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl028d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(n).active_spike='bbl028d05_a_PTD.spk.mat';
ICdata(n).active_rawid=120735;
ICdata(n).postpassive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120737;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (none) - Active hard 1 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl028d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(n).active_spike='bbl028d06_a_PTD.spk.mat';
ICdata(n).active_rawid=120736;
ICdata(n).postpassive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120737;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard    

% Pre-passive (none) - Active easy 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl028d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(n).active_spike='bbl028d08_a_PTD.spk.mat';
ICdata(n).active_rawid=120738;
ICdata(n).postpassive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120737;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (none) - Active hard 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl028d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl028/sorted/';
ICdata(n).active_spike='bbl028d09_a_PTD.spk.mat';
ICdata(n).active_rawid=120739;
ICdata(n).postpassive_spike='bbl028d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120737;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy 1 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl030e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(n).prepassive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120792;
ICdata(n).active_spike='bbl030e07_a_PTD.spk.mat';
ICdata(n).active_rawid=120793;
ICdata(n).postpassive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120799;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard 1 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl030e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(n).prepassive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120792;
ICdata(n).active_spike='bbl030e08_a_PTD.spk.mat';
ICdata(n).active_rawid=120797;
ICdata(n).postpassive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120799;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl030e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(n).prepassive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120792;
ICdata(n).active_spike='bbl030e11_a_PTD.spk.mat';
ICdata(n).active_rawid=120801;
ICdata(n).postpassive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120799;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl030e-a1'; 
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl030/sorted/';
ICdata(n).prepassive_spike='bbl030e06_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120792;
ICdata(n).active_spike='bbl030e12_a_PTD.spk.mat';
ICdata(n).active_rawid=120806;
ICdata(n).postpassive_spike='bbl030e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120799;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl032f-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl032/sorted/';
ICdata(n).prepassive_spike='bbl032f02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120899;
ICdata(n).active_spike='bbl032f03_a_PTD.spk.mat';
ICdata(n).active_rawid=120901;
ICdata(n).postpassive_spike='bbl032f05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120906;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl032f-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl032/sorted/';
ICdata(n).prepassive_spike='bbl032f02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120899;
ICdata(n).active_spike='bbl032f04_a_PTD.spk.mat';
ICdata(n).active_rawid=120903;
ICdata(n).postpassive_spike='bbl032f05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120906;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive (none) - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl033c-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl033/sorted/';
ICdata(n).active_spike='bbl033c03_a_PTD.spk.mat';
ICdata(n).active_rawid=120940;
ICdata(n).postpassive_spike='bbl033c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120942;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (none) - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl033c-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl033/sorted/';
ICdata(n).postpassive_spike='bbl033c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120942;
ICdata(n).active_spike='bbl033c04_a_PTD.spk.mat';
ICdata(n).active_rawid=120941;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl034e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(n).prepassive_spike='bbl034e02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120983;
ICdata(n).active_spike='bbl034e03_a_PTD.spk.mat';
ICdata(n).active_rawid=120984;
ICdata(n).postpassive_spike='bbl034e05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120986;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl034e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(n).prepassive_spike='bbl034e02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120983;
ICdata(n).active_spike='bbl034e04_a_PTD.spk.mat';
ICdata(n).active_rawid=120985;
ICdata(n).postpassive_spike='bbl034e05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120986;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl034e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(n).prepassive_spike='bbl034e02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=120983;
ICdata(n).active_spike='bbl034e07_a_PTD.spk.mat';
ICdata(n).active_rawid=120988;
ICdata(n).postpassive_spike='bbl034e05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=120986;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive- Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl036e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl034e/sorted/';
ICdata(n).prepassive_spike='bbl036e05_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121034;
ICdata(n).active_spike='bbl036e06_a_PTD.spk.mat';
ICdata(n).active_rawid=121035;
ICdata(n).postpassive_spike='bbl036e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121038;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------
% This cell is a special case because I run on and off BF
% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl039d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(n).prepassive_spike='bbl039d02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121282;
ICdata(n).active_spike='bbl039d04_a_PTD.spk.mat';
ICdata(n).active_rawid=121284;
ICdata(n).postpassive_spike='bbl039d06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121288;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl039d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(n).prepassive_spike='bbl039d02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121282;
ICdata(n).active_spike='bbl039d05_a_PTD.spk.mat';
ICdata(n).active_rawid=121285;
ICdata(n).postpassive_spike='bbl039d06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121288;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl039d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(n).prepassive_spike='bbl039d02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121282;
ICdata(n).active_spike='bbl039d07_a_PTD.spk.mat';
ICdata(n).active_rawid=121289;
ICdata(n).postpassive_spike='bbl039d06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121288;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard 2 - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl039d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl039d/sorted/';
ICdata(n).prepassive_spike='bbl039d02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121282;
ICdata(n).active_spike='bbl039d08_a_PTD.spk.mat';
ICdata(n).active_rawid=121292;
ICdata(n).postpassive_spike='bbl039d06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121288;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

%--------------------------------------------------------------------------

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl041e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(n).prepassive_spike='bbl041e02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121417;
ICdata(n).active_spike='bbl041e03_a_PTD.spk.mat';
ICdata(n).active_rawid=121418;
ICdata(n).postpassive_spike='bbl041e05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121420;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl041e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(n).prepassive_spike='bbl041e02_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121417;
ICdata(n).active_spike='bbl041e04_a_PTD.spk.mat';
ICdata(n).active_rawid=121419;
ICdata(n).postpassive_spike='bbl041e05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121420;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (even if it occurred after another active) - Active easy
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl041e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(n).prepassive_spike='bbl041e06_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121421;
ICdata(n).active_spike='bbl041e07_a_PTD.spk.mat';
ICdata(n).active_rawid=121422;
ICdata(n).postpassive_spike='bbl041e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121424;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='2';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl041e-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl041e/sorted/';
ICdata(n).prepassive_spike='bbl041e06_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=121421;
ICdata(n).active_spike='bbl041e08_a_PTD.spk.mat';
ICdata(n).active_rawid=121423;
ICdata(n).postpassive_spike='bbl041e09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121424;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive (missing) - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl053c-a3';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
ICdata(n).active_spike='bbl053c04_a_PTD.spk.mat';
ICdata(n).active_rawid=121933;
ICdata(n).postpassive_spike='bbl053c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121934;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl053c-a3';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
ICdata(n).active_spike='bbl053c07_a_PTD.spk.mat';
ICdata(n).active_rawid=121937;
ICdata(n).postpassive_spike='bbl053c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121934;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl053c-a3';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl053c/sorted/';
ICdata(n).active_spike='bbl053c08_a_PTD.spk.mat';
ICdata(n).active_rawid=121940;
ICdata(n).postpassive_spike='bbl053c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=121934;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive (missing) - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl058c-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl058c/sorted/';
ICdata(n).active_spike='bbl058c04_a_PTD.spk.mat';
ICdata(n).active_rawid=122567;
ICdata(n).postpassive_spike='bbl058c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=122575;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl058c-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl058c/sorted/';
ICdata(n).active_spike='bbl058c06_a_PTD.spk.mat';
ICdata(n).active_rawid=122579;
ICdata(n).postpassive_spike='bbl058c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=122575;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl058c-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl058c/sorted/';
ICdata(n).active_spike='bbl058c07_a_PTD.spk.mat';
ICdata(n).active_rawid=122580;
ICdata(n).postpassive_spike='bbl058c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=122575;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive (missing) - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl060c-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl060c/sorted/';
ICdata(n).active_spike='bbl060c07_a_PTD.spk.mat';
ICdata(n).active_rawid=122710;
ICdata(n).postpassive_spike='bbl060c08_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=122712;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive (missing) - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='3';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl060c-a2';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl060c/sorted/';
ICdata(n).active_spike='bbl060c07_a_PTD.spk.mat';
ICdata(n).active_rawid=122710;
ICdata(n).postpassive_spike='bbl060c08_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=122712;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive (missing) - Active hard - Postpassive (3 units in one
% tetrode)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d06_a_PTD.spk.mat';
ICdata(n).active_rawid=125264;
ICdata(n).postpassive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125265;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active pure tone - Postpassive (3 units in one
% tetrode)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d08_a_PTD.spk.mat';
ICdata(n).active_rawid=125267;
ICdata(n).postpassive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125265;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active easy - Postpassive (3 units in one
% tetrode)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d10_a_PTD.spk.mat';
ICdata(n).active_rawid=125270;
ICdata(n).postpassive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125265;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active hard - Postpassive (3 units in one
% tetrode)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d06_a_PTD.spk.mat';
ICdata(n).active_rawid=125264;
ICdata(n).postpassive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125265;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active Pure Tone - Postpassive (3 units in one
% tetrode)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d08_a_PTD.spk.mat';
ICdata(n).active_rawid=125267;
ICdata(n).postpassive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125265;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active Easy - Postpassive (3 units in one
% tetrode)
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d10_a_PTD.spk.mat';
ICdata(n).active_rawid=125270;
ICdata(n).postpassive_spike='bbl071d07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125265;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive (missing) - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a2';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d06_a_PTD.spk.mat';
ICdata(n).active_rawid=125264;
ICdata(n).postpassive_spike='bbl071d09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125268;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a3';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d08_a_PTD.spk.mat';
ICdata(n).active_rawid=125267;
ICdata(n).postpassive_spike='bbl071d09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125268;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive (missing) - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl071d-a3';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).note='OK isolation P99 A90';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl071d/sorted/';
ICdata(n).active_spike='bbl071d10_a_PTD.spk.mat';
ICdata(n).active_rawid=125270;
ICdata(n).postpassive_spike='bbl071d09_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125268;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl074g-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
ICdata(n).prepassive_spike='bbl074g03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=125662;
ICdata(n).active_spike='bbl074g05_a_PTD.spk.mat';
ICdata(n).active_rawid=125665;
ICdata(n).postpassive_spike='bbl074g06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125667;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=p% Post-passive - Active hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl074g-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl074g/sorted/';
ICdata(n).prepassive_spike='bbl074g03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=125662;
ICdata(n).active_spike='bbl074g07_a_PTD.spk.mat';
ICdata(n).active_rawid=125668;
ICdata(n).postpassive_spike='bbl074g06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125667;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------
 
% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl078k-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(n).prepassive_spike='bbl078k03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=125886;
ICdata(n).active_spike='bbl078k04_a_PTD.spk.mat';
ICdata(n).active_rawid=125887;
ICdata(n).postpassive_spike='bbl078k06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125889;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl078k-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(n).prepassive_spike='bbl078k03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=125886;
ICdata(n).active_spike='bbl078k05_a_PTD.spk.mat';
ICdata(n).active_rawid=125888;
ICdata(n).postpassive_spike='bbl078k06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125889;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl078k-a1';
ICdata(n).area='ICc';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl078k/sorted/';
ICdata(n).prepassive_spike='bbl078k03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=125886;
ICdata(n).active_spike='bbl078k07_a_PTD.spk.mat';
ICdata(n).active_rawid=125890;
ICdata(n).postpassive_spike='bbl078k06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=125889;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl081d-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(n).prepassive_spike='bbl081d03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=126472;
ICdata(n).active_spike='bbl081d04_a_PTD.spk.mat';
ICdata(n).active_rawid=126473;
ICdata(n).postpassive_spike='bbl081d05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=126474;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl081d-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(n).prepassive_spike='bbl081d03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=126472;
ICdata(n).active_spike='bbl081d06_a_PTD.spk.mat';
ICdata(n).active_rawid=126475;
ICdata(n).postpassive_spike='bbl081d05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=126474;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl081d-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(n).prepassive_spike='bbl081d03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=126472;
ICdata(n).active_spike='bbl081d04_a_PTD.spk.mat';
ICdata(n).active_rawid=126473;
ICdata(n).postpassive_spike='bbl081d05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=126474;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Babybell';
ICdata(n).craniotomy='4';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='bbl081d-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Babybell/bbl081d/sorted/';
ICdata(n).prepassive_spike='bbl081d03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=126472;
ICdata(n).active_spike='bbl081d06_a_PTD.spk.mat';
ICdata(n).active_rawid=126475;
ICdata(n).postpassive_spike='bbl081d05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=126474;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard
