

narf_set_path

cellid='btn144f-d1';
runclass='SPN';

% get info from celldb
cfd=dbgetscellfile('cellid',cellid,'runclass',runclass);

spikefile=[cfd(1).path cfd(1).respfile];
parmfile=[cfd(1).stimpath cfd(1).stimfile];


% set some parameters
filtfmt='envelope';
fs=100;
includeprestim=1;

% load stimulus envelope
[stim,stimparam]=loadstimfrombaphy(parmfile,[],[],filtfmt,...
                                   fs,0,0,includeprestim);

figure;
imagesc(stim(:,:,1));


% load response
options=struct('rasterfs',fs,...
               'includeprestim',includeprestim,...
               'channel',cfd(1).channum,...
               'unit',cfd(1).unit,...
               'tag_masks',{{'Reference'}});
[r,tags]=loadspikeraster(spikefile,options);

figure;
goodtrials=squeeze(~isnan(r(1,:,:)));
imagesc(goodtrials);

% get rid of stimuli that don't have any response data.
keepstim=find(goodtrials(1,:));

r=r(:,:,keepstim);
stim=stim(:,:,keepstim);

goodtrials=squeeze(~isnan(r(1,:,:)));
repcount=sum(goodtrials);  % number of repeats of each stimulus

valstim=find(repcount==max(repcount));
eststim=find(repcount<max(repcount));

stim_est=stim(:,:,eststim);
r_est=r(:,:,eststim);
psth_est=nanmean(r_est,2);

stim_val=stim(:,:,valstim);
r_val=r(:,:,valstim);
psth_val=nanmean(r_val,2);

psth_est=psth_est(:);
stim_est=stim_est(:,:);

outfile=sprintf('/auto/data/tmp/SPN_data_%s.mat',cellid);
save(outfile);

return


timelag=2;

figure;
subplot(1,2,1);
plot(stim_est(1,1:(end-timelag)),psth_est((timelag+1):end),'.');
title(sprintf('cc channel 1: %.2f',...
      xcov(stim_est(1,1:(end-timelag)),psth_est((timelag+1):end),0,'coef')));

subplot(1,2,2);
plot(stim_est(2,1:(end-timelag)),psth_est((timelag+1):end),'.');
title(sprintf('cc channel 2: %.2f',...
      xcov(stim_est(2,1:(end-timelag)),psth_est((timelag+1):end),0,'coef')));

% pseudo code for STRF estimation:

% for each stimulus channel - x  1 to 2
%   for each time lag - u  -50 to 50 
%      strf(x,u)= 1/T * mean( psth_est(t) * stim(x,t-u) )





