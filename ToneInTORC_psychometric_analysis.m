% Psychometric analysis Tone-in-noise variable SNR task
% Daniela 12/10/2015

narf_set_path
baphy_set_path

dbopen;

minrawid=114634;     % 11/23/2015

sql=['SELECT gDataRaw.* FROM gDataRaw',...
     ' WHERE runclass="PTD"',...
     ' AND behavior="active"',...
     ' AND cellid like "TTL%"',...
     ' AND not(bad)',...
     ' AND gDataRaw.id>',num2str(minrawid)];
 
 
rawfiles=mysql(sql);
filecount=length(rawfiles);

parmfiles=cell(filecount,1);
for ii=1:filecount,
    parmfiles{ii}=[rawfiles(ii).resppath rawfiles(ii).parmfile];
end

AllRT=[];
medianRT=[]; 
meanRT=[];

for ii = 1:length(parmfiles),
    rawid= rawfiles(ii).id;
    baphyparameter = dbReadData(rawid);
    % Include only Variable SNR behavioral sessions
    if length(baphyparameter.Trial_TargetIdxFreq) > 2,
        
        LoadMFile(parmfiles{ii})
        
        trialparms=exptparams.TrialObject;
        trialcount=globalparams.rawfilecount;
        perf=exptparams.Performance(1:trialcount);
        
        % Not good because the length of parmfiles is not the length that
        % AllRT sholud have after exclusion of parmfiles that are not
        % Variable SNR task
        
        
        % code from di_nolick function
        trialtargetid=zeros(globalparams.rawfilecount,1);
        if isfield(exptparams,'UniqueTargets') && length(exptparams.UniqueTargets)>1,
            UniqueCount=length(exptparams.UniqueTargets);
            for tt=1:trialcount,
                trialtargetid(tt)=find(strcmp(perf(tt).ThisTargetNote,...
                    exptparams.UniqueTargets),1);
            end
        end
        
        % use indexes to creat a column vector of trial SNRs
        trialSNR=zeros(globalparams.rawfilecount,1);
        trialHit=zeros(globalparams.rawfilecount,1);
        trialRT=zeros(globalparams.rawfilecount,1);
        for rr=1:trialcount,
            trialHit(rr)=perf(rr).Hit==1; %Hit trials
            trialSNR(rr)=trialparms.RelativeTarRefdB(trialtargetid(rr));
            trialRT(rr)=perf(rr).ReactionTime;
        end
        trialSNRhit=trialSNR(find(trialHit));
        trialRThit=trialRT(find(trialHit));
      
        SNR=flipud(unique(trialSNRhit));
        for jj=1:length(SNR),
            snrIdx=find(trialSNRhit==SNR(jj));
            medianRT(ii,jj)=median(trialRThit(snrIdx));
            meanRT(ii,jj)=mean(trialRThit(snrIdx));
        end
     
    end
end

Allmean = mean(meanRT);
Allmedian = median(medianRT);

figure
plot(SNR, Allmean, 'k-o')
hold on
plot(SNR,Allmedian, 'r-o')
xlabel('SNR(dB)')
ylabel('RT(sec)')
Title('Variable SNR task - RTvsSNR')
legend('mean','median')


