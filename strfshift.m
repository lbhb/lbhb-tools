% function strfc=strfshift(strf,shiftbins,shifttbins);
% shiftbins is rounded to nearest integer
function [strfc,n0]=strfshift(th0,shiftbins,shifttbins);

if ~exist('shifttbins','var'),
   shifttbins=0;
end

smooth = [100 size(th0,2).*4];
sc=smooth(1)./size(th0,1);
th0 = interpft(interpft(th0,smooth(1),1),smooth(2),2);
%sc=3;
%th0=imresize(th0,sc,'bilinear');
shiftbins=shiftbins.*sc;

bfshift=round(shiftbins);
th0=shift(th0,[bfshift shifttbins]);
n0=ones(size(th0));
if bfshift>0,
   th0(1:bfshift,:)=0;
   n0(1:bfshift,:)=0;
else
   th0((end+bfshift+1):end,:)=0;
   n0((end+bfshift+1):end,:)=0;
end
if shifttbins>0,
   th0(:,1:shifttbins)=0;
   n0(:,1:shifttbins)=0;
elseif shifttbins<0,
   th0(:,(end+shifttbins+1):end)=0;
   n0(:,(end+shifttbins+1):end)=0;
end

%th0=imresize(th0,1./sc,'bilinear',0);
%n0=imresize(n0,1./sc);
strfc=th0;
