% function outfile=exportRaster(spkfile,options,outfile[=autoset])
%
% runs loadspikeraster(spkfile,options) as well as associated baphy
% parameter file and saves contents to a .mat-format file that can be read
% by other programs (including those written in python!)
%
% created SVD 2015-09-24
%
function outfile=exportRaster(spkfile,options,outfile)

% generate parmfile name
parmfile=strrep(spkfile,[filesep 'sorted' filesep],filesep);
parmfile=strrep(parmfile,'.spk.mat','');

narf_set_path;
r=loadspikeraster(spkfile,options);

LoadMFile(parmfile);

if ~exist('outfile','var'),
   outfile=strrep(spkfile,[filesep 'sorted' filesep],...
      [filesep 'tmp' filesep]);
   extrastring='';
   ff=fields(options);
   for ii=1:length(ff),
      v=options.(ff{ii});
      if isnumeric(v),
         extrastring=[extrastring '.' ff{ii} num2str(v)];
      elseif iscell(v),
         extrastring=[extrastring '.' ff{ii} v{1}];
      else
         % assume it's a string
         extrastring=[extrastring '.' ff{ii} v];
      end
   end
   outfile=strrep(outfile,'.spk.mat',[extrastring '.mat']);
   outfile = regexprep(outfile,' +',' ');
   outfile=strrep(outfile,' ','_');
   fprintf('saving to %s\n',outfile);
end

save(outfile,'r','globalparams','exptparams','exptevents');
