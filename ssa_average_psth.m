%Script to plot average PSTH of SSA data.
%ZPS, 9/4/15

%get a list of SSA cells
ssa_cells = dbgetscellfile('runclass', 'SSA');
ssa_cellids = {ssa_cells.cellid};
start_idx = find(strcmp(ssa_cellids, 'chn062c-c1')); %half-max protocol only
cellids = ssa_cellids(start_idx:end);
%load and cache if not cached already
cache_path = '/auto/users/schwarza/code/ssa/cache/';
for cellidx=1:length(cellids)
  cellid = cellids{cellidx};
  if ~exist([cache_path cellid '.mat']);
    ssa_plot(cellid);
  end
end
%set variables
rare =   [1 4];
common = [2 5];
onset =  [3 6];
pop_psth = nan(length(cellids), 40, 6);
%screen cells against criteria
crit.isolation = 80;
crit.level = 65;
crit.bandwidth = 0.125;
crit.dur = 0.1; %noise burst duration
crit.isi = 0.3; %interstimulus interval
prepip = crit.isi/2;
pipdur = crit.dur;
reject.isolation = 0;
reject.params = 0;
reject.sig = 0;
analyzed = 0;
for cellidx=1:length(cellids)
 cellid = cellids{cellidx};
 load([cache_path cellid '.mat'])
 if (cellfiledata.isolation < crit.isolation)
    reject.isolation = reject.isolation + 1;
    continue
 end
 if (exptparams.TrialObject.ReferenceHandle.Bandwidth ~= crit.bandwidth) && ...
    (exptparams.TrialObject.OveralldB ~= crit.level) && ... 
    (exptparams.TrialObject.ReferenceHandle.PipDuration ~= crit.dur) && ...
    (exptparams.TrialObject.ReferenceHandle.PipInterval ~= crit.isi)
    reject.params = reject.params + 1;
    continue
 end
 if ~is_sig
   reject.sig = reject.sig + 1;
   continue
 end
 %if a cell passes criteria, calculate a normalized psth
 analyzed = analyzed + 1;
 start_bin = round(prepip*rasterfs)+1; %bin in which stimulus begins
 mr = nanmean(r,2)*rasterfs;
 mr_spont = nanmean(nanmean(mr(1:start_bin-1,:)));
 pop_psth(cellidx,:,:) = (mr-mr_spont)/max(max(abs(mr-mr_spont)));
end
disp(sprintf('\nAnalyzing %d recordings.', length(cellids)));
disp(sprintf('%d rejected - isolation < %%%d.', reject.isolation, crit.isolation));
disp(sprintf('%d rejected - stimulus parameters incorrect.', reject.params));
disp(sprintf('%d rejected - did not respond to stimulus.', reject.sig));
disp(sprintf('%d included in analysis.', analyzed));
%plot average psth for rare and common condition
m_rare = nanmean(nanmean(pop_psth(:,:,rare),1),3);
sem_rare = nanstd(nanmean(pop_psth(:,:,rare),3),0,1)./sqrt(analyzed);
m_common = nanmean(nanmean(pop_psth(:,:,common),1),3);
sem_common = nanstd(nanmean(pop_psth(:,:,common),3),0,1)./sqrt(analyzed);
tt = (1:40)./rasterfs-prepip;
fig = figure;
hold on
errorbar(tt, m_rare, sem_rare, 'r', 'linewidth', 2)
errorbar(tt, m_common, sem_common, 'b', 'linewidth', 2)
aa = axis;
plot([0 0], aa(3:4), 'g--')
plot([0 0]+ pipdur, aa(3:4), 'g--')
xlabel('Time from Stimulus Onset (s)')
ylabel('Average Normalized Response')
legend('Rare', 'Common')
title(sprintf('Average PSTH (n = %d)', analyzed))
axis tight
hold off
%save figure
archive_path = ...
  ['/auto/users/schwarza/ssa_spn/plots/' datestr(now(),'yyyy_mm_dd') '/'];
if ~exist(archive_path)
  mkdir(archive_path);
end
print(fig, '-depsc', [archive_path 'average_psth'])
