function opto_sim(rawid, channel, unit);

if ~exist('channel','var'),
    channel=1;
end
if ~exist('unit','var'),
    unit=1;
end

% line colors:
spc={[0.5 0.5 0.5],[0 .7 0]};
% shading colors:
ssc={[0.7 0.7 0.7],[0 1 0]};

printstring={};
    
cfd=dbgetscellfile('rawid',rawid);

if ~isempty(cfd),
    parms=dbReadData(rawid);
    
    for cfidx=1:min(length(cfd),2),
        mfile=[cfd(cfidx).stimpath cfd(cfidx).stimfile];
        spikefile=[cfd(cfidx).path cfd(cfidx).respfile];
        channel=cfd(cfidx).channum;
        unit=cfd(cfidx).unit;
        
        options=struct('rasterfs',5000,'psthfs',100,'usesorted',1,...
                       'psth',1,'datause','Both');
        if parms.Ref_PostStimSilence>0.2,
            options.PreStimSilence=parms.Ref_PreStimSilence;
            options.PostStimSilence=0.2;
        end
        options.raster_pix=1;
        if ~isempty(cfd(cfidx).goodtrials),
            options.trialrange=eval(cfd(cfidx).goodtrials);
        end
        options.spc=spc;
        options.ssc=ssc;
        
        options.rasterfs=25;
        options.includeprestim=0;
        options.tag_masks={'SPECIAL-ALL'};
        
        binsize=1;
        [r,tags]=loadspikeraster(spikefile,options);
        
        rtemp=cat(1,r(:,:,1:2),r(:,:,3:4)).*options.rasterfs;
        mm=squeeze(mean(rtemp,2));
        
        [mXC,eXC]=spike_train_similarity(rtemp,binsize);
        fprintf('\n');
        
        figure;
        subplot(2,1,1);plot(squeeze(nanmean(rtemp,2)));
        legend('light on','light off');
        title(sprintf('%s XC within: %.4f+/-%.3f across: %.4f+/-%.3f',...
                      cfd(cfidx).cellid,mXC(1),eXC(1),mXC(2),eXC(2)));
        
        subplot(2,1,2);
        
        hist(sqrt(mm));
        legend('light on','light off');
        title(sprintf('mean on/off: %.1f/%.1f',mean(mm)));
        
         
    end
    drawnow;
   
end

