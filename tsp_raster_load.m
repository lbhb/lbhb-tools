% function res=tsp_raster_load(cellid,batchid,rasterfs,forcereload);
%
% returns res structure
%    res.cellid
%    res.batchid
%    res.rasterfs - sampling rate of rasters
%    res.stimfiles - baphy parameter file names corresponding to rasters
%    res.baphyparms - baphy parameter structures from celldb for each file
%    res.rresp - cell array of reference responses for each trial
%        rresp{n} = [time X rep X stimid] matrix of responses for nth file
%    res.tresp - cell array for target responses
%        tresp{n} = [time X rep X tarcategory] values:
%        tarcategory - selective attention: 4 categories
%          1: probe at BF (low SNR)
%          2: conditioning at BF (high SNR)
%          3: probe away
%          4: conditioning away
%        tarcategory - variable SNR: 2 categories
%          1: low SNR (at BF implied)
%          2: high SNR
%    res.rtags - event tags for each reference in each file;
%    res.ttags - event tags for each target;
%    res.routcome - behavioral outcomes for each reference's trial
%        routcome{n} = [rep X stim] values:
%          0: passive
%          1: hit
%          2: miss (adjacent hit/fa)  
%          3: false alarm   
%          4: miss (spaced-out animal b/c adjacent trials are misses)
%    res.toutcome - behavioral outcomes for each target's trial
%    res.DI - DI [tarcategory X file] (nan for passive or target not used)
%    res.taskname - description of batch;
%    res.tasktype - 1: selective attention, 2: variable SNR;
%    res.tlabels - labels for each tarcategory;
%    res.tasklabels - labels for the 'A1' or 'A2' task;
%    res.filecode - filecode indicating behavior condition
%          Px - passive #x
%          A1 - attend BF or low (hard) SNR
%          A2 - attend away or high (easy) SNR
%
% created SVD 2014-10-01
function res=tsp_raster_load(cellid,batchid,rasterfs,forcereload)
    
    if ~exist('rasterfs','var'),
        rasterfs=100;
    end
    if ~exist('forcereload','var'),
        forcereload=0;
    end
    this_version=1;
    
    % check for cache file
    global SERVER_PATH
    basepath=[SERVER_PATH 'tmp' filesep 'tsp' filesep];
    cachefile=sprintf('%stsp_%s_%d_%dHz.mat',basepath,cellid,...
                      batchid,rasterfs);
    
    if exist(cachefile,'file') && ~forcereload,
        load(cachefile);
        % make sure cached file was generated by latest saved_version of
        % this function
        if exist('saved_version','var') && saved_version==this_version,
           return
        end
    end
    
    % load the slow way
    dbopen;
    
    cellfiledata=dbgettspfiles(cellid,batchid);

    stimfiles={cellfiledata.stimfile};
    stimpaths=cell(1,length(stimfiles));
    resppaths=cell(1,length(stimfiles));
    rresp={};
    rtrialset={};
    cresp={};
    tresp={};
    rtags={};
    ctags={};
    ttags={};
    routcome={};
    toutcome={};
    coutcome={};
    iso=cat(2,cellfiledata.isolation);
    baphyparms=cell(length(stimfiles),1);
    baphyperf=cell(length(stimfiles),1);
    for ii=1:length(stimfiles),
        [baphyparms{ii},baphyperf{ii}]=dbReadData(cellfiledata(ii).rawid);
        
        stimpaths{ii}=[cellfiledata(ii).stimpath cellfiledata(ii).stimfile];
        spkfile=[cellfiledata(ii).path cellfiledata(ii).respfile];
        resppaths{ii}=spkfile;
        options=struct;
        options.rasterfs=rasterfs;
        options.channel=cellfiledata(ii).channum;
        options.unit=cellfiledata(ii).unit;
        options.includeprestim=1;
        options.includeincorrect=1;
        if ~isempty(cellfiledata(ii).goodtrials),
            trialrange=eval(['[' cellfiledata(ii).goodtrials ']'])
            ff=find(diff(trialrange(30:end))>1)+30;
            if ~isempty(ff),
               fprintf('TRUNCATING %d trials at %d b/c reminder\n',...
                  trialrange(end),ff(1));
               trialrange=trialrange(1:ff(1));
            end
            options.trialrange=trialrange;
        end
        
        options.tag_masks={'Reference'};
        [rresp{ii},rtags{ii},trialset,exptevents]=...
            loadspikeraster(spkfile,options);
        rtrialset{ii}=trialset;

        % figure out behavioral outcome of each trial
        trialcount=exptevents(end).Trial;
        trialoutcome=zeros(trialcount,1);
        [~,hittrials]=evtimes(exptevents,'*PUMPON*');
        [~,misstrials]=evtimes(exptevents,'OUTCOME,MISS');
        [~,fatrials]=evtimes(exptevents,'OUTCOME,FALSEALARM');
        trialoutcome(hittrials)=1;
        trialoutcome(misstrials)=4;
        trialoutcome(fatrials)=3;
        % special case for misses followed or preceeded by a hit or
        % FA, ie, a trial when the animal was probably awake
        trialoutcome(trialoutcome(1:(end-1))==4 & ...
                     ismember(trialoutcome(2:end),[1 3]))=2;
        ff=find(trialoutcome(2:end)==4 &...
                ismember(trialoutcome(1:(end-1)),[1 3]))+1;
        trialoutcome(ff)=2;
        
        routcome{ii}=ones(size(trialset)).*nan;
        routcome{ii}(trialset>0)=trialoutcome(trialset(trialset>0));
        
        % handle files with and without targets
        if isfield(baphyparms{ii},'Tar_PreStimSilence'),
           options.tag_masks={'Target'};
           options.includeprestim=[baphyparms{ii}.Ref_PreStimSilence ...
              baphyparms{ii}.Ref_PostStimSilence];
           [tresp{ii},ttags{ii},trialset]=loadspikeraster(spkfile,options);
           toutcome{ii}=ones(size(trialset)).*nan;
           toutcome{ii}(trialset>0)=trialoutcome(trialset(trialset>0));
           
           options.tag_masks={'Catch'};
           [cresp{ii},ctags{ii},trialset]=loadspikeraster(spkfile,options);
           coutcome{ii}=ones(size(trialset)).*nan;
           coutcome{ii}(trialset>0)=trialoutcome(trialset(trialset>0));
        else
           % no target, leave empty
           tresp{ii}=[];ttags{ii}={};toutcome{ii}=[];
           cresp{ii}=[];ctags{ii}={};coutcome{ii}=[];
        end
        
        % merge catch data into target cell array
        if ~isempty(cresp{ii}) && sum(~isnan(cresp{ii}(:)))>0,
            goodcat=find(~isnan(cresp{ii}(1,1,:)));
            ttags{ii}={ttags{ii}{:} ctags{ii}{goodcat}};
            dd=size(tresp{ii});
            dc=size(cresp{ii});
            if dc(2)>dd(2),
                tresp{ii}=cat(2,tresp{ii},ones(dd(1),dc(2)-dd(2),dd(3)).*nan);
                toutcome{ii}=cat(1,toutcome{ii},ones(dc(2)-dd(2),dd(3)).*nan);
            else
                cresp{ii}=cat(2,cresp{ii},ones(dc(1),dd(2)-dc(2),dc(3)).*nan);
                coutcome{ii}=cat(1,coutcome{ii},ones(dd(2)-dc(2),dc(3)).*nan);
            end
            % this is a little kludgy, but should work if the last
            % target was always the catch stimulus
            if isfield(baphyperf{ii},'uDiscriminationIndex'),
               if isfield(baphyparms{ii},'Trial_CatchIdxFreq') &&...
                     sum(baphyparms{ii}.Trial_CatchIdxFreq),
                  cattaridx=find(baphyparms{ii}.Trial_CatchIdxFreq, 1 );
               else
                  cattaridx=length(baphyperf{ii}.uDiscriminationIndex);
               end
               baphyperf{ii}.uDiscriminationIndex(cattaridx)=...
                   baphyperf{ii}.cDiscriminationIndex;
            end
            tresp{ii}=cat(3,tresp{ii},cresp{ii}(:,:,goodcat));
            toutcome{ii}=cat(2,toutcome{ii},coutcome{ii}(:,goodcat));
        end
     end
    
    % align various targets to meaningful categories
    ret=request_celldb_batch(batchid,cellid);
    filecount=length(rresp);
    if ~isempty(ret),
        filecode=ret{1}.filecode;
    else
        filecode=cell(size(baphyparms));
        pp=0;
        aa=0;
        for ff=1:filecount,
            if strcmpi(baphyparms{ff}.BehaveObjectClass,'Passive'),
                pp=pp+1;
                filecode{ff}=['P',num2str(pp)];
            else
                aa=aa+1;
                filecode{ff}=['A',num2str(aa)];
            end
        end
    end
    firstactiveidx=min(find(strcmp(filecode,'A1') | strcmp(filecode,'A2')));
    parms=baphyparms{firstactiveidx};
    
    batchdata=dbget('sBatch',batchid);
    taskname=batchdata.name;
    if ismember(batchid,[252 268 274 275 287]),
       tasktype=1; % selective attention
       % figure out BF
       maxtaridx=min(find(parms.Trial_TargetIdxFreq==max(parms.Trial_TargetIdxFreq)));
       if strcmp(filecode{firstactiveidx},'A1'),
          BF=parms.Tar_Frequencies(maxtaridx);
       else
          tdiff=abs(parms.Tar_Frequencies-parms.Tar_Frequencies(maxtaridx));
          farthesttaridx=min(find(tdiff==max(tdiff)));
          BF=parms.Tar_Frequencies(farthesttaridx);
       end
       finaltarcount=4;
       tlabels={'BF probe','BF cond','Away probe','Away cond'};
       tasklabels={'Attend BF', 'Attend away'};
    elseif ismember(batchid,[251 253 254]),
       tasktype=2; % variable SNR
       maxSNRidx=find(parms.Trial_RelativeTarRefdB==max(parms.Trial_RelativeTarRefdB));
       BF=parms.Tar_Frequencies(maxSNRidx);
       finaltarcount=2;
       tlabels={'Low SNR','High SNR'};
       tasklabels={'Hard', 'Easy'};
    end
    DI=nan(finaltarcount,filecount);
    
    % find out which target corresponds to which Tar Sound index
    % selective attention: 4 categories
    % 1: probe at BF (low SNR)
    % 2: conditioning at BF (high SNR)
    % 3: probe away
    % 4: conditioning away
    %
    % variable SNR: 2 categories
    % 1: low SNR (at BF implied)
    % 2: high SNR
    
    for ii=1:length(stimfiles),
        % find out which target corresponds to which Tar Sound index
        SNR=baphyparms{ii}.Trial_RelativeTarRefdB;
        newtresp=zeros(size(tresp{ii},1),size(tresp{ii},2),finaltarcount).*nan;
        newtoutcome=zeros(size(toutcome{ii},1),finaltarcount).*nan;
        for jj=1:length(ttags{ii}),
            tdata=strsep(ttags{ii}{jj},',',1);
            thisfreq=str2double(tdata{2});
            thistaridx=find(thisfreq==baphyparms{ii}.Tar_Frequencies, 1 );
            % must be within 10% of BF to be called BF
            atBF=~floor(abs(thisfreq-BF)./mean([thisfreq BF])*9.9);
            if tasktype==1 && length(SNR)<=2,
               lowSNR=1;  % special case, early behaviors
            elseif length(SNR)==4,
               if thistaridx<3,
                  lowSNR=(SNR(thistaridx)==min(SNR(1:2)));
               else
                  lowSNR=(SNR(thistaridx)==min(SNR(3:4)));
               end
            else
               lowSNR=(SNR(thistaridx)<max(SNR));
            end
            toneindex=(1-atBF)*2+(2-lowSNR);
            newtresp(:,:,toneindex)=tresp{ii}(:,:,jj);
            newtoutcome(:,toneindex)=toutcome{ii}(:,jj);
            
            if isfield(baphyperf{ii},'uDiscriminationIndex'),
               DI(toneindex,ii)=baphyperf{ii}.uDiscriminationIndex(thistaridx);
            end
        end
        tresp{ii}=newtresp;
        toutcome{ii}=newtoutcome;
    end
    
    res.cellid=cellid;
    res.batchid=batchid;
    res.rasterfs=rasterfs;
    res.stimfiles=stimfiles;
    res.stimpaths=stimpaths;
    res.resppaths=resppaths;
    res.baphyparms=baphyparms;
    res.rresp=rresp;
    res.rtrialset=rtrialset;
    res.tresp=tresp;
    res.rtags=rtags;
    res.ttags=ttags;
    res.routcome=routcome;
    res.toutcome=toutcome;
    res.DI=DI;
    res.iso=iso;
    res.taskname=taskname;
    res.tasktype=tasktype;
    res.tlabels=tlabels;
    res.tasklabels=tasklabels;
    res.filecode=filecode;
    saved_version=this_version;
    save(cachefile,'res','saved_version');
    unix(['chmod a+w ',cachefile]);
    