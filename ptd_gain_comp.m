% function results=ptd_gain_comp(cellid,options)
%
% calculate gain and spont difference between each active PTD file for a
% given cell relative to passive.
%
% options.makenewfigs=0;  - always plot to same figures; 
% options.makenewfigs=1;  -  plot new figures; 
%
% options.savefigures=0; - don't save figures in folder
% options.savefigures=1; - save figures in folder
%
% SVD modified 2017-04-12 to be compatible with other IC PTD analysis code
%
function results=ptd_gain_comp(cellid,options)

if ~exist('options','var'),
   options=struct();
end

options.runclass=getparm(options,'runclass','PTD');
options.unit=getparm(options,'unit',1);
options.channel=getparm(options,'channel',1);
options.psthfs=getparm(options,'psthfs',100); 
options.sigthreshold=getparm(options,'sigthreshold',4);
options.datause=getparm(options,'datause','Reference Only');
options.active=getparm(options,'active',0);
options.usesorted=getparm(options,'usesorted',1);
options.psth=1;
options.rasterfs=getparm(options,'rasterfs',options.psthfs);
options.lick=getparm(options,'lick',0);
options.includeprestim=getparm(options,'includeprestim',[]);
options.rawid=getparm(options,'rawid',[]);
options.raster=getparm(options,'raster',1);
options.mergeset=getparm(options,'mergeset',{});
options.gainAnalysis=getparm(options,'gainAnalysis',[]);
options.binsize=getparm(options,'binsize',1);

options.pupil_min=getparm(options,'pupil_min',0);

% MAKENEWFIGS=0;  - always plot to same figures; 
% MAKENEWFIGS=1;  - plot new figures; 
options.makenewfigs=getparm(options,'makenewfigs',0);
% savefigures=0; - don't save figures in folder
% savefigures=1; - save figures in folder
options.savefigures=getparm(options,'savefigures',0);


dbopen;
if ~isempty(options.rawid)
   if length(options.rawid)>1
      rawstr=mat2str(options.rawid);
      rawstr=rawstr(2:(end-1));
   else
      rawstr=num2str(options.rawid);
   end
   rawstr=strrep(rawstr,' ',',');
   sitestr=['gDataRaw.id in (',rawstr,')'];
   %active=0;
else
   sitestr=['runclass in ("',strrep(options.runclass,',','","'),'")'];
end

if ~options.usesorted
   siteid=strsep(cellid,'-');
   if length(siteid)>1
      options.channel=siteid{2}(1)-'a'+1;
      options.unit=str2num(siteid{2}(2));
   end
   siteid=siteid{1};
   sql=['SELECT gDataRaw.* FROM gDataRaw',...
      ' WHERE gDataRaw.cellid="',siteid,'"',...
      ' AND ',sitestr,...
      ' AND not(bad)',...
      ' ORDER BY parmfile'];
elseif options.active
   % this never runs??
   sql=['SELECT gDataRaw.*,sCellFile.goodtrials,sCellFile.channum,sCellFile.unit,sCellFile.singleid,sCellFile.area',...
      ' FROM gDataRaw,sCellFile',...
      ' WHERE gDataRaw.id=sCellFile.rawid',...
      ' AND sCellFile.cellid="',cellid,'"',...
      ' AND ',sitestr,...
      ' AND not(bad)',...
      ' AND behavior="active"',...
      ' ORDER BY parmfile'];
else
   sql=['SELECT gDataRaw.*,sCellFile.goodtrials,',...
      ' sCellFile.channum,sCellFile.unit,sCellFile.singleid,sCellFile.area',...
      ' FROM gDataRaw,sCellFile',...
      ' WHERE gDataRaw.id=sCellFile.rawid',...
      ' AND sCellFile.cellid="',cellid,'"',...
      ' AND ',sitestr,...
      ' AND not(bad)',...
      ' ORDER BY parmfile'];
end

rawdata=mysql(sql);

rids=cat(1,rawdata.id);
for fidx=1:length(options.rawid)
   ff=find(rids==options.rawid(fidx));
   if fidx==1
      newrawdata=rawdata(ff);
   else
      newrawdata(fidx)=rawdata(ff);
   end
end
rawdata=newrawdata;

if length(rawdata)>=1
   siteid=rawdata(1).cellid;
end

r={};
pup={};
r1000={};
rref={};
tags={};
minresp=0;
maxresp=0;

filecount=length(rawdata);
% forcing the second file to always be "active" (or postpassive)
active=[0; 1];
parms=cell(filecount,1);
for fidx=1:length(rawdata)
   
   % check for limited trial range
   if ~isempty(rawdata(fidx).goodtrials)
       options.trialrange=eval(rawdata(fidx).goodtrials);
   elseif isfield(options,'trialrange')
       options=rmfield(options,'trialrange');
   end
   if options.usesorted
      options.unit=rawdata(fidx).unit;
      options.channel=rawdata(fidx).channum;
   end
%    if strcmpi(rawdata(fidx).behavior,'active')
%       active(fidx)=1;
%    end
   
   options1k=options;
   options1k.includeprestim=0;
   options1k.PreStimSilence=0;
   options1k.PostStimSilence=0;
   options1k.rasterfs=1000;  
   
   
   if any(strcmp(rawdata(fidx).parmfile,options.mergeset))
       for tt=1:length(options.mergeset)
           mfile=[rawdata(fidx).resppath options.mergeset{tt}];
           [tr,tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
           [tr1k]=raster_load(mfile,options.channel,options.unit,options1k);
           if tt==1
               r{fidx}=tr.*options.rasterfs;
               r1000{fidx}=tr.*options.rasterfs;
           else
               r{fidx}=cat(2,r{fidx},tr1k.*options.rasterfs);
               r1000{fidx}=cat(2,r{fidx},tr1k.*options.rasterfs);
           end
       end
   elseif strcmpi(options.datause,'Target Only')
      mfile=[rawdata(fidx).resppath rawdata(fidx).parmfile];
      [r{fidx},tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
      LoadMFile(mfile);
      baphyparms=dbReadData(rawdata(fidx).id);
      tarmaxidx=size(r{fidx},3);
      if isfield(baphyparms,'Trial_OverlapRefIdx')
         tarrefidx=ifstr2num(baphyparms.Trial_OverlapRefIdx);
         
         % append fixed reference TORC response for comparison with target+TORC response
         if ~isempty(tarrefidx)
            roptions=options;
            roptions.datause='Reference Only';
            trr=raster_load(mfile,options.channel,options.unit,roptions);
            trr=trr(:,:,tarrefidx);
            if size(trr,2)>size(r{fidx},2)
                % pad target reps with nan
                r{fidx}=cat(2,r{fidx},nan(size(r{fidx},1),size(trr,2)-size(r{fidx},2),size(r{fidx},3)));
            else size(trr,2)<size(r{fidx},2);
                trr=cat(2,trr,nan(size(trr,1),size(r{fidx},2)-size(trr,2)));
            end
            if size(r{fidx},1)>size(trr,1)
                trr=cat(1,trr,nan(size(r{fidx},1)-size(trr,1),size(trr,2)));
            end
            r{fidx}=cat(3,r{fidx},trr);
         end
      end
      
      % scale to spikes/sec
      r{fidx}=r{fidx}*options.rasterfs;
      r1000{fidx}=[]; % not used for target-only analysis
   else
       mfile=[rawdata(fidx).resppath rawdata(fidx).parmfile];
       [r{fidx},tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
       r{fidx}=r{fidx}.*options.rasterfs;
       r1000{fidx}=options.rasterfs .*...
          raster_load(mfile,options.channel,options.unit,options1k);
   end
   
   if options.pupil_min
      poptions=options;
      poptions.pupil=1;
      poptions.pupil_median=1;
      poptions.pupil_offset=0.75;
      poptions.tag_masks={'Reference'};
      
      pup{fidx}=loadevpraster(mfile,poptions);
      
   end
   
   parms{fidx}=dbReadData(rawdata(fidx).id);
   PreStimSilence=parms{fidx}.Ref_PreStimSilence;
   Duration=parms{fidx}.Ref_Duration;
   PostStimSilence=parms{fidx}.Ref_PostStimSilence;
   
   mr=squeeze(nanmean(r{fidx},2));
   maxresp=max(maxresp,max(mr(:)));
   minresp=min(minresp,min(mr(:)));
   
end
if isfield(options,'trialrange')
   options=rmfield(options,'trialrange');
end

fs=options.rasterfs;
PreBins=round(PreStimSilence.*fs);
DurBins=round(Duration.*fs);
PostBins=round(PostStimSilence.*fs);
OffsetBins=round(0.15.*fs);  


mm=maxresp;


activeidx=find(active);
activecount=length(activeidx);


results.rawid=cat(1,rawdata(activeidx).id);
results.singleid=cat(1,rawdata(activeidx).singleid);  
results.masterid=cat(1,rawdata(activeidx).masterid);  

results.spont_active=zeros(activecount,1);
results.spont_passive=zeros(activecount,1);
results.gain=zeros(activecount,3);
results.gainZ=zeros(activecount,3);
results.spontZ=zeros(activecount,3);
results.passidx=zeros(activecount,1);
results.tarfreq=zeros(activecount,1);
results.tarfreqpass=zeros(activecount,1);
results.tardist=zeros(activecount,1);
results.tardiff=zeros(activecount,1);
results.area=cell(activecount,1);
results.strfc=[];
results.strfsnr=0;
results.strfgain=0;

doing_targets=strcmpi(options.datause,'Target Only');

if options.makenewfigs
    fh=figure;
else
    if doing_targets
        fh=4;
    else
        fh=2;
    end
end

epochstr={'onset','sustained','offset'};

ref_vs_tar_pass=[nan; nan];
ref_vs_tar_act=[nan; nan];
ref_all_pass=nan;
ref_all_act=nan;

for aidx=1:activecount
   
   sfigure(fh);
   clf;
   
   if doing_targets
      tarfreq=parms{activeidx(aidx)}.Tar_Frequencies(1);
      tarmatch=zeros(filecount,1);
      for jj=1:filecount
         if parms{jj}.Tar_Frequencies(1)==tarfreq && ...
               length(parms{jj}.Tar_Frequencies)==length(parms{activeidx(aidx)}.Tar_Frequencies),
            tarmatch(jj)=1;
         end
      end
   else
      tarmatch=ones(filecount,1);
   end
   
   if sum(~active & tarmatch)
%       if options.firstpassiveonly==1
%          passidx=find(~active & tarmatch, 1);
%          passfile=rawdata(passidx).parmfile;
%          
%       elseif options.firstpassiveonly==2
%          passidx=max(find(~active & tarmatch,2));
%          passfile=rawdata(passidx).parmfile;
%          
%       else
%          passidx=find(~active & tarmatch);
%          passfile='PASSIVE';
%       end

      passidx=find(~active & tarmatch);
      passfile=rawdata(passidx).parmfile;
      
      r1=cat(2,r{passidx});
      r2=r{activeidx(aidx)};
      actfile=strrep(rawdata(activeidx(aidx)).parmfile,'.m','');
      
      if size(r2,1)<size(r1,1)
          r1=r1(1:size(r2,1),:,:);
      end
      
      if doing_targets
          % select only response to probe target
          probeidx=3;
          if length(tags{1})<probeidx
              probeidx=2;
              disp('old probe target format?')
              if length(tags{1})<probeidx
                 probeidx=1;
                  disp('missing probe target?');
                  %keyboard
              end
          end
          probetag=tags{1}{probeidx};
          midx1=find(ismember(tags{1},{probetag}));
          midx2=find(ismember(tags{2},{probetag}));
          if ~isempty(midx1) && ~isempty(midx2)
              r1=r1(:,:,[midx1 size(r1,3)]);
              r2=r2(:,:,[midx2 size(r2,3)]);
              tarmaxidx=1;
          else
              disp('probe target not found');
              keyboard
          end
      else
         if options.pupil_min
            pm1=nanmean(pup{passidx}(1:PreBins,:,:),1);
            pm2=nanmean(pup{activeidx(aidx)}(1:PreBins,:,:),1);
            p1=pm1(~isnan(pm1));
            p2=pm2(~isnan(pm2));
            thresh2=mean(p2)-std(p2)*2;
            belowthresh1=find(pm1<thresh2);
            belowthresh2=find(pm2<thresh2);
            r1(:,belowthresh1)=nan;
            r2(:,belowthresh2)=nan;
            fprintf('removing pupil below-threshold trials: pas: %d act: %d\n',...
               length(belowthresh1),length(belowthresh2));
            xx=(floor(min([p1(:);p2(:)])/5)*5):5:(ceil(max([p1(:);p2(:)])/5)*5)';
            n1=hist(p1,xx);
            n2=hist(p2,xx);
            sfigure(fh);
            subplot(3,2,5);Tartufo
            bar(xx,[n1(:) n2(:)]);
            hold on
            aa=axis;
            plot([thresh2 thresh2],[aa(3) aa(4)]);
            axis([xx(1)-5 xx(end)+5 aa(3:4)]);
            legend({passfile,actfile},'Interpreter','none');
         end
         
         % equalize number of reps for each stim
         for stimidx=1:size(r1,3)
            validx1=find(~isnan(r1(1,:,stimidx)));
            validx2=find(~isnan(r2(1,:,stimidx)));
            minrep=min(length(validx1),length(validx2));
            r1(:,1:minrep,stimidx)=r1(:,validx1(1:minrep),stimidx);
            r1(:,(minrep+1):end,stimidx)=nan;
            r2(:,1:minrep,stimidx)=r2(:,validx2(1:minrep),stimidx);
            r2(:,(minrep+1):end,stimidx)=nan;
         end
         
         %r1=r1(:,1:minrep,:);
         %r2=r2(:,1:minrep,:);
         %eithernanidx=find(isnan(r1) | isnan(r2));
         %r1(eithernanidx)=nan;
         %r2(eithernanidx)=nan;
      end
      
      m1=nanmean(r1,2);
      m2=nanmean(r2,2);
      
      if size(m2,1)<size(m1,1)
         m1=m1(1:size(m2,1),:,:);
      end
      
      if doing_targets && tarmaxidx<size(m1,3)
         rm1=m1(:,:,(tarmaxidx+1):end);
         rm2=m2(:,:,(tarmaxidx+1):end);
         m1=m1(:,:,1:tarmaxidx);
         m2=m2(:,:,1:tarmaxidx);
         
      else
         rm1=[];
         rm2=[];
      end
      
      
      % compute mean spont rate for each stimulus condition
      sp1=nanmean(nanmean(m1(1:PreBins,:)));
      sp2=nanmean(nanmean(m2(1:PreBins,:)));
      
      % compute mean response for each stimulus condition
      % (stimulus can be all references (~doing_targets)
      % or individual targets or TORC4 (ie, the reference paired with the
      % target)
      rrange=(PreBins+8):(PreBins+DurBins);
      if doing_targets
         ref_vs_tar_pass=squeeze(nanmean(nanmean(r1(rrange,:,:),2),1));
         ref_vs_tar_act=squeeze(nanmean(nanmean(r2(rrange,:,:),2),1));
      else
         % calculate average response to all refs.
         ref_all_pass=nanmean(nanmean(nanmean(r1(rrange,:,:))));
         ref_all_act=nanmean(nanmean(nanmean(r2(rrange,:,:))));
      end
      
      fprintf('spont P vs A: %.2f vs. %.2f\n',sp1,sp2);
      
      sfigure(fh);
      
      subplot(3,2,1);
      cla
      hold on
      tt=(1:size(m1,1))'./fs-PreStimSilence;
      if ~isempty(rm1)
          % plot matched TORC response if exists
          plot(tt,[nanmean(rm1,3)-sp1],'k--');
          plot(tt,[nanmean(rm2,3)-sp2],'--','Color',[0.5 0.5 0.5]);
          lset={[passfile,'-REF'],[actfile,'-REF'],passfile,actfile};
      else
         lset={passfile,actfile};
      end
      plot(tt,[nanmean(m1,3)-sp1],'Color',[0.0 0.9 0.0]);
      plot(tt,[nanmean(m2,3)-sp2],'Color',[0.0 0.0 1.0]);
      hold off
      
      hl=legend(lset);
      set(hl,'Interpreter','none');
      aa=axis;
      axis([tt(1) tt(end) -maxresp/3 maxresp]);
      if doing_targets
         title(sprintf('Target response (%d) - %s',tarfreq, cellid));
      else
         title(sprintf('Reference response - %s', cellid));
      end
      
      
      % 
      if doing_targets
         subplot(3,2,2);
         plot([ref_vs_tar_pass,ref_vs_tar_act]);
         xlabel('SNR (high to -inf)');
      end
      
      for epoch=1:3 % onset, sustained, offset
         
         if epoch==1  % onset
            rrange=(PreBins+1):(PreBins+7);
         elseif epoch==2  %sustained
            rrange=(PreBins+8):(PreBins+DurBins);
         elseif epoch==3  %offset
            rrange=(PreBins+DurBins):(PreBins+DurBins+8);
         end
         
         
         if doing_targets
            stim_set_count=2;  % target and target-paired ref
         else
            stim_set_count=1;  % all references
         end
         
         for stimidx=1:stim_set_count
            
            if doing_targets && stimidx==1
               % target and target-paired ref
               tm1=nanmean(m1(rrange,:,1:tarmaxidx),2);
               tm2=nanmean(m2(rrange,:,1:tarmaxidx),2);
               desc='Target';
            elseif doing_targets && stimidx==2
               % target-paired ref
               tm1=nanmean(rm1(rrange,:,end),2);
               tm2=nanmean(rm2(rrange,:,end),2);
               desc='Ref';
            else
               stim_set_count=1;  % all references
               tm1=nanmean(m1(rrange,:,:),2);
               tm2=nanmean(m2(rrange,:,:),2);
               desc='Ref';
            end
      
         
            gidx=find(~isnan(tm1(:)) & ~isnan(tm2(:)));
            tm1=tm1(gidx);
            tm2=tm2(gidx);
            tm1=tm1-sp1;
            tm2=tm2-sp2;
            
            if strcmp(options.gainAnalysis, 'Traditional')
               % "traditional" Sean's way
               % regress on Active=(gain)*Passive+offset
               % results.spont_passive = spont_passive
               % results.spont_active = spont_active + offset
               a=tm1;
               b=tm2;
               os=0;
               xname='File 1 (pas)';
               yname='File 2 (act)';
            else
               % difference way
               % regress on Diff=(diff_gain)*Mean+diff_offset
               % gain=diff_gain+os; ( gain=diff_gain+1 )
               % results.spont_passive = spont_passive
               % results.spont_active = spont_active + diff_offset
               
               a=(tm1+tm2)./2;
               b=tm2-tm1;
               os=1;
               xname='Mean';
               yname='Difference';
            end
            if options.binsize>1
               if os==0
                  ab=sortrows([a+b a b]);  % sort by mean response
                  ab=ab(:,2:3);
               else
                  ab=sortrows([a b]);  % sort by mean response
               end
               nn=floor(size(ab,1)/options.binsize)*options.binsize;
               ab=ab(1:nn,:);
               ab=reshape(ab,options.binsize,nn./options.binsize,2);
               ab=squeeze(nanmean(ab,1));
               a=ab(:,1);
               b=ab(:,2);
            end
            [~,si]=sort(rand(size(a)));
            a=a(si);
            b=b(si);
            
            jackcount=min(length(a),20);
            jackstep=length(a)./jackcount;
            gset=zeros(jackcount,2);
            for jj=1:jackcount
               kk=[1:round((jj-1).*jackstep) round(jj*jackstep+1):length(a)];
               kk=kk(:);
               %gset(jj,:)=polyfit(a(kk),b(kk),1);
               gset(jj,:)=regress(b(kk),[a(kk) ones(size(a(kk)))]);
            end
            
            pm=mean(gset,1);
            pe=nanstd(gset,0) * sqrt(jackcount-1);
            
            %p=polyfit(a,b,1);
            p=regress(b,[a ones(size(a))]);
            
            subplot(3,3,3+epoch+(stimidx-1)*3);
            
            x=[min(a),max(a)];
            y=[min(a) max(a)]*(p(1)+os)+p(2);
            mp=sortrows([tm1 tm2]);
            if ~doing_targets
               binsize=min(length(a),10);
               nn=floor(size(mp,1)/binsize)*binsize;
               mp=mp(1:nn,:);
               mp=reshape(mp,[binsize,nn./binsize,size(mp,2)]);
               mp=squeeze(nanmean(mp,1));
            end
            
            plot([-mm/20 mm],[-mm/20 mm],'k--','LineWidth',0.5);
            hold on
            plot(mp(:,1),mp(:,2),'.','Color',[0.5 0.5 0.5]);
            plot(x,y,'r-','LineWidth',1);
            hold off
            axis tight square
            title(sprintf('%s %s gain: %.2f',desc,epochstr{epoch},os+p(1)));
            xlabel(passfile,'Interpreter','none');
            ylabel(actfile,'Interpreter','none');
            
            results.gain(aidx,epoch,stimidx)=os+pm(1);
            results.gainZ(aidx,epoch,stimidx)=pm(1)./pe(1);
            results.spontZ(aidx,epoch,stimidx)=pm(2)./pe(2);
         end
         
      end
      
      fullpage landscape;
      
      results.ref_vs_tar_pass=ref_vs_tar_pass';
      results.ref_vs_tar_act=ref_vs_tar_act';
      results.ref_all_pass=ref_all_pass;
      results.ref_all_act=ref_all_act;
      results.spont_passive(aidx)=sp1;
      results.spont_active(aidx)=sp2+pm(2);
      results.passidx(aidx)=passidx;
      results.tarfreq(aidx)=parms{activeidx(aidx)}.Tar_Frequencies(1);
      
      if isfield(parms{passidx(1)},'Tar_Frequencies')
          results.tarfreqpass(aidx)=parms{passidx(1)}.Tar_Frequencies(1);
      end
      results.area{aidx}=rawdata(activeidx(aidx)).area;
      
      % if options.savefigures=True save ref and tar psth/gain figures
      if options.savefigures
          
          set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
          if doing_targets
              saveas(gcf, [options.figurepath cellid '_' passfile '_' actfile '_tar.pdf']);
          else
              saveas(gcf, [options.figurepath cellid '_' passfile '_' actfile '_ref.pdf']);
          end
          %close(gcf);
      end
   
   end
   
end

if doing_targets
   disp('Not reference data, skipping STRF analysis');
   return
   
end
if ~exist('passidx','var')
   return
end


%% STRF analysis
parmfile=[rawdata(passidx(1)).resppath rawdata(passidx(1)).parmfile];
LoadMFile(parmfile);
TorcObject=exptparams.TrialObject.ReferenceHandle;
rasterfs=options1k.rasterfs;
includefirstcycle=0;
jackN=16;

rp=cat(2,r1000{passidx});
ra=r1000{activeidx(aidx)};
if size(ra,2)<size(rp,2)
   rp=rp(:,1:size(ra,2),:);
elseif size(rp,2)<size(ra,2)
   ra=ra(:,1:size(rp,2),:);
end
rp(isnan(ra))=nan;
ra(isnan(rp))=nan;


[strfmm,snrpass,StimParam,strfee]=strf_est_core(rp,TorcObject,rasterfs,includefirstcycle,jackN);
strfpass=strfmm;
thispass=rawdata(passidx(1)).parmfile;

% passive tuning properties
[bf,bw,lat,offlat]=strf_get_tuning(strfpass,StimParam,strfee);
ybf=log2(bf./StimParam.lfreq);

if options.makenewfigs
   figure;
else
   sfigure(3);
end
clf

for aidx=1:activecount
   parmfile=[rawdata(activeidx(aidx)).resppath rawdata(activeidx(aidx)).parmfile];
   LoadMFile(parmfile);
   TorcObject=exptparams.TrialObject.ReferenceHandle;
   tarfreq=exptparams.TrialObject.TargetHandle.Frequencies(1);
   
   maxoct=StimParam.octaves;
   stepsize2=maxoct./(size(strfmm,1));
   shb=(maxoct./2-log2(tarfreq./StimParam.lfreq))./stepsize2;
   
   %res=det_gain_analysis(r1000([passidx(1) activeidx(aidx) passidx(1)]),TorcObject,rasterfs,cellid,0,tarfreq)
   taroct=log2(tarfreq./StimParam.lfreq);
   
   ra=r1000{activeidx(aidx)};
   thisact=rawdata(activeidx(aidx)).parmfile;
   jackN=0;
   [strfest,snr,~,strfee]=strf_est_core(ra,TorcObject,rasterfs,includefirstcycle,jackN);
   
   d1=gsmooth(strfest,0.1);
   d2=gsmooth(strfpass,0.1);
   mm=max(abs(cat(1,d1(:),d2(:))));
   [strfactc,n0]=strfshift(strfest,shb,0);
   [strfpasc,n0]=strfshift(strfpass,shb,0);
   d1=gsmooth(strfactc,0.1);
   d2=gsmooth(strfpasc,0.1);
   mm=max(abs(cat(1,d1(:),d2(:))));
   strfactc=strfactc./mm;
   strfpasc=strfpasc./mm;
   
   % calculate STRF global gain
   b=strfactc(:)-strfpasc(:);
   a=(strfactc(:)+strfpasc(:))/2;
   p=regress(b,[a ones(size(a))]);

   results.strfgain(aidx)=p(1)+1;
   
   subplot(3,activecount.*2,aidx*2-1);
   stplot(strfest,StimParam.lfreq,StimParam.basep,1,StimParam.octaves,[-mm mm]);
   hold on
   aa=axis;
   plot(aa(1:2),[1 1].*taroct,'r--');
   hold off
   title(sprintf('%s %s snr: %.2f',cellid,thisact,snr),'Interpreter','none');
   
   subplot(3,activecount.*2,activecount.*2+aidx*2-1);
   stplot(strfpass,StimParam.lfreq,StimParam.basep,1,StimParam.octaves,[-mm mm]);
   hold on
   aa=axis;
   ydiff=aa(4)-aa(3);
   ym=aa(3)+ydiff./2;
   plot(aa(1:2),[ybf ybf],'k--');
   plot([lat lat],aa(3:4),'k--');
   plot([offlat offlat],aa(3:4),'k--');
   plot(aa(1:2),[1 1].*taroct,'r--');
   hold off
   title(sprintf('%s %s snr: %.2f',cellid,thispass,snrpass),'Interpreter','none');

   subplot(3,activecount.*2,activecount*4+aidx*2-1);
   dstrf=strfest-strfpass;
   stplot(dstrf,StimParam.lfreq,StimParam.basep,1,StimParam.octaves,[-mm mm]);
   hold on
   aa=axis;
   plot(aa(1:2),[1 1].*taroct,'r--');
   hold off
   title(sprintf('Tar: %.0f',tarfreq));
   
   
   subplot(3,activecount.*2,aidx*2);
   stplot(strfactc,StimParam.lfreq,StimParam.basep,1,StimParam.octaves,[-1 1]);
   hold on
   aa=axis;
   plot(aa(1:2),[1 1].*maxoct/2,'r--');
   hold off
   colorbar
   
   subplot(3,activecount.*2,activecount.*2+aidx*2);
   stplot(strfpasc.*results.strfgain(aidx),StimParam.lfreq,StimParam.basep,1,StimParam.octaves,[-1 1]);
   hold on
   aa=axis;
   plot(aa(1:2),[1 1].*maxoct/2,'r--');
   hold off
   colorbar
   title('gain norm passive');
   
   subplot(3,activecount.*2,activecount.*4+aidx*2);
   stplot(strfactc-strfpasc,StimParam.lfreq,StimParam.basep,1,StimParam.octaves,[-1 1]);
   hold on
   aa=axis;
   plot(aa(1:2),[1 1].*maxoct/2,'r--');
   hold off
   colorbar
   
   results.tardist(aidx)=taroct-ybf;
   tarbin=taroct./StimParam.octaves.*size(strfpass,1);
   tarbin=floor(tarbin):ceil(tarbin);
   if min(tarbin)>size(strfpass,1)
      tarbin=size(strfpass,1);
   elseif max(tarbin)<1
      tarbin=1;
   else
      tarbin=tarbin(tarbin>0 & tarbin<=size(strfpass,1));
   end
   
   results.tardiff(aidx)=mean(mean(dstrf(tarbin,2:4)))./...
      max(max(abs(strfpass(tarbin,2:4))));
   
   % active, passive, gainnorm passive
   results.strfc=cat(3,strfactc,strfpasc, strfpasc.*results.gain(aidx));
   
   %results.strfsnr=min([snrpass snr]);
   results.strfsnr=[snr snrpass];
   
   % save strf figure is options.savefigures = True
   if options.savefigures
       saveas(gcf, [options.figurepath cellid '_' passfile '_' actfile '_STRF.pdf']);
       %close(gcf);
   end
   
   
end


   
