% DS analysis to compare HR, FAR and DI between "easy" and "hard" sessions 
% of the Tone-in-TORC behavior

clear all
narf_set_path
baphy_set_path


dbopen;

animal = 'Babybell';
cellprefix='bbl';
% animal = 'Tartufo';
% cellprefix='TAR';
% animal = 'Boleto';
% cellprefix='BOL';
% animal = 'Chiodino';
% cellprefix='CDN';
% animal = 'Turkeytail';
% cellprefix='TTL';

sql=['SELECT gDataRaw.* FROM gDataRaw',...
     ' WHERE runclass="PTD"',...
     ' AND cellid like "' cellprefix '%"',...
     ' AND not(bad)'];

rawfiles=mysql(sql);
filecount=length(rawfiles);

parmfiles=cell(filecount,1);
for ii=1:filecount,
    parmfiles{ii}=[rawfiles(ii).resppath rawfiles(ii).parmfile];
end

HRall = [];
FARall = [];
DIall = [];
TargetSNRall = [];
masterid = []

% following block of code from di_plot.m function to extract masterid


  
for ii = 10:length(parmfiles);
    %LoadMfile(parmfiles{ii})
    %exptparams=replicate_behavior_analysis(parmfiles{ii}, 1);
    
 
    rawid=rawfiles(ii).id;
    
    [parm,perf] = dbReadData(rawid);
    
    %       if ~isfield(perf,'EarlyTrial'),
    %           exptparams = replicate_behavior_analysis(parmfiles{ii}, 1);
    %           close
    %           [parm,perf] = dbReadData(rawid);
    %       end
    
    % I do not want to include short sessions
    if rawfiles(ii).corrtrials > 15,
        [DI, HR, FAR, details]=di_nolick(parmfiles{ii});
        disp(parmfiles{ii})
        TargetSNR = parm.Trial_RelativeTarRefdB(1);
        
        masterid = [masterid rawfiles(ii).masterid];
        HRall = [HRall HR*100];
        FARall = [FARall FAR*100];
        DIall = [DIall DI*100];
        TargetSNRall = [TargetSNRall TargetSNR];
    else
        disp(['skipping: ' parmfiles{ii}]);
    end
    
    
end


dayN=zeros(size(HRall));
umasterid=unique(masterid);
HR_day=zeros(size(umasterid));
DI_day=zeros(size(umasterid));
FAR_day=zeros(size(umasterid));
TarSNRall_day=zeros(size(umasterid));


for ii=1:length(umasterid);
    ff=find(masterid==umasterid(ii));
    DI_day(ii)=nanmean(DIall(ff));
    HR_day(ii)=nanmean(HRall(ff));
    FAR_day(ii)=nanmean(FARall(ff));
    TarSNRall_day(ii)=nanmean(TargetSNRall(ff));
    dayN(ff)=ii;
end


FARallsm = gsmooth(FARall,2);
HRallsm = gsmooth(HRall,2);
DIallsm = gsmooth(DIall,2);


figure;
axis([0 length(parmfiles) -20 100]);
plot(HRallsm, 'r')
hold on
plot(FARallsm, 'g')
plot(DIallsm, 'k')
plot(TargetSNRall, '.')
title([animal ' behavioral analysis per session']);
xlabel('Number of sessions');
ylabel('DI HR FAR TargetSNR');
legend('HR','FAR','DI','TargetSNR')
hold off


figure;
plot(HR_day, 'r')
hold on
plot(FAR_day, 'g')
plot(DI_day, 'k')
plot(TarSNRall_day)
title([animal ' behavioral analysis average per day']);
xlabel('Training day')
ylabel('DI HR FAR TargetSNR')
legend('HR', 'FAR', 'DI', 'TargetSNR')
hold off






% %   Code to compare easy vs hard task
% HR_easy = [];
% HR_hard = [];
% FAR_easy = [];
% FAR_hard = [];
% DI_easy = [];
% DI_hard = [];
% 
% 
%   for jj = 1:length(HRall),
%       
%       thresh = -15;
%       
%       if TargetSNRall(jj) <= thresh,
%           HR_hard = [HR_hard HRall(jj)];
%           FAR_hard = [FAR_hard FARall(jj)];
%           DI_hard = [DI_hard DIall(jj)];
%           
%       elseif TargetSNRall(jj) <= 0,
%           HR_easy = [HR_easy HRall(jj)];
%           FAR_easy = [FAR_easy FARall(jj)];
%           DI_easy = [DI_easy DIall(jj)];
%       end
%      
%       
%   end
%  
%   allVec = nan(8,length(parmfiles));
%     
%   allVec(1, 1:length(HR_easy)) = HR_easy;
%   allVec(2, 1:length(HR_hard)) = HR_hard;
%   allVec(3, 1:length(FAR_easy)) = FAR_easy;
%   allVec(4, 1:length(FAR_hard)) = FAR_hard;
%   allVec(5, 1:length(DI_easy)) = DI_easy;
%   allVec(6, 1:length(DI_hard)) = DI_hard;
%   
%   FigMatrix = [round(nanmean(allVec(1,:))), round(nanmean(allVec(2,:)));...
%       round(nanmean(allVec(3,:))), round(nanmean(allVec(4,:)));...
%       round(nanmean(allVec(5,:))), round(nanmean(allVec(6,:)))];
%   
%   
% %   % ttest assuming equal variances
% %   [p_HR, meanDiff_HR, stats_HR] = randttest(HR_easy, HR_hard);
% %   [p_FAR, meanDiff_FAR, stats_FAR] = randttest(FAR_easy, FAR_hard);
% %   [p_DI, meanDiff_DI, stats_DI] = randttest(DI_easy, DI_hard);
% %   
%   meanvalues=zeros(1,6);
%   [meanvalues(1), HR_easy_err] = jackmeanerr(HR_easy(~isnan(HR_easy))',20);
%   [meanvalues(2), HR_hard_err] = jackmeanerr(HR_hard(~isnan(HR_hard))',20);
%   [meanvalues(3), FAR_easy_err] = jackmeanerr(FAR_easy(~isnan(HR_easy))',20)
%   [meanvalues(4), FAR_hard_err] = jackmeanerr(FAR_hard(~isnan(FAR_hard))',20);
%   [meanvalues(5), DI_easy_err] = jackmeanerr(DI_easy(~isnan(DI_easy))',20);
%   [meanvalues(6), DI_hard_err] = jackmeanerr(DI_hard(~isnan(DI_hard))',20);
%   errs=[HR_easy_err HR_hard_err FAR_easy_err FAR_hard_err DI_easy_err DI_hard_err];
%   
%   figure
%   errorbar([0.855 1.145 1.855 2.145 2.855 3.145],meanvalues,errs,'k.');
%   hold on
%   h = bar(FigMatrix);
%   title(['TTL performance Tone-in-TORC task - minrawid: ' num2str(minrawid) ' Threshold: ' num2str(thresh)]);
%   set(h(1), 'Facecolor', [0,0.8,0]);
%   set(h(2), 'Facecolor', [1,0,0.3]);
%   set(gca, 'XTick', 1:3);
%   set(gca, 'XTickLabel', {'HR', 'FAR', 'DI'});
%   ylim([0 100]);
%   l = cell(1,2);
%   l{1} = 'Easy'; l{2} = 'Hard';
%   legend(h,l, 'Location', 'NorthWest');
%   
% 
%   
%   