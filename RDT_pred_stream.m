% function [pred1,pred2,predboth,resp,meta]=RDT_pred_stream(cellid,batchid,modelname);
%
% cellid:     e.g., 'chn016c-d1'  or cellid='btn144a-a1'
% modelname:  e.g., 'fbSNS18ch100_lognn_wc03_fir15_fit05a'
%             modelname='fbSNS18ch100_wc03_fir15_fit05a'
% batchid=269:  A1 passive
% batchid=273:  PEG passive
%
% returns:  pred1 - predicted response to stream 1 only (includes
%                   both target and reference phase of each trial)
%           pred2 - predicted response to stream 2 only. Zero if
%                   single-stream trial
%           resp  - Actual response (averaged across all repetitions for that
%                   stream)
%			meta  - Information regarding the model fit
%		    r0    - Baseline firing rate (full prediction should be
%		    		pred1+pred2+r0)

function [pred1,pred2,predboth,resp,meta]=RDT_pred_stream(cellid,batchid,modelname)

dbopen
sql=['SELECT * FROM NarfResults WHERE cellid="',cellid,'"',...
     ' AND batch=',num2str(batchid),...
     ' AND modelname="',modelname,'"'];

ndata=mysql(sql);
if isempty(ndata),
    disp('NarfResults entry not found');
    return
end
modelpath=char(ndata(1).modelpath);

[pred1,pred2,predboth,resp,meta]=RDT_pred_stream_from_modelpath(modelpath);
