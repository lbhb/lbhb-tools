baphy_set_path
dbopen;

minrawid=113670;

sql=['SELECT gDataRaw.* FROM gDataRaw INNER JOIN gData',...
     ' ON gDataRaw.id=gData.rawid AND gData.name="Trial_LightPulseDuration"',...
     ' WHERE runclass="PTD"',...
     ' AND behavior="active"',...
     ' AND cellid like "TTL%"',...
     ' AND not(bad)',...
     ' AND gDataRaw.id>',num2str(minrawid),...
     ' AND gData.value>0'];

rawfiles=mysql(sql);
filecount=length(rawfiles);

parmfiles=cell(filecount,1);
for ii=1:filecount,
    parmfiles{ii}=[rawfiles(ii).resppath rawfiles(ii).parmfile];
end

%parmfiles={'/auto/data/daq/Turkeytail/TTL053/TTL053a05_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL051/TTL051b07_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL049/TTL049f02_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL049/TTL049b04_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL049/TTL049f02_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL049/TTL049a02_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL048/TTL048d04_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL048/TTL048c04_a_PTD.m',...
%           '/auto/data/daq/Turkeytail/TTL057/TTL057a11_a_PTD.m',...
%          };

di=zeros(length(parmfiles),2);
hr=zeros(length(parmfiles),2);
fa=zeros(length(parmfiles),2);
early=zeros(length(parmfiles),2);
rt=zeros(length(parmfiles),2);
srt=zeros(length(parmfiles),2);
Nrt=zeros(length(parmfiles),2);
INCLUDE_EARLY_TRIALS=1;
tc=zeros(length(parmfiles),1);

for ii=1:length(parmfiles),
   disp(parmfiles{ii});
   [DI, HR, FAR, details]=di_nolick(parmfiles{ii});
   hr(ii,:)=details.oHR;
   fa(ii,:)=details.oFAR;
   di(ii,:)=details.oDI;
   tc(ii)=details.trialcount;
end

gg=find(~isnan(hr(:,1)) & ~isnan(hr(:,2)) & ...
   hr(:,1)>0.5 & hr(:,2)>0.5 & tc>40);
tcw=tc(gg);
tcw=tcw./sum(tcw).*length(gg);

figure
subplot(1,3,1);
plot(hr(gg,1),hr(gg,2),'.');
hold on
plot([0.5 1.05],[0.5 1.05],'k--');
hold off
axis square tight
title('hit rate');
xlabel(sprintf('light off (%.2f)',median(hr(gg,1))));
ylabel(sprintf('light on (%.2f)',median(hr(gg,2))));

subplot(1,3,2);
plot(fa(gg,1),fa(gg,2),'.');
hold on
plot([0.0 0.5],[0.0 0.5],'k--');
hold off
axis square tight
title('fa rate');
xlabel(sprintf('light off (%.2f)',median(fa(gg,1))));
ylabel(sprintf('light on (%.2f)',median(fa(gg,2))));

subplot(1,3,3);
plot(di(gg,1),di(gg,2),'.');
hold on
plot([0.5 1],[0.5 1],'k--');
hold off
axis square tight
title('di');
xlabel(sprintf('light off (%.2f)',median(di(gg,1))));
ylabel(sprintf('light on (%.2f)',median(di(gg,2))));



return


for ii=1:length(parmfiles),
   
    clear exptparams globalparams exptevents
    LoadMFile(parmfiles{ii});
    if ~isfield(exptparams.Performance(1),'OptoLight'),
        exptparams=replicate_behavior_analysis(parmfiles{ii});
    end
    
    tt=1:exptparams.TotalTrials;
    
    a=[cat(1,exptparams.Performance(tt).OptoLight) cat(1,exptparams.Performance(tt).EarlyTrial)...
       cat(1,exptparams.Performance(tt).FalseAlarm)...
       cat(1,exptparams.Performance(tt).Hit) cat(1,exptparams.Performance(tt).Miss)];
    trt=cat(1,exptparams.Performance(tt).ReactionTime);
    
    offTr=find(a(:,1)==0);
    onTr=find(a(:,1)==1);
    [length(offTr) sum(a(offTr,2:end)) nanmean(trt(offTr))]
    [length(onTr) sum(a(onTr,2:end)) nanmean(trt(onTr))]
    
    if INCLUDE_EARLY_TRIALS,
        hr(ii,:)=[sum(a(offTr,4))/length(offTr) sum(a(onTr,4))/length(onTr)];
        fa(ii,:)=[sum(a(offTr,3))/length(offTr) sum(a(onTr,3))/length(onTr)];
        early(ii,:)=[sum(a(offTr,2))/length(offTr) sum(a(onTr,2))/length(onTr)];
        bb=a(:,2)+a(:,3);
        early(ii,:)=[sum(bb(offTr))/length(offTr) sum(bb(onTr))/length(onTr)];
    else
        vton=intersect(onTr,find(a(:,2)==0));
        vtoff=intersect(ofTr,find(a(:,2)==0));
        hr(ii,:)=[sum(a(vton,4))/length(vton) sum(a(vtoff,4))/length(vtoff)];
        fa(ii,:)=[sum(a(vton,3))/length(vton) sum(a(vtoff,3))/length(vtoff)];
        early(ii,:)=[sum(a(offTr,2))/length(offTr) sum(a(onTr,2))/length(onTr)];
    end
    rt(ii,:)=[nanmedian(trt(offTr)) nanmedian(trt(onTr))];
    srt(ii,:)=[nanstd(trt(offTr)) nanstd(trt(onTr))];
    Nrt(ii,:)=[sum(~isnan(trt(offTr))) sum(~isnan(trt(onTr)))];
    [fa rt srt Nrt ]
end

figure
subplot(1,3,1);
plot(hr(:,1),hr(:,2),'.');
hold on
plot([0.0 0.7],[0.0 0.7],'k--');
hold off
axis square tight
title('hit rate');
xlabel(sprintf('light off (%.2f)',mean(hr(:,1))));
ylabel(sprintf('light on (%.2f)',mean(hr(:,2))));

subplot(1,3,2);
plot(fa(:,1),fa(:,2),'.');
hold on
plot([0.0 0.7],[0.0 0.7],'k--');
hold off
axis square tight
title('fa rate');
xlabel(sprintf('light off (%.2f)',mean(fa(:,1))));
ylabel(sprintf('light on (%.2f)',mean(fa(:,2))));

subplot(1,3,3);
plot(early(:,1),early(:,2),'.');
hold on
plot([0.0 0.7],[0.0 0.7],'k--');
hold off
axis square tight
title('early rate');
xlabel(sprintf('light off (%.2f)',mean(early(:,1))));
ylabel(sprintf('light on (%.2f)',mean(early(:,2))));




