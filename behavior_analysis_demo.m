
runclass='TSP';

dbopen;

% figure out numerical code for runclass
sql=['SELECT * FROM gRunClass WHERE name="',runclass,'"'];
rcdata=mysql(sql);
runclassid=rcdata.id;

% find all RawData files that match runclass, produces a parameter
% called Trial_CatchIdxFreq and aren't flagged as somehow bad
sql=['SELECT gDataRaw.*,lower(left(gDataRaw.cellid,3)) as animal',...
     'FROM gDataRaw,gData',...
     ' WHERE gDataRaw.id=gData.rawid',...
     ' AND gDataRaw.runclassid=',num2str(runclassid),...
     ' AND gData.name="Trial_CatchIdxFreq"',...
     ' AND not(gDataRaw.bad)',...
     ' AND not(gDataRaw.cellid like "tst%")',...
     ' AND gDataRaw.behavior="active"',...
     ' ORDER BY gDataRaw.id'];
gdata=mysql(sql);

% set up outputs to group by animals
animals=unique({gdata.animal});
animalcount=length(animals);
animalidx=zeros(size(gdata));
di=zeros(length(gdata),4);

% go through each file
for ii=1:length(gdata),
    
    [baphyparms,baphyperf]=dbReadData(gdata(ii).id);
    TarIdxFreq=baphyparms.Trial_TargetIdxFreq;
    CatchIdxFreq=baphyparms.Trial_CatchIdxFreq;
    
    % various checks to see if file is valid
    if isempty(baphyparms),
        fprintf('%30s: Tar SNR(Freq): not parm/perf in gData\n',...
                gdata(ii).parmfile);
        bad=1;
    elseif isempty(baphyperf) || ...
            (isfield(baphyperf,'Performance') && isempty(baphyperf.Performance)),
        fprintf('%30s: no performance data. Passive?\n',...
                gdata(ii).parmfile);
        bad=1;
        
    elseif length(TarIdxFreq)==3 && TarIdxFreq(3)==0 && ...
            length(CatchIdxFreq)==3 && sum(CatchIdxFreq(1:2))==0,
        fprintf('%30s: Tar SNR(Freq): %d(%.2f) / %d(%.2f)  Catch: %d(%.2f)\n',...
                gdata(ii).parmfile,...
                baphyparms.Trial_RelativeTarRefdB(1),TarIdxFreq(1),...
                baphyparms.Trial_RelativeTarRefdB(2),TarIdxFreq(2),...
                baphyparms.Trial_RelativeTarRefdB(3), ...
                CatchIdxFreq(3));
        bad=0;
    else
        fprintf('%30s: weird situation??\n',gdata(ii).parmfile);
        bad=1;
    end
    
    % if good, pull out di for various conditions
    if ~bad,
        di(ii,1)=baphyperf.DiscriminationIndex;
        di(ii,2:3)=baphyperf.uDiscriminationIndex(1:2);
        if isfield(baphyperf,'cDiscriminationIndex'),
            di(ii,4)=baphyperf.cDiscriminationIndex;
        else
            di(ii,4)=nan;
        end
        animalidx(ii)=find(strcmp(gdata(ii).animal,animals));
    end
end

keepidx=find(di(:,1)>0);
animalidx=animalidx(keepidx);
di=di(keepidx,:);

figure;
for aidx=1:animalcount,
    useidx=find(animalidx==aidx & ~isnan(di(:,4)) & di(:,2)>60 );
    
    subplot(animalcount,2,aidx*2-1);
    hist(di(useidx,2:4));
    title(animals{aidx});
    
    subplot(animalcount,3,aidx*3);
    mm=zeros(3,1);ee=zeros(3,1);
    [mm(1),ee(1)]=jackmeanerr(di(useidx,2),20,0);
    [mm(2),ee(2)]=jackmeanerr(di(useidx,3),20,0);
    [mm(3),ee(3)]=jackmeanerr(di(useidx,4),20,0);
    [mean(di(useidx,2:4));median(di(useidx,2:4))]
    errorbar(mm,ee,'.');
    axis([0 4 0 100]);
    hold on
    plot([0 4],[50 50],'k--');
    bar(mm);
    hold off
    xlabel('Ecom - Hcom - Huncom');
    title(sprintf('mDI: %.1f %.1f %.1f (%d)',mm,length(useidx)));
end
