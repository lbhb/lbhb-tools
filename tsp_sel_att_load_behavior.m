

dbopen;

sql=['SELECT gDataRaw.*,lower(left(gDataRaw.cellid,3)) as animal FROM gDataRaw,gData',...
     ' WHERE gDataRaw.id=gData.rawid',...
     ' AND gDataRaw.runclassid=103',...
     ' AND gData.name="Trial_CatchIdxFreq"',...
     ' AND not(gDataRaw.bad)',...
     ' AND not(gDataRaw.cellid like "tst%")',...
     ' AND gDataRaw.behavior="active"',...
     ' ORDER BY gDataRaw.id'];
gdata=mysql(sql);

animals=unique({gdata.animal});
animalcount=length(animals);
animalidx=zeros(size(gdata));
di=zeros(length(gdata),4);
hr=zeros(length(gdata),4);
fa=zeros(length(gdata),4);
tarchannel=zeros(size(gdata));
training=cat(1,gdata.training);
for ii=1:length(gdata),
    
    [baphyparms,baphyperf]=dbReadData(gdata(ii).id);
    TarIdxFreq=baphyparms.Trial_TargetIdxFreq;
    CatchIdxFreq=baphyparms.Trial_CatchIdxFreq;
    if isempty(baphyparms),
        fprintf('%30s: Tar SNR(Freq): not parm/perf in gData\n',...
                gdata(ii).parmfile);
        bad=1;
    elseif isempty(baphyperf) || length(fields(baphyperf))<=1,
        fprintf('%30s: no baphyperf --passive?\n',gdata(ii).parmfile);
        bad=1;
    elseif length(TarIdxFreq)>=3 && sum(TarIdxFreq(3)>0)<=2 && ...
            length(CatchIdxFreq)>=3 && sum(CatchIdxFreq>0)>0,
        fprintf('%30s: Tar SNR(Freq): %d(%.2f) / %d(%.2f)  Catch: %d(%.2f)\n',...
                gdata(ii).parmfile,...
                baphyparms.Trial_RelativeTarRefdB(1),TarIdxFreq(1),...
                baphyparms.Trial_RelativeTarRefdB(2),TarIdxFreq(2),...
                baphyparms.Trial_RelativeTarRefdB(3), ...
                CatchIdxFreq(3));
        bad=0;
    else
        fprintf('%30s: weird situation??\n',gdata(ii).parmfile);
        bad=1;
    end
    if ~bad && ~isfield(baphyperf,'cFARate'),
        %keyboard
        parmfile=[gdata(ii).resppath gdata(ii).parmfile];
        if exist(parmfile,'file'),
           try
            replicate_behavior_analysis(parmfile,1);
            [baphyparms,baphyperf]=dbReadData(gdata(ii).id);
           catch 
              disp('error recalc');
           end
        end
    end
    
    if ~bad,
        tc=baphyparms.Trial_TargetChannel;
        condidx=min(find(TarIdxFreq==max(TarIdxFreq)));
        tf=TarIdxFreq;
        tf(condidx)=0;
        probeidx=min(find(tf==max(tf)));
        tarchannel(ii)=tc(condidx);

        di(ii,1)=baphyperf.DiscriminationIndex;
        di(ii,2:3)=baphyperf.uDiscriminationIndex([condidx probeidx]);
        hr(ii,1)=baphyperf.HitRate;
        hr(ii,2:3)=baphyperf.uHitRate([condidx probeidx]);
        fa(ii,1)=baphyperf.FalseAlarmRate;
        if isfield(baphyperf,'cDiscriminationIndex'),
            di(ii,4)=baphyperf.cDiscriminationIndex;
        else
            di(ii,4)=nan;
        end
        if isfield(baphyperf,'cHitRate'),
            hr(ii,4)=baphyperf.cHitRate;
            fa(ii,4)=baphyperf.cFARate;
        else
            hr(ii,4)=nan;
            fa(ii,4)=nan;
        end
        animalidx(ii)=find(strcmp(gdata(ii).animal,animals));
    end
end

keepidx=find(di(:,1)>0);
animalidx=animalidx(keepidx);
di=di(keepidx,:);
hr=hr(keepidx,:);
fa=fa(keepidx,:);
tarchannel=tarchannel(keepidx,:);
training=training(keepidx);

