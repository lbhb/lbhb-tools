% Charlie Heller
% 2/1/2017

% This function creates a linear "neural receptive field" for a given 
% neuron for a given stimulus based on the network activity in neurons
% recorded simultaneously

% Inputs:
    % network_activity: spikes array of all other neurons (fromat: bins X
    % stim = 1 X reps X cells)
    % neural_response: vector of spikes for given neuron (format: bins X
    % stim = 1 X reps)
% Output:
    % Linear filter, h, with dimensions: 1 X time. So, h assigns a weight
    % to every other neuron in the population at each time point

function [h, pinv_call] = linear_NRF(network_activity, neural_response)

cellcount = size(network_activity, 4);
repcount = size(network_activity, 3);
bincount = size(network_activity, 1);

network_activity = reshape(network_activity, [bincount*repcount cellcount]);
neural_response = reshape(neural_response, [bincount*repcount 1]);

if size(network_activity,1) ~= size(neural_response,1)
    error('dimension mismatch between network and neuron')
end

% Make filter

Csr = network_activity'*neural_response/size(network_activity,1);
Css = network_activity'*network_activity/size(network_activity,1);

if rank(Css) < cellcount
    h = pinv(Css)'*Csr;
    pinv_call = 1;
    %warning('Too close to unity matrix to use inverse. Implementing pinv.')
else
    h = inv(Css)'*Csr;
    pinv_call = 0;
end
