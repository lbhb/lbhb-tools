% Charlie Heller
% 3/1/17

% Neural receptive field model (reverse correlation between each neuron
% and the population firing rate)

% Can specify if you'd like the model to be fit using raw data, principle 
% components (PCA analysis), or "factors" (factor analysis)

% Inputs
%   options (struct): contains info about population (r_params) as well as
%                     a field called data containing neural population
%                     data. In addition, contains field called method to
%                     denote whether raw data, PCA, or FA should be used.


% Output
%   fit (struct): Will pass back options struct with an additional field
%                  called output, containing the model fit data 
%                  (array of predicted spikes), and an array containing
%                  model fit evaluation (covariance with true response 
%                  matrix).


function [fit] = model_fit(options)

fit = options;

fit.output = struct;     % To hold output variables

bincount = fit.bincount;
repcount = fit.repcount;
stimcount = fit.stimcount;
cellcount = fit.cellcount;
spont_evoked = fit.prestimonly;
smoothpcs = fit.smoothpcs;
sigmapcs = fit.sigmapcs;
Mse = options.Mse;



if spont_evoked   % If fitting to spont data only
    r_diff = fit.r;
    r = fit.r;  
else  % If evoked, we want to try to predict intrinsic dynamics (r - r0)
    r_diff = fit.r_diff;
    r = fit.r_diff;  
end
    
    
valSetSize = (repcount - floor(repcount*0.99));

% ========================== Network model ===============================

if strcmp(options.method, 'noDimReduce')
    
    % Create predicted responses
    sample_h = zeros(stimcount, repcount, cellcount, cellcount - 1);
    pred_resp = zeros(bincount, stimcount, repcount, cellcount);
    for kk = 1:stimcount
        for jj = 1:repcount
            
            if valSetSize > 1
                if (jj+valSetSize-1) > repcount
                    validation = [jj:repcount, 1:((valSetSize-1)-(repcount-jj))];            
                else
                    validation = jj:(jj+valSetSize-1);
                end
            else
                validation = jj; 
            end
            window=setdiff(1:repcount,validation);
            
            for ii = 1:cellcount
                
                % Create network activity matrix
                sample_act = r_diff(:, kk, window, :);
                sample_act(:,:,:,ii) = [];
                
                % Create response vector
                sample_resp = r_diff(:,kk,window, :);
                sample_resp = sample_resp(:,:,:,ii);
                
                % Create filter
                [sample_h(kk, jj, ii, :), pinv_call] = linear_NRF(sample_act,sample_resp);  
                
                % Gather network activity in remainder of data set
                stim = r_diff(:,kk,validation,:);
                stim(:,:,:,ii) = [];
                stim = reshape(stim, [bincount*(length(validation)) cellcount-1]);
                
                % Predict responses 
                pred_resp(:, kk, jj, ii) = squeeze(sample_h(kk,jj,ii,:))'*stim';
            end 
        end 
    end
    
    % Compare responses to predicted responses
    cov = cross_cov(pred_resp, r, Mse);
    
    % Save outputs
    fit.output.resp = pred_resp;
    fit.output.cov = cov;
    fit.output.weights = sample_h;
    
% ======================== For PCA =======================================
elseif strcmp(options.method, 'PCA')
    
    display('Fitting with PCA')
    tic;
    % Compute principle components
    pcCount = fit.nPCs;
    U = zeros(repcount*bincount*stimcount, pcCount, cellcount);
    S = zeros(pcCount, pcCount, cellcount);
    V = zeros(cellcount-1, pcCount, cellcount);
    for ii = 1:cellcount
        if smoothpcs
            r_pc = reshape(r_diff, bincount*stimcount*repcount, cellcount);
            r_pc = gsmooth(r_pc, sigmapcs);
            r_pc = reshape(r_pc, bincount, stimcount, repcount, cellcount);
            r_pc(:,:,:,ii) = [];
            [U(:,:,ii) S(:,:,ii) V(:,:,ii)] = svds(reshape(r_pc, [bincount*repcount*stimcount cellcount-1]), pcCount);
        else
            r_pc = r_diff;
            r_pc(:,:,:,ii) = [];
            [U(:,:,ii) S(:,:,ii) V(:,:,ii)] = svds(reshape(r_pc, [bincount*repcount*stimcount cellcount-1]), pcCount);
        end
    end
    
  
    pred_resp_net = zeros(bincount, stimcount, repcount, cellcount, pcCount);
    net_cov = zeros(stimcount, repcount, cellcount, pcCount);
    cov_long = zeros(cellcount, pcCount);
    % Compute model
    for nPCs = 1:pcCount
        for kk = 1:stimcount
            for jj = 1:repcount

                if valSetSize > 1
                    if (jj+valSetSize-1) > repcount
                        validation = [jj:repcount, 1:((valSetSize-1)-(repcount-jj))];            
                    else
                        validation = jj:(jj+valSetSize-1);
                    end
                else
                    validation = jj; 
                end
                window=setdiff(1:repcount,validation);
                
                for ii = 1:cellcount                   
                    % Create response vector
                    sample_resp = r_diff(:,kk,window,:);
                    sample_resp = sample_resp(:,:,:,ii);
                    sample_resp = squeeze(sample_resp);
                    
                    % Get test activity
                    sample_act = r_diff(:, kk, window, :);
                    sample_act(:,:,:,ii) = [];
                    
                    %Perform PCA on sample set
                    [Ut St Vt] = svds(reshape(sample_act, bincount*(length(window)), cellcount-1), nPCs);
                    
                    %{
                    % Create filter using pc input
                    %pc = U(:,:,ii)*S(:,:,ii);
                    
                    % For individual pcs
                    filter_input = reshape(pc(:,nPCs), [bincount stimcount repcount-1 1]);
                    %fit_input=filter_input(:,:,window);
                    pca_h = linear_NRF(filter_input, sample_resp);

                    % For net pcs
                    filter_input_net = reshape(pc(:,1:nPCs), [bincount stimcount repcount-1 nPCs]);
                    %fit_input=filter_input_net(:,:,window,:);
                    pca_h_net = linear_NRF(filter_input_net, sample_resp);
                    %}
                    
                    pc = Ut*St*Vt';
                    pc = reshape(pc, [bincount stimcount length(window) cellcount-1]);
                    pca_h_net = linear_NRF(pc, sample_resp);
                    
                    stim = r_diff(:,kk,validation,:);
                    stim(:,:,:,ii) = [];
                    stim = reshape(stim, [bincount*(length(validation)) cellcount-1]);
                   
                    %net_stim = squeeze(filter_input_net(:,:,validation,1:nPCs));
                    pred_resp_net(:, kk, jj, ii, nPCs) = pca_h_net'*stim';   %net_stim';
                end
            end
        end
       
        net_cov(:,:,:,nPCs) = cross_cov(pred_resp_net(:,:,:,:,nPCs), r, Mse);
        % String together all reps/time and compute single covariance for
        % each neurons per pc
        prn = reshape(squeeze(pred_resp_net(:,:,:,:,nPCs)), bincount*stimcount*repcount, cellcount);
        trl = reshape(squeeze(r), bincount*stimcount*repcount, cellcount);
        cov_long(:, nPCs) = cross_cov(prn, trl, Mse);
    end

    fit.output.resp_n = pred_resp_net;
    fit.output.cov_n = net_cov;
    fit.output.cov_overall = cov_long;
    fit.output.U = U;
    fit.output.S = S;
    fit.output.V = V;
    t = toc;
    display(['Time to finish pca with ', num2str(pcCount), ' PCs: ', num2str(t)])
    
    
    
% ============================ Factor Analysis ============================
elseif strcmp(options.method, 'FA')
display('Calculating using FA')

    tic;
    % Compute principle components
    pcCount = fit.nFacs;
    lambda = zeros(cellcount-1, pcCount, cellcount, pcCount);
    T = zeros(pcCount, pcCount, cellcount, pcCount);
    F = zeros(repcount*bincount*stimcount, pcCount, cellcount, pcCount);
    for ii = 1:cellcount
        r_pc = r_diff;
        r_pc(:,:,:,ii) = [];
        for jj = 1:pcCount
            [lambda(:,1:jj,ii,jj) psi T(1:jj,1:jj,ii,jj) stats F(:,1:jj,ii,jj)]...
                = factoran(reshape(r_pc, [bincount*repcount*stimcount ...
                cellcount-1]), jj,'rotate','none');   
        end
    end
    
    pred_resp_sing = zeros(bincount, stimcount, repcount, cellcount, pcCount);
    pred_resp_net = zeros(bincount, stimcount, repcount, cellcount, pcCount);
    single_cov = zeros(stimcount, repcount, cellcount, pcCount);
    net_cov = zeros(stimcount, repcount, cellcount, pcCount);
    
    % Compute model
    for nPCs = 1:pcCount
        for kk = 1:stimcount
            for jj = 1:repcount

                if valSetSize > 1
                    if (jj+valSetSize-1) > repcount
                        validation = [jj:repcount, 1:((valSetSize-1)-(repcount-jj))];            
                    else
                        validout = model_fit(input);ation = jj:(jj+valSetSize-1);
                    end
                else
                    validation = jj; 
                end
                window=setdiff(1:repcount,validation);

                for ii = 1:cellcount                   
                    % Create response vector
                    sample_resp = r_diff(:,kk,:,:);
                    sample_resp = sample_resp(:,:,:,ii);

                    % For individual pcs
                    filter_input = F(:,nPCs,ii,nPCs)*T(nPCs,nPCs,ii,nPCs)';
                    filter_input = reshape(filter_input, [bincount stimcount repcount 1]);
                    fa_h = linear_NRF(filter_input, sample_resp);

                    % For net pcs
                    filter_input_net = F(:,1:nPCs,ii,nPCs)*T(1:nPCs,1:nPCs,ii,nPCs)';
                    filter_input_net = reshape(filter_input_net, [bincount stimcount repcount nPCs]);
                    fa_h_net = linear_NRF(filter_input_net, sample_resp);
                    
                    % Gather network activity in remainder of data set
                    stim = r_diff(:,kk,validation,:);
                    stim(:,:,:,ii) = [];
                    stim = reshape(stim, [bincount*(length(validation)) cellcount-1]);
                    
                    % Create predicitions
                    pred_resp_sing(:, kk, jj, ii, nPCs) = fa_h'*filter_input(:,:,validation)';
                    net_stim = squeeze(filter_input_net(:,:,validation,1:nPCs));
                    pred_resp_net(:, kk, jj, ii, nPCs) = fa_h_net'*net_stim';                   
                end
            end
        end
    
        single_cov(:,:,:,nPCs) = cross_cov(pred_resp_sing(:,:,:,:,nPCs), r, Mse);
        net_cov(:,:,:,nPCs) = cross_cov(pred_resp_net(:,:,:,:,nPCs), r, Mse);
    end

    fit.output.resp_s = pred_resp_sing;
    fit.output.resp_n = pred_resp_net;
    fit.output.cov_s = single_cov;
    fit.output.cov_n = net_cov;
    fit.output.lambda = lambda;
    fit.output.F = F;
    fit.output.T = T;
    t = toc;
    display(['Time to finish FA with ', num2str(pcCount), ' factors: ', num2str(t)])      
end


end