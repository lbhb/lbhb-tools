% Charlie Heller
% 2/1/2017

% This function is used to load 64 channel array data from PPS_VOC
% experiments. It will only load the VOC data, and remove all other PPS
% stimuli


% Arguments

% load_options: struct containing the fields below
    % rawid: rawid number of recording location ex: 118698 
    % miniso: isolation confidence criterion for neurons ex: '>84'
    % includeprestim: logical (0 or 1) stating whether you want the prestimulus
    % data
    % onlyprestim: logical (0 or 1) stating if you only would like data from
    % pre stimulus period
    % combinestim: if you wish to combine data from all stimuli (for
    % example, might want this if only looking at spont period)
    % fs: sampling rate, will default to 20
    % pupil: logical (0 or 1) stating whether you want to load pupil data

% Output

% r: matrix of spikes: time X stim X rep X neurons. r will be normalized to
% (r = r-mean/std)
% r_diff = normalized r - r0, where r0 is the mean
% r_params: structure containing information about the responses:
    % prestim time 
    % duration time
    % poststim time
    % bincount
    % stimcount
    % repcount
    % cellcount
% p: if pupil = 1, matrix of pupil data: time X stim X rep


function [tr tr_diff r r_diff r_params cfd p] = array_64_VOC_load(options)

%% Assign local variables based on input load structure. Assign default
%  variables in case of missing fields

if ~isfield(options, 'rawid')
    error('must specify rawid')
else
    rawid = options.rawid;
end

if ~isfield(options, 'miniso')
    warning('Isolation set to >84')
    miniso = '>84';
else
    miniso = options.miniso;
end

if ~isfield(options, 'fs')
    fs = 20;
else
    fs = options.fs;
end

if ~isfield(options, 'includeprestim')
    warning('Loading all data, including prestim and evoked period')
    includeprestim = 1;
else 
    includeprestim = options.includeprestim;
end

if ~isfield(options, 'combinestim')
    combinestim = 0;
else
    combinestim = options.combinestim;
end

if ~isfield(options, 'onlyprestim')
    warning('Loading all data, including prestim and evoked period')
    onlyprestim = 0;
else
    onlyprestim = options.onlyprestim;
end

if ~isfield(options, 'pupil')
    warning('pupil data will not be loaded')
    pupil = 0;
else
    pupil = options.pupil;
end

%% Load paramters for rawid specified in options
cfd = dbgetscellfile('rawid', rawid, 'isolation', miniso);
runclass = [];

% find parameter file and spikefile
parmfile = [cfd(1).stimpath cfd(1).stimfile];
spikefile = [cfd(1).path cfd(1).respfile];

% get list of unique (non-repeated) cellids
[cellids ia] = unique({cfd.cellid});
% use them to get out non-repeated units
channels =cat(1,cfd(ia).channum);
units=cat(1,cfd(ia).unit);

%% Add to r_params
r_params.cellids = cellids;
r_params.channels = channels;
r_params.units = units;

% load parameters
LoadMFile(parmfile)

% Note the set up of: silence, stim, silence (given below in sec)
prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
duration = exptparams.TrialObject.ReferenceHandle.Duration;
poststim = exptparams.TrialObject.ReferenceHandle.PostStimSilence;

% specify options for loading spike raster
options = struct();
options.avg_stim=getparm(options,'avg_stim',0);
options.rasterfs = fs;
if isempty(runclass) && ~isempty(strfind(parmfile,'VOC_VOC')),
   options.tag_masks={'Reference1'};
elseif isempty(runclass),
   options.tag_masks={'Reference'};
else
   options.runclass=runclass;
end

r_options = options;
r_options.includeprestim = includeprestim;
r_options.channel=channels;
r_options.unit=units;
r_options.meansub=0;

%% Create r_params struct to be returned
r_params.prestim = prestim;
r_params.duration = duration;
r_params.poststim = poststim;

%% Load spike raster
[tr,tags,ttrialset] = loadsiteraster(spikefile, r_options);
   tr=tr*r_options.rasterfs;
r = tr;  % r is matrix of responses: time X reps X stim X cell
bincount = size(r,1);
repcount = size(r,2);
stimcount = size(r,3);
cellcount = size(r,4);

% Find relevant stimuli for analysis of VOC data
y = sum(r,2);
tf2 = find(~isnan(y(1,1,:,1)));
r = r(:,:,tf2,:);
stimcount = size(r,3);       % redefine stimcount
r = permute(r, [1 3 2 4]);

keepidx = find(~isnan(r(:,1,1,1)));  % delete nans from time dim
r = r(keepidx,:,:,:);
bincount = size(r,1);

tr = r;

% Normalize each cell to its mean: (rate - mean)/std
r = reshape(r, [bincount*stimcount*repcount cellcount]);
for ii = 1:cellcount
   r(:,ii) = r(:,ii) - mean(r(:,ii));
   r(:,ii) = r(:,ii)./std(r(:,ii));
end

r = reshape(r, [bincount*stimcount repcount cellcount]);
tr = reshape(tr, [bincount*stimcount repcount cellcount]);

% Average firing response (PSTH) of each cell and subtracting it from r
r0 = repmat(mean(r,2), [1 repcount]);
tr0 = repmat(mean(tr,2), [1 repcount]);
tr_diff = tr-tr0;
r_diff = r-r0;

r_diff = reshape(r_diff, [bincount stimcount repcount cellcount]); 
r = reshape(r, [bincount stimcount repcount cellcount]); 
tr = reshape(tr, [bincount stimcount repcount cellcount]); 
tr_diff = reshape(tr_diff, [bincount stimcount repcount cellcount]); 
if onlyprestim
    r = r(1:(prestim*fs), :,:,:);
    r_diff = r_diff(1:(prestim*fs), :,:,:);
    tr = tr(1:(prestim*fs),:,:,:);
    tr_diff = tr_diff(1:(prestim*fs), :,:,:);
end

if combinestim
    bincountComb = size(r,1);
    r = reshape(r, [bincountComb*stimcount 1 repcount cellcount]);
    r_diff  = reshape(r_diff, [bincountComb*stimcount 1 repcount cellcount]);
    tr_diff = reshape(tr_diff, [bincountComb*stimcount 1 repcount cellcount]);
    tr = reshape(tr, [bincountComb*stimcount 1 repcount cellcount]);
end

%% Load pupil data
if pupil == 1
    pr_options = options;
    pr_options.pupil = 1;
    pr_options.pupil_median=1;
    pr_options.pupil_mm=0;
    pr_options.pupil_offset=0.75; 
    pr_options.includeprestim = includeprestim;
    pr_options.fs = fs;
    [p,ptags,ptrialset] = loadevpraster(parmfile, pr_options);

    p = p(:,:,tf2);
    p = permute(p, [1 3 2]);
    kidx = find(~isnan(p(:,1,1)));
    p = p(kidx,:,:);
    p = reshape(p, [bincount*stimcount*repcount 1]);

    for ii = 1:cellcount
       p = p - mean(p);
       p = p./std(p);
    end
    p = reshape(p, [bincount stimcount repcount]);
else 
    p = 0;
end

if onlyprestim
    p = p(1:(prestim*fs), :,:);
end

if combinestim
    bincountComb = size(p,1);
    p = reshape(p, [bincountComb*stimcount 1 repcount]);
end
%% Add parameters to r_params

r_params.bincount = size(r,1);
r_params.stimcount = size(r,2);
r_params.repcount = size(r,3);
r_params.cellcount = size(r,4); 
r_params.r = r;
r_params.r_diff = r_diff;
