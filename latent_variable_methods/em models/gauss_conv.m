% Charlie Heller
% 2/24/17

function [K] = gauss_conv(x, pts, tau)  
    if tau < 50
        tau = 1;
    end
    g = exp(-0.5 * (x - length(x)/2).^2 / tau);
    %display('passed')
    g = g/max(g);
    K = conv(g, pts, 'same');
    K = K./max(K);
end