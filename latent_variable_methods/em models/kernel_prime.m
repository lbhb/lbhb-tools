
% Derivative of square diff gaussian kernel

% Specificy the param you'd like to differentiate w.r.t (tau or sigma)

function [K] = kernel_prime(x, y, tau, sigma, param)
n = length(x);
diff = zeros(n,n);
    for ii = 1:n
       for jj = 1:n
           diff(ii,jj) = ((x(ii) - y(jj)));
       end
    end

if strcmp(param, 'tau')
    
    K = real(sigma^2 * exp(-0.5 * diff^.2 / tau^2) * ((diff.^2)/ (tau^3))); %real((-diff.^2 * sigma)/(tau^3) * (exp(diff.^2 /(2*tau^2))));  %real(sigma * exp(-0.5 * diff^.2 / tau) * ((diff.^2)/ (tau^(3/2))));
    
elseif strcmp(param, 'sigma')
    
    K = 2*sigma*exp(- 0.5 * diff.^2 / tau^2); %sigma*exp(diff.^2 /(tau^2));  %sqrt(sigma)*exp(- 0.5 * diff.^2 / tau);
    
end