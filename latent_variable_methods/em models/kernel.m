% Charlie Heller
% 2/24/17

% Input two matrices and parameters (no. of depends on Kernel type -
% smooth, fast, etc.)

% Returns coavriance matrix


function [K] = kernel(x, y, tau, sigma)  
    
    diff = bsxfun(@minus,x,y').^2;
    
    K = sigma^2 * exp(-.5 * (1/tau^2) * diff);
    %K = K + 0.001*eye(size(K));
end