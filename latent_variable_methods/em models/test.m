% Create pseudo observation data
t = -5:.2:4.8;
n = length(t);

nNeur = 5;   % 5 psuedo neurons
obs = zeros(nNeur, n);
obs(1,:) = 10*sin(t);
obs(1,:) = (obs(1,:) - mean(obs(1,:)))/std(obs(1,:));
obs(2,:) = 1*sin(t);
obs(1,:) = (obs(2,:) - mean(obs(2,:)))/std(obs(2,:));
obs(3,:) = .5*sin(t);
obs(1,:) = (obs(3,:) - mean(obs(3,:)))/std(obs(3,:));
obs(4,:) = 10*sin(t);
obs(1,:) = (obs(4,:) - mean(obs(4,:)))/std(obs(4,:));
obs(5,:) = .01*sin(t);
obs(1,:) = (obs(5,:) - mean(obs(5,:)))/std(obs(5,:));

obs = obs + poissrnd(5, [nNeur, n]);


Tau = 0.35;
Kss = kernel(t,t,Tau);

% want to represent normal dist in relation to standard normals: 
% u + B*N(0,1) - B is matrix such that B*B' is the square root of our
% covarianve matrix Kss. Use Cholesky decomposition

L = chol(Kss, 'lower');

% Now, create gaussian priors
f_prior = L*normrnd(0, 1, [n, 1]);


figure; 
subplot(3,1,1)
imagesc(L)
subplot(3,1,2)
imagesc(Kss)
subplot(3,1,3)
plot(f_prior)
hold on
plot(mean(obs,1))



% Now iteritively update Tau and weights for each neuron to latent state

% Solve linear system
%f_prior = sin(t)';
X = linsolve(f_prior, obs');

% Get error between solution and actual

mod = f_prior;
mse = sum(sum((mod - mean(obs,1)').^2));
display(mse)
% Update Tau
