narf_set_path

%% Load continuous data

rawid = 118698;  % BOL005c VOC
%rawid = 118758;  % BOL006b VOC
% rawid = 116168;  % zee015h VOC

load_options = struct();
load_options.rawid = rawid;
load_options.miniso = '>84';
load_options.fs = 10;
load_options.pupil = 1;
[r, r_byt, trialset, trialset_byt, tags, params, p, p_bt, ptrialset, ptrialset_bt, ptags] = load_cont(load_options);
bincount = size(r,1);
repcount = size(r,2);
stimcount = size(r,3);
cellcount = size(r,4);


%% Get rid of Nans
ntrials = size(r_byt, 2);
r = reshape(r_byt, [bincount*ntrials, cellcount]);
r(isnan(r(:,1)),:) = []; 
p = reshape(p_bt, [bincount*ntrials, 1]);
p(isnan(p(:,1)),:) = [];
%% Reshape, normalize, smooth, and perform PCA

for ii = 1:cellcount
    r(:, ii) = (r(:,ii) - mean(r,2))./std(r(:,ii));
end
p = (p - mean(p))./std(p);

% Smooth r and p
sigma = 15;
r = gsmooth(r, sigma);
p = gsmooth(p, sigma);

%% Perform PCA

[U S V] = svd(r);
pcs = U*S;

pup_cov = zeros(cellcount,1);
for ii = 1:cellcount
    pup_cov(ii) = xcov(pcs(:,ii), p, 0, 'coeff');
end

ind = find(abs(pup_cov) == max(abs(pup_cov)));

figure;
subplot(2,2,1)
hold on
title(['traces smoothed with sigma = ' num2str(sigma)])
plot(p)
plot(pcs(:,ind), 'r')
axis('tight')
legend('pupil', ['pc ' num2str(ind)])
legend('boxoff')
subplot(2,2,3)
plot(abs(pup_cov))
ylabel('xcov')
xlabel('pc')
title('pupil vs. pcs')
subplot(2,2,4)
plot(diag(S))
title('pc weights')

