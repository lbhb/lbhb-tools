narf_set_path

% Charlie Heller
% 01/25/2017
% Load multi-electrode data, calculate the 'NRF' of individual neurons
% where NRF is the a filter describing the population activity

%% Load data 

% Load true data or pseudo data
true = 1; % 0 = synthetic data, 1 = real data, 2 = reduced version of real data

if true == 1
    
    rawid = 118698;  % BOL005c VOC
    %rawid = 118758;  % BOL006b VOC
    % rawid = 116168;  % zee015h VOC

    % Create load parameters structure for array_64_VOC_load
    load_options.rawid = rawid;
    load_options.miniso = '>84';
    load_options.includeprestim = 1;
    load_options.onlyprestim = 1;
    load_options.combinestim = 1;
    load_options.fs = 2;
    load_options.pupil = 1;

    % r is normalized, r_diff has mean subtracted and tr is true response
    [tr tr_diff r r_diff r_params cfd p] = array_64_VOC_load(load_options);
    display('loaded true data')
    bincount = r_params.bincount;
    stimcount = r_params.stimcount;
    repcount = r_params.repcount;
    cellcount = r_params.cellcount;
    r_params.fs = load_options.fs; 
    r_params.includeprestim = load_options.includeprestim;
    r_params.prestimonly = load_options.onlyprestim;
    
elseif true == 2
   
    rawid = 118698;  % BOL005c VOC
    %rawid = 118758;  % BOL006b VOC
    % rawid = 116168;  % zee015h VOC

    % Create load parameters structure for array_64_VOC_load
    load_options.rawid = rawid;
    load_options.miniso = '>84';
    load_options.includeprestim = 1;
    load_options.onlyprestim = 1;
    load_options.combinestim = 1;
    load_options.fs = 5;
    load_options.pupil = 1;

    % r is normalized, r_diff has mean subtracted and tr is true response
    [tr tr_diff r r_diff r_params cfd p] = array_64_VOC_load(load_options);
    display('Duplicated data with noise')
    bincount = r_params.bincount;
    stimcount = r_params.stimcount;
    repcount = r_params.repcount;
    cellcount = r_params.cellcount;
    r_params.fs = load_options.fs; 
    r_params.includeprestim = load_options.includeprestim;
    r_params.prestimonly = load_options.onlyprestim;  
    
    % Replicate data from first half of neurons, recount cells, add noise
    nset = floor(cellcount/2);
    
    %{
    r_diff = repmat(r_diff(:,:,:,nset:cellcount), [1 1 1 2]);
    r = repmat(r(:,:,:,nset:cellcount), [1 1 1 2]);
    r_params.cellcount = size(r,4);
    cellcount = size(r,4);
    %}
    r_diff = r_diff(:,:,:,nset:cellcount);
    r_diff = cat(4, r_diff, randn(size(r_diff)));
    r = r(:,:,:,nset:cellcount);
    r = cat(4, r, randn(size(r))./1000);
    
    r_diff=r_diff(:,:,:,1:cellcount);
    r=r(:,:,:,1:cellcount);
    
    %r_diff = r_diff + 0.1*randn(size(r));
    %r = r + 0.1*randn(size(r));
    
elseif true == 0
    display('synthetic data with fixed n of PCs')
    % Create matrix with 4 known principle components
    r_params = struct;
    bincount = 40;
    r_params.bincount = bincount;
    stimcount = 1;
    r_params.stimcount = stimcount;
    repcount = 40;
    r_params.repcount = repcount;
    cellcount = 12;
    r_params.cellcount = cellcount;
    r_params.prestimonly = 1;
    pcs = 3;

    U = randn(bincount*repcount, pcs);
    S = 10*diag(pcs:-1:1);
    V = 1 + randn(cellcount, pcs);   
    
    r = reshape(U*S*V', [bincount stimcount repcount cellcount]);
    %r = r + randn(size(r)).*0.1;
    r_diff = r;
    
    
    p = randn(size(r_diff));  % Just to prevent code from throwing errors
    load_options = struct();
    load_options.combinestim = 1;
    rawid = 'test data';
    fs = 10;
    load_options.fs = fs;
end



%% Network model fitting. Can use "regular" network model or reduced dimenionsality
% Model via PCA or FA.

% Recalculate filter but use only 95% of data
% Use these filters to predict the firing of each neuron
% This loop produces a plot of all these filters, as well as a matrix:
% pred_resp that corresponds to the predicted deviation from the psth for
% each neuron. It does this for all possible "windows" of population
% activity, for all stimuli

 
% Choose method
method = 'noDimReduce';  %'noDimReduce';  'PCA' 'FA'

% Covariance fit or MSE fit
Mse = 1;


% ================ Set parameters for model fit ==================
input = r_params;
input.method = method;
input.r_diff = r_diff;
input.r = r;
input.p = p;
input.Mse = Mse;
input.smoothpcs = 0;
input.sigmapcs = 5;
% PC params
input.nPCs = cellcount - 1;

% FA params
input.nFacs = 5;

% Call model_fit. Produce structure that mirrors input structure, with
% additional field of "output"


out = model_fit(input);

if strcmp(method, 'noDimReduce')
    pred = out.output.resp;
    cov_rn = out.output.cov;    
end

if strcmp(method, 'PCA')
    pred = out.output.resp_s;
    pred_net = out.output.resp_n;
    cov = out.output.cov_s;
    cov_net = out.output.cov_n;
    cov_overall = out.output.cov_overall;
    nPCs = out.nPCs;
end

if strcmp(method, 'FA')
    pred = out.output.resp_s;
    pred_net = out.output.resp_n;
    cov = out.output.cov_s;
    cov_net = out.output.cov_n;
    nPCs = out.nFacs;
end

%% Qualitative evaluation of prediction ability of model
% which stim
if load_options.combinestim == 1
    stim = 1;
    titl = 'combined';
else
    stim = 2;   % can be one or two
    titl = num2str(stim);
end

% =========== Plotting qualitative results of raw network model =========== 
if strcmp(method, 'noDimReduce')
    
    % Plot r0+ r_diff predicted (model) and compare to only r0
    r0 = repmat(mean(input.r, 3), [1 1 repcount 1]);  % psth
    
    if load_options.onlyprestim
        pred = out.output.resp;
    else
        pred = r0 + out.output.resp; % Model predicting of r_diff + null model (psth only)
    end

    pred_coeff_r0 = cross_cov(r0, input.r, 0);
    pred_coeff_full = cross_cov(pred, input.r, 0);
    p_norm = squeeze(mean(input.p(:,stim,:), 1)/max(max(input.p(:,stim,:))))/10 ...
        + abs(min(squeeze(mean(input.p(:,stim,:), 1)/max(max(input.p(:,stim,:))))/10));
    diff = nanmean(pred_coeff_full(stim,:,:), 3) - nanmean(pred_coeff_r0(stim,:,:), 3);
    
    % Look at prediction over all reps, averaged over neurons
    figure;
    
    if load_options.onlyprestim
        hold on
        plot(nanmean(pred_coeff_full(stim,:,:), 3), 'b-o', 'linewidth', 3)

        plot(p_norm, 'k-o', 'linewidth', 3) 
        legend('rN', 'pupil')
        legend('boxoff')
        title({'Performance of network model', ['pupil vs. model: '...
            num2str(xcov(p_norm, nanmean(pred_coeff_full(stim,:,:), 3),0,'coeff'))]})
        xlabel('Cross-validation Trials')
        ylabel('Prediction coefficient')
        suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
            ['spontonly: ' num2str(load_options.onlyprestim)]});
        hold off
        
        figure;
        hold on
        plot(p_norm, nanmean(pred_coeff_full(stim,:,:),3),'o', 'markersize', 3, 'markerfacecolor', 'b')
        title(['pupil vs. model: '...
            num2str(xcov(p_norm, nanmean(pred_coeff_full(stim,:,:), 3),0,'coeff'))])
        suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
            ['spontonly: ' num2str(load_options.onlyprestim) ...
            ', fs: ' num2str(load_options.fs)]});
        hold off
    else
        hold on
        plot(nanmean(pred_coeff_r0(stim,:,:), 3), 'r-o', 'linewidth', 3)
        plot(nanmean(pred_coeff_full(stim,:,:), 3), 'b-o', 'linewidth', 3)
        plot(diff, 'g-o', 'linewidth', 3)

        plot(p_norm, 'k-o', 'linewidth', 3) 
        legend('r0', 'rN', 'diff', 'pupil')
        legend('boxoff')
        title({'Performance of network model', ...
            ['pupil vs. diff: ' num2str(xcov(p_norm, diff, 0,'coeff'))], ...
            ['diff vs. r0: ' num2str(xcov(diff, nanmean(pred_coeff_r0(stim,:,:),3), 0,'coeff'))]})
        xlabel('Cross-validation Trials')
        ylabel('Prediction coefficient')
        suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
            ['spontonly: ' num2str(load_options.onlyprestim) ...
            ', fs: ' num2str(load_options.fs)]});
        hold off
        
        figure;
        hold on
        plot(squeeze(p_norm), squeeze(diff), 'o', 'markersize', 3, 'markerfacecolor', 'b')
        title(['pupil vs. diff: '...
            num2str(xcov(p_norm, diff,0,'coeff'))])
        suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
            ['spontonly: ' num2str(load_options.onlyprestim) ...
            ', fs: ' num2str(load_options.fs)]});
        hold off
    end
    
    
    % Scatter plots of rN vs. r0
    if load_options.onlyprestim == 0
        c = linspace(1,100,cellcount);
        figure; 
        hold on
        for ii = 1:repcount
            subplot(7, floor(repcount/7) + 1, ii)
            hold on
            plot(abs(squeeze(pred_coeff_r0(stim, ii, :))), ...
                abs(squeeze(pred_coeff_full(stim, ii, :))), 'ro', 'markersize', 4, 'markerfacecolor', 'r')
            %scatter(squeeze(abs(pred_coeff_r0(stim, ii, :))), ...
            %    squeeze(abs(pred_coeff_full(stim, ii, :))), [], c, 'filled')
            axis('tight')
            plot([0 1], [0 1] , 'k', 'linewidth', 2)
            axis('tight')
        end
        suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
            ['spontonly: ' num2str(load_options.onlyprestim)...
            ', fs:' num2str(load_options.fs)]});
     end
    
% ============== Plotting results from model fitting with PCA ===========    
else
    
    if Mse == 1
        titl = 'mse';
    else
        titl = 'prediction coeff';
    end
    mse = zeros(cellcount, nPCs);
    
    % Plot performace for each neuron over PCs
    % using prediction coefficient calculated over long string. 
    figure;
    hold on
    for jj = 1:cellcount
        subplot(6,floor(cellcount/6) + 1,jj)
        hold on
        title(['Neuron: ' num2str(jj)]);        
        mse(jj, :) = abs(cov_overall(jj,:)); 
        % mse across all PCs/Factors
        
        plot(mse(jj,:), 'r', 'linewidth', 3)
        axis('tight')
    end
    subplot(6,floor(cellcount/6) + 1,jj+1)
    hold on
    plot(squeeze(mean(mse,1)), 'r', 'linewidth', 2);
    title('Mean')
    axis('tight')
    suptitle({['rawid: ' num2str(rawid) 'using: ' titl ...
         ', fs:' num2str(load_options.fs)], 'PC step'})
    
    % Plot PC step as a function of neurons
    figure; hold on
    pc_step = zeros(cellcount, input.nPCs);
    for jj = 1:cellcount
        subplot(6,floor(cellcount/6) + 1,jj)
        hold on
        title(['Neuron: ' num2str(jj)]);
        for ii = 1:input.nPCs
            if ii ~= 1
                pc_step(jj,ii) = abs(mse(jj, ii)) - abs(mse(jj, ii-1));
            else
                if Mse == 1
                    pc_step(jj,ii) = abs(mse(jj, 1)) - 1;
                else
                    pc_step(jj,ii) = abs(mse(jj, 1));
                end
            end
        end
         plot(pc_step(jj,:), 'b-', 'linewidth', 1)
         axis('tight')
    end
    subplot(6,floor(cellcount/6) + 1,jj+1)
    hold on
    plot(squeeze(mean(pc_step,1)), 'r', 'linewidth', 2);
    title('Mean')
    axis('tight')
     suptitle({['rawid: ' num2str(rawid) 'using: ' titl ...
         ', fs:' num2str(load_options.fs)], 'PC step'})
    
    figure;
    hold on
    plot(squeeze(mean(pc_step,1)), 'r', 'linewidth', 2);
    title('Mean')
    axis('tight')
    suptitle({['rawid: ' num2str(rawid) 'using: ' titl ...
         ', fs:' num2str(load_options.fs)], 'PC step'}) 
end
% =========================================================================

%% Attmept to find clusters of neuronal subtypes/networks

% Did network model perform better than chance for a given neuron?
% Use bootstrapped measures of mse and xcovcov_overall = out.output.cov_overall; to decide
if strcmp(method, 'noDimReduce')
    
       
    % If evoked, we want to try to predict intrinsic dynamics (r - r0) and
    % model outputs a fit for r_diff in the evoked case
    r0 = repmat(mean(input.r, 3), [1 1 repcount 1]);  % psth
    if load_options.onlyprestim == 0
        model = out.output.resp + r0;
    else
        model = out.output.resp;
    end
    
    pval = zeros(cellcount,1);
    mspval = zeros(cellcount,1);
    true = zeros(cellcount,1);
    truems = zeros(cellcount,1);
    nshuffle = 1000;
 
    % --- Calculate p-value for each neuron using bootstrap --- 
    % String together all responses for given stim and get prediction
    % coefficient for each neuron. Then do this many times, each time
    % shuffling the true response
    rtemp = reshape(input.r(:,stim,:,:), bincount*repcount, cellcount);
    model = reshape(model(:,stim,:,:), bincount*repcount, cellcount);
    
    for jj = 1:cellcount

        cov = zeros(nshuffle,1);
        msqe = zeros(nshuffle,1);
        true(jj) = xcov(rtemp(:,jj), model(:,jj), 0, 'coeff'); 
        truems(jj) = sqrt(mean((model(:,jj) - rtemp(:,jj)).^2)) ...
               ./ sqrt(mean(rtemp(:,jj).^2));
        count = 0;
        mscount = 0;
        for ii = 1:nshuffle
           
           sh = shuffle(rtemp(:,jj)); 
           cov(ii) = xcov(sh, model(:,jj), 0, 'coeff'); 
           msqe(ii) = sqrt(mean((model(:,jj) - sh).^2)) ...
               ./ sqrt(mean(sh.^2));
           if true(jj) < 0 && cov(ii) < true(jj)
               count = count + 1; 
           elseif true(jj) > 0 && cov(ii) > true(jj)
               count = count + 1; 
           elseif msqe(ii) < truems(jj)
               mscount = mscount + 1;  
           end
           
        end    
        pval(jj) = count/nshuffle;
        mspval(jj) = mscount/nshuffle;
    end
    
    % Create summary plot
    cov = cross_cov(model, rtemp, 0);
    mse = cross_cov(model, rtemp, 1);
    figure; hold on; subplot(3,2,1); 
    hold on;
    plot(mse, 'r-o', 'linewidth', 3)
    %plot(squeeze(mean(mse,1)), 'r', 'linewidth', 3)
    title('mse')
    axis('tight')
    subplot(3,2,2)
    hold on
    plot(abs(cov), 'r-o', 'linewidth', 3)
    %plot(squeeze(mean(abs(cov),1)), 'r', 'linewidth', 3)
    title('Prediction coeff') 
    axis('tight')
    subplot(3,2,4); hold on;
    plot(pval, '-o');
    plot([0; cellcount], [0.05; 0.05], '--r')
    axis('tight')
    ylabel('p-value')
    title('prediction coeff pval')% When computing mse or xcov, we will add back r0 to model prediction based on r_diff
    subplot(3,2,3)
    hold on
    plot(mspval, '-o')
    plot([0; cellcount], [0.05; 0.05], '--r')
    ylabel('p-value')
    title('mse pval')
    axis('tight')
    subplot(3,2,5)
    hold on
    tr_std = reshape(tr(:,stim, :,:), bincount*repcount, cellcount);
    plot(squeeze(std(tr_std))./mean(tr_std), 'b-o', 'linewidth', 2);
    %plot(squeeze(mean(mean(input.r(:,stim,:,:),3),1)), 'linewidth', 2)
    %plot(squeeze(mean(input.r(:,stim,:,:),3))', '*')
    %axis([0 cellcount min(min(squeeze(mean(input.r(:,stim,:,:),3)))) ...
    %    max(max(squeeze(mean(input.r(:,stim,:,:),3))))])
    axis('tight')
    title('Variance of firing rate')
    ylabel('Fano factor')
    xlabel('neurons')
    subplot(3,2,6)
    hold on
    plot(squeeze(mean(tr(:,stim,:,:),1))', '*')
    plot(squeeze(mean(mean(tr(:,stim,:,:),1),3)), 'linewidth', 2)
    title(['Raw firing rate: fs: ' num2str(load_options.fs)])
    ylabel('Hz')
    axis('tight')
    suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
        ['spontonly: ' num2str(load_options.onlyprestim) ...
        ', fs: ' num2str(load_options.fs)]});
    
    figure;
    subplot(2,1,1)
    hold on
    plot(squeeze(std(tr_std))./mean(tr_std), abs(cov), '.')
    xlabel('fano factor')
    ylabel('prediction coefficient')
    title(num2str(xcov(squeeze(std(tr_std))./mean(tr_std), abs(cov), 0, 'coeff')))
    subplot(2,1,2)
    hold on
    plot(mean(tr_std), abs(cov), '.')
    xlabel('Normalized firing rate')
    ylabel('Prediction coefficient')
    title(num2str(xcov(mean(tr_std), abs(cov), 0, 'coeff')))
    suptitle({['stim: ' titl ', rawid: ' num2str(rawid)], ...
       ['spontonly: ' num2str(load_options.onlyprestim) ...
       ', fs: ' num2str(load_options.fs)]});  
end

%% Perform normal PCA on the above data and compare across neurons, with 
%  pupil etc.
r = input.r(:, stim, :, :);
p = input.p(:, stim, :);
%input.smoothpcs = 1;
if input.smoothpcs
    r = reshape(r, bincount*repcount*1, cellcount);
    r = gsmooth(r, input.sigmapcs);
else
    r = reshape(r, [bincount*1*repcount, cellcount]);
end

r = reshape(r, [bincount*1*repcount, cellcount]);
p = reshape(p, [bincount*1*repcount, 1]);
[U S V] = svd(r);
pcs = U*S;

pup_cov = zeros(cellcount,1);
for ii = 1:cellcount
    pup_cov(ii) = xcov(p, pcs(:,ii), 0, 'coeff');
end

figure;
subplot(2,1,1)
hold on
title('PC weights')
plot(diag(S))
subplot(2,1,2)
hold on
title('PC correlation with pupil')
plot(abs(pup_cov))
suptitle({['rawid: ' num2str(rawid)], ...
        ['spontonly: ' num2str(load_options.onlyprestim) ...
        'fs: ' num2str(load_options.fs)]});
% plot pc weights against each cell
figure; hold on
for ii = 1:cellcount 
   subplot(6,floor(cellcount/6) + 1,ii)
   hold on
   plot(V(:,ii), '-')
   title(['pc: ' num2str(ii)])
   axis('tight')
end
suptitle(['PC weights vs. neurons,   fs:' num2str(load_options.fs)]);

% Plot 1st 3 PCs in 3d scatter
figure; plot3k([(V(:,1)), (V(:,2)), (V(:,4))], ...
    'Marker', {'.', 15}, 'Labels', ...
    {'weights', 'pc 1', 'pc 2', 'pc 4', 'colorscale'}, 'PlotType', 'scatter')




















%{
% Look at "coupling strength" i.e rN performance, across neurons vs. std
% Look at rN performance and std across trials

figure;
hold on
subplot(1,2,1)
hold on
plot(nanstd(squeeze(model_cov)), nanmean(squeeze(model_cov), 1),'.k')
title('Performance by trial')
xlabel('std of rN prediction coeff for across trials')
ylabel('mean of rN prediction coeff for across trials')
subplot(1,2,2)
hold on
for ii = 1:cellcount
    if pval(ii) < 0.05
        plot(nanstd(squeeze(model_cov(:,ii,:))'), nanmean(squeeze(model_cov(:,ii,:)), 1)','.g')
    else
        plot(nanstd(squeeze(model_cov(:,ii,:))'), nanmean(squeeze(model_cov(:,ii,:)), 1)','.k')
    end
end
xlabel('std of rN prediction coeff across neurons')
ylabel('mean of rN prediction coeff across neurons')
title('Performance by neuron')

% rN and pupil
figure;
hold on
plot(squeeze(mean(pupil,1)), squeeze(nanmean(model_cov,2)),'.k')
title(['Covariance: ' num2str(xcov(squeeze(mean(pupil,1)), squeeze(nanmean(model_cov,2)),0,'coeff'))])
ylabel('pupil (by trial)')
xlabel('rN prediction coefficient (by trial)')
%}



%{
%% Compare performance of PCA vs. FA vs. r_N on a neuron by neuron basis

% for all reps
rN = repmat(model_cov, [pcCount 1 1 1]);
figure;
subplot(2,1,1)
hold on
title('Average over all trials')
plot(squeeze(abs(nanmean(mean(pca_model_cov(:,1,:,:),4),3))), 'color', [1,0.5,0.5], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(pca_model_cov_net(:,1,:,:),4),3))), 'color', [0.5,0.5,1], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(fa_model_cov(:,1,:,:),4),3))), 'color', [0.5,1,0.5], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(fa_model_cov_net(:,1,:,:),4),3))), 'color', [0,0,0], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(rN(:,:,:),3),2))), 'color', [0.5 0.5 0.5], 'Linewidth', 3, 'linestyle', '--')
for ii = 1:pcCount
    if ii == 1
        plot(ii,abs(nanmean(mean(pca_model_cov_net(ii,1,:,:),4),3)), 'or', 'markersize', 7, 'markerfacecolor', 'r')
        plot(ii,abs(nanmean(mean(fa_model_cov_net(ii,1,:,:),4),3)), 'ok', 'markersize', 7, 'markerfacecolor', 'k')
    else
        plot(ii,abs(nanmean(mean(pca_model_cov_net(ii,1,:,:),4),3)) - abs(nanmean(mean(pca_model_cov_net(ii-1,1,:,:),4),3)), 'or','markersize', 7, 'markerfacecolor', 'r')
        plot(ii,abs(nanmean(mean(fa_model_cov_net(ii,1,:,:),4),3)) - abs(nanmean(mean(fa_model_cov_net(ii-1,1,:,:),4),3)), 'ok', 'markersize', 7, 'markerfacecolor', 'k')
    end

end

plot(squeeze(abs((mean(pca_model_cov(:,1,:,:),4)))), '.r')
plot(squeeze(abs(mean(fa_model_cov(:,1,:,:),4))), '.g')
%plot(squeeze(abs(mean(fa_model_cov_net(:,1,:,:),4))), 'xk')


legend('Individual PCs', 'Net PCs', 'Ind. factors', 'Net factors', 'rN', 'pca step', 'FA step')
legend('boxoff')
xlabel('PCs')
ylabel('Predicition coefficient')
axis('tight')
hold off

% Compute comparison index: abs(A-B/A+B) for PCA vs. r_N over all pcs
subplot(2,1,2)
hold on
diff = squeeze(abs(pca_model_cov_net)) - abs(rN);
diff_fa = squeeze(abs(fa_model_cov_net)) - abs(rN);
Sum = abs(rN) + squeeze(abs(pca_model_cov_net));
Sum_fa = abs(rN) + squeeze(abs(fa_model_cov_net));
ratio = zeros(pcCount, cellcount, repcount);
fa_ratio = zeros(pcCount, cellcount, repcount);
for kk = 1:pcCount
    for ii = 1:repcount
        for jj = 1:cellcount
            ratio(kk,jj,ii) = 3*diff(kk,jj,ii)/Sum(kk,jj,ii);
            fa_ratio(kk,jj,ii) = 3*diff_fa(kk,jj,ii)/Sum_fa(kk,jj,ii);
        end
    end
end

plot(squeeze(median(nanmean(ratio,3),2)), '--k', 'linewidth', 3)
plot(squeeze(median(nanmean(fa_ratio,3),2)), '--g', 'linewidth', 3)
plot(repmat(0, [pcCount 1]), '-r', 'linewidth', 2)
ind = [1:pcCount]+.3;
plot(ind,squeeze(nanmean(ratio(:,:,:),3)),'.k')
plot(squeeze(nanmean(fa_ratio(:,:,:),3)),'.g')
for ii = 1:cellcount
    for jj = 1:pcCount
        if squeeze(nanmean(fa_ratio(3,ii,:),3)) > 0
            plot([ind(jj) jj], [squeeze(nanmean(ratio(jj,ii,:),3)), squeeze(nanmean(fa_ratio(jj,ii,:),3))], '-k')
        else
            %plot([ind(jj) jj], [squeeze(nanmean(ratio(jj,ii,:),3)), squeeze(nanmean(fa_ratio(jj,ii,:),3))], '-k')
        end

    end
end

ylabel('Comparison Index')
xlabel('PCs')
legend('Median of index: pca', 'Median of index: fa', 'Equal predictive ability')
legend('boxoff')
title({['abs(A-B/A+B) for A = pca and B = rN']})
axis([1 pcCount+0.5 -.5 .3])
hold off

%%%%%%%%%%%%%%%% plot over subset of reps/neurons

% For looking at subset of reps
reps = 1:repcount; %find(model_cov_av(1,:) <= 0.2);

% only neurons that were predicted better than chance using rN model
neurons = find(pval < 0.05);

rN = repmat(model_cov, [pcCount 1 1 1]);
figure;
subplot(2,1,1)
hold on
title('subset of trials')
plot(squeeze(abs(nanmean(mean(pca_model_cov(:,1,neurons,reps),4),3))), 'color', [1,0.5,0.5], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(pca_model_cov_net(:,1,neurons,reps),4),3))), 'color', [0.5,0.5,1], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(fa_model_cov(:,1,neurons,reps),4),3))), 'color', [0.5,1,0.5], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(fa_model_cov_net(:,1,neurons,reps),4),3))), 'color', [0,0,0], 'Linewidth', 3)
plot(squeeze(abs(nanmean(mean(rN(:,neurons,reps),3),2))), 'color', [0.5 0.5 0.5], 'Linewidth', 3, 'linestyle', '--')
for ii = 1:pcCount
    if ii == 1
        plot(ii,abs(nanmean(mean(pca_model_cov_net(ii,1,neurons,reps),4),3)), 'or', 'markersize', 7, 'markerfacecolor', 'r')
        plot(ii,abs(nanmean(mean(fa_model_cov_net(ii,1,neurons,reps),4),3)), 'ok', 'markersize', 7, 'markerfacecolor', 'k')
    else
        plot(ii,abs(nanmean(mean(pca_model_cov_net(ii,1,neurons,reps),4),3)) - abs(nanmean(mean(pca_model_cov_net(ii-1,1,neurons,reps),4),3)), 'or','markersize', 7, 'markerfacecolor', 'r')
        plot(ii,abs(nanmean(mean(fa_model_cov_net(ii,1,neurons,reps),4),3)) - abs(nanmean(mean(fa_model_cov_net(ii-1,1,neurons,reps),4),3)), 'ok', 'markersize', 7, 'markerfacecolor', 'k')
    end

end

plot(squeeze(abs((mean(pca_model_cov(:,1,neurons,reps),4)))), '.r')
plot(squeeze(abs(mean(fa_model_cov(:,1,neurons,reps),4))), '.g')
%plot(squeeze(abs(mean(fa_model_cov_net(:,1,:,:),4))), 'xk')


legend('Individual PCs', 'Net PCs', 'Ind. factors', 'Net factors', 'rN', 'pca step', 'FA step')
legend('boxoff')
xlabel('PCs')
ylabel('Predicition coefficient')
axis('tight')
hold off

% Compute comparison index: abs(A-B/A+B) for PCA vs. r_N over all pcs
subplot(2,1,2)
hold on
diff = squeeze(abs(pca_model_cov_net)) - abs(rN);
diff_fa = squeeze(abs(fa_model_cov_net)) - abs(rN);
Sum = abs(rN) + squeeze(abs(pca_model_cov_net));
Sum_fa = abs(rN) + squeeze(abs(fa_model_cov_net));
ratio = zeros(pcCount, cellcount, repcount);
fa_ratio = zeros(pcCount, cellcount, repcount);
for kk = 1:pcCount
    for ii = 1:repcount
        for jj = 1:cellcount
            ratio(kk,jj,ii) = diff(kk,jj,ii)/Sum(kk,jj,ii);
            fa_ratio(kk,jj,ii) = diff_fa(kk,jj,ii)/Sum_fa(kk,jj,ii);
        end
    end
end

plot(squeeze(median(nanmean(ratio(:,neurons,reps),3),2)), '--k', 'linewidth', 3)
plot(squeeze(median(nanmean(fa_ratio(:,neurons,reps),3),2)), '--g', 'linewidth', 3)
plot(repmat(0, [pcCount 1]), '-r', 'linewidth', 2)
ind = [1:pcCount]+.3;
plot(ind,squeeze(nanmean(ratio(:,neurons,reps),3)),'.k')
plot(squeeze(nanmean(fa_ratio(:,neurons,reps),3)),'.g')
for ii = 5;1:cellcount
    for jj = 1:pcCount
        if squeeze(nanmean(fa_ratio(3,ii,reps),3)) > 0
            plot([ind(jj) jj], [squeeze(nanmean(ratio(jj,ii,reps),3)), squeeze(nanmean(fa_ratio(jj,ii,reps),3))], '-k')
        else
            %plot([ind(jj) jj], [squeeze(nanmean(ratio(jj,ii,:),3)), squeeze(nanmean(fa_ratio(jj,ii,:),3))], '-k')
        end

    end
end

ylabel('Comparison Index')
xlabel('PCs')
legend('Median of index: pca', 'Median of index: fa', 'Equal predictive ability')
legend('boxoff')
title({['abs(A-B/A+B) for A = pca and B = rN']})
axis([1 pcCount+0.5 -.5 .5])
hold off  


%% PCA vs. NNMF with pupil

% Perform PCA on response data

bincountn = 5;               % stim are combined
bins = size(r,1);
if bins ~= bincountn
    r_pc = nanmean(reshape(r,[bins/bincountn bincountn*stimcount repcount cellcount]));
    r_pc = reshape(r_pc, [bincountn*repcount*stimcount cellcount]);
    p = nanmean(reshape(pupil, [bins/bincountn bincountn*stimcount repcount 1]));
    p = reshape(p, [bincountn*stimcount*repcount 1]);
else
    r_pc = reshape(r, [bincount*repcount*stimcount cellcount]);
    p = reshape(pupil, [bincount*repcount*stimcount 1]);

end

[U S V] = svd(r_pc);
pcs = U*S;

figure;
hold on
for ii = 1:9
    subplot(3, 3, ii)
    hold on
    plot(pcs(:,ii));
    plot(p, '-g')
    title(num2str(xcov(pcs(:,ii),p,0,'coeff')))
    axis('tight')
end

[lambda psi T stats F] = factoran(r_pc,5, 'rotate', 'none');
fcs = T*F';

figure;
hold on
for ii = 1:size(T,1)
   subplot(size(T,1),2,ii)
   hold on
   plot(fcs(ii,:))
   plot(p,'-g')
   title(num2str(xcov(fcs(ii,:), p, 0, 'coeff')))
end

%% Use "network activity" to predict pupil diameter

rN_p = zeros(repcount,1);
pca_p = zeros(repcount,1);
fa_p = zeros(repcount,1);

numPCs = 2;
for jj = 1:repcount
    if valSetSize > 1
        if (jj+valSetSize-1) > repcount
            validation = [jj:repcount, 1:((valSetSize-1)-(repcount-jj))];            
        else
            validation = jj:(jj+valSetSize-1);
        end
    else
        validation = jj; 
    end
    window=setdiff(1:repcount,validation);
    
    % set up sample matrices
    r_samp = r(:,1,window,:);
    pup = pupil(:,1,window);
    
    p = reshape(pupil, [bincount stimcount repcount 1]);
    p = p(:,1,window,:);
    
    stim = r(:,1,validation,:);
    stim = reshape(stim, [bincount cellcount]);
    
    % "Normal NRF"
    h = linear_NRF(r_samp, p);    
    pred_pup = stim*h;
    rN_p(jj) = xcov(pred_pup, pupil(:,1,validation), 0, 'coeff');
    
    % Using PCA
    [U S V] = svds(reshape(r_samp, [bincount*stimcount*length(window) cellcount]),numPCs);
    pcs = U*S;
    pca_in = pcs(:,numPCs)*V(:,numPCs)';
    pca_in = reshape(pca_in, [bincount stimcount length(window) cellcount]);
    pca_h = linear_NRF(pca_in, p);
    pca_pred_pup = stim*pca_h;
    pca_p(jj) = xcov(pca_pred_pup, pupil(:,1,validation), 0, 'coeff');
    
    % Using FA
    [lambda psi T stats F] = factoran(reshape(r_samp, [bincount*length(window) cellcount]),numPCs);
    fa_in = F(:,numPCs)*T(numPCs,numPCs)'*lambda(:,numPCs)';
    fa_in = reshape(fa_in, [bincount stimcount length(window) cellcount]);
    fa_h = linear_NRF(fa_in, p);
    fa_pred_pup = stim*fa_h;
    fa_p(jj) = xcov(fa_pred_pup, pupil(:,1,validation), 0, 'coeff');
end

figure;
hold on
subplot(3,1,1)
hold on
plot(abs(rN_p))
title('r_N')
subplot(3,1,2)
hold on
plot(abs(pca_p),'r')
title(['PCA, numPCs: ' num2str(numPCs)])
subplot(3,1,3)
hold on
plot(abs(fa_p),'g')
title(['FA, numPCs: ' num2str(numPCs)])
%plot(pcs(:,2),'k')
%plot(squeeze(nanmean(nanmean(r(:,1,:,:),4),1)),'k')

%}
