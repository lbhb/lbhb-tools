% Charlie Heller
% 3/1/2017

% Compute the covariance between a model prediction and actual response for
% a given cell, on a given trial, to a given stimulus.

% Inputs
%     X: model prediction (time X stim X reps X cells)
%     Y: true data (time X stim X reps X cells)

% Outputs
%     cov: covariance between X and Y (stim X reps X cells)


function [cov] = cross_cov(X, Y, mse)

bincount = size(X,1);
stimcount = size(X,2);
repcount = size(X,3);
cellcount = size(X,4);

if mse == 0
    mse = 0;
    cov = zeros(stimcount, repcount, cellcount);
    for kk = 1:stimcount
        for jj = 1:repcount
            for ii = 1:cellcount
                cov(kk, jj, ii) = xcov(X(:,kk,jj,ii), Y(:,kk,jj,ii), 0, 'coeff');            
            end
        end
    end
    
elseif mse
    
    cov = zeros(stimcount, repcount, cellcount);
    for kk = 1:stimcount
        for jj = 1:repcount
            for ii = 1:cellcount
                d = sqrt(mean((X(:,kk,jj,ii) - Y(:,kk,jj,ii)).^2));
                s = sqrt(mean(abs(Y(:,kk,jj,ii).^2)));
                cov(kk, jj, ii) = d./s;            
            end
        end
    end
    
end


end