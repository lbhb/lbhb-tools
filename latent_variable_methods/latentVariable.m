narf_set_path
% Charlie Heller
% Experiment with latent variable methods


%% Load data

rawid = 118698;  % BOL005c VOC
% rawid = 118758;  % BOL006b VOC
% rawid = 116168;  % zee015h VOC

% Create load parameters structure for array_64_VOC_load
load_options.rawid = rawid;
load_options.miniso = '>84';
load_options.includeprestim = 1;
load_options.onlyprestim = 1;
load_options.combinestim = 1;
load_options.fs = 10;
load_options.pupil = 1;

% r is normalized, r_diff has mean subtracted and tr is true response
[tr tr_diff r r_diff r_params cfd p] = array_64_VOC_load(load_options);   
bincount = r_params.bincount;
stimcount = r_params.stimcount;
repcount = r_params.repcount;
cellcount = r_params.cellcount;

%% Process data for PCA

% Trial average and temporally smooth data for PCA analysis (Cunningham et
% al)
r_PCA = r;

r_PCA = squeeze(mean(r_PCA,3));
r_PCA = smooth3(r_PCA, 'gaussian',1,10);
r_PCA2 = r_PCA(:,2,:);
r_PCA1 = r_PCA(:,1,:);
r_PCA_resh_2 = reshape(r_PCA2, [bincount cellcount]);
r_PCA_resh_1 = reshape(r_PCA1, [bincount cellcount]);

[U1 S1 V1] = svd(r_PCA_resh_1);
[U2 S2 V2] = svd(r_PCA_resh_2);
PCs = U*S;
% State space plot
figure;
hold on
plot(V1(:,1),V1(:,2))
plot(V2(:,1),V2(:,2),'g')