% Charlie Heller
% Dimensionality reduction function for 64 electrode array experiments
% Started on 2-6-2017

% Input
    % r: response matrix (bins X stim X trials X cells)
    % spont: logical for using spont period
    % evoked: logical for using evoked period
    % fs: sampling rate for analysis
    % r_params: struct containing information about r
    % ValSet: data to be projected onto principle components
    % Number of PCs to project onto
% Output
    

function [PCs V PCA_pred W H NNMF_pred] = PCA(varargin) 
    nargs = length(varargin);
   for ii = 1:nargs 
       if strcmp(varargin{ii}, 'r')
          r = varargin{ii+1}; 
       end
       
       if strcmp(varargin{ii}, 'spont')
          spont = varargin{ii+1}; 
       end
       
       if strcmp(varargin{ii}, 'evoked')
          evoked = varargin{ii+1}; 
       end
       
       if strcmp(varargin{ii}, 'nbins')
          nbins = varargin{ii+1}; 
       end
       
       if strcmp(varargin{ii}, 'r_params')
          r_params = varargin{ii+1}; 
       end
       
       if strcmp(varargin{ii}, 'valSet')
          valSet = varargin{ii+1};
          valSet_tf = 1;
       end
       
       if strcmp(varargin{ii}, 'nPCs')
          nPCs = varargin{ii+1}; 
       end
       
       if strcmp(varargin{ii}, 'tr')
          tr = varargin{ii+1}; 
       end
   end    

    stimcount = size(r,2);
    repcount = size(r,3);
    cellcount = size(r,4);
    fs = r_params.fs;
    
    prestim = r_params.prestim*fs;
    endStim = size(r,1);
    
    % Use specifications for spont/evoked/bins to rebin data for analysis
    if spont && ~evoked
        if r_params.includeprestim == 1
            aa= 1:prestim;
        else
            error('Spont period is not loaded')
        end    
        r = r(aa,:,:,:);
        tr = tr(aa,:,:,:);
        bins=length(aa);
        if nbins ~= bins
            r=nanmean(reshape(r,[bins/nbins nbins*stimcount repcount cellcount]));
            r = reshape(r, [nbins stimcount repcount cellcount]);
            tr=nanmean(reshape(tr,[bins/nbins nbins*stimcount repcount cellcount]));
            tr = reshape(tr, [nbins stimcount repcount cellcount]);
        end
        
    elseif evoked && ~spont
        if r_params.prestimonly == 0 && r_params.includeprestim == 1
            ps = r_params.poststim*fs;
            aa=(prestim+1):(endStim-ps);
        elseif r_params.prestimonly == 1
            error('Evoked period is not loaded')
        elseif r_params.includeprestim == 0     % if input vector doesn't contain prestim
            aa = 1:(endStim);
        end        
        r = r(aa,:,:,:);
        tr = tr(aa,:,:,:);
        bins=length(aa);
        if nbins ~= bins
            r=nanmean(reshape(r,[bins/nbins nbins*stimcount repcount cellcount]));
            r = reshape(r, [nbins stimcount repcount cellcount]);
            tr=nanmean(reshape(tr,[bins/nbins nbins*stimcount repcount cellcount]));
            tr = reshape(tr, [nbins stimcount repcount cellcount]);      
        end
        
    elseif evoked && spont
        if r_params.prestimonly == 0 && r_params.includeprestim == 1
            ps = r_params.poststim*fs;
            aa=1:(endStim-ps);
        elseif r_params.prestimonly == 1
            error('Evoked period is not loaded')
        elseif r_params.includeprestim == 0     % if input vector doesn't contain prestim
            error('Spont is not loaded')
        end
        bins=length(aa);
        r = r(aa,:,:,:);
        tr = tr(aa,:,:,:);
        if nbins ~= bins
            r=nanmean(reshape(r,[bins/nbins nbins*stimcount repcount cellcount]));
            r = reshape(r, [nbins stimcount repcount cellcount]);

            tr=nanmean(reshape(tr,[bins/nbins nbins*stimcount repcount cellcount]));
            tr = reshape(tr, [nbins stimcount repcount cellcount]);      
        end
    end
    
    % PCA    
    [U S V] = svd(reshape(r, [nbins*stimcount*repcount cellcount]));
    PCs = U*S;
    if exist('valSet_tf','var')
        PCA_pred = zeros(size(valSet,1), nPCs);
        for ii = 1:nPCs
            PCA_pred(:,ii) = squeeze(valSet)*V(:,ii);
        end
    else
        PCA_pred = 0;
    end
    
   % NNMR    
   [W H] = nnmf(reshape(tr, [nbins*stimcount*repcount cellcount]), nPCs);
   if exist('valSet_tf', 'var')
       NNMF_pred = zeros(size(valSet,1), nPCs);
       for ii = 1:nPCs
           NNMF_pred(:,ii) = squeeze(valSet)*H(ii,:)';
       end
   else
       NNMF_pred = 0;
   end
   
end