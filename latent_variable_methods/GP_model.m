narf_set_path

% Charlie Heller 

% Modeling continuous data using psth and latent variables (fit using
% gaussian processes)

%% Load continuous data

%rawid = 118698;  % BOL005c VOC
rawid = 118758;  % BOL006b VOC
% rawid = 116168;  % zee015h VOC

load_options = struct();
load_options.rawid = rawid;
load_options.miniso = '>84';
load_options.fs = 10;
[r, r_byt, trialset, trialset_byt, tags, params] = load_cont(load_options);

%% Process continous data

bincount = size(r,1);
repcount = size(r,2);
stimcount = size(r,3);
cellcount = size(r,4);
ntrials = size(r_byt, 2);

%{
% Normalize the firing rate of r and r by trial
r = reshape(r, [bincount*repcount*stimcount, cellcount]);
r_byt = reshape(r_byt, [bincount*ntrials, cellcount]);
for ii = 1:cellcount
   r(:,ii) = r(:,ii) - nanmean(r(:,ii));
   r(:,ii) = r(:,ii)./nanstd(r(:,ii));
   r_byt(:,ii) = r_byt(:,ii) - nanmean(r_byt(:,ii));
   r_byt(:,ii) = r_byt(:,ii)./nanstd(r_byt(:,ii));
end
r = reshape(r, [bincount, repcount, stimcount, cellcount]);
r_byt = reshape(r_byt, [bincount, ntrials, cellcount]);
%}

% Subtract spont "psth" (b0) from all data. Define b0 as average rate 
% in preceding stimulus period - so, we are making the assumptions that
% there is spont rate independent of stim, time etc.

bins = params.prestim*load_options.fs;
spont_psth = nanmean(nanmean(nanmean(r(1:bins, :, :, :),2),1),3);
spont_psth = squeeze(repmat(spont_psth, [bincount 1 1 1]));


% Subtract psth from evoked periods

evoked_psth = nanmean(r((bins+1):bincount, :, :, :), 2);


%r((bins+1):bincount, :, :, :) = r((bins+1):bincount, :, :, :) - r0_voc;
%r = r - b0;

% Create r0 + b0 continuous stream for all neurons
r0 = zeros(size(r_byt));
for ii = 1:ntrials
    [i, j] = find(trialset == ii);
    r0(1:bins,ii, :) = zeros(length(1:bins), cellcount);
    r0((bins+1):bincount,ii,:) = - spont_psth((bins+1):bincount,:) ...
        + squeeze(evoked_psth(:, :, j, :)); 
end

r0 = reshape(r0, [bincount*ntrials, cellcount]);
r0(isnan(r0(:,1)),:) = [];   % Remove any columns with Nan
b0 = spont_psth(1, :);
b0 = repmat(b0, [size(r0,1), 1]);


% rb0 is now a matrix of average responses over time. During the evoked
% period, it is a value of psth evoked (for a given stimulus) - psth spont.
% (for that given cell) During spont period, it is a value of psth spont 
% only (for that cell).

% Formulate response matrix in identical format
r = reshape(r_byt, [bincount*ntrials, cellcount]);
r(isnan(r(:,1)),:) = []; 

%% Create latent variable model to account for fluctuations beyond psth
% Create pseudo data for model verification
pseudo = 1;
if pseudo
    t = 1:(size(r,1));
    pts = 0 + .1*randn(size(r));
    %pts = ones(size(r));
    pts = mean(pts,2);
    weights = 1e-3*ones(size(r,2), 1);
    weights(1:5) = 0.3;
    weights(6:15) = 0.1;
    weights(16:20) = 1e-4;
    weights(21:30) = 0.5;
    weights(31:41) = .01;
    Tau_true = 1500;
    rt = (weights*gauss_conv(t, pts, Tau_true))';
    noise = 0*0.001*poissrnd(1, size(rt));
    rt = rt + noise;
    b0t = repmat(mean(noise, 1),  size(rt,1), 1);
    r0t = zeros(size(rt)); %repmat(mean(rt,2), 1, size(rt,2));

else
    rt = r(:,:);
    r0t = r0(:,:);
    b0t = b0(:,:);
end

% Create latent variable prior (GP given by cov kernel * rand pts)
t = 1:(size(rt,1));
mu = rt - r0t - b0t;
pts = mu; % + 0.1*(randn(size(rt,1), size(rt,2)));
pts = mean(pts, 2);
prev = 0;
step = 101;
Tau = 1000;
w = 1e-3*ones(cellcount,1);
count = 0;
i = 0;


while abs(step) > 10
    if i == 0
        MaxIterations = 1;
    else 
        MaxIterations = 10;
    end
    i = i + 1;
    

    tauPrev = Tau;
    wPrev = w;
    cost = @(x) sum(sum((rt - ((w*gauss_conv(t,pts, abs(x(1))))' + r0t + b0t)).^2));
    x0 = Tau; 
    tic;
    options = optimset('PlotFcns',@optimplotfval, 'MaxIter', MaxIterations);
    Tau = fminunc(cost, x0, options)
    toc
  
    
    
    cost2 = @(x) sum(sum((rt - ((x(1:cellcount)*gauss_conv(t,pts, abs(Tau)))' + r0t + b0t)).^2));
    x02 = w;
    tic
    w = fminunc(cost2, x02, options)
    toc
   
    c = sum(sum((rt - ((w*gauss_conv(t,pts, abs(Tau)))' + r0t + b0t)).^2));
    step = c - prev;
    display(step);
    prev = c;   

end


%{
while abs(step) > 0.1
    if i == 0
        MaxIterations = 1;
    else 
        MaxIterations = 10;
    end
    i = i + 1;
    options = optimset('PlotFcns',@optimplotfval, 'MaxIter', MaxIterations);
    % ===== E Step =====

    % pts = gauss_conv(t, (w'*(rt-r0t)')'./cellcount, Tau);
    lv = gauss_conv(t, (w'*(rt-b0t)')'./cellcount, Tau)';
    % ===== M Step =====
 
    cost2 = @(x) sum(sum((rt - ((x(1:cellcount)*lv')' + r0t + b0t)).^2));
    x02 = w;
    tic
    w = fminunc(cost2, x02, options)
    toc
    
      tauPrev = Tau;
      wPrev = w;
%     cost = @(x) sum(sum((rt - ((w*gauss_conv(t,pts, abs(x(1)))')' + r0t + b0t)).^2));
%     x0 = Tau; 
%     tic;
%     options = optimset('PlotFcns',@optimplotfval, 'MaxIter', 1);
%     Tau = fminunc(cost, x0, options)
%     toc
    
    c = sum(sum((rt - ((w*gauss_conv(t,pts, abs(Tau)))' + r0t + b0t)).^2));
    step = c - prev;
    display(step);
    prev = c;     
end
%}

%  cost = @(x) sum(sum((rt - ((x(1:cellcount)*gauss_conv(t,pts, abs(x(1+cellcount)))')' + r0t + b0t)).^2));
%     x0 = [w; Tau]; 
%     tic;
%     options = optimset('PlotFcns',@optimplotfval, 'MaxIter', 100);
%     theta = fminunc(cost, x0, options)
%     toc
%     w = theta(1:cellcount);
%     Tau = theta(1+cellcount);

if step < 0
    p_params = [w; Tau];
    posterior = (gauss_conv(t,pts,Tau));
    model = (w*posterior)' + r0t + b0t;
    
else
    p_params = [wPrev; tauPrev];
    posterior = (gauss_conv(t,pts,tauPrev));
    model = (wPrev*posterior)' + r0t + b0t;

end

cell = 29;
figure; 
subplot(2,1,1)
hold on
plot(posterior)
legend('drift')
legend('boxoff')
title({['Tau predicted:   '  num2str(p_params(length(p_params)))], ...
    ['Tau true:   '  num2str(Tau_true)]})
axis('tight')
subplot(2,1,2)
hold on
plot(rt(:,cell))
plot(r0t(:,cell) + b0t(:,cell), 'r')
plot(model(:,cell), 'g')
axis('tight')
legend('True', 'Mean', 'LV model')
legend('boxoff')
title({['xcov: ' num2str(xcov(rt(:,cell), r0t(:,cell) + b0t(:,cell), 0, 'coeff'))  '   ' ...
    num2str(xcov(rt(:,cell), model(:,cell), 0, ...
    'coeff')) ], ['mse: ' num2str(sum((rt(:,cell)- (r0t(:,cell) + b0t(:,cell))).^2)) '   ' ...
    num2str(sum((rt(:,cell) - model(:,cell)).^2))]})


