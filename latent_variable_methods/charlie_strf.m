narf_set_path
% Calculate single cell STRF from PPS data
% Charlie Heller 1/20/2017

% Cell to analyze
% cellid = 'eno054c-b1';     % 99.4% isolation
cellid = 'eno049d-b1';    % 94.7% isolation

% Default is to read data in with Fs = 100. So, 10ms bins.
% r only represents data during stimulus presentation (preStim and postStim
% have been removed). r is time X reps
% stim is Time X Freq. It was repeated size(r,2) times

[r, stim, stimparam, pr] = pps_load(cellid);    

% Average the response over trials

r = mean(r,2);
nf = size(stim,2);  % Num of frequencies, used later

% Create delayed stimulus matrix

% Window = 100ms
fs = 10;
win = 100/fs;

% make delayed stim for auto-corr whic returns win of 0 delay, wind of 1
% delay etc...
% make delayed stim for cross corr

stim = reshape(stim, [size(stim,1)*size(stim,2) 1]);   % make it column

stim_delay = zeros(size(stim,1), win);
for ii = 1:win
    if ii == 1
        stim_delay(:,ii) = stim;
    else
        stim_delay(:,ii) = [stim_delay(1:ii-1,ii); stim(1:size(stim,1)-ii+1)];
    end
end

stim_delay = reshape(stim_delay, [size(stim_delay,1)/(nf) (win*nf)]);

stim = repmat(reshape(stim, [size(stim,1)/nf nf]),1,win);

% Gather correlation coefficients between stim and response (should give
% 1 X (lag*freq) vector of coeffs

Csr = zeros(win*nf, 1);
for ii = 1:(win*nf)
    Csr(ii) = stim_delay(:,ii)'*r/length(r);
end

Csr_view = reshape(Csr, [nf win]);

figure;
imagesc(Csr_view)

% Autocorrelation

Css = stim_delay'*stim_delay;%/size(stim_delay,2);

figure;
imagesc(Css)

% Now compute strf: Css-1 * Csr

% Since matrix is nearly singular, use svd (implemented by pinv) rather than inverse
if rank(Css) == size(Css,1)
    h = reshape(inv(Css)*Csr, [nf, win]);
else
    h = reshape(pinv(Css)*Csr, [nf, win]);
    warning('Using pinv')
end
if 0
    h = reshape(Csr, [nf,win]);
end
figure;
fsize = 16000;
tsize = 100;
h = interpft(interpft(h,fsize,1), tsize,2);
imagesc(h)






