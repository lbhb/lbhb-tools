% Charlie Heller

% Load continuous data for latent variable anlaysis

function [r, r_bt, trialset, trialset_bt, tags, stim_info, p, p_bt, ptrialset, ptrialset_bt, ptags] = load_cont(options)


if ~isfield(options, 'rawid')
    error('must specify rawid')
else
    rawid = options.rawid;
end

if ~isfield(options, 'miniso')
    warning('Isolation set to >84')
    miniso = '>84';
else
    miniso = options.miniso;
end

if ~isfield(options, 'fs')
    fs = 20;
else
    fs = options.fs;
end

if ~isfield(options, 'pupil')
    pupil = 0;
else
    pupil = options.pupil;
end


load_options = struct();
load_options.rawid = rawid;
load_options.rasterfs = fs;
load_options.miniso = miniso;
load_options.tag_masks = {'Reference'};
load_options.includeprestim = 1;
load_options.avg_stim = 0;

cfd = dbgetscellfile('rawid', rawid, 'isolation', miniso);

% find parameter file and spikefile
parmfile = [cfd(1).stimpath cfd(1).stimfile];
spikefile = [cfd(1).path cfd(1).respfile];

% load parameters
LoadMFile(parmfile)

% Note the set up of: silence, stim, silence (given below in sec)
prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
duration = exptparams.TrialObject.ReferenceHandle.Duration;
poststim = exptparams.TrialObject.ReferenceHandle.PostStimSilence;

% get list of unique (non-repeated) cellids
[cellids ia] = unique({cfd.cellid});
% use them to get out non-repeated units
channels =cat(1,cfd(ia).channum);
units=cat(1,cfd(ia).unit);

load_options.channel=channels;
load_options.unit=units;
load_options.meansub=0;

[r, tags, trialset] = loadsiteraster(spikefile, load_options);

load_options.tag_masks = {'SPECIAL-TRIAL'};
[r_bt, trialset_bt] = loadsiteraster(spikefile, load_options);

stim_info = struct();
stim_info.prestim = prestim;
stim_info.duration = duration;
stim_info.poststim = poststim;

if pupil
    pr_options = load_options;
    pr_options.pupil = 1;
    pr_options.pupil_median=1;
    pr_options.pupil_mm=0;
    pr_options.pupil_offset=0.75; 
    pr_options.fs = fs;
    [p, ptags, ptrialset] = loadevpraster(parmfile, pr_options);

    pr_options.tag_masks = {'SPECIAL-TRIAL'};
    [p_bt, ptrialset_bt] = loadevpraster(parmfile, pr_options);
end

end