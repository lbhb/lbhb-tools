
batch=259;
threshold2=0.007;

modelname1='env100_logn_adp2pcf_fir15_siglog100_fit05h_fit05c';
modelname2='env100_logn_dep2pcf_fir15_siglog100_fit05h_fit05c';
module='depression_filter_bank';

p1=narf_model_parms(batch,modelname1,module);
p2=narf_model_parms(batch,modelname2,module);
