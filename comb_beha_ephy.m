narf_set_path
close all
drawnow;
dbopen;

% want to save the figures? 0 no saving, 1 saving
savefigures=0

% choose the animal to analyze
animal='Babybell' 
%animal='Button' 
%animal='Magic'
%animal='Slee' %:  both Magic and Button
%animal='Tartufo' % AC PTD data from array


% chose what data set to look at 
dataset='IC_all_data'; % Babybell IC data all combined
% dataset='IC_prepassive_only'; % Babybell IC data only prepassive
% dataset='IC_postpassive_only'; % Babybell IC data only postpassive
% dataset='AC'; % Tartufo AC array data

if strcmpi(animal,'Babybell') & strcmpi(dataset, 'IC_all_data'),
    
    %     fname='/auto/users/daniela/ICdata';
    %     load(fname)
    generate_IC_TIN_db_all; % bbl IC struct array
    
    cellids={ICdata.cellid};
    
    % bbl036e-a1 has fixed torc in bbl036e08 and bbl036e09
    % bbl036e-a1 has nonfixed torc in bbl036e05 and bbl036e06
    cellidsNFix = {'bbl020c-a1','bbl020c-a3','bbl021h-a1','bbl022g-a1',...
        'bbl023c-a1','bbl023c-a2','bbl036e-a1', 'bbl071d-a1'};
    
    rawids=cell(size(cellids));
    for ii=1:length(ICdata),
        rawids{ii}=[ICdata(ii).passive_rawid ICdata(ii).active_rawid];
    end
    
elseif strcmpi(animal,'Babybell') & strcmpi(dataset, 'IC_prepassive_only'),
    
    %     fname='/auto/users/daniela/ICdata';
    %     load(fname)
    generate_IC_TIN_db_prePassive; % bbl IC struct array prepassive only
    
    cellids={ICdata.cellid};
    
    % bbl036e-a1 has fixed torc in bbl036e08 and bbl036e09
    % bbl036e-a1 has nonfixed torc in bbl036e05 and bbl036e06
    cellidsNFix = {'bbl020c-a1','bbl020c-a3','bbl021h-a1','bbl022g-a1',...
        'bbl023c-a1','bbl023c-a2','bbl036e-a1', 'bbl071d-a1'};
    
    rawids=cell(size(cellids));
    for ii=1:length(ICdata),
        rawids{ii}=[ICdata(ii).passive_rawid ICdata(ii).active_rawid];
    end
    
elseif strcmpi(animal,'Babybell') & strcmpi(dataset, 'IC_postpassive_only'),
    
    %     fname='/auto/users/daniela/ICdata';
    %     load(fname)
    generate_IC_TIN_db_postPassive; % bbl IC struct array prepassive only
    
    cellids={ICdata.cellid};
    
    % bbl036e-a1 has fixed torc in bbl036e08 and bbl036e09
    % bbl036e-a1 has nonfixed torc in bbl036e05 and bbl036e06
    cellidsNFix = {'bbl020c-a1','bbl020c-a3','bbl021h-a1','bbl022g-a1',...
        'bbl023c-a1','bbl023c-a2','bbl036e-a1', 'bbl071d-a1'};
    
    rawids=cell(size(cellids));
    for ii=1:length(ICdata),
        rawids{ii}=[ICdata(ii).passive_rawid ICdata(ii).active_rawid];
    end
       
    
% analize Tartufo AC data        
elseif strcmpi(animal,'Tartufo') & strcmpi(dataset, 'AC'),
    
    generate_AC_TIN_db; % bbl IC structure
    
    % bbl036e-a1 has fixed torc in bbl036e08 and bbl036e09
    cellids={ACdata.cellid};
    
    rawids=cell(size(cellids));
    for ii=1:length(ACdata),
        rawids{ii}=[ACdata(ii).passive_rawid ACdata(ii).active_rawid];
    end
    
    
% Analyze Sean's animals for comparison
elseif strcmpi(animal,'Button') || strcmpi(animal,'Magic') || ...
        strcmpi(animal,'Slee')
    metapath='/auto/users/svd/data/slee/Aim7Analysis/IC_DataBase3';
    cellids=cell(1,95);
    rawids=cell(1,95);
    ICdata=struct();
    for jj=1:95;
        
        %----------------------------
        %load analysis file
        fname = sprintf('%s/IC_Data_File_%d',metapath,jj);
        load(fname);
        siteid=ICDataFile.datID(1:min(findstr(ICDataFile.datID,'Ch')-1));
        channel=char(str2num(ICDataFile.datID(end-2))-1+'a');
        unit=(ICDataFile.datID(end));
        
        cellids{jj}=[siteid '-' channel unit];
        if strcmpi(cellids{jj},'btn25b-a1'), cellids{jj}='btn025b-a1'; end
        if strcmpi(cellids{jj},'btn26a-a1'), cellids{jj}='btn026a-a1'; end
        if strcmpi(cellids{jj},'btn33a-a1'), cellids{jj}='btn033a-a1'; end
        if strcmpi(cellids{jj},'btn33b-a1'), cellids{jj}='btn033b-a1'; end
        if strcmpi(cellids{jj},'btn33c-a1'), cellids{jj}='btn033c-a1'; end
        if strcmpi(cellids{jj},'btn0111b-b1'), cellids{jj}='btn111b-b1'; end
        
        sql=['SELECT * FROM gDataRaw',...
            ' WHERE parmfile like "',ICDataFile.PreFileName,'%"',...
            ' OR parmfile like "',ICDataFile.OnBFTarFileName,'%"'];
        rawdata=mysql(sql);
        rawids{jj}=cat(2,rawdata.id);
        
        if ICDataFile.IsICC,
            area='ICc';
        else
            area='ICx';
        end
        ICdata(jj).area=area;
        ICdata(jj).on_BF=1;
        %       dbsetarea(siteid,area);
    end
    
    jj=find(strcmpi(cellids,'btn079d-b2') | strcmpi(cellids,'btn118-b1'));
    cellids=cellids(setdiff(1:95,jj));
    rawids=rawids(setdiff(1:95,jj));
    
    cellidsNFix = cellids;
else
    
    % old code
    %    sql=['SELECT DISTINCT sCellFile.cellid,sCellFile.area',...
    %       ' FROM sCellFile INNER JOIN gAnimal',...
    %       ' ON left(sCellFile.cellid,3) like gAnimal.cellprefix',...
    %       ' INNER JOIN gDataRaw ON gDataRaw.id=sCellFile.rawid',...
    %       ' INNER JOIN gSingleRaw ON gSingleRaw.id=sCellFile.singlerawid',...
    %       ' WHERE gDataRaw.behavior="active"',...
    %       ' AND gSingleRaw.isolation>=90',...
    %       ' AND sCellFile.runclassid=42',...
    %       ' AND gAnimal.animal like "',animal,'"',...
    %       ' ORDER BY cellid'];
    %    cdata=mysql(sql);
    %    cellids={cdata.cellid};
    %    cellidsNFix = cellids;
end

if strcmpi(animal,'Slee')
    results_beha=varSNR_behavior('Magic');
    r2=varSNR_behavior('Button');
    ff=fields(results_beha);
    for ii=1:length(ff),
        results_beha.(ff{ii})=cat(1,results_beha.(ff{ii}),r2.(ff{ii}));
    end
else
    results_beha=varSNR_behavior(animal);
end

if savefigures,
    set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
    saveas(gcf, ['/auto/users/daniela/docs/AC_IC_project/eps_files_bbl/comb_beha_ephy_saved_figures/'...
        dataset '_behavior_analysis.tiff']);
    close(gcf);
end

%% load gain data
results_beha.gain = nan(length(results_beha.rawid),3);
results_beha.gainZ = nan(length(results_beha.rawid),3);
results_beha.spont_active = nan(size(results_beha.rawid));
results_beha.spont_passive = nan(size(results_beha.rawid));
results_beha.spontZ = nan(length(results_beha.rawid),3);
results_beha.targain = nan(length(results_beha.rawid),3);
results_beha.targainZ = nan(length(results_beha.rawid),3);
results_beha.singleid = nan(size(results_beha.rawid));
results_beha.tardist = nan(size(results_beha.rawid));
results_beha.tardiff = nan(size(results_beha.rawid));
results_beha.on_BF = nan(size(results_beha.rawid));
results_beha.ICc = nan(size(results_beha.rawid));
results_beha.cellid = cell(size(results_beha.rawid));
results_beha.area = cell(size(results_beha.rawid));
results_beha.strfc = cell(size(results_beha.rawid));
results_beha.strfsnr = nan(size(results_beha.rawid,1),2);
results_beha.strfgain = nan(size(results_beha.rawid,1),1);
results_beha.ref_vs_tar_pass = nan(size(results_beha.rawid,1),2);
results_beha.ref_vs_tar_act = nan(size(results_beha.rawid,1),2);


for ii=1:length(rawids),
    
    % WHY DO WE DEFINE options struct here if we redefine it in
    % ptd_gain_comp??? WHY is firstpassiveonly=2 and then it's set to 1?
    
    %     options=struct('firstpassiveonly',2,'runclass','PTD,TOR','psthfs',50,'binsize',50,...
    %         'gainAnalysis','Traditional');
    
    % figurepath changes depending on what dataset is run in prev cell
    
    if strcmpi(dataset, 'AC'),
        options=struct('firstpassiveonly',2,'runclass','PTD,TOR','psthfs',50,'binsize',2,...
            'figurepath', '/auto/users/daniela/docs/AC_IC_project/eps_files_bbl/comb_beha_ephy_saved_figures/AC_data_Tartufo/');
        
    elseif strcmpi(dataset, 'IC_prepassive_only'),
        options=struct('firstpassiveonly',2,'runclass','PTD,TOR','psthfs',50,'binsize',2,...
            'figurepath', '/auto/users/daniela/docs/AC_IC_project/eps_files_bbl/comb_beha_ephy_saved_figures/IC_data_preP/');
        
    elseif strcmpi(dataset, 'IC_postpassive_only'),
        options=struct('firstpassiveonly',2,'runclass','PTD,TOR','psthfs',50,'binsize',2,...
            'figurepath', '/auto/users/daniela/docs/AC_IC_project/eps_files_bbl/comb_beha_ephy_saved_figures/IC_data_postP/');
        
    else
        options=struct('firstpassiveonly',2,'runclass','PTD,TOR','psthfs',50,'binsize',2,...
            'figurepath', '/auto/users/daniela/docs/AC_IC_project/eps_files_bbl/comb_beha_ephy_saved_figures/IC_data_all/');
    end
    
    if ~isempty(rawids{ii}),
        options.rawid=rawids{ii};
    end
    
    % compute gain for a single cell:
    results_gain=ptd_gain_comp(cellids{ii},options);
    
    if ~ismember(cellids{ii},cellidsNFix),
        toptions=options;
        toptions.datause='Target Only';
        results_tar=ptd_gain_comp(cellids{ii},toptions);
    end
    
    for idx = 1:length(results_gain.rawid),
        
        jj = min(find(results_beha.rawid == results_gain.rawid(idx), 1));
        
        if ~isnan(results_beha.gain(jj)),
           % not the first time this rawid has appeared (>1 cell at site)
           ff=fields(results_beha);
           for kk=1:length(ff),
              results_beha.(ff{kk})=cat(1,results_beha.(ff{kk}),...
                 results_beha.(ff{kk})(jj,:));
           end
           jj=length(results_beha.rawid);
        end
        if ~isempty(jj)
           % save ephys results
           results_beha.cellid{jj}=cellids{ii};
           results_beha.area{jj}=results_gain.area{idx};
           results_beha.gain(jj,:) = results_gain.gain(idx,:);
           results_beha.gainZ(jj,:) = results_gain.gainZ(idx,:);
           results_beha.spont_active(jj) = results_gain.spont_active(idx);
           results_beha.spont_passive(jj) = results_gain.spont_passive(idx);
           results_beha.spontZ(jj,:) = results_gain.spontZ(idx,:);
           if ~ismember(cellids{ii},cellidsNFix),
              results_beha.targain(jj,:) = results_tar.gain(idx,:,1);
              results_beha.targainZ(jj,:) = results_tar.gainZ(idx,:,1);
              results_beha.ref_vs_tar_pass(jj,:) = results_tar.ref_vs_tar_pass;
              results_beha.ref_vs_tar_act(jj,:) = results_tar.ref_vs_tar_act;
           end
           results_beha.singleid(jj) = results_gain.singleid(idx);
           results_beha.tardist(jj) = results_gain.tardist(idx);
           results_beha.tardiff(jj) = results_gain.tardiff(idx);
           results_beha.strfc{jj} = results_gain.strfc;
           results_beha.strfsnr(jj,:) = results_gain.strfsnr;
           results_beha.strfgain(jj) = results_gain.strfgain;
           
           
           results_beha.on_BF(jj)=ICdata(ii).on_BF;
          
           if strcmpi(ICdata(ii).area,'ICx'),
               results_beha.ICc(jj)=0;
           else
               results_beha.ICc(jj)=1;
           end
        end
    end
end

%% stats 
results_beha.on_BF(isnan(results_beha.on_BF))=-1;

epoch=2;

% all cells
% goodblocks=find(~isnan(results_beha.gain(:,epoch)));

% on BF only
goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.on_BF==1);

% off BF only
%goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.on_BF==0);

% ICc only
%goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.ICc==1);

% ICx only
%goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.ICc==0);

% easy only
%goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.difficulty==1);

% % hard only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.difficulty==3);

% % passive responses enhanced by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_pass(:,1)) &...
%     results_beha.ref_vs_tar_pass(:,1)>results_beha.ref_vs_tar_pass(:,2));

% % passive responses suppressed by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_pass(:,1)) &...
%     results_beha.ref_vs_tar_pass(:,1)<results_beha.ref_vs_tar_pass(:,2));

% % active responses enhanced by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_act(:,1)) &...
%     results_beha.ref_vs_tar_act(:,1)>results_beha.ref_vs_tar_act(:,2));

% % active responses suppressed by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_act(:,1)) &...
%     results_beha.ref_vs_tar_act(:,1)<results_beha.ref_vs_tar_act(:,2));

rawid=results_beha.rawid(goodblocks);
probeDI=results_beha.probeDI(goodblocks);
gain=results_beha.gain(goodblocks,epoch);
gainZ=results_beha.gainZ(goodblocks,epoch);
targain=results_beha.targain(goodblocks,epoch);
targainZ=results_beha.targainZ(goodblocks,epoch);
spont=results_beha.spont_active(goodblocks)-results_beha.spont_passive(goodblocks);
spontZ=results_beha.spontZ(goodblocks,epoch);
difficulty=results_beha.difficulty(goodblocks);
difficulty(difficulty>2)=2;
block_order=results_beha.block_order(goodblocks);
singleid=results_beha.singleid(goodblocks);
unique_singleid=unique(singleid(~isnan(gain))'); % use all data
tardist=results_beha.tardist(goodblocks);
tardiff=results_beha.tardiff(goodblocks);
strfgain=results_beha.strfgain(goodblocks);

probeHR=results_beha.tokenprobeHR(goodblocks);
probeFAR=results_beha.tokenFAR(goodblocks);
probeHRz=results_beha.tokenprobeHRz(goodblocks);
probeFARz=results_beha.tokenFARz(goodblocks);
% probeHR=results_beha.probeHR(goodblocks);
FAR=results_beha.FAR(goodblocks);

area=results_beha.area(goodblocks);
% areacat=ones(size(area));
% areacat(strcmpi(area,'ICx'))=2;
areacat=ones(size(area))+1;
areacat(strcmpi(area,'ICc'))=1;

dprime=probeHR-probeFAR;
bias=-(probeHR+probeFAR)./2;
dprimeZ=probeHRz-probeFARz;
%biasZ=-(probeHRz+probeFARz)./2;
f_rat=probeFAR./(probeFAR+probeHR);
f_rat(f_rat<0.05) = 0.05;
f_rat(f_rat>0.95) = 0.95;
biasZ=norminv(f_rat);

%bias=((1-probeHR).*(1-probeFAR))./(probeHR.*probeFAR)./2;
%bias=log(bias+1);
%bias=-0.5*(log(probeHR./(1-probeHR))+log(probeFAR./(1-probeFAR)));

strfc=cat(4,results_beha.strfc{goodblocks});
strfc(strfc==0)=nan;
strfd=strfc(:,:,1,:)-strfc(:,:,2,:);  % act-pas
deltaA=squeeze(mean(mean(strfd(50:51,12,:),1),2));
%gain=deltaA+1;

col=[.8 .8 .8];

%F2=abs(results_beha.tardist(goodblocks)) < 1;
F2=ones(size(goodblocks));

% figures
figure;

subplot(3,4,1);

xx=linspace(-1,1,20);
gsig=abs(gainZ)>2;
gnsig=abs(gainZ)<=2;
histcomp(gain(gsig & F2)-1,gain(gnsig & F2)-1,'gain sig','NS','pct',[-1 1],1,16,1);

subplot(3,4,5);

ssig=abs(spontZ)>2;
snsig=abs(spontZ)<=2;
histcomp(spont(ssig),spont(snsig),'spont sig','NS','',[-10 10],1,16,1);

for ii=[2:4 6:8],
   switch ii,
      case 2, 
         X=bias;
         Y=gain;
         Yz=gainZ;
         Xlabel='bias';
         Ylabel='ref gain';
      case 3,
         X=dprime;
         Y=gain;
         Yz=gainZ;
         Xlabel='dprime';
         Ylabel='ref gain';
      case 4,
         X=probeDI;
         Y=gain;
         Yz=gainZ;
         Xlabel='probeDI';
         Ylabel='ref gain';
      case 6,
         X=biasZ;
         Y=gain;
         Yz=gainZ;
         Xlabel='biasZ';
         Ylabel='gain';
      case 7,
         X=dprimeZ;
         Y=gain;
         Yz=gainZ;
         Xlabel='dprimeZ';
         Ylabel='gain';
      case 8,
         X=probeHRz;
         Y=gain;
         Yz=gainZ;
         Xlabel='probeHRz';
         Ylabel='gain';
   end
   
   subplot(3,4,ii);
   
   gg=find(~isnan(X) & ~isnan(Y));
   X=X(gg);
   Y=Y(gg);
   Yz=Yz(gg);
   medX=median(X);
   mX=[mean(X(X<=medX)) mean(X(X>medX))];
   mean_Y=[mean(Y(X<=medX)) mean(Y(X>medX))];
   n=[sum(X<=medX) sum(X>medX)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   
   for mid=unique_singleid,
      jj=find(singleid(gg)==mid);
      plot(X(jj),Y(jj),'-o','Color',col);
      hold on
   end
   plot(X(abs(Yz)>2 & F2(gg)),Y(abs(Yz)>2 & F2(gg)),'ko');
   %plot(X(areacat==1 & F2),Y(areacat==1 & F2),'ko');
   errorbar(mX,mean_Y,se_Y,'r','LineWidth',2);
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   
   cXY=xcov(X,Y,0,'coeff');
   title(sprintf('cc(X,Y)=%.3f',cXY));
end

subplot(3,4,9);

Y=gain;
Ystr='gain';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud),
   didx=find(X==ud(diff));
   
   mean_Y(diff)=mean(Y(didx));
   se_Y(diff)=std(Y(didx))./sqrt(length(didx));
end
savediff=[];
for mid=unique_singleid,
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   f1=find(X(ii)==2);
   f2=find(X(ii)==3);
   if ~isempty(f1) && ~isempty(f2),
      savediff=cat(1,savediff,[Y(ii(f1(1))) Y(ii(f2(1)))]);
   end
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);


subplot(3,4,10);

Y=dprimeZ;
Ystr='dprimeZ';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud),
   didx=find(X==ud(diff));
   
   mean_Y(diff)=mean(Y(didx));
   se_Y(diff)=std(Y(didx))./sqrt(length(didx));
end
for mid=unique_singleid,
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);


subplot(3,4,11);

Y=biasZ;
Ystr='biasZ';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud),
   didx=find(X==ud(diff));
   
   mean_Y(diff)=nanmean(Y(didx));
   se_Y(diff)=nanstd(Y(didx))./sqrt(length(didx));
end
for mid=unique_singleid,
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);

subplot(3,4,12);

Y=probeDI;
Ystr='probeDI';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud),
   didx=find(X==ud(diff));
   
   mean_Y(diff)=nanmean(Y(didx));
   se_Y(diff)=nanstd(Y(didx))./sqrt(length(didx));
end
for mid=unique_singleid,
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);


% subplot(3,4,12);
% timematrix=sortrows([rawid probeDI./100 probeFAR gain]);
% plot(timematrix(:,2:end));
% legend('probeDI','FAR','gain');
% xlabel('time');

fullpage landscape

if savefigures,
    set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
    saveas(gcf, [options.figurepath dataset '_gain_analysis.tiff']);
    close(gcf);
end


gtaridx=find(~isnan(targain) & targain>0.1 & targain<10);
if isempty(gtaridx),
   return
end


figure;

subplot(2,3,1);
Y=gain;
Yz=gainZ(gtaridx);
A=gain(gtaridx);
B=targain(gtaridx);
catidx=ones(size(A));
catidx(abs(Yz)<=2)=2;
% catidx(bias(gtaridx)>median(bias(gtaridx)))=2;
% catidx(areacat(gtaridx)==2)=2;
plotcomp(gain(gtaridx),targain(gtaridx),'refgain','targain',[],catidx);

for ii=[4:4],
   switch ii,
      case 4
         A=targain(gtaridx)-gain(gtaridx);
         %D=(biasZ(gtaridx)>median(biasZ(gtaridx)))+1;
         %Xlabel='Bias (lo/hi)';
         %D=difficulty(gtaridx);
         %Xlabel='Difficulty (lo/hi)';
         %D=(probeDI(gtaridx)>median(probeDI(gtaridx)))+1;
         %Xlabel='probeDI (lo/hi)';
         D=(dprimeZ(gtaridx)>median(dprimeZ(gtaridx)))+1;
         Xlabel='Dprime (lo/hi)';
         
         Ylabel='tar-ref gain';
   end
   F3=F2(gtaridx);
   
   subplot(2,3,ii);
   
   ud=unique(D);
   mean_A=zeros(length(ud),1);
   se_A=zeros(length(ud),1);
   for diff=1:length(ud),
      didx=find(D==ud(diff));
      
      mean_A(diff)=mean(A(didx));
      se_A(diff)=std(A(didx))./sqrt(length(didx));
   end
   for mid=unique_singleid,
      jj=find(singleid(gtaridx)==mid);
      if ~isempty(jj),
         plot(D(jj),A(jj),'-o','Color',col);
         hold on
      end
   end
   errorbar(mean_A,se_A,'r');
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   aa=axis;
   axis([0 max(ud)+1 -0.5 0.5]);
end

for ii=[2 3 5 6],
   switch ii,
      case 2
         X=biasZ(gtaridx);
         Y=gain(gtaridx);
         Yz=gainZ(gtaridx);
         Xlabel='BiasZ';
         Ylabel='ref gain';
      case 3
         X=biasZ(gtaridx);
         Y=targain(gtaridx);
         Yz=targainZ(gtaridx);
         Xlabel='BiasZ';
         Ylabel='tar gain';
      case 5
         X=dprimeZ(gtaridx);
         Y=gain(gtaridx);
         Yz=gainZ(gtaridx);
         Xlabel='dprimeZ';
         Ylabel='ref gain';
      case 6
         X=dprimeZ(gtaridx);
         Y=targain(gtaridx);
         Yz=targainZ(gtaridx);
         Xlabel='dprimeZ';
         Ylabel='tar gain';
   end
   F3=F2(gtaridx);
   
   subplot(2,3,ii);
   medX=median(X);
   mX=[mean(X(X<=medX)) mean(X(X>medX))];
   mean_Y=[mean(Y(X<=medX)) mean(Y(X>medX))];
   n=[sum(X<=medX) sum(X>medX)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   
   for mid=unique_singleid,
      ii=find(singleid(gtaridx)==mid);
      plot(X(ii),Y(ii),'-o','Color',col);
      hold on
   end
   plot(X(abs(Yz)>2 & F3),Y(abs(Yz)>2 & F3),'ko');
   errorbar(mX,mean_Y,se_Y,'r');
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   cXY=xcov(X,Y,0,'coeff');
   ceXY=xcov(exp(X),Y,0,'coeff');
   title(sprintf('cc(X,Y)=%.3f',cXY));
end

fullpage landscape

if savefigures,
    set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
    saveas(gcf, [options.figurepath dataset '_RefTar_gain_analysis.tiff']);
    close(gcf);
end

%% local/global analysis
strfsnr=results_beha.strfsnr(goodblocks);
strfgoodblocks=goodblocks(strfsnr>0.4);
strfsnr=results_beha.strfsnr(strfgoodblocks);
strfdifficulty=results_beha.difficulty(strfgoodblocks);

gainstrf=strfgain(ismember(goodblocks,strfgoodblocks));
iccstrf=results_beha.ICc(strfgoodblocks);
onBFstrf=results_beha.on_BF(strfgoodblocks);

strfc=cat(4,results_beha.strfc{strfgoodblocks});
strfc(strfc==0)=nan;

A=squeeze(mean(mean(strfc(50:51,12,:,:),1),2))';
deltaA=A(:,1)-A(:,2);
deltaAlocal=A(:,1)-A(:,2).*gainstrf;
%deltaAlocal=A(:,1)-A(:,3);

figure;
subplot(3,3,1);
strfd=strfc(21:80,1:50,1,:)-strfc(21:80,1:50,2,:);  % act-pas
strfn=sum(~isnan(strfd),4);
strfd=nanmean(strfd,4);
stplot(strfd + flipud(strfd), 500, 62.5, 0, 4);
colorbar

%stplot(stdata,lfreq,tleng,smooth,noct,clim,siglev)

subplot(3,3,2);
hist(deltaA);
xlabel('deltaA');

subplot(3,3,3);
stplot(strfn);
colorbar

subplot(3,3,7);
hist(deltaAlocal);
xlabel('deltaAlocal');

subplot(3,3,4);
plot(gainstrf(find(onBFstrf)),deltaA(find(onBFstrf)),'r.');
hold on
plot(gainstrf(find(~onBFstrf)),deltaA(find(~onBFstrf)),'b.');
hold off
xlabel('global gain');
ylabel('deltaA');

subplot(3,3,5);
plot(gainstrf(find(onBFstrf)),deltaAlocal(find(onBFstrf)),'r.');
hold on
plot(gainstrf(find(~onBFstrf)),deltaAlocal(find(~onBFstrf)),'b.');
hold off
xlabel('global gain');
ylabel('deltaAlocal');

subplot(3,3,6);
plot(A(find(onBFstrf),2),deltaA(find(onBFstrf)),'r.');
hold on
plot(A(find(~onBFstrf),2),deltaA(find(~onBFstrf)),'b.');
hold off
xlabel('A passive');
ylabel('deltaA');
legend('on BF','off BF');
legend boxoff


for ii=[8:9],
   switch ii,
      case 8, 
         X=difficulty(ismember(goodblocks,strfgoodblocks));
         Y=gain(ismember(goodblocks,strfgoodblocks));
         Yz=gainZ(ismember(goodblocks,strfgoodblocks));
         Xlabel='difficulty';
         Ylabel='strfgain';
      case 9, 
         X=difficulty(ismember(goodblocks,strfgoodblocks));
         Y=deltaAlocal;
         Yz=ones(size(Y))+2;
         Xlabel='difficulty';
         Ylabel='deltaAlocal';
   end
   
   subplot(3,3,ii);
   
   gg=find(~isnan(X) & ~isnan(Y));
   X=X(gg);
   Y=Y(gg);
   Yz=Yz(gg);
   
   for mid=unique_singleid,
      jj=find(singleid(gg)==mid);
      plot(X(jj),Y(jj),'-o','Color',col);
      hold on
   end
   plot(X(abs(Yz)>2 & F2(gg)),Y(abs(Yz)>2 & F2(gg)),'ko');
   %plot(X(areacat==1 & F2),Y(areacat==1 & F2),'ko');
   
   medX=median(X);
   mX=[mean(X(X<=medX)) mean(X(X>medX))];
   mean_Y=[mean(Y(X<=medX & onBFstrf)) mean(Y(X>medX & onBFstrf))];
   n=[sum(X<=medX & onBFstrf) sum(X>medX & onBFstrf)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   errorbar(mX,mean_Y,se_Y,'r','LineWidth',2);
   mean_Y=[mean(Y(X<=medX & ~onBFstrf)) mean(Y(X>medX & ~onBFstrf))];
   n=[sum(X<=medX & ~onBFstrf) sum(X>medX & ~onBFstrf)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   errorbar(mX,mean_Y,se_Y,'b','LineWidth',2);
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   
   cXY=xcov(X,Y,0,'coeff');
   title(sprintf('cc(X,Y)=%.3f',cXY));
end

if savefigures,
    set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
    saveas(gcf, [options.figurepath 'STRF_stats.tiff']);
    close(gcf);
end
