function ssa_data_browser
  
  narf_set_path;
  dbopen;
  
  %create figure window and associated data
  f = figure('Position', [300 300 210 200]);
  h = guidata(f);
  %h.modelname = 'env100_logn_adp1pcno_fir15_siglog100_fit05h_fit05c';
  %h.modelname = 'env100_logn_adp2pcf_fir15_siglog100_fit05h_fit05c';
  h.modelname = 'env100_logn_dep2pcf_fir15_siglog100_fit05h_fit05c';
  %h.modelname = 'env100_logn_dep2pcf_fir15_zsoft_fit05h_fit05c';
  guidata(f, h)

  %create a menu of all SSA/SPN cells
  ssa_cells = dbgetscellfile('runclass', 'SSA');
  ssa_cellids = {ssa_cells.cellid};
  spn_cells = mysql(['SELECT cellid FROM NarfResults', ...
    ' WHERE modelname like "', h.modelname, '"',...
    ' AND batch=259']);
  spn_cellids = {spn_cells.cellid};
  cellids = intersect(ssa_cellids, spn_cellids);
  cell_list = uicontrol('Style', 'listbox', ...
    'Position', [0, 0, 100, 200], ...
    'String', cellids, ...
    'Callback', @select_cell);

  %create plot button
  plot_button = uicontrol('Style', 'pushbutton', ...
    'String', 'Plot', ...
    'Position', [110 0 100 200], ...
    'Callback', @do_plot);
end

function select_cell(hObject, callbackdata)
  %select a cell to plot
  h = guidata(gcbo);
  cellids = get(hObject, 'String');
  idx = get(hObject, 'Value');
  h.cellid = cellids{idx};
  guidata(gcbo, h)
end

function do_plot(hObject, callbackdata)
  %lay out SSA and SPN data plots on a single figure
  h = guidata(gcbo);
  f = figure('Position', [900, 80, 750, 900]);
  ax1 = subplot(4, 2, 1:2);
  ax2 = subplot(4, 2, 3:4);
  ax3 = subplot(4, 2, 5);
  ax4 = subplot(4, 2, 6);
  ax5 = subplot(4, 2, 7);
  ax6 = subplot(4, 2, 8);
  ssa_options = struct('fig', f, 'psth_ax1', ax1, 'psth_ax2', ax2,...
    'raster_ax', 0, 'add_title', 0);
  spn_options = struct('fig', f, 'psth_ax', ax3, 'fir_ax', ax4, ...
    'cartoon_ax1', ax5, 'cartoon_ax2', ax6);
  ssa_plot(h.cellid, ssa_options);
  spn_plot(h.cellid, h.modelname, spn_options)
  t = suptitle([h.cellid ' ' h.modelname], 0);
  set(t, 'Interpreter', 'none')
end

function spn_plot(cellid, modelname, options)
  %input parsing
  if (~exist('options', 'var'))
    fig = figure;
    ax1 = subplot(3,2,1:2);
    ax2 = subplot(3,2,3:4);
    ax3 = subplot(3,2,5);
    ax4 = subplot(3,2,6);
    options = struct('fig', fig, ...
                     'psth_ax', ax1, ...
                     'fir_ax', ax2, ...
                     'cartoon_ax1', ax3, ...
                     'cartoon_ax2', ax4); 
  end
  %load and cache data
  spn_cells = dbgetscellfile('runclass','SPN');
  n = find(strcmp({spn_cells.cellid}, cellid));
  cellfiledata = spn_cells(n);
  LoadMFile([cellfiledata.stimpath cellfiledata.stimfile]);
  cache = '/auto/users/schwarza/code/ssa/cache/';
  f = [cache cellid '_' modelname '.mat'];
  if exist(f, 'file')
    load(f);
  else
    [~, m] = spn_narf_load(cellid, 259, modelname, modelname);
    save(f, 'm', 'exptparams', 'cellfiledata')
  end
  %calculate stimulus frequencies
  l = exptparams.TrialObject.ReferenceHandle.LowFreq;
  h = exptparams.TrialObject.ReferenceHandle.HighFreq;
  freqs = (h+l)./2;
  freq_str = {sprintf('%0.1f kHz', freqs(1)/1000),
              sprintf('%0.1f kHz', freqs(2)/1000)};
  %plot psth, depression cartoon, and strf
  axes(options.psth_ax)
  prestim = exptparams.TrialObject.ReferenceHandle.PreStimSilence;
  dur = exptparams.TrialObject.ReferenceHandle.Duration;
  if strfind(modelname,'env100')
    fs = 100;
    tt = ((1:size(m.avg_fit_psth,1)).*(1/fs))-prestim;
    hold on
    plot(tt, gsmooth(m.avg_fit_psth).*fs, 'c', 'LineWidth', 2)
    axis tight
    aa = axis;
    plot([0 0],aa(3:4),'g--')
    plot([0 0]+dur,aa(3:4),'g--')
    hold off
    xlabel('Time From Stimulus Onset (s)')
    ylabel('Spikes/s')
  else
    plot(gsmooth(m.avg_fit_psth), 'c', 'LineWidth', 2)
  end
  str = {sprintf('Strength F1 %0.2f ', m.depression_strength(:,1)),
	 sprintf('Strength F2 %0.2f ', m.depression_strength(:,2)),
         sprintf('Tau F1 %d ', m.depression_tau(:,1)),
	 sprintf('Tau F2 %d ', m.depression_tau(:,2)),
         'Depression Magnitude',
         sprintf('F1 %0.2f, F2 %0.2f', m.depression_magnitude(1), ...
	                               m.depression_magnitude(2)),
	 sprintf('Prediction Correlation: %0.2f', m.r_test)};
  text(0.1,0.5, str, 'Units', 'normalized')
  title('SPN PSTH')
  axes(options.fir_ax)
  fir_abs_max = max(max(abs(m.fir_coefs(:,:))));
  imagesc(m.fir_coefs(:,:),[-fir_abs_max fir_abs_max])
  set(options.fir_ax, 'YTick', [1 2], 'YTickLabel', freq_str, ...
                      'XTick', [0 5 10 15], 'XTickLabel', {'0' '50' '100' '150'})
  xlabel('Delay (ms)')
  title('FIR Filter')
  axes(options.cartoon_ax1)
  tt = (1:size(m.depression_response,2))./100;
  plot(tt, m.depression_response(1,:)')
  axis tight
  title(['Depression Cartoon ' freq_str{1}])
  xlabel('Time (s)')
  ylabel('Depressed Stimulus');
  axes(options.cartoon_ax2)
  plot(tt, m.depression_response(2,:)')
  axis tight
  xlabel('Time (s)')
  ylabel('Depressed Stimulus');
  title(['Depression Cartoon ' freq_str{2}])
end
