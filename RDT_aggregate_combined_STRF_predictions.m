narf_set_path();
dbopen();

models = {
	%'fbSNS18ch100_wc03_fir15_fit05a', 
	%'fbSNS18ch100_wc03_fir15_siglog100_fit05h'
	'fbSNS18ch100_wc02_fir15_fit05a',
	%'fbSNS18ch100_wc04_fir15_fit05a',
	%'fbSNS18ch100_wc03_adp1pc_fir15_fit05a',
	};

modelpath = '/auto/users/bburan/code/raw_files';
savefile = fullfile(modelpath, 'STRF_fit_results_L_NL');

% One batch is all A1 cells, the other is all PEG cells.  Just combine them for
% the sake of analysis.
if length(models) == 1
	modelnames = sprintf('"%s"', models{end});
else
	modelnames = [sprintf('"%s", ', models{1:end-1}), sprintf('"%s"', models{end})];
end

sql = 'SELECT * FROM NarfResults WHERE modelname in (%s) AND batch in (273, 269)';
%sql = 'SELECT * FROM NarfResults WHERE modelname in (%s) AND batch in (284, 285)';

sql = sprintf(sql, modelnames);
model_fits = mysql(sql);
if isempty(model_fits),
	error('Model data not found in NARF');
end

% Required to bootstrap narf and get the data we need
global XXX FLATXXX META

load(savefile)
ids = 1:length(results);
for j = 1:length(results),
	ids(j) = results{j}.model.id;
end
%results = cell(1, length(model_fits));
%results = {};
%ids = [];

for j = 1:length(model_fits),
	model = model_fits(j);
	if any(ids == model.id),
		fprintf(1, 'Skipping cell %s\n', model.cellid);
	else
		fprintf(1, 'Processing cell %s (%d of %d)\n', model.cellid, j, length(model_fits));
		modelpath = char(model.modelpath);
		[pred1, pred2, predboth, resp, meta] = RDT_pred_stream_from_modelpath(modelpath);
		meta.cellid = model.cellid;

		X = struct();
		X.predboth = predboth;
		X.pred1 = pred1;
		X.pred2 = pred2;
		X.resp = resp;
		X.meta = meta;
		X.model = model;
		results{end+1} = X;
	end
end
save(savefile, 'results');
