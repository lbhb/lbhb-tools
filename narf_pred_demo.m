% narf_pred_demo

cellid='eno027d-c1';
modelname='env100_logn_adp1pcno_fir15_siglog100_fit05h_fit05c';
batch=259;

stimfile='eno027d03_p_SPN';

[pred,resp,fs,perf]=load_narf_prediction(cellid,modelname,batch,stimfile);


dbopen
sql=['SELECT * FROM gDataRaw WHERE parmfile LIKE "',stimfile,'%"'];
rawdata=mysql(sql);
baphyparms=dbReadData(rawdata.id);

prebins=round(baphyparms.Ref_PreStimSilence.*fs);
postbins=round(baphyparms.Ref_PostStimSilence.*fs);

m_pred=nanmean(pred(:,:),2);
m_resp=nanmean(resp(:,:),2);
minY=min([m_pred;m_resp]);
maxY=max([m_pred;m_resp]);

figure;
plot([m_pred m_resp]);
hold on
plot([prebins prebins],[minY maxY],'g--');
plot(size(m_pred,1)-postbins+[0 0],[minY maxY],'g--');
hold off
legend('predicted','actual');