clear all; close all;

baphy_set_path

dbopen(1);
fs=50;

channel_no=input('Which channel? ');
unit_no=input('Which unit? ');
filename=input('What is the fig filename? ','s');

%% before training
spkfile='/auto/data/daq/Pennybun/PNB020/sorted/PNB020a08_p_TON.spk.mat';
options=struct('channel',channel_no,'unit',unit_no,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'Reference'}}); 
[r, tags]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id

% average across repetitions to get the PSTH
r2=nanmean(r,2);
psth1 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

clear r
clear r2

s1=strsep(tags{1} ,',');
s2=strsep(tags{2} ,',');
%% Training, BF tone with reward
spkfile='/auto/data/daq/Pennybun/PNB020/sorted/PNB020a10_p_PTD.spk.mat';
options=struct('channel',channel_no,'unit',unit_no,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'TARGET'}}); 
[r, tags]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id

% average across repetitions to get the PSTH
r2=nanmean(r,2);
psth2 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

clear r
clear r2

s3=strsep(tags{1} ,',');
s4=strsep(tags{2} ,',');

%% after training
spkfile='/auto/data/daq/Pennybun/PNB020/sorted/PNB020a11_p_TON.spk.mat';
options=struct('channel',channel_no,'unit',unit_no,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'Reference'}}); 
[r, tags]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id

% average across repetitions to get the PSTH
r2=nanmean(r,2);
psth3 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

clear r
clear r2

%% plotting
prestimsilence=0.5; % 0.5 sec prestimulus silence
ymax=max(max([psth1;psth2;psth3]));
ymin=min(min([psth1;psth2;psth3]));

cond=input('BF + reward=1 or non-BF + reward=2? ');

timepoints=(1:size(psth1,1))./options.rasterfs - prestimsilence;
figure;
clf

subplot(2,3,1)
plot(timepoints,psth1)
xlim([-1 5]);
ylabel('spike/sec');
xlabel('time (s)');

legend(num2str(s1{2}),num2str(s2{2}));
title('before training');
ylim([ymin ymax+10])
line([0 0],[ymin ymax+10],'Color','black','LineStyle','--')
line([1 1],[ymin ymax+10],'Color','black','LineStyle','--')

% firing rate analysis
prestim_id = find(timepoints <= 0 & timepoints > -0.1);
onset_id= find(timepoints <= 0.05 & timepoints > 0);
sustain_id= find(timepoints <= 1 & timepoints > 0.05);
offset_id= find(timepoints <= 1.1 & timepoints > 1);

prestim_b4=mean(psth1(prestim_id,1));
data1_b4(1)=mean(psth1(onset_id,1))-prestim_b4;  % onset
data1_b4(2)=mean(psth1(sustain_id,1))-prestim_b4; %sustain
data1_b4(3)=mean(psth1(offset_id,1))-prestim_b4; % offset

data1_b4'

% plotting
timepoints=(1:size(psth2,1))./options.rasterfs - prestimsilence;

subplot(2,3,2)
plot(timepoints,psth2(:,1),'b-')
hold on; plot(timepoints,psth2(:,2),'k-')
xlim([-1 5]);
ylabel('spike/sec');
xlabel('time (s)');


if cond==1 
legend([num2str(s3{2}) 'Reward'],[num2str(s4{2}) 'No reward']);
title('BF tone + Reward');
elseif cond==2
legend([num2str(s4{2}) 'Reward'],[num2str(s3{2}) 'No reward']);
title('nonBF tone + Reward');
end
ylim([ymin ymax+10])
line([0 0],[ymin ymax+10],'Color','black','LineStyle','--')
line([1 1],[ymin ymax+10],'Color','black','LineStyle','--')

% plotting
timepoints=(1:size(psth3,1))./options.rasterfs - prestimsilence;

subplot(2,3,3)
plot(timepoints,psth3)
xlim([-1 5]);
ylabel('spike/sec');
xlabel('time (s)');

legend(num2str(s1{2}),num2str(s2{2}));
title('after VNS');
ylim([ymin ymax+10])
line([0 0],[ymin ymax+10],'Color','black','LineStyle','--')
line([1 1],[ymin ymax+10],'Color','black','LineStyle','--')

% firing rate analysis

prestim_aft=mean(psth3(prestim_id,1));
data1_aft(1)=mean(psth3(onset_id,1))-prestim_aft;
data1_aft(2)=mean(psth3(sustain_id,1))-prestim_aft;
data1_aft(3)=mean(psth3(offset_id,1))-prestim_aft;

data1_aft'

saveas(gcf,filename,'fig');

% % plotting
% 
% cond=input('2nd set, BF=1 or non-BF=2? ');
% pair=input('paired=1 or unpaired=2? ');
% 
% timepoints=(1:size(psth4,1))./options.rasterfs - prestimsilence;
% 
% subplot(2,3,4)
% plot(timepoints,psth4)
% xlim([-1 5]);
% ylabel('spike/sec');
% xlabel('time (s)');
% s1=strsep(tags{1} ,',');
% s2=strsep(tags{2} ,',');
% legend(num2str(s1{2}),num2str(s2{2}));
% title('before VNS');
% ylim([ymin ymax+10])
% line([0 0],[ymin ymax+10],'Color','black','LineStyle','--')
% line([1 1],[ymin ymax+10],'Color','black','LineStyle','--')
% 
% % firing rate analysis
% prestim_b4_2=mean(psth4(prestim_id,1));
% data2_b4(1)=mean(psth4(onset_id,1))-prestim_b4_2;
% data2_b4(2)=mean(psth4(sustain_id,1))-prestim_b4_2;
% data2_b4(3)=mean(psth4(offset_id,1))-prestim_b4_2;
% 
% data2_b4'
% 
% % plotting
% timepoints=(1:size(psth5,1))./options.rasterfs - prestimsilence;
% 
% subplot(2,3,5)
% plot(timepoints,psth5(:,1),'b-')
% hold on; plot(timepoints,psth5(:,2),'k-')
% xlim([-1 5]);
% ylabel('spike/sec');
% xlabel('time (s)');
% ylim([0 max(max(ymax))+10])
% 
% 
% if cond==1 & pair==1
% legend([num2str(s1{2}) num2str(s3{2})],[num2str(s1{2}) num2str(s4{2})]);
% title('VNS + BF tone (paired)');
% elseif cond==1 & pair==2
% legend([num2str(s1{2}) num2str(s3{2})],[num2str(s1{2}) num2str(s4{2})]);
% title('VNS + BF tone (unpaired)');
% elseif cond==2 & pair==1
% legend([num2str(s2{2}) num2str(s3{2})],[num2str(s2{2}) num2str(s4{2})]);
% title('VNS + nonBF tone (paired)');
% end
% ylim([ymin ymax+10])
% line([0 0],[ymin ymax+10],'Color','black','LineStyle','--')
% line([1 1],[ymin ymax+10],'Color','black','LineStyle','--')
% 
% % plotting
% timepoints=(1:size(psth6,1))./options.rasterfs - prestimsilence;
% 
% subplot(2,3,6)
% plot(timepoints,psth6)
% xlim([-1 5]);
% ylabel('spike/sec');
% xlabel('time (s)');
% s1=strsep(tags{1} ,',');
% s2=strsep(tags{2} ,',');
% legend(num2str(s1{2}),num2str(s2{2}));
% title('after VNS');
% ylim([ymin ymax+10])
% line([0 0],[ymin ymax+10],'Color','black','LineStyle','--')
% line([1 1],[ymin ymax+10],'Color','black','LineStyle','--')
% 
% % firing rate analysis
% 
% prestim_aft_2=mean(psth6(prestim_id,1));
% data2_aft(1)=mean(psth6(onset_id,1))-prestim_aft_2;
% data2_aft(2)=mean(psth6(sustain_id,1))-prestim_aft_2;
% data2_aft(3)=mean(psth6(offset_id,1))-prestim_aft_2;
% 
% data2_aft'
% 
% saveas(gcf,filename,'fig');
%saveas(gcf,filename,'epsc');