% CCtraining psth grand average
clear all
close all

load CCtraining_psth_all

psth_pre=psthmean_pre;
psth1=psthmean1;
psth2=psthmean2;
psth_post=psthmean_post;

% PSTH grandave/grandmean
BFreward_index=find(reward==1);
offBFreward_index=find(reward==0);

grandmean_pre_BF=nanmean(psth_pre(:,BFreward_index),2);
grandmean1_BF=nanmean(psth1(:,BFreward_index),2);
grandmean2_BF=nanmean(psth2(:,BFreward_index),2);
grandmean_post_BF=nanmean(psth_post(:,BFreward_index),2);

grandmean_pre_offBF=nanmean(psth_pre(:,offBFreward_index),2);
grandmean1_offBF=nanmean(psth1(:,offBFreward_index),2);
grandmean2_offBF=nanmean(psth2(:,offBFreward_index),2);
grandmean_post_offBF=nanmean(psth_post(:,offBFreward_index),2);

rasterfs=30;
prestimsilence=0.5;
timepoints=(1:size(grandmean_pre_BF,1))./rasterfs - prestimsilence;

figure
clf
% BF reward
subplot(2,1,1)
plot(timepoints',grandmean_pre_BF,'b-'); hold on;
plot(timepoints',grandmean1_BF,'r-'); hold on;
plot(timepoints',grandmean2_BF,'m-'); hold on;
plot(timepoints',grandmean_post_BF,'k-');
legend('pre-train','train1','train2','post-train')
title('BF reward training')
xlabel('time (s)')
ylabel('spike rate')

% offBF reward
subplot(2,1,2)
plot(timepoints',grandmean_pre_offBF,'b-');hold on;
plot(timepoints',grandmean1_offBF,'r-');hold on;
plot(timepoints',grandmean2_offBF,'m-');hold on;
plot(timepoints',grandmean_post_offBF,'k-');hold on;
title('offBF reward training')
xlabel('time (s)')
ylabel('spike rate')
