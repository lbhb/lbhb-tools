% CCtraining TORC psth analysis
clear all
close all

data_to_use = 'database_new'; % more stringent requirement for stable trial_raster
data_to_use2 = 'NMKdatabase_new';
%data_to_use = 'database'  % less stringent requirement for stable trial_raster
%data_to_use2 = 'NMKdatabase';

onset=1;   %0 is sustained (resp), 1 is onset (onset)

if onset==0
   psth_file= ['CCtraining_TORCpsth_resp_',data_to_use];
   psth_file2= ['CCtraining_TORCpsth_resp_',data_to_use2];
elseif onset==1
   psth_file= ['CCtraining_TORCpsth_onset_',data_to_use];
   psth_file2= ['CCtraining_TORCpsth_onset_',data_to_use2];
end
load(psth_file);

valid_cell=(find(psthmean_pre(1,:)~=0));

cell_BFreward1=zeros(1,length(valid_cell));
cell_BFreward2=zeros(1,length(valid_cell));

cell_offBFreward1=zeros(1,length(valid_cell));
cell_offBFreward2=zeros(1,length(valid_cell));

mean_pre=mean(psthmean_pre(:,valid_cell)-mean(psthsp_pre(:,valid_cell),1),1);
se_pre=(std(psthmean_pre(:,valid_cell)-mean(psthsp_pre(:,valid_cell),1),0,1))/sqrt(size(psthmean_pre(:,valid_cell),1));
mean1=mean(psthmean1(:,valid_cell)-mean(psthsp1(:,valid_cell),1),1);
se_mean1=(std(psthmean1(:,valid_cell)-mean(psthsp1(:,valid_cell),1),0,1))/sqrt(size(psthmean1(:,valid_cell),1));
mean2=mean(psthmean2(:,valid_cell)-mean(psthsp2(:,valid_cell),1),1);
mean_post=mean(psthmean_post(:,valid_cell)-mean(psthsp_post(:,valid_cell),1),1);
se_post=(std(psthmean_post(:,valid_cell)-mean(psthsp_post(:,valid_cell),1),0,1))/sqrt(size(psthmean_post(:,valid_cell),1));
reward_all=reward(valid_cell);

load(psth_file2);

reward_NMK=reward;
cell_BFreward1_NMK=zeros(1,length(reward_NMK));
cell_BFreward2_NMK=zeros(1,length(reward_NMK));

cell_offBFreward1_NMK=zeros(1,length(reward_NMK));
cell_offBFreward2_NMK=zeros(1,length(reward_NMK));

mean_pre_NMK=mean(psthmean_pre-mean(psthsp_pre,1),1);
se_pre_NMK=(std(psthmean_pre-mean(psthsp_pre,1),0,1))/sqrt(size(psthmean_pre,1));
mean1_NMK=mean(psthmean1-mean(psthsp1,1),1);
se_mean1_NMK=(std(psthmean1-mean(psthsp1,1),0,1))/sqrt(size(psthmean1,1));
mean2_NMK=mean(psthmean2-mean(psthsp2,1),1);
mean_post_NMK=mean(psthmean_post-mean(psthsp_post,1),1);
se_post_NMK=(std(psthmean_post-mean(psthsp_post,1),0,1))/sqrt(size(psthmean_post,1));

mean_pre(length(mean_pre)+1:length(mean_pre)+length(reward_NMK))=mean_pre_NMK;
se_pre(length(se_pre)+1:length(se_pre)+length(reward_NMK))=se_pre_NMK;
mean1(length(mean1)+1:length(mean1)+length(reward_NMK))=mean1_NMK;
se_mean1(length(se_mean1)+1:length(se_mean1)+length(reward_NMK))=se_mean1_NMK;
mean2(length(mean2)+1:length(mean2)+length(reward_NMK))=mean2_NMK;
%se_mean2(length(se_mean2):length(se_mean2)+length(reward_NMK))=se_mean2_NMK;
mean_post(length(mean_post)+1:length(mean_post)+length(reward_NMK))=mean_post_NMK;
se_post(length(se_post)+1:length(se_post)+length(reward_NMK))=se_post_NMK;

reward_all(length(reward_all)+1:length(reward_all)+length(reward_NMK))=reward_NMK;
cell_BFreward1(length(reward_all)+1:length(reward_all)+length(reward_NMK))=cell_BFreward1_NMK;
cell_BFreward2(length(reward_all)+1:length(reward_all)+length(reward_NMK))=cell_BFreward2_NMK;

cell_offBFreward1(length(reward_all)+1:length(reward_all)+length(reward_NMK))=cell_offBFreward1_NMK;
cell_offBFreward2(length(reward_all)+1:length(reward_all)+length(reward_NMK))=cell_offBFreward2_NMK;

%normalization
pre_norm=(mean_pre-mean_pre)./(abs(mean_pre)+abs(mean_pre));
train1_norm=(mean1-mean_pre)./(abs(mean1)+abs(mean_pre));
train2_norm=(mean2-mean_pre)./(abs(mean2)+abs(mean_pre));
post_norm=(mean_post-mean_pre)./(abs(mean_post)+abs(mean_pre));


for nn=1:length(pre_norm)
    if reward_all(nn)==1 
        if mean_pre(nn)-2*se_pre(nn)>0 || mean_post(nn)-2*se_post(nn)>0 || mean1(nn)-2*se_mean1(nn)>0 
%             mean_pre(nn)>(0+2*se_pre(nn)) || mean1(nn)>(0+2*se_mean1(nn)) || mean_post(nn)>(0+2*se_post(nn))
            figure(1);
            subplot(1,2,1)
            plot(1:3,[pre_norm(nn) train1_norm(nn) post_norm(nn)]);hold on
            cell_BFreward1(nn)=1;
            if isnan(mean2(nn))==0
                subplot(1,2,2)
                plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
                cell_BFreward2(nn)=1;
            end
        end
    elseif reward_all(nn)==0
        if mean_pre(nn)-2*se_pre(nn)>0 || mean_post(nn)-2*se_post(nn)>0 || mean1(nn)-2*se_mean1(nn)>0 
%         if mean_pre(nn)>(0+2*se_pre(nn)) || mean1(nn)>(0+2*se_mean1(nn)) || mean_post(nn)>(0+2*se_post(nn))
            figure(2);
            subplot(1,2,1)
            plot(1:3,[pre_norm(nn) train1_norm(nn) post_norm(nn)]); hold on
            cell_offBFreward1(nn)=1;
            if isnan(mean2(nn))==0
                subplot(1,2,2)
                plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
                cell_offBFreward2(nn)=1;
            end
        end
    end
end
figure(1);

subplot(1,2,1)
valid1_id=find(cell_BFreward1==1);
grandmean_pre=nanmean(pre_norm(valid1_id));
grandmean1=nanmean(train1_norm(valid1_id));
grandmean_post=nanmean(post_norm(valid1_id));

sem_pre=nanstd(pre_norm(valid1_id))./sqrt(length(pre_norm(valid1_id)));
sem1=nanstd(train1_norm(valid1_id))./sqrt(length(train1_norm(valid1_id)));
sem_post=nanstd(post_norm(valid1_id))./sqrt(length(post_norm(valid1_id)));
%plot(1:3,[grandmean_pre grandmean1 grandmean_post],'k-','LineWidth',4); hold on
errorbar(1:3,[grandmean_pre grandmean1 grandmean_post],[sem_pre sem1 sem_post],'k-','LineWidth',4)
% xticks(1:3);
xticklabels({'Pre','Training','Post'});
if onset==0
title('BF reward training (Sustained)')
elseif onset==1
    title('BF reward training (Onset)')
end

subplot(1,2,2)
valid2_id=find(cell_BFreward2==1);
grandmean_pre=nanmean(pre_norm(valid2_id));
grandmean1=nanmean(train1_norm(valid2_id));
grandmean2=nanmean(train2_norm(valid2_id));
grandmean_post=nanmean(post_norm(valid2_id));

sem_pre=nanstd(pre_norm(valid2_id))./sqrt(length(pre_norm(valid2_id)));
sem1=nanstd(train1_norm(valid2_id))./sqrt(length(train1_norm(valid2_id)));
sem2=nanstd(train2_norm(valid2_id))./sqrt(length(train2_norm(valid2_id)));
sem_post=nanstd(post_norm(valid2_id))./sqrt(length(post_norm(valid2_id)));

%plot(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],'k-','LineWidth',4);
errorbar(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],[sem_pre sem1 sem2 sem_post],'k-','LineWidth',4);
% xticks(1:4);
xticklabels({'Pre','Training1','Training2','Post'});
title('BF reward training')

figure(2)

subplot(1,2,1)
valid01_id=find(cell_offBFreward1==1);
grandmean_pre=nanmean(pre_norm(valid01_id));
grandmean1=nanmean(train1_norm(valid01_id));
grandmean_post=nanmean(post_norm(valid01_id));

sem_pre=nanstd(pre_norm(valid01_id))./sqrt(length(pre_norm(valid01_id)));
sem1=nanstd(train1_norm(valid01_id))./sqrt(length(train1_norm(valid01_id)));
sem_post=nanstd(post_norm(valid01_id))./sqrt(length(post_norm(valid01_id)));

%plot(1:3,[grandmean_pre grandmean1 grandmean_post],'k-','LineWidth',4);
errorbar(1:3,[grandmean_pre grandmean1 grandmean_post],[sem_pre sem1 sem_post],'k-','LineWidth',4)
% xticks(1:3);
xticklabels({'Pre','Training','Post'});
if onset==0
title('offBF reward training (Sustained)')
elseif onset==1
    title('offBF reward training (Onset)')
end

subplot(1,2,2)
valid02_id=find(cell_offBFreward2==1);
grandmean_pre=nanmean(pre_norm(valid02_id));
grandmean1=nanmean(train1_norm(valid02_id));
grandmean2=nanmean(train2_norm(valid02_id));
grandmean_post=nanmean(post_norm(valid02_id));

sem_pre=nanstd(pre_norm(valid02_id))./sqrt(length(pre_norm(valid02_id)));
sem1=nanstd(train1_norm(valid02_id))./sqrt(length(train1_norm(valid02_id)));
sem2=nanstd(train2_norm(valid02_id))./sqrt(length(train2_norm(valid02_id)));
sem_post=nanstd(post_norm(valid02_id))./sqrt(length(post_norm(valid02_id)));

% plot(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],'k-','LineWidth',4);
errorbar(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],[sem_pre sem1 sem2 sem_post],'k-','LineWidth',4);
% xticks(1:4);
xticklabels({'Pre','Training1','Training2','Post'});
title('offBF reward training')

