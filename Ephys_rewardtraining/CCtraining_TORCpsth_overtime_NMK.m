%% Classical conditioning training TORC psth overtime
clear all
close all
baphy_set_path

CCtraining_TORC_NMKdatabase
load CCtraining_strf_snr_NMK

fs=30;
onset=0;

%n=1;
for nn=1:n
    nn
    if snr_pre_torc(nn)>0.12 | snr1(nn)>0.12 | snr2(nn)>0.12 | snr3(nn)>0.12 | snr_post_torc(nn)>0.12
        
        resppath=valid_data(nn).resppath;
        mfilepath=valid_data(nn).mfilepath;
        
        cellid=valid_data(nn).cellid;
        
        mfile_pre=valid_data(nn).mfile_pre_torc;
        resp_pre=valid_data(nn).resp_pre_torc;
        mfile_active=valid_data(nn).mfile_active;
        resp_active=valid_data(nn).resp_active;
        mfile_post=valid_data(nn).mfile_post_torc;
        resp_post=valid_data(nn).resp_post_torc;
        BF=valid_data(nn).BF;
        offBF=valid_data(nn).offBF;
        tar_reward=valid_data(nn).tar_reward;
        
        cellfiledata = dbgetscellfile('cellid', cellid);
        
        mfilename_pre=[mfilepath mfile_pre];
        spkfile_pre=[resppath resp_pre];
        
        mfilename=[mfilepath mfile_active];
        spkfile=[resppath resp_active];
        
        mfilename_post=[mfilepath mfile_post];
        spkfile_post=[resppath resp_post];
        
        %options
        channel=valid_data(nn).channel;
        unit=valid_data(nn).unit;
        trials=valid_data(nn).trials;
        if length(trials)<=66
            options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}});
            options_post=options_pre;
            options1=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}},'includeincorrect',1,'trialrange',trials);
        elseif length(trials)>66
            trials1=1:floor(length(trials)/2);
            trials2=floor(length(trials)/2)+1:length(trials);
            options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}});
            options_post=options_pre;
            options1=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}},'includeincorrect',1,'trialrange',trials1);
            options2=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}},'includeincorrect',1,'trialrange',trials2);
        end
        
        prestimsilence1=0.4; % 0.4 sec prestimulus silence
        prestimsilence2=0.5;
        prestimsilence3=0.4;
        
        figure(nn);
        clf
        
        [r_pre, tags_pre]=loadspikeraster(spkfile_pre,options_pre); %r is time X repetition X stimulus id
        
        %%% Do mean(nanmean(r,2),3)
        % average across repetitions to get the PSTH
        r_pre=mean(nanmean(r_pre,2),3);
        
        psth_pre = r_pre* options_pre.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
        
        %% Pre-training
        subplot(3,2,1)
        timepoints=(1:size(psth_pre,1))./options_pre.rasterfs- prestimsilence1;
        if onset==0
            time_index=find(timepoints>0.07 & timepoints<=3); % sustained resp
        elseif onset==1
            time_index=find(timepoints>0 & timepoints<=0.07); % onset
        end
        prestim= find(timepoints <= 0 & timepoints >= -0.5); % sp rate
        
        % onset_id= find(timepoints <= 0.07 & timepoints > 0);
        % sustain_id= find(timepoints <= 1 & timepoints > 0.07);
        % offset_id= find(timepoints <= 1.1 & timepoints > 1);
        % stim_id=find(timepoints <= 1 & timepoints > 0); % during tone presentation
        
        
        %s1=strsep(tags_pre{1} ,',');
        %s2=strsep(tags_pre{2} ,',');
        %if s1{2}==BF
        plot(timepoints,psth_pre,'b-');hold on
        %plot(timepoints,psth_pre(:,2),'r-');
        
        %psthmean_pre(:,nn)=psth_pre(time_index,1); % sustained resp
        %psthsp_pre(:,nn)=psth_pre(prestim,1); % spont rate
        %legend({'BF','nonBF'},'FontSize',7);
        %elseif s2{2}==BF
        %plot(timepoints,psth_pre(:,1),'r-');hold on
        %plot(timepoints,psth_pre(:,2),'b-');
        psthmean_pre(:,nn)=psth_pre(time_index,1); % sustained resp
        psthsp_pre(:,nn)=psth_pre(prestim,1); % spont rate
        %legend({'nonBF','BF'},'FontSize',7);
        %end
        %xlim([-0.5 3]);
        %legend('boxoff');
        ylabel('spike/sec');
        xlabel('time (s)');
        ymax=max(max(psth_pre));
        %ylim([0 ymax+10])
        line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
        line([3 3],[0 ymax+10],'Color','black','LineStyle','--')
        title(sprintf('%s trials ALL',basename(mfilename_pre)),'fontsize',8,'fontweight','normal');
        
        % LoadMFile(mfilename);
        % TargetFrequencies = exptparams.TrialObject.TargetHandle.Frequencies;
        % tarfreqasoctaves = log2(TargetFrequencies)-log2(StimParam.lfreq);
        
        %% During training
        % First half of training
        if length(trials)<=66
            %len1= floor(length(trials)/2);
            %tstart=ceil(trials(1)/2);
            [r, tags]=loadspikeraster(spkfile,options1);
            %len=size(r,2);
            r1=mean(nanmean(r(:,:,:),2),3);
            psth1= r1* options1.rasterfs;
        elseif length(trials)>66
            %len1= floor(length(trials)/4);
            %len2= len1*2;
            %tstart=ceil(trials(1)/4);
            [r, tags]=loadspikeraster(spkfile,options1);
            %len=size(r,2);
            r1=mean(nanmean(r(:,:,:),2),3);
            psth1= r1* options1.rasterfs;
            clear r
            [r, tags]=loadspikeraster(spkfile,options2);
            r2=mean(nanmean(r(:,:,:),2),3);
            psth2= r2* options2.rasterfs;
        end
        
        subplot(3,2,3)
        timepoints=(1:size(psth1,1))./options1.rasterfs - prestimsilence2;
        if onset==0
            time_index=find(timepoints>0.07 & timepoints<=1); % sustained resp
        elseif onset==1
            time_index=find(timepoints>0 & timepoints<=0.07); % onset
        end
        %s1=strsep(tags{1} ,',');
        %s2=strsep(tags{2} ,',');
        % if s1{2}==BF
        plot(timepoints,psth1(:,1),'b-');hold on
        %plot(timepoints,psth1(:,2),'r-');
        psthmean1(:,nn)=psth1(time_index,1); % sustained resp
        psthsp1(:,nn)=psth1(prestim,1); % spont rate
        if tar_reward==BF
            %legend({'BF reward','nonBF'},'FontSize',7);
            reward(nn)=1; %1 is BF reward, 0 is nonBF reward
        else
            %legend({'BF','nonBF reward'},'FontSize',7);
            reward(nn)=0; %1 is BF reward, 0 is nonBF reward
        end
        % elseif s2{2}==BF
        %     plot(timepoints,psth1(:,1),'r-');hold on
        %     plot(timepoints,psth1(:,2),'b-');
        %     psthmean1(:,nn)=psth1(time_index,2); % sustained resp
        %     psthsp1(:,nn)=psth1(prestim,2); % spont rate
        %      if tar_reward==BF
        %         legend({'nonBF','BF reward'},'FontSize',7);
        %         reward(nn)=1; %1 is BF reward, 0 is nonBF reward
        %     else
        %         legend({'nonBF reward','BF'},'FontSize',7);
        %         reward(nn)=0; %1 is BF reward, 0 is nonBF reward
        %      end
        % end
        % if s1{2}==BF
        % plot(timepoints,psth1(:,1),'b-');hold on
        % plot(timepoints,psth1(:,2),'r-');
        % psthmean1(:,nn)=psth1(time_index,1);
        % %legend('BF','nonBF');
        % elseif s2{2}==BF
        % plot(timepoints,psth1(:,1),'r-');hold on
        % plot(timepoints,psth1(:,2),'b-');
        % psthmean1(:,nn)=psth1(time_index,2);
        % %legend('nonBF','BF');
        % end
        %xlim([-0.5 3]);
        %legend('boxoff');
        ylabel('spike/sec');
        xlabel('time (s)');
        ymax=max(max(psth1));
        %ylim([0 ymax+10])
        line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
        line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
        title(sprintf('%s first half of training session',basename(mfilename)),'fontsize',8,'fontweight','normal');
        %title(sprintf('%s trials %d-%d',basename(mfilename),trials(1),length(trials)),'fontsize',8,'fontweight','normal');
        
        % Second half of training
        if length(trials)>66
            subplot(3,2,4)
            %timepoints=(1:size(psth_pre,1))./options.rasterfs - prestimsilence;
            %s1=strsep(tags{1} ,',');
            %s2=strsep(tags{2} ,',');
            %if s1{2}==BF
            plot(timepoints,psth2(:,1),'b-');hold on
            %plot(timepoints,psth2(:,2),'r-');
            psthmean2(:,nn)=psth2(time_index,1);% sustained resp
            psthsp2(:,nn)=psth2(prestim,1); % spont rate
            %legend('BF','nonBF');
            %elseif s2{2}==BF
            %         plot(timepoints,psth2(:,1),'r-');hold on
            %         plot(timepoints,psth2(:,2),'b-');
            %         psthmean2(:,nn)=psth2(time_index,2);% sustained resp
            %         psthsp2(:,nn)=psth2(prestim,2); % spont rate
            %         %legend('nonBF','BF');
            %     end
            %xlim([-0.5 3]);
            ylabel('spike/sec');
            xlabel('time (s)');
            ymax=max(max(psth2));
            %ylim([0 ymax+10])
            line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
            line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
            title(sprintf('%s second half of training session',basename(mfilename)),'fontsize',8,'fontweight','normal');
            %title(sprintf('%s trials %d-%d',basename(mfilename),trials(1)+len1*2+1,trials(end)),'fontsize',8,'fontweight','normal');
        else
            psthmean2(:,nn)=NaN(length(time_index),1);
            psthsp2(:,nn)=NaN(length(prestim),1); % spont rate
        end
        
 %% Post-training
        if prod(size(mfile_post))~=0
            [r_post, tags_post]=loadspikeraster(spkfile_post,options_post); %r is time X repetition X stimulus id
            
            % average across repetitions to get the PSTH
            r_post=mean(nanmean(r_post,2),3);
            psth_post = r_post* options_post.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
            
            timepoints=(1:size(psth_post,1))./options_post.rasterfs - prestimsilence3;
            if onset==0
                time_index=find(timepoints>0.07 & timepoints<=3); % sustained resp
            elseif onset==1
                time_index=find(timepoints>0 & timepoints<=0.07); % onset
            end
            
         
            subplot(3,2,5)
            %s1=strsep(tags_post{1} ,',');
            %s2=strsep(tags_post{2} ,',');
            %if s1{2}==BF
            plot(timepoints,psth_post(:,1),'b-');hold on
            %plot(timepoints,psth_post(:,2),'r-');
            psthmean_post(:,nn)=psth_post(time_index,1);% sustained resp
            psthsp_post(:,nn)=psth_post(prestim,1); % spont rate
            %legend({'BF','nonBF'},'FontSize',7);
            %elseif s2{2}==BF
            %plot(timepoints,psth_post(:,1),'r-');hold on
            %plot(timepoints,psth_post(:,2),'b-');
            %psthmean_post(:,nn)=psth_post(time_index,2);% sustained resp
            %psthsp_post(:,nn)=psth_post(prestim,2); % spont rate
            %legend({'nonBF','BF'},'FontSize',7);
            %end
            %xlim([-0.5 3]);
            %legend('boxoff');
            ylabel('spike/sec');
            xlabel('time (s)');
            ymax=max(max(psth_post));
            %ylim([0 ymax+10])
            line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
            line([3 3],[0 ymax+10],'Color','black','LineStyle','--')
            title(sprintf('%s trials ALL',basename(mfilename_post)),'fontsize',8,'fontweight','normal');
        end
        
        if reward(nn)==1 %onBF reward
            saveas(gcf,[cellid '_TORCpsth'],'epsc')
            saveas(gcf,[cellid '_TORCpsth'],'png')
            savefig([cellid '_TORCpsth.fig'])
        else %offBF reward
            saveas(gcf,[cellid '_rev_TORCpsth'],'epsc')
            saveas(gcf,[cellid '_rev_TORCpsth'],'png')
            savefig([cellid '_rev_TORCpsth.fig'])
        end
    end
end

if onset==1
save('CCtraining_TORCpsth_onset_NMK','psthmean_pre','psthmean1','psthmean2','psthmean_post','reward',...
    'psthsp_pre','psthsp1','psthsp2','psthsp_post')
elseif onset==0
save('CCtraining_TORCpsth_resp_NMK','psthmean_pre','psthmean1','psthmean2','psthmean_post','reward',...
    'psthsp_pre','psthsp1','psthsp2','psthsp_post')
end
