% CCtraining psth overtime analysis
clear all
close all

onset=0;   %0 is sustained (resp), 1 is onset (onset)

if onset==0
load CCtraining_psth_resp_new   
elseif onset==1
load CCtraining_psth_onset_new
end

cell_BFreward1=zeros(1,length(reward));
cell_BFreward2=zeros(1,length(reward));

cell_offBFreward1=zeros(1,length(reward));
cell_offBFreward2=zeros(1,length(reward));

% mean_pre=mean(psthmean_pre-mean(psthsp_pre,1),1);
% se_pre=(std(psthmean_pre-mean(psthsp_pre,1),0,1))/sqrt(size(psthmean_pre,1));
% mean1=mean(psthmean1-mean(psthsp1,1),1);
% se_mean1=(std(psthmean1-mean(psthsp1,1),0,1))/sqrt(size(psthmean1,1));
% mean2=mean(psthmean2-mean(psthsp2,1),1);
% mean_post=mean(psthmean_post-mean(psthsp_post,1),1);
% se_post=(std(psthmean_post-mean(psthsp_post,1),0,1))/sqrt(size(psthmean_post,1));

mean_pre=mean(psthmean_pre-mean(psthsp_pre,1),1);
se_pre=(std(psthmean_pre-mean(psthsp_pre,1),0,1))/sqrt(size(psthmean_pre,1));
mean1=mean(psthmean1-mean(psthsp1,1),1);
se_mean1=(std(psthmean1-mean(psthsp1,1),0,1))/sqrt(size(psthmean1,1));
mean2=mean(psthmean2-mean(psthsp2,1),1);
mean_post=mean(psthmean_post-mean(psthsp_post,1),1);
se_post=(std(psthmean_post-mean(psthsp_post,1),0,1))/sqrt(size(psthmean_post,1));

%normalization
pre_norm=(mean_pre-mean_pre)./(abs(mean_pre)+abs(mean_pre));
train1_norm=(mean1-mean_pre)./(abs(mean1)+abs(mean_pre));
train2_norm=(mean2-mean_pre)./(abs(mean2)+abs(mean_pre));
post_norm=(mean_post-mean_pre)./(abs(mean_post)+abs(mean_pre));

% sig_unit1=find(mean_pre-2*se_pre>0 & mean1-2*se_mean1>0 & mean_post-2*se_post>0 & reward==1);
% sig_unit0=find(mean_pre-2*se_pre>0 & mean1-2*se_mean1>0 & mean_post-2*se_post>0 & reward==0);
% figure(1);
% subplot(1,2,1)
% unit_matrix1=[pre_norm(sig_unit1)' train1_norm(sig_unit1)' post_norm(sig_unit1)'];
% plot(1:3,unit_matrix1);hold on;
% plot(1:3,mean(unit_matrix1,1),'k-','LineWidth',4)
% xticks(1:3)
% xticklabels({'Pre','Training','Post'})
% title('BF reward training (sustained resp)')


for nn=1:length(reward)
    if reward(nn)==1 
        if mean_pre(nn)-2*se_pre(nn)>0 && mean_post(nn)-2*se_post(nn)>0 && mean1(nn)-2*se_mean1(nn)>0 
            figure(1);
            subplot(1,2,1)
            plot(1:3,[pre_norm(nn) train1_norm(nn) post_norm(nn)]);hold on
            cell_BFreward1(nn)=1;
            if isnan(mean2(nn))==0
                subplot(1,2,2)
                plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
                cell_BFreward2(nn)=1;
            end
        end
    elseif reward(nn)==0 
        if mean_pre(nn)-2*se_pre(nn)>0 && mean_post(nn)-2*se_post(nn)>0 && mean1(nn)-2*se_mean1(nn)>0
            %mean_pre(nn)>(0+2*se_pre(nn)) || mean1(nn)>(0+2*se_mean1(nn)) || mean_post(nn)>(0+2*se_post(nn))
            figure(2);
            subplot(1,2,1)
            plot(1:3,[pre_norm(nn) train1_norm(nn) post_norm(nn)]); hold on
            cell_offBFreward1(nn)=1;
            if isnan(mean2(nn))==0
                subplot(1,2,2)
                plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
                cell_offBFreward2(nn)=1;
            end
        end
    end
end
figure(1);
subplot(1,2,1)
valid1_id=find(cell_BFreward1==1);
grandmean_pre=mean(pre_norm(valid1_id));
grandmean1=mean(train1_norm(valid1_id));
grandmean_post=mean(post_norm(valid1_id));

sem_pre=std(pre_norm(valid1_id))./sqrt(length(pre_norm(valid1_id)));
sem1=std(train1_norm(valid1_id))./sqrt(length(train1_norm(valid1_id)));
sem_post=std(post_norm(valid1_id))./sqrt(length(post_norm(valid1_id)));

% plot(1:3,[grandmean_pre grandmean1 grandmean_post],'k-','LineWidth',4);
errorbar(1:3,[grandmean_pre grandmean1 grandmean_post],[sem_pre sem1 sem_post],'k-','LineWidth',4)
%xticks(1:3)
xticklabels({'Pre','Training','Post'})
if onset==0
title('BF reward training (Sustained)')
elseif onset==1
    title('BF reward training (Onset)')
end

subplot(1,2,2)
valid2_id=find(cell_BFreward2==1);
grandmean_pre=mean(pre_norm(valid2_id));
grandmean1=mean(train1_norm(valid2_id));
grandmean2=mean(train2_norm(valid2_id));
grandmean_post=mean(post_norm(valid2_id));

sem_pre=std(pre_norm(valid2_id))./sqrt(length(pre_norm(valid2_id)));
sem1=std(train1_norm(valid2_id))./sqrt(length(train1_norm(valid2_id)));
sem2=std(train2_norm(valid2_id))./sqrt(length(train2_norm(valid2_id)));
sem_post=std(post_norm(valid2_id))./sqrt(length(post_norm(valid2_id)));

% plot(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],'k-','LineWidth',4);
errorbar(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],[sem_pre sem1 sem2 sem_post],'k-','LineWidth',4);
%xticks(1:4)
xticklabels({'Pre','Training1','Training2','Post'})
title('BF reward training')

figure(2)
subplot(1,2,1)
valid01_id=find(cell_offBFreward1==1);
grandmean_pre=mean(pre_norm(valid01_id));
grandmean1=mean(train1_norm(valid01_id));
grandmean_post=mean(post_norm(valid01_id));
sem_pre=std(pre_norm(valid01_id))./sqrt(length(pre_norm(valid01_id)));
sem1=std(train1_norm(valid01_id))./sqrt(length(train1_norm(valid01_id)));
sem_post=std(post_norm(valid01_id))./sqrt(length(post_norm(valid01_id)));

% plot(1:3,[grandmean_pre grandmean1 grandmean_post],'k-','LineWidth',4);
errorbar(1:3,[grandmean_pre grandmean1 grandmean_post],[sem_pre sem1 sem_post],'k-','LineWidth',4)
%xticks(1:3)
xticklabels({'Pre','Training','Post'})
if onset==0
title('offBF reward training (Sustained)')
elseif onset==1
    title('offBF reward training (Onset)')
end

subplot(1,2,2)
valid02_id=find(cell_offBFreward2==1);
grandmean_pre=mean(pre_norm(valid02_id));
grandmean1=mean(train1_norm(valid02_id));
grandmean2=mean(train2_norm(valid02_id));
grandmean_post=mean(post_norm(valid02_id));

sem_pre=std(pre_norm(valid02_id))./sqrt(length(pre_norm(valid02_id)));
sem1=std(train1_norm(valid02_id))./sqrt(length(train1_norm(valid02_id)));
sem2=std(train2_norm(valid02_id))./sqrt(length(train2_norm(valid02_id)));
sem_post=std(post_norm(valid02_id))./sqrt(length(post_norm(valid02_id)));

% plot(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],'k-','LineWidth',4);
errorbar(1:4,[grandmean_pre grandmean1 grandmean2 grandmean_post],[sem_pre sem1 sem2 sem_post],'k-','LineWidth',4);
%xticks(1:4)
xticklabels({'Pre','Training1','Training2','Post'})
title('offBF reward training')

