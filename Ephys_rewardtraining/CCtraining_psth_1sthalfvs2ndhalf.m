% CCtraining psth grand average
clear all
close all

load CCtraining_psth_all

psth_pre=psthmean_pre;
psth1=psthmean1;
psth2=psthmean2;
psth_post=psthmean_post;

cell_BFreward=zeros(1,length(reward));
cell_BFreward2=zeros(1,length(reward));

cell_offBFreward=zeros(1,length(reward));
cell_offBFreward2=zeros(1,length(reward));

%% Separate psth into 1st and 2nd halves
rasterfs=30;
prestimsilence=0.5; % 0.5 sec prestimulus silence
timepoints=(1:size(psth_pre,1))./rasterfs - prestimsilence;
time_index1=find(timepoints>0 & timepoints<=0.5); % PSTH 1st half
time_index2=find(timepoints>0.5 & timepoints<=1.0); % PSTH 2nd half
prestim= find(timepoints <= 0 & timepoints >= -0.5); % spont rate

psth_pre_1=psth_pre(time_index1,:);
psth_pre_2=psth_pre(time_index2,:);
psthsp_pre=psth_pre(prestim,:);

psth1_1=psth1(time_index1,:);
psth1_2=psth1(time_index2,:);
psthsp1=psth1(prestim,:);

psth_post_1=psth_post(time_index1,:);
psth_post_2=psth_post(time_index2,:);
psthsp_post=psth_post(prestim,:);

%% mean for each halve
mean_pre_1=mean(psth_pre_1-mean(psthsp_pre,1),1);
se_pre_1=(std(psth_pre_1-mean(psthsp_pre,1),0,1))/sqrt(size(psth_pre_1,1));
mean_pre_2=mean(psth_pre_2-mean(psthsp_pre,1),1);
se_pre_2=(std(psth_pre_2-mean(psthsp_pre,1),0,1))/sqrt(size(psth_pre_2,1));

mean1_1=mean(psth1_1-mean(psthsp1,1),1);
se_mean1_1=(std(psth1_1-mean(psthsp1,1),0,1))/sqrt(size(psth1_1,1));
mean1_2=mean(psth1_2-mean(psthsp1,1),1);
se_mean1_2=(std(psth1_2-mean(psthsp1,1),0,1))/sqrt(size(psth1_2,1));

mean_post_1=mean(psth_post_1-mean(psthsp_post,1),1);
se_post_1=(std(psth_post_1-mean(psthsp_post,1),0,1))/sqrt(size(psth_post_1,1));
mean_post_2=mean(psth_post_2-mean(psthsp_post,1),1);
se_post_2=(std(psth_post_2-mean(psthsp_post,1),0,1))/sqrt(size(psth_post_2,1));

%% normalization
pre_norm_1=(mean_pre_1-mean_pre_1)./(abs(mean_pre_1)+abs(mean_pre_1));
pre_norm_2=(mean_pre_2-mean_pre_2)./(abs(mean_pre_2)+abs(mean_pre_2));

train1_norm_1=(mean1_1-mean_pre_1)./(abs(mean1_1)+abs(mean_pre_1));
train1_norm_2=(mean1_2-mean_pre_2)./(abs(mean1_2)+abs(mean_pre_2));

post_norm_1=(mean_post_1-mean_pre_1)./(abs(mean_post_1)+abs(mean_pre_1));
post_norm_2=(mean_post_2-mean_pre_2)./(abs(mean_post_2)+abs(mean_pre_2));


%% first halve
for nn=1:length(reward)
    if reward(nn)==1 
        if mean_pre_1(nn)-2*se_pre_1(nn)>0 && mean_post_1(nn)-2*se_post_1(nn)>0 && mean1_1(nn)-2*se_mean1_1(nn)>0 
            figure(1);
            subplot(1,2,1)
            plot(1:3,[pre_norm_1(nn) train1_norm_1(nn) post_norm_1(nn)]);hold on
            cell_BFreward(nn)=1;
%             if isnan(mean2(nn))==0
%                 subplot(1,2,2)
%                 plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
%                 cell_BFreward2(nn)=1;
%             end
        end
    elseif reward(nn)==0 
        if mean_pre_1(nn)-2*se_pre_1(nn)>0 && mean_post_1(nn)-2*se_post_1(nn)>0 && mean1_1(nn)-2*se_mean1_1(nn)>0
            %mean_pre(nn)>(0+2*se_pre(nn)) || mean1(nn)>(0+2*se_mean1(nn)) || mean_post(nn)>(0+2*se_post(nn))
            %figure(1);
            subplot(1,2,2)
            plot(1:3,[pre_norm_1(nn) train1_norm_1(nn) post_norm_1(nn)]); hold on
            cell_offBFreward(nn)=1;
%             if isnan(mean2(nn))==0
%                 subplot(1,2,2)
%                 plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
%                 cell_offBFreward2(nn)=1;
%             end
        end
    end
end

%% second halve
for nn=1:length(reward)
    if reward(nn)==1 
        if mean_pre_2(nn)-2*se_pre_2(nn)>0 && mean_post_2(nn)-2*se_post_2(nn)>0 && mean1_2(nn)-2*se_mean1_2(nn)>0 
            figure(2);
            subplot(1,2,1)
            plot(1:3,[pre_norm_2(nn) train1_norm_2(nn) post_norm_2(nn)]);hold on
            cell_BFreward(nn)=1;
%             if isnan(mean2(nn))==0
%                 subplot(1,2,2)
%                 plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
%                 cell_BFreward2(nn)=1;
%             end
        end
    elseif reward(nn)==0 
        if mean_pre_2(nn)-2*se_pre_2(nn)>0 && mean_post_2(nn)-2*se_post_2(nn)>0 && mean1_2(nn)-2*se_mean1_2(nn)>0
            %mean_pre(nn)>(0+2*se_pre(nn)) || mean1(nn)>(0+2*se_mean1(nn)) || mean_post(nn)>(0+2*se_post(nn))
            %figure(2);
            subplot(1,2,2)
            plot(1:3,[pre_norm_2(nn) train1_norm_2(nn) post_norm_2(nn)]); hold on
            cell_offBFreward(nn)=1;
%             if isnan(mean2(nn))==0
%                 subplot(1,2,2)
%                 plot(1:4,[pre_norm(nn) train1_norm(nn) train2_norm(nn) post_norm(nn)]);hold on
%                 cell_offBFreward2(nn)=1;
%             end
        end
    end
end

figure(1);
subplot(1,2,1)
valid1_id=find(cell_BFreward==1);
grandmean_pre_1=mean(pre_norm_1(valid1_id));
grandmean1_1=mean(train1_norm_1(valid1_id));
grandmean_post_1=mean(post_norm_1(valid1_id));
plot(1:3,[grandmean_pre_1 grandmean1_1 grandmean_post_1],'k-','LineWidth',4);
xticks(1:3)
xticklabels({'Pre','Training','Post'})
title('BF reward training (0-0.5s)')

subplot(1,2,2)
valid0_id=find(cell_offBFreward==1);
grandmean_pre_1=mean(pre_norm_1(valid0_id));
grandmean1_1=mean(train1_norm_1(valid0_id));
grandmean_post_1=mean(post_norm_1(valid0_id));
plot(1:3,[grandmean_pre_1 grandmean1_1 grandmean_post_1],'k-','LineWidth',4);
xticks(1:3)
xticklabels({'Pre','Training','Post'})
title('offBF reward training (0-0.5s)')

figure(2);
subplot(1,2,1)
valid1_id=find(cell_BFreward==1);
grandmean_pre_2=mean(pre_norm_2(valid1_id));
grandmean1_2=mean(train1_norm_2(valid1_id));
grandmean_post_2=mean(post_norm_2(valid1_id));
plot(1:3,[grandmean_pre_2 grandmean1_2 grandmean_post_2],'k-','LineWidth',4);
xticks(1:3)
xticklabels({'Pre','Training','Post'})
title('BF reward training (0.5-1.0s)')

subplot(1,2,2)
valid0_id=find(cell_offBFreward==1);
grandmean_pre_2=mean(pre_norm_2(valid0_id));
grandmean1_2=mean(train1_norm_2(valid0_id));
grandmean_post_2=mean(post_norm_2(valid0_id));
plot(1:3,[grandmean_pre_2 grandmean1_2 grandmean_post_2],'k-','LineWidth',4);
xticks(1:3)
xticklabels({'Pre','Training','Post'})
title('offBF reward training (0.5-1.0s)')

