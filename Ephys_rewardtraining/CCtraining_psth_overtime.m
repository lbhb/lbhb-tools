%% Classical conditioning training target/tone PSTH analysis
clear all
close all
baphy_set_path

CCtraining_database_new
fs=30;
onset=0; %0 is sustained (resp), 1 is onset (onset), 2 is -0.5 to 1.5s (all)

%n=2;
for nn=1:n
    nn
resppath=valid_data(nn).resppath;
mfilepath=valid_data(nn).mfilepath;

cellid=valid_data(nn).cellid;

mfile_pre=valid_data(nn).mfile_pre;
resp_pre=valid_data(nn).resp_pre;
mfile_active=valid_data(nn).mfile_active;
resp_active=valid_data(nn).resp_active;
mfile_post=valid_data(nn).mfile_post;
resp_post=valid_data(nn).resp_post;
BF=valid_data(nn).BF;
offBF=valid_data(nn).offBF;
tar_reward=valid_data(nn).tar_reward;

cellfiledata = dbgetscellfile('cellid', cellid);

mfilename_pre=[mfilepath mfile_pre];
spkfile_pre=[resppath resp_pre];

mfilename=[mfilepath mfile_active];
spkfile=[resppath resp_active];

mfilename_post=[mfilepath mfile_post];
spkfile_post=[resppath resp_post];

%options
channel=valid_data(nn).channel;
unit=valid_data(nn).unit;
trials=valid_data(nn).trials;
if nn<3
    options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'target'}});
    options_post=options_pre;
else
    options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}});
    options_post=options_pre;
end
options=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'target'}},'includeincorrect',1); 

prestimsilence=0.5; % 0.5 sec prestimulus silence

figure(nn);
clf

%% Pre-training
[r_pre, tags_pre]=loadspikeraster(spkfile_pre,options_pre); %r is time X repetition X stimulus id
% average across repetitions to get the PSTH
r_pre=nanmean(r_pre,2);
psth_pre = squeeze(r_pre)* options_pre.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH


subplot(3,2,1)
timepoints=(1:size(psth_pre,1))./options_pre.rasterfs - prestimsilence;
if onset==0
    time_index=find(timepoints>0.07 & timepoints<=1); % sustained resp
elseif onset==1
    time_index=find(timepoints>0 & timepoints<=0.07); % onset
elseif onset==2
    time_index=find(timepoints>=-0.5 & timepoints<=1.5); % -0.5 to 1.5s PSTH
end
prestim= find(timepoints <= 0 & timepoints >= -0.5);

% onset_id= find(timepoints <= 0.07 & timepoints > 0);
% sustain_id= find(timepoints <= 1 & timepoints > 0.07);
% offset_id= find(timepoints <= 1.1 & timepoints > 1);
% stim_id=find(timepoints <= 1 & timepoints > 0); % during tone presentation

s1=strsep(tags_pre{1} ,',');
s2=strsep(tags_pre{2} ,',');

if onset==2
    if s1{2}==BF
        psthmean_pre(:,nn)=psth_pre(time_index,1);
    elseif s2{2}==BF
        psthmean_pre(:,nn)=psth_pre(time_index,2);
    end
    
else
    if s1{2}==BF
        plot(timepoints,psth_pre(:,1),'b-');hold on
        plot(timepoints,psth_pre(:,2),'r-');
        psthmean_pre(:,nn)=psth_pre(time_index,1); % sustained resp
        psthsp_pre(:,nn)=psth_pre(prestim,1); % spont rate
        legend({'BF','nonBF'},'FontSize',7);
    elseif s2{2}==BF
        plot(timepoints,psth_pre(:,1),'r-');hold on
        plot(timepoints,psth_pre(:,2),'b-');
        psthmean_pre(:,nn)=psth_pre(time_index,2); % sustained resp
        psthsp_pre(:,nn)=psth_pre(prestim,2); % spont rate
        legend({'nonBF','BF'},'FontSize',7);
    end
    xlim([-0.5 3]);
    legend('boxoff');
    ylabel('spike/sec');
    xlabel('time (s)');
    ymax=max(max(psth_pre));
    %ylim([0 ymax+10])
    line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
    line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
    title(sprintf('%s trials ALL',basename(mfilename_pre)),'fontsize',8,'fontweight','normal');
end
% LoadMFile(mfilename);
% TargetFrequencies = exptparams.TrialObject.TargetHandle.Frequencies;
% tarfreqasoctaves = log2(TargetFrequencies)-log2(StimParam.lfreq);
%% During training
% First half of training session
if length(trials)<=66
    len1= floor(length(trials)/2);
    tstart=ceil(trials(1)/2);
    [r, tags]=loadspikeraster(spkfile,options);
    r1=nanmean(r(:,tstart:tstart+len1-1,:),2);
    psth1= squeeze(r1)* options.rasterfs;
else
    len1= floor(length(trials)/4);
    len2= len1*2;
    tstart=ceil(trials(1)/4);
    [r, tags]=loadspikeraster(spkfile,options);
    r1=nanmean(r(:,tstart:tstart+len1-1,:),2);
    psth1= squeeze(r1)* options.rasterfs;
    
    r2=nanmean(r(:,tstart+len1:tstart+len2-1,:),2);
    psth2= squeeze(r2)* options.rasterfs;
end

subplot(3,2,3)
timepoints=(1:size(psth1,1))./options.rasterfs - prestimsilence;
%time_index=find(timepoints>0.07 & timepoints<=1);
s1=strsep(tags{1} ,',');
s2=strsep(tags{2} ,',');
if onset==2
    if s1{2}==BF
        psthmean1(:,nn)=psth1(time_index,1);
    elseif s2{2}==BF
        psthmean1(:,nn)=psth1(time_index,2);
    end
    if tar_reward==BF
        reward(nn)=1; %1 is BF reward, 0 is nonBF reward
    else
        
        reward(nn)=0; %1 is BF reward, 0 is nonBF reward
    end
    
else
    if s1{2}==BF
        plot(timepoints,psth1(:,1),'b-');hold on
        plot(timepoints,psth1(:,2),'r-');
        psthmean1(:,nn)=psth1(time_index,1); % sustained resp
        psthsp1(:,nn)=psth1(prestim,1); % spont rate
        if tar_reward==BF
            legend({'BF reward','nonBF'},'FontSize',7);
            reward(nn)=1; %1 is BF reward, 0 is nonBF reward
        else
            legend({'BF','nonBF reward'},'FontSize',7);
            reward(nn)=0; %1 is BF reward, 0 is nonBF reward
        end
    elseif s2{2}==BF
        plot(timepoints,psth1(:,1),'r-');hold on
        plot(timepoints,psth1(:,2),'b-');
        psthmean1(:,nn)=psth1(time_index,2); % sustained resp
        psthsp1(:,nn)=psth1(prestim,2); % spont rate
        if tar_reward==BF
            legend({'nonBF','BF reward'},'FontSize',7);
            reward(nn)=1; %1 is BF reward, 0 is nonBF reward
        else
            legend({'nonBF reward','BF'},'FontSize',7);
            reward(nn)=0; %1 is BF reward, 0 is nonBF reward
        end
    end
% if s1{2}==BF
% plot(timepoints,psth1(:,1),'b-');hold on
% plot(timepoints,psth1(:,2),'r-');
% psthmean1(:,nn)=psth1(time_index,1);
% %legend('BF','nonBF');
% elseif s2{2}==BF
% plot(timepoints,psth1(:,1),'r-');hold on
% plot(timepoints,psth1(:,2),'b-');
% psthmean1(:,nn)=psth1(time_index,2);
% %legend('nonBF','BF');
% end
xlim([-0.5 3]);
legend('boxoff');
ylabel('spike/sec');
xlabel('time (s)');
ymax=max(max(psth1));
%ylim([0 ymax+10])
line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
title(sprintf('%s trials %d-%d',basename(mfilename),trials(1),trials(1)+len1*2),'fontsize',8,'fontweight','normal');
end
% Second half of training session
if length(trials)>66
    if onset==2
        if s1{2}==BF
            psthmean2(:,nn)=psth2(time_index,1);
        elseif s2{2}==BF
            psthmean2(:,nn)=psth2(time_index,2);
        end
        
    else
        subplot(3,2,4)
        %timepoints=(1:size(psth_pre,1))./options.rasterfs - prestimsilence;
        %s1=strsep(tags{1} ,',');
        %s2=strsep(tags{2} ,',');
        if s1{2}==BF
            plot(timepoints,psth2(:,1),'b-');hold on
            plot(timepoints,psth2(:,2),'r-');
            psthmean2(:,nn)=psth2(time_index,1);% sustained resp
            psthsp2(:,nn)=psth2(prestim,1); % spont rate
            %legend('BF','nonBF');
        elseif s2{2}==BF
            plot(timepoints,psth2(:,1),'r-');hold on
            plot(timepoints,psth2(:,2),'b-');
            psthmean2(:,nn)=psth2(time_index,2);% sustained resp
            psthsp2(:,nn)=psth2(prestim,2); % spont rate
            %legend('nonBF','BF');
        end
    
    xlim([-0.5 3]);
    ylabel('spike/sec');
    xlabel('time (s)');
    ymax=max(max(psth2));
    %ylim([0 ymax+10])
    line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
    line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
    title(sprintf('%s trials %d-%d',basename(mfilename),trials(1)+len1*2+1,trials(end)),'fontsize',8,'fontweight','normal');
    end
else
    if onset==2
        psthmean2(:,nn)=NaN(length(time_index),1);
    else
        psthmean2(:,nn)=NaN(length(time_index),1);
        psthsp2(:,nn)=NaN(length(prestim),1); % spont rate
    end
end

if prod(size(mfile_post))~=0
[r_post, tags_post]=loadspikeraster(spkfile_post,options_post); %r is time X repetition X stimulus id
% average across repetitions to get the PSTH
r_post=nanmean(r_post,2);
psth_post = squeeze(r_post)* options_post.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

timepoints=(1:size(psth_post,1))./options_post.rasterfs - prestimsilence;
%time_index=find(timepoints>0.07 & timepoints<=1);

%% Post-training
subplot(3,2,5)
s1=strsep(tags_post{1} ,',');
s2=strsep(tags_post{2} ,',');

if onset==2
    if s1{2}==BF
        psthmean_post(:,nn)=psth_post(time_index,1);
    elseif s2{2}==BF
        psthmean_post(:,nn)=psth_post(time_index,2);
    end
else
    
    if s1{2}==BF
        plot(timepoints,psth_post(:,1),'b-');hold on
        plot(timepoints,psth_post(:,2),'r-');
        psthmean_post(:,nn)=psth_post(time_index,1);% sustained resp
        psthsp_post(:,nn)=psth_post(prestim,1); % spont rate
        legend({'BF','nonBF'},'FontSize',7);
    elseif s2{2}==BF
        plot(timepoints,psth_post(:,1),'r-');hold on
        plot(timepoints,psth_post(:,2),'b-');
        psthmean_post(:,nn)=psth_post(time_index,2);% sustained resp
        psthsp_post(:,nn)=psth_post(prestim,2); % spont rate
        legend({'nonBF','BF'},'FontSize',7);
    end
    xlim([-0.5 3]);
    legend('boxoff');
    ylabel('spike/sec');
    xlabel('time (s)');
    ymax=max(max(psth_post));
    %ylim([0 ymax+10])
    line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
    line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
    title(sprintf('%s trials ALL',basename(mfilename_post)),'fontsize',8,'fontweight','normal');
end
end
% if onset==0
%     if reward(nn)==1 %onBF reward
%         saveas(gcf,[cellid '_psth'],'epsc')
%         saveas(gcf,[cellid '_psth'],'png')
%         savefig([cellid '_psth.fig'])
%     else %offBF reward
%         saveas(gcf,[cellid '_rev_psth'],'epsc')
%         saveas(gcf,[cellid '_rev_psth'],'png')
%         savefig([cellid '_rev_psth.fig'])
%     end
% end
end

% "_new" suffix means we're using the new database (stringent trial
% selection)
if onset==2
    save('CCtraining_psth_all_new','psthmean_pre','psthmean1','psthmean2','psthmean_post','reward')
elseif onset==0
    save('CCtraining_psth_resp_new','psthmean_pre','psthmean1','psthmean2','psthmean_post','reward',...
        'psthsp_pre','psthsp1','psthsp2','psthsp_post')
elseif onset==1
    save('CCtraining_psth_onset_new','psthmean_pre','psthmean1','psthmean2','psthmean_post','reward',...
        'psthsp_pre','psthsp1','psthsp2','psthsp_post')
    
end