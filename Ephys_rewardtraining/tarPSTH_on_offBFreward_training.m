% BF tone/target PSTH for neurons with BF-reward and offBF-reward training

clear all
close all
baphy_set_path


CCtraining_database_new
fs=30;
xx=0;

cellid=strings(1,n);
a=1;
%a=83;

for nn=a:n
    
    %nn
    resppath=valid_data(nn).resppath;
    mfilepath=valid_data(nn).mfilepath;
    
    cellid{nn}=valid_data(nn).cellid;
    
    if nn >1 & strcmp(cellid{nn},'PNB023e-b1')~=1
        name_equal=strcmp(cellid{nn-1},cellid{nn});
        
        if name_equal==1
            nn
            cellid{nn}
            xx=xx+1;
            mfile_pre=valid_data(nn-1).mfile_pre;
            resp_pre=valid_data(nn-1).resp_pre;
            mfile_active1=valid_data(nn-1).mfile_active;
            resp_active1=valid_data(nn-1).resp_active;
            mfile_mid=valid_data(nn-1).mfile_post;
            resp_mid=valid_data(nn-1).resp_post;
            
            mfile_active2=valid_data(nn).mfile_active;
            resp_active2=valid_data(nn).resp_active;
            mfile_post=valid_data(nn).mfile_post;
            resp_post=valid_data(nn).resp_post;
            
            BF=valid_data(nn).BF;
            offBF=valid_data(nn).offBF;
            
            tar_reward1=valid_data(nn-1).tar_reward;
            tar_reward2=valid_data(nn).tar_reward;
            
            cellfiledata1 = dbgetscellfile('cellid', cellid{nn-1});
            cellfiledata2 = dbgetscellfile('cellid', cellid{nn});
            
            mfilename_pre=[mfilepath mfile_pre];
            spkfile_pre=[resppath resp_pre];
            
            mfilename1=[mfilepath mfile_active1];
            spkfile1=[resppath resp_active1];
            
            mfilename_mid=[mfilepath mfile_mid];
            spkfile_mid=[resppath resp_mid];
            
            mfilename2=[mfilepath mfile_active2];
            spkfile2=[resppath resp_active2];
            
            mfilename_post=[mfilepath mfile_post];
            spkfile_post=[resppath resp_post];
            
            %options
            channel=valid_data(nn).channel;
            unit=valid_data(nn).unit;
            trials1=valid_data(nn-1).trials;
            trials2=valid_data(nn).trials;
            if nn<0
                options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'target'}});
                options_mid=options_pre;
                options_post=options_pre;
            else
                options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}});
                options_mid=options_pre;
                options_post=options_pre;
            end
            options=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'target'}},'includeincorrect',1);
            
            
            %% load data
            [r_pre, tags_pre]=loadspikeraster(spkfile_pre,options_pre); %r is time X repetition X stimulus id
            s1pre=strsep(tags_pre{1} ,',');
            s2pre=strsep(tags_pre{2} ,',');
            [r_mid, tags_mid]=loadspikeraster(spkfile_mid,options_mid);
            s1mid=strsep(tags_mid{1} ,',');
            s2mid=strsep(tags_mid{2} ,',');
            [r_post, tags_post]=loadspikeraster(spkfile_post,options_post);
            s1post=strsep(tags_post{1} ,',');
            s2post=strsep(tags_post{2} ,',');
            [r1, tags1]=loadspikeraster(spkfile1,options);
            s11=strsep(tags1{1} ,',');
            s21=strsep(tags1{2} ,',');
            [r2, tags2]=loadspikeraster(spkfile2,options);
            s12=strsep(tags2{1} ,',');
            s22=strsep(tags2{2} ,',');
            
            % average across repetitions to get the PSTH
            r_pre=nanmean(r_pre,2);
            psth_pre = squeeze(r_pre)* options_pre.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
            r_mid=nanmean(r_mid,2);
            psth_mid = squeeze(r_mid)* options_mid.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
            r_post=nanmean(r_post,2);
            psth_post = squeeze(r_post)* options_post.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
            if length(trials1)<=66
                len1= floor(length(trials1)/2);
                tstart1=ceil(trials1(1)/2);    
            else
                len1= floor(length(trials1)/4);
                %len2= len1*2;
                tstart1=ceil(trials1(1)/4);
            end
                       
%             %if length(trials1)<=66
%             len1= floor(length(trials1)/2);
%             tstart1=ceil(trials1(1)/2);
%             %[r1, tags1]=loadspikeraster(spkfile1,options);
            resp1=nanmean(r1(:,tstart1:tstart1+len1-1,:),2);
            psth1 = squeeze(resp1)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
            %end
            if length(trials2)<=66
                len2= floor(length(trials2)/2);
                tstart2=ceil(trials2(1)/2);    
            else
                len2= floor(length(trials2)/4);
                %len2= len1*2;
                tstart2=ceil(trials2(1)/4);
            end
            %[r2, tags2]=loadspikeraster(spkfile2,options);
            resp2=nanmean(r2(:,tstart2:tstart2+len2-1,:),2);
            psth2 = squeeze(resp2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
            %end
            % timepoints
            prestimsilence=0.5; % 0.5 sec prestimulus silence
            timepoints=(1:size(psth_pre,1))./options_pre.rasterfs - prestimsilence;
            time_psth=find(timepoints>=-0.5 & timepoints<=3.0); % -0.5 to 3.0s PSTH
            time_sp=find(timepoints>=-0.5 & timepoints<=0);
            %% plotting
            
            % PSTH drawing
            if s1pre{2}==BF
                psthmean_pre=psth_pre(time_psth,1);
                psthsp_pre=psth_pre(time_sp,1);
            elseif s2pre{2}==BF
                psthmean_pre=psth_pre(time_psth,2);
                psthsp_pre=psth_pre(time_sp,2);
            end
            if s1mid{2}==BF
                psthmean_mid=psth_mid(time_psth,1);
                psthsp_mid=psth_mid(time_sp,1);
            elseif s2mid{2}==BF
                psthmean_mid=psth_mid(time_psth,2);
                psthsp_mid=psth_mid(time_sp,2);
            end
            if s1post{2}==BF
                psthmean_post=psth_post(time_psth,1);
                psthsp_post=psth_post(time_sp,1);
            elseif s2post{2}==BF
                psthmean_post=psth_post(time_psth,2);
                psthsp_post=psth_post(time_sp,2);
            end
            if s11{2}==BF
                psthmean1=psth1(time_psth,1);
                psthsp1=psth1(time_sp,1);
            elseif s21{2}==BF
                psthmean1=psth1(time_psth,2);
                psthsp1=psth1(time_sp,2);
            end
            if s12{2}==BF
                psthmean2=psth2(time_psth,1);
                psthsp2=psth2(time_sp,1);
            elseif s22{2}==BF
                psthmean2=psth2(time_psth,2);
                psthsp2=psth2(time_sp,2);
            end
            mean_pre=mean(psthmean_pre-mean(psthsp_pre,1),1);
            se_pre=(std(psthmean_pre-mean(psthsp_pre,1),0,1))/sqrt(size(psthmean_pre,1));
            mean_mid=mean(psthmean_mid-mean(psthsp_mid,1),1);
            se_mid=(std(psthmean_mid-mean(psthsp_mid,1),0,1))/sqrt(size(psthmean_mid,1));
            mean_post=mean(psthmean_post-mean(psthsp_post,1),1);
            se_post=(std(psthmean_post-mean(psthsp_post,1),0,1))/sqrt(size(psthmean_post,1));
            
            mean1=mean(psthmean1-mean(psthsp1,1),1);
            se_mean1=(std(psthmean1-mean(psthsp1,1),0,1))/sqrt(size(psthmean1,1));
            mean2=mean(psthmean2-mean(psthsp2,1),1);
            se_mean2=(std(psthmean2-mean(psthsp2,1),0,1))/sqrt(size(psthmean2,1));
           
            if mean_pre-2*se_pre>0 && mean_mid-2*se_mid>0 && mean_post-2*se_post>0 && mean1-2*se_mean1>0 && mean2-2*se_mean2>0
                figure(xx);
                clf
                if tar_reward1==BF && tar_reward2==offBF
                    subplot(3,5,1)
                    plot(timepoints(time_psth),psthmean_pre);
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    %xlabel('time(s)')
                    ylabel('spikes/sec')
                    title([cellid{nn} 'passive'])
                    subplot(3,5,2)
                    plot(timepoints(time_psth),psthmean1);
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    title('active BFreward')
                    subplot(3,5,3)
                    plot(timepoints(time_psth),psthmean_mid);
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    title('passive')
                    subplot(3,5,4)
                    plot(timepoints(time_psth),psthmean2);
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    title('active offBFreward')
                    subplot(3,5,5)
                    plot(timepoints(time_psth),psthmean_post);
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    title( 'passive')
                   
                end
            end
                  
            %% Lick rate
            clear options_pre options_mid options_post options
            if nn<0
                options_pre=struct('rasterfs',fs,'lfp',2,'tag_masks',{{'target'}});
                options_mid=options_pre;
                options_post=options_pre;
            else
                options_pre=struct('rasterfs',fs,'lfp',2,'tag_masks',{{'reference'}});
                options_mid=options_pre;
                options_post=options_pre;
            end
            options=struct('rasterfs',fs,'lfp',2,'tag_masks',{{'target'}},'includeincorrect',1);
            
            [l_pre, tags_pre]=loadevpraster(mfilename_pre,options_pre); %r is time X repetition X stimulus id
            s1pre=strsep(tags_pre{1} ,',');
            s2pre=strsep(tags_pre{2} ,',');
            [l_mid, tags_mid]=loadevpraster(mfilename_mid,options_mid);
            s1mid=strsep(tags_mid{1} ,',');
            s2mid=strsep(tags_mid{2} ,',');
            [l_post, tags_post]=loadevpraster(mfilename_post,options_post);
            s1post=strsep(tags_post{1} ,',');
            s2post=strsep(tags_post{2} ,',');
            [l1, tags1]=loadevpraster(mfilename1,options);
            s11=strsep(tags1{1} ,',');
            s21=strsep(tags1{2} ,',');
            [l2, tags2]=loadevpraster(mfilename2,options);
            s12=strsep(tags2{1} ,',');
            s22=strsep(tags2{2} ,',');
            
             % average across repetitions to get the PSTH
            l_pre=nanmean(l_pre,2);
            licks_pre = squeeze(l_pre)* options_pre.rasterfs; %
            l_mid=nanmean(l_mid,2);
            licks_mid = squeeze(l_mid)* options_mid.rasterfs; %
            l_post=nanmean(l_post,2);
            licks_post = squeeze(l_post)* options_post.rasterfs; %
           
            l1mean=nanmean(l1(:,tstart1:tstart1+len1-1,:),2);
            licks1 = squeeze(l1mean)* options.rasterfs; %
            
            l2mean=nanmean(l2(:,tstart2:tstart2+len2-1,:),2);
            licks2 = squeeze(l2mean)* options.rasterfs; %
            % average lick rate drawing
            if s1pre{2}==tar_reward1
                lickmean_pre=licks_pre(time_psth,1);
            elseif s2pre{2}==tar_reward1
                lickmean_pre=licks_pre(time_psth,2); 
            end
            if s1mid{2}==tar_reward1
                lickmean_mid=licks_mid(time_psth,1);
            elseif s2mid{2}==tar_reward1
                lickmean_mid=licks_mid(time_psth,2);
            end
            if s1post{2}==tar_reward1
                lickmean_post=licks_post(time_psth,1);
            elseif s2post{2}==tar_reward1
                lickmean_post=licks_post(time_psth,2);
            end
            if s11{2}==tar_reward1
                lickmean1=licks1(time_psth,1);
                l1_BFlick=l1(:,tstart1:tstart1+len1-1,1)*options.rasterfs;
                l1_BFlick=l1_BFlick(time_psth,:);
            elseif s21{2}==tar_reward1
                lickmean1=licks1(time_psth,2);
                l1_BFlick=l1(:,tstart1:tstart1+len1-1,2)*options.rasterfs;
                l1_BFlick=l1_BFlick(time_psth,:);
            end
            if s12{2}==tar_reward1
                lickmean2=licks2(time_psth,1);
                l2_BFlick=l2(:,tstart2:tstart2+len2-1,1)*options.rasterfs;
                l2_BFlick=l2_BFlick(time_psth,:);
            elseif s22{2}==tar_reward1
                lickmean2=licks2(time_psth,2);
                l2_BFlick=l2(:,tstart2:tstart2+len2-1,2)*options.rasterfs;
                l2_BFlick=l2_BFlick(time_psth,:);
            end
            if mean_pre-2*se_pre>0 && mean_mid-2*se_mid>0 && mean_post-2*se_post>0 && mean1-2*se_mean1>0 && mean2-2*se_mean2>0
                figure(xx);
                if tar_reward1==BF && tar_reward2==offBF
                    subplot(3,5,6)
                    plot(timepoints(time_psth),lickmean_pre);
                    ylim([-1 max([lickmean_pre;lickmean1;lickmean_mid; lickmean2; lickmean_post])])
                    xlim([-0.5 3])
                    xlabel('time(s)')
                    ylabel('lick rate (/sec)')
                    title([cellid{nn} 'passive'])
                    subplot(3,5,7)
                    plot(timepoints(time_psth),lickmean1);
                    ylim([-1 max([lickmean_pre;lickmean1;lickmean_mid; lickmean2; lickmean_post])])
                    xlim([-0.5 3])
                    title( 'active BFreward')
                    subplot(3,5,8)
                    plot(timepoints(time_psth),lickmean_mid);
                    ylim([-1 max([lickmean_pre;lickmean1;lickmean_mid; lickmean2; lickmean_post])])
                    xlim([-0.5 3])
                    title( 'passive')
                    subplot(3,5,9)
                    plot(timepoints(time_psth),lickmean2);
                    ylim([-1 max([lickmean_pre;lickmean1;lickmean_mid; lickmean2; lickmean_post])])
                    xlim([-0.5 3])
                    title( 'active offBFreward')
                    subplot(3,5,10)
                    plot(timepoints(time_psth),lickmean_post);
                    ylim([-1 max([lickmean_pre;lickmean1;lickmean_mid; lickmean2; lickmean_post])])
                    xlim([-0.5 3])
                    title('passive')
                end
            end
                %% lick raster
                if mean_pre-2*se_pre>0 && mean_mid-2*se_mid>0 && mean_post-2*se_post>0 && mean1-2*se_mean1>0 && mean2-2*se_mean2>0
                    figure(xx)
                    subplot(3,5,12)
                    %                 tbl1=table(trials1,)
                    %                 heatmap(l1_BFlick,timepoints(time_psth),1:size(l1_BFlick,2))
                    xvalues1=timepoints(time_psth);
                    yvalues1=1:size(l1_BFlick,2);
                    imagesc(xvalues1,yvalues1,l1_BFlick');
                    xlim([-0.5 3])
                    xlabel('time (s)')
                    ylabel('trial')
                    subplot(3,5,14)
                    xvalues2=timepoints(time_psth);
                    yvalues2=1:size(l2_BFlick,2);
                    imagesc(xvalues2,yvalues2,l2_BFlick');
                    xlim([-0.5 3])
                    xlabel('time (s)')
                    ylabel('trial')
                    
                    %% lick-raster filter
                    if s11{2}==BF
                        r1_tar=r1(time_psth,tstart1:tstart1+len1-1,1);
                        %r1_tar2=r1(time_psth,tstart1:tstart1+len1-1,1);
                    elseif s21{2}==BF
                        r1_tar=r1(time_psth,tstart1:tstart1+len1-1,2);
                        %r1_tar2=r1(time_psth,tstart1:tstart1+len1-1,2);
                    end
                    if s12{2}==BF
                        r2_tar=r2(time_psth,tstart2:tstart2+len2-1,1);
                    elseif s22{2}==BF
                        r2_tar=r2(time_psth,tstart2:tstart2+len2-1,2);
                    end
                    %l1_BFlick is the lick raster
                    t_per_bin=(1/fs);
                    t=0.2;
                    conv_time=[-t,t];
                    conv_bin=round(conv_time/t_per_bin);
                    conv_filter=ones(1,conv_bin(2)-conv_bin(1));
                    for trial_no1=1: size(r1_tar,2)
                        lick_filter1=find(l1_BFlick(:,trial_no1)>0);
                        if isnan(lick_filter1)~=1
                            for ii=1:length(lick_filter1)
                                conv_box=conv(conv_filter',l1_BFlick(lick_filter1(ii),trial_no1));
                                if lick_filter1(ii)<= conv_bin(2)
                                    l1_BFlick(lick_filter1(ii):lick_filter1(ii)+conv_bin(2)-1,trial_no1)=conv_box(lick_filter1(ii):lick_filter1(ii)+conv_bin(2)-1);
                                else
                                    l1_BFlick(lick_filter1(ii)+conv_bin(1):lick_filter1(ii)+conv_bin(2)-1,trial_no1)=conv_box;
                                end
                            end
                            
                        end
                        l1_BFlick=l1_BFlick(1:length(time_psth),:);
                        r1_tar(l1_BFlick(:,trial_no1)>0,trial_no1)=nan;
                    end
                    psth1_nolick=nanmean(r1_tar,2)*options.rasterfs;
                    %psth1_nolick2=nanmean(r1_tar2,2)*options.rasterfs;
                    
                    for trial_no2=1: size(r2_tar,2)
                        lick_filter2=find(l2_BFlick(:,trial_no2)>0);
                        if isnan(lick_filter2)~=1
                            for ii=1:length(lick_filter2)
                                conv_box=conv(conv_filter',l2_BFlick(lick_filter2(ii),trial_no2));
                                if lick_filter2(ii)<= conv_bin(2)
                                    l2_BFlick(lick_filter2(ii):lick_filter2(ii)+conv_bin(2)-1,trial_no2)=conv_box(lick_filter2(ii):lick_filter2(ii)+conv_bin(2)-1);
                                else
                                    l2_BFlick(lick_filter2(ii)+conv_bin(1):lick_filter2(ii)+conv_bin(2)-1,trial_no2)=conv_box;
                                end
                            end
                            
                        end
                        l2_BFlick=l2_BFlick(1:length(time_psth),:);
                        r2_tar(l2_BFlick(:,trial_no2)>0,trial_no2)=nan;
                    end
                    psth2_nolick=nanmean(r2_tar,2)*options.rasterfs;
                    
                    figure(xx)
                    subplot(3,5,2)
                    hold on;
                    plot(timepoints(time_psth),psth1_nolick,'r-');
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    xlabel('time(s)')
                    ylabel('spikes/sec')
                    
                    %                 subplot(3,5,18)
                    %                 plot(timepoints(time_psth),psth1_nolick2);
                    %                 ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    %                 xlim([-0.5 3])
                    
                    subplot(3,5,4)
                    hold on;
                    plot(timepoints(time_psth),psth2_nolick,'r-');
                    ylim([min([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post]) max([psthmean_pre;psthmean1;psthmean_mid; psthmean2; psthmean_post])])
                    xlim([-0.5 3])
                    xlabel('time(s)')
                    ylabel('spikes/sec')
                    
                    
                    
                    cellid{nn}
                    savefig([cellid{nn} '_BFoffBFreward.fig'])
                    saveas(gcf,[cellid{nn} '_BFoffBFreward'],'png')
                    
                end
        end
    end
end