% BF tone/target PSTH for neurons with BF-reward training only

clear all
close all
baphy_set_path


CCtraining_database
fs=30;
xx=0;

cellid=strings(1,n);
for nn=1:n
    resppath=valid_data(nn).resppath;
    mfilepath=valid_data(nn).mfilepath;
    BF=valid_data(nn).BF;
    offBF=valid_data(nn).offBF;
    tar_reward=valid_data(nn).tar_reward;
    
    cellid{nn}=valid_data(nn).cellid;
    
    if tar_reward==BF & strcmp(cellid{nn},'PNB023e-b1')~=1 & strcmp(cellid{nn},'PNB024c-a1')~=1 & strcmp(cellid{nn},'PNB019i-a3')~=1 & strcmp(cellid{nn},'SIK006b-02-1')~=1
        
        nn
        cellid{nn}
        
        mfile_pre=valid_data(nn).mfile_pre;
        resp_pre=valid_data(nn).resp_pre;
        mfile_active=valid_data(nn).mfile_active;
        resp_active=valid_data(nn).resp_active;
        mfile_post=valid_data(nn).mfile_post;
        resp_post=valid_data(nn).resp_post;
        
        cellfiledata1 = dbgetscellfile('cellid', cellid{nn});
        
        mfilename_pre=[mfilepath mfile_pre];
        spkfile_pre=[resppath resp_pre];
        
        mfilename1=[mfilepath mfile_active];
        spkfile1=[resppath resp_active];
        
        mfilename_post=[mfilepath mfile_post];
        spkfile_post=[resppath resp_post];
        
        %options
        channel=valid_data(nn).channel;
        unit=valid_data(nn).unit;
        trials1=valid_data(nn).trials;
        if nn<4
            options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'target'}});
            options_mid=options_pre;
            options_post=options_pre;
        else
            options_pre=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'reference'}});
            options_mid=options_pre;
            options_post=options_pre;
        end
        options=struct('channel',channel,'unit',unit,'rasterfs',fs,'includeprestim',1,'tag_masks',{{'target'}},'includeincorrect',1);
        
        %% load data
        [r_pre, tags_pre]=loadspikeraster(spkfile_pre,options_pre); %r is time X repetition X stimulus id
        s1pre=strsep(tags_pre{1} ,',');
        s2pre=strsep(tags_pre{2} ,',');
        [r_post, tags_post]=loadspikeraster(spkfile_post,options_post);
        s1post=strsep(tags_post{1} ,',');
        s2post=strsep(tags_post{2} ,',');
        [r1, tags1]=loadspikeraster(spkfile1,options);
        s11=strsep(tags1{1} ,',');
        s21=strsep(tags1{2} ,',');
        
        % average across repetitions to get the PSTH
        resp_pre=nanmean(r_pre,2);
        psth_pre = squeeze(resp_pre)* options_pre.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
        
        resp_post=nanmean(r_post,2);
        psth_post = squeeze(resp_post)* options_post.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
        
        if length(trials1)<=66
            len1= floor(length(trials1)/2);
            tstart1=ceil(trials1(1)/2);
            
        else
            len1= floor(length(trials1)/4);
            len2= len1*2;
            tstart1=ceil(trials1(1)/4);
        end
%         
%         len1= floor(length(trials1)/2);
%         tstart1=ceil(trials1(1)/2);
        resp1=nanmean(r1(:,tstart1:tstart1+len1-1,:),2);
        psth1 = squeeze(resp1)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
        
        % timepoints
        prestimsilence=0.5; % 0.5 sec prestimulus silence
        timepoints=(1:size(psth_pre,1))./options_pre.rasterfs - prestimsilence;
        time_psth=find(timepoints>=-0.5 & timepoints<=3.0); % -0.5 to 3.0s PSTH
        time_sp=find(timepoints>=-0.5 & timepoints<=0);
        %% plotting
        
        % PSTH drawing
        if s1pre{2}==BF
            psthmean_pre=psth_pre(time_psth,1);
            psthsp_pre=psth_pre(time_sp,1);
        elseif s2pre{2}==BF
            psthmean_pre=psth_pre(time_psth,2);
            psthsp_pre=psth_pre(time_sp,2);
        end
        
        if s1post{2}==BF
            psthmean_post=psth_post(time_psth,1);
            psthsp_post=psth_post(time_sp,1);
        elseif s2post{2}==BF
            psthmean_post=psth_post(time_psth,2);
            psthsp_post=psth_post(time_sp,2);
        end
        
        if s11{2}==BF
            psthmean1=psth1(time_psth,1);
            psthsp1=psth1(time_sp,1);
        elseif s21{2}==BF
            psthmean1=psth1(time_psth,2);
            psthsp1=psth1(time_sp,2);
        end
        
        mean_pre=mean(psthmean_pre-mean(psthsp_pre,1),1);
        se_pre=(std(psthmean_pre-mean(psthsp_pre,1),0,1))/sqrt(size(psthmean_pre,1));
        
        mean_post=mean(psthmean_post-mean(psthsp_post,1),1);
        se_post=(std(psthmean_post-mean(psthsp_post,1),0,1))/sqrt(size(psthmean_post,1));
        
        mean1=mean(psthmean1-mean(psthsp1,1),1);
        se_mean1=(std(psthmean1-mean(psthsp1,1),0,1))/sqrt(size(psthmean1,1));
        
        if mean_pre-2*se_pre>0 && mean_post-2*se_post>0 && mean1-2*se_mean1>0
            xx=xx+1;
            figure(xx);
            clf
            subplot(3,3,1)
            plot(timepoints(time_psth),psthmean_pre);
            ylim([min([psthmean_pre;psthmean1; psthmean_post]) max([psthmean_pre;psthmean1; psthmean_post])])
            xlim([-0.5 3])
            %xlabel('time(s)')
            ylabel('spikes/sec')
            title([cellid{nn} 'passive'])
            subplot(3,3,2)
            plot(timepoints(time_psth),psthmean1);
            ylim([min([psthmean_pre;psthmean1; psthmean_post]) max([psthmean_pre;psthmean1; psthmean_post])])
            xlim([-0.5 3])
            title('active BFreward')
            subplot(3,3,3)
            plot(timepoints(time_psth),psthmean_post);
            ylim([min([psthmean_pre;psthmean1; psthmean_post]) max([psthmean_pre;psthmean1; psthmean_post])])
            xlim([-0.5 3])
            title( 'passive')
        
        
        %% Lick rate
        clear options_pre options_mid options_post options
        if nn<4
            options_pre=struct('rasterfs',fs,'lfp',2,'tag_masks',{{'target'}});
            options_mid=options_pre;
            options_post=options_pre;
        else
            options_pre=struct('rasterfs',fs,'lfp',2,'tag_masks',{{'reference'}});
            options_mid=options_pre;
            options_post=options_pre;
        end
        options=struct('rasterfs',fs,'lfp',2,'tag_masks',{{'target'}},'includeincorrect',1);
        
        [l_pre, tags_pre]=loadevpraster(mfilename_pre,options_pre); %r is time X repetition X stimulus id
        s1pre=strsep(tags_pre{1} ,',');
        s2pre=strsep(tags_pre{2} ,',');
        [l_post, tags_post]=loadevpraster(mfilename_post,options_post);
        s1post=strsep(tags_post{1} ,',');
        s2post=strsep(tags_post{2} ,',');
        [l1, tags1]=loadevpraster(mfilename1,options);
        s11=strsep(tags1{1} ,',');
        s21=strsep(tags1{2} ,',');
        
        % average across repetitions to get the PSTH
        lmean_pre=nanmean(l_pre,2);
        licks_pre = squeeze(lmean_pre)* options_pre.rasterfs; %
        
        lmean_post=nanmean(l_post,2);
        licks_post = squeeze(lmean_post)* options_post.rasterfs; %
        
        l1mean=nanmean(l1(:,tstart1:tstart1+len1-1,:),2);
        licks1 = squeeze(l1mean)* options.rasterfs; %
        % average lick rate drawing
        if s1pre{2}==tar_reward
            lickmean_pre=licks_pre(time_psth,1);
            l_pre_BFlick=l_pre(time_psth,:,1)*options.rasterfs;
        elseif s2pre{2}==tar_reward
            lickmean_pre=licks_pre(time_psth,2);
            l_pre_BFlick=l_pre(time_psth,:,2)*options.rasterfs;
        end
        if s1post{2}==tar_reward
            lickmean_post=licks_post(time_psth,1);
            l_post_BFlick=l_post(time_psth,:,1)*options.rasterfs;
        elseif s2post{2}==tar_reward
            lickmean_post=licks_post(time_psth,2);
            l_post_BFlick=l_post(time_psth,:,2)*options.rasterfs;
        end
        if s11{2}==tar_reward
            lickmean1=licks1(time_psth,1);
            l1_BFlick=l1(time_psth,tstart1:tstart1+len1-1,1)*options.rasterfs;
            %l1_BFlick=l1_BFlick(time_psth,:);
        elseif s21{2}==tar_reward
            lickmean1=licks1(time_psth,2);
            l1_BFlick=l1(time_psth,tstart1:tstart1+len1-1,2)*options.rasterfs;
            %l1_BFlick=l1_BFlick(time_psth,:);
        end
        
       % if mean_pre-2*se_pre>0 && mean_post-2*se_post>0 && mean1-2*se_mean1>0
            figure(xx);
            subplot(3,3,4)
            plot(timepoints(time_psth),lickmean_pre);
            ylim([-1 max([lickmean_pre;lickmean1;lickmean_post])])
            xlim([-0.5 3])
            xlabel('time(s)')
            ylabel('lick rate (/sec)')
            title([cellid{nn} 'passive'])
            subplot(3,3,5)
            plot(timepoints(time_psth),lickmean1);
            ylim([-1 max([lickmean_pre;lickmean1;lickmean_post])])
            xlim([-0.5 3])
            title( 'active BFreward')
            subplot(3,3,6)
            plot(timepoints(time_psth),lickmean_post);
            ylim([-1 max([lickmean_pre;lickmean1;lickmean_post])])
            xlim([-0.5 3])
            title('passive')
            
        %end
        %% lick raster
        %if mean_pre-2*se_pre>0 && mean_post-2*se_post>0 && mean1-2*se_mean1>0
            figure(xx)
            xvalues1=timepoints(time_psth);
            yvalues1=1:size(l1_BFlick,2);
            subplot(3,3,7)
            imagesc(xvalues1,yvalues1,l_pre_BFlick');
            xlim([-0.5 3])
            xlabel('time (s)')
            ylabel('trial')
            
            subplot(3,3,8)
            imagesc(xvalues1,yvalues1,l1_BFlick');
            xlim([-0.5 3])
            xlabel('time (s)')
            ylabel('trial')
            
            subplot(3,3,9)
            imagesc(xvalues1,yvalues1,l_post_BFlick');
            xlim([-0.5 3])
            xlabel('time (s)')
            ylabel('trial')
            
            %% lick-raster filter
            if s11{2}==BF
                r1_tar=r1(time_psth,tstart1:tstart1+len1-1,1);
                %r1_tar2=r1(time_psth,tstart1:tstart1+len1-1,1);
            elseif s21{2}==BF
                r1_tar=r1(time_psth,tstart1:tstart1+len1-1,2);
                %r1_tar2=r1(time_psth,tstart1:tstart1+len1-1,2);
            end
            
            if s1pre{2}==BF
                 r_pre_tar=r_pre(time_psth,:,1);
            elseif s2pre{2}==BF
                r_pre_tar=r_pre(time_psth,:,2);
            end
            
            if s1post{2}==BF
                r_post_tar=r_post(time_psth,:,1);
            elseif s2post{2}==BF
                r_post_tar=r_post(time_psth,:,2);
            end
            
            
            %l1_BFlick is the lick raster
            t_per_bin=(1/fs);
            t=0.2;
            conv_time=[-t,t];
            conv_bin=round(conv_time/t_per_bin);
            conv_filter=ones(1,conv_bin(2)-conv_bin(1));
            for trial_no1=1: size(r1_tar,2)
                lick_filter1=find(l1_BFlick(:,trial_no1)>0);
                if isnan(lick_filter1)~=1
                    for ii=1:length(lick_filter1)
                        conv_box=conv(conv_filter',l1_BFlick(lick_filter1(ii),trial_no1));
                        if lick_filter1(ii)<= conv_bin(2)
                            l1_BFlick(lick_filter1(ii):lick_filter1(ii)+conv_bin(2)-1,trial_no1)=conv_box(lick_filter1(ii):lick_filter1(ii)+conv_bin(2)-1);
                        else
                            l1_BFlick(lick_filter1(ii)+conv_bin(1):lick_filter1(ii)+conv_bin(2)-1,trial_no1)=conv_box;
                        end
                    end
                    
                end
                l1_BFlick=l1_BFlick(1:length(time_psth),:);
                r1_tar(l1_BFlick(:,trial_no1)>0,trial_no1)=nan;
            end
            psth1_nolick=nanmean(r1_tar,2)*options.rasterfs;
            %psth1_nolick2=nanmean(r1_tar2,2)*options.rasterfs;
            
            %resp pre no lick
             for trial_no=1: size(r_pre_tar,2)
                lick_filter=find(l_pre_BFlick(:,trial_no)>0);
                if isnan(lick_filter)~=1
                    for ii=1:length(lick_filter)
                        conv_box=conv(conv_filter',l_pre_BFlick(lick_filter(ii),trial_no));
                        if lick_filter(ii)<= conv_bin(2)
                            l_pre_BFlick(lick_filter(ii):lick_filter(ii)+conv_bin(2)-1,trial_no)=conv_box(lick_filter(ii):lick_filter(ii)+conv_bin(2)-1);
                        else
                            l_pre_BFlick(lick_filter(ii)+conv_bin(1):lick_filter(ii)+conv_bin(2)-1,trial_no)=conv_box;
                        end
                    end
                    
                end
                l_pre_BFlick=l_pre_BFlick(1:length(time_psth),:);
                r_pre_tar(l_pre_BFlick(:,trial_no)>0,trial_no)=nan;
            end
            psth_pre_nolick=nanmean(r_pre_tar,2)*options.rasterfs;
            
            %resp post no lick
            for trial_no=1: size(r_post_tar,2)
                lick_filter=find(l_post_BFlick(:,trial_no)>0);
                if isnan(lick_filter)~=1
                    for ii=1:length(lick_filter)
                        conv_box=conv(conv_filter',l_post_BFlick(lick_filter(ii),trial_no));
                        if lick_filter(ii)<= conv_bin(2)
                            l_post_BFlick(lick_filter(ii):lick_filter(ii)+conv_bin(2)-1,trial_no)=conv_box(lick_filter(ii):lick_filter(ii)+conv_bin(2)-1);
                        else
                            l_post_BFlick(lick_filter(ii)+conv_bin(1):lick_filter(ii)+conv_bin(2)-1,trial_no)=conv_box;
                        end
                    end
                    
                end
                l_post_BFlick=l_post_BFlick(1:length(time_psth),:);
                r_post_tar(l_post_BFlick(:,trial_no)>0,trial_no)=nan;
            end
            psth_post_nolick=nanmean(r_post_tar,2)*options.rasterfs;
            
            
            figure(xx)
            subplot(3,3,1)
            hold on
            plot(timepoints(time_psth),psth_pre_nolick,'r-');
           
            subplot(3,3,2)
            hold on
            plot(timepoints(time_psth),psth1_nolick,'r-');
            
            
            subplot(3,3,3)
            hold on
            plot(timepoints(time_psth),psth_post_nolick,'r-');
%             ylim([min([psthmean_pre;psthmean1; psthmean_post]) max([psthmean_pre;psthmean1; psthmean_post])])
%             xlim([-0.5 3])
%             xlabel('time(s)')
%             ylabel('spikes/sec')
            
            cellid{nn}
            savefig([cellid{nn} '_BFreward_only.fig'])
            saveas(gcf,[cellid{nn} '_BFreward_only'],'png')
            
            
           
            
           
                psth_pre_all(:,xx)=psthmean_pre;
                psth_post_all(:,xx)=psthmean_post;
                psth1_all_nolick(:,xx)=psth1_nolick;
           
                
            
        end
        
    end
    
end

time_index=find(timepoints>0.07 & timepoints<=1); % sustained resp

mean_pre=mean(psth_pre_all(time_index,:)-mean(psth_pre_all(time_sp,:),1),1);

mean1=mean(psth1_all_nolick(time_index,:)-mean(psth1_all_nolick(time_sp,:),1),1);

mean_post=mean(psth_post_all(time_index,:)-mean(psth_post_all(time_sp,:),1),1);

%normalization
pre_norm=(mean_pre-mean_pre)./(abs(mean_pre)+abs(mean_pre));
train1_norm=(mean1-mean_pre)./(abs(mean1)+abs(mean_pre));
%train2_norm=(mean2-mean_pre)./(abs(mean2)+abs(mean_pre));
post_norm=(mean_post-mean_pre)./(abs(mean_post)+abs(mean_pre));

figure(xx+1)
clf
        for kk=1:length(pre_norm)        
            plot(1:3,[pre_norm(kk) train1_norm(kk) post_norm(kk)]);hold on
        end
grandmean_pre=mean(pre_norm);
grandmean1=mean(train1_norm);
grandmean_post=mean(post_norm);
plot(1:3,[grandmean_pre grandmean1 grandmean_post],'k-','LineWidth',4);
xticks(1:3)
xticklabels({'Pre','Training','Post'})
title('BF reward training (sustain)')
