clear all; close all;

baphy_set_path

dbopen(1);
channel_no=input('Which channel? ');
unit_no=input('Which unit? ');

%% before training
spkfile='/auto/data/daq/Pennybun/PNB042/sorted/PNB042k05_p_TON.spk.mat';
options=struct('channel',channel_no,'unit',unit_no,'rasterfs',30,'includeprestim',1,'tag_masks',{{'Reference'}}); 
[r, tags1]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id

% average across repetitions to get the PSTH
r2=nanmean(r,2);
psth1 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

clear r
clear r2
%% Training, BF tone with reward
spkfile='/auto/data/daq/Pennybun/PNB042/sorted/PNB042k08_a_PTD.spk.mat';
options=struct('channel',channel_no,'unit',unit_no,'rasterfs',30,'includeprestim',1,'tag_masks',{{'Targ'}}); 
[r, tags2]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id

% average across repetitions to get the PSTH
r2=nanmean(r,2);
psth2 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

clear r
clear r2

%% after training
spkfile='/auto/data/daq/Pennybun/PNB042/sorted/PNB042k09_p_TON.spk.mat';
options=struct('channel',channel_no,'unit',unit_no,'rasterfs',30,'includeprestim',1,'tag_masks',{{'Reference'}}); 
[r, tags3]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id

% average across repetitions to get the PSTH
r2=nanmean(r,2);
psth3 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH

clear r
clear r2

% %% Training, nonBF tone with reward
% spkfile='/auto/data/daq/Pennybun/PNB020/sorted/PNB020a10_p_PTD.spk.mat';
% options=struct('channel',channel_no,'unit',unit_no,'rasterfs',30,'includeprestim',1,'tag_masks',{{'Targ'}}); 
% [r, tags4]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id
% 
% % average across repetitions to get the PSTH
% r2=nanmean(r,2);
% psth4 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
% 
% clear r
% clear r2

% %% after training
% spkfile='/auto/data/daq/Pennybun/PNB020/sorted/PNB020a11_p_TON.spk.mat';
% options=struct('channel',channel_no,'unit',unit_no,'rasterfs',30,'includeprestim',1,'tag_masks',{{'Reference'}}); 
% [r, tags5]=loadspikeraster(spkfile,options); %r is time X repetition X stimulus id
% 
% % average across repetitions to get the PSTH
% r2=nanmean(r,2);
% psth5 = squeeze(r2)* options.rasterfs; % also convert to spikes/sec, rasterfs is the sampling rate while drawing PSTH
% 
% 
% clear r
% clear r2

%%
% plotting
prestimsilence=0.5; % 0.5 sec prestimulus silence
ymax1=max(max([psth1;psth3]));
ymax2=max(psth2);
ymax=max([ymax1 ymax2]);

timepoints=(1:size(psth1,1))./options.rasterfs - prestimsilence;
figure;
clf
subplot(3,2,1)
plot(timepoints,psth1)
xlim([-1 5]);
ylabel('spike/sec');
xlabel('time (s)');
s1=strsep(tags1{1} ,',');
s2=strsep(tags1{2} ,',');
legend(num2str(s1{2}),num2str(s2{2}));
title('before training');
ylim([0 ymax+10])
line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
line([1 1],[0 ymax+10],'Color','black','LineStyle','--')


% plotting
timepoints=(1:size(psth2,1))./options.rasterfs - prestimsilence;

subplot(3,2,2)
plot(timepoints,psth2)
xlim([-1 5]);
ylabel('spike/sec');
xlabel('time (s)');
s1=strsep(tags2{1} ,',');
s2=strsep(tags2{2} ,',');
legend([num2str(s1{2}) num2str(s1{3})],[num2str(s2{2}) num2str(s2{3})]);
title('During training, BF tone with reward');
ylim([0 ymax+10])
line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
line([1 1],[0 ymax+10],'Color','black','LineStyle','--')


% plotting
timepoints=(1:size(psth3,1))./options.rasterfs - prestimsilence;

subplot(3,2,3)
plot(timepoints,psth3)
xlim([-1 5]);
ylabel('spike/sec');
xlabel('time (s)');
s1=strsep(tags3{1} ,',');
s2=strsep(tags3{2} ,',');
legend(num2str(s1{2}),num2str(s2{2}));
title('after BF-reward training');
ylim([0 ymax+10])
line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
line([1 1],[0 ymax+10],'Color','black','LineStyle','--')

% %plotting
% timepoints=(1:size(psth4,1))./options.rasterfs - prestimsilence;
% 
% subplot(3,2,4)
% plot(timepoints,psth4)
% xlim([-1 5]);
% ylabel('spike/sec');
% xlabel('time (s)');
% s1=strsep(tags4{1} ,',');
% s2=strsep(tags4{2} ,',');
% legend(num2str(s1{2}),num2str(s2{2}));
% title('During training, nonBF tone with reward');
% ylim([0 ymax+10])
% line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
% line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
% 
% % plotting
% timepoints=(1:size(psth5,1))./options.rasterfs - prestimsilence;
% 
% subplot(3,2,5)
% plot(timepoints,psth5)
% xlim([-1 5]);
% ylabel('spike/sec');
% xlabel('time (s)');
% s1=strsep(tags5{1} ,',');
% s2=strsep(tags5{2} ,',');
% legend(num2str(s1{2}),num2str(s2{2}));
% title('after nonBF-reward training');
% ylim([0 ymax+10])
% line([0 0],[0 ymax+10],'Color','black','LineStyle','--')
% line([1 1],[0 ymax+10],'Color','black','LineStyle','--')
