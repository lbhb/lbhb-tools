%% Classical conditioning training STRF over time analysis
clear all
close all
baphy_set_path

data_to_use = 'database_new'; % more stringent requirement for stable trial_raster
%data_to_use = 'NMKdatabase_new';
%data_to_use = 'database'  % less stringent requirement for stable trial_raster
%data_to_use = 'NMKdatabase';

databasecmd = ['CCtraining_TORC_',data_to_use];
eval(databasecmd);

%CCtraining_TORC_database_new  % more stringent requirement for stable trial_raster
%CCtraining_TORC_NMKdatabase_new
%CCtraining_TORC_database  % less stringent requirement for stable trial_raster
%CCtraining_TORC_NMKdatabase

%n=1;
for nn=1:n
nn
resppath=valid_data(nn).resppath;
mfilepath=valid_data(nn).mfilepath;

cellid=valid_data(nn).cellid;

mfile_pre_torc=valid_data(nn).mfile_pre_torc;
resp_pre_torc=valid_data(nn).resp_pre_torc;
mfile_pre=valid_data(nn).mfile_pre;
resp_pre=valid_data(nn).resp_pre;
mfile_active=valid_data(nn).mfile_active;
resp_active=valid_data(nn).resp_active;
mfile_post=valid_data(nn).mfile_post;
resp_post=valid_data(nn).resp_post;
mfile_post_torc=valid_data(nn).mfile_post_torc;
resp_post_torc=valid_data(nn).resp_post_torc;
BF=valid_data(nn).BF;
offBF=valid_data(nn).offBF;
tar_reward=valid_data(nn).tar_reward;

cellfiledata = dbgetscellfile('cellid', cellid);

%mfilename=[resppath valid_data(n).resp_pre_torc];

mfilename_pre_torc=[mfilepath mfile_pre_torc];
spkfile_pre_torc=[resppath resp_pre_torc];

mfilename_pre=[mfilepath mfile_pre];
spkfile_pre=[resppath resp_pre];

mfilename=[mfilepath mfile_active];
spkfile=[resppath resp_active];

mfilename_post=[mfilepath mfile_post];
spkfile_post=[resppath resp_post];

mfilename_post_torc=[mfilepath mfile_post_torc];
spkfile_post_torc=[resppath resp_post_torc];


channel=valid_data(nn).channel;
unit=valid_data(nn).unit;
trials=valid_data(nn).trials;


USEFIRSTCYCLE=0;

figure(nn);
clf
%if nn>1
a = subplot(3,3,1);
[strfest,snr,StimParam] = strf_offline2(mfilename_pre_torc,spkfile_pre_torc,channel,unit,a);
title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_pre),snr),'fontsize',8,'fontweight','normal');
snr_pre_torc(nn)=snr;

% else
% a = subplot(3,3,1);
% [strfest,snr,StimParam] = strf_offline2(mfilename_pre,spkfile_pre,channel,unit,a);
% title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_pre),snr),'fontsize',8,'fontweight','normal');
% snr_pre_torc(nn)=snr;
% end

LoadMFile(mfilename);
TargetFrequencies = exptparams.TrialObject.TargetHandle.Frequencies;
tarfreqasoctaves = log2(TargetFrequencies)-log2(StimParam.lfreq);

if length(trials)<=66
    len1= floor(length(trials)/2);
    len2= length(trials);
else
    len1= floor(length(trials)/3);
    len2= len1*2;
    len3= len1*3;
end


a = subplot(3,3,4);
options = struct('usefirstcycle', USEFIRSTCYCLE, 'trialrange', trials(1):trials(len1));
[strfest,snr,StimParam] = strf_offline2(mfilename,spkfile,channel,unit,a,options);
snr1(nn)=snr;
title(sprintf('%s trials %d-%d snr=%.2f',basename(mfilename),options.trialrange(1),options.trialrange(end),snr),'fontsize',8,'fontweight','normal');
xlim = get(gca,'XLim');
hold on
if TargetFrequencies(1)==BF && tar_reward==BF
    plot(xlim,tarfreqasoctaves(1)*[1 1],'b-');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'r--');
    reward(nn)=1; %1 is BF reward, 0 is nonBF reward
elseif TargetFrequencies(2)==BF && tar_reward==BF
    plot(xlim,tarfreqasoctaves(1)*[1 1],'r--');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'b-');
    reward(nn)=1; %1 is BF reward, 0 is nonBF reward
elseif TargetFrequencies(1)==BF && tar_reward==offBF
     plot(xlim,tarfreqasoctaves(1)*[1 1],'b--');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'r-');
    reward(nn)=0; %1 is BF reward, 0 is nonBF reward
elseif TargetFrequencies(2)==BF && tar_reward==offBF
     plot(xlim,tarfreqasoctaves(1)*[1 1],'r-');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'b--');
    reward(nn)=0; %1 is BF reward, 0 is nonBF reward
end
hold off

a = subplot(3,3,5);
options = struct('usefirstcycle', USEFIRSTCYCLE, 'trialrange', trials(len1+1):trials(len2));
[strfest,snr,StimParam] = strf_offline2(mfilename,spkfile,channel,unit,a,options);
snr2(nn)=snr;
title(sprintf('%s trials %d-%d snr=%.2f',basename(mfilename),options.trialrange(1),options.trialrange(end),snr),'fontsize',8,'fontweight','normal');
xlim = get(gca,'XLim');
hold on
if TargetFrequencies(1)==BF && tar_reward==BF
    plot(xlim,tarfreqasoctaves(1)*[1 1],'b-');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'r--');
elseif TargetFrequencies(2)==BF && tar_reward==BF
    plot(xlim,tarfreqasoctaves(1)*[1 1],'r--');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'b-');
elseif TargetFrequencies(1)==BF && tar_reward==offBF
     plot(xlim,tarfreqasoctaves(1)*[1 1],'b--');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'r-');
elseif TargetFrequencies(2)==BF && tar_reward==offBF
     plot(xlim,tarfreqasoctaves(1)*[1 1],'r-');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'b--');
end
hold off

if length(trials)>66
a = subplot(3,3,6);
options = struct('usefirstcycle', USEFIRSTCYCLE, 'trialrange', trials(len2+1):trials(len3));
[strfest,snr,StimParam] = strf_offline2(mfilename,spkfile,channel,unit,a,options);
snr3(nn)=snr;
title(sprintf('%s trials %d-%d snr=%.2f',basename(mfilename),options.trialrange(1),options.trialrange(end),snr),'fontsize',8,'fontweight','normal');
xlim = get(gca,'XLim');
hold on
if TargetFrequencies(1)==BF && tar_reward==BF
    plot(xlim,tarfreqasoctaves(1)*[1 1],'b-');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'r--');
elseif TargetFrequencies(2)==BF && tar_reward==BF
    plot(xlim,tarfreqasoctaves(1)*[1 1],'r--');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'b-');
elseif TargetFrequencies(1)==BF && tar_reward==offBF
     plot(xlim,tarfreqasoctaves(1)*[1 1],'b--');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'r-');
elseif TargetFrequencies(2)==BF && tar_reward==offBF
     plot(xlim,tarfreqasoctaves(1)*[1 1],'r-');
    plot(xlim,tarfreqasoctaves(2)*[1 1],'b--');
end
hold off
else
    snr3(nn)=NaN;
end

if prod(size(mfile_post_torc))~=0
% a = subplot(3,3,8);
% [strfest,snr,StimParam] = strf_offline2(mfilename_post,spkfile_post,channel,unit,a);
% title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_post),snr),'fontsize',8,'fontweight','normal');
% snr_post(nn)=snr;

a = subplot(3,3,7);
[strfest,snr,StimParam] = strf_offline2(mfilename_post_torc,spkfile_post_torc,channel,unit,a);
title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_post_torc),snr),'fontsize',8,'fontweight','normal');
snr_post_torc(nn)=snr;
end

h = gcf; % Current figure handle
set(h,'Resize','off');
%set(h,'PaperPositionMode','manual');
%set(h,'PaperPosition',[0 0 9 6]);
% set(h,'PaperUnits','centimeters');
% set(h,'PaperSize',[50 50]);
% %set(h,'Position',[0 0 9 6]);
% if reward(nn)==1 %onBF reward
%     saveas(gcf,[cellid '_strf'],'epsc')
%     saveas(gcf,[cellid '_strf'],'png')
%     savefig([cellid '_strf.fig'])
% else %offBF reward
%     saveas(gcf,[cellid '_rev_strf'],'epsc')
%     saveas(gcf,[cellid '_rev_strf'],'png')
%     savefig([cellid '_rev_strf.fig'])
% end
end

% save SNR data for TORCs:
save(['CCtraining_strf_snr_',data_to_use],'snr_pre_torc','snr1','snr2','snr3','snr_post_torc','reward')


