% trial raster for units recorded using array

    options.spikefile='/auto/data/daq/Shaggyink/SIK013/sorted/SIK013c08_a_PTD.spk.mat';
    options.rasterfs=500;
    options.channel=29;  % electrode
    options.unit=1;
    options.usesorted=1;
    options.psthfs=15;
    options.datause='Per trial';
    options.includeincorrect=1;
    options.psth=0;
    
    FILEDATA.parmfile='/auto/data/daq/Shaggyink/SIK013/SIK013c08_a_PTD.m';
    channel=options.channel;
    ii=options.unit;
    UNITCOUNT=1;
    
    [r,tags]=raster_load(FILEDATA.parmfile,channel,ii,options);
    if(~isempty(r))
        hs=subplot(2,UNITCOUNT,1);
        raster_plot(FILEDATA.parmfile,r,tags,hs,options);
        hs=subplot(2,UNITCOUNT,2);
        plot(nanmean(r,1).*500);
        aa=axis;
        axis([-5 aa(2:4)]);
        xlabel('trial');
        ylabel('mean spikes/sec');
    end