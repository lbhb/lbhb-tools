% README FOR GENERATING CC EPhys Figures


%
% TORC analysis
%

% Load TORC data, compute STRFs, save SNRs
CCtraining_TORC_overtime
% inside: data_to_use specifies the database:
%data_to_use = 'database_new'; % more stringent requirement for stable trial_raster
%data_to_use = 'NMKdatabase_new';
%data_to_use = 'database'  % less stringent requirement for stable trial_raster
%data_to_use = 'NMKdatabase';

% extract either onset or sustained response from PSTH for cells with
% SNR>0.12 (depends on output of CCtraining_TORC_overtime)
CCtraining_TORCpsth_overtime
% inside: use onset=0 or 1 to specify which epoch of the response
% data_to_use specified as above

% plot summary:
CCtraining_TORCpsth_analysis
%inside: use onset=0 or 1 to specify which epoch of the response
% data_to_use is a non-NMK data_to_use from above
% data_to_use2 is a NMK data_to_use from above



%
% Target (tone) analysis
%
% CCtraining_database - only two databases rather than the four above, no
% distinction between "NMK" (3-sec passive torc) and other cells
% CCtraining_database_new hard-coded

CCtraining_psth_overtime
% inside: data_to_use specifies the database "new" or not new
%   currently hard-coded as CCtraining_database_new
% onset=1; %0 is sustained (resp), 1 is onset (onset), 2 is -0.5 to 1.5s (all)

% plot summary
CCtraining_psth_analysis
% onset=0;   %0 is sustained (resp), 1 is onset (onset)


%
% remove lick analysis
%
tarPSTH_on_offBFreward_training
