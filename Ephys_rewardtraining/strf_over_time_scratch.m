baphy_set_path



mfilename_pre_tor='/auto/data/daq/Pennybun/PNB019/PNB019c02_p_PTD.m';
spkfile_pre_tor='/auto/data/daq/Pennybun/PNB019/sorted/PNB019c02_p_PTD.spk.mat';

mfilename_pre='/auto/data/daq/Pennybun/PNB019/PNB019c03_p_PTD.m';
spkfile_pre='/auto/data/daq/Pennybun/PNB019/sorted/PNB019c03_p_PTD.spk.mat';

mfilename='/auto/data/daq/Pennybun/PNB019/PNB019c04_a_PTD.m';
spkfile='/auto/data/daq/Pennybun/PNB019/sorted/PNB019c04_a_PTD.spk.mat';

mfilename_post='/auto/data/daq/Pennybun/PNB019/PNB019c05_p_PTD.m';
spkfile_post='/auto/data/daq/Pennybun/PNB019/sorted/PNB019c05_p_PTD.spk.mat';

mfilename_post_tor='/auto/data/daq/Pennybun/PNB019/PNB019c06_p_TOR.m';
spkfile_post_tor='/auto/data/daq/Pennybun/PNB019/sorted/PNB019c06_p_TOR.spk.mat';


channel=1;
unit=1;


USEFIRSTCYCLE=0;

figure();

a = subplot(3,3,2);
[strfest,snr,StimParam] = strf_offline2(mfilename_pre,spkfile_pre,channel,unit,a);
title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_pre),snr),'fontsize',8,'fontweight','normal');

LoadMFile(mfilename);
TargetFrequencies = exptparams.TrialObject.TargetHandle.Frequencies;
tarfreqasoctaves = log2(TargetFrequencies)-log2(StimParam.lfreq);

a = subplot(3,3,4);
options = struct('usefirstcycle', USEFIRSTCYCLE, 'trialrange', 1:23);
[strfest,snr,StimParam] = strf_offline2(mfilename,spkfile,channel,unit,a,options);
title(sprintf('%s trials %d-%d snr=%.2f',basename(mfilename),options.trialrange(1),options.trialrange(end),snr),'fontsize',8,'fontweight','normal');
xlim = get(gca,'XLim');
hold on
plot(xlim,tarfreqasoctaves(1)*[1 1],'r--');
plot(xlim,tarfreqasoctaves(2)*[1 1],'b--');
hold off

a = subplot(3,3,5);
options = struct('usefirstcycle', USEFIRSTCYCLE, 'trialrange', 24:46);
[strfest,snr,StimParam] = strf_offline2(mfilename,spkfile,channel,unit,a,options);
title(sprintf('%s trials %d-%d snr=%.2f',basename(mfilename),options.trialrange(1),options.trialrange(end),snr),'fontsize',8,'fontweight','normal');
xlim = get(gca,'XLim');
hold on
plot(xlim,tarfreqasoctaves(1)*[1 1],'r--');
plot(xlim,tarfreqasoctaves(2)*[1 1],'b--');
hold off

a = subplot(3,3,6);
options = struct('usefirstcycle', USEFIRSTCYCLE, 'trialrange', 47:70);
[strfest,snr,StimParam] = strf_offline2(mfilename,spkfile,channel,unit,a,options);
title(sprintf('%s trials %d-%d snr=%.2f',basename(mfilename),options.trialrange(1),options.trialrange(end),snr),'fontsize',8,'fontweight','normal');
xlim = get(gca,'XLim');
hold on
plot(xlim,tarfreqasoctaves(1)*[1 1],'r--');
plot(xlim,tarfreqasoctaves(2)*[1 1],'b--');
hold off

a = subplot(3,3,7);
[strfest,snr,StimParam] = strf_offline2(mfilename_post,spkfile_post,channel,unit,a);
title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_post),snr),'fontsize',8,'fontweight','normal');

a = subplot(3,3,8);
[strfest,snr,StimParam] = strf_offline2(mfilename_post_tor,spkfile_post_tor,channel,unit,a);
title(sprintf('%s trials ALL snr=%.2f',basename(mfilename_post_tor),snr),'fontsize',8,'fontweight','normal');

