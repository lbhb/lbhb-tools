%cellid='oys027b-c1'; % has two behavior sessions. single unit, low spont,facilitation
%cellid='oys027b-c2'; % has two behavior sessions. multiunit.
%cellid='oys027c-c1'; % single unit, low spont. 
%cellid='oys027c-c2'; % multiunit.
%cellid='oys028a-c2';
%cellid='oys028a-c1'; % single unit. Interesting dynamics here too. Reliable responses.
%cellid='oys033a-a1'; % single unit. Interesting dynamics, but not very responsive.
%cellid='oys033a-b1'; % multiunit. Interesting response.
%cellid='oys033b-a1'; % multiunit. Resp to both target. (compare 12). high spont.
%cellid='oys033b-b1'; % single unit. Not super responsive cell (compare 12). low spont.
%cellid='oys033c-a1';
%cellid='oys033c-b1'; % single unit, very cool resp. (compare 12). resp to tar2 inhibitory.
%cellid='oys034b-a1'; % singel unit. High spont. Inhbitory resp?
%cellid='oys034c-b1'; % multiunit. nothing. Weird STRF and basically no resp.
%cellid='oys034c-a2';
%cellid='oys034d-a1';
%cellid='oys034d-a2';
%cellid='oys024c-a1';

cellid='chn013a-a1';

close all

% go to cell db to find active and passive data for this cell
if ~exist('active','var'),
   active=0;
end
if active,
    cellfiledata=dbgetscellfile('cellid',cellid,'runclass','RDT',...
                            'behavior','active');
    fidx=1;
else
    cellfiledata=dbgetscellfile('cellid',cellid,'runclass','RDT',...
                             'Trial_Mode','RandAndRep');
    if strcmpi(cellfiledata(1).behavior,'passive'),
        fidx=1;
    elseif strcmpi(cellfiledata(end).behavior,'passive'),
        fidx=length(cellfiledata);
    else
        fidx=1;
    end
end

parmfile=[cellfiledata(fidx).stimpath cellfiledata(fidx).stimfile];
spikefile=[cellfiledata(fidx).path cellfiledata(fidx).respfile];
options.rasterfs=100;
options.channel=cellfiledata(1).channum;
options.unit=cellfiledata(1).unit;
options.resp_shift=0.0;

% r=response (time X trialcount), only for correct trials
[r,params]=load_RDT_by_trial(parmfile,spikefile,options); 

rrefall=[];
rtar1solo=[];
rtar1=[];
rtar2solo=[];
rtar2=[];
TargetStartBin=params.TargetStartBin(params.CorrectTrials);
ThisTarget=params.ThisTarget(params.CorrectTrials);
BigSequenceMatrix=params.BigSequenceMatrix(:,:,params.CorrectTrials);
singleTrial=squeeze(BigSequenceMatrix(1,2,:)==-1);
TarRepCount=params.SamplesPerTrial-max(TargetStartBin)+1;
TarDur=params.PreStimSilence+TarRepCount.*params.SampleDur;
TarBins=round(params.rasterfs.*TarDur);

for tt=1:length(TargetStartBin),
    if TargetStartBin(tt)>0,
        tarstart=round((TargetStartBin(tt)-1).*params.SampleDur.*params.rasterfs+1);
        tar=nan(size(r,1),1);
        tar(1:TarBins)=r(tarstart-1+(1:TarBins),tt);
        if ThisTarget(tt)==params.TargetIdx(1) && ...
                BigSequenceMatrix(1,2,tt)==-1,
            rtar1solo=cat(2,rtar1solo,tar);
        elseif ThisTarget(tt)==params.TargetIdx(1),
            rtar1=cat(2,rtar1,tar);
        elseif ThisTarget(tt)==params.TargetIdx(2) && ...
                BigSequenceMatrix(1,2,tt)==-1,
            rtar2solo=cat(2,rtar2solo,tar);
        elseif ThisTarget(tt)==params.TargetIdx(2),
            rtar2=cat(2,rtar2,tar);
        else
            %disp('no target match??');
        end
        refend=tarstart-1+round(params.rasterfs.*params.PreStimSilence);
    else
        refend=size(r,1);
    end
    ref=nan(size(r,1),1);
    ref(1:refend)=r(1:refend,tt);
    rrefall=cat(2,rrefall,ref);
end

rref1solo=[];
rref1=[];
rref2solo=[];
rref2=[];
singleTrial=squeeze(BigSequenceMatrix(1,2,:)==-1);
for bb=2:params.SamplesPerTrial,
    br=params.SampleStarts(bb):params.SampleStops(bb);
    ff=find((TargetStartBin<0 | bb<TargetStartBin) &...
            singleTrial & ...
            squeeze(BigSequenceMatrix(bb,1,:)==params.TargetIdx(1)));
    rref1solo=cat(2,rref1solo,r(br,ff));
    ff=find((TargetStartBin<0 | bb<TargetStartBin) &...
            ~singleTrial & ...
            squeeze(BigSequenceMatrix(bb,1,:)==params.TargetIdx(1)));
    rref1=cat(2,rref1,r(br,ff));
    ff=find((TargetStartBin<0 | bb<TargetStartBin) &...
            singleTrial & ...
            squeeze(BigSequenceMatrix(bb,1,:)==params.TargetIdx(2)));
    rref2solo=cat(2,rref2solo,r(br,ff));
    ff=find((TargetStartBin<0 | bb<TargetStartBin) &...
            ~singleTrial & ...
            squeeze(BigSequenceMatrix(bb,1,:)==params.TargetIdx(2)));
    rref2=cat(2,rref2,r(br,ff));
end
r1=nanmean(rref1solo,2);
r2=nanmean(rref2solo,2);
ScaleFactor=zeros(TarRepCount,2,2);
NScaleFactor=zeros(TarRepCount,2,2);
for t=1:TarRepCount,
    bb=round(params.rasterfs.*params.PreStimSilence) + (t-1).*length(br) ...
       + (1:length(br));
    if t==1,
        r1s=nanmean([rtar1solo(bb,:) rref1solo],2);
        r2s=nanmean([rtar2solo(bb,:) rref2solo],2);
        %r1=nanmean(rref1,2);
        %r2=nanmean(rref2,2);
        r1=nanmean([rtar1(bb,:) rref1],2);
        r2=nanmean([rtar2(bb,:) rref2],2);
        %r1=nanmean([rtar1solo(bb,:)],2);
        %r2=nanmean([rtar2solo(bb,:)],2);
    end
    
    tr=nanmean(rtar1solo(bb,:),2);
    NScaleFactor(t,1,1)=r1s'*tr./(r1s'*r1s);
    tr=nanmean(rtar1(bb,:),2);
    NScaleFactor(t,1,2)=r1'*tr./(r1'*r1);
    tr=nanmean(rtar2solo(bb,:),2);
    NScaleFactor(t,2,1)=r2s'*tr./(r2s'*r2s);
    tr=nanmean(rtar2(bb,:),2);
    NScaleFactor(t,2,2)=r2'*tr./(r2'*r2);
    tr=nanmean(rtar1solo(bb,:),2);
    ScaleFactor(t,1,1)=r1s'*tr./(r1s'*r1s).*max(r1s);
    tr=nanmean(rtar1(bb,:),2);
    ScaleFactor(t,1,2)=r1'*tr./(r1'*r1).*max(r1);
    tr=nanmean(rtar2solo(bb,:),2);
    ScaleFactor(t,2,1)=r2s'*tr./(r2s'*r2s).*max(r2s);
    tr=nanmean(rtar2(bb,:),2);
    ScaleFactor(t,2,2)=r2'*tr./(r2'*r2).*max(r2);
end

zb=zeros(round(params.rasterfs.*params.PreStimSilence),1);
rref1solo=cat(1,zb,repmat(nanmean(rref1solo,2),[TarRepCount 1]));
rref1=cat(1,zb,repmat(nanmean(rref1,2),[TarRepCount 1]));
rref2solo=cat(1,zb,repmat(nanmean(rref2solo,2),[TarRepCount 1]));
rref2=cat(1,zb,repmat(nanmean(rref2,2),[TarRepCount 1]));

figure(1);
clf

subplot(3,2,1);
cla
boundbins=round(options.rasterfs.*0.05);
tt=((1:size(params.r_avg,1))-boundbins-0.5)./options.rasterfs;
for ii=1:params.SampleCount
    tr=[params.r_raster{ii,1} params.r_raster{ii,3}];
    mr=nanmean(tr,2);
    if ii==params.TargetIdx(1),
        plot(tt,mr,'r','LineWidth',2);
    elseif ii==params.TargetIdx(2),
        plot(tt,mr,'g','LineWidth',2);
    else
        plot(tt,mr,'k','LineWidth',0.5);
    end
    
    hold on
end

ttsf=params.SampleStarts(1:TarRepCount)./params.rasterfs-params.PreStimSilence;
if ~isempty(rtar1solo),
    subplot(3,2,3);
    plot(ttsf,NScaleFactor(:,1,1),'rs-');
    hold on
    plot(ttsf,NScaleFactor(:,1,2),'ro-');
    
    hold off
    aa=axis;
    legend('tarsolo','tardual');
end
title(sprintf('target 1 (sample #%d)',params.TargetIdx(1)));
if ~isempty(rtar2solo),
    subplot(3,2,5);
    plot(ttsf,NScaleFactor(:,2,1),'gs-');
    hold on
    plot(ttsf,NScaleFactor(:,2,2),'go-');
    
    hold off
    aa=axis;
    legend('tarsolo','tardual');
end
title(sprintf('target 2 (sample #%d)',params.TargetIdx(2)));


subplot(3,2,2);
tt=(1:TarBins)./params.rasterfs-params.PreStimSilence;
ttsf=params.SampleStarts(1:TarRepCount)./params.rasterfs-params.PreStimSilence;
plot(tt,nanmean(rrefall(1:TarBins,:),2),'LineWidth',2,'Color',[0.6 0.6 1]);
title([cellid ' -- reference']);
aa1=axis;

subplot(3,2,4);
if ~isempty(rtar1solo),
    plot(tt,rref1solo(1:TarBins),'b-');
    hold on
    plot(tt,rref1(1:TarBins),'LineWidth',2,'Color',[0.6 0.6 1]);
    plot(tt,nanmean(rtar1solo(1:TarBins,:),2),'Color',[0.5 0 0]);
    plot(tt,nanmean(rtar1(1:TarBins,:),2),'LineWidth',2,'Color',[0.5 0 0]);
    plot(ttsf,ScaleFactor(:,1,1),'s','Color',[0.5 0 0]);
    plot(ttsf,ScaleFactor(:,1,2),'o','Color',[0.5 0 0],'LineWidth',2);
    
    hold off
    aa=axis;
    legend('ref1','ref2','tar1','tar2');
else
    plot(tt,[nanmean(rtar1(1:TarBins,:),2)]);
end
title(sprintf('target #%d',params.TargetIdx(1)));
aa2=axis;

subplot(3,2,6);
if ~isempty(rtar2solo),
    plot(tt,rref2solo(1:TarBins),'b-');
    hold on
    plot(tt,rref2(1:TarBins),'LineWidth',2,'Color',[0.6 0.6 1]);
    plot(tt,nanmean(rtar2solo(1:TarBins,:),2),'Color',[0.5 0 0]);
    plot(tt,nanmean(rtar2(1:TarBins,:),2),'LineWidth',2,'Color',[0.5 0 0]);
    plot(ttsf,ScaleFactor(:,2,1),'s','Color',[0.5 0 0]);
    plot(ttsf,ScaleFactor(:,2,2),'o','Color',[0.5 0 0],'LineWidth',2);
    hold off
    %plot(tt,[nanmean(rtar2solo(1:TarBins,:),2) nanmean(rtar2(1:TarBins,:),2)...
    %         rref2solo(1:TarBins) rref2(1:TarBins)]);
    legend('ref1','ref2','tar1','tar2');
else
    plot(tt,[nanmean(rtar2(1:TarBins,:),2)]);
end
title(sprintf('target #%d',params.TargetIdx(2)));
aa3=axis;
aamax=[aa1(1:2) 0 max([aa1(4) aa2(4) aa3(4)])];

return



ccmatrix=cell(length(params.TargetIdx),1);
msematrix=cell(length(params.TargetIdx),1);
for ii=1:length(params.TargetIdx),
    targetidx=params.TargetIdx(ii);
    r_avg=squeeze(params.r_avg(:,targetidx,:));
    
    stim2=intersect(params.r_second{targetidx,1},params.r_second{targetidx,2});
    ri2=find(ismember(params.r_second{targetidx,1},stim2));
    nr2=length(ri2);
    r2=params.r_raster{targetidx,1}(:,ri2);
    ti2=find(ismember(params.r_second{targetidx,2},stim2));
    nt2=length(ti2);
    t2=params.r_raster{targetidx,2}(:,ti2);
    
    %nr1=size(params.r_raster{targetidx,3},2);
    %r1=mean(params.r_raster{targetidx,3}(:,round(linspace(1,nr1,nr2))),2);
    %nt1=size(params.r_raster{targetidx,4},2);
    %t1=mean(params.r_raster{targetidx,4}(:,round(linspace(1,nt1,nr2))),2);
    r1=params.r_raster{targetidx,3};
    % t1 includes t1 and r2
    t1=[params.r_raster{targetidx,3} params.r_raster{targetidx,4}];
    
    mr1=mean(r1,2);
    vr1=var(r1,0,2);
    fr1=vr1./mr1./options.rasterfs;
    fr1(mr1==0)=1;
    mr2=mean(r2,2);
    vr2=var(r2,0,2);
    fr2=vr2./mr2./options.rasterfs;
    fr2(mr2==0)=1;
    mt1=mean(t1,2);
    vt1=var(t1,0,2);
    ft1=vt1./mt1./options.rasterfs;
    ft1(mt1==0)=1;
    mt2=mean(t2,2);
    vt2=var(t2,0,2);
    ft2=vt2./mt2./options.rasterfs;
    ft2(mt2==0)=1;
    
    sb=round(options.rasterfs*0.03+5):...
       round(options.rasterfs.*(0.02+params.SampleDur)+5);
    mdt2r2=zeros(size(t2,2)*size(r2,2),1);
    mdt2r1=zeros(size(t2,2)*size(r1,2),1);
    mdt2t1=zeros(size(t2,2)*size(t1,2),1);
    mdr2r1=zeros(size(r2,2)*size(r1,2),1);
    mdr2t1=zeros(size(r2,2)*size(t1,2),1);
    cc=0;
    cc1=0;
    cc2=0;
    for kk=1:size(t2,2),
        for jj=1:size(r2,2),
            cc=cc+1;mdt2r2(cc)=std(t2(sb,kk)-r2(sb,jj));
        end
        for jj=1:size(r1,2),
            cc1=cc1+1;mdt2r1(cc1)=std(t2(sb,kk)-r1(sb,jj));
        end
        for jj=1:size(t1,2),
            cc2=cc2+1;mdt2t1(cc2)=std(t2(sb,kk)-t1(sb,jj));
        end
    end
    cc=0;
    cc1=0;
    for kk=1:size(r2,2),
        for jj=1:size(r1,2),
            cc=cc+1;mdr2r1(cc)=std(r2(sb,kk)-r1(sb,jj));
        end
        for jj=1:size(t1,2),
            cc1=cc1+1;mdr2t1(cc1)=std(r2(sb,kk)-t1(sb,jj));
        end
    end
    
    fprintf('(active=%d taridx=%d) ',active,targetidx);
    fprintf('MD: t2r2=%.2f t2r1=%.2f t2t1=%.2f r2r1=%.2f r2t1=%.2f\n',...
            mean(mdt2r2),mean(mdt2r1),mean(mdt2t1),...
            mean(mdr2r1),mean(mdr2t1));
    
    targetidx=params.TargetIdx(ii);
    
    % exact pair analysis
    ref2pairs=params.r_second{targetidx,1};
    tar2pairs=params.r_second{targetidx,2};
    
    olappairs=intersect(unique(ref2pairs),unique(tar2pairs));
    rmatch=zeros(size(params.r_raster{1,1},1),4,length(olappairs));
    for jj=1:length(olappairs),
        ff=find(params.r_second{targetidx,1}==olappairs(jj));
        rmatch(:,1,jj)=nanmean(params.r_raster{targetidx,1}(:,ff),2);
        ff=find(params.r_second{targetidx,2}==olappairs(jj));
        rmatch(:,2,jj)=nanmean(params.r_raster{targetidx,2}(:,ff),2);
    end
    if ~isempty(params.r_raster{targetidx,3}),
        rmatch(:,3,1)=nanmean(params.r_raster{targetidx,3},2);
        rmatch(:,4,1)=nanmean(params.r_raster{targetidx,4},2);
    end
    
    r_raster_match={};
    ff=find(ismember(params.r_second{targetidx,1},olappairs));
    ff=1:length(params.r_second{targetidx,1});
    r_raster_match{1}=params.r_raster{targetidx,1}(:,ff);
    ff=find(ismember(params.r_second{targetidx,2},olappairs));
    ff=1:length(params.r_second{targetidx,2});
    r_raster_match{2}=params.r_raster{targetidx,2}(:,ff);
    
    ccmatrix{ii}=zeros(5,5);
    msematrix{ii}=zeros(5,5);
    N=300;
    for jj=1:5,
        for kk=jj:5,
            cc=zeros(N,1);
            mm=zeros(N,1);
            if jj==3,jjalt=5;else jjalt=jj-1; end
            if kk==3,kkalt=5;else kkalt=kk-1; end
            v1=zeros(size(sb(:)));
            v2=zeros(size(sb(:)));
            for nn=1:N
                if jj<3 && kk<3,
                    t1=ceil(rand*size(r_raster_match{jj},2));
                    t2=ceil(rand*(size(r_raster_match{kk},2)-1));
                    if t2<=t1;t2=t2+1; end
                    v1=r_raster_match{jj}(sb,t1);
                    v2=r_raster_match{kk}(sb,t2);
                elseif jj<3,
                    t1=ceil(rand*size(r_raster_match{jj},2));
                    t2=ceil(rand.*size(params.r_raster{targetidx,kkalt},2));
                    v1=r_raster_match{jj}(sb,t1);
                    if ~isempty(params.r_raster{targetidx,kkalt})
                        v2=params.r_raster{targetidx,kkalt}(sb,t2);
                    end
                else
                    t1=ceil(rand*size(params.r_raster{targetidx,jjalt},2));
                    t2=ceil(rand*(size(params.r_raster{targetidx,kkalt},2)-1));
                    if t2>=t1, t2=t2+1; end
                    if ~isempty(params.r_raster{targetidx,jjalt})
                        v1=params.r_raster{targetidx,jjalt}(sb,t1);
                    end
                    if t2>0 && ~isempty(params.r_raster{targetidx,kkalt})
                        v2=params.r_raster{targetidx,kkalt}(sb,t2);
                    end
                end
                cc(nn)=xcov(v1,v2,0,'coeff');
                mm(nn)=std(v1-v2)./100;
            end
            ccmatrix{ii}(jj,kk)=nanmean(cc);
            msematrix{ii}(jj,kk)=nanmean(mm);
       end
    end
    
    % sub in alternative calculations for mr2 and tr2
    mr2=mean(rmatch(:,1,:),3);
    tr2=mean(rmatch(:,2,:),3);
    
    figure;
    subplot(3,2,1);
    boundbins=round(options.rasterfs.*0.05);
    tt=((1:size(params.r_avg,1))-boundbins-0.5)./options.rasterfs;
    
    plot(tt,mr2,'LineWidth',2,'Color',[0.6 0.6 1]);
    hold on
    plot(tt,mt2,'LineWidth',2,'Color',[0.5 0 0]);
    plot(tt,mr1,'b-');
    plot(tt,mt1,'Color',[0.5 0 0]);
    aa=axis;
    plot([0 0],aa(3:4),'g--');
    plot([0 0]+params.SampleDur,aa(3:4),'g--');
    mm=nanmean(params.r_avg(:));
    plot(tt([1 end]),[mm mm],'g--');
    hold off
    axis tight
    legend('ref2','tar2','ref1','tar1');
    title(sprintf('cell %s %s',...
                  cellid,basename(parmfile)),'Interpreter','none');
    
    subplot(3,2,2);
    raster=cat(2,params.r_raster{targetidx,1:4});
    raster=cat(2,params.r_raster{targetidx,1:3});
    rraster=raster'-nanmin(raster(:));
    maxrast=nanmin(rraster(rraster>0));
    rraster(rraster>maxrast)=maxrast;
    rraster=rraster./nanmax(rraster(:));
    rraster=1-repmat(rraster,[1 1 3]);
    imagesc(tt,1:size(raster,2),rraster);
    hold on
    aa=axis;
    cc=0;
    for jj=1:2,
        cc=cc+size(params.r_raster{targetidx,jj},2);
        plot(aa(1:2),[cc+0.5 cc+0.5],'k--');
    end
    plot([0 0],aa(3:4),'k--');
    plot([0 0]+params.SampleDur,aa(3:4),'k--');
    hold off
    %colormap(1-gray);
    title(sprintf('raster - targetid: %d',targetidx),'Interpreter','none');
    
    
    if ~isempty(strf) && length(strf)>1,
        subplot(3,2,3);
        smstrf=imresize(strf,2,'bilinear');
        smstrf=imresize(smstrf,2,'bilinear');
        imagesc((1:size(smstrf,2)-1).*2.5,1:size(smstrf,1),smstrf,...
                [-1 1].*max(abs(smstrf(:))));
        axis xy
        title(strf);
        
        aa=axis;
        yy=get(gca,'YTick');
        set(gca,'YTickLabel',[]);
        ff=round(2.^linspace(log2(200),log2(20000),size(smstrf,1)));
        for kk=1:length(yy),
            text(aa(1),yy(kk),num2str(ff(yy(kk))),'HorizontalAlignment','right');
        end
    end
    if ~isempty(s),
        subplot(3,2,4);
        
        ff=min(find(ThisTarget==targetidx &...
                    squeeze(BigSequenceMatrix(1,2,:))==-1));
        startbin=round(params.SampleStartTimes(TargetStartBin(ff)).*100);
        stopbin=round((params.SampleStartTimes(TargetStartBin(ff))+params.SampleDur).*100);
        imagesc((1:(stopbin-startbin+1)).*10,1:size(s,1),...
                log2(s(:,startbin:stopbin,ff,1)));
        axis xy
        title(sprintf('target sample %d',targetidx));
        
        aa=axis;
        yy=get(gca,'YTick');
        set(gca,'YTickLabel',[]);
        ff=round(2.^linspace(log2(200),log2(20000),size(s,1)));
        for kk=1:length(yy),
            text(aa(1),yy(kk),num2str(ff(yy(kk))),'HorizontalAlignment','right');
        end
    end
    
    sl={'r2','t2','rt','r1','t1'};
    subplot(3,2,5);
    imagesc(msematrix{ii},[0 1].*max(abs(msematrix{ii}(:))));
    axis square
    axis off
    for kk=1:5,
        text(kk,5.5,sl{kk},'VerticalAlign','top','HorizontalAlignment','center');
        text(0.5,kk,sl{kk},'HorizontalAlignment','right');
    end
    title('MSE');
    colorbar
    
    subplot(3,2,6);
    imagesc(ccmatrix{ii},[0 1].*max(abs(ccmatrix{ii}(:))));
    axis square
    axis off
    for kk=1:5,
        text(kk,5.5,sl{kk},'VerticalAlign','top','HorizontalAlignment','center');
        text(0.5,kk,sl{kk},'HorizontalAlignment','right');
    end
    title('cross-corr');
    colorbar
    
    
    if 0
        subplot(3,2,3);
        plot(tt,fr2,'b-');
        hold on
        plot(tt,ft2,'r-');
        plot(tt,fr1,'b-','LineWidth',2);
        plot(tt,ft1,'r-','LineWidth',2);
        aa=axis;
        plot([0 0],aa(3:4),'g--');
        plot([0 0]+params.SampleDur,aa(3:4),'g--');
        mm=nanmean(params.r_avg(:));
        plot(tt([1 end]),[0 0],'g--');
        hold off
        axis tight
        title(sprintf('FF: r1=%.3f r2=%.3f t1=%.3f t2=%.3f',...
                      nanmean(fr1(sb)),nanmean(fr2(sb)),...
                  nanmean(ft1(sb)),nanmean(ft2(sb))));
    end
    
    mfr1(ii,2-active)=nanmean(fr1(sb));
    mfr2(ii,2-active)=nanmean(fr2(sb));
    mft1(ii,2-active)=nanmean(ft1(sb));
    mft2(ii,2-active)=nanmean(ft2(sb));
end

ccref=zeros(params.SampleCount,6);
N=50;
for sidx=1:params.SampleCount,
   txc=zeros(N,6);
   ff1=find(ismember(params.r_second{sidx,1},params.TargetIdx));
   ff5=find(ismember(params.r_second{sidx,5},params.TargetIdx));
   if ~ismember(sidx,params.TargetIdx) &&...
           length(ff1)>=2 && length(ff5)>=2,
       for nn=1:N,
           t1=ceil(rand*length(ff1));
           t2=ceil(rand*(length(ff1)-1));
           t3=ceil(rand*length(ff5));
           t4=ceil(rand*(length(ff5)-1));
           t5=ceil(rand*(size(params.r_raster{sidx,3},2)));
           t6=ceil(rand*(size(params.r_raster{sidx,3},2)-1));
           if t2>=t1, t2=t2+1; end
           if t4>=t3, t4=t4+1; end
           if t6>=t5, t6=t6+1; end
           v1=params.r_raster{sidx,1}(sb,ff1(t1));
           v2=params.r_raster{sidx,1}(sb,ff1(t2));
           v3=params.r_raster{sidx,5}(sb,ff5(t3));
           v4=params.r_raster{sidx,5}(sb,ff5(t4));
           v5=params.r_raster{sidx,3}(sb,t5);
           v6=params.r_raster{sidx,3}(sb,t6);
           if std(v1) && std(v2), 
               txc(nn,1)=xcov(v1,v2,0,'coeff');
           end
           if std(v1) && std(v4), 
               txc(nn,2)=xcov(v1,v4,0,'coeff');
           end
           if std(v3) && std(v4), 
               txc(nn,3)=xcov(v3,v4,0,'coeff');
           end
           if std(v1) && std(v5),
               txc(nn,4)=xcov(v1,v5,0,'coeff'); %ref2 vs ref1
           end
           if std(v3) && std(v5), 
               txc(nn,5)=xcov(v3,v5,0,'coeff'); %ref2tar vs ref1
           end
           if std(v5) && std(v6),
               txc(nn,6)=xcov(v5,v6,0,'coeff'); %ref1 vs ref1
           end
       end
       ccref(sidx,:)=mean(txc);
   end
end

return


for sidx=1:params.SampleCount,
    txc=zeros(N,6);
    for nn=1:N,
        
        t1=ceil(rand*size(params.r_raster{sidx,1},2));
        t2=ceil(rand*(size(params.r_raster{sidx,1},2)-1));
        t3=ceil(rand*size(params.r_raster{sidx,5},2));
        t4=ceil(rand*(size(params.r_raster{sidx,5},2)-1));
        t5=ceil(rand*(size(params.r_raster{sidx,3},2)));
        t6=ceil(rand*(size(params.r_raster{sidx,3},2)-1));
        if t1 && t2
           v1=params.r_raster{sidx,1}(sb,t1);
           v2=params.r_raster{sidx,1}(sb,t2);
       end
       if t3 && t4,
           v3=params.r_raster{sidx,5}(sb,t3);
           v4=params.r_raster{sidx,5}(sb,t4);
       end
       if t5 && t6,
           v5=params.r_raster{sidx,3}(sb,t5);
           v6=params.r_raster{sidx,3}(sb,t6);
       end
       
       if  t1 && t2 && std(v1) && std(v2), 
          txc(nn,1)=xcov(v1,v2,0,'coeff');
       end
       if t1 && t4 && std(v1) && std(v4), 
          txc(nn,2)=xcov(v1,v4,0,'coeff');
       end
       if t3 && t4 && std(v3) && std(v4), 
          txc(nn,3)=xcov(v3,v4,0,'coeff');
       end
       if t1 && t3 && t5 && t6,
           if std(v1) && std(v5),
               txc(nn,4)=xcov(v1,v5,0,'coeff'); %ref2 vs ref1
           end
           if std(v3) && std(v5), 
               txc(nn,5)=xcov(v3,v5,0,'coeff'); %ref2tar vs ref1
           end
           if std(v5) && std(v6),
               txc(nn,6)=xcov(v5,v6,0,'coeff'); %ref1 vs ref1
           end
       end
   end
   ccref(sidx,:)=nanmean(txc);
end

