        
disp('');
disp('tsp_gainsum_do_plot');

for ii=1:length(r),
   P1inst=abs([r(ii).P1P_z])>3;
   P2inst=abs([r(ii).P2P_z])>3;
   Pinst=[P1inst P2inst];
   AAsig=abs(r(ii).AA_z)>2;
   APsig=abs(r(ii).AA_z)>2;
   fprintf('%12s, %3.0f, %3.0f, "%d%d%d%d", %6.2f, %6.2f, "%d%d", %6.2f, %6.2f, "%d%d", %.0f, %.0f, %d\n',...
      r(ii).cellid,r(ii).iso,r(ii).tar_resp,Pinst,...
      r(ii).AA_r0,r(ii).AA_beta(2)-1,AAsig,...
      r(ii).AP_beta-[0 1],APsig,...
      r(ii).DImean,r(ii).DImin,r(ii).inband);
end

aa=cat(1,r.AA_beta);
ap=cat(1,r.AP_beta);
a1p=cat(1,r.A1P_beta);
a2p=cat(1,r.A2P_beta);
pp1=cat(1,r.P1P_beta);
pp2=cat(1,r.P2P_beta);

crit='AA_z';
cc=cat(1,r.(crit));
cc5=abs(cc(:,1));
cc=abs(cc(:,2));
cc2=[cat(1,r.P1P_z) cat(1,r.P2P_z)];
cc3=cat(1,r.inband);
cc4=cat(1,r.AP_z);
cc4=abs(cc4(:,2));
cc6=cat(1,r.DImean);

%g1=find(cc>2 & cc2(:,2)<3 & cc2(:,4)<3);

% dc or gain change and A-P changes
g1=find((cc5>2 | cc>2) & cc4>2);

% dc or gain change sig w/ Bonferoni
g1=find((cc5>4 | cc>4));

%g1=find(cc4>3);
g2=setdiff(1:length(cc),g1);

MED=0;
BC=16;

catidx=ones(length(a1p),1);
catidx(g2)=2;
a1z=cat(1,r.A1P_z);
a2z=cat(1,r.A2P_z);
catidx(a1z(:,2)==0 | a2z(:,2)==0)=0;

figure;

subplot(4,3,1);
histcomp(aa(g1,1),aa(g2,1),'AA sg','AA ns','spont',[-15 15],MED,BC);

subplot(4,3,2);
histcomp(log2(aa(g1,2)),log2(aa(g2,2)),'AA sg','AA ns', ...
         'log2 gain',[-1.2 1.2],MED,BC);
%histcomp((aa(g1,2)-1),(aa(g2,2)-1),'AA sg','AA ns', ...
%         'log2 gain',[-1.5 1.5],MED,BC);

subplot(4,3,3);
plotcomp(aa(:,1),log2(aa(:,2)),'AA DC','log2(g)',[-5 15 -1.5 1.5],catidx);

subplot(4,3,4);
histcomp(ap(g1,1),ap(g2,1),'AP sg','AP ns','spont',[-15 15],MED,BC);

subplot(4,3,5);
histcomp(log2(ap(g1,2)),log2(ap(g2,2)),'AP sg','AP ns', ...
         'log2 gain',[-1.2 1.2],MED,BC);

subplot(4,3,6);
plotcomp(ap(:,1),log2(ap(:,2)),'AP DC','log2(g)',[-5 15 -1.5 1.5],catidx);

if 0,
    subplot(4,3,7);
    histcomp(pp1(g1,1),pp1(g2,1),'PP1 sg','PP1 ns','spont',[-15 15],MED,BC);

    subplot(4,3,8);
    histcomp(log2(pp1(g1,2)),log2(pp1(g2,2)),'pp1 sig','pp1 ns', ...
             'log2 gain',[-1.5 1.5],MED,BC);

    subplot(4,3,10);
    histcomp(pp2(g1,1),pp2(g2,1),'PP2 sg','PP2 ns','spont',[-15 15],MED,BC);

    subplot(4,3,11);
    histcomp(log2(pp2(g1,2)),log2(pp2(g2,2)),'pp2 sig','pp2 ns', ...
             'log2 gain',[-1.5 1.5],MED,BC);

    
else
   
   subplot(4,3,7);
   plotcomp(a1p(:,1),a2p(:,1),'A1-P','A2-P',[-10 10 -10 10],catidx)
   
   subplot(4,3,8);
   plotcomp(log2(a1p(:,2)),log2(a2p(:,2)),'A1-P','A2-P',...
            [-1 1 -1 1],catidx);
   
   tp1=pp1;
   tp1(tp1(:,2)==1,:)=pp2(tp1(:,2)==1,:);
   tp2=pp2;
   tp2(tp2(:,2)==1,:)=pp1(tp2(:,2)==1,:);
   %tp1=(tp1+tp2)./2;
   
   u1=g1;
   %u1=1:length(ap);
   %u1=find(a1p(:,2)>0 & a2p(:,2)>0  & aa(:,2)>0 & ap(:,2)>0);
   subplot(4,3,10);
   [dcm,dce]=jackmeanerr([ap(u1,1) aa(u1,1)],500,0);
   if ~isnan(dce),
      errorbar(dcm,dce,'k.');
      hold on
      bar(dcm);
      hold off
      ylabel('mean spont diff');
      axis([0 5 -1 3]);
   end
   
   subplot(4,3,11);
   [gm,ge]=jackmeanerr(log2([ap(u1,2) aa(u1,2) ]),500,0);
   if ~isnan(ge),
      errorbar(gm,ge,'k.');
      hold on
      bar(gm);
      hold off
      axis([0 5 -0.1 0.3]);
      ylabel('mean log2(gain) diff');
      xlabel('A-P  --  A2-A1');
   end
end

tar_resp=cat(3,r.tar_resp);
tar_sign=sign(tar_resp);
r0=nanmean(monster_ref(1:12,3,:));
r1=nanstd(monster_ref(1:end-12,3,:));
r0t=repmat(r0,[size(monster_tar,1) 3 1]);
r1t=repmat(r1.*tar_sign,[size(monster_tar,1) 3 1]);
r0=repmat(r0,[size(monster_ref,1) 3 1]);
r1=repmat(r1,[size(monster_ref,1) 3 1]);
rr=(monster_ref-r0)./r1;
tt=(monster_tar-r0t)./r1t;
for ii=1:size(rr(:,:),2),
    ff=find(isnan(rr(:,ii)));
    if ~isempty(ff),
        rr(ff,ii)=nanmean(rr(1:12,ii));
    end
    ff=find(isnan(tt(:,ii)));
    if ~isempty(ff),
        tt(ff,ii)=nanmean(tt(1:12,ii));
    end
end
p1=repmat(sign(sum(sum(rr(:,:,g1)))),[size(rr,1) size(rr,2)]);
%for ii=1:size(rr,3),
%    rr(:,:,ii)=rconv2(rr(:,:,ii),[0;0;1;1;1]./3);
%end

attcolors=[1 0 0; 0 0 1; 0 0 0];
attshade=[1 0.8 0.8; 0.8 0.8 1; 0.8 0.8 0.8];

subplot(4,3,9);
cla
rasterfs=25;
prestimsilence=0.5;
durtoplot=1.5;
plotbins=1:round((prestimsilence+durtoplot).*rasterfs);
taxis=(plotbins)'./rasterfs-prestimsilence;
p1=find(catidx==1);
mnorm=gsmooth(squeeze(rr(:,3,p1)),[0.5 0.001]);
for sidx=1:size(rr,2),
   ttr=squeeze(rr(:,sidx,p1));
   ttr=gsmooth(ttr,[0.5 0.0001]);
   
   m=nanmean(ttr(plotbins,:),2);
   se=nanstd(ttr(plotbins,:)-mnorm(plotbins,:),0,2)./sqrt(length(p1));
   errorshade(taxis,m,se,attcolors(sidx,:),attshade(sidx,:));
   hold on
end
hl=zeros(3,1);
for sidx=1:size(rr,2),
   ttr=squeeze(rr(:,sidx,p1));
   ttr=gsmooth(ttr,[0.5 0.0001]);
   m=nanmean(ttr(plotbins,:),2);
   hl(sidx)=plot(taxis,m,'LineWidth',2,'Color',attcolors(sidx,:));
end
hold off

legend(hl,'A1','A2','P');
legend boxoff

subplot(4,3,12);
cla
PreStimBins=round(rasterfs.*prestimsilence);

% tg=tt(PreStimBins+(1:10),:,:);
% t0=repmat(nanmean(tt(1:PreStimBins,:,:)),[size(tg,1) 1 1]);
% %tg=tg-t0;
% tg=squeeze(nanmean(tg,1))';
% nantar=find(isnan(tg(:,3)));
% nnantar=find(~isnan(tg(:,3)));
% tg(nantar,3)=nanmean(rr(PreStimBins:end,3,nantar));
% tg=tg./repmat(tg(:,3),[1 3]);
% plotcomp(tg(nnantar,1),tg(nnantar,2));
% 
taxis=(1:size(tt,1))'./rasterfs-prestimsilence;
p1=find(squeeze(~isnan(tt(13,3,:))) & catidx==1);
mnorm=gsmooth(squeeze(tt(:,3,p1)),[0.5 0.001]);
for sidx=1:size(tt,2),
   ttt=squeeze(tt(:,sidx,p1));
   ttt=gsmooth(ttt,[0.75 0.0001]);
   
   m=nanmean(ttt(:,:),2);
   se=nanstd(ttt(:,:)-mnorm(:,:),0,2)./sqrt(length(p1));
   errorshade(taxis,m,se,attcolors(sidx,:),attshade(sidx,:));
   hold on
end
hl=zeros(3,1);
for sidx=1:size(tt,2),
   ttt=squeeze(tt(:,sidx,p1));
   ttt=gsmooth(ttt,[0.75 0.0001]);
   m=nanmean(ttt(:,:),2);
   hl(sidx)=plot(taxis,m,'LineWidth',2,'Color',attcolors(sidx,:));
end
hold off

colormap(gray);
set(gcf,'Name',sprintf('Batch %d',batchid));
