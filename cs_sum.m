
narf_set_path;
global NARF_PATH
addpath([NARF_PATH filesep 'scripts']);

dbopen;
if ~exist('batchid','var'),
    batchid=259;
end
modelnames={'env100_log2_fir_fit05',...
            'env100_logfree_fir_fit05',...
            'env100_logfree_fir_siglog100_fit05s06',...
            'env100_logfree_dep1_siglog100_fit05',...
            'env100_logfree_volterra_siglog100_fit05s06',...
            'env100_logfree_depct2_siglog100_fit05s06',...
            'env100_logfree_voltdepct2_siglog100_fit05'...
            'env100_logfree_depct2volt_siglog100_fit05s06'};
modelcount=length(modelnames);
for mm=1:modelcount,
    sql=['SELECT * FROM NarfResults WHERE batch=',num2str(batchid),...
         ' AND modelname="',modelnames{mm},'" order by cellid'];
    rundata=mysql(sql);
    if mm==1,
        cellcount=length(rundata);
        predxc=zeros(cellcount,modelcount);
        predxcraw=zeros(cellcount,modelcount);
        prederr=zeros(cellcount,modelcount);
        cellids=cell(cellcount,1);
    end
    for ii=1:length(rundata),
        cellids{ii}=rundata(ii).cellid;
        predxc(ii,mm)=rundata(ii).r_ceiling;
        predxcraw(ii,mm)=rundata(ii).r_test;
        prederr(ii,mm)=rundata(ii).r_floor;
    end
end
predsig=(predxcraw-prederr)>0;

mtidx=4;
fitset=export_fit_parameters(batchid, cellids, modelnames(mtidx));
auto_overlap=zeros(cellcount,1);
band_resp=zeros(cellcount,2);
cs_data=zeros(cellcount,3);
for ii=1:cellcount,
    res=spn_tuning_match(cellids{ii},batchid,0);
    if batchid==259,
        auto_overlap(ii)=100.*max(res);
    else
        auto_overlap(ii)=100.*res(1);
    end
    if length(res)==1,
        fprintf('%-13s: (%.3f-%.3f) %3.0f\n',cellids{ii},...
                predxc(ii,[1 end]),100.*res(1));
    elseif length(res)==2, 
        fprintf('%-13s: (%.3f-%.3f) %3.0f %3.0f\n',cellids{ii},...
                predxc(ii,[1 end]),100.*res);
    end
    if isfield(fitset(ii),'p04fir_filter_coefs'),
        firsum=sum(fitset(ii).p04fir_filter_coefs,2);
        firstd=std(fitset(ii).p04fir_filter_coefs(:));
    else
        firsum=sum(fitset(ii).p06fir_filter_coefs,2);
        firstd=std(fitset(ii).p06fir_filter_coefs(:));
    end
    band_resp(ii,:)=firsum./(firstd+(firstd==0));
    
    cs_data(ii,:)=[auto_overlap(ii) band_resp(ii,:)];
end

overlapthresh=20;
olapidx=find(cs_data(:,1)>=overlapthresh & predsig(:,end));
nolapidx=find(cs_data(:,1)<overlapthresh);

if 1 || batchid==240,
    inhsideidx=find(cs_data(:,1)>=overlapthresh & ...
                    abs(cs_data(:,2))>0.4 & abs(cs_data(:,3))>0.4);
    nosideidx=setdiff(olapidx,inhsideidx);
else
    inhsideidx=find(cs_data(:,1)>=overlapthresh & ...
                    sign(cs_data(:,2))~=sign(cs_data(:,3)));
    nosideidx=setdiff(olapidx,inhsideidx);
end
mpred=[mean(predxc(nolapidx,:));
       mean(predxc(olapidx,:));
       mean(predxc(nosideidx,:));
       mean(predxc(inhsideidx,:))];
pdiff=zeros(size(mpred,1),size(mpred,2)-1);
for ii=1:size(pdiff,2),
    pdiff(1,ii)=randpairtest(predxc(nolapidx,ii),...
                             predxc(nolapidx,ii+1),1000,1);
    pdiff(2,ii)=randpairtest(predxc(olapidx,ii),...
                             predxc(olapidx,ii+1),1000,1);
    pdiff(3,ii)=randpairtest(predxc(nosideidx,ii),...
                             predxc(nosideidx,ii+1),1000,1);
    pdiff(4,ii)=randpairtest(predxc(inhsideidx,ii),...
                             predxc(inhsideidx,ii+1),1000,1);
end
% remove non-olap data
mpred=mpred(2:end,:);
pdiff=pdiff(2:end,:);



figure;
modidx=[3 8];

subplot(2,2,1);
plot(predxc(nosideidx,modidx(1)),predxc(nosideidx,modidx(end)),'g.');
hold on
plot(predxc(inhsideidx,modidx(1)),predxc(inhsideidx,modidx(end)),'r.');
plot([0 1.1],[0 1.1],'k--');
hold off
axis equal tight

legend('one-band cells','two-band cells','Location','southeast');
xlabel(modelnames{modidx(1)}(16:end),'Interpreter','none');
ylabel(modelnames{modidx(end)}(16:end),'Interpreter','none');
title(sprintf('batch %d predsum',batchid));


subplot(2,2,2);
%predimprove=predxcraw(olapidx,:)-prederr(olapidx,:) > ...
%    repmat(predxcraw(olapidx,1),[1 modelcount]);
% don't require significant improvement
predimprove=predxcraw(olapidx,:) > ...
    repmat(predxcraw(olapidx,3),[1 modelcount]);
pie=[~predimprove(:,5) & ~predimprove(:,6) ...
     predimprove(:,5) & ~predimprove(:,6) ...
     ~predimprove(:,5) & predimprove(:,6) ...
     predimprove(:,5) & predimprove(:,6)];
bar(sum(pie)./sum(sum(pie)));
xlabel('none-vol-dep-both');
title(sprintf('fraction improving (n=%d)',length(olapidx)));


subplot(2,1,2);
bar(mpred-0.2);
axis([0.5 size(pdiff,1)+0.5 0 0.6]);
set(gca,'YTick',[0 0.1 0.2 0.3 0.4 0.5 0.6],...
        'YTickLabel',[0.2 0.3 0.4 0.5 0.6 0.7 0.8]);
for ii=1:size(pdiff,1),
    for jj=1:size(pdiff,2),
        text(ii-0.45+jj.*0.11,max(mpred(ii,jj+(0:1)))-0.2+0.02,...
             sprintf('p=%.3f',pdiff(ii,jj)),...
             'HorizontalAlign','Left','Rotation',90);
    end
end
lset=cell(modelcount,1);
for ii=1:modelcount
    lset{ii}=modelnames{ii}(16:end);
end
hl=legend(-1,lset{:});
set(hl,'Interpreter','none');

xlabel(sprintf(['No overlap (n=%d) - Center overlap (n=%d) -',...
                ' 1-band (n=%d) - 2-band (n=%d)'],...
               length(nolapidx),length(olapidx),length(nosideidx),...
               length(inhsideidx)));
ylabel('Mean prediction correlation');
title(sprintf('Batch %d prediction summary',batchid));
fullpage landscape
drawnow;

%% tunign analysis

mtidx=6;
depimprove=(predxcraw(:,6)-prederr(:,6)) > predxcraw(:,3);
depvimprove=(predxcraw(:,8)-prederr(:,8)) > predxcraw(:,3);
fitset=export_fit_parameters(batchid, cellids, modelnames(mtidx));
fitset2=export_fit_parameters(batchid, cellids, modelnames(end));
tau=zeros(cellcount,2);
tauch=zeros(cellcount,2);
tau2=zeros(cellcount,2);
ntau=zeros(cellcount,1);
strfstr=zeros(cellcount,10);
strfamp=zeros(cellcount,2);
ct=zeros(cellcount,1);
for ii=1:cellcount,
    ct(ii)=abs(fitset(ii).p04depression_filter_bank_crosstalk);
    tau(ii,:)=abs(fitset(ii).p04depression_filter_bank_tau.*10); % ms
    fir=fitset(ii).p06fir_filter_coefs;
    s=[sum(sum(fir([1 3],:))) sum(sum(fir([2 4],:)))];
    if s(1)<0,s=-s; end
    strfamp(ii,:)=s./sum(abs(s));
    
    s=abs(sum([fir([1 3],:) fir([2 4],:)],2));
    s=s./sum(s);
    ntau(ii)=tau(ii,:)*s;
    s=abs(sum(fir([1 3],:),2));
    s=s./sum(s);
    tauch(ii,1)=tau(ii,:)*s;
    s=abs(sum(fir([2 4],:),2));
    s=s./sum(s);
    tauch(ii,2)=tau(ii,:)*s;
    
    tau2(ii,:)=abs(fitset2(ii).p04depression_filter_bank_tau.*10); % ms
    fir2=fitset2(ii).p07fir_filter_coefs;
    s=abs(sum(fir2,2))';
    strfstr(ii,:)=s./sum(s);
end

figure;

subplot(3,2,1);
dd=[strfamp(olapidx,2) depvimprove(olapidx)];
dd=sortrows(dd);
ff=find(dd(:,2));
plot(dd(:,1),'k-','LineWidth',2);
hold on
plot(ff,dd(ff,1),'ko');
plot([1 length(dd)],[0 0],'k--');
hold off
axis([0 length(olapidx)+1 -1 1]);
xlabel('neuron');
ylabel('ch2 amp relative to c1');

subplot(3,2,2);
dd=[ct(olapidx) depvimprove(olapidx)];
dd=sortrows(dd);
ff=find(dd(:,2));
plot(dd(:,1),'k-','LineWidth',2);
hold on
plot(ff,dd(ff,1),'ko');
hold off
axis([0 length(olapidx)+1 0 100]);
xlabel('neuron');
ylabel('dep crosstalk');

subplot(3,2,3);
dd=[ntau(olapidx) tauch(olapidx,:) depvimprove(olapidx) tau(olapidx,:)];
dd=sortrows(dd);
ff=find(dd(:,4));
semilogy(dd(:,2),'r-','LineWidth',2);
hold on
semilogy(dd(:,3),'b-','LineWidth',2);
semilogy(dd(:,5:6),'.','Color',[.8 .8 .8]);
semilogy(ff,dd(ff,1),'ko');
hold off
axis([0 length(olapidx)+1 5 5000]);
set(gca,'YTick',[10 100 1000],'YTickLabel',{10^1,10^2,10^3});
xlabel('neuron');
ylabel('tau depression per chan');
legend('ch1','ch2','Location','southeast');

subplot(3,2,4);
dd=[ntau(olapidx) tau(olapidx,:) depvimprove(olapidx)];
dd=sortrows(dd);
ff=find(dd(:,4));
semilogy(dd(:,2:3),'r.');
hold on
semilogy(dd(:,1),'k','LineWidth',2);
semilogy(ff,dd(ff,1),'ko');
hold off
axis([0 length(olapidx)+1 5 5000]);
set(gca,'YTick',[10 100 1000],'YTickLabel',{10^1,10^2,10^3});
xlabel('neuron');
ylabel('tau depression per synapse');
title(sprintf('Batch %d tuning summary',batchid));

subplot(3,2,5);
dd=[predxcraw(olapidx,8)-predxcraw(olapidx,6) strfstr(olapidx,:)];
dd=sortrows(dd);
imagesc(dd(:,2:end)');
axis xy
ylabel('channel');
xlabel('neuron (sorted by pred improvement dep2v over dep2)');
title('rel gain per channel');

subplot(3,2,6);
%dd=[mean(strfstr(olapidx,[6]),2) ...
%    sort(tau2(olapidx,:),2) depvimprove(olapidx) ];
dd=[predxcraw(olapidx,8)-predxcraw(olapidx,6) ...
    sort(tau2(olapidx,:),2) depvimprove(olapidx) ];
dd=sortrows(dd);
ff=find(dd(:,4));
semilogy(dd(:,2:3),'LineWidth',2);
hold on
semilogy(ff,dd(ff,2:3),'ko');
hold off
axis([0 length(olapidx)+1 5 5000]);
set(gca,'YTick',[10 100 1000],'YTickLabel',{10^1,10^2,10^3});
xlabel('neuron');
legend('dep1','dep2');
return



if batchid==240,

    data={...
        {'por023a-a1',[90 3 1]},...
        {'por023a-b1',[10 3 1]},...
        {'por023a-c1',[100 2 -2]},...
        {'por023a-c2',[100 1 -2]},...
        {'por023a-d1',[100 3 -1]},...
        {'por024a-a1',[10 3 1]},...
        {'por024a-a2',[60 1 -1]},...
        {'por024a-b1',[90 2 -1]},...
        {'por024a-b2',[90 2 -2]},...
        {'por024a-c1',[10 -2 -2]},...
        {'por024a-c2',[70 1 -2]},...
        {'por025a-b1',[70 2 -1]},...
        {'por025a-c1',[70 2 1]},...
        {'por025a-c2',[10 3 1]},...
        {'por025a-d1',[80 2 0]},...
        {'por026a-b1',[30 2 -1]},...
        {'por026a-d1',[60 -2 -1]},...
        {'por026b-a1',[5 3 -2]},...
        {'por026b-a2',[40 3 -2]},...
        {'por026b-b1',[80 2 0]},...
        {'por026b-b2',[100 2 1]},...
        {'por026b-c1',[100 3 -1]},...
        {'por026b-c2',[0 1 1]},...
        {'por026b-d1',[0 -2 2]},...
        {'por026c-a1',[10 2 -1]},...
        {'por026c-b1',[0 2 -1]},...
        {'por026c-b2',[10 2 -1]},...
        {'por026c-c1',[0 3 -1]},...
        {'por026c-d1',[5 -2 -2]},...
        {'por026c-d2',[100 3 1]},...
        {'por027a-a1',[100 2 -1]},...
        {'por027a-b1',[100 2 -1]},...
        {'por027a-c1',[80 2 -1]},...
        {'por027b-b1',[80 2 -1]},...
        {'por027b-c1',[0 1 1]},...
        {'por028b-b1',[0 -1 2]},...
        {'por028b-c1',[100 3 -2]},...
        {'por028b-d1',[100 2 0]},...
        {'por028d-a1',[10 -1 1]},...
        {'por028d-a2',[100 2 -1]},...
        {'por028d-b1',[100 3 -2]},...
        {'por028d-c1',[5 0 -1]},...
        {'por028d-c2',[100 1 1]},...
        {'por028d-d1',[100 1 2]},...
        {'por029a-a1',[80 1 1]},...
        {'por029a-a2',[100 1 1]},...
        {'por029a-b1',[0 2 1]},...
        {'por029a-b2',[0 3 1]},...
        {'por030b-b1',[100 1 2]},...
        {'por030b-b2',[100 -1 1]},...
        {'por031a-09-1',[80 3 0]},...
        {'por031a-09-2',[80 -1 1]},...
        {'por031a-19-1',[100 2 1]},...
        {'por031a-19-2',[80 2 1]},...
        {'por049a-07-1',[100 2 -1]},...
        {'por049a-10-1',[100 2 0]},...
        {'por049a-12-1',[100 2 -1]},...
        {'por049a-12-2',[100 3 1]},...
        {'por049a-15-1',[100 3 -1]},...
        {'por049a-15-2',[100 3 -1]},...
        {'por049a-16-1',[100 1 0]},...
        {'por049a-16-2',[100 3 0]},...
        {'por049a-18-1',[100 3 0]},...
        {'por049a-26-1',[100 2 0]},...
        {'por049a-31-1',[0 1 2]},...
        {'por053a-06-1',[80 2 0]},...
        {'por053a-10-1',[100 2 0]},...
        {'por053a-12-1',[50 2 -1]},...
        {'por053a-32-1',[10 3 -1]},...
        {'por076b-a1',[100 3 -1]},...
        {'por076b-d1',[20 -3 2]},...
        {'por096b-b1',[70 3 -1]},...
        {'por096b-b2',[0 2 -2]},...
        {'por096b-c1',[100 3 1]},...
        {'por098b-a1',[90 -1 3]},...
        {'por100a-b1',[100 2 1]},...
        {'por100a-b2',[100 3 -1]},...
        {'por102a-b1',[100 2 2]},...
        {'por102a-b2',[100 1 1]},...
        {'por102a-d1',[100 2 -1]},...
        {'por102c-a1',[80 3 -2]},...
        {'por102c-b1',[80 2 1]},...
        {'por102c-b2',[80 -1 1]},...
        {'por108b-a1',[100 -1 1]},...
        {'por108b-b2',[100 2 -2]},...
        {'por108b-c1',[60 -1 1]} };
    
    cs_data_hand=zeros(cellcount,3);
    for ff=1:cellcount,
        ii=find(strcmp(data{ff}{1},cellids));
        
        % assume cellids come in same order!
        cs_data_hand(ii,:)=data{ff}{2};
        
        %if auto_overlap(ii)>=0,
        %    cs_data_hand(ii,1)=auto_overlap(ii);
        %end
    end
end
