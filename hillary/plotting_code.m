
% code for plotting per cell information for all models

plot_cells(ac_all_data,ac_by_cell)
plot_cells(pc_all_data,pc_by_cell)
plot_cells(acno_all_data,acno_by_cell)
plot_cells(pcno_all_data,pcno_by_cell)
plot_cells(wcac_all_data,wcac_by_cell)
plot_cells(wcpc_all_data,wcpc_by_cell)
plot_cells(wcacno_all_data,wcacno_by_cell)
plot_cells(wcpcno_all_data,wcpcno_by_cell)
plot_cells(adp1_all_data,adp1_by_cell)

% all of the spider plot code for previous version of spider_plot

spider_plot(pc_all_data)
spider_plot(pcno_all_data)
spider_plot(wcpc_all_data)
spider_plot(wcpcno_all_data)
spider_plot(adp1_all_data)