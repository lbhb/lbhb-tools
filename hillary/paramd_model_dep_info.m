
function depression_stats=paramd_model_dep_info(all_pc_model_data, ...
    pc_filtered_cell_lists,filter_nickname)

% function depression_stats=all_model_dep_info(all_pc_model_data, ...
%   pc_filtered_cell_lists,filter_nickname)
%
% Plots fraction of cells with less depression on C2 for each combination
% of model, FIR magnitude calculation, and depression calculation.
% Generates a structure containing a summary of depression information for
% all models. Modified function for use with parameterized FIR models
% (ap3z1).
%
% Inputs:
%   all_pc_model_data - structure containing all pc model structures
%       from narf_load_demo
%   pc_filtered_cell_lists - structure of pc models containing vectors of
%       cells satisfying specified analysis criteria
%   filter_nickname - string for filter shorthand
%
% Output: a plot saved as jpg, and a structure with depression statistics
%   indexed by model

model_num=length(all_pc_model_data);
model_list={zeros(model_num,1)};
for ii=1:model_num
    model_list{ii}={all_pc_model_data(ii).model_nickname};
end

% define subset of cells to plot
cell_num=zeros(model_num,1);
if nargin==1
    filter_nickname='all';
    for ii=1:model_num
        unfiltered_cell_num=length(all_pc_model_data(1).gains);
        cell_num(ii)=length(all_pc_model_data(1).raw_channel_magnitude);
        analysis_cells.(char(model_list{ii}))=1:cell_num(ii);
    end
else
    for ii=1:model_num
        unfiltered_cell_num=length(all_pc_model_data(1).gains);
        cell_num(ii)=length(pc_filtered_cell_lists.( ...
            char(model_list{ii})));
        analysis_cells.(char(model_list{ii}))=pc_filtered_cell_lists.( ...
            char(model_list{ii}));
    end
end

archive_path=['/auto/users/hillary/code/archive/' ...
    datestr(now(),'yyyy-mm-dd')];
filename=[archive_path '/ap3z1_model_depression_' ...
    filter_nickname '_cells'];
title_filter_nickname=strrep(filter_nickname,'_',' ');

% preallocate
gains=zeros(model_num,unfiltered_cell_num,2);
depression_magnitude=zeros(model_num,unfiltered_cell_num,2);
depression_strength=zeros(model_num,unfiltered_cell_num,2);
depression_tau=zeros(model_num,unfiltered_cell_num,2);

% sort data by best channel
for ii=1:model_num
    if size(all_pc_model_data(ii).depression_magnitude,2)==2
    for jj=1:unfiltered_cell_num
        if all_pc_model_data(ii).best_gains(jj)==1 && ...
            ismember(jj,analysis_cells.(char(model_list{ii})))==1
            gains(ii,jj,:)= ...
                [all_pc_model_data(ii).gains(jj,1), ...
                all_pc_model_data(ii).gains(jj,2)];
            depression_magnitude(ii,jj,:)= ...
                [all_pc_model_data(ii).depression_magnitude(jj,1), ...
                all_pc_model_data(ii).depression_magnitude(jj,2)];
            depression_strength(ii,jj,:)= ...
                [all_pc_model_data(ii).depression_strength(jj,1), ...
                all_pc_model_data(ii).depression_strength(jj,2)];
            depression_tau(ii,jj,:)= ...
                [all_pc_model_data(ii).depression_tau(jj,1), ...
                all_pc_model_data(ii).depression_tau(jj,2)];
        elseif all_pc_model_data(ii).best_gains(jj)==2 && ...
            ismember(jj,analysis_cells.(char(model_list{ii})))==1
            gains(ii,jj,:)= ...
                [all_pc_model_data(ii).gains(jj,2), ...
                all_pc_model_data(ii).gains(jj,1)];
            depression_magnitude(ii,jj,:)= ...
                [all_pc_model_data(ii).depression_magnitude(jj,2), ...
                all_pc_model_data(ii).depression_magnitude(jj,1)];
            depression_strength(ii,jj,:)= ...
                [all_pc_model_data(ii).depression_strength(jj,2), ...
                all_pc_model_data(ii).depression_strength(jj,1)];
            depression_tau(ii,jj,:)= ...
                [all_pc_model_data(ii).depression_tau(jj,2), ...
                all_pc_model_data(ii).depression_tau(jj,1)];
        end
    end
    end
end

% calculate depression change between channels
del_dep_magnitude=zeros(model_num,unfiltered_cell_num);
del_dep_strength=zeros(model_num,unfiltered_cell_num);
del_dep_tau=zeros(model_num,unfiltered_cell_num);

for ii=1:model_num
    del_dep_magnitude(ii,:)=depression_magnitude(ii,:,2)-...
        depression_magnitude(ii,:,1);
    del_dep_strength(ii,:)=depression_strength(ii,:,2)-...
        depression_strength(ii,:,1);
    del_dep_tau(ii,:)=depression_tau(ii,:,2)-...
        depression_tau(ii,:,1);
end

dec_dep_magnitude=zeros(model_num,1);
dec_dep_strength=zeros(model_num,1);
inc_dep_tau=zeros(model_num,1);

for ii=1:model_num
    dec_dep_magnitude(ii)= ...
        length(find(del_dep_magnitude(ii,:)<0));
    dec_dep_strength(ii)= ...
        length(find(del_dep_strength(ii,:)<0));
    inc_dep_tau(ii)= ...
        length(find(del_dep_tau(ii,:)>0));
end

ratio_dep_magnitude=zeros(model_num,1);
ratio_dep_strength=zeros(model_num,1);
ratio_dep_tau=zeros(model_num,1);

for ii=1:model_num
    ratio_dep_magnitude(ii)=dec_dep_magnitude(ii)/cell_num(ii);
    ratio_dep_strength(ii)=dec_dep_strength(ii)/cell_num(ii);
    ratio_dep_tau(ii)=inc_dep_tau(ii)/cell_num(ii);
end

ratios=[ratio_dep_magnitude,ratio_dep_strength,ratio_dep_tau];
bar_labels={zeros(model_num,4)};

for ii=1:model_num
    model_nickname=model_list{ii}{1};
    bar_labels{ii,1}=[];
    bar_labels{ii,2}=sprintf('%s',model_nickname,' magnitude');
    bar_labels{ii,3}=sprintf('%s',model_nickname,' strength');
    bar_labels{ii,4}=sprintf('%s',model_nickname,' tau');
end

bar_labels=reshape(bar_labels',model_num*4,1);
ytick=.5:.25:2.5;
annotations={zeros(model_num*3,1)};

for ii=1:model_num
    annotations{ii,1}=sprintf('%d/%d',dec_dep_magnitude(ii),cell_num(ii));
    annotations{ii,2}=sprintf('%d/%d',dec_dep_strength(ii),cell_num(ii));
    annotations{ii,3}=sprintf('%d/%d',inc_dep_tau(ii),cell_num(ii));
end
annotations=reshape(annotations',model_num*3,1);

% plot bar graph showing percent of cells with decreased C2 depression for
% each combination of models, FIR weight calculations, and depression
% calculations
fig=figure;
hold on

barh(ratios)
set(gca,'YTick',ytick,'YTickLabel',bar_labels);

title(['All model depression stats for ' title_filter_nickname ' cells'])
xlabel('Fraction of cells with decreased C2 magnitude/strength, increased C2 tau')
xlim([0 1])

xpos=reshape(ratios',model_num*3,1);
n=1;
jj=1;

for ii=1:length(xpos)
    if n~=4
        text(xpos(ii)+.01,ytick(ii+jj),annotations{n})
        n=n+1;
    else
        jj=jj+1;
        text(xpos(ii)+.01,ytick(ii+jj),annotations{n})
        n=n+1;
    end
end

hold
print(fig,'-djpeg','-r150',filename);

% fill in a struct with the information about C2 depression
dim1=size(del_dep_magnitude,1);
dim2=size(del_dep_magnitude,2);
depression_stats=struct( ...
    'model_nickname',zeros(model_num,1), ...
    'del_dep_magnitude',zeros(dim1,dim2), ...
    'dec_dep_magnitude',zeros(length(dec_dep_magnitude),1), ...
    'del_dep_strength',zeros(dim1,dim2), ...
    'dec_dep_strength',zeros(length(dec_dep_magnitude),1), ...
    'del_dep_tau',zeros(dim1,dim2), ...
    'inc_dep_tau',zeros(length(dec_dep_magnitude),1));

for ii=1:model_num
    depression_stats(ii).model_nickname= ...
        all_pc_model_data(ii).model_nickname;
    depression_stats(ii).del_dep_magnitude= ...
        reshape( ...
        find(del_dep_magnitude(ii,:)~=0), ...
        length(find(del_dep_magnitude(ii,:)~=0)),1);
    depression_stats(ii).dec_dep_magnitude= ...
        dec_dep_magnitude(ii);
    depression_stats(ii).del_dep_strength= ...
        reshape( ...
        find(del_dep_strength(ii,:)~=0), ...
        length(find(del_dep_strength(ii,:)~=0)),1);
    depression_stats(ii).dec_dep_strength= ...
        dec_dep_strength(ii);
    depression_stats(ii).del_dep_tau= ...
        reshape( ...
        find(del_dep_tau(ii,:)~=0), ...
        length(find(del_dep_tau(ii,:)~=0)),1);
    depression_stats(ii).inc_dep_tau= ...
        inc_dep_tau(ii);
end

depression_stats=depression_stats';
