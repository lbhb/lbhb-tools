
function new_c1vc2dep(all_data,filtered_cell_list,filter_nickname)

% function new_c1vc2dep(all_data,filtered_cell_list,filter_nickname)
% 
% A revised version of c1vc2dep with weighting by early magnitude and more
% metrics for depression.
%
% Using the all_data structure generated with narf_load_demo, plots 
% depression of the best channel in the FIR filter against the depression 
% of the secondary channel for cells indexes in filtered_cell_list.
%
% Inputs:
%  all_data - a struct generated with narf_load_demo
%  filtered_cell_list - optional input vector of filtered cells, generated
%       using filter_cells
%  filter_nickname - optional input string specifying a nickname for 
%       filtering scheme, which is used in the plot title and filename
%
% Output:
%  Three plots for each combination of depression measure, displayed in the
%  same figure window sequentially and printed to the archive folder.

cell_num=length(all_data.best_early);
depression_magnitude=all_data.depression_magnitude;
depression_strength=all_data.depression_strength;
depression_tau=all_data.depression_tau;
assignment_mat=all_data.best_early;
channel_magnitudes=all_data.early_channel_magnitude;
metric_string={ ...
    'magnitude', ...
    'strength', ...
    'tau'};
metrics=length(metric_string);

if nargin==1
    analysis_cells=1:cell_num;
    filter_nickname='all';
else
    analysis_cells=filtered_cell_list;
end

% specify filename for printed jpeg
label=[all_data.model_nickname '_' filter_nickname];
archive_path=['/auto/users/hillary/code/archive/' ...
    datestr(now(),'yyyy-mm-dd')];
filename=[archive_path '/c1vc2dep_' label];
title_filter_nickname=strrep(filter_nickname,'_',' ');

% sort data by best channel
assigned_depression_magnitude=zeros(cell_num,2);
assigned_depression_strength=zeros(cell_num,2);
assigned_depression_tau=zeros(cell_num,2);
weight=zeros(metrics,cell_num,2);

for kk=1:cell_num
    switch assignment_mat(kk)
        case 1
            weight(1,kk,:)= ...
                [channel_magnitudes(kk,1), ...
                channel_magnitudes(kk,2)];
            weight(2,kk,:)= ...
                [channel_magnitudes(kk,1), ...
                channel_magnitudes(kk,2)];
            weight(3,kk,:)= ...
                [channel_magnitudes(kk,1), ...
                channel_magnitudes(kk,2)];
            assigned_depression_magnitude(kk,:)= ...
                [depression_magnitude(kk,1), ...
                depression_magnitude(kk,2)];
            assigned_depression_strength(kk,:)= ...
                [depression_strength(kk,1), ...
                depression_strength(kk,2)];
            assigned_depression_tau(kk,:)= ...
                [depression_tau(kk,1), ...
                depression_tau(kk,2)];
        case 2
            weight(1,kk,:)= ...
                [channel_magnitudes(kk,2), ...
                channel_magnitudes(kk,1)];
            weight(2,kk,:)= ...
                [channel_magnitudes(kk,2), ...
                channel_magnitudes(kk,1)];
            weight(3,kk,:)= ...
                [channel_magnitudes(kk,2), ...
                channel_magnitudes(kk,1)];
            assigned_depression_magnitude(kk,:)= ...
                [depression_magnitude(kk,2), ...
                depression_magnitude(kk,1)];
            assigned_depression_strength(kk,:)= ...
                [depression_strength(kk,2), ...
                depression_strength(kk,1)];
            assigned_depression_tau(kk,:)= ...
                [depression_tau(kk,2), ...
                depression_tau(kk,1)];
    end
end
depression=zeros(metrics,cell_num,2);
depression(1,:,:)=assigned_depression_magnitude;
depression(2,:,:)=assigned_depression_strength;
depression(3,:,:)=assigned_depression_tau;

% plot C1 vs. C2 depression for each cell in analysis_cells
fig=figure;
for ii=1:metrics
    for jj=1:cell_num
        if ismember(jj,analysis_cells)==1 && ii==1
            plot(depression(ii,jj,1), depression(ii,jj,2),'kd', ...
                'MarkerFaceColor','k');
            hold on
        elseif ismember(jj,analysis_cells)==1
            loglog(depression(ii,jj,1), depression(ii,jj,2),'kd', ...
                'MarkerFaceColor','k');
            hold on
        end
        
        if jj==analysis_cells(end) && ii==1
            plot([0 1],[0 1],'k--')
        elseif jj==analysis_cells(end)
            hline=refline(1,0);
            set(hline,'LineStyle','--','Color','k')
        end
    end

    if ii==1
        depression_type='magnitude';
    elseif ii==2
        depression_type='strength';
    elseif ii==3
        depression_type='tau';
    end
    title(['Depression ' depression_type ' of C1 vs. C2 ' ...
        ' for ' title_filter_nickname ...
        ' cells'])
    xlabel(['C1 Depression ' depression_type])
    ylabel(['C2 Depression ' depression_type])
    axis square
    print(fig,'-djpeg','-r150',[filename '_' metric_string{ii}]);
    hold off
end

end




