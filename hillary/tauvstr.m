
function tauvstr(all_data,filtered_cell_list,filter_nickname)

% function tauvstr(all_data,filtered_cell_list,filter_nickname)
% 
% Using the all_data structure generated with narf_load_demo, plots 
% depression tau against depression strength for each channel
% of cells indexes in filtered_cell_list.
%
% Inputs:
%  all_data - a struct generated with narf_load_demo
%  filtered_cell_list - optional input vector of filtered cells, generated
%       using filter_cells
%  filter_nickname - optional input string specifying a nickname for 
%       filtering scheme, which is used in the plot title and filename
%
% Output:
%  Two plots for different channel weightings (early and raw), displayed in
%  the same figure window sequentially and printed to the archive folder.

cell_num=length(all_data.best_raw);
depression_str=all_data.depression_strength;
depression_tau=all_data.depression_tau;
weights=2;
assignment_mat=zeros(weights,cell_num);
assignment_mat(1,:)=all_data.best_raw;
assignment_mat(2,:)=all_data.best_early;
channel_magnitudes=zeros(weights,cell_num,2);
channel_magnitudes(1,:,:)=all_data.raw_channel_magnitude;
channel_magnitudes(2,:,:)=all_data.early_channel_magnitude;
metric_string={ ...
    'raw_magnitude', ...
    'early_magnitude'};
metrics=length(metric_string);

if nargin==1
    analysis_cells=1:cell_num;
    filter_nickname='all';
else
    analysis_cells=filtered_cell_list;
end

% specify filename for printed jpeg
label=[all_data.model_nickname '_' filter_nickname];
archive_path=['/auto/users/hillary/code/archive/' ...
    datestr(now(),'yyyy-mm-dd')];
filename=[archive_path '/tauvstr_' label];
title_filter_nickname=strrep(filter_nickname,'_',' ');

% sort data by best channel
assigned_depression_str=zeros(weights,cell_num,2);
assigned_depression_tau=zeros(weights,cell_num,2);
weight=zeros(metrics,cell_num,2);
n=1;
for jj=1:weights
    for kk=1:cell_num
        switch assignment_mat(jj,kk)
            case 1
                weight(n,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                weight(n+1,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                assigned_depression_str(jj,kk,:)= ...
                    [depression_str(kk,1), ...
                    depression_str(kk,2)];
                assigned_depression_tau(jj,kk,:)= ...
                    [depression_tau(kk,1), ...
                    depression_tau(kk,2)];
            case 2
                weight(n,kk,:)= ...
                    [channel_magnitudes(jj,kk,2), ...
                    channel_magnitudes(jj,kk,1)];
                weight(n+1,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                assigned_depression_str(jj,kk,:)= ...
                    [depression_str(kk,2), ...
                    depression_str(kk,1)];
                assigned_depression_tau(jj,kk,:)= ...
                    [depression_tau(kk,2), ...
                    depression_tau(kk,1)];
        end
    end
    n=n+2;
end

% plot depression tau vs. str for each cell in analysis_cells
fig=figure;
for ii=1:metrics
    m=1;
    for jj=1:cell_num
        if ismember(jj,analysis_cells)==1
            c1=loglog(abs(assigned_depression_str(ii,jj,1)), ...
                abs(assigned_depression_tau(ii,jj,1)),'kd', ...
                'MarkerFaceColor','k');
            if m==1
            hold on
            end
            c2=loglog(abs(assigned_depression_str(ii,jj,2)), ...
                abs(assigned_depression_tau(ii,jj,2)),'kd');
            str_vec=abs(squeeze(assigned_depression_str(ii,jj,:)));
            tau_vec=abs(squeeze(assigned_depression_tau(ii,jj,:)));
            plot(str_vec,tau_vec,'k-');
            m=m+1;
        end
    end
    if ii==1
        weighting='raw';
    elseif ii==2
        weighting='early';
    end
    title(['Depression strength vs. depression tau using ' ...
        weighting ' synaptic weight for ' title_filter_nickname ...
        ' cells'])
    xlabel('Depression strength')
    ylabel('Depression tau')
    legend([c1 c2],'Channel 1','Channel 2')
    plot([0 1],[0 1],'k--')
    print(fig,'-djpeg','-r150',[filename '_' metric_string{ii}]);
    hold off
end

end




