
function plot_cells(all_data,by_cell)

% function plot_cells(all_data,by_cell)
% 
% Using structures generated with narf_load_demo, generates graphical
% displays of the FIR filters with depression cartoons, along with
% calculated channel and depression magnitudes from narf_load_demo.
%
% Inputs:
%  all_data - structure output of narf_load_demo
%  by_cell - structure output of narf_load_demo
%
% Output: Many figures printed to jpeg files, displayed sequentially in a
%  single figure window. All calculated FIR weights are displayed, with
%  'best channel' designation indicated by an asterisk (*) next to the
%  calculated channel magnitude.

cell_num=length(by_cell);
fnames={zeros(cell_num,1)};
label=['/' all_data.model_nickname];
archive_path=['/auto/users/hillary/code/archive/' ...
    datestr(now(),'yyyy-mm-dd') label '/'];

% preallocate
fs=100;
stim=[zeros(fs./2,1);ones(fs,1)./2;zeros(fs,1);
        ones(round(fs./10),1);zeros(round(fs./10),1);
        ones(round(fs./10),1);zeros(7.*fs./10,1)]';
fir_coefs=zeros(2,15);

fig=figure('Units','normalized', ...
    'Position',[.2 .2 .8 .4], ...
    'PaperPositionMode','auto');

for ii=1:cell_num
    % specify file names for saving figures
    fnames{ii}=cellstr([archive_path label '_cellplot_' ...
        num2str(ii) '_' sprintf('%s',by_cell(ii).cellid)]);
    
    % specify FIR coefficients
    fir_coefs(1,:)=by_cell(ii).fir_coefs(1,:);
    fir_coefs(2,:)=by_cell(ii).fir_coefs(2,:);
    
    % figure
    model_title=strrep(all_data.model_nickname,'_',' ');
    suptitle([model_title '\_' ...
        num2str(ii) '\_' num2str(by_cell(ii).cellid)])
    
    % configure depression responses
    respii=squeeze(by_cell(ii).depression_response);
    if size(respii,2)==1
    respii=respii';
    end
    
    % plot stimulus and depression cartoons
    subplot(1,2,1)
    plot([stim'+1 respii']);

    if size(by_cell(ii).depression_magnitude,2)==2
        text(25,1.9, ...
            ['Index 1 depression magnitude=', ...
            num2str(by_cell(ii).depression_magnitude(1,1))], ...
            'Color',[0 102/255 0])
        text(25,1.85, ...
            ['Index 1 depression impact=', ...
            num2str(by_cell(ii).depression_impact(1,1))], ...
            'Color',[0 102/255 0])
        text(25,1.8, ...
            ['Index 2 depression magnitude=', ...
            num2str(by_cell(ii).depression_magnitude(1,2))], ...
            'Color',[1 0 0])
        text(25,1.75, ...
            ['Index 2 depression impact=', ...
            num2str(by_cell(ii).depression_impact(1,2))], ...
            'Color',[1 0 0])
    elseif size(by_cell(ii).depression_magnitude,2)==1
        text(25,1.9, ...
            ['Depression magnitude=', ...
            num2str(by_cell(ii).depression_magnitude(1,1))], ...
            'Color',[0 102/255 0])
        text(25,1.85, ...
            ['Depression impact=', ...
            num2str(by_cell(ii).depression_impact(1,1))], ...
            'Color',[0 102/255 0])
    end
    title('Depression cartoon')
    
    % plot fir filters
    subplot(1,2,2)
    fir_abs_max=max(max(abs(fir_coefs(:,:))));
    imagesc(fir_coefs(:,:),[-fir_abs_max fir_abs_max])
    axis xy

    best_raw=all_data.best_raw(ii);
    best_signed=all_data.best_signed(ii);
    best_unsigned=all_data.best_unsigned(ii);
    best_early=all_data.best_early(ii);
    
    if best_raw==1
        text(9,1.35,['Index 1 raw weight=' num2str( ...
            all_data.raw_channel_magnitude(ii,1)) '*'])
        text(9,2.35,['Index 2 raw weight=' num2str( ...
            all_data.raw_channel_magnitude(ii,2))])
    elseif best_raw==2
        text(9,1.35,['Index 1 raw weight=' num2str( ...
            all_data.raw_channel_magnitude(ii,1))])
        text(9,2.35,['Index 2 raw weight=' num2str( ...
            all_data.raw_channel_magnitude(ii,2)) '*'])
    end
    
    if best_signed==1
        text(9,1.25,['Index 1 signed weight=' num2str( ...
            all_data.signed_channel_magnitude(ii,1)) '*'])
        text(9,2.25,['Index 2 signed weight=' num2str( ...
            all_data.signed_channel_magnitude(ii,2))])
    elseif best_signed==2
        text(9,1.25,['Index 1 signed weight=' num2str( ...
            all_data.signed_channel_magnitude(ii,1))])
        text(9,2.25,['Index 2 signed weight=' num2str( ...
            all_data.signed_channel_magnitude(ii,2)) '*'])
    end
    
    if best_unsigned==1
        text(9,1.15,['Index 1 unsigned weight=' num2str( ...
            all_data.unsigned_channel_magnitude(ii,1)) '*'])
        text(9,2.15,['Index 2 unsigned weight=' num2str( ...
            all_data.unsigned_channel_magnitude(ii,2))])
    elseif best_unsigned==2
        text(9,1.15,['Index 1 unsigned weight=' num2str( ...
            all_data.unsigned_channel_magnitude(ii,1))])
        text(9,2.15,['Index 2 unsigned weight=' num2str( ...
            all_data.unsigned_channel_magnitude(ii,2)) '*'])
    end
    
    if best_early==1
        text(9,1.05,['Index 1 early weight=' num2str( ...
            all_data.early_channel_magnitude(ii,1)) '*'])
        text(9,2.05,['Index 2 early weight=' num2str( ...
            all_data.early_channel_magnitude(ii,2))])
    elseif best_early==2
        text(9,1.05,['Index 1 early weight=' num2str( ...
            all_data.early_channel_magnitude(ii,1))])
        text(9,2.05,['Index 2 early weight=' num2str( ...
            all_data.early_channel_magnitude(ii,2)) '*'])
    end
    
    title('FIR filter')
    filename=fnames{ii};
    print(fig,'-djpeg','-r150',cell2mat(filename));
    
end

end
