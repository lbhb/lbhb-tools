
[nodep_data,by_cell]=narf_load_demo('%',259, ...
           'env100_logn_fir15_siglog100_fit05h_fit05c','no_dep');
[adp_data,by_cell]=narf_load_demo('%',259, ...
           'env100_logn_wc02c_adp1pcno_fir15_siglog100_fit05h_fit05c','adp1');

all_model_data=nodep_data;
all_model_data(2)=adp_data;

filtered_cell_lists=filter_cells(all_model_data,...
                   'two_channel','depression_improved');

c1vc2dep(adp_data,filtered_cell_lists.adp1,'perf');
