
function c1vc2dep(all_data,filtered_cell_list,filter_nickname)

% function c1vc2dep(all_data,filtered_cell_list,filter_nickname)
% 
% Using the all_data structure generated with narf_load_demo, plots 
% depression of the best channel in the FIR filter against the depression 
% of the secondary channel for cells indexes in filtered_cell_list.
%
% Inputs:
%  all_data - a struct generated with narf_load_demo
%  filtered_cell_list - optional input vector of filtered cells, generated
%       using filter_cells
%  filter_nickname - optional input string specifying a nickname for 
%       filtering scheme, which is used in the plot title and filename
%
% Output:
%  Eight plots for each combination of channel weighting and depression
%  calculation, displayed in the same figure window sequentially and
%  printed to the archive folder.

cell_num=length(all_data.best_raw);
depression_magnitude=all_data.depression_magnitude;
depression_impact=all_data.depression_impact;
weights=4;
assignment_mat=zeros(weights,cell_num);
assignment_mat(1,:)=all_data.best_raw;
assignment_mat(2,:)=all_data.best_signed;
assignment_mat(3,:)=all_data.best_unsigned;
assignment_mat(4,:)=all_data.best_early;
channel_magnitudes=zeros(weights,cell_num,2);
channel_magnitudes(1,:,:)=all_data.raw_channel_magnitude;
channel_magnitudes(2,:,:)=all_data.signed_channel_magnitude;
channel_magnitudes(3,:,:)=all_data.unsigned_channel_magnitude;
channel_magnitudes(4,:,:)=all_data.early_channel_magnitude;
metric_string={ ...
    'raw_magnitude', ...
    'raw_impact', ...
    'signed_magnitude', ...
    'signed_impact', ...
    'unsigned_magnitude', ...
    'unsigned_impact', ...
    'early_magnitude', ...
    'early_impact'};
metrics=length(metric_string);

if nargin==1
    analysis_cells=1:cell_num;
    filter_nickname='all';
else
    analysis_cells=filtered_cell_list;
end

% specify filename for printed jpeg
label=[all_data.model_nickname '_' filter_nickname];
archive_path=['/auto/users/hillary/code/archive/' ...
    datestr(now(),'yyyy-mm-dd')];
filename=[archive_path '/c1vc2dep_' label];
title_filter_nickname=strrep(filter_nickname,'_',' ');

% sort data by best channel
assigned_depression_impact=zeros(weights,cell_num,2);
assigned_depression_magnitude=zeros(weights,cell_num,2);
weight=zeros(metrics,cell_num,2);
n=1;
for jj=1:weights
    for kk=1:cell_num
        switch assignment_mat(jj,kk)
            case 1
                weight(n,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                weight(n+1,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                assigned_depression_magnitude(jj,kk,:)= ...
                    [depression_magnitude(kk,1), ...
                    depression_magnitude(kk,2)];
                assigned_depression_impact(jj,kk,:)= ...
                    [depression_impact(kk,1), ...
                    depression_impact(kk,2)];
            case 2
                weight(n,kk,:)= ...
                    [channel_magnitudes(jj,kk,2), ...
                    channel_magnitudes(jj,kk,1)];
                weight(n+1,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                assigned_depression_magnitude(jj,kk,:)= ...
                    [depression_magnitude(kk,2), ...
                    depression_magnitude(kk,1)];
                assigned_depression_impact(jj,kk,:)= ...
                    [depression_impact(kk,2), ...
                    depression_impact(kk,1)];
        end
    end
    n=n+2;
end

depression=zeros(weights,cell_num,2);
n=1;
for ii=1:weights
    depression(n,:,:)=assigned_depression_magnitude(ii,:,:);
    depression(n+1,:,:)=assigned_depression_impact(ii,:,:);
    n=n+2;
end

% plot C1 vs. C2 depression for each cell in analysis_cells
fig=figure;
for ii=1:metrics
    for jj=1:cell_num
        if ismember(jj,analysis_cells)==1
            plot(depression(ii,jj,1), depression(ii,jj,2),'kd', ...
                'MarkerFaceColor','k');
            hold on
        end
    end
    if ismember(ii,[1 3 5 7])==1
        depression_type='magnitude';
    elseif ismember(ii,[2 4 6 8])==1
        depression_type='impact';
    end
    if ii==1 || ii==2
        weighting='raw';
    elseif ii==3 || ii==4
        weighting='signed';
    elseif ii==5 || ii==6
        weighting='unsigned';
    elseif ii==7 || ii==8
        weighting='early';
    end
    title(['Depression ' depression_type ' of C1 vs. C2 using ' ...
        weighting ' synaptic weight for ' title_filter_nickname ...
        ' cells'])
    xlabel(['C1 Depression ' depression_type])
    ylabel(['C2 Depression ' depression_type])
    plot([-1 1],[-1 1],'k--')
    ylim([-1 1])
    xlim([-1 1])
    print(fig,'-djpeg','-r150',[filename '_' metric_string{ii}]);
    hold off
end

end




