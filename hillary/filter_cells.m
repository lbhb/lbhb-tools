
function filtered_cell_lists=filter_cells(all_model_data,varargin)

% function filtered_cell_lists=filter_cells(all_model_data,criteria)
%
% Returns a model-indexed structure containing lists of indexes
% corresponding to cells satisfying the desired filtering criteria.
%
% Inputs:
%  all_model_data - structure of structures from narf_load_demo
%  criteria - comma-separated list of strings, from the following
%       'two_channel' - excludes cells whose best channel is 10-fold or
%           higher in signed magnitude than the secondary channel
%       'within_channel_variance' - excludes cells with insignificant
%           weighting on one of the two FIR channels, based on within-
%           channel variance
%       'between_channel_variance' - excludes cells containing a
%           relatively insignificant FIR channel, based on between-channel
%           variance
%       'model_performance' - excludes cells with low prediction scores
%           (r_test<0.2)
%       'model_improvement' - for ac vs. pc model comparison only. excludes
%           cells whose prediction scores are not significantly improved
%           relative to the corresponding model (ac for pc models, and pc
%           for ac models).
%       'depression_improved' - excludes cells for which modeling synaptic
%           depression does not improve model performance
%       'max_one_inhibitory' - excludes cells containing two
%           negatively-weighted FIR channels
%       'max_one_inhibitory_early' - excludes cells containing two
%           FIR channels that are negatively-weighted in the first 5 time
%           bins
%       'c1_unimodal' - excludes cells whose best channel is bimodal
%       'early_c1_unimodal' - excludes cells whose best channel is bimodal
%           in the first 5 time bins
%
% Output:
%  filtered_cell_list - a structure indexed by model, containing
%       vectors of cell indexes satisfying the indicated criteria

model_num=length(all_model_data);
cell_num=size(all_model_data(1).raw_channel_magnitude,1);
criteria_num=length(varargin);
criteria_list={varargin{:}};
filtered_cell_mat=zeros(model_num,criteria_num,cell_num);
ac_model_num=0;
pc_model_num=0;
ac_model_idxs=zeros(model_num,1);
pc_model_idxs=zeros(model_num,1);

for ii=1:model_num
    if strfind(all_model_data(ii).model_nickname,'ac')~=0
        ac_model_num=ac_model_num+1;
        ac_model_idxs(ii)=ii;
    elseif strfind(all_model_data(ii).model_nickname,'pc')~=0
        pc_model_num=pc_model_num+1;
        pc_model_idxs(ii)=ii;
    else
        pc_model_num=pc_model_num+1;
        pc_model_idxs(ii)=ii;
    end
end

% calculate values for different filters
signed_magnitude_ratio=zeros(model_num,cell_num);
within_channel_variance=zeros(model_num,cell_num,2);
between_channel_variance=zeros(model_num,cell_num);
model_performance=zeros(model_num,cell_num);
ac_model_performance=zeros(ac_model_num,cell_num);
pc_model_performance=zeros(pc_model_num,cell_num);
fir_weight=zeros(model_num,cell_num,2);
early_fir_weight=zeros(model_num,cell_num,2);
modality_index=zeros(model_num,cell_num,2);
early_modality_index=zeros(model_num,cell_num,2);
best_raw_channel=zeros(model_num,cell_num);

for ii=1:model_num
    if size(all_model_data(ii).depression_magnitude,2)==2
        for jj=1:cell_num
            
            best_raw_channel(ii,:)=all_model_data(ii).best_raw;
            best_signed_channel=all_model_data(ii).best_signed(jj);
            
            switch best_raw_channel(ii,jj)
                case 1
                    secondary_raw_channel=2;
                case 2
                    secondary_raw_channel=1;
            end
            
            switch best_signed_channel
                case 1
                    secondary_signed_channel=2;
                case 2
                    secondary_signed_channel=1;
            end
            
            signed_magnitude_ratio(ii,jj)= ...
                abs(all_model_data(ii).signed_channel_magnitude(jj,best_signed_channel)/ ...
                all_model_data(ii).signed_channel_magnitude(jj,secondary_signed_channel));
            
            within_channel_variance(ii,jj,1)= ...
                var(all_model_data(ii).fir_coefs(1,:,jj));
            within_channel_variance(ii,jj,2)= ...
                var(all_model_data(ii).fir_coefs(2,:,jj));
            
            between_channel_variance(ii,jj)= ...
                var(all_model_data(ii).fir_coefs(best_raw_channel(ii,jj),:,jj))/ ...
                var(all_model_data(ii).fir_coefs(secondary_raw_channel,:,jj));
            
            model_performance(ii,jj)=all_model_data(ii).r_test(jj);
            
            if ismember(ac_model_idxs,ii)==1
                ac_model_performance(ii,jj)=all_model_data(ii).r_test(jj);
            elseif ismember(pc_model_idxs,ii)==1
                pc_model_performance(ii,jj)=all_model_data(ii).r_test(jj);
            end
            
            fir_weight(ii,jj,:)= ...
                all_model_data(ii).raw_channel_magnitude(jj,:);
            early_fir_weight(ii,jj,:)= ...
                all_model_data(ii).early_channel_magnitude(jj,:);
            
            modality_index(ii,jj,1)= ...
                abs(sum(all_model_data(ii).fir_coefs(1,:,jj))/ ...
                sum(abs(all_model_data(ii).fir_coefs(1,:,jj))));
            modality_index(ii,jj,2)= ...
                abs(sum(all_model_data(ii).fir_coefs(2,:,jj))/ ...
                sum(abs(all_model_data(ii).fir_coefs(2,:,jj))));
            
            early_modality_index(ii,jj,1)= ...
                abs(sum(all_model_data(ii).fir_coefs(1,1:5,jj))/ ...
                sum(abs(all_model_data(ii).fir_coefs(1,1:5,jj))));
            early_modality_index(ii,jj,2)= ...
                abs(sum(all_model_data(ii).fir_coefs(2,1:5,jj))/ ...
                sum(abs(all_model_data(ii).fir_coefs(2,1:5,jj))));
        end
    end
end

% apply filtering criteria
for ii=1:model_num
    for jj=1:criteria_num
        switch criteria_list{jj}
            case 'two_channel'
                for kk=1:cell_num
                    if signed_magnitude_ratio(ii,kk)<10
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'within_channel_variance'
                for kk=1:cell_num
                    if within_channel_variance(ii,kk, ...
                            best_raw_channel(ii,kk))>1.4
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'between_channel_variance'
                for kk=1:cell_num
                    if between_channel_variance(ii,kk)>0.1
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'model_performance'
                for kk=1:cell_num
                    if model_performance(ii,kk)>0.2
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'model_improvement'
                if ismember(ii,pc_model_idxs)==1 && ...
                        isempty(strfind( ...
                        all_model_data(ii).model_nickname,'adp'))==1
                    comparison_model=all_model_data(ii-1);
                elseif ismember(ii,ac_model_idxs)==1
                    comparison_model=all_model_data(ii+1);
                else
                    break
                end
                for kk=1:cell_num
                    if model_performance(ii,kk)> ...
                            comparison_model.r_test(kk)+.01
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'depression_improved'
                for n=1:model_num
                    if isempty(strfind( ...
                            all_model_data(n).model_nickname,'no_dep'))==0
                        comparison_model=all_model_data(n);
                    end
                end
                
                for kk=1:cell_num
                    if model_performance(ii,kk)> ...
                            comparison_model.r_test(kk)
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'max_one_inhibitory'
                for kk=1:cell_num
                    if fir_weight(ii,kk,1)>0 || fir_weight(ii,kk,2)>0
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'max_one_inhibitory_early'
                for kk=1:cell_num
                    if early_fir_weight(ii,kk,1)>0 || ...
                            early_fir_weight(ii,kk,2)>0
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
            case 'c1_unimodal'
                if best_raw_channel(ii,1)~=0
                for kk=1:cell_num
                    if modality_index( ...
                            ii,kk,best_raw_channel(ii,kk))>0.4
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
                end
            case 'early_c1_unimodal'
                if best_raw_channel(ii,1)~=0
                for kk=1:cell_num
                    if early_modality_index( ...
                            ii,kk,best_raw_channel(ii,kk))>0.7
                        filtered_cell_mat(ii,jj,kk)=kk;
                    end
                end
                end
        end
    end
end

% include only cells satisfying all criteria
for ii=1:model_num
    filtered_cell_list=zeros(cell_num,1);
    for jj=1:cell_num
        if ismember(0,filtered_cell_mat(ii,:,jj))==0
            filtered_cell_list(jj)=jj;
        end
    end
    filtered_cell_list=filtered_cell_list(filtered_cell_list~=0);
    if ii==1
    filtered_cell_lists=struct(all_model_data(ii).model_nickname, ...
        filtered_cell_list);
    else
        filtered_cell_lists.(all_model_data(ii).model_nickname)= ...
            filtered_cell_list;
    end
end

