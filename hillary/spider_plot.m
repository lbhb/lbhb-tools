
function spider_plot(all_data,filtered_cell_list,filter_nickname)

% function spider_plot(all_data,filtered_cell_list,filter_nickname)
% 
% Using the all_data structure generated with narf_load_demo, plots 
% weight of each channel in the FIR filter against its depression 
% magnitude, with lines connecting the points corresponding to a cell.
%
% Inputs:
%  all_data - a struct generated with narf_load_demo
%  filtered_cell_list - optional input vector of filtered cells, generated
%       using filter_cells
%  filter_nickname - optional input string specifying a nickname for 
%       filtering scheme, which is used in the plot title and filename
%
% Output:
%  Two plots for each combination of channel weighting and depression
%  calculation, displayed in the same figure window sequentially and
%  printed to the archive folder.

cell_num=length(all_data.best_raw);
depression_magnitude=all_data.depression_magnitude;
depression_tau=all_data.depression_tau;
depression_strength=all_data.depression_strength;
weights=2;
assignment_mat=zeros(weights,cell_num);
assignment_mat(1,:)=all_data.best_raw;
assignment_mat(2,:)=all_data.best_early;
channel_magnitudes=zeros(weights,cell_num,2);
channel_magnitudes(1,:,:)=all_data.raw_channel_magnitude;
channel_magnitudes(2,:,:)=all_data.early_channel_magnitude;
metric_string={ ...
    'raw_magnitude', ...
    'early_magnitude', ...
    'raw_tau', ...
    'early_tau', ...
    'raw_strength', ...
    'early_strength'};
metrics=length(metric_string);

if nargin==1
    analysis_cells=1:cell_num;
    filter_nickname='all';
else
    analysis_cells=filtered_cell_list;
end

% specify filename for printed jpeg
label=[all_data.model_nickname '_' filter_nickname];
archive_path=['/auto/users/hillary/code/archive/' ...
    datestr(now(),'yyyy-mm-dd')];
filename=[archive_path '/spider_' label];
title_filter_nickname=strrep(filter_nickname,'_',' ');

% sort data by best channel
assigned_depression_magnitude=zeros(weights,cell_num,2);
assigned_depression_tau=zeros(weights,cell_num,2);
assigned_depression_strength=zeros(weights,cell_num,2);
weight=zeros(weights,cell_num,2);
for jj=1:weights
    for kk=1:cell_num
        switch assignment_mat(jj,kk)
            case 1
                weight(jj,kk,:)= ...
                    [channel_magnitudes(jj,kk,1), ...
                    channel_magnitudes(jj,kk,2)];
                assigned_depression_magnitude(jj,kk,:)= ...
                    [depression_magnitude(kk,1), ...
                    depression_magnitude(kk,2)];
                assigned_depression_tau(jj,kk,:)= ...
                    [depression_tau(kk,1), ...
                    depression_tau(kk,2)];
                assigned_depression_strength(jj,kk,:)= ...
                    [depression_strength(kk,1), ...
                    depression_strength(kk,2)];
            case 2
                weight(jj,kk,:)= ...
                    [channel_magnitudes(jj,kk,2), ...
                    channel_magnitudes(jj,kk,1)];
                assigned_depression_magnitude(jj,kk,:)= ...
                    [depression_magnitude(kk,2), ...
                    depression_magnitude(kk,1)];
                assigned_depression_tau(jj,kk,:)= ...
                    [depression_tau(kk,1), ...
                    depression_tau(kk,2)];
                assigned_depression_strength(jj,kk,:)= ...
                    [depression_strength(kk,1), ...
                    depression_strength(kk,2)];
        end
    end
end

depression=[assigned_depression_magnitude; ...
    assigned_depression_tau; ...
    assigned_depression_strength];
weight_cat=[weight;weight;weight];

% calculate magnitudes of depression change between channels
depression_info=struct('metric',metric_string, ...
    'change_in_depression',zeros(cell_num,1), ...
    'decreased_c2_depression',zeros(cell_num,1), ...
    'increased_c2_depression',zeros(cell_num,1));

for ii=1:metrics
    depression_info(ii).change_in_depression ...
        =depression(ii,:,2)-depression(ii,:,1);
    del_dep=depression_info(ii).change_in_depression;
    for jj=1:cell_num
        if ismember(jj,analysis_cells)==1
            if del_dep(jj)<0
                depression_info(ii).decreased_c2_depression(jj) ...
                    =del_dep(jj);
            elseif del_dep(jj)>0
                depression_info(ii).increased_c2_depression(jj) ...
                    =del_dep(jj);
            end
        end
    end
    depression_info(ii).decreased_c2_depression= ...
        depression_info(ii).decreased_c2_depression( ...
        depression_info(ii).decreased_c2_depression~=0);
    depression_info(ii).increased_c2_depression= ...
        depression_info(ii).increased_c2_depression( ...
        depression_info(ii).increased_c2_depression~=0);
end

% plot synaptic weights vs. depression magnitudes on a per channel basis
weight_mat=zeros(cell_num,2);
text_xpos=zeros(metrics,1);
for ii=1:metrics
    for jj=1:cell_num
        if ismember(jj,analysis_cells)==1
            weight_mat(jj,:)=weight_cat(ii,jj,:);
        else
            weight_mat(jj,:)=0;
        end
    end
    weight_mat=weight_mat(weight_mat~=0);
    if isempty(min(min(weight_mat)))==1
        text_xpos(ii)=0;
    else
        text_xpos(ii)=min(min(weight_mat));
    end
    weight_mat=zeros(cell_num,2);
end

fig=figure;
for ii=1:metrics
    m=1;
    for jj=1:cell_num
        if ismember(jj,analysis_cells)==1 && (ii==1 || ii==2)
            c1=plot(weight_cat(ii,jj,1),depression(ii,jj,1), ...
                'rd','MarkerFaceColor','r');
            if m==1
                hold on
            end
            m=m+1;
            c2=plot(weight_cat(ii,jj,2),depression(ii,jj,2), ...
                'bd','MarkerFaceColor','b');
            wt_vec=squeeze(weight_cat(ii,jj,:));
            dep_vec=squeeze(depression(ii,jj,:));
            plot(wt_vec,dep_vec,'k-');
        elseif ismember(jj,analysis_cells)==1
            c1=semilogy(weight_cat(ii,jj,1),depression(ii,jj,1), ...
                'rd','MarkerFaceColor','r');
            if m==1
                hold on
            end
            m=m+1;
            c2=semilogy(weight_cat(ii,jj,2),depression(ii,jj,2), ...
                'bd','MarkerFaceColor','b');
            wt_vec=squeeze(weight_cat(ii,jj,:));
            dep_vec=squeeze(depression(ii,jj,:));
            plot(wt_vec,dep_vec,'k-');
            
        end
    end
    
    if ii==1 || ii==3 || ii==5
        weighting='raw';
    elseif ii==2 || ii==4 || ii==6
        weighting='early';
    end
    
    if ii==1 || ii==2
        depression_type='magnitude';
    elseif ii==3 || ii==4
        depression_type='tau';
    elseif ii==5 || ii==6
        depression_type='strength';
    end
    
    title(['Depression ' depression_type ' vs. ' ...
        weighting ' synaptic weight for ' title_filter_nickname ...
        ' cells'])
    xlabel('Synaptic weight')
    ylabel('Depression strength')
    legend([c1 c2],'Ch 1','Ch 2')
    text(text_xpos(ii),.95, sprintf( ...
        '%d cells with decreased Ch 2 depression.', ...
        length(depression_info(ii).decreased_c2_depression)))
    text(text_xpos(ii),.925, sprintf( ...
        '      Average decrease=%.3f+/-%.3f, Median=%.3f.', ...
        mean(depression_info(ii).decreased_c2_depression), ...
        std(depression_info(ii).decreased_c2_depression), ...
        median(depression_info(ii).decreased_c2_depression)))
    text(text_xpos(ii),.9, sprintf( ...
        '%d cells with increased Ch 2 depression.', ...
        length(depression_info(ii).increased_c2_depression)))
    text(text_xpos(ii),.875, sprintf( ...
        '      Average increase=%.3f+/-%.3f, Median=%.3f.', ...
        mean(depression_info(ii).increased_c2_depression), ...
        std(depression_info(ii).increased_c2_depression), ...
        median(depression_info(ii).increased_c2_depression)))
    print(fig,'-djpeg','-r150',[filename '_' metric_string{ii}]);
    hold off
end

end
