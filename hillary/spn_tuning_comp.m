% tsp_tuning_comp
%
%
if ~exist('batchid','var'),
    batchid=259;
    %batchid=267;
end
if ~exist('modname2','var'),
    modname1='env100_logn_fir15_siglog100_fit05h_fit05c';
    %modname2='env100_logn_adp1pcno_fir15_siglog100_fit05h_fit05c';
    %modname2='env100_logn_wc02c_dep1pcno_fir15_siglog100_fit05h_fit05c';
    modname2='env100_logn_wc02c_adp1pcno_fir15_siglog100_fit05h_fit05c';

    %modname2='fb18ch100_lognn_wcg02_fir15_siglog100_fit05h_fit05c'
    %modname2='fb18ch100_lognn_wcg02_adp1pcno_fir15_siglog100_fit05h_fit05c'
end

narf_set_path;
dbopen;

RED=[237 41 36]./255;
BLUE=[102 204 255]./255;

cachefile=sprintf('%sspn_tuning_comp.%d.%s.mat',...
                  '/auto/data/tmp/svd/',batchid,modname2);

if ~exist('RELOAD','var') || RELOAD || ~exist(cachefile,'file'),
    
    modelnames={modname1,modname2};
    
    dbopen;
    sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid),...
         ' ORDER BY cellid'];
    celldata=mysql(sql);
    cellids={celldata.cellid};
    global NARF_PATH
    addpath([NARF_PATH filesep 'scripts']);
    
    fitset=export_fit_parameters(batchid, cellids, modelnames);
    
    cellids={fitset(:,1).cellid};
    r_test=[cat(1,fitset(:,1).r_test) cat(1,fitset(:,2).r_test)];
    r_floor=[cat(1,fitset(:,1).r_floor) cat(1,fitset(:,2).r_floor)];
    r_ceiling=[cat(1,fitset(:,1).r_ceiling) cat(1,fitset(:,2).r_ceiling)];
    
    RELOAD=0;
    
    save(cachefile,'batchid','modname1','modname2','modelnames','cellids', ...
         'fitset','r_test','r_floor','r_ceiling','celldata');
else
    fprintf('loading from cachefile %s\n',cachefile);
    load(cachefile);
end

sql=['SELECT * FROM NarfBatches where batch=',num2str(batchid)];
bdata=mysql(sql);
for ii=1:length(bdata),
    c=bdata(ii).cellid;
    jj=find(strcmp(cellids,c));
    fitset(jj,1).est_snr=bdata(ii).est_snr;
    fitset(jj,1).est_reps=bdata(ii).est_reps;
    fitset(jj,1).val_snr=bdata(ii).val_snr;
    fitset(jj,1).val_reps=bdata(ii).val_reps;
end
snr_est=cat(1,fitset(:,1).est_snr);
snr_val=cat(1,fitset(:,1).val_snr);
estreps=cat(1,fitset(:,1).est_reps);
valreps=cat(1,fitset(:,1).val_reps);

cellids={fitset(:,1).cellid};
cellcount=length(cellids);

% fitset(:,1) is behavior independent
deptau=zeros(cellcount,2);
depstr=zeros(size(deptau));
depmag=zeros(size(deptau));
firmag=zeros(cellcount,2);
firmag2=zeros(cellcount,2);
bfchan=zeros(cellcount,1);
twochan=zeros(cellcount,1);
chanfreq=zeros(cellcount,2)
for ii=1:cellcount,
    
    if isfield(fitset(ii,2),'p04depression_filter_bank_tau'),
        cc=length(fitset(ii,2).p04depression_filter_bank_tau);
        deptau(ii,1:cc)=fitset(ii,2).p04depression_filter_bank_tau;
        depstr(ii,1:cc)=fitset(ii,2).p04depression_filter_bank_strength;
        wc=eye(2);
        firmag2(ii,:)=mean(fitset(ii,2).p06fir_filter_coefs(:,1:5),2);
    elseif isfield(fitset(ii,2),'p05depression_filter_bank_tau'),
        cc=length(fitset(ii,2).p05depression_filter_bank_tau);
        deptau(ii,1:cc)=fitset(ii,2).p05depression_filter_bank_tau;
        depstr(ii,1:cc)=fitset(ii,2).p05depression_filter_bank_strength;
        wc=fitset(ii,2).p03weight_channels_weights;
        firmag2(ii,:)=mean(fitset(ii,2).p07fir_filter_coefs(:,1:5),2);
    else
        % VOC batch
        cc=length(fitset(ii,2).p06depression_filter_bank_tau);
        deptau(ii,1:cc)=fitset(ii,2).p06depression_filter_bank_tau;
        depstr(ii,1:cc)=fitset(ii,2).p06depression_filter_bank_strength;
        wc_phi=fitset(ii,2).p04weight_channels_phi;
        chanfreq(ii,:)=wc_phi(:,1)';
        firmag2(ii,:)=mean(fitset(ii,2).p08fir_filter_coefs(:,1:7),2);
    end
    tauvalid(ii,1:cc)=1;
    
    if isfield(fitset(ii,1),'p04fir_filter_coefs'),
        firmag(ii,:)=mean(fitset(ii,1).p04fir_filter_coefs(:,1:5),2);
        %firmag(ii,:)=(wc*firmag(ii,:)')';
        firmag(ii,:)=firmag(ii,:)*wc;
    else
        firmag(ii,:)=mean(fitset(ii,1).p06fir_filter_coefs(:,1:5),2);
    end
    bfchan(ii)=find(firmag(ii,:)==max(firmag(ii,:)));
    if firmag(ii,bfchan(ii))./firmag(ii,3-bfchan(ii)) <15,
        twochan(ii)=1;
    end
    
    if ~isempty(findstr('_dep',modname2)),
        %only if dep, not adp!
        depstr=abs(depstr);
    end
    deptau=abs(deptau);
    
    % depression calculations - magnitude of effect on cartoon stimulus
    fs=100;
    stim=0.75.*[zeros(fs./2,1);ones(fs,1)./2;zeros(fs,1);
             ones(round(fs./10),1);zeros(round(fs./10),1);
             ones(round(fs./10),1);zeros(7.*fs./10,1)]';
    stimmax=max(stim(:));
    tau_norm=100;
    str_norm=100;
    
    tresp=depression_bank(stim,depstr(ii,:)./ ...
                          str_norm,deptau(ii,:)./fs*tau_norm,1);
    for jj=1:size(tresp,1)
        depmag(ii,jj)=1-sum(tresp(jj,:))./sum(stim);
    end
    fprintf('cellid %s bfchan=%d (%.2f %.2f)\n',...
            cellids{ii},bfchan(ii),depmag(ii,:));
    
    if max(abs([depmag(ii,:)]))>2,
        keyboard
    end
end

figure;
subplot(3,2,1);
plot([0 1],[0 1],'k--');
ff=find(sum(r_test>r_floor.*2,2)>0);
hold on
plot(r_ceiling(ff,1),r_ceiling(ff,2),'k.');
hold off
axis tight square
mn2=strrep(modname2,'_fit05h_fit05c','');
title(mn2,'Interpreter','none');
xlabel(sprintf('mean=%.3f',mean(r_ceiling(ff,1))));
ylabel(sprintf('mean=%.3f',mean(r_ceiling(ff,2))));

%ff=find(r_test(:,2)>r_test(:,1)+0.005 & twochan);
threshold=0.01;
ff=find(snr_est.^2 .* estreps > threshold & snr_val.^2 .* valreps > threshold);
%ff=find(twochan);


ts=depstr(ff,:);
strbfe=ts(firmag2(ff,:)>0);
strbfi=ts(firmag2(ff,:)<0);

subplot(3,2,2);
x=linspace(-20,80,10)';
ne=hist(strbfe,x);
ni=hist(strbfi,x);
hb=bar(x,[ne' ni']);
set(hb(1),'FaceColor',RED);
set(hb(2),'FaceColor',BLUE);
xlabel(sprintf('stre=%.1f stri=%.1f',mean(strbfe),mean(strbfi)));
aa=axis;
axis([-30 90 aa(3:4)]);

ta=sqrt(deptau(ff,:));
taubfe=ta(firmag2(ff,:)>0);
taubfi=ta(firmag2(ff,:)<0);

subplot(3,2,4);
x=linspace(0,20,10)';
ne=hist(taubfe,x);
ni=hist(taubfi,x);
hb=bar(x,[ne' ni']);
set(hb(1),'FaceColor',RED);
set(hb(2),'FaceColor',BLUE);
xlabel(sprintf('taue=%.1f taui=%.1f',mean(taubfe.^2),mean(taubfi.^2)));
aa=axis;
axis([-1 21 aa(3:4)]);

mg=depmag(ff,:);
magbfe=mg(firmag2(ff,:)>0);
magbfi=mg(firmag2(ff,:)<0);

subplot(3,2,6);
x=linspace(-0.1,1,10)';
ne=hist(magbfe,x);
ni=hist(magbfi,x);
hb=bar(x,[ne' ni']);
set(hb(1),'FaceColor',RED);
set(hb(2),'FaceColor',BLUE);
xlabel(sprintf('mage=%.2f magi=%.2f',mean(magbfe),mean(magbfi)));
aa=axis;
axis([-.2 1.1 aa(3:4)]);

tsf=mean([taubfe;taubfi]);
subplot(3,2,3);
mm=[mean(taubfe).*tsf mean(taubfi).*tsf;
    mean(strbfe) mean(strbfi);
    mean(magbfe).*100 mean(magbfi).*100];
hb=bar(mm);
set(hb(1),'FaceColor',RED);
set(hb(2),'FaceColor',BLUE);
hold on
os=0.1428;
plot(1-os+randn(size(taubfe))./50,taubfe.*tsf,'k.');
plot(1+os+randn(size(taubfi))./50,taubfi.*tsf,'k.');
plot(2-os+randn(size(strbfe))./50,strbfe,'k.');
plot(2+os+randn(size(strbfi))./50,strbfi,'k.');
plot(3-os+randn(size(magbfe))./50,magbfe.*100,'k.');
plot(3+os+randn(size(magbfi))./50,magbfi.*100,'k.');
hold off

subplot(3,2,5);
magbfei=[depmag(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),1);
         depmag(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),2)];
magoffbfei=[depmag(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),2);
            depmag(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),1)];
magbfei=[depstr(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),1);
         depstr(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),2)]./100;
magoffbfei=[depstr(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),2);
            depstr(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),1)]./100;
plot([-0.5 1],[-0.5 1],'k--');
hold on
plot(magoffbfei,magbfei,'k.');
hold off
axis tight square
xlabel(sprintf('offbfei=%.2f',mean(magoffbfei)));
ylabel(sprintf('bfei=%.2f',mean(magbfei)));
title(sprintf('bf>offbf: %d/%d',sum(magbfei>magoffbfei),length(magbfei)));

fullpage portrait

if ismember(batchid,[137 246 267 271]), % VOC/NAT batches
    figure
    
    if batchid==267,
        gg=((chanfreq(:,1)>1.1 & chanfreq(:,1)<2) |...
            (chanfreq(:,2)>1.1 & chanfreq(:,2)<2) |...
            (chanfreq(:,1)>4.5 & chanfreq(:,1)<7) |...
            (chanfreq(:,2)>4.5 & chanfreq(:,2)<7)) .* ...
           (rand(length(chanfreq),1)>1);
    elseif batchid==246,
        gg=((chanfreq(:,1)>1.1 & chanfreq(:,1)<1.6) |...
            (chanfreq(:,2)>1.1 & chanfreq(:,2)<1.6)) .* ...
           (rand(length(chanfreq),1)>0.6);
    else
        gg=zeros(length(chanfreq),1);
    end
    %ff=find(twochan);
    ff=find(twochan & sum(r_test>r_floor.*2,2)>0 & ...
            chanfreq(:,1)>0 & chanfreq(:,2)>0 & ~gg);
    ff=find(snr_est.^2 .* estreps > threshold & ...
            snr_val.^2 .* valreps > threshold & ~gg);
    
    magbfei=[depmag(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),1);
             depmag(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),2)];
    magoffbfei=[depmag(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),2);
                depmag(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),1)];
    bfei=[chanfreq(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),1);
          chanfreq(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),2)];
    offbfei=[chanfreq(ff(firmag2(ff,1)>0 & firmag2(ff,2)<0),2);
             chanfreq(ff(firmag2(ff,1)<0 & firmag2(ff,2)>0),1)];
    bfee=[chanfreq(ff(firmag2(ff,1)>firmag2(ff,2) & firmag2(ff,2)>0),1);
          chanfreq(ff(firmag2(ff,1)>0 & firmag2(ff,2)>firmag2(ff,1)),2)];
    offbfee=[chanfreq(ff(firmag2(ff,1)>firmag2(ff,2) & firmag2(ff,2)>0),2);
          chanfreq(ff(firmag2(ff,1)>0 & firmag2(ff,2)>firmag2(ff,1)),1)];
    
    fe=[chanfreq(ff(firmag2(ff,1)>0),1);
        chanfreq(ff(firmag2(ff,2)>0),2)];
    fi=[chanfreq(ff(firmag2(ff,1)<0),1);
        chanfreq(ff(firmag2(ff,2)<0),2)];
    mage=[depmag(ff(firmag2(ff,1)>0),1);
          depmag(ff(firmag2(ff,2)>0),2)];
    magi=[depmag(ff(firmag2(ff,1)<0),1);
          depmag(ff(firmag2(ff,2)<0),2)];
    
    lbfei=log2(abs(bfei)+0.1);
    loffbfei=log2(abs(offbfei)+0.1);
    octdist=lbfei-loffbfei;
    lbfee=log2(abs(bfee)+0.1);
    loffbfee=log2(abs(offbfee)+0.1);
    lfe=log2(abs(fe)+0.1);
    lfi=log2(abs(fi)+0.1);
    
    subplot(3,2,1);
    plot([-5 5],[-5 5],'k--');
    hold on
    plot(lbfei,loffbfei,'k.');
    hold off
    axis tight square
    
    subplot(3,2,2);
    plot([-5 5],[-5 5],'k--');
    hold on
    plot(lbfee,loffbfee,'k.');
    hold off
    axis tight square
    
    %subplot(2,2,2);
    %hist(octdist);
    
    subplot(3,2,3);
    f=linspace(-1.5,5,16);
    ne=hist(lbfei,f);
    ni=hist(loffbfei,f);
    hb=bar(f(:),[ne(:) ni(:)],'stacked');
    set(hb(1),'FaceColor',RED);
    set(hb(2),'FaceColor',BLUE);
    aa=axis;
    axis([-2 5 aa(3:4)]);
    xx=[-2 0 2 4];
    set(gca,'XTick',xx);
    set(gca,'XTickLabel',2.^xx);
   
    subplot(3,2,4);
    ne=hist(lfe,f);
    ni=hist(lfi,f);
    hb=bar(f(:),[ne(:) ni(:)],'stacked');
    set(hb(1),'FaceColor',RED);
    set(hb(2),'FaceColor',BLUE);
    aa=axis;
    axis([-2 5 aa(3:4)]);
    xx=[-2 0 2 4];
    set(gca,'XTick',xx);
    set(gca,'XTickLabel',2.^xx);
    
    thr=0.35;
    if batchid==137,
        smth=25;
        bfdim=150;
    else
        smth=60;
        bfdim=200;
    end
    
    subplot(3,2,5);
    plot(lbfei,magbfei,'.','Color',RED);
    hold on
    plot(loffbfei,magoffbfei,'.','Color',BLUE);
    [F1,ctrs1,ctrs2]=smoothhist2D([lbfei,magbfei],smth,[bfdim 100]);
    contour(ctrs1,ctrs2,F1,[1 1].*thr.*max(F1(:)),'LineColor',RED);
    [F2,ctrs1,ctrs2]=smoothhist2D([loffbfei,magoffbfei],smth,[bfdim 100]);
    contour(ctrs1,ctrs2,F2,[1 1].*thr.*max(F1(:)),'LineColor',BLUE);
    hold off
    %axis([-2 5 -.2 0.7]);
    xx=[-2 0 2 4];
    set(gca,'XTick',xx);
    set(gca,'XTickLabel',2.^xx);
    
    subplot(3,2,6);
    plot(lfe,mage,'.','Color',RED);
    hold on
    plot(lfi,magi,'.','Color',BLUE);
    [F1,ctrs1,ctrs2]=smoothhist2D([lfe,mage],smth,[bfdim 100]);
    contour(ctrs1,ctrs2,F1,[1 1].*thr.*max(F1(:)),'LineColor',RED);
    [F2,ctrs1,ctrs2]=smoothhist2D([lfi,magi],smth,[bfdim 100]);
    contour(ctrs1,ctrs2,F2,[1 1].*thr.*max(F2(:)),'LineColor',BLUE);
    hold off
    axis([-2 5 -.2 0.7]);
    xx=[-2 0 2 4];
    set(gca,'XTick',xx);
    set(gca,'XTickLabel',2.^xx);
    
    fullpage portrait
end

