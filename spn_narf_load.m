function [all_data,by_cell]=spn_narf_load(cellid,batch,modelname,modelnickname)

% function [all_data,by_cell]=spn_narf_load(cellid,batch,modelname)
%
% Generates structures containing cell information for the indicated model.
% Use '%' as a wildcard for cellid to obtain information for all cells in
% the batch.
%
% Inputs:
%  cellid - baphy cellid or search string
%  batch - baphy/narf data set number
%  modelname - narf model name
%  modelnickname - string specifying shorthand name for model,
%    e.g., 'ac' or 'wcpcno' or 'adp2'
%
% Output structures contain the following fields:
%   Raw channel magnitude
%   Unsigned channel magnitude
%   Signed channel magnitude
%   Early latency channel magnitude
%   Best raw channel (1 or 2)
%   Best signed channel (1 or 2)
%   Best unsigned channel (1 or 2)
%   Best early channel (1 or 2)
%   Depression strength
%   Depression tau
%   Depression response
%   Depression magnitude
%   Depression impact
%   Depression crosstalk
%   r_test
%   r_floor
%   r_ceiling
% Where all_data contains matrices housing all cell data, and by_cell
% indexes the information on a per-cell basis.
% 
% Modified from narf_load_demo, ZPS 8/23/15

global STACK XXX

sql=['SELECT * FROM NarfResults WHERE cellid like "',cellid,'"',...
    ' AND batch=',num2str(batch),...
    ' AND modelname like "',modelname,'" ORDER BY cellid,modelname'];
narfdata=mysql(sql);

if isempty(narfdata),
    error('no (cellid,batch,modelname) match in NarfResults');
end

% preallocate variables
channel_unsigned_magnitude=zeros(length(narfdata),2);
channel_signed_magnitude=zeros(length(narfdata),2);
channel_raw_magnitude=zeros(length(narfdata),2);
channel_early_magnitude=zeros(length(narfdata),2);
gains=zeros(length(narfdata),2);
dep_impact=zeros(length(narfdata),2);
best_unsigned=zeros(length(narfdata),1);
best_signed=zeros(length(narfdata),1);
best_raw=zeros(length(narfdata),1);
best_early=zeros(length(narfdata),1);
best_gains=zeros(length(narfdata),1);
delays=zeros(length(narfdata),2);
r_test=zeros(length(narfdata),1);
r_floor=zeros(length(narfdata),1);
r_ceiling=zeros(length(narfdata),1);
fir_coefs=zeros(2,15,length(narfdata));
avg_fit_psth=zeros(400,1,length(narfdata));
avg_test_psth=zeros(400,2,length(narfdata));
crosstalk=zeros(length(narfdata),1);

for ii=1:length(narfdata)
    
    modelpath=char(narfdata(ii).modelpath);
    load_model(modelpath);
    update_xxx(1);
    
    training_file=XXX{1}.training_set{1};
    test_file=XXX{1}.test_set{1};

    %calculate average psth of fit and test trials
    avg_fit_psth(:,:,ii)=nanmean(XXX{end}.dat.(training_file).resp(:,:),2);
    avg_test_psth(:,:,ii)=squeeze(nanmean(XXX{end}.dat.(test_file).resp(:,:,:),3));

    % extract information about depression
    [mod,modidx]=find_modules(STACK,'depression_filter_bank');
    if size(mod,1)==0 && size(modidx,1)==0
        
        if ii==1
            depstr=zeros(length(narfdata),1);
            deptau=zeros(length(narfdata),1);
            tresp=zeros(1,350,length(narfdata));
            depmag=zeros(length(narfdata),1);
        end
        
        % get model performance stats
        r_test(ii)=XXX{end}.score_test_corr;
        r_floor(ii)=XXX{end}.score_test_floorcorr;
        r_ceiling(ii)=XXX{end}.score_test_ceilingcorr;
        
    else
        
        modidx=modidx{1};
        mod=mod{1}{1};

	%crosstalk
	crosstalk(ii)=mod.crosstalk;

	%cartoon ssa stimulus
	fs=100;           %sampling rate (hz)
	dur=0.1;          %noise burst duration (s)
	isi=0.3;          %interstimulus interval (s)
	common=[repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 8]) ...
	  zeros(1, isi*fs) zeros(1, dur*fs) ...
	  repmat([zeros(1, isi*fs) ones(1, dur*fs)], [1 1]) ...
	  0];
	rare=[repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 8]) ...
	  zeros(1, isi*fs) ones(1, dur*fs) ...
	  repmat([zeros(1, isi*fs) zeros(1, dur*fs)], [1 1]) ...
	  0];
	stim=[common rare];
	
        if ii==1
            depstr=zeros(length(narfdata), size(mod.strength,1), ...
	                                   size(mod.strength,2));
            deptau=zeros(size(depstr));
            tresp=zeros(size(depstr,3),length(stim),length(narfdata));
            depmag=zeros(length(narfdata),size(depstr,3));
        end
        
        if mod.facil_on
            depstr(ii,:,:)=mod.strength;
            deptau(ii,:,:)=mod.tau;
        else
            depstr(ii,:,:)=abs(mod.strength);
            deptau(ii,:,:)=abs(mod.tau);
        end
	
	%depression calculations - magnitude of effect on cartoon SSA stimulus
	tau_norm = fs/100;
	str_norm = max(stim(:))*fs;
	[synapses,channels]=size(mod.tau);
	for c=1:channels
	  u=depstr(ii,:,c)./str_norm;
	  tau=deptau(ii,:,c).*tau_norm;
	  tresp(c,:)=adp_bank(stim, u', tau');
	end
        for jj=1:size(tresp,1)
            depmag(ii,jj)=1-sum(tresp(jj,:,ii))./sum(stim);
        end
	
        % calculate depression impact - magnitude of effect on real stimulus
        dep_input_stim=XXX{modidx}.dat.(training_file).stim;
        dep_output_stim=XXX{modidx+1}.dat.(training_file).stim;
        [T,S,C]=size(dep_input_stim);
        dep_input_stim=reshape(dep_input_stim,T*S,C);
        offset=repmat(mod.offset_in,T*S,1);
        
        if size(dep_input_stim,2)~=size(offset,2)
            offset=repmat(offset,1,C);
        end
        
        dep_input_stim=dep_input_stim-offset;
        dep_input_stim=dep_input_stim.*(dep_input_stim>0);
        dep_output_stim=reshape(dep_output_stim,T*S,C);
        dep_impact(ii,:)=mean(dep_input_stim- ...
            dep_output_stim)./mean(dep_input_stim);
        
        % extract information about FIR filter
        [mod,modidx]=find_modules(STACK,'fir_filter');
        
        if size(mod,1)==0 && size(modidx,1)==0
            
            [mod,modidx]=find_modules(STACK,'pole_zeros');
            mod=mod{1}{1};
            gains(ii,:)=mod.gains;
            if gains(ii,1)> ...
                    gains(ii,2)
                best_gains(ii)=1;
            else
                best_gains(ii)=2;
            end
            delays(ii,:)=mod.delays;
            
        else
            
            modidx=modidx{1};
            mod=mod{1}{1};
            training_file=XXX{modidx}.training_set{1};
            fir_coefs(:,:,ii)=mod.coefs;
            early_latency_fir_coefs=fir_coefs(:,1:5,ii);
            
            input_stim=XXX{2}.dat.(training_file).stim;
            fir_output_pc=XXX{modidx+1}.dat.(training_file).stim_filtered;
            fir_output=XXX{modidx+1}.dat.(training_file).stim;
            
            % find pre/post silence
            any_stim=nanmean(nanmean(input_stim,2),3);
            non_zero_bins=find(any_stim);
            
            % only look at periods where stimulus is playing
            fir_output_pc_nonzero=fir_output_pc(non_zero_bins,:,:);
            fir_output_nonzero=fir_output(non_zero_bins,:,:);
            [T,S,C]=size(fir_output_pc_nonzero);
            fir_output_pc_nonzero=reshape(fir_output_pc_nonzero,T*S,C);
            fir_output_nonzero=reshape(fir_output_nonzero,T*S,1);
            
            channel_unsigned_magnitude(ii,:)= ...
                std(fir_output_pc_nonzero)./std(fir_output_nonzero);
            channel_signed_magnitude(ii,:)= ...
                mean(fir_output_pc_nonzero)./std(fir_output_nonzero);
            % abs(mean(fir_output_nonzero));
            channel_raw_magnitude(ii,:)= ...
                mean(fir_coefs(:,:,ii),2)';
            % /std(fir_coefs(:))
            channel_early_magnitude(ii,:)= ...
                mean(early_latency_fir_coefs,2)';
            
            % evaluate best channel for each weighting
            if channel_unsigned_magnitude(ii,1)> ...
                    channel_unsigned_magnitude(ii,2)
                best_unsigned(ii)=1;
            else
                best_unsigned(ii)=2;
            end
            
            if channel_signed_magnitude(ii,1)> ...
                    channel_signed_magnitude(ii,2)
                best_signed(ii)=1;
            else
                best_signed(ii)=2;
            end
            
            if channel_raw_magnitude(ii,1)> ...
                    channel_raw_magnitude(ii,2)
                best_raw(ii)=1;
            else
                best_raw(ii)=2;
            end
            
            if channel_early_magnitude(ii,1)> ...
                    channel_early_magnitude(ii,2)
                best_early(ii)=1;
            else
                best_early(ii)=2;
            end
        end
        
        % get model performance stats
        r_test(ii)=XXX{end}.score_test_corr;
        r_floor(ii)=XXX{end}.score_test_floorcorr;
        r_ceiling(ii)=XXX{end}.score_test_ceilingcorr;
        
    end
end

depstr=squeeze(depstr);
deptau=squeeze(deptau);
depmag=squeeze(depmag);

by_cell=struct( ...
    'cellid',zeros(length(narfdata)), ...
    'fir_coefs',zeros(2,15,length(narfdata)), ...
    'raw_channel_magnitude',zeros(length(channel_raw_magnitude)), ...
    'signed_channel_magnitude',zeros(length(channel_signed_magnitude)), ...
    'unsigned_channel_magnitude',zeros(length(channel_unsigned_magnitude)), ...
    'early_channel_magnitude',zeros(length(channel_early_magnitude)), ...
    'gains',zeros(length(gains)), ...
    'best_raw',zeros(length(best_raw)), ...
    'best_signed',zeros(length(best_signed)), ...
    'best_unsigned',zeros(length(best_unsigned)), ...
    'best_early',zeros(length(best_early)), ...
    'best_gains',zeros(length(best_gains)), ...
    'delays',zeros(length(delays)), ...
    'depression_strength',zeros(size(depstr)), ...
    'depression_tau',zeros(size(deptau)), ...
    'depression_response',zeros(size(tresp,1)), ...
    'depression_magnitude',zeros(length(depmag)), ...
    'depression_impact',zeros(length(dep_impact)), ...
    'r_test',zeros(length(narfdata)), ...
    'r_floor',zeros(length(narfdata)), ...
    'r_ceiling',zeros(length(narfdata)), ...
    'avg_fit_psth',zeros(400,1,length(narfdata)), ...
    'avg_test_psth',zeros(400,2,length(narfdata)), ...
    'crosstalk',zeros(length(narfdata)));

for ii=1:length(narfdata)
    by_cell(ii).cellid=narfdata(ii).cellid;
    by_cell(ii).fir_coefs=fir_coefs(:,:,ii);
    by_cell(ii).raw_channel_magnitude=channel_raw_magnitude(ii,:);
    by_cell(ii).signed_channel_magnitude=channel_signed_magnitude(ii,:);
    by_cell(ii).unsigned_channel_magnitude=channel_unsigned_magnitude(ii,:);
    by_cell(ii).early_channel_magnitude=channel_early_magnitude(ii,:);
    by_cell(ii).gains=gains(ii,:);
    by_cell(ii).best_raw=best_raw(ii);
    by_cell(ii).best_signed=best_signed(ii);
    by_cell(ii).best_unsigned=best_unsigned(ii);
    by_cell(ii).best_early=best_early(ii);
    by_cell(ii).best_gains=best_gains(ii);
    by_cell(ii).delays=delays(ii,:);
    by_cell(ii).depression_strength=depstr(:,:,ii);
    by_cell(ii).depression_tau=deptau(:,:,ii);
    by_cell(ii).depression_response=tresp(:,:,ii);
    by_cell(ii).depression_magnitude=depmag(ii,:);
    by_cell(ii).depression_impact=dep_impact(ii,:);
    by_cell(ii).r_test=r_test(ii);
    by_cell(ii).r_floor=r_floor(ii);
    by_cell(ii).r_ceiling=r_ceiling(ii);
    by_cell(ii).avg_fit_psth=avg_fit_psth(:,:,ii);
    by_cell(ii).avg_test_psth=avg_test_psth(:,:,ii);
    by_cell(ii).crosstalk=crosstalk(ii);
end

all_data=struct( ...
    'model_nickname',{modelnickname}, ...
    'fir_coefs',{fir_coefs}, ...
    'raw_channel_magnitude',{channel_raw_magnitude}, ...
    'signed_channel_magnitude',{channel_signed_magnitude}, ...
    'unsigned_channel_magnitude',{channel_unsigned_magnitude}, ...
    'early_channel_magnitude',{channel_early_magnitude}, ...
    'gains',{gains}, ...
    'best_raw',{best_raw}, ...
    'best_signed',{best_signed}, ...
    'best_unsigned',{best_unsigned}, ...
    'best_early',{best_early}, ...
    'best_gains',{best_gains}, ...
    'delays',{delays}, ...
    'depression_strength',{depstr}, ...
    'depression_tau',{deptau}, ...
    'depression_response',{tresp}, ...
    'depression_magnitude',{depmag}, ...
    'depression_impact',{dep_impact}, ...
    'r_test',{r_test}, ...
    'r_floor',{r_floor}, ...
    'r_ceiling',{r_ceiling}, ...
    'avg_fit_psth',{avg_fit_psth},...
    'avg_test_psth',{avg_test_psth},...
    'crosstalk',{crosstalk});
end
