% function [pred1,pred2,predboth,resp,meta]=RDT_pred_stream_from_modelpath(modelpath);
%
% modelpath:  e.g., modelpath column from SQL query on NarfResults
%
% returns:  pred1
%               predicted response to stream 1 only (includes both target and
%               reference phase of each trial)
%           pred2
%               predicted response to stream 2 only. Zero if single-stream trial
%           resp
%               Actual response (averaged across all repetitions for that
%               stream)
%           meta
%               Information regarding the model fit
%           r0
%               Baseline firing rate (full prediction should be pred1+pred2+r0)

function [pred1,pred2,predboth,resp,meta,r0]=RDT_pred_stream_from_modelpath(modelpath);

load_model(modelpath);

global STACK XXX FLATXXX META
FLATXXX=XXX;

if ismember(META.batch,[281 282]),
    % special stuff for fits with RandSingle files
    if length(XXX{1}.training_set)>1,
        matched=0;
        for ii=1:length(XXX{1}.training_set),
            if ~matched && isempty(findstr(XXX{1}.training_set{ii},'_est')),
                XXX{1}.test_set={XXX{1}.training_set{ii}};
                XXX{1}.training_set{ii}=[XXX{1}.training_set{ii} '_all'];
                matched=1;
            end
        end
    else
        XXX{1}.test_set{1}=strrep(XXX{1}.test_set{1},'_val','');
    end
else
    XXX{1}.test_set{1}=strrep(XXX{1}.test_set{1},'_val','');
end
f=XXX{1}.test_set{1};
fe=XXX{1}.training_set{1};

zpad=14;

% Load combined predictions for streams
STACK{1}{1}.zpad=zpad;
STACK{1}{1}.stream=6;
STACK{1}{1}.include_prestim=1;
XXX=XXX(1);
FLATXXX=FLATXXX(1);
update_xxx(1);
predboth=XXX{end}.dat.(f).stim((zpad+1):end,:,:);
meta.score_test_corr = XXX{end}.score_test_corr;
meta.score_test_floorcorr = XXX{end}.score_test_floorcorr;

% load stream 1 pred only
STACK{1}{1}.zpad=zpad;
STACK{1}{1}.stream=4;
STACK{1}{1}.include_prestim=1;
XXX=XXX(1);
FLATXXX=FLATXXX(1);
update_xxx(1);
pred1=XXX{end}.dat.(f).stim((zpad+1):end,:,:);

% load stream 2 pred only
STACK{1}{1}.stream=5;
STACK{1}{1}.include_prestim=1;
XXX=XXX(1);
FLATXXX=FLATXXX(1);
update_xxx(1);
pred2=XXX{end}.dat.(f).stim((zpad+1):end,:,:);

resp = XXX{end}.dat.(f).resp((zpad+1):end,:,:);

meta.experiment = XXX{1}.test_set{1};
meta.trial_to_sequence = XXX{end}.dat.(f).trial2sequence;
