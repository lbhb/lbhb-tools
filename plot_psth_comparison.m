function [ph,rate]=plot_psth_comparison(mfiles,ah,options)
options.psth=1;
options.raster=0;
options.colors=getparm(options,'colors',[0 0 0; 1 0 0]);
for i=1:length(mfiles)
     [r{i},tags{i},~,~,op_out]=raster_load(mfiles{i},options.channel,options.unit,options);
     t=(0:(size(r{i},1)-1))./op_out.rasterfs*1000;
     rate(i,:)=mean(r{i},2)*op_out.rasterfs;
     rate_serr(i,:)=nanstd(r{i},[],2)./sqrt(sum(all(~isnan(r{i}),1)))*op_out.psthfs;
     axes(ah);
     [ph(i,1),ph(i,2)]=errorshade(t,rate(i,:),rate_serr(i,:),options.colors(i,:),options.colors(i,:));
     set(ph(i,2),'EdgeColor','none','FaceAlpha',.3);
end