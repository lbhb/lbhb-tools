function [h,LegendLabels]=plot_cum_rth(exptparams,globalparams,ax)
hold(ax,'on')
BinSize = 0.04;
TrialIndex=find(arrayfun(@(x)length(x.FalseAlarm),exptparams.Performance)==2,1)-1;
MaxBinTime=nanmax([exptparams.FirstLick.Tar exptparams.FirstLick.Tar])+BinSize;
PLOT_CUMLICK=1;
if isfield(exptparams,'UniqueTargets') && length(exptparams.UniqueTargets)>1
  targetid={exptparams.Performance(1:TrialIndex).ThisTargetNote};
  
  % if multiple different targets (eg, catch trials) plot first lick
  % histograms for each separately.
  FAcount=sum(cat(1,exptparams.Performance(1:TrialIndex).FalseAlarm));
  FAR=FAcount./TrialIndex;
  UniqueCount=length(exptparams.UniqueTargets);
  HR=zeros(UniqueCount,1);
  RT=zeros(UniqueCount,1);
  colormtx=jet;
  colormtx=colormtx(round(linspace(1,64,length(exptparams.UniqueTargets))),:);
  for jj=1:UniqueCount,
    thistargetii=find(strcmp(targetid,exptparams.UniqueTargets{jj}));
    thisFAcount=sum(cat(1,exptparams.Performance(thistargetii).FalseAlarm));
    if TrialIndex > thisFAcount,
        HR(jj)=sum(cat(1,exptparams.Performance(thistargetii).Hit))./...
               (length(thistargetii)-thisFAcount);
    end
    RT(jj)=nanmean(exptparams.FirstLick.Tar(thistargetii));
    DI=exptparams.Performance(end).uDiscriminationIndex;
    
    h1=hist(exptparams.FirstLick.Tar(thistargetii),0:BinSize:MaxBinTime);
    if ~isempty(h1)
       % normalize so it becomes the probability of lick
       h1=h1/sum(h1).*HR(jj); 
       if PLOT_CUMLICK, h1=cumsum(h1); end
       h(jj)=stairs(0:BinSize:MaxBinTime,h1,'color',colormtx(jj,:),'linewidth',2,'Parent',ax);
    end
    
    LegendLabels{jj}=sprintf('%d dB(HR:%.2f RT:%.2f DI:%d n:%d)',...
                             exptparams.TrialObject.RelativeTarRefdB(jj),...
                             HR(jj),RT(jj),DI(jj),...
                             length(thistargetii)-thisFAcount);
  end
  LegendLabels{end+1}=sprintf('Ref(FAR:%.2f)',FAR);
else
  h1=hist(exptparams.FirstLick.Tar(1:TrialIndex),0:BinSize:MaxBinTime);
  HitOrMissCount=sum(cat(1,exptparams.Performance(1:TrialIndex).Hit) | cat(1,exptparams.Performance(1:TrialIndex).Miss));
  if ~isempty(h1)
    h1=h1/HitOrMissCount; % normalize to make h probability of lick
    if PLOT_CUMLICK, h1=cumsum(h1); end
    h1=stairs(0:BinSize:MaxBinTime,h1,'color',[1 .5 .5],'linewidth',2,'Parent',ax);
    hold on;
  end
  LegendLabels={'Tar','Ref'};
end
ct=cat(1,exptparams.Performance(1:TrialIndex).FirstCatchTime);
fct=find(~isnan(ct) & ct<cat(1,exptparams.Performance(1:TrialIndex).FirstLickTime));
if ~isempty(fct),
    h1=hist(exptparams.FirstLick.Catch(fct),0:BinSize:MaxBinTime);
    
    if ~isempty(h1)
        h1=h1/length(fct); % normalize to convert to probability
        if PLOT_CUMLICK, h1=cumsum(h1); end
        stairs(0:BinSize:MaxBinTime,h1,'color',[.5 .5 .5],'linewidth',2);
        RT=nanmean(exptparams.FirstLick.Catch(fct));
        HR=sum(~isnan(exptparams.FirstLick.Catch(fct)))/length(fct);
        LegendLabels{end+1}=LegendLabels{end};
        LegendLabels{end-1}=sprintf('Catch(HR:%.2f RT:%.2f DI:%.0f n:%d)',...
            HR,RT,exptparams.Performance(end).cDiscriminationIndex,length(fct));
    end
end
%inter-reference interval
switch globalparams.displayparams.cum_rt_FA_histogram_style
    case 're_ref'
        PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
        PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;
        Duration=exptparams.TrialObject.ReferenceHandle.Duration;
        IRI=PreStimSilence+PostStimSilence+Duration;
        times=exptparams.FirstLick.Ref(1:TrialIndex)-PreStimSilence;
        lick_times_re_refs=mod(times,IRI);
        lick_times_re_refs(times<0)=times(times<0);        
        MaxBinTime_Ref=IRI;
        Ref_bins=-PreStimSilence:BinSize:MaxBinTime_Ref;
        h1=hist(lick_times_re_refs,Ref_bins);
    case 're_trial'
        MaxBinTime_Ref=MaxBinTime;
        Ref_bins=0:BinSize:MaxBinTime_Ref;
        h1=hist(exptparams.FirstLick.Ref(1:TrialIndex),Ref_bins);        
    otherwise
        error('globalparams.displayparams.cum_rt_FA_histogram_style must be ''re_ref'' or ''re_trial''')
end
if ~isempty(h1),
    switch globalparams.displayparams.cum_rt_FA_histogram_style
        case 're_ref'
            %norm=sum([exptparams.Performance(1:TrialIndex).FalseAlarm]);
            norm=length(exptparams.FirstLick.Ref);
        case 're_trial'
            norm=length(exptparams.FirstLick.Ref);
    end
    h1=h1/norm; % normalize to convert to probability
    if PLOT_CUMLICK, h1=cumsum(h1); end
    h(end+1)=stairs(Ref_bins,h1,'color',[.1 .5 .1],'linewidth',2,'Parent',ax);
    lh=legend(h,LegendLabels,'Location','SE');
    %LegPos = get(h,'position');
    set(lh,'fontsize',8);
    xlim=get(ax,'Xlim');
    ylim=get(ax,'Ylim');
    %LegPos(1:2) = [0.005 ylim(2)]; % put the legend on the far left of the screen
    %LegPos(1:2) = [0.4 0.425]; % put the legend on the far left of the screen
    %set(h,'position', LegPos);
    
end
set(ax,'YLim',[0 1]);
switch globalparams.displayparams.cum_rt_FA_histogram_style
    case 're_ref'
        set(ax,'XLim',[0 max([IRI,exptparams.BehaveObject.ResponseWindow])]) % exptparams.BehaveObject.ResponseWindow+exptparams.BehaveObject.EarlyWindow??
end
if PLOT_CUMLICK, 
    title(ax,'Cumulative RT Histogram');
else
    title(ax,'RT Histogram');
end
xlabel(ax,'Time (seconds)');