clear all;
day=5;
switch day
    case 1
        hard_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_09_PTD_2.m';%-12 -7, .8 .2
        %easy_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_09_PTD_2.m'; %not many trial (stopped to record)
        easy_files{1}='Z:\Babybell\bbl005\bbl005a02_a_PTD.m';
        hard_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_09_PTD_3.m';
        easy_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_09_PTD_4.m';
    case 2
        hard_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_12_PTD_4.m';
        easy_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_12_PTD_5.m';
        hard_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_12_PTD_6.m';
        easy_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_12_PTD_7.m';
    case 3
        hard_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_19_PTD_5.m';
        easy_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_19_PTD_6.m';
        hard_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_19_PTD_7.m';
        easy_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_19_PTD_8.m';   
    case 4
        easy_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_20_PTD_5.m';
        easy_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_19_PTD_6.m';  
        easy_files{3}='H:\daq\Babybell\training2016\Babybell_2016_12_20_PTD_7.m';   
        hard_files={};
    case 5
       easy_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_21_PTD_6.m';
       hard_files{1}='H:\daq\Babybell\training2016\Babybell_2016_12_21_PTD_7.m'; 
       easy_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_21_PTD_8.m';
       hard_files{2}='H:\daq\Babybell\training2016\Babybell_2016_12_21_PTD_9.m'; 
       easy_files{3}='H:\daq\Babybell\training2016\Babybell_2016_12_21_PTD_10.m';
end
nc=max(length(hard_files),length(easy_files));
Maximize(figure);ax=subplot1(2,nc,'Gap',[.02 .05]);

for i=1:length(easy_files)
LoadMFile(easy_files{i})
globalparams.displayparams.cum_rt_FA_histogram_style='re_ref';
[he(i,:),le{i}]=plot_cum_rth(exptparams,globalparams,ax(i));
title(ax(i),easy_files{i})
end

for i=1:length(hard_files)
LoadMFile(hard_files{i})
globalparams.displayparams.cum_rt_FA_histogram_style='re_ref';
[hh(i,:),lh{i}]=plot_cum_rth(exptparams,globalparams,ax(nc+i));
title(ax(nc+i),hard_files{i})
end
ylabel(ax(1),'Easy')
ylabel(ax(nc+1),'Hard')

line_inds={2:3,[1 3]};
if(isempty(hard_files))
    hh=zeros(0,3);
    lh={};
end
for j=1:length(line_inds)
h_compare=[he(:,line_inds{j});hh(:,line_inds{j})];
colormtx=jet;
colormtx=colormtx(round(linspace(1,64,size(h_compare,1))),:);
type=[ones(length(easy_files),1);2*ones(length(hard_files),1)];
files=[easy_files,hard_files];
l=[le,lh];
for i=1:length(files)
   f=strsep(files{i},'_');
   if(length(f)==3)
       rn(i)=str2double(f{1}(end-1:end));
   else
       rn(i)=str2double(f{end}(1:end-2));
   end
end
[rn,si]=sort(rn);
h_compare=h_compare(si,:);
type=type(si);
l=l(si);
figure;ax=axes;hold(ax,'on')
for i=1:size(h_compare,1)
    h(i)=stairs(get(h_compare(i,1),'XData'),get(h_compare(i,1),'YData'),'color',colormtx(i,:),'linewidth',2,'Parent',ax);
    hfa(i)=stairs(get(h_compare(i,2),'XData'),get(h_compare(i,2),'YData'),'--','color',colormtx(i,:),'linewidth',2,'Parent',ax);
    switch type(i)
        case 1
            st='Easy';
        case 2
            st='Hard';
    end
    st=[st,' R ',num2str(rn(i))];
    lt{i}=[st,' ',l{i}{line_inds{j}(1)}];
    lt_fa{i}=[st,' ',l{i}{line_inds{j}(2)}];
end
ylim([0 1])
legend([h,hfa],[lt,lt_fa],'Location','best')

end