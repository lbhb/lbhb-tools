function plot_false_alarm_histogram(parmfile,varargin)
[pathname,basename]=fileparts(parmfile);
LoadMFile(parmfile);

trial_window=[];
if(~isempty(varargin))
    trial_window=varargin{1};
end
if(isempty(trial_window))
    trial_window=1:length(exptparams.FirstLick.Ref);
end
exptevents(~ismember([exptevents.Trial],trial_window))=[];
exptparams.FirstLick.Ref=exptparams.FirstLick.Ref(trial_window);
for i=1:length(trial_window)
    in_trial=arrayfun(@(x)x.Trial==trial_window(i),exptevents);
    n_ref(i)=sum(arrayfun(@(x)~isempty(strfind(x.Note,'Stim ')) && ~isempty(strfind(x.Note,'Reference')),exptevents(in_trial)));
end
bins=[0:.2:ceil(max(exptparams.FirstLick.Ref))];
binsCH=[0:.05:ceil(max(exptparams.FirstLick.Ref))];
refs=unique(n_ref);
refs(refs==1)=[];
figure('Color','w','Position',[107   224   512   749]);
N=nan(length(refs),length(bins));
N_CH=nan(length(refs),length(binsCH));
for i=1:length(refs)
    ax(i)=subplot(length(refs)+1,1,i);
    hold on;
    N(i,:)=histc(exptparams.FirstLick.Ref(n_ref==refs(i)),bins);
    N_CH(i,:)=histc(exptparams.FirstLick.Ref(n_ref==refs(i)),binsCH);
    bh=bar(bins,N(i,:)./sum(N(i,:)),'histc');
    set(bh,'FaceColor',[.7 .7 .7],'EdgeColor','none')
    % plot(bins,cumsum(N(i,:))./sum(N(i,:))/10,'.-k')
    in_trial=find(arrayfun(@(x)x.Trial==trial_window(find(n_ref==refs(i),1)),exptevents));
    evs=exptevents(in_trial(arrayfun(@(x)~isempty(strfind(x.Note,'Stim ')) && ~isempty(strfind(x.Note,'Reference')),exptevents(in_trial))));
    for j=1:length(evs)
        line([evs(j).StartTime evs(j).StopTime],[0 0],'Color','g','LineWidth',2)
    end
    ylabel([num2str(refs(i)),' refs'])
end
ax(i+1)=subplot(length(refs)+1,1,i+1);
hold on;
bh=bar(bins,sum(N)./sum(N(:)),'histc');
ylim(ax(end),[0 max(sum(N)./sum(N(:)))])
set(bh,'FaceColor',[.7 .7 .7],'EdgeColor','none')

for j=1:length(evs)
    line([evs(j).StartTime evs(j).StopTime],[0 0],'Color','g','LineWidth',2)
end
ylabel(['Any refs'])
xlabel('First Lick Time (sec)')
set(gcf,'Name',basename);
linkaxes(ax(1:end-1))
%yl=get(ax(1),'Ylim');
yl=[0 max(max(N./repmat(sum(N,2),1,size(N,2))))];
set(ax(1:end-1),'YLim',[0 yl(2)])
set(ax(end),'XLim',get(ax(1),'XLim'))
for i=1:length(refs)
    plot(binsCH,cumsum(N_CH(i,:))./sum(N_CH(i,:))*yl(2),'-k','Parent',ax(i))
end
plot(binsCH,cumsum(sum(N_CH))./sum(N_CH(:))*max(get(ax(end),'YLim')),'-k','Parent',ax(end))

ax1p=get(ax(1),'Position');
axphp=ax1p;
axphp(1)=axphp(1)+axphp(3)*(2/3);
axphp(3)=axphp(3)*(1/3);
axphp(2)=axphp(2)+axphp(4)*.2;
axph=axes('Position',axphp);
plot_false_alarm_per_histogram(exptparams);

title(ax(1),['Trials ',num2str(trial_window(1)),' to ',num2str(trial_window(end)),', ',num2str(sum(N(:))),'/',num2str(length(trial_window)),' FAs'],'HorizontalAlignment','right')
set(ax,'XLim',[0 max([evs.StartTime])+exptparams.BehaveObject.EarlyWindow])
a=2;

if(1)
    baphy_remote_figsave(gcf,[],globalparams,'FA histogram')
end
    