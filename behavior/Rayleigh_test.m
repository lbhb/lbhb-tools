function [ray_sig_indices] = Rayleigh_test(spike_counts,synchronies,p)

% Returns 1s in indices where synchrony is significant with a p value of p,
% 0 when it isn't
if(~isequal(size(spike_counts),size(synchronies)))
    error('spike_counts and synchronies must be of the same size.')
end
if(ndims(synchronies)>3)
    error('size of synchonies is too large')
end

seuil=nan(1,numel(spike_counts));
ray=nan(1,numel(spike_counts));
for i = 1:numel(spike_counts)
    if isnan(spike_counts(i))
        seuil(i)=1;
        ray(i)=0;
    elseif spike_counts(i)<101
        seuil(i)=ray_table(spike_counts(i),p);
        ray(i)= synchronies(i);
    else
        seuil(i)=ray_table(spike_counts(i),p);
        ray(i)=2*spike_counts(i)*synchronies(i)^2;
    end
end
ray_sig_indices=false(size(seuil));
ray_sig_indices(ray>seuil)=true;
%ray_sig_indices(ray<=seuil)=false;
ray_sig_indices=reshape(ray_sig_indices,size(synchronies));
ray_sig_indices(isnan(synchronies))=false;