function seuil=ray_table(n,alpha)

if n==0
    seuil=1;
elseif n>100
    switch alpha
        case 0.1
            seuil = 4.605;
        case 0.05
            seuil = 5.991;
        case 0.025
            seuil = 7.378;
        case 0.01
            seuil = 9.210;
        case 0.001
            seuil = 13.816;
    end
else
    switch alpha
        case 0.1
            seuil(1:5)=linspace(1,0.677,5);
            seuil(6)=0.618;
            seuil(7)=0.572;
            seuil(8)=0.535;
            seuil(9)=0.504;
            seuil(10)=0.478;
            seuil(11)=0.456;
            seuil(12)=0.437;
            seuil(13)=0.420;
            seuil(14)=0.405;
            seuil(15)=0.391;
            seuil(16)=0.379;
            seuil(17)=0.367;
            seuil(18)=0.357;
            seuil(19)=0.348;
            seuil(20)=0.339;
            seuil(21)=0.331;
            seuil(22)=0.323;
            seuil(23)=0.316;
            seuil(24)=0.309;
            seuil(25:100)=interp1([25 30 35 40 45 50 100],[0.303 0.277 0.256 0.240 0.226 0.214 0.15],25:100,'linear');
        case 0.05
            seuil(1:5)=linspace(1,0.754,5);
            seuil(6)=0.690;
            seuil(7)=0.642;
            seuil(8)=0.602;
            seuil(9)=0.569;
            seuil(10)=0.540;
            seuil(11)=0.516;
            seuil(12)=0.494;
            seuil(13)=0.475;
            seuil(14)=0.458;
            seuil(15)=0.443;
            seuil(16)=0.429;
            seuil(17)=0.417;
            seuil(18)=0.405;
            seuil(19)=0.394;
            seuil(20)=0.385;
            seuil(21)=0.375;
            seuil(22)=0.367;
            seuil(23)=0.359;
            seuil(24)=0.351;
            seuil(25:100)=interp1([25 30 35 40 45 50 100],[0.344 0.315 0.292 0.273 0.257 0.244 0.17],25:100,'linear');
        case 0.025
            seuil(1:5)=linspace(1,0.816,5);
            seuil(6)=0.753;
            seuil(7)=0.702;
            seuil(8)=0.660;
            seuil(9)=0.624;
            seuil(10)=0.594;
            seuil(11)=0.567;
            seuil(12)=0.544;
            seuil(13)=0.524;
            seuil(14)=0.505;
            seuil(15)=0.489;
            seuil(16)=0.474;
            seuil(17)=0.460;
            seuil(18)=0.447;
            seuil(19)=0.436;
            seuil(20)=0.425;
            seuil(21)=0.415;
            seuil(22)=0.405;
            seuil(23)=0.397;
            seuil(24)=0.389;
            seuil(25:100)=interp1([25 30 35 40 45 50 100],[0.381 0.348 0.323 0.302 0.285 0.270 0.19],25:100,'linear');
        case 0.01
            seuil(1:5)=linspace(1,0.879,5);
            seuil(6)=0.825;
            seuil(7)=0.771;
            seuil(8)=0.725;
            seuil(9)=0.687;
            seuil(10)=0.655;
            seuil(11)=0.627;
            seuil(12)=0.602;
            seuil(13)=0.580;
            seuil(14)=0.560;
            seuil(15)=0.542;
            seuil(16)=0.525;
            seuil(17)=0.510;
            seuil(18)=0.496;
            seuil(19)=0.484;
            seuil(20)=0.472;
            seuil(21)=0.461;
            seuil(22)=0.451;
            seuil(23)=0.441;
            seuil(24)=0.432;
            seuil(25:100)=interp1([25 30 35 40 45 50 100],[0.423 0.387 0.359 0.336 0.318 0.301 0.21],25:100,'linear');
        case 0.001
            seuil(1:5)=linspace(1,0.991,5);
            seuil(6)=0.940;
            seuil(7)=0.891;
            seuil(8)=0.847;
            seuil(9)=0.808;
            seuil(10)=0.775;
            seuil(11)=0.743;
            seuil(12)=0.716;
            seuil(13)=0.692;
            seuil(14)=0.669;
            seuil(15)=0.649;
            seuil(16)=0.630;
            seuil(17)=0.613;
            seuil(18)=0.597;
            seuil(19)=0.583;
            seuil(20)=0.569;
            seuil(21)=0.556;
            seuil(22)=0.544;
            seuil(23)=0.533;
            seuil(24)=0.522;
            seuil(25:100)=interp1([25 30 35 40 45 50 100],[0.512 0.470 0.436 0.409 0.386 0.367 0.26],25:100,'linear');
    end
    seuil=seuil(floor(n));
end
