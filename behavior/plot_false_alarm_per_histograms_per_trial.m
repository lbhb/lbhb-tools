function plot_false_alarm_per_histograms_per_trial(exptparams,varargin)
%plot_false_alarm_per_histograms_per_trial(exptparams)
%plot_false_alarm_per_histograms_per_trial(exptparams,blocksize)
block_size=[];
if(ischar(exptparams))
    parmfile=exptparams;
    clear exptparams
    [pathname,basename]=fileparts(parmfile);
    LoadMFile(parmfile);
end
if(~isempty(varargin))
    block_size=varargin{1};
end
if(isempty(block_size))
    block_size=20;
end

total_trials=length(exptparams.FirstLick.Ref);
trial_starts=1:block_size:total_trials;
trial_ends=trial_starts+block_size-1;
trial_ends(trial_ends>total_trials)=total_trials;
p=[.1 .0444  .08    min(0.1*length(trial_starts),0.8778)];
fh=figure('Units','Normalized','Position',p);
for i=1:length(trial_starts)
    ax(i)=subplot(length(trial_starts),1,i);
    plot_false_alarm_per_histogram(exptparams,[trial_starts(i):trial_ends(i)]);
    ylabel(['t=',num2str(trial_starts(i)),' to ',num2str(trial_ends(i))])
end
linkaxes(ax);
xlabel('Time (sec)')
set(ax(1:end-1),'XTickLabel','')

a=2;