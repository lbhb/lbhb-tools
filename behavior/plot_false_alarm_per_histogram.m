function plot_false_alarm_per_histogram(exptparams,varargin)
%plot_false_alarm_per_histogram(exptparams)
%plot_false_alarm_per_histogram(exptparams,trial_window)
options.reference='StimOnset';
options.reference='TrialOnset';
options.remove_pre_stim_time=1;
options.remove_pre_stim_time=0;
if(ischar(exptparams))
    parmfile=exptparams;
    clear exptparams
    [pathname,basename]=fileparts(parmfile);
    LoadMFile(parmfile);
end
trial_window=[];
if(~isempty(varargin))
trial_window=varargin{1};
end
if(isempty(trial_window))
    trial_window=1:length(exptparams.FirstLick.Ref);
end
axph=gca;
%inter-reference interval
IRI=exptparams.TrialObject.ReferenceHandle.PreStimSilence+exptparams.TrialObject.ReferenceHandle.PostStimSilence+exptparams.TrialObject.ReferenceHandle.Duration; 
switch options.reference
    case 'StimOnset'
        subtract_time=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
    case 'TrialOnset'
        subtract_time=0;
end
times=exptparams.FirstLick.Ref(trial_window)-subtract_time;
if(options.remove_pre_stim_time)
    times(times<(exptparams.TrialObject.ReferenceHandle.PreStimSilence-subtract_time))=[];
end
lick_times_re_refs=mod(times,IRI);
binsPH=linspace(0,IRI,20);
binsCPH=linspace(0,IRI,100);
Np=histc(lick_times_re_refs,binsPH);
Np_CH=histc(lick_times_re_refs,binsCPH);
 
bh=bar(binsPH,Np./sum(Np),'histc','Parent',axph);
hold on;
set(bh,'FaceColor',[.7 .7 .7],'EdgeColor','none')
yl=get(axph,'YLim');
plot(binsCPH,cumsum(Np_CH)./sum(Np_CH)*yl(2),'-k','Parent',axph)
line([0 exptparams.TrialObject.ReferenceHandle.Duration]+exptparams.TrialObject.ReferenceHandle.PreStimSilence-subtract_time,[0 0],'Color','g','LineWidth',2,'Parent',axph)
xlim([0 IRI])

lick_times_re_refs(isnan(lick_times_re_refs))=[];
VS=abs(sum(exp(-1i*2*pi*lick_times_re_refs/IRI)))/length(lick_times_re_refs);
p_val=0.01;
is_sig5 = Rayleigh_test(length(lick_times_re_refs),VS,0.05);
is_sig1 = Rayleigh_test(length(lick_times_re_refs),VS,0.01);
if(is_sig1)
    sig_str='**';
elseif(is_sig5)
    sig_str='*';
else
    sig_str='';
end
title(axph,sprintf(['VS = %.02f',sig_str,' N = %d'],VS,sum(~isnan(times))))