dbopen;

cellid='%';
batch=259;
modelnames={'env100_logn_fir15_siglog100_fit05h_fit05c',...
            'env100_logn_adp1pcno_fir15_siglog100_fit05h_fit05c',...
            'env100_logn_wc02c_adp1pcno_fir15_siglog100_fit05h_fit05c'};

modelidx=1;
[lin_data,lin_by_cell]=narf_load_demo('%',batch,modelnames{modelidx},'lin');
modelidx=2;
[adp_data,adp_by_cell]=narf_load_demo('%',batch,modelnames{modelidx},'adp1');
modelidx=3;
[wc_data,wc_by_cell]=narf_load_demo('%',batch,modelnames{modelidx},'wcadp1');

figure;
plot([-0.1 1.1],[-0.1 1.1],'k--');
hold on
plot(lin_data.r_ceiling,adp_data.r_ceiling,'.');
hold off
axis([-0.1 1.1 -0.1 1.1]);
axis square

figure;
plot([0 0],[-0.5 1],'k--');
hold on
plot([-8 12],[0 0],'k--');
plot(adp_data.early_channel_magnitude(:),adp_data.depression_magnitude(:),'.');
hold off
axis tight
xlabel('channel gain');
ylabel('channel dep');
