% function cell_rasters_DS(cellid,runclass,options)
%
% options: .psthfs[=15]
%          .unit[=1]
%          .channel[=1]
%          .active[=1]  (0 means active and passive)
%          .datause[='Collapse both']
%          .usesorted[=1] (0 means raw threhold crossing)
%          .h[=figure] (default new figure)
%          .lick[=1] (1 means plot lick below psth)
%          .raster[=1] (0 means don't plot raster)
%          .rawid[=[]] (list of specific rawfile ids to include)
%          .rasterfs[=1000] 
%          options.rasterfs=options.psthfs 
%          
% created svd for SFN 2007 + plotting modifications DS 2017

function cell_rasters_DS(cellid,runclass,options)

if ~exist('options','var'),
   options=[];
end
if ~isfield(options,'psthfs')
   options.psthfs=15;
end
if ~isfield(options,'unit')
   options.unit=1;
end
if ~isfield(options,'channel')
   options.channel=1;
end
if ~isfield(options,'active')
   options.active=1;
end
if ~isfield(options,'maxplots')
   options.maxplots=0;
end
if ~isfield(options,'axes')
   options.axes=[];
end
options.rasterfs=options.psthfs;
options.sigthreshold=getparm(options,'sigthreshold',4);

if ~isfield(options,'datause')
   options.datause='Collapse both';
end
if ~isfield(options,'usesorted')
   options.usesorted=1;
end
options.psth=1;

if ~isfield(options,'rasterfs')
   options.rasterfs=1000;
end

%options.lfp=2;
if ~isfield(options,'h')
   options.h=figure;
end
if ~isfield(options,'lick')
   options.lick=1;
end 
if ~isfield(options,'PreStimSilence'),
   options.PreStimSilence=0.35; %hard-coded for PTD -DS
end 
if ~isfield(options,'PostStimSilence'),
   options.PostStimSilence=0.35; %hard-coded for PTD -DS
end
options.rawid=getparm(options,'rawid',[]);
options.raster=getparm(options,'raster',1);
options.mergeset=getparm(options,'mergeset',{});

dbopen;
if ~isempty(options.rawid),
   if length(options.rawid)>1,
      rawstr=mat2str(options.rawid);
      rawstr=rawstr(2:(end-1));
   else
      rawstr=num2str(options.rawid);
   end
   rawstr=strrep(rawstr,' ',',');
   sitestr=['gDataRaw.id in (',rawstr,')']
   active=0;
else
   sitestr=['runclass in ("',strrep(runclass,',','","'),'")'];
end
siteid=strsep(cellid,'-');
if length(siteid)>1,
   options.channel=siteid{2}(1)-'a'+1;
   options.unit=str2num(siteid{2}(2));
end
siteid=siteid{1};

if ~options.usesorted,
   sql=['SELECT gDataRaw.* FROM gDataRaw',...
        ' WHERE gDataRaw.cellid="',siteid,'"',...
        ' AND ',sitestr,...
        ' AND not(bad)',...
        ' ORDER BY parmfile']
elseif options.active,
   sql=['SELECT gDataRaw.*,sCellFile.goodtrials FROM gDataRaw,sCellFile',...
        ' WHERE gDataRaw.id=sCellFile.rawid',...
        ' AND sCellFile.cellid="',cellid,'"',...
        ' AND ',sitestr,...
        ' AND not(bad)',...
        ' AND behavior="active"',...
        ' ORDER BY parmfile']
else
   sql=['SELECT gDataRaw.*,sCellFile.goodtrials FROM gDataRaw,sCellFile',...
        ' WHERE gDataRaw.id=sCellFile.rawid',...
        ' AND sCellFile.cellid="',cellid,'"',...
        ' AND ',sitestr,...
        ' AND not(bad)',...
        ' ORDER BY parmfile']
end
rawdata=mysql(sql);
if length(rawdata)>=1,
   siteid=rawdata(1).cellid;
end

if options.maxplots>0 && length(rawdata)>options.maxplots,
   rawdata=rawdata(1:options.maxplots);
end

rawid=cat(1,rawdata.id);
r={};
tags={};
mresp=0;
for fidx=1:length(rawdata),
   if ~isempty(rawdata(fidx).goodtrials),
       options.trialrange=eval(rawdata(fidx).goodtrials);
   elseif isfield(options,'trialrange'),
       options=rmfield(options,'trialrange');
   end
   
   if any(strcmp(rawdata(fidx).parmfile,options.mergeset)),
       keyboard 
       for tt=1:length(options.mergeset),
           mfile=[rawdata(fidx).resppath options.mergeset{tt}]
           [tr,tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
           if tt==1,
               r{fidx}=tr;
           else
               r{fidx}=cat(2,r{fidx},tr);
           end
       end
   else
       mfile=[rawdata(fidx).resppath rawdata(fidx).parmfile];
       [r{fidx},tags{fidx}]=raster_load(mfile,options.channel,options.unit,options);
   end
   
   smcount=round(options.rasterfs./options.psthfs);
   smfilt=ones(smcount,1)./smcount.*1000;
   mr=rconv2(squeeze(nanmean(r{fidx},2)),smfilt);
   mr=max(mr(:));
   mresp=max(mresp,mr);
end


% probably can delete this stuff
if isfield(options,'trialrange'),
   options=rmfield(options,'trialrange');
end
if ~isfield(options,'lickmax'),
   options.lickmax=0.55;
end
if ~isfield(options,'psthmax'),
   options.psthmax=mresp.*0.95;
end

sfigure(options.h); 
% DS modified from original to call Luke's function UTget and have subplots
% fit in the window well and put all of it into an if statement
% The else part only works with options.datause='Collapse reference' or 'Collapse
% target'

% figure out subplot arrangement
stimcount=size(r{1},3); % how many different stimuli
filecount=length(rawdata);

% figure out behavioral state
file_category=cell(filecount,1);
for fidx=1:filecount,
   baphy_parms=dbReadData(rawid(fidx));
   if strcmpi(baphy_parms.BehaveObjectClass,'Passive') && fidx==1,
      file_category{fidx}='P1';
   elseif strcmpi(baphy_parms.BehaveObjectClass,'Passive'),
      file_category{fidx}='Px';
   elseif baphy_parms.Trial_TargetIdxFreq(1)>0.2,
      file_category{fidx}='AE';
   elseif baphy_parms.Trial_TargetIdxFreq(1)<0.2,
      file_category{fidx}='AH';
   end
end

pre_stime = baphy_parms.Ref_PreStimSilence;
dur_stime = baphy_parms.Ref_Duration;
post_stime = baphy_parms.Ref_PostStimSilence;
t_axes = ((1/options.psthfs):(1/options.psthfs):(pre_stime+dur_stime+post_stime))-pre_stime;

% specify response window
r_win_on=find(t_axes>0.0 & t_axes<0.2);
r_win_sust=find(t_axes>0.2 & t_axes<0.75);
r_win_off=find(t_axes>0.75 & t_axes<0.95);
sp_win=find(t_axes<=0);

% two extra plots to allow two average response plots
subs=UTget_subs(stimcount+3);
rowcount = subs{1};
colcount = subs{2};

rmean=zeros(stimcount,filecount);
rerr=zeros(stimcount,filecount);
spmean=zeros(stimcount,filecount);
sperr=zeros(stimcount,filecount);

%the following code plots the PSTH for each behavioral block in the
%same figure
hn = zeros(filecount,1);
clr = lines(filecount);
s_legend = {};
maxy=0;
for rr=1:stimcount,
   hs=subplot(rowcount,colcount,rr+3);
   
   for fidx=1:filecount,
      this_resp=r{fidx}(:,:,rr).*options.psthfs;
      
      hn(fidx) = plot(t_axes, squeeze(nanmean(this_resp,2)), 'Color', clr(fidx,:),'LineWidth', 1);
      
      % compute mean response during onset window 
      rmean_onset(rr,fidx)=nanmean(nanmean(this_resp(r_win_on,:)));
      rerr_onset(rr,fidx)=nanstd(nanmean(this_resp(r_win_on,:)))./sqrt(size(this_resp(r_win_on,:),2));
      
      % compute mean response during sustained window 
      rmean_sust(rr,fidx)=nanmean(nanmean(this_resp(r_win_sust,:)));
      rerr_sust(rr,fidx)=nanstd(nanmean(this_resp(r_win_sust,:)))./sqrt(size(this_resp(r_win_sust,:),2));
      
      % compute mean response during offset window 
      rmean_off(rr,fidx)=nanmean(nanmean(this_resp(r_win_off,:)));
      rerr_off(rr,fidx)=nanstd(nanmean(this_resp(r_win_off,:)))./sqrt(size(this_resp(r_win_off,:),2));
      
      % compute mean response during prestim window 
      spmean(rr,fidx)=nanmean(nanmean(this_resp(sp_win,:)));
      sperr(rr,fidx)=nanstd(nanmean(this_resp(sp_win,:)))./sqrt(size(this_resp,2));
      
      s_legend{fidx} = strrep(rawdata(fidx).parmfile,'.m',['_' file_category{fidx}]);
      hold on
      
   end
   title(tags{1}{rr},'Interpreter','none');
   xlabel('Time after stimulus onset (sec)');
   ylabel('spikes/sec');
   hold off
   
   % maxy=record max y axis
   aa=axis;
   maxy=max(maxy,aa(4));
   
end

for rr=1:stimcount,
   hs=subplot(rowcount,colcount,rr+3);
   axis([t_axes(1) t_axes(end) 0 maxy]);

   %put onset and offeset sound dotted lines on plots
   line([0  0], [0 maxy],'linestyle','--','color','g');
   line([0.75  0.75], [0 maxy],'linestyle','--','color','g');
end

% summary plot for onset response window
hs=subplot(rowcount,colcount,1);
if stimcount>1,
   errorbar(rmean_onset-spmean,rerr);
else
   bar([(rmean_onset-spmean); zeros(size(rmean_onset))]);
end
title(sprintf('%s evoked onset', cellid));
ylabel('spikes/sec');

l = legend(s_legend{:});
%legend boxoff
set(l, 'Interpreter', 'none')

% summary plot for sustained response window
hs=subplot(rowcount,colcount,2);
if stimcount>1,
   errorbar(rmean_sust-spmean,rerr);
else
   bar([(rmean_sust-spmean); zeros(size(rmean_sust))]);
end
title(sprintf('%s evoked sustained', cellid));
ylabel('spikes/sec');


% summary plot spont
hs=subplot(rowcount,colcount,3);
if stimcount>1,
   errorbar(spmean,rerr);
else
   bar([spmean; zeros(size(spmean))]);
end

title(sprintf('%s spont ', cellid));
ylabel('sp/sec');

if stimcount==1,
   % also put legend on PSTH plot in case colors don't match bars
   hs=subplot(rowcount,colcount,stimcount+3);
   l = legend(s_legend{:});
   %legend boxoff
   set(l, 'Interpreter', 'none')
end

fullpage portrait

