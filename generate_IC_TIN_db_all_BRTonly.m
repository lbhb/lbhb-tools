% IC data structure bbl IC RH. 2017-06-07 Daniela Saderi
% DS 2017-09-05 modified from generate IC_TIN_db_all to have only Beartooth
% data

ICdata=struct;
n=1;

% Pre-passive - Active easy - Postpassive
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT005c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT005c/sorted/';
ICdata(n).prepassive_spike='BRT005c04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127334;
ICdata(n).active_spike='BRT005c06_a_PTD.spk.mat';
ICdata(n).active_rawid=127336;
ICdata(n).postpassive_spike='BRT005c07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127338;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT005c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT005c/sorted/';
ICdata(n).prepassive_spike='BRT005c04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127334;
ICdata(n).active_spike='BRT005c08_a_PTD.spk.mat';
ICdata(n).active_rawid=127339;
ICdata(n).postpassive_spike='BRT005c07_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127338;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT006d-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT006d/sorted/';
ICdata(n).prepassive_spike='BRT006d04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127412;
ICdata(n).active_spike='BRT006d05_a_PTD.spk.mat';
ICdata(n).active_rawid=127413;
ICdata(n).postpassive_spike='BRT006d06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127415;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active Pure Tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT006d-a2';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT006d/sorted/';
ICdata(n).prepassive_spike='BRT006d04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127412;
ICdata(n).active_spike='BRT006d07_a_PTD.spk.mat';
ICdata(n).active_rawid=127416;
ICdata(n).postpassive_spike='BRT006d06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127415;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active easy - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT007c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT007c/sorted/';
ICdata(n).prepassive_spike='BRT007c04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127456;
ICdata(n).active_spike='BRT007c05_a_PTD.spk.mat';
ICdata(n).active_rawid=127457;
ICdata(n).postpassive_spike='BRT007c06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127458;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT007c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT007c/sorted/';
ICdata(n).prepassive_spike='BRT007c04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127456;
ICdata(n).active_spike='BRT007c07_a_PTD.spk.mat';
ICdata(n).active_rawid=127459;
ICdata(n).postpassive_spike='BRT007c06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127458;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT007c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT007c/sorted/';
ICdata(n).prepassive_spike='BRT007c04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127456;
ICdata(n).active_spike='BRT007c010_a_PTD.spk.mat';
ICdata(n).active_rawid=127462;
ICdata(n).postpassive_spike='BRT007c06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127458;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active hard - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=1; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT007c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT007c/sorted/';
ICdata(n).prepassive_spike='aBRT007c04_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127456;
ICdata(n).active_spike='BRT007c012_a_PTD.spk.mat';
ICdata(n).active_rawid=127464;
ICdata(n).postpassive_spike='BRT007c06_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127458;
ICdata(n).on_BF=1; % 1=on BF target, 0=off BF
ICdata(n).difficulty=1;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active hard - Postpassive (missing)
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT009a-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT009a/sorted/';
ICdata(n).prepassive_spike='BRT009a03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127529;
ICdata(n).active_spike='BRT009a04_a_PTD.spk.mat';
ICdata(n).active_rawid=127530;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=2;  % 0=pure tone, 1=easy, 2=hard


%--------------------------------------------------------------------------

% Pre-passive - Active pure tone - Postpassive
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT009c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT009c/sorted/';
ICdata(n).prepassive_spike='BRT009c03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127536;
ICdata(n).active_spike='BRT009c04_a_PTD.spk.mat';
ICdata(n).active_rawid=127537;
ICdata(n).postpassive_spike='BRT009c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127539;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard

% Pre-passive - Active easy
n=length(ICdata)+1;
ICdata(n).animal='Beartooth';
ICdata(n).craniotomy='1';
ICdata(n).pupil=0; % 0=no pupil, 1=pupil
ICdata(n).cellid='BRT009c-a1';
ICdata(n).area='ICx';
ICdata(n).type='Var_SNR';
ICdata(n).datapath='/auto/data/daq/Beartooth/BRT009c/sorted/';
ICdata(n).prepassive_spike='BRT009c03_p_PTD.spk.mat';
ICdata(n).prepassive_rawid=127536;
ICdata(n).active_spike='BRT009c06_a_PTD.spk.mat';
ICdata(n).active_rawid=127540;
ICdata(n).postpassive_spike='BRT009c05_p_PTD.spk.mat';
ICdata(n).postpassive_rawid=127539;
ICdata(n).on_BF=0; % 1=on BF target, 0=off BF
ICdata(n).difficulty=0;  % 0=pure tone, 1=easy, 2=hard
