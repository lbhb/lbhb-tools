

batch1=267;
threshold1=0.007;

batch2=266;
threshold2=0.007;

modelname='fb18ch100_lognn_wcg03_ap3z1_dexp_fit05v';
module='pole_zeros';


p1=narf_model_parms(batch1,modelname,module);
p2=narf_model_parms(batch2,modelname,module);

d1=zeros(size(p1));
for ii=1:length(p1),
    gg=p1(ii).gains;
    gg(p1(ii).delays<0)=0;
    jj=min(find(abs(gg)==max(abs(gg))));
    d1(ii)=p1(ii).delays(jj);
end
est_snr=cat(1,p1.est_snr);
est_reps=cat(1,p1.est_reps);
val_snr=cat(1,p1.val_snr);
val_reps=cat(1,p1.val_reps);
okv=find(est_snr.^2 .* est_reps > threshold1 & ...
         val_snr.^2 .* val_reps > threshold1);
d1=d1(okv);
%d1(d1<0)=0;

d2=zeros(size(p2));
for ii=1:length(p2),
    gg=p2(ii).gains;
    gg(p2(ii).delays<0)=0;
    jj=min(find(abs(gg)==max(abs(gg))));
    d2(ii)=p2(ii).delays(jj);
end
est_snr=cat(1,p2.est_snr);
est_reps=cat(1,p2.est_reps);
val_snr=cat(1,p2.val_snr);
val_reps=cat(1,p2.val_reps);
okv=find(est_snr.^2 .* est_reps > threshold2 & ...
         val_snr.^2 .* val_reps > threshold2);
d2=d2(okv);
%d2(d2<0)=0;

p=randttest(d1,d2,5000,1);

figure;
xx=linspace(-10,60,15)+2.4;
subplot(2,1,1);
b1=hist(d1,xx);
b1=b1./sum(b1);
bar(xx,b1);
title(sprintf('batch %d -- n=%d mean=%.3f',batch1,length(d1),mean(d1)));
subplot(2,1,2);
b2=hist(d2,xx);
b2=b2./sum(b2);
bar(xx,b2);
title(sprintf('batch %d -- n=%d mean=%.3f p=%.3f',...
              batch2,length(d2),mean(d2),p));

