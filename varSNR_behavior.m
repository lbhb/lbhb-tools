% function results=varSNR_behavior(animal)
% SVD and DS to analyze task-difficulty behavioral data - 2017-04-11


function results=varSNR_behavior(animal,include_training,pure_tone_epoch)

narf_set_path

dbopen;

if ~exist('animal','var')
    animal = 'Babybell';
end
if ~exist('include_training','var')
    include_training=0;
end

if ~exist('pure_tone_epoch','var')
    pure_tone_epoch=0;
end

if strcmpi(animal,'Babybell')
    cellprefix='bbl';
elseif strcmpi(animal,'Tartufo')
    cellprefix='TAR';
elseif strcmpi(animal,'Button')
    cellprefix='btn';
elseif strcmpi(animal,'Magic')
    cellprefix='mgc';
elseif strcmpi(animal, 'Beartooth')
    cellprefix='BRT';
elseif strcmpi(animal, 'Leyva')
    cellprefix='ley';
end

if ~include_training
    sTrain=' AND gDataRaw.training=0';
else
    sTrain='';
end

% only look at subset that has pure tone behavior

if pure_tone_epoch
   PT=' AND gDataRaw.id>125259';
else
   PT='';
end

% To add: only pick training sessions with Trial_OveralldB =>55 dB (trained only)
% separated var SNR from other SVD and DS style
sql=['SELECT gDataRaw.*,gCellMaster.penid,gPenetration.pendate FROM gDataRaw',...
    ' INNER JOIN gCellMaster ON gDataRaw.masterid=gCellMaster.id'...
    ' INNER JOIN gPenetration ON gCellMaster.penid=gPenetration.id'...
    ' WHERE runclass="PTD"',...
    ' AND trials>20',...
    PT,...
    sTrain, ...
    ' AND not(resppath like "L:%")',...
    ' AND behavior="active"',...
    ' AND gDataRaw.cellid like "' cellprefix '%"',...
    ' AND not(gDataRaw.bad) ORDER BY gDataRaw.id'];

rawdata=mysql(sql);
filecount=length(rawdata);

% data to collect:
masterid=nan(filecount,1);
training=nan(filecount,1);
difficulty=nan(filecount,1);
probeDI=nan(filecount,1);
meanDI=nan(filecount,1);
probeHR=nan(filecount,1);
probeRT=nan(filecount,1);
FAR=nan(filecount,1);
tokenFAR=nan(filecount,1);
tokenprobeHR=nan(filecount,1);
tokenFARz=nan(filecount,1);
tokenprobeHRz=nan(filecount,1);
block_order=nan(filecount,1);
cumulativehits=nan(filecount,1);
trialcount=nan(filecount,1);
probeSNR=nan(filecount,1);
probe_freq=nan(filecount,1);

% rawids for sessions with TONEinTORCs SVD style
flipsetrawids=[120446 120448 120451 120475 120477 120525 ...
    120528 120530 120538 120539 120542 120544];

% rawids for sessions with TONEinTORCs DS style
straddlesetrawids=[120110 120111 120163 120165 120184 120185 ...
    120188 120190 120199 120201 120207 120209 120211 120214 ...
    120234 120234 120254 120256 120258 120260 120272 120273 ...
    120274 120275 120293 120283 120285 120286 120289 120290 ...
    120293 120310 120311 120312 120313 120314 120316 120317 ...
    120435 120436 120437];


lastpendate='';
thispencount=0;
thishits=0;
for ii=1:filecount
    rawid=rawdata(ii).id;
    training(ii)=rawdata(ii).training;
    
    [parms,perf]=dbReadData(rawdata(ii).id);
    badfile=0;
    
    if parms.Trial_OveralldB<55
        badfile=1;
    end
    if ~isfield(perf,'uDiscriminationIndex')
        
        evpfile=[rawdata(ii).resppath rawdata(ii).respfileevp];
        if exist(evpfile,'file')
            parmfile=[rawdata(ii).resppath rawdata(ii).parmfile];
            replicate_behavior_analysis(parmfile,1);
            close;
            [parms,perf]=dbReadData(rawdata(ii).id);
        else
            badfile=1;
        end
    end
    if ~badfile
        Trial_TargetIdxFreq=parms.Trial_TargetIdxFreq;
        tcount=length(Trial_TargetIdxFreq);
        if tcount==5 && isinf(parms.Trial_RelativeTarRefdB(1)) && Trial_TargetIdxFreq(1)==0.8
            difficulty(ii)=0;  %pure-tone common
        elseif tcount==5 && sum(Trial_TargetIdxFreq==0.2)==5
            difficulty(ii)=2; % medium
        elseif tcount==5 && Trial_TargetIdxFreq(1)==0.3
            difficulty(ii)=1;  %easy
        elseif tcount==5 && Trial_TargetIdxFreq(1)==0.1
            difficulty(ii)=3;  %hard
        elseif tcount==1 && strcmpi(animal,'Tartufo')
            difficulty(ii)=0; %tone only
        elseif tcount==1 && strcmpi(animal,'Leyva')
            difficulty(ii)=0; %tone only
            
        elseif ismember(rawid,flipsetrawids)
            if parms.Trial_RelativeTarRefdB(2)<0.5
                difficulty(ii)=1;
            else
                difficulty(ii)=3;
            end
            
        elseif ismember(rawid,straddlesetrawids)
            if parms.Trial_RelativeTarRefdB(1)>parms.Trial_RelativeTarRefdB(2)
                difficulty(ii)=1;
            else
                difficulty(ii)=3;
            end
        end
        
        %uDI=perf.uDiscriminationIndex; % these value come from the default analysis of DI
        uHitRate=perf.uHitRate;
        uReactionTime=perf.uReactionTime;
        
        % assign the probe idx depending on experimental set up
        probeidx=0;
        if length(parms.Trial_RelativeTarRefdB)==5
            probeidx=3; % always test with middle SNR (varSNR behavior)
        elseif ismember(rawid,flipsetrawids)
            probeidx=2; % 2nd target always same SNR
        elseif ismember(rawid,straddlesetrawids)
            probeidx=2; % 2nd target always same SNR
        elseif tcount==1  % don't use pure tone-only as probe
            probeidx=1;
        end
        if probeidx
            %probeDI(ii)=uDI(probeidx);
            probeHR(ii)=uHitRate(probeidx);
            probeRT(ii)=uReactionTime(probeidx);
            probeSNR(ii)=parms.Trial_RelativeTarRefdB(probeidx);
            probe_freq(ii)=parms.Tar_Frequencies(probeidx);
        end
        FAR(ii)=perf.FalseAlarmRate;
        
        masterid(ii)=rawdata(ii).masterid;
        
        % figure out which block this was today
        pendate=rawdata(ii).pendate;
        if ~strcmpi(pendate,lastpendate)
            thispencount=0;
            thishits=0;
        end
        lastpendate=pendate;
        thispencount=thispencount+1;
        block_order(ii)=thispencount;
        cumulativehits(ii)=thishits;
        thishits=thishits+perf.Hit(1);
        trialcount(ii)=rawdata(ii).trials;
        
        if probeidx
            try
                parmfile=[rawdata(ii).resppath rawdata(ii).parmfile];
                %    di_nolick outputs: metrics and metrics_newT are structs containing the metrics
                %    metrics_newT has metrics calculated only from trials immediately
                %    following hits or misses (should be trials in which a new stimulus was
                %    played)
                [metrics,metrics_newT]=di_nolick(parmfile);
                tokenFAR(ii)=metrics.FAR;
                details=metrics.details;
                
                meanDI(ii)=nanmean(details.uDI);
                
                if probeidx
                    probeDI(ii)=details.uDI(probeidx);
                    probeHR(ii)=details.uHR(probeidx);
                   %probeRT(ii)=uReactionTime(probeidx);
                   %probeSNR(ii)=parms.Trial_RelativeTarRefdB(probeidx);
                   %probe_freq(ii)=parms.Tar_Frequencies(probeidx);
                end
                FAR(ii)=perf.FalseAlarmRate;
                
                %[~, ~, ~, tokenFAR(ii), details]=di_nolick(parmfile);
                tokenprobeHR(ii)=details.uHR(probeidx);
                
                avgrefcount=mean(parms.Trial_ReferenceCountFreq.*...
                    0:length(parms.Trial_ReferenceCountFreq)-1);
                
                uHR = details.uHit(probeidx) / ...
                   (details.uHit(probeidx)+details.uMiss(probeidx));
                if uHR>0.99
                   uHR=0.99;
                elseif uHR<0.01
                   uHR=0.01;
                end
                uFAR = details.FAs / (details.FAs+details.CRs);
                if uFAR>0.99
                   uFAR=0.99;
                elseif uFAR<0.01
                   uFAR=0.01;
                end
                
                hitZ = norminv(uHR);
                farZ = norminv(uFAR);
                
%                  % number of target occur
%                 nhit=(details.uHit(probeidx)+details.uMiss(probeidx));
%                 % number of distractors
%                 nfar=details.FAs+details.CRs;
%                 
%                 % method to correct for HR=100 for d' analysis (Macmillian & Kaplan, 1985)
%                 tokenprobeHR(ii)=(details.uHit(probeidx)-1/(nhit+nfar))/nhit;
%                 
%                 p=1/(1+avgrefcount);
%                 m=nhit*p;
%                 sigma=sqrt(nhit*p*(1-p));
%                 hitZ=(tokenprobeHR(ii)*nhit - m) ./sigma;
%                 
%                 p=1/(1+avgrefcount);
%                 m=nfar*p;
%                 sigma=sqrt(nfar*p*(1-p));
%                 farZ=(tokenFAR(ii)*nfar - m) ./sigma;

                tokenprobeHR(ii)=uHR;
                tokenprobeHRz(ii)=hitZ;
                tokenFARz(ii)=farZ;
                
                % calculate overall dprime 
%                 dprime=tokenprobeHR(ii)-tokenFAR(ii);
%                 bias=-(tokenprobeHR(ii)+tokenFAR(ii))./2;
%                 
%                 dprimeZ(ii)=tokenprobeHRz(ii)-tokenFARz(ii);
%                 f_rat=tokenFAR(ii)./(tokenFAR(ii)+tokenprobeHR(ii));
%                 f_rat(f_rat<0.01) = 0.01;
%                 f_rat(f_rat>0.99) = 0.99;
%                 biasZ(ii)=norminv(f_rat);
                
                
%                 hitZ-farZ
%                 keyboard
            catch
                disp('failed to run di_nolick');
                tokenprobeHR(ii)=probeHR(ii)./100;
                tokenFAR(ii)=FAR(ii)./100;
            end
           % print the name of the file, meanDI, probeDI, zscored-probeHR, FAR
            fprintf('%s %.2f %.2f %.2f %.2f \n',rawdata(ii).parmfile, meanDI(ii), probeDI(ii), tokenprobeHRz(ii),...
                tokenFARz(ii));
        end
    end
end

goodblocks=find(~isnan(difficulty) & ~isnan(probeDI));

results=struct;
results.rawid=cat(1,rawdata.id);
results.masterid=masterid;
results.difficulty=difficulty;
results.probeDI=probeDI;
results.meanDI=meanDI;
results.probeHR=probeHR;
results.probeRT=probeRT;
results.FAR=FAR;
results.tokenprobeHR=tokenprobeHR;
results.tokenFAR=tokenFAR;
results.tokenprobeHRz=tokenprobeHRz;
results.tokenFARz=tokenFARz;
results.probeSNR=probeSNR;
results.probe_freq=probe_freq;
results.block_order=block_order;
results.cumulativehits=cumulativehits;
results.trialcount=trialcount;

ff=fields(results);
for ii=1:length(ff)
    results.(ff{ii})=results.(ff{ii})(goodblocks);
end


unique_masterid=unique(masterid(:)'); % use all data
%unique_masterid=unique(masterid(~training)'); % only use ephys data

%calculate differences within-session
diffDI=zeros(size(unique_masterid));
diffFAR=zeros(size(unique_masterid));
for jj=1:length(unique_masterid)
    diffDI(jj)=mean(probeDI(masterid==unique_masterid(jj) & difficulty==3)) -...
        mean(probeDI(masterid==unique_masterid(jj) & difficulty==1));
    diffFAR(jj)=mean(FAR(masterid==unique_masterid(jj) & difficulty==3)) -...
        mean(FAR(masterid==unique_masterid(jj) & difficulty==1));
end

col=[.8 .8 .8];
figure;
diff=0:3;
subplot(3,3,1);
mean_di=zeros(3,1);
se_di=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_di(i)=nanmean(probeDI(didx));
    se_di(i)=nanstd(probeDI(didx))./sqrt(length(didx));
end

for jj=1:length(unique_masterid)
    hold on
    ii=find(masterid==unique_masterid(jj) & difficulty~=2 & meanDI>0.5);
    plot(difficulty(ii),probeDI(ii),'-o','Color', col);
end

nn=find(~isnan(mean_di));
errorbar(diff(nn), mean_di(nn), se_di(nn),'r');
hold off
xlabel('difficulty');
ylabel('probe DI');

dprime=tokenprobeHR-tokenFAR;
bias=-(tokenprobeHR+tokenFAR)./2;

dprimeZ=tokenprobeHRz-tokenFARz;
%biasZ=-(tokenprobeHRz+tokenFARz)./2;
f_rat=tokenFAR./(tokenFAR+tokenprobeHR);
f_rat(f_rat<0.01) = 0.01;
f_rat(f_rat>0.99) = 0.99;
biasZ=norminv(f_rat);

subplot(3,3,2);

Y=biasZ;
Ylabel='biasZ';

mean_x=zeros(3,1);
se_x=zeros(3,1);

for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn),mean_x(nn),se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(3,3,3);

Y=dprimeZ;
Ylabel='dprimeZ';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(3,3,4);

Y=probeRT;
Ylabel='probeRT';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(3,3,5);

Y=tokenprobeHRz;
Ylabel='tokenprobeHRz';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);

subplot(3,3,6);

Y=tokenFARz;
Ylabel='tokenFARz';

mean_x=zeros(3,1);
se_x=zeros(3,1);
for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'.', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);


% subplot(3,3,4);
% hist(diffDI,-45:10:45);
% xlabel('DI difference hard-easy');
%
%
% subplot(3,3,5);
% hist(diffFAR,-45:10:45);
% xlabel('FAR difference hard-easy');

subplot(3,3,7);

mean_x=zeros(5,1);
se_x=zeros(5,1);
for bl=1:5
    didx=find(block_order==bl);
    
    mean_x(bl)=nanmean(probeDI(didx));
    se_x(bl)=nanstd(probeDI(didx))./sqrt(length(didx));
end
plot(block_order,probeDI,'.', 'Color', col);
hold on
errorbar(mean_x,se_x,'r');
hold off
xlabel('block order');
ylabel('probe DI');

subplot(3,3,8);

mean_x=zeros(5,1);
se_x=zeros(5,1);
for bl=1:5
    didx=find(block_order==bl);
    
    mean_x(bl)=mean(FAR(didx));
    se_x(bl)=std(FAR(didx))./sqrt(length(didx));
end
plot(block_order,FAR,'-o', 'Color', col);
hold on
errorbar(mean_x(nn), se_x(nn),'r');
hold off
xlabel('block order');
ylabel('FAR');

subplot(3,3,9);

Y=FAR;
Ylabel='FAR';

mean_x=zeros(3,1);
se_x=zeros(3,1);

for i=1:length(diff)
    didx=find(difficulty==diff(i) & meanDI>0.5);
    
    mean_x(i)=nanmean(Y(didx));
    se_x(i)=nanstd(Y(didx))./sqrt(length(didx));
end
plot(difficulty,Y,'-o', 'Color', col);
hold on
errorbar(diff(nn), mean_x(nn), se_x(nn),'r');
hold off
xlabel('difficulty');
ylabel(Ylabel);


% subplot(3,3,9);
% 
% mean_x=zeros(5,1);
% se_x=zeros(5,1);
% for bl=1:5
%     didx=find(block_order==bl);
%     
%     mean_x(bl)=mean(probeRT(didx));
%     se_x(bl)=std(probeRT(didx))./sqrt(length(didx));
% end
% 
% plot(block_order,probeRT,'.');
% hold on
% errorbar(mean_x(nn),se_x(nn),'r');
% hold off 
% xlabel('block order');
% ylabel('probe RT');







