function [p,pe]=gaincomp(r1,r2,binsize)

if ~exist('binsize','var'),
   binsize=1;
end

minrep=min(size(r1,2),size(r2,2));
r1=r1(:,1:minrep,:);
r2=r2(:,1:minrep,:);
% eithernanidx=find(isnan(r1) | isnan(r2));
% r1(eithernanidx)=nan;
% r2(eithernanidx)=nan;

m1=nanmean(r1,2);
m2=nanmean(r2,2);

if size(m2,1)<size(m1,1),
   m1=m1(1:size(m2,1),:,:);
end
      
gidx=find(~isnan(m1(:)) & ~isnan(m2(:)));
m1=m1(gidx);
m2=m2(gidx);

mm=nanmax(cat(1,m1(:),m2(:)));


% a=m1;
% b=m2;
% os=0;
a=(m1+m2)./2;
b=m2-m1;
os=1;

if binsize>1,
   if os==0,
      ab=sortrows([a+b a b]);  % sort by mean response
      ab=ab(:,2:3);
   else
      ab=sortrows([a b]);  % sort by mean response
   end
   nn=floor(size(ab,1)/binsize)*binsize;
   ab=ab(1:nn,:);
   ab=reshape(ab,binsize,nn./binsize,2);
   ab=squeeze(nanmean(ab,1));
   a=ab(:,1);
   b=ab(:,2);
end

[~,si]=sort(rand(size(a)));
a=a(si);
b=b(si);
      
jackcount=20;
jackstep=length(a)./jackcount;
gset=zeros(jackcount,2);
for jj=1:jackcount,
   kk=[1:round((jj-1).*jackstep) round(jj*jackstep+1):length(a)];
   kk=kk(:);
   %gset(jj,:)=polyfit(a(kk),b(kk),1);
   gset(jj,:)=regress(b(kk),[a(kk) ones(size(a(kk)))]);
end
      
pm=mean(gset,1);
pe=nanstd(gset,0) * sqrt(jackcount-1);

%p=polyfit(a,b,1);
p=regress(b,[a ones(size(a))]);

subplot(1,2,1);

plot(a,b,'.');
hold on
x=[min(a),max(a)];
y=[min(a) max(a)]*p(1)+p(2);
plot(x,y,'k--');
hold off
axis tight square

subplot(1,2,2);

x=[min(a),max(a)];
y=[min(a) max(a)]*(p(1)+os)+p(2);
mp=sortrows([m1 m2]);
binsize=5;
nn=floor(size(mp,1)/binsize)*binsize;
mp=mp(1:nn,:);
mp=reshape(mp,binsize,nn./binsize,size(mp,2));
mp=squeeze(nanmean(mp,1));

plot([-mm/20 mm],[-mm/20 mm],'k--','LineWidth',0.5);
hold on
plot(mp(:,1),mp(:,2),'.','Color',[0.5 0.5 0.5]);
plot(x,y,'r-','LineWidth',1);
hold off
axis tight square
title(sprintf('gain: %.2f',os+p(1)));
xlabel(passfile,'Interpreter','none');
ylabel(actfile,'Interpreter','none');


