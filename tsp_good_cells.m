function tsp_good_cells(batchid)

narf_set_path

dbopen

sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid),...
     ' ORDER BY cellid'];
rundata=mysql(sql);

cellids={rundata.cellid};

ioset={'por102a-b1','por102c-a1',...
       'por102c-b1','por108a-a1','por108a-b1','por110a-b1',...
       'por110a-c1','por110a-d1','por110a-d2','por113a-a1',...
       'por118b-c1','por119b-c1'};
cellids=setdiff(cellids,ioset);
r=[];
rall=[];
cellcount=0;
aacellcount=0;
monster_ref=[];
monster_tar=[];
goodcellids={};
aacellids={};
for ii=1:length(cellids),
    
    cellid=cellids{ii};
    fprintf('tsp_resp_resp2(%s)\n',cellid);
    tr=tsp_resp_resp2(cellid,batchid);
    if isstruct(tr),
        cellcount=cellcount+1;
        goodcellids=cat(2,goodcellids,cellid);
        ff=fields(tr);
        for jj=1:length(ff),
            rall(cellcount).(ff{jj})=tr.(ff{jj});
        end
        if ~isnan(tr.AA_beta(1)),
           aacellcount=aacellcount+1;
           aacellids=cat(2,aacellids,cellid);
           r=rall(ismember(goodcellids,aacellids));
           if ismember(batchid, [252 274 275 287]),
              monster_ref=cat(3,monster_ref,tr.rresp(1:75,:));
              monster_tar=cat(3,monster_tar,tr.tresp(1:31,:));
           else
              monster_ref=cat(3,monster_ref,tr.rresp(1:88,:));
              monster_tar=cat(3,monster_tar,tr.tresp(1:44,:));
           end
        end
        fprintf('%12s, %3s, %3s, %6s, %6s, %6s, %4s, %6s, %6s, %4s, %2s, %2s, %1s\n',...
           'cellid','iso','tar','inst','AA_r0','AAgain','Asig','A1Pgn','A2Pgn','Psig','DI','DI','B');
        for jj=1:length(r),
            P1inst=abs([r(jj).P1P_z])>3;
            P2inst=abs([r(jj).P2P_z])>3;
            if isempty(P1inst),P1inst=[0 0]; end;
            if isempty(P2inst),P2inst=[0 0]; end;
            Pinst=[P1inst P2inst];
            AAsig=abs(r(jj).AA_z)>2;
            APsig=abs(r(jj).AA_z)>2;
            fprintf('%12s, %3.0f, %3.0f, "%d%d%d%d", %6.2f, %6.2f, "%d%d", %6.2f, %6.2f, "%d%d", %2.0f, %2.0f, %d\n',...
                    r(jj).cellid,r(jj).iso,r(jj).tar_resp,Pinst,...
                    r(jj).AA_r0,r(jj).AA_beta(2)-1,AAsig,...
                    r(jj).AP_beta-[0 1],APsig,...
                    r(jj).DImean,r(jj).DImin,r(jj).inband);
        end
        
        drawnow
        %tr
        %pause
    end
    
end

for ii=1:length(r)
    if isempty(r(ii).P1P_z),
        r(ii).P1P_beta=[0 1];
        r(ii).P1P_z=[0 0];
    end
    if isempty(r(ii).P2P_z),
        r(ii).P2P_beta=[0 1];
        r(ii).P2P_z=[0 0];
    end
    if isempty(r(ii).A1P_z),
        r(ii).A1P_beta=[0 1];
        r(ii).A1P_z=[0 0];
    end
    if isempty(r(ii).A2P_z),
        r(ii).A2P_beta=[0 1];
        r(ii).A2P_z=[0 0];
    end
end

tsp_gainsum_do_plot;

% prediction analysis (dep vs. behavior effects)
[cellids,predxc]=LF_get_preds(batchid);
depset=zeros(aacellcount,1);
dephelps=(predxc(:,4)-predxc(:,1)>0.015);
depcellid=cellids(dephelps);
nodepcellid=cellids(~dephelps);
    
for ii=1:aacellcount,
    if sum(strcmp(r(ii).cellid,depcellid)),
        depset(ii)=1;
    end
end

figure;
subplot(2,2,1);
plot(mean(rr(:,:,find(depset)),3));
title('dep cells');

subplot(2,2,2);
plot(mean(rr(:,:,find(~depset)),3));
title('no dep cells');

subplot(2,2,3);
plotcomp(log2(a1p(find(depset),2)),log2(a2p(find(depset),2)),'A1-P','A2-P',...
         [-1 1 -1 1]);

subplot(2,2,4);
plotcomp(log2(a1p(find(~depset),2)),log2(a2p(find(~depset),2)),'A1-P','A2-P',...
         [-1 1 -1 1]);

mm=nanmean(nanmean(monster_ref(PreStimBins:end,:,:),1),2);
mm=repmat(squeeze(mm),[1 3]);
PreStimBins=round(rasterfs.*prestimsilence);
rr0=nanmean(monster_ref(1:PreStimBins,:,:));
rr0=repmat(rr0,[size(monster_ref,1) 1 1]);
sg=squeeze(nanmean(monster_ref(1:PreStimBins-1,:,:),1))';
rr=(monster_ref-rr0);
rg=squeeze(nanmean(rr((PreStimBins+1):end,:,:),1))';
rt0=nanmean(monster_ref((PreStimBins+6):end,:,:));
rt0=repmat(rt0,[size(monster_tar,1) 1 1]);
tar_resp=cat(3,r.tar_resp);
tar_sign=sign(tar_resp);
tt=(monster_tar-rt0);
tg=squeeze(nanmean(tt(PreStimBins+(1:8),:,:),1))';
%tt=(monster_tar);
%tg=squeeze(nanmean(tt(PreStimBins+(1:5),:,:),1))'-rg;
nantar=find(isnan(tg(:,3)));
nnantar=find(~isnan(tg(:,3)));

% dep vs. non-dep cells
%ct=depset+1;

% behavior-mod cells
ct=catidx;

% all cells
ct=double(catidx>0);

if batchid==253,
    nn={'hard','easy'};
else
    nn={'on','off'};
end

figure;
subplot(3,3,1);
plotcomp(sg(:,1),sg(:,2),['spont ' nn{1}],['spont ' nn{2}],[],ct);
subplot(3,3,2);
plotcomp(rg(:,1),rg(:,2),['ref ' nn{1}],['ref ' nn{2}],[],ct);
subplot(3,3,3);
plotcomp(tg(:,1),tg(:,2),['tar ' nn{1}],['tar ' nn{2}],[],ct);

nsg=sg./mm;
nrg=rg./mm;
ntg=tg./mm;
nsg=[nanmean(nsg(:,1:2),2)-nsg(:,3) nsg(:,1)-nsg(:,2)];
nrg=[nanmean(nrg(:,1:2),2)-nrg(:,3) nrg(:,1)-nrg(:,2)];
ntg=[nanmean(ntg(:,1:2),2)-ntg(:,3) ntg(:,1)-ntg(:,2)];

meandiff=[nanmean(nsg); nanmean(nrg); nanmean(ntg)];
subplot(3,3,4);
bar(meandiff);
legend('act-pas',[nn{1} '-' nn{2}]);

subplot(3,3,5);
os=0.1428;
bar(meandiff);
hold on
plot(1-os+randn(size(nsg(:,1)))./50,nsg(:,1),'k.');
plot(1+os+randn(size(nsg(:,2)))./50,nsg(:,2),'k.');
plot(2-os+randn(size(nrg(:,1)))./50,nrg(:,1),'k.');
plot(2+os+randn(size(nrg(:,2)))./50,nrg(:,2),'k.');
plot(3-os+randn(size(ntg(:,1)))./50,ntg(:,1),'k.');
plot(3+os+randn(size(ntg(:,2)))./50,ntg(:,2),'k.');
hold off
axis([0.5 3.5 -0.5 0.5]);


subplot(3,3,7);
plot(sort(sg(:,1:2)));
title(sprintf('Soff>on: %d/%d (%.3f)',sum(sg(:,2)>=sg(:,1)),...
              length(sg(:,1)),signtest(sg(:,1)-sg(:,2))));
ylabel(sprintf('Smean on: %.3f off %.3f pas %.3f',nanmean(sg./mm)));
subplot(3,3,8);
plot(sort(rg(:,1:2)));
title(sprintf('Roff>on: %d/%d (%.3f)',sum(rg(:,2)>=rg(:,1)),...
              length(rg(:,1)),signtest(rg(:,1)-rg(:,2))));
ylabel(sprintf('Rmean on: %.3f off %.3f pas %.3f',nanmean(rg./mm)));
subplot(3,3,9);
plot(sort(tg(:,1:2)));
title(sprintf('Toff>on: %d/%d (%.3f)',sum(tg(:,2)>=tg(:,1)),...
              length(tg(:,1)),signtest(tg(:,1)-tg(:,2))));
ylabel(sprintf('Tmean on: %.3f off %.3f pas %.3f',nanmean(tg./mm)));
fullpage portrait

if batchid==287,
   
   A1keep={'eno008b-a2','eno030c-a1','eno030c-a2','eno045g-c1','por022b-a1',...
      'por024b-b2','por026b-b2','por027a-a1','por027a-b1','por028d-a1',...
      'por028d-a2','por028d-b1','sti019b-a1','sti020a-b2',...
      };
   A2keep={'eno004c-b1','eno009b-a1','eno020b-d1','eno024d-b2','eno024d-c1',...
      'eno031b-b1','eno031b-c1','eno032e-a1','eno032e-c2','eno035b-a1',...
      'eno045g-c2','por022b-c1','por025a-c2','por025c-d1','por026b-d1',...
      'sti019c-c1','sti019c-c2','sti019c-c3','sti019c-c4','sti019c-d1',...
      };
   
   A1P_beta=cat(1,rall.A1P_beta);
   A2P_beta=cat(1,rall.A2P_beta);
   olapidx=find(~isnan(A1P_beta(:,1)) & ~isnan(A2P_beta(:,1)));
   a1onlyidx=find(~isnan(A1P_beta(:,1)) & isnan(A2P_beta(:,1)) & ...
      ismember(goodcellids,A1keep)');
   a2onlyidx=find(isnan(A1P_beta(:,1)) & ~isnan(A2P_beta(:,1)) & ...
      ismember(goodcellids,A2keep)');
   
   xx=linspace(-1,1,20)';
   a1b=hist((A1P_beta(olapidx,2))-1,xx)';
   a1o=hist((A1P_beta(a1onlyidx,2))-1,xx)';
   a2b=hist((A2P_beta(olapidx,2))-1,xx)';
   a2o=hist((A2P_beta(a2onlyidx,2))-1,xx)';
   
   figure;
   subplot(2,2,1);
   bar(xx,[a1b a1o],'stacked');
   title(sprintf('A1P median olap %.3f only %.3f (%d)',...
      mean((A1P_beta(olapidx,2))),mean((A1P_beta(a1onlyidx,2))),length(olapidx)+length(a1onlyidx)));
   axis([-1.2 1.2 0 16]);
   
   subplot(2,2,2);
   bar(xx,[a2b a2o],'stacked');
   title(sprintf('A2P median olap %.3f only %.3f (%d)',...
      mean((A2P_beta(olapidx,2))),mean((A2P_beta(a2onlyidx,2))),length(olapidx)+length(a2onlyidx)));
   axis([-1.2 1.2 0 16]);
   colormap(gray)
   
   for ii=[olapidx; a1onlyidx; a2onlyidx]',
      fprintf('%14s %.3f %.3f\n',rall(ii).cellid,...
         A1P_beta(ii,2)-1,A2P_beta(ii,2)-1);
   end
   
elseif ismember(batchid,[274 275]),
   if batchid==274,
      aidx=1;
   else
      aidx=2;
   end
   f=find(~isnan(tg(:,aidx)) & ~isnan(tg(:,3)) &...
      ~isnan(rg(:,aidx)) & ~isnan(rg(:,3)));
     %& abs(rg(:,3))<10 & abs(tg(:,3))<10);
   figure;
   subplot(3,2,1);
   plotcomp(rg(f,3),rg(f,aidx),'ref pass','ref act');
   subplot(3,2,2);
   plotcomp(tg(f,3),tg(f,aidx),'tar pass','tar act');
   xx=linspace(-20,20,20);
   subplot(3,2,3);
   hist(rg(f,[3 aidx]),xx);
   title(sprintf('median pas %.2f act %.2f',median(rg(f,[3 aidx]))));
   xlabel('ref response');
   
   subplot(3,2,4);
   hist(tg(f,[3 aidx]),xx);
   title(sprintf('median pas %.2f act %.2f',median(tg(f,[3 aidx]))));
   xlabel('tar response');
   
   if batchid==274,
      beta=cat(1,r(f).A1P_beta);
   else
      beta=cat(1,r(f).A2P_beta);
   end
   subplot(3,2,5);
   hist(beta(:,1));
   xx=linspace(-2,2,10);
   subplot(3,2,6);
   hist(log2(beta(:,2)),xx);
   
   set(gcf,'Name',sprintf('batch %d ref/tar',batchid));
end

   
   

keyboard


%outpath='/auto/users/svd/docs/current/irvine_talk/';
outpath='';
outfile=sprintf('%sbatch_%d.mat',outpath,batchid);
save(outfile);
printfile=sprintf('%sbatch_%d.pdf',outpath,batchid);
print('-dpdf',printfile);



function [cellids,predxc]=LF_get_preds(batchid)

if ismember(batchid,[252,268,287]) || ~exist('batchid','var'),
   batchid=268;
end

tsp_pred_sum;
