narf_set_path
close all
drawnow;
dbopen;

% want to save the figures? 0 no saving, 1 saving
savefigures=0;

% do you want to plot rasters of subset of cells determined in goodblocks?
plot_rasters=0;

% Set this to 1 if you want to only look at dataset with pupil correction
PUPIL_ONLY=0;

% choose the animal to analyze
animal='Baby+Bear';
% animal='Babybell';
% animal='Beartooth';
%animal='Button'; 
%animal='Magic';
%animal='Slee'; %:  both Magic and Button
%animal='Tartufo'; % AC PTD data from array


% chose what data set to look at 
dataset='IC_all_data_multiple_passives'; % Babybell+Beartooth IC data all combined
% dataset='IC_bbl_only'; % bbl IC only
% dataset='IC_BRT_only'; % BRT IC only
% dataset='AC'; % Tartufo AC array data

% Look at prepassive vs active
% Set to 0 if prepassive vs active - remember to set including_behavior =1
% Set to 1 if postpassive vs active - remember to set including_behavior =1 
% Set to 2 if prepassive vs postpassive
comparison = 0; % MAKE SURE IT MATCHES WHAT YOU HAVE SET IN ptd_load_data.m

% load data matrix
results_beha.on_BF(isnan(results_beha.on_BF))=-1;

% save data matrix
savefile=sprintf('/auto/data/tmp/daniela/ptd_pop_data_%s_%s_pupil%d_comparison%d.mat',...
   animal,dataset, PUPIL_ONLY, comparison);

fprintf('loading from %s\n',savefile);
load(savefile);


%% stats 

epoch=2;  % 1=onset, 2=sustained, 3=offset

% all cells with prepassive/active or postpassive/active or
% prepassive/postpassive depending on what the variable passive is set to
% in the ptd_load_data.m script.
goodblocks=find(~isnan(results_beha.gain(:,epoch)));
if PUPIL_ONLY==1
    subset='prepass_active_pupil_corrected';
else
    subset='prepass_active';
end

% on BF only (determined by looking at STRF)
%goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.on_BF==1);
% subset='on_BF';

% off BF only (determined by looking at STRF)
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.on_BF==0);
% subset='off_BF';

% ICc only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.ICc==1);
% subset='ICc';

% ICx only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.ICc==0);
% subset='ICx';

% easy only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.difficulty==1);
% subset='easy';

% % hard only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & results_beha.difficulty==3);
% subset='hard';

% % passive responses enhanced by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_pass(:,1)) &...
%     results_beha.ref_vs_tar_pass(:,1)>results_beha.ref_vs_tar_pass(:,2));
% subset='passive_r_enhanced';

% % active responses enhanced by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_act(:,1)) &...
%     results_beha.ref_vs_tar_act(:,1)>results_beha.ref_vs_tar_act(:,2));
% subset='active_r_enhanced';

% % active responses suppressed by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_act(:,1)) &...
%     results_beha.ref_vs_tar_act(:,1)<results_beha.ref_vs_tar_act(:,2));
% subset='active_r_suppressed';

% % passive responses suppressed by tone
% goodblocks=find(~isnan(results_beha.ref_vs_tar_pass(:,1)) &...
%     results_beha.ref_vs_tar_pass(:,1)<results_beha.ref_vs_tar_pass(:,2));
% subset='passive_r_suppressed';

% Prepassive vs active only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==0);
% if PUPIL_ONLY==1
%     subset='pre-passive_active_pupil_corrected';
% else
%     subset='pre-passive_active';
% end

% % Prepassive vs active only with Pupil (this is not corrected, run with PUPIL_ONLY=0)
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==0 & results_beha.pupil==1);
% subset='pre-passive_active_pupil_not_corrected';

% % First Post-passive vs active only
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==1);
% if PUPIL_ONLY==1
%     subset='first_post-passive_active_pupil_corrected';
% else
%     subset='first_post-passive_active';
% end

% %First Post-Passive vs active only with Pupil (this is not corrected, run with PUPIL_ONLY=0)
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==1 & results_beha.pupil==1);
% subset='first_post-passive_active_pupil_not_corrected';

% % Prepassive vs active + tone enhances TORC responses
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==0 & ...
%    results_beha.ref_vs_tar_pass(:,1)>results_beha.ref_vs_tar_pass(:,2));
% subset='pre-passive_active_r_tone_enhanced';

% % Prepassive vs active + tone suppresses TORC responses
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==0 & ...
%    results_beha.ref_vs_tar_pass(:,1)<results_beha.ref_vs_tar_pass(:,2));
% subset='pre-passive_active_r_tone_suppressed';
%
% % First Post-passive vs active + tone enhances TORC responses
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==1 & ...
%    results_beha.ref_vs_tar_pass(:,1)>results_beha.ref_vs_tar_pass(:,2));
% subset='first_post-passive_active_r_tone_enhanced';
 
% % First Post-passive vs active + tone suppresses TORC responses
% goodblocks=find(~isnan(results_beha.gain(:,epoch)) & ...
%    results_beha.passive_position==1 & ...
%    results_beha.ref_vs_tar_pass(:,1)<results_beha.ref_vs_tar_pass(:,2));
% subset='first_post-passive_active_r_tone_suppressed';

rawid=results_beha.rawid(goodblocks);
probeDI=results_beha.probeDI(goodblocks);
gain=results_beha.gain(goodblocks,epoch);
gainZ=results_beha.gainZ(goodblocks,epoch);
targain=results_beha.targain(goodblocks,epoch);
targainZ=results_beha.targainZ(goodblocks,epoch);
spont=results_beha.spont_active(goodblocks)-results_beha.spont_passive(goodblocks);
spontZ=results_beha.spontZ(goodblocks,epoch);
difficulty=results_beha.difficulty(goodblocks);
difficulty(difficulty>2)=2;
block_order=results_beha.block_order(goodblocks);
singleid=results_beha.singleid(goodblocks);
unique_singleid=unique(singleid(~isnan(gain))'); % use all data
tardist=results_beha.tardist(goodblocks);
tardiff=results_beha.tardiff(goodblocks);
strfgain=results_beha.strfgain(goodblocks);
cellids=unique(results_beha.cellid(goodblocks));

probeHR=results_beha.tokenprobeHR(goodblocks);
probeFAR=results_beha.tokenFAR(goodblocks);
probeHRz=results_beha.tokenprobeHRz(goodblocks);
probeFARz=results_beha.tokenFARz(goodblocks);
% probeHR=results_beha.probeHR(goodblocks);
FAR=results_beha.FAR(goodblocks);

area=results_beha.area(goodblocks);
% areacat=ones(size(area));
% areacat(strcmpi(area,'ICx'))=2;
areacat=ones(size(area))+1;
areacat(strcmpi(area,'ICc'))=1;

dprime=probeHR-probeFAR;
bias=-(probeHR+probeFAR)./2;
dprimeZ=probeHRz-probeFARz;
biasZ=-(probeHRz+probeFARz)./2;
%bias=((1-probeHR).*(1-probeFAR))./(probeHR.*probeFAR)./2;
%bias=log(bias+1);
%bias=-0.5*(log(probeHR./(1-probeHR))+log(probeFAR./(1-probeFAR)));

strfc=cat(4,results_beha.strfc{goodblocks});
strfc(strfc==0)=nan;
strfd=strfc(:,:,1,:)-strfc(:,:,2,:);  % act-pas
deltaA=squeeze(mean(mean(strfd(50:51,12,:),1),2));
%%gain=deltaA+1;

col=[.8 .8 .8];

%F2=abs(results_beha.tardist(goodblocks)) < 1;
F2=ones(size(goodblocks));

% figures
figure;

subplot(3,4,1);

xx=linspace(-1,1,20);
gsig=abs(gainZ)>2;
gnsig=abs(gainZ)<=2;
histcomp(gain(gsig & F2)-1,gain(gnsig & F2)-1,'gain sig','NS','pct',[-1 1],1,16,1);

subplot(3,4,5);

ssig=abs(spontZ)>2;
snsig=abs(spontZ)<=2;
histcomp(spont(ssig),spont(snsig),'spont sig','NS','',[-10 10],1,16,1);

for ii=[2:4 6:8]
   switch ii
      case 2
         X=bias;
         Y=gain;
         Yz=gainZ;
         Xlabel='bias';
         Ylabel='ref gain';
      case 3
         X=dprime;
         Y=gain;
         Yz=gainZ;
         Xlabel='dprime';
         Ylabel='ref gain';
      case 4
         X=probeDI;
         Y=gain;
         Yz=gainZ;
         Xlabel='probeDI';
         Ylabel='ref gain';
      case 6
         X=biasZ;
         Y=gain;
         Yz=gainZ;
         Xlabel='biasZ';
         Ylabel='gain';
      case 7
         X=dprimeZ;
         Y=gain;
         Yz=gainZ;
         Xlabel='dprimeZ';
         Ylabel='gain';
      case 8
         X=probeHRz;
         Y=gain;
         Yz=gainZ;
         Xlabel='probeHRz';
         Ylabel='gain';
   end
   
   subplot(3,4,ii);
   
   gg=find(~isnan(X) & ~isnan(Y));
   X=X(gg);
   Y=Y(gg);
   Yz=Yz(gg);
   medX=median(X);
   mX=[mean(X(X<=medX)) mean(X(X>medX))];
   mean_Y=[mean(Y(X<=medX)) mean(Y(X>medX))];
   n=[sum(X<=medX) sum(X>medX)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   
   for mid=unique_singleid
      jj=find(singleid(gg)==mid);
      plot(X(jj),Y(jj),'-o','Color',col);
      hold on
   end
   plot(X(abs(Yz)>2 & F2(gg)),Y(abs(Yz)>2 & F2(gg)),'ko');
   %plot(X(areacat==1 & F2),Y(areacat==1 & F2),'ko');
   errorbar(mX,mean_Y,se_Y,'r','LineWidth',2);
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   
   cXY=xcov(X,Y,0,'coeff');
   title(sprintf('cc(X,Y)=%.3f',cXY));
end

subplot(3,4,9);

Y=gain;
Ystr='gain';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud)
   didx=find(X==ud(diff));
   
   mean_Y(diff)=mean(Y(didx));
   se_Y(diff)=std(Y(didx))./sqrt(length(didx));
end
savediff=[];
for mid=unique_singleid
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   f1=find(X(ii)==2);
   f2=find(X(ii)==3);
   if ~isempty(f1) && ~isempty(f2)
      savediff=cat(1,savediff,[Y(ii(f1(1))) Y(ii(f2(1)))]);
   end
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);


subplot(3,4,10);

Y=dprimeZ;
Ystr='dprimeZ';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud)
   didx=find(X==ud(diff));
   
   mean_Y(diff)=mean(Y(didx));
   se_Y(diff)=std(Y(didx))./sqrt(length(didx));
end
for mid=unique_singleid
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);


subplot(3,4,11);

Y=biasZ;
Ystr='biasZ';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud)
   didx=find(X==ud(diff));
   
   mean_Y(diff)=nanmean(Y(didx));
   se_Y(diff)=nanstd(Y(didx))./sqrt(length(didx));
end
for mid=unique_singleid
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);

subplot(3,4,12);

Y=probeDI;
Ystr='probeDI';
X=difficulty+1;
Xstr='difficulty';

gg=find(~isnan(X) & ~isnan(Y));
X=X(gg);
Y=Y(gg);

ud=unique(X);
mean_Y=zeros(length(ud),1);
se_Y=zeros(length(ud),1);
for diff=1:length(ud)
   didx=find(X==ud(diff));
   
   mean_Y(diff)=nanmean(Y(didx));
   se_Y(diff)=nanstd(Y(didx))./sqrt(length(didx));
end
for mid=unique_singleid
   ii=find(singleid(gg)==mid);
   plot(X(ii),Y(ii),'-o','Color',col);
   hold on
end
errorbar(mean_Y,se_Y,'r');
hold off
aa=axis;
axis([min(X)-0.5 max(X)+0.5 aa(3:4)]);
xlabel(Xstr);
ylabel(Ystr);


% subplot(3,4,12);
% timematrix=sortrows([rawid probeDI./100 probeFAR gain]);
% plot(timematrix(:,2:end));
% legend('probeDI','FAR','gain');
% xlabel('time');

% if savefigures,
%     set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
%     saveas(gcf, [options.figurepath dataset '_gain_analysis.pdf']);
%     close(gcf);
% end

if savefigures
    export_fig([options.figurepath dataset '_' subset '_gain_analysis.eps'], '-eps');
end

gtaridx=find(~isnan(targain) & targain>0.1 & targain<10);
if isempty(gtaridx)
   return
end


figure;

subplot(2,3,1);
Y=gain;
Yz=gainZ(gtaridx);
A=gain(gtaridx);
B=targain(gtaridx);
catidx=ones(size(A));
catidx(abs(Yz)<=2)=2;
% catidx(bias(gtaridx)>median(bias(gtaridx)))=2;
% catidx(areacat(gtaridx)==2)=2;
plotcomp(gain(gtaridx),targain(gtaridx),'refgain','targain',[],catidx);

for ii=[4:4]
   switch ii
      case 4
         A=targain(gtaridx)-gain(gtaridx);
         %D=(biasZ(gtaridx)>median(biasZ(gtaridx)))+1;
         %Xlabel='Bias (lo/hi)';
         %D=difficulty(gtaridx);
         %Xlabel='Difficulty (lo/hi)';
         %D=(probeDI(gtaridx)>median(probeDI(gtaridx)))+1;
         %Xlabel='probeDI (lo/hi)';
         D=(dprimeZ(gtaridx)>median(dprimeZ(gtaridx)))+1;
         Xlabel='Dprime (lo/hi)';
         
         Ylabel='tar-ref gain';
   end
   F3=F2(gtaridx);
   
   subplot(2,3,ii);
   
   ud=unique(D);
   mean_A=zeros(length(ud),1);
   se_A=zeros(length(ud),1);
   for diff=1:length(ud)
      didx=find(D==ud(diff));
      
      mean_A(diff)=mean(A(didx));
      se_A(diff)=std(A(didx))./sqrt(length(didx));
   end
   for mid=unique_singleid
      jj=find(singleid(gtaridx)==mid);
      if ~isempty(jj)
         plot(D(jj),A(jj),'-o','Color',col);
         hold on
      end
   end
   errorbar(mean_A,se_A,'r');
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   aa=axis;
   axis([0 max(ud)+1 -0.5 0.5]);
end

for ii=[2 3 5 6]
   switch ii
      case 2
         X=biasZ(gtaridx);
         Y=gain(gtaridx);
         Yz=gainZ(gtaridx);
         Xlabel='BiasZ';
         Ylabel='ref gain';
      case 3
         X=biasZ(gtaridx);
         Y=targain(gtaridx);
         Yz=targainZ(gtaridx);
         Xlabel='BiasZ';
         Ylabel='tar gain';
      case 5
         X=dprimeZ(gtaridx);
         Y=gain(gtaridx);
         Yz=gainZ(gtaridx);
         Xlabel='dprimeZ';
         Ylabel='ref gain';
      case 6
         X=dprimeZ(gtaridx);
         Y=targain(gtaridx);
         Yz=targainZ(gtaridx);
         Xlabel='dprimeZ';
         Ylabel='tar gain';
   end
   F3=F2(gtaridx);
   
   subplot(2,3,ii);
   medX=median(X);
   mX=[mean(X(X<=medX)) mean(X(X>medX))];
   mean_Y=[mean(Y(X<=medX)) mean(Y(X>medX))];
   n=[sum(X<=medX) sum(X>medX)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   
   for mid=unique_singleid
      ii=find(singleid(gtaridx)==mid);
      plot(X(ii),Y(ii),'-o','Color',col);
      hold on
   end
   plot(X(abs(Yz)>2 & F3),Y(abs(Yz)>2 & F3),'ko');
   errorbar(mX,mean_Y,se_Y,'r');
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   cXY=xcov(X,Y,0,'coeff');
   ceXY=xcov(exp(X),Y,0,'coeff');
   title(sprintf('cc(X,Y)=%.3f',cXY));
end

% if savefigures,
%     set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
%     saveas(gcf, [options.figurepath dataset '_RefTar_gain_analysis.pdf']);
%     close(gcf);
% end

if savefigures
    export_fig([options.figurepath dataset '_' subset '_RefTar_gain_analysis.eps'], '-eps');
end


%% local/global analysis
strfsnr=results_beha.strfsnr(goodblocks);
strfgoodblocks=goodblocks(strfsnr>0.4);
strfsnr=results_beha.strfsnr(strfgoodblocks);
strfdifficulty=results_beha.difficulty(strfgoodblocks);

gainstrf=strfgain(ismember(goodblocks,strfgoodblocks));
iccstrf=results_beha.ICc(strfgoodblocks);
onBFstrf=results_beha.on_BF(strfgoodblocks);

strfc=cat(4,results_beha.strfc{strfgoodblocks});
strfc(strfc==0)=nan;

A=squeeze(mean(mean(strfc(50:51,12,:,:),1),2))';
deltaA=A(:,1)-A(:,2);
deltaAlocal=A(:,1)-A(:,2).*gainstrf;
%deltaAlocal=A(:,1)-A(:,3);

figure;
subplot(3,3,1);
strfd=strfc(21:80,1:50,1,:)-strfc(21:80,1:50,2,:);  % act-pas
strfn=sum(~isnan(strfd),4);
strfd=nanmean(strfd,4);
stplot(strfd + flipud(strfd), 500, 62.5, 0, 4);
colorbar

%stplot(stdata,lfreq,tleng,smooth,noct,clim,siglev)

subplot(3,3,2);
hist(deltaA);
xlabel('deltaA');

subplot(3,3,3);
stplot(strfn);
colorbar

subplot(3,3,7);
hist(deltaAlocal);
xlabel('deltaAlocal');

subplot(3,3,4);
plot(gainstrf(find(onBFstrf)),deltaA(find(onBFstrf)),'r.');
hold on
plot(gainstrf(find(~onBFstrf)),deltaA(find(~onBFstrf)),'b.');
hold off
xlabel('global gain');
ylabel('deltaA');

subplot(3,3,5);
plot(gainstrf(find(onBFstrf)),deltaAlocal(find(onBFstrf)),'r.');
hold on
plot(gainstrf(find(~onBFstrf)),deltaAlocal(find(~onBFstrf)),'b.');
hold off
xlabel('global gain');
ylabel('deltaAlocal');

subplot(3,3,6);
plot(A(find(onBFstrf),2),deltaA(find(onBFstrf)),'r.');
hold on
plot(A(find(~onBFstrf),2),deltaA(find(~onBFstrf)),'b.');
hold off
xlabel('A passive');
ylabel('deltaA');
legend('on BF','off BF');
legend boxoff


for ii=[8:9]
   switch ii
      case 8 
         X=difficulty(ismember(goodblocks,strfgoodblocks));
         Y=gain(ismember(goodblocks,strfgoodblocks));
         Yz=gainZ(ismember(goodblocks,strfgoodblocks));
         Xlabel='difficulty';
         Ylabel='strfgain';
      case 9 
         X=difficulty(ismember(goodblocks,strfgoodblocks));
         Y=deltaAlocal;
         Yz=ones(size(Y))+2;
         Xlabel='difficulty';
         Ylabel='deltaAlocal';
   end
   
   subplot(3,3,ii);
   
   gg=find(~isnan(X) & ~isnan(Y));
   X=X(gg);
   Y=Y(gg);
   Yz=Yz(gg);
   
   for mid=unique_singleid
      jj=find(singleid(gg)==mid);
      plot(X(jj),Y(jj),'-o','Color',col);
      hold on
   end
   plot(X(abs(Yz)>2 & F2(gg)),Y(abs(Yz)>2 & F2(gg)),'ko');
   %plot(X(areacat==1 & F2),Y(areacat==1 & F2),'ko');
   
   medX=median(X);
   mX=[mean(X(X<=medX)) mean(X(X>medX))];
   mean_Y=[mean(Y(X<=medX & onBFstrf)) mean(Y(X>medX & onBFstrf))];
   n=[sum(X<=medX & onBFstrf) sum(X>medX & onBFstrf)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   errorbar(mX,mean_Y,se_Y,'r','LineWidth',2);
   mean_Y=[mean(Y(X<=medX & ~onBFstrf)) mean(Y(X>medX & ~onBFstrf))];
   n=[sum(X<=medX & ~onBFstrf) sum(X>medX & ~onBFstrf)];
   se_Y=[std(Y(X<=medX)) std(Y(X>medX))]./sqrt(n);
   errorbar(mX,mean_Y,se_Y,'b','LineWidth',2);
   hold off
   xlabel(Xlabel);
   ylabel(Ylabel);
   
   cXY=xcov(X,Y,0,'coeff');
   title(sprintf('cc(X,Y)=%.3f',cXY));
end

% if savefigures,
%     set(gcf,'units', 'normalized', 'position', [0 0 1 1], 'paperorientation', 'landscape');
%     saveas(gcf, [options.figurepath 'STRF_stats.pdf']);
%     close(gcf);
% end

if savefigures
    export_fig([options.figurepath dataset '_' subset '_STRF_stats.eps'], '-eps');
end

%% plot the rasters and PSTHs of cells in goodblocks
if plot_rasters
    
    raster_options=struct('lick',0,'psthfs',25,'active',0);
    
    for ii=1:length(goodblocks)
        cell_rasters(cellids{ii},'PTD',raster_options);
    end
end




