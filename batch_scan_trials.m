
batchid=252;
rasterfs=500;

narf_set_path;
dbopen;

sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid),...
     ' ORDER BY cellid'];
celldata=mysql(sql);

ii=50;

%cfd=dbbatchcells(batchid,celldata(ii).cellid);
cfd=dbgettspfiles(celldata(ii).cellid,batchid);
activefile=strcmpi({cfd.behavior},'active');
filecount=length(cfd);


fprintf('Computing per trial raster for cell %s...\n',celldata(ii).cellid);

sfigure(1);

maxa=0;
for ff=1:filecount,
    hs=subplot(2,filecount,ff);
    
    options.parmfile=[cfd(ff).stimpath cfd(ff).stimfile];
    options.spikefile=[cfd(ff).path cfd(ff).respfile];
    options.rasterfs=rasterfs;
    options.channel=cfd(ff).channum;
    options.unit=cfd(ff).unit;
    options.usesorted=1;
    options.psthfs=15;
    options.datause='Per trial';
    options.includeincorrect=1;
    options.psth=1;
    
    [r,tags]=raster_load(options.spikefile,options.channel,...
                         options.unit,options);
    raster_plot(options.parmfile,r,tags,hs,options);
    title(sprintf('%s (%d-%d, %.0f)',cfd(ff).stimfile,cfd(ff).channum,...
                  cfd(ff).unit,cfd(ff).isolation),...
          'Interpreter','none');
    
    hs=subplot(2,filecount,ff+filecount);
    plot(nanmean(r,1).*rasterfs);
    aa=axis;
    maxa=max(maxa,aa(4));
    if ~isempty(cfd(ff).goodtrials),
        goodtrials=eval(cfd(ff).goodtrials);
        hold on
        plot([min(goodtrials) min(goodtrials)],aa(3:4),'r--');
        plot([max(goodtrials) max(goodtrials)],aa(3:4),'r--');
        hold off
    end
    axis([-5 size(r,2)+5 aa(3:4)]);
    xlabel('trial');
    ylabel('mean spikes/sec');
end
for ff=1:filecount,
    hs=subplot(2,filecount,ff+filecount);
    aa=axis;
    axis([aa(1:3) maxa]);
end
