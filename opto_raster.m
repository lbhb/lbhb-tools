function opto_raster(rawid, channel, axes)

if ~exist('channel','var'),
    channel=1;
end
if ~exist('axes','var'),
    axes=[];
end
if ~isnumeric(channel),
   cellid=channel;
   cfd=dbgetscellfile('rawid',rawid,'cellid',cellid);
else
   cellid=[];
   cfd=dbgetscellfile('rawid',rawid);

end

% line colors:
spc={[0.2 0.2 0.2],[0 .6 0]};
% shading colors:
ssc={[0.7 0.7 0.7],[0.7 1 0.7]};

printstring={};

if ~isempty(cfd),
    parms=dbReadData(rawid);
    
    if isempty(axes),
       figure
       rrange=1:min(length(cfd),2);
    else
       rrange=1;
    end
    
    for cfidx=rrange,
        mfile=[cfd(cfidx).stimpath cfd(cfidx).stimfile];
        channel=cfd(cfidx).channum;
        unit=cfd(cfidx).unit;
        if ~isempty(axes),
           h=axes;
        else
           h=subplot(2,1,cfidx);
        end
        if isempty(findstr('Torc',parms.ReferenceClass)) &&...
              parms.Ref_Duration<0.5,
           options=struct('rasterfs',5000,'psthfs',100,'usesorted',1,...
              'psth',1,'datause','Both');
           if parms.Ref_PostStimSilence>0.2,
               options.PreStimSilence=parms.Ref_PreStimSilence;
               options.PostStimSilence=0.2;
           end
        else
           % special for Torcs, 
           options=struct('rasterfs',1000,'psthfs',20,'usesorted',1,...
              'psth',1,'datause','Light/no light');
           %options=struct('rasterfs',1000,'psthfs',20,'usesorted',1,...
           %   'psth',1,'datause','Light+Collapse both');
           options.PreStimSilence=0.2;
           options.PostStimSilence=0.2;
        end
        options.raster_pix=1;
        if ~isempty(cfd(cfidx).goodtrials),
            options.trialrange=eval(cfd(cfidx).goodtrials);
        end
        options.spc=spc;
        options.ssc=ssc;
        raster_online(mfile,channel,unit,h,options);
        title(sprintf('%s - %s(%d) - iso: %.1f',cfd(cfidx).cellid,...
           basename(mfile),rawid,cfd(cfidx).isolation));
        if strcmpi(parms.TrialObjectClass,'RefTarOpt'),
            hold on;
            aa=axis;
            if strcmpi(parms.Trial_LightEpoch,'Sound') ||...
                    strcmpi(parms.Trial_LightEpoch,'SoundOnset'),
                lightontime=parms.Trial_LightPulseShift;
                lightofftime=parms.Trial_LightPulseShift+...
                    parms.Trial_LightPulseDuration;
            elseif strcmpi(parms.Trial_LightEpoch,'WholeTrial'),
                lightontime=parms.Trial_LightPulseShift+aa(1);
                lightofftime=parms.Trial_LightPulseShift+...
                    parms.Trial_LightPulseDuration+aa(1);
            end
            plot(lightontime*[1 1],aa(3:4),'r--');
            plot(lightofftime*[1 1],aa(3:4),'r--');
        end
        hold off
        
    end
    %subplot(2,1,1);
    %[pp,bb,ee]=fileparts(mfile);
    %title(sprintf('%s - rawid %d',bb,rawid));
    if isempty(axes),
       basedir='/auto/users/svd/docs/current/opto/';
       
       set(gcf,'PaperPosition',[2.25 2.5 3 6]);
    
       printstring{end+1}=...
          sprintf('print -f%d -dpdf %s%s.pdf',gcf,basedir,bb);
    end
end

if isempty(axes),
   disp('TO PRINT:');
   for ii=1:length(printstring),
      disp(printstring{ii});
   end
end